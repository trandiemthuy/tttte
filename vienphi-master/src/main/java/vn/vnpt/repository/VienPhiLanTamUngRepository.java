package vn.vnpt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.VienPhiLanTamUng;

import java.util.List;
import java.util.Optional;

public interface VienPhiLanTamUngRepository extends JpaRepository<VienPhiLanTamUng, Long> {

    List<VienPhiLanTamUng> findAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndTtLanTamUng(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt, int ttLanTamUng);
    List<VienPhiLanTamUng> findAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndTtLanTamUngAndTtSuDung(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt, int ttLanTamUng, int ttSuDung);

    Optional<VienPhiLanTamUng> findBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndSttLanTamUng(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt, String sttLanTamUng);

    int countAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvtt(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt);
    int countAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndTtLanTamUng(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt, int ttLanTamUng);

    @Transactional
    @Modifying
    @Query("update VienPhiLanTamUng set ttSuDung = 1, sttLanTt = :sttLanTt where soVaoVien = :soVaoVien and soVaoVienDt = :soVaoVienDt and loaiKcb = :loaiKcb and dvtt = :dvtt and ttLanTamUng = 1 and ttSuDung = 0")
    void setDaSuDungTamUng(
        @Param("soVaoVien") long soVaoVien,
        @Param("soVaoVienDt") long soVaoVienDt,
        @Param("loaiKcb") int loaiKcb,
        @Param("dvtt") String dvtt,
        @Param("sttLanTt") String sttLanTt
    );

    @Transactional
    @Modifying
    @Query("update VienPhiLanTamUng set ttSuDung = 0, sttLanTt = null where soVaoVien = :soVaoVien and soVaoVienDt = :soVaoVienDt and loaiKcb = :loaiKcb and dvtt = :dvtt and sttLanTt = :sttLanTt and ttSuDung = 1 and ttLanTamUng = 1")
    void setChuaSuDungTamUng(
        @Param("soVaoVien") long soVaoVien,
        @Param("soVaoVienDt") long soVaoVienDt,
        @Param("loaiKcb") int loaiKcb,
        @Param("dvtt") String dvtt,
        @Param("sttLanTt") String sttLanTt
    );
}
