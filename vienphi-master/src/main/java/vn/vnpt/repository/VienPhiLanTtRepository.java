package vn.vnpt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.vnpt.domain.VienPhiLanTt;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface VienPhiLanTtRepository extends JpaRepository<VienPhiLanTt, Long> {

    List<VienPhiLanTt> findAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndTtLanTt(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt, int ttLanTt);

    Optional<VienPhiLanTt> findBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndSttLanTt(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt, String sttLanTt);

    int countAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvtt(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt);
    int countAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndTtLanTt(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt, int ttLanTt);

    @Query(value = "select COALESCE(SUM(soTienThucThu), 0) from VienPhiLanTt where soVaoVien = :soVaoVien and soVaoVienDt = :soVaoVienDt and loaiKcb = :loaiKcb and dvtt = :dvtt and ttLanTt = 1")
    BigDecimal getTongSoTienThucThuLanTt(@Param("soVaoVien") long soVaoVien, @Param("soVaoVienDt") long soVaoVienDt, @Param("loaiKcb") int loaiKcb, @Param("dvtt") String dvtt);

    @Query(value = "select COALESCE(SUM(soTienThoiLai), 0) from VienPhiLanTt where soVaoVien = :soVaoVien and soVaoVienDt = :soVaoVienDt and loaiKcb = :loaiKcb and dvtt = :dvtt and ttLanTt = 1")
    BigDecimal getTongSoTienThoiLaiLanTt(@Param("soVaoVien") long soVaoVien, @Param("soVaoVienDt") long soVaoVienDt, @Param("loaiKcb") int loaiKcb, @Param("dvtt") String dvtt);

}
