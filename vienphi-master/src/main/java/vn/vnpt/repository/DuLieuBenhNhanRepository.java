package vn.vnpt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.vnpt.domain.DuLieuBenhNhan;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author NguyenDucAnhKhoi
 */
public interface DuLieuBenhNhanRepository extends JpaRepository<DuLieuBenhNhan, Long> {

    Optional<DuLieuBenhNhan> findAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvtt(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt);

    @Query(
        nativeQuery = true,
        value = "select NVL(MAX(TO_NUMBER(SUBSTR(MA_LK, 0, INSTR(MA_LK, '/') - 1))), 0) from BSGD_VIENPHI.KCB_DULIEUBENHNHAN where dvtt = :dvtt and EXTRACT(year from NGAY_VAO) = :year"
    )
    Long getMaxSttMaLkByDvttAndYear(@Param("dvtt") String dvtt, @Param("year") long year);

    @Query(
        nativeQuery = true,
        value = "select * from BSGD_VIENPHI.KCB_DULIEUBENHNHAN where DVTT = :dvtt and LOAIKCB = :loaiKcb and trunc(NGAY_VAO) between :tuNgay and :denNgay"
    )
    List<DuLieuBenhNhan> getDanhSachBenhNhan(@Param("tuNgay") Date tuNgay, @Param("denNgay") Date denNgay, @Param("loaiKcb") int loaiKcb, @Param("dvtt") String dvtt);

    @Query(
        nativeQuery = true,
        value = "select * from BSGD_VIENPHI.KCB_DULIEUBENHNHAN where DVTT = :dvtt and LOAIKCB = :loaiKcb and trunc(NGAY_VAO) between :tuNgay and :denNgay and MA_BN= :maBn"
    )
    Optional<DuLieuBenhNhan> timBenhNhan(@Param("tuNgay") Date tuNgay, @Param("denNgay") Date denNgay, @Param("loaiKcb") int loaiKcb, @Param("dvtt") String dvtt, @Param("maBn") long maBn);

    @Query(
        nativeQuery = true,
        value = "select * from BSGD_VIENPHI.KCB_DULIEUBENHNHAN where DVTT = :dvtt and LOAIKCB = :loaiKcb and trunc(NGAY_RA) between :tuNgay and :denNgay and NGAY_RA is not null"
    )
    List<DuLieuBenhNhan> getDanhSachBenhNhanHoanTatKham(@Param("tuNgay") Date tuNgay, @Param("denNgay") Date denNgay, @Param("loaiKcb") int loaiKcb, @Param("dvtt") String dvtt);
}
