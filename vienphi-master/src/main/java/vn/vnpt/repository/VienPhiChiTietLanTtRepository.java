package vn.vnpt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.vnpt.domain.VienPhiChiTietLanTt;

/**
 * @author NguyenDucAnhKhoi
 */
public interface VienPhiChiTietLanTtRepository extends JpaRepository<VienPhiChiTietLanTt, Long> {
}
