package vn.vnpt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.vnpt.domain.BangKe;
import vn.vnpt.service.dto.bangke.BangKeTongTienDto;

import java.util.List;
import java.util.Map;

public interface BangKeRepository extends JpaRepository<BangKe, Long> {

    @Query(
        value = "select * from BSGD_VIENPHI.BANGKE where SOVAOVIEN = :soVaoVien and SOVAOVIEN_DT = :soVaoVienDt and LOAIKCB = :loaiKcb and DVTT = :dvtt ORDER BY to_number(substr(TEN_NHOM, 1, instr(TEN_NHOM, '.'))), TEN_NHOM_CON, VATTU_STENT, STT_STENT",
        nativeQuery = true
    )
   List<BangKe> findAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvtt(@Param("soVaoVien") long soVaoVien, @Param("soVaoVienDt") long soVaoVienDt, @Param("loaiKcb") int loaiKcb, @Param("dvtt") String dvtt);

    void deleteAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvtt(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt);

    @Query(
        value = "select sum(thanhTienBv) as tongThanhTienBv, sum(thanhTienBh) as tongThanhTienBh, sum(quyBhyt) as tongQuyBhyt, sum(cungChiTra) as tongCungChiTra, sum(khac) as tongKhac, sum(nguoiBenh) as tongNguoiBenh from BangKe where soVaoVien = :soVaoVien and soVaoVienDt = :soVaoVienDt and loaiKcb = :loaiKcb and dvtt = :dvtt"
    )
    Map<String, Object> getTongTien(@Param("soVaoVien") long soVaoVien, @Param("soVaoVienDt") long soVaoVienDt, @Param("loaiKcb") int loaiKcb, @Param("dvtt") String dvtt);

}
