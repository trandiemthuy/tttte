package vn.vnpt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.vnpt.domain.VienPhiQuyenBienLai;

import java.util.List;

public interface VienPhiQuyenBienLaiRepository extends JpaRepository<VienPhiQuyenBienLai, Long> {

    List<VienPhiQuyenBienLai> findAllByDvttAndTrangThaiOrderByTenQuyenBienLai(String dvtt, int trangThai);
    List<VienPhiQuyenBienLai> findAllByDvttOrderByTenQuyenBienLai(String dvtt);
}
