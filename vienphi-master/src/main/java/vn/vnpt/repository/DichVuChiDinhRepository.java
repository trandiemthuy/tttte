package vn.vnpt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.DichVuChiDinh;

/**
 * @author NguyenDucAnhKhoi
 */
public interface DichVuChiDinhRepository extends JpaRepository<DichVuChiDinh, Long> {

    @Transactional
    @Modifying
    @Query(
        nativeQuery = true,
        value = "merge into BSGD_VIENPHI.KCB_DICHVUCHIDINH dv " +
            "using (select SOVAOVIEN, SOVAOVIEN_DT, DVTT, LOAIKCB, SOPHIEU_MADV, ROW_NUMBER() OVER (ORDER BY NGAY_YL) as STT_CONGKHAM from BSGD_VIENPHI.KCB_DICHVUCHIDINH where SOVAOVIEN = :soVaoVien and SOVAOVIEN_DT = :soVaoVienDt and LOAIKCB = :loaiKcb and DVTT = :dvtt and MA_NHOM_4210 = 13) t " +
            "on (dv.SOVAOVIEN = t.SOVAOVIEN and dv.SOVAOVIEN_DT = t.SOVAOVIEN_DT and dv.DVTT = t.DVTT and dv.LOAIKCB = t.LOAIKCB and dv.SOPHIEU_MADV = t.SOPHIEU_MADV) " +
            "when MATCHED THEN " +
            "UPDATE SET dv.STT_CONGKHAM = t.STT_CONGKHAM"
    )
    public void updateSttCongKham(@Param("soVaoVien") long soVaoVien, @Param("soVaoVienDt") long soVaoVienDt, @Param("loaiKcb") int loaiKcb, @Param("dvtt") String dvtt);


    @Transactional
    @Modifying
    @Query(
        nativeQuery = true,
        value = "merge into BSGD_VIENPHI.KCB_DICHVUCHIDINH dv " +
            "using (select SOVAOVIEN, SOVAOVIEN_DT, DVTT, LOAIKCB, SOPHIEU_MADV, ROW_NUMBER() OVER (ORDER BY don_gia DESC) as STT_STENT from BSGD_VIENPHI.KCB_DICHVUCHIDINH where SOVAOVIEN = :soVaoVien and SOVAOVIEN_DT = :soVaoVienDt and LOAIKCB = :loaiKcb and DVTT = :dvtt  and VATTU_CHUNGGOI = 1 and VATTU_STENT = 1) t " +
            "on (dv.SOVAOVIEN = t.SOVAOVIEN and dv.SOVAOVIEN_DT = t.SOVAOVIEN_DT and dv.DVTT = t.DVTT and dv.LOAIKCB = t.LOAIKCB and dv.SOPHIEU_MADV = t.SOPHIEU_MADV) " +
            "when MATCHED THEN " +
            "UPDATE SET dv.STT_STENT = t.STT_STENT"
    )
    public void updateSttStent(@Param("soVaoVien") long soVaoVien, @Param("soVaoVienDt") long soVaoVienDt, @Param("loaiKcb") int loaiKcb, @Param("dvtt") String dvtt);

}
