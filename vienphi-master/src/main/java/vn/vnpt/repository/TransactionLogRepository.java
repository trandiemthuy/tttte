package vn.vnpt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.vnpt.domain.TransactionLog;

public interface TransactionLogRepository extends JpaRepository<TransactionLog, Long> {
}
