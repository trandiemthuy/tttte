package vn.vnpt.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "BSGD_VIENPHI", name = "VIENPHI_LANTAMUNG",
    uniqueConstraints = { @UniqueConstraint(columnNames = {"SOVAOVIEN", "SOVAOVIEN_DT", "LOAIKCB", "DVTT", "STT_LANTAMUNG"}) },
    indexes = {
        @Index(columnList = "SOVAOVIEN, SOVAOVIEN_DT, LOAIKCB, DVTT")
    }
)
public class VienPhiLanTamUng implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "ID_LANTAMUNG")
    private Long idLanTamUng;

    @Column(name = "SOVAOVIEN")
    private long soVaoVien;

    @Column(name = "SOVAOVIEN_DT")
    private long soVaoVienDt;

    @Column(name = "LOAIKCB", columnDefinition = "NUMBER(1)")
    private int loaiKcb;

    @Column(name = "DVTT", length = 50)
    private String dvtt;

    @Column(name = "STT_LANTAMUNG", length = 50)
    private String sttLanTamUng;

    @Column(name = "MA_BN")
    private long maBn;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_GIO_TAO")
    private Date ngayGioTao;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_GIO_CAP_NHAT")
    private Date ngayGioCapNhat;

    @Column(name = "MA_NV_TAO")
    private long maNvTao;

    @Column(name = "TEN_NV_TAO", length = 100)
    private String tenNvTao;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_THU")
    private Date ngayThu;

    @Column(name = "MA_PHONGBAN_THU", length = 50)
    private String maPhongBanThu;

    @Column(name = "SO_TIEN_TAM_UNG", precision = 18, scale = 4)
    private BigDecimal soTienTamUng;

    @Column(name = "SO_TIEN_BN_TRA", precision = 18, scale = 4)
    private BigDecimal soTienBnTra;

    @Column(name = "SO_TIEN_THOI_LAI", precision = 18, scale = 4)
    private BigDecimal soTienThoiLai;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "MA_QUYEN_BIENLAI")
    private VienPhiQuyenBienLai vienPhiQuyenBienLai;

    @Column(name = "SO_BIENLAI")
    private Integer soBienLai;

    @Column(name = "GHICHU", length = 50)
    private String ghiChu;

    @Column(name = "TT_LANTAMUNG", columnDefinition = "NUMBER(1)")
    private int ttLanTamUng;

    @Column(name = "TT_SUDUNG", columnDefinition = "NUMBER(1)")
    private int ttSuDung;

    @Column(name = "STT_LANTT", length = 50)
    private String sttLanTt;

    @Column(name = "ID_LANTAMUNG_NGOAI")
    private Integer idLanTamUngNgoai;

    @PrePersist
    private void onPrePersist() {
    }

    @PreUpdate
    private void onPreUpdate() {
    }

}
