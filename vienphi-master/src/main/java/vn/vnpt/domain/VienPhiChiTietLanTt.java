package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "BSGD_VIENPHI", name = "VIENPHI_CHITIETLANTT",
    indexes = {
        @Index(columnList = "SOVAOVIEN, SOVAOVIEN_DT, LOAIKCB, DVTT, STT_LANTT")
    }
)
public class VienPhiChiTietLanTt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "ID_CHITIETLANTT")
    private long idChiTietLanTt;

    @Column(name = "STT_CHITIETLANTT", length = 50)
    private String sttChiTietLanTt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TRANSACTION")
    private TransactionLog transactionLog;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "SOVAOVIEN", referencedColumnName = "SOVAOVIEN"),
        @JoinColumn(name = "SOVAOVIEN_DT", referencedColumnName = "SOVAOVIEN_DT"),
        @JoinColumn(name = "LOAIKCB", referencedColumnName = "LOAIKCB"),
        @JoinColumn(name = "DVTT", referencedColumnName = "DVTT"),
        @JoinColumn(name = "STT_LANTT", referencedColumnName = "STT_LANTT")
    })
    private VienPhiLanTt vienPhiLanTt;

    @Column(name = "MA_BN")
    private long maBn;

    @Column(name = "SOPHIEU_MADV", length = 50)
    private String soPhieuMaDv;

    @Column(name = "MA_NHOM_4210", columnDefinition = "NUMBER(2)")
    private int maNhom4210;

    @Column(name = "NHOM_LANTT", length = 50)
    private String nhomLanTt;

    @Column(name = "NOI_DUNG", length = 500)
    private String noiDung;

    @Column(name = "DVT", length = 50)
    private String dvt;

    @Column(name = "SO_LUONG", precision = 18, scale = 4)
    private BigDecimal soLuong;

    @Column(name = "DON_GIA", precision = 18, scale = 4)
    private BigDecimal donGia;

    @Column(name = "THANH_TIEN", precision = 18, scale = 4)
    private BigDecimal thanhTien;

    @Column(name = "QUY_BHYT", precision = 18, scale = 4)
    private BigDecimal quyBhyt;

    @Column(name = "CUNG_CHI_TRA", precision = 18, scale = 4)
    private BigDecimal cungChiTra;

    @Column(name = "KHAC", precision = 18, scale = 4)
    private BigDecimal khac;

    @Column(name = "NGUOI_BENH", precision = 18, scale = 4)
    private BigDecimal nguoiBenh;

    @Column(name = "TRAN_TT", precision = 18, scale = 4)
    private BigDecimal tranTt;

    @Column(name = "PHAI_TT", precision = 18, scale = 4)
    private BigDecimal phaiTt;

    @Column(name = "GHICHU_CHITIETLANTT", length = 500)
    private String ghiChuChiTietLanTt;

}
