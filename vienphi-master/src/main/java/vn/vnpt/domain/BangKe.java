package vn.vnpt.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(schema = "BSGD_VIENPHI", name = "BANGKE",
    indexes = {
        @Index(columnList = "SOVAOVIEN, SOVAOVIEN_DT, LOAIKCB, DVTT")
    }
)
public class BangKe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_BANGKE")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long idBangKe;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TRANSACTION")
    private TransactionLog transactionLog;

    @Column(name = "SOVAOVIEN")
    private long soVaoVien;

    @Column(name = "SOVAOVIEN_DT")
    private long soVaoVienDt;

    @Column(name = "LOAIKCB", columnDefinition = "NUMBER(1)")
    private int loaiKcb;

    @Column(name = "DVTT", length = 50)
    private String dvtt;

    @Column(name = "MA_BN")
    private long maBn;

    @Column(name = "TEN_NHOM", length = 500)
    private String tenNhom;

    @Column(name = "MA_NHOM_4210", columnDefinition = "NUMBER(2)")
    private int maNhom4210;

    @Column(name = "TEN_NHOM_CON", length = 500)
    private String tenNhomCon;

    @Column(name = "NGOAI_DANH_MUC", columnDefinition = "NUMBER(1)")
    private int ngoaiDanhMuc;

    @Column(name = "NOI_DUNG", length = 500)
    private String noiDung;

    @Column(name = "DVT", length = 50)
    private String dvt;

    @Column(name = "SO_LUONG", precision = 18, scale = 4)
    private BigDecimal soLuong;

    @Column(name = "DON_GIA_BV", precision = 18, scale = 4)
    private BigDecimal donGiaBv;

    @Column(name = "DON_GIA_BH", precision = 18, scale = 4)
    private BigDecimal donGiaBh;

    @Column(name = "TLTT_THEO_DV", precision = 18, scale = 4)
    private BigDecimal tyLeThanhToanTheoDv;

    @Column(name = "THANH_TIEN_BV", precision = 18, scale = 4)
    private BigDecimal thanhTienBv;

    @Column(name = "TLTT_BHYT", precision = 18, scale = 4)
    private BigDecimal tyLeThanhToanBhyt;

    @Column(name = "THANH_TIEN_BH", precision = 18, scale = 4)
    private BigDecimal thanhTienBh;

    @Column(name = "QUY_BHYT", precision = 18, scale = 4)
    private BigDecimal quyBhyt;

    @Column(name = "CUNG_CHI_TRA", precision = 18, scale = 4)
    private BigDecimal cungChiTra;

    @Column(name = "KHAC", precision = 18, scale = 4)
    private BigDecimal khac;

    @Column(name = "NGUOI_BENH", precision = 18, scale = 4)
    private BigDecimal nguoiBenh;

    @Column(name = "SOPHIEU_MADV", length = 50)
    private String soPhieuMaDv;

    @Column(name = "TT_THANHTOAN", columnDefinition = "NUMBER(1)")
    private Integer ttThanhToan;

    @Column(name = "GHI_CHU", length = 50)
    private String ghiChu;

    @Column(name = "VATTU_CHUNGGOI", columnDefinition = "NUMBER(1)")
    private Integer vatTuChungGoi;

    @Column(name = "STT_GOIVATTU", columnDefinition = "NUMBER(3)")
    private Integer sttGoiVatTu;

    @Column(name = "VATTU_STENT", columnDefinition = "NUMBER(1)")
    private Integer vatTuStent;

    @Column(name = "STT_STENT", columnDefinition = "NUMBER(3)")
    private Integer sttStent;

    @Column(name = "TRAN_TT", precision = 18, scale = 4)
    private BigDecimal tranTt;

    @Column(name = "MA_THE", length = 15)
    private String maThe;

    @Column(name = "GT_THE_TU")
    @Temporal(TemporalType.DATE)
    private Date gtTheTu;

    @Column(name = "GT_THE_DEN")
    @Temporal(TemporalType.DATE)
    private Date gtTheDen;

    @Column(name = "MUC_HUONG", precision = 3)
    private BigDecimal mucHuong;

    @Column(name = "CHI_PHI_KCB_TU")
    @Temporal(TemporalType.DATE)
    private Date chiPhiKcbTu;

    @Column(name = "CHI_PHI_KCB_DEN")
    @Temporal(TemporalType.DATE)
    private Date chiPhiKcbDen;

    @Column(name = "STT_DIEUTRI", columnDefinition = "NUMBER(3)")
    private Integer sttDieuTri;

}
