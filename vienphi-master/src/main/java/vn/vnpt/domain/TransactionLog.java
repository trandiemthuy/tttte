package vn.vnpt.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "BSGD_VIENPHI", name = "TRANSACTION_LOG",
    indexes = {
        @Index(columnList = "THOI_GIAN, DVTT")
    }
)
public class TransactionLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "ID_TRANSACTION")
    private long idTransaction;

    @Column(name = "DVTT", length = 50)
    private String dvtt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "THOI_GIAN")
    @CreatedDate
    private Date thoiGian;

    @Column(name = "OBJECT_ID", length = 100)
    private String objectId;
}
