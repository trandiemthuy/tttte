package vn.vnpt.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author NguyenDucAnhKhoi
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(schema = "BSGD_VIENPHI", name = "KCB_DULIEUBENHNHAN",
    uniqueConstraints = @UniqueConstraint(columnNames = {"MA_LK", "SOVAOVIEN", "SOVAOVIEN_DT", "LOAIKCB", "DVTT"}),
    indexes = {
        @Index(columnList = "SOVAOVIEN, SOVAOVIEN_DT, LOAIKCB, DVTT"),
        @Index(columnList = "NGAY_VAO, LOAIKCB, DVTT"),
        @Index(columnList = "NGAY_RA, LOAIKCB, DVTT")
    }
)
public class DuLieuBenhNhan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "ID_DULIEUBENHNHAN")
    private long idDuLieuBenhNhan;

    @Column(name = "MA_LK", length = 50)
    private String maLk;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TRANSACTION")
    private TransactionLog transactionLog;

    @Column(name = "SOVAOVIEN")
    private long soVaoVien;

    @Column(name = "SOVAOVIEN_DT")
    private long soVaoVienDt;

    @Column(name = "LOAIKCB", columnDefinition = "NUMBER(1)")
    private int loaiKcb;

    @Column(name = "DVTT", length = 50)
    private String dvtt;

    @Column(name = "MA_BN", length = 100)
    private long maBn;

    @Column(name = "HO_TEN", length = 255)
    private String hoTen;

    @Temporal(TemporalType.DATE)
    @Column(name = "NGAY_SINH")
    private Date ngaySinh;

    @Column(name = "GIOI_TINH", columnDefinition = "NUMBER(1)")
    private int gioiTinh;

    @Column(name = "DIA_CHI", length = 1024)
    private String diaChi;

    @Column(name = "STT_DIEUTRI", columnDefinition = "NUMBER(3)")
    private Integer sttDieuTri;

    @Column(name = "CO_BHYT", columnDefinition = "NUMBER(1)")
    private Integer coBhyt;

    @Column(name = "MA_THE", length = 15)
    private String maThe;

    @Column(name = "MA_DKBD", length = 50)
    private String maDkbd;

    @Column(name = "TEN_DKBD", length = 1000)
    private String tenDkbd;

    @Temporal(TemporalType.DATE)
    @Column(name = "GT_THE_TU")
    private Date gtTheTu;

    @Temporal(TemporalType.DATE)
    @Column(name = "GT_THE_DEN")
    private Date gtTheDen;

    @Column(name = "PHAN_TRAM_THE", precision = 3, scale = 0)
    private BigDecimal phanTramThe;

    @Temporal(TemporalType.DATE)
    @Column(name = "NGAY_DU_5_NAM")
    private Date ngayDu5nam;

    @Temporal(TemporalType.DATE)
    @Column(name = "MIEN_CUNG_CT")
    private Date mienCungCt;

    @Column(name = "MA_KHUVUC", length = 2)
    private String maKhuVuc;

    @Column(name = "CAN_NANG", precision = 10, scale = 4)
    private BigDecimal canNang;

    @Column(name = "TRANG_THAI_BN", columnDefinition = "NUMBER(1)")
    private Integer trangThaiBn;

    @Column(name = "MA_BENH", length = 15)
    private String maBenh;

    @Column(name = "TEN_BENH", length = 500)
    private String tenBenh;

    @Column(name = "MA_BENHKHAC", length = 255)
    private String maBenhKhac;

    @Column(name = "TEN_BENHKHAC", length = 255)
    private String tenBenhKhac;

    @Column(name = "MA_LYDO_VVIEN", columnDefinition = "NUMBER(1)")
    private Integer maLyDoVvien;

    @Column(name = "MA_NOI_CHUYEN", length = 5)
    private String maNoiChuyen;

    @Column(name = "MA_TAI_NAN", columnDefinition = "NUMBER(1)")
    private Integer maTaiNan;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_VAO")
    private Date ngayVao;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_KHAM")
    private Date ngayKham;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_RA")
    private Date ngayRa;

    @Column(name = "KET_QUA_DTRI", columnDefinition = "NUMBER(1)")
    private Integer ketQuaDtri;

    @Column(name = "TINH_TRANG_RV", columnDefinition = "NUMBER(1)")
    private Integer tinhTrangRv;

    @Column(name = "MA_KHOA_BC", precision = 15)
    private String maKhoaBc;

    @Column(name = "MA_KHOA", precision = 15)
    private String maKhoa;

    @Column(name = "TEN_KHOA", precision = 1000)
    private String tenKhoa;

    @Column(name = "TEN_CSKCB", precision = 1000)
    private String tenCskcb;

    @Column(name = "TUYEN_DONVI", columnDefinition = "NUMBER(1)")
    private Integer tuyenDonVi;

    @Column(name = "TEN_TINH", precision = 500)
    private String tenTinh;

    @Column(name = "TT_THANHTOAN", columnDefinition = "NUMBER(19) DEFAULT 0")
    private Integer ttThanhToan;

    @Column(name = "TT_TAMUNG", columnDefinition = "NUMBER(19) DEFAULT 0")
    private Integer ttTamUng;

    @Column(name = "TT_IN_BANGKE", columnDefinition = "NUMBER(19) DEFAULT 0")
    private Integer ttInBangKe;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "duLieuBenhNhan")
    private List<DichVuChiDinh> dsDichVuChiDinh;

    @PrePersist
    public void setDefaultColumn() {
        if (ttThanhToan == null) {
            ttThanhToan = 0;
        }
        if (ttTamUng == null) {
            ttTamUng = 0;
        }
        if (ttInBangKe == null) {
            ttInBangKe = 0;
        }
    }

}
