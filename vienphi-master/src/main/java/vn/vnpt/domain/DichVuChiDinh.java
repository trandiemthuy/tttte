package vn.vnpt.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author NguyenDucAnhKhoi
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(schema = "BSGD_VIENPHI", name = "KCB_DICHVUCHIDINH",
    indexes = {
        @Index(columnList = "MA_LK, SOVAOVIEN, SOVAOVIEN_DT, LOAIKCB, DVTT")
    }
)
public class DichVuChiDinh {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_DICHVUCHIDINH")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private long idDichVuChiDinh;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TRANSACTION")
    private TransactionLog transactionLog;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "MA_LK", referencedColumnName = "MA_LK"),
        @JoinColumn(name = "SOVAOVIEN", referencedColumnName = "SOVAOVIEN"),
        @JoinColumn(name = "SOVAOVIEN_DT", referencedColumnName = "SOVAOVIEN_DT"),
        @JoinColumn(name = "LOAIKCB", referencedColumnName = "LOAIKCB"),
        @JoinColumn(name = "DVTT", referencedColumnName = "DVTT")
    })
    private DuLieuBenhNhan duLieuBenhNhan;

    @Column(name = "MA_BN")
    private Long maBn;

    @Column(name = "SOPHIEU_MADV", length = 50)
    private String soPhieuMaDv;

    @Column(name = "GHI_CHU", length = 50)
    private String ghiChu;

    @Column(name = "MA_THUOC", length = 255)
    private String maThuoc;

    @Column(name = "TEN_THUOC", length = 1024)
    private String tenThuoc;

    @Column(name = "MA_DICH_VU", length = 20)
    private String maDichVu;

    @Column(name = "TEN_DICH_VU", length = 1024)
    private String tenDichVu;

    @Column(name = "MA_VAT_TU", length = 255)
    private String maVatTu;

    @Column(name = "TEN_VAT_TU", length = 1024)
    private String tenVatTu;

    @Column(name = "MA_GIUONG", length = 14)
    private String maGiuong;

    @Column(name = "DVT", length = 50)
    private String dvt;

    @Column(name = "SO_LUONG", precision = 10, scale = 4)
    private BigDecimal soLuong;

    @Column(name = "DON_GIA", precision = 18, scale = 4)
    private BigDecimal donGia;

    @Column(name = "NGOAI_DANH_MUC", columnDefinition = "NUMBER(1)")
    private Integer ngoaiDanhMuc;

    @Column(name = "MA_NHOM_4210", columnDefinition = "NUMBER(2)")
    private int maNhom4210;

    @Column(name = "HAM_LUONG", length = 1024)
    private String hamLuong;

    @Column(name = "DUONG_DUNG", length = 20)
    private String duongDung;

    @Column(name = "LIEU_DUNG", length = 255)
    private String lieuDung;

    @Column(name = "SO_DANG_KY", length = 255)
    private String soDangKy;

    @Column(name = "TT_THAU", length = 50)
    private String ttThau;

    @Column(name = "MA_KHOA", length = 15)
    private String maKhoa;

    @Column(name = "MA_BAC_SI", length = 255)
    private String maBacSi;

    @Column(name = "MA_BENH", length = 255)
    private String maBenh;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_YL")
    private Date ngayYl;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_KQ")
    private Date ngayKq;

    @Column(name = "MA_PTTT", columnDefinition = "NUMBER(1)")
    private Integer maPttt;

    @Column(name = "STT_CONGKHAM", columnDefinition = "NUMBER(3)")
    private Integer sttCongKham;

    @Column(name = "NAM_CUNG_GIUONG", columnDefinition = "NUMBER(1)")
    private Integer namCungGiuong;

    @Column(name = "SO_NGUOI_NAM", columnDefinition = "NUMBER(3)")
    private Integer soNguoiNam;

    @Column(name = "DVKT_PHAT_SINH", columnDefinition = "NUMBER(1)")
    private Integer dvktPhatSinh;

    @Column(name = "CUNG_EKIP_PT", columnDefinition = "NUMBER(1)")
    private Integer cungEkipPt;

    @Column(name = "TLTT_BHYT_CLS", precision = 18, scale = 4)
    private BigDecimal tyLeTtBhytCls;

    @Column(name = "TL_THUOC_VTYT", precision = 18, scale = 4)
    private BigDecimal tyLeThuocVtyt;

    @Column(name = "VATTU_CHUNGGOI", columnDefinition = "NUMBER(1)")
    private Integer vatTuChungGoi;

    @Column(name = "STT_GOIVATTU", columnDefinition = "NUMBER(3)")
    private Integer sttGoiVatTu;

    @Column(name = "VATTU_STENT", columnDefinition = "NUMBER(1)")
    private Integer vatTuStent;

    @Column(name = "STT_STENT", columnDefinition = "NUMBER(3)")
    private Integer sttStent;

    @Column(name = "TRAN_TT", precision = 18, scale = 4)
    private BigDecimal tranTt;

    @Column(name = "TT_THANHTOAN", columnDefinition = "NUMBER(1) DEFAULT 0")
    private Integer ttThanhToan;

    @PrePersist
    public void setDefaultColumn() {
        if (ttThanhToan == null) {
            ttThanhToan = 0;
        }
    }
}
