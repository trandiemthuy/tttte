package vn.vnpt.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "BSGD_VIENPHI", name = "VIENPHI_QUYENBIENLAI",
    indexes = {
        @Index(columnList = "DVTT, TRANGTHAI")
    }
)
public class VienPhiQuyenBienLai implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "MA_QUYEN_BIENLAI")
    private Long maQuyenBienLai;

    @Column(name = "TEN_QUYEN_BIENLAI", length = 500)
    private String tenQuyenBienLai;

    @Column(name = "KYHIEU_QUYEN_BIENLAI", length = 500)
    private String kyHieuQuyenBienLai;

    @Column(name = "DVTT", length = 50)
    private String dvtt;

    @Column(name = "SO_BATDAU")
    private int soBatDau;

    @Column(name = "SO_KETTHUC")
    private int soKetThuc;

    @Column(name = "SO_HIENTAI")
    private int soHienTai;

    @Column(name = "TRANGTHAI", columnDefinition = "NUMBER(1)")
    private int trangThai;

    @Column(name = "NHANVIEN")
    private long nhanVien;

    @Column(name = "NGOAITRU", columnDefinition = "NUMBER(1)")
    private int ngoaiTru;

    @Column(name = "TAMUNG", columnDefinition = "NUMBER(1)")
    private int tamUng;

    @Column(name = "BHYT", columnDefinition = "NUMBER(1)")
    private int bhyt;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_GIO_TAO")
    private Date ngayGioTao;

}
