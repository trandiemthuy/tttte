package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "BSGD_VIENPHI", name = "VIENPHI_LANTT",
    uniqueConstraints = { @UniqueConstraint(columnNames = {"SOVAOVIEN", "SOVAOVIEN_DT", "LOAIKCB", "DVTT", "STT_LANTT"}) },
    indexes = {
        @Index(columnList = "SOVAOVIEN, SOVAOVIEN_DT, LOAIKCB, DVTT")
    }
)
public class VienPhiLanTt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "ID_LANTT")
    private long idLanTt;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ID_TRANSACTION")
    private TransactionLog transactionLog;

    @Column(name = "SOVAOVIEN")
    private long soVaoVien;

    @Column(name = "SOVAOVIEN_DT")
    private long soVaoVienDt;

    @Column(name = "LOAIKCB", columnDefinition = "NUMBER(1)")
    private int loaiKcb;

    @Column(name = "DVTT", length = 50)
    private String dvtt;

    @Column(name = "STT_LANTT", length = 50)
    private String sttLanTt;

    @Column(name = "MA_BN")
    private long maBn;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_GIO_TAO")
    private Date ngayGioTao;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_GIO_CAP_NHAT")
    private Date ngayGioCapNhat;

    @Column(name = "MA_NV_TAO")
    private long maNvTao;

    @Column(name = "TEN_NV_TAO", length = 100)
    private String tenNvTao;

    @Column(name = "MA_PHONGBAN_TT", length = 50)
    private String maPhongBanTt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_THU")
    private Date ngayThu;

    @Column(name = "SO_TIEN_PHAITT", precision = 18, scale = 4)
    private BigDecimal soTienPhaiTt;

    @Column(name = "SO_TIEN_MIEN_GIAM", precision = 18, scale = 4)
    private BigDecimal soTienMienGiam;

    @Column(name = "SO_TIEN_TAM_UNG", precision = 18, scale = 4)
    private BigDecimal soTienTamUng;

    @Column(name = "SO_TIEN_THUC_THU", precision = 18, scale = 4)
    private BigDecimal soTienThucThu;

    @Column(name = "SO_TIEN_THOI_LAI", precision = 18, scale = 4)
    private BigDecimal soTienThoiLai;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "MA_QUYEN_BIENLAI")
    private VienPhiQuyenBienLai vienPhiQuyenBienLai;

    @Column(name = "SO_BIENLAI")
    private int soBienLai;

    @Column(name = "HINHTHUC_TT")
    private int hinhThucTt;

    @Column(name = "TT_LANTT", columnDefinition = "NUMBER(1)")
    private int ttLanTt;

    @Column(name = "GHI_CHU", length = 500)
    private String ghiChu;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "vienPhiLanTt")
    private List<VienPhiChiTietLanTt> listVienPhiChiTietLanTt;

    @PrePersist
    private void onPrePersist() {
    }

    @PreUpdate
    private void onPreUpdate() {
    }

}
