package vn.vnpt.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.DichVuChiDinh;
import vn.vnpt.domain.DuLieuBenhNhan;
import vn.vnpt.domain.VienPhiChiTietLanTt;
import vn.vnpt.domain.VienPhiLanTt;
import vn.vnpt.repository.*;
import vn.vnpt.service.constant.thanhtoan.TtLanThanhToanEnum;
import vn.vnpt.service.dto.LanKcbDto;
import vn.vnpt.service.dto.ThongTinVpTongHopDto;
import vn.vnpt.service.dto.VienPhiChiTietLanTtDto;
import vn.vnpt.service.dto.VienPhiLanTtDto;
import vn.vnpt.service.dto.bangke.BangKeTongTienDto;
import vn.vnpt.service.mapper.LanKcbDtoMapper;
import vn.vnpt.service.mapper.VienPhiLanTtMapper;
import vn.vnpt.web.rest.errors.BadRequestAlertException;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class VienPhiLanTtService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String ENTITY_NAME = "VienPhiLanTt";

    @Autowired
    VienPhiLanTtRepository vienPhiLanTtRepository;

    @Autowired
    VienPhiChiTietLanTtService vienPhiChiTietLanTtService;

    @Autowired
    VienPhiLanTamUngService vienPhiLanTamUngService;

    @Autowired
    BangKeServices bangKeServices;

    @Autowired
    DuLieuBenhNhanService duLieuBenhNhanService;

    @Autowired
    DichVuChiDinhService dichVuChiDinhService;

    @Autowired
    VienPhiLanTtMapper vienPhiLanTtMapper;

    @Autowired
    LanKcbDtoMapper lanKcbDtoMapper;

    private void checkValidCurrency(VienPhiLanTt lanTt) {
        BigDecimal soTienPhaiTt = lanTt.getSoTienPhaiTt();
        BigDecimal soTienMienGiam = lanTt.getSoTienMienGiam();
        BigDecimal soTienTruMienGiam = soTienPhaiTt.subtract(soTienMienGiam);
        BigDecimal soTienTamUng = lanTt.getSoTienTamUng();
        BigDecimal soTienThucThu = lanTt.getSoTienThucThu();
        BigDecimal soTienThoiLai = lanTt.getSoTienThoiLai();

        /* Kiểm tra số tiền phải thanh toán */
        BigDecimal total = lanTt.getListVienPhiChiTietLanTt()
            .stream()
            .map(VienPhiChiTietLanTt::getPhaiTt)
            .reduce(BigDecimal.valueOf(0), BigDecimal::add);
        if (total.compareTo(soTienPhaiTt) != 0)
            throw new BadRequestAlertException("Tổng tiền không hợp lệ !", ENTITY_NAME, "invalidMoney");

        /* Kiểm tra tổng tiền tạm ứng */
        BigDecimal soTienTamUngConLai = vienPhiLanTamUngService.getSoTienTamUngConLai(
            new LanKcbDto(lanTt.getSoVaoVien(), lanTt.getSoVaoVienDt(), lanTt.getLoaiKcb(), lanTt.getDvtt())
        );
        if (soTienTamUng.compareTo(soTienTamUngConLai) != 0)
            throw new BadRequestAlertException("Số tiền tạm ứng không hợp lệ !", ENTITY_NAME, "invalidMoney");

        /* Kiểm tra các cột số tiền
         *  Nếu soTienTruMienGiam >= soTienTamUng:
         *       soTienThucThu = soTienTruMienGiam - soTienTamUng
         *       soTienThoiLai = 0
         *  Nếu soTienTruMienGiam < soTienTamUng:
         *       soTienThucThu = 0
         *       soTienThoiLai = soTienTamUng - soTienTruMienGiam
         * */
        if (soTienTruMienGiam.compareTo(soTienTamUng) >= 0) {
            if (soTienThucThu.compareTo(soTienTruMienGiam.subtract(soTienTamUng)) != 0 || soTienThoiLai.compareTo(BigDecimal.ZERO) != 0)
                throw new BadRequestAlertException("Số tiền không hợp lệ !", ENTITY_NAME, "invalidMoney");
        }
        else {
            if (soTienThucThu.compareTo(BigDecimal.ZERO) != 0 || soTienThoiLai.compareTo(soTienTamUng.subtract(soTienTruMienGiam)) != 0)
                throw new BadRequestAlertException("Số tiền không hợp lệ !", ENTITY_NAME, "invalidMoney");
        }
    }

    private void checkValidDetail(VienPhiLanTt lanTt) {
        /* Kiểm tra các dịch vụ sắp thanh toán đã được thanh toán chưa */
        List<VienPhiChiTietLanTt> listDaThanhToan = vienPhiChiTietLanTtService.getDaThanhToan(new LanKcbDto(lanTt.getSoVaoVien(), lanTt.getSoVaoVienDt(), lanTt.getLoaiKcb(), lanTt.getDvtt()));

        lanTt.getListVienPhiChiTietLanTt().forEach(e -> {
            boolean inValid = listDaThanhToan.stream()
                .anyMatch(dvDaThanhToan -> dvDaThanhToan.getSoPhieuMaDv().equals(e.getSoPhieuMaDv()));
            if (inValid) {
                throw new BadRequestAlertException("Danh sách dịch vụ thanh toán không hợp lệ!", ENTITY_NAME, "invalidDetail");
            }
        });
    }

    private void checkSynchronization(VienPhiLanTt lanTt) {
        /*
        Do something to check ...
        */
    }

    private void checkValid(VienPhiLanTt lanTt) {
        checkValidCurrency(lanTt);
        checkValidDetail(lanTt);
        checkSynchronization(lanTt);
    }

    @Transactional
    public VienPhiLanTt createVienPhiLanTt(VienPhiLanTtDto vienPhiLanTtDto) {
        VienPhiLanTt vienPhiLanTt = vienPhiLanTtMapper.toEntity(vienPhiLanTtDto);
        LanKcbDto lanKcbDto = lanKcbDtoMapper.toDto(vienPhiLanTtDto);
        DuLieuBenhNhan duLieuBenhNhan = duLieuBenhNhanService.getDuLieuBenhNhan(lanKcbDto);
        List<DichVuChiDinh> dsDichVuChiDinh = duLieuBenhNhan.getDsDichVuChiDinh();

        this.checkValid(vienPhiLanTt);
        int sttLanTt = vienPhiLanTtRepository.countAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvtt(lanKcbDto.getSoVaoVien(), lanKcbDto.getSoVaoVienDt(), lanKcbDto.getLoaiKcb(), lanKcbDto.getDvtt()) + 1;
        vienPhiLanTt.setSttLanTt(duLieuBenhNhan.getMaLk() + "_" + sttLanTt);
        vienPhiLanTt.setTtLanTt(TtLanThanhToanEnum.DA_THANH_TOAN.getValue());
        vienPhiLanTtRepository.save(vienPhiLanTt);
        vienPhiChiTietLanTtService.saveAll(vienPhiLanTt.getListVienPhiChiTietLanTt(), vienPhiLanTt);
        List<String> dsSoPhieuMaDvTemp = vienPhiLanTtDto.getDsSoPhieuMaDv();
        dichVuChiDinhService.updateTtThanhToanDichVuChiDinh(dsDichVuChiDinh, dsSoPhieuMaDvTemp, 1);
        int soLanThanhToanConLai = vienPhiLanTtRepository.countAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndTtLanTt(lanKcbDto.getSoVaoVien(), lanKcbDto.getSoVaoVienDt(), lanKcbDto.getLoaiKcb(), lanKcbDto.getDvtt(), TtLanThanhToanEnum.DA_THANH_TOAN.getValue());
        duLieuBenhNhanService.updateSoLanThanhToanDuLieuBenhNhan(duLieuBenhNhan, soLanThanhToanConLai);
        vienPhiLanTamUngService.setDaSuDungTamUng(lanKcbDto, vienPhiLanTt.getSttLanTt());
        return vienPhiLanTt;
    }

    @Transactional
    public List<VienPhiLanTt> getListVienPhiLanTt(LanKcbDto lanKcbDto) {
        long soVaoVien = lanKcbDto.getSoVaoVien();
        long soVaoVienDt = lanKcbDto.getSoVaoVienDt();
        int loaiKcb = lanKcbDto.getLoaiKcb();
        String dvtt = lanKcbDto.getDvtt();
        return vienPhiLanTtRepository.findAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndTtLanTt(soVaoVien, soVaoVienDt, loaiKcb, dvtt, TtLanThanhToanEnum.DA_THANH_TOAN.getValue());
    }

    private void checkDeleteVienPhiLanTt(VienPhiLanTt vienPhiLanTt) {
        /*
        Do something to check contrains,
        if invalid, throws some Exception
        * */
    }

    @Transactional
    public void deleteVienPhiLanTt(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt, String sttLanTt) {
        VienPhiLanTt vienPhiLanTt = vienPhiLanTtRepository.findBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndSttLanTt(soVaoVien, soVaoVienDt, loaiKcb, dvtt, sttLanTt)
            .orElseThrow(NoSuchElementException::new);
        checkDeleteVienPhiLanTt(vienPhiLanTt);
        vienPhiLanTt.setTtLanTt(TtLanThanhToanEnum.DA_HUY_THANH_TOAN.getValue());
        vienPhiLanTtRepository.save(vienPhiLanTt);

        LanKcbDto lanKcbDto = new LanKcbDto(soVaoVien, soVaoVienDt, loaiKcb, dvtt);
        DuLieuBenhNhan duLieuBenhNhan = duLieuBenhNhanService.getDuLieuBenhNhan(lanKcbDto);
        List<DichVuChiDinh> dsDichVuChiDinh = duLieuBenhNhan.getDsDichVuChiDinh();
        List<String> dsSoPhieuMaDvTemp = vienPhiLanTt.getListVienPhiChiTietLanTt().stream()
            .map(VienPhiChiTietLanTt::getSoPhieuMaDv)
            .collect(Collectors.toList());
        dichVuChiDinhService.updateTtThanhToanDichVuChiDinh(dsDichVuChiDinh, dsSoPhieuMaDvTemp, 0);
        int soLanThanhToanConLai = vienPhiLanTtRepository.countAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndTtLanTt(soVaoVien, soVaoVienDt, loaiKcb, dvtt, TtLanThanhToanEnum.DA_THANH_TOAN.getValue());
        duLieuBenhNhanService.updateSoLanThanhToanDuLieuBenhNhan(duLieuBenhNhan, soLanThanhToanConLai);
        vienPhiLanTamUngService.setChuaSuDungTamUng(lanKcbDto, sttLanTt);
    }

    @Transactional
    public ThongTinVpTongHopDto ttVienPhiTongHop(LanKcbDto lanKcbDto){
        ThongTinVpTongHopDto result = new ThongTinVpTongHopDto();
        BangKeTongTienDto bangKeTongTienDto = bangKeServices.getTongTien(lanKcbDto);

        result.setThanhToan(vienPhiLanTtRepository.getTongSoTienThucThuLanTt(lanKcbDto.getSoVaoVien(), lanKcbDto.getSoVaoVienDt(), lanKcbDto.getLoaiKcb(), lanKcbDto.getDvtt()));
        result.setTamUng(vienPhiLanTamUngService.getTongSoTienDaTamUng(lanKcbDto));
        result.setTamUngConLai(vienPhiLanTamUngService.getSoTienTamUngConLai(lanKcbDto));
        result.setTongDaThu(result.getThanhToan().add(result.getTamUng()));
        result.setTongChiPhi(bangKeTongTienDto.getTongThanhTienBv());
        result.setTongBh(bangKeTongTienDto.getTongThanhTienBh());
        result.setBhChiTra(bangKeTongTienDto.getTongQuyBhyt());
        result.setCungChiTra(bangKeTongTienDto.getTongCungChiTra());
        result.setBnPhaiTra(bangKeTongTienDto.getTongNguoiBenh());
        result.setHoanUng(vienPhiLanTtRepository.getTongSoTienThoiLaiLanTt(lanKcbDto.getSoVaoVien(), lanKcbDto.getSoVaoVienDt(), lanKcbDto.getLoaiKcb(), lanKcbDto.getDvtt()));
        result.setConLai(
            vienPhiChiTietLanTtService.getChuaThanhToan(lanKcbDto)
            .parallelStream()
            .map(VienPhiChiTietLanTtDto::getPhaiTt)
            .reduce(BigDecimal.ZERO, BigDecimal::add)
        );
        return result;
    }
}
