package vn.vnpt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.DichVuChiDinh;
import vn.vnpt.domain.DuLieuBenhNhan;
import vn.vnpt.repository.DichVuChiDinhRepository;
import vn.vnpt.service.dto.DichVuChiDinhDto;
import vn.vnpt.service.mapper.DichVuChiDinhMapper;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author NguyenDucAnhKhoi
 */
@Service
public class DichVuChiDinhService {

    @Autowired
    DichVuChiDinhRepository dichVuChiDinhRepository;

    @Autowired
    DichVuChiDinhMapper dichVuChiDinhMapper;

    @Transactional
    public void initDsDichVuChiDinh(List<DichVuChiDinhDto> dsDichVuChiDinhDto, DuLieuBenhNhan duLieuBenhNhan) {
        AtomicInteger i = new AtomicInteger(0);
        List<DichVuChiDinh> dsDichVuChiDnh = dsDichVuChiDinhDto
            .stream()
            .map(e -> {
                DichVuChiDinh dichVuChiDinh = dichVuChiDinhMapper.toEntity(e);
                dichVuChiDinh.setDuLieuBenhNhan(duLieuBenhNhan);
                dichVuChiDinh.setTransactionLog(duLieuBenhNhan.getTransactionLog());
                dichVuChiDinh.setMaBn(duLieuBenhNhan.getMaBn());
                dichVuChiDinh.setSoPhieuMaDv(new Date().getTime() + "--" +  i.getAndIncrement());
                dichVuChiDinh.setTtThanhToan(0);
                return dichVuChiDinh;
            })
            .collect(Collectors.toList());
        dichVuChiDinhRepository.saveAll(dsDichVuChiDnh);
    }

    @Transactional
    public void afterUpdateDsDichVuChiDinh(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt) {
        dichVuChiDinhRepository.updateSttCongKham(soVaoVien, soVaoVienDt, loaiKcb, dvtt);
        dichVuChiDinhRepository.updateSttStent(soVaoVien, soVaoVienDt, loaiKcb, dvtt);
    }

    @Transactional
    public void updateTtThanhToanDichVuChiDinh(List<DichVuChiDinh> dsDichVuChiDinh, List<String> dsSoPhieuMaDv, int ttThanhToan) {
        dichVuChiDinhRepository.saveAll(
            dsDichVuChiDinh.stream()
                .filter(e -> dsSoPhieuMaDv.stream().anyMatch(soPhieuMaDv -> soPhieuMaDv.equals(e.getSoPhieuMaDv())) )
                .peek(e -> e.setTtThanhToan(ttThanhToan))
                .collect(Collectors.toList())
        );
    }

}
