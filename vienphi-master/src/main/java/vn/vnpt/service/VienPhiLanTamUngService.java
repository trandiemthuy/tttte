package vn.vnpt.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.DuLieuBenhNhan;
import vn.vnpt.domain.VienPhiLanTamUng;
import vn.vnpt.repository.DuLieuBenhNhanRepository;
import vn.vnpt.repository.VienPhiLanTamUngRepository;
import vn.vnpt.service.constant.thanhtoan.TtLanTamUngEnum;
import vn.vnpt.service.constant.thanhtoan.TtSuDungLanTamUngEnum;
import vn.vnpt.service.dto.LanKcbDto;
import vn.vnpt.service.dto.VienPhiLanTamUngDto;
import vn.vnpt.service.mapper.LanKcbDtoMapper;
import vn.vnpt.service.mapper.VienPhiLanTamUngMapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class VienPhiLanTamUngService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String ENTITY_NAME = "VienPhiLanTamUng";

    @Autowired
    VienPhiLanTamUngMapper vienPhiLanTamUngMapper;

    @Autowired
    LanKcbDtoMapper lanKcbDtoMapper;

    @Autowired
    DuLieuBenhNhanService duLieuBenhNhanService;

    @Autowired
    VienPhiLanTamUngRepository vienPhiLanTamUngRepository;

    @Transactional
    public VienPhiLanTamUng createLanTamUng(VienPhiLanTamUngDto vienPhiLanTamUngDto) {
        VienPhiLanTamUng vienPhiLanTamUng = vienPhiLanTamUngMapper.toEntity(vienPhiLanTamUngDto);
        LanKcbDto lanKcbDto = lanKcbDtoMapper.toDto(vienPhiLanTamUngDto);

        DuLieuBenhNhan duLieuBenhNhan = duLieuBenhNhanService.getDuLieuBenhNhan(lanKcbDto);
        int sttLanTamUng = vienPhiLanTamUngRepository.countAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvtt(lanKcbDto.getSoVaoVien(), lanKcbDto.getSoVaoVienDt(), lanKcbDto.getLoaiKcb(), lanKcbDto.getDvtt()) + 1;

        vienPhiLanTamUng.setTtLanTamUng(TtLanTamUngEnum.DA_TAM_UNG.getValue());
        vienPhiLanTamUng.setSttLanTamUng(duLieuBenhNhan.getMaLk() + "_" + sttLanTamUng);
        vienPhiLanTamUng.setTtSuDung(0);
        vienPhiLanTamUngRepository.save(vienPhiLanTamUng);

        int soLanTamUngConLai = vienPhiLanTamUngRepository.countAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndTtLanTamUng(lanKcbDto.getSoVaoVien(), lanKcbDto.getSoVaoVienDt(), lanKcbDto.getLoaiKcb(), lanKcbDto.getDvtt(), TtLanTamUngEnum.DA_TAM_UNG.getValue());
        duLieuBenhNhanService.updateSoLanTamUngDuLieuBenhNhan(duLieuBenhNhan, soLanTamUngConLai);
        return vienPhiLanTamUng;
    }

    @Transactional
    public VienPhiLanTamUng getLanTamUng(long idLanTamUng) {
        return vienPhiLanTamUngRepository.findById(idLanTamUng)
            .orElseThrow(NoSuchElementException::new);
    }

    @Transactional
    public void deleteLanTamUng(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt, String sttLanTamUng) {
        VienPhiLanTamUng vienPhiLanTamUng = vienPhiLanTamUngRepository.findBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndSttLanTamUng(soVaoVien, soVaoVienDt, loaiKcb, dvtt, sttLanTamUng)
            .orElseThrow(NoSuchElementException::new);
        LanKcbDto lanKcbDto = new LanKcbDto(soVaoVien, soVaoVienDt, loaiKcb, dvtt);
        DuLieuBenhNhan duLieuBenhNhan = duLieuBenhNhanService.getDuLieuBenhNhan(lanKcbDto);

        if (vienPhiLanTamUng.getTtSuDung() == 0
            && vienPhiLanTamUng.getTtLanTamUng() == TtLanTamUngEnum.DA_TAM_UNG.getValue() ) {
            vienPhiLanTamUng.setTtLanTamUng(TtLanTamUngEnum.DA_HUY_TAM_UNG.getValue());
            vienPhiLanTamUngRepository.save(vienPhiLanTamUng);

            int soLanTamUngConLai = vienPhiLanTamUngRepository.countAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndTtLanTamUng(soVaoVien, soVaoVienDt, loaiKcb, dvtt, TtLanTamUngEnum.DA_TAM_UNG.getValue());
            duLieuBenhNhanService.updateSoLanTamUngDuLieuBenhNhan(duLieuBenhNhan, soLanTamUngConLai);
        }
        else {
            throw new IllegalStateException("Lần tạm ứng đã được sử dụng hoặc đã hủy");
        }
    }

    @Transactional
    public List<VienPhiLanTamUng> getListLanTamUng(LanKcbDto lanKcbDto) {
        return vienPhiLanTamUngRepository.findAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndTtLanTamUng(
            lanKcbDto.getSoVaoVien(),
            lanKcbDto.getSoVaoVienDt(),
            lanKcbDto.getLoaiKcb(),
            lanKcbDto.getDvtt(),
            TtLanTamUngEnum.DA_TAM_UNG.getValue()
        );
    }

    @Transactional
    public BigDecimal getTongSoTienDaTamUng(LanKcbDto lanKcbDto) {
        return vienPhiLanTamUngRepository.findAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndTtLanTamUng(
            lanKcbDto.getSoVaoVien(),
            lanKcbDto.getSoVaoVienDt(),
            lanKcbDto.getLoaiKcb(),
            lanKcbDto.getDvtt(),
            TtLanTamUngEnum.DA_TAM_UNG.getValue()
        ).parallelStream()
            .map(VienPhiLanTamUng::getSoTienTamUng)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Transactional
    public BigDecimal getSoTienTamUngConLai(LanKcbDto lanKcbDto) {
        return vienPhiLanTamUngRepository.findAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvttAndTtLanTamUngAndTtSuDung(
            lanKcbDto.getSoVaoVien(),
            lanKcbDto.getSoVaoVienDt(),
            lanKcbDto.getLoaiKcb(),
            lanKcbDto.getDvtt(),
            TtLanTamUngEnum.DA_TAM_UNG.getValue(),
            TtSuDungLanTamUngEnum.CHUA_SU_DUNG.getValue()
        ).parallelStream()
        .map(VienPhiLanTamUng::getSoTienTamUng)
        .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Transactional
    public void setDaSuDungTamUng(LanKcbDto lanKcbDto, String sttLanTt) {
        vienPhiLanTamUngRepository.setDaSuDungTamUng(lanKcbDto.getSoVaoVien(), lanKcbDto.getSoVaoVienDt(), lanKcbDto.getLoaiKcb(), lanKcbDto.getDvtt(), sttLanTt);
    }

    @Transactional
    public void setChuaSuDungTamUng(LanKcbDto lanKcbDto, String sttLanTt) {
        vienPhiLanTamUngRepository.setChuaSuDungTamUng(lanKcbDto.getSoVaoVien(), lanKcbDto.getSoVaoVienDt(), lanKcbDto.getLoaiKcb(), lanKcbDto.getDvtt(), sttLanTt);
    }

}
