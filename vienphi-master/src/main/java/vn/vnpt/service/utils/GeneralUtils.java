package vn.vnpt.service.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class GeneralUtils {

    public static Map<String, Object> convertToMap(Object object) {
        ObjectMapper mapper = new ObjectMapper();
       return mapper.convertValue(object, new TypeReference<Map<String, Object>>() {});
    }

}
