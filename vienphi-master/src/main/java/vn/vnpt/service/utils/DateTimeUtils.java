package vn.vnpt.service.utils;

import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class DateTimeUtils {

    public static final Locale LOCALE_VIETNAM = new Locale("vi", "VN");
    public static final String dayPrefix = "ngày";
    public static final String monthPrefix = "tháng";
    public static final String yearPrefix = "năm";
    public static final String hourPosfix = "giờ";
    public static final String minutePosfix = "phút";
    public static final String secondPosfix = "giây";

    public static final String VIEN_PHI_DATE_FORMAT = "dd/MM/yyyy";
    public static final String VIEN_PHI_TIME_FORMAT = "HH:mm:ss";
    public static final String B_4210_DATE_FORMAT = "yyyyMMdd";
    public static final String B_4210_DATE_TIME_FORMAT = "yyyyMMddHHmm";
    public static final String B_4210_TIME_FORMAT = "HHmm";

    public static Date getCurrentDate() {
        return Date.from(Instant.now());
    }

    public static String readDate(Date date) {
        String str = String.format("%s %s %s %s %s %s", dayPrefix, getDate(date), monthPrefix, getMonth(date), yearPrefix, getYear(date));
        return StringUtils.capitalize(str);
    }

    public static String readDateFull(Date date) {
        String str = String.format("%s, %s %s %s %s %s %s", getDayOfWeek(date), dayPrefix, getDate(date), monthPrefix, getMonth(date), yearPrefix, getYear(date));
        return StringUtils.capitalize(str);
    }

    public static String readHourMinute(String value) {
        String hour = value.split(":")[0];
        String minute = value.split(":")[1];
        return String.format("%s %s %s %s", hour, hourPosfix, minute, minutePosfix);
    }

    public static String getDayOfWeek(Date date) {
        final LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        return dayOfWeek.getDisplayName(TextStyle.FULL_STANDALONE, LOCALE_VIETNAM);
    }

    public static String getDate(Date date) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
    }

    public static String getMonth(Date date) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return String.valueOf(cal.get(Calendar.MONTH) + 1);
    }

    public static String getYear(Date date) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return String.valueOf(cal.get(Calendar.YEAR));
    }

    public static Date toDate(String value, String format) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            return simpleDateFormat.parse(value);
        }
        catch (ParseException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String toString(Date value, String format) {
        if (value == null) {
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(value);
    }

}
