package vn.vnpt.service.utils;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import vn.vnpt.service.constant.ReportMediaType;
import vn.vnpt.service.constant.ReportTypeEnum;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author NguyenDucAnhKhoi
 */
public final class ReportUtils {

    private static final Logger logger = LoggerFactory.getLogger(ReportUtils.class);

    private static void printInfo(String fileName, Map<String, Object> parameters) {
        logger.debug("\n");
        logger.debug("Report file name: " + fileName);
        parameters.forEach((key, value) -> {
            logger.debug("Param [" + key + "]: " + value);
        });
        logger.debug("End " + fileName);
        logger.debug("\n");
    }

    public static ResponseEntity<ByteArrayResource> responseEntityReport(String reportPath, Map<String, Object> parameters, List dataSource, ReportTypeEnum type) {
        try {
            ClassPathResource classPathResource = new ClassPathResource(reportPath);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(classPathResource.getInputStream());
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRBeanCollectionDataSource(dataSource));
            String filename = generateFileName(Objects.requireNonNull(classPathResource.getFilename()), type);
            printInfo(filename, parameters);
            return responseEntityReport(jasperPrint, filename, type);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static ResponseEntity<ByteArrayResource> responseEntityReport(String reportPath, Map<String, Object> parameters, Connection connection, ReportTypeEnum type) {
        try {
            ClassPathResource classPathResource = new ClassPathResource(reportPath);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(classPathResource.getInputStream());
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection);
            String filename = generateFileName(Objects.requireNonNull(classPathResource.getFilename()), type);
            printInfo(filename, parameters);
            return responseEntityReport(jasperPrint, filename, type);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private static String generateFileName(String fileName, ReportTypeEnum type) {
        String extension = ReportMediaType.getExtension(type);
        String name = fileName.substring(0, fileName.length() - 7) + "_" + DateTimeUtils.toString(DateTimeUtils.getCurrentDate(), "yyyyMMddHHmmss") + extension;
        logger.debug("exportType=" + extension);
        logger.debug("fileName=" + name);
        return name;
    }

    private static ResponseEntity<ByteArrayResource> responseEntityReport(JasperPrint jasperPrint, String filename, ReportTypeEnum type) throws JRException {
        ByteArrayResource body = printReport(jasperPrint, type);
        HttpHeaders httpHeaders = new HttpHeaders();
        ContentDisposition contentDisposition = ContentDisposition.builder(type == ReportTypeEnum.VIEW_PDF || type == ReportTypeEnum.VIEW_HTML ? "inline" : "attachment")
            .filename(filename)
            .build();
        httpHeaders.setContentDisposition(contentDisposition);
        httpHeaders.setContentType(ReportMediaType.getMediaType(type));
        return ResponseEntity
            .ok()
            .headers(httpHeaders)
            .body(body);
    };

    private static ByteArrayResource printReport(JasperPrint jasperPrint, ReportTypeEnum type) throws JRException {
        switch (type) {
            case VIEW_PDF:
            case PDF:
                return printPdfReport(jasperPrint);
            case XLS:
                return printToXlsReport(jasperPrint);
            case XLSX:
                return printToXlsxReport(jasperPrint);
            case RTF:
                return printToRtfReport(jasperPrint);
            case HTML:
            case VIEW_HTML:
                return printToHtmlReport(jasperPrint);
            default:
                throw new JRException("Unknown report format");
        }
    }

    private static ByteArrayResource printPdfReport(JasperPrint jasperPrint) throws JRException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArrayOutputStream));
        exporter.exportReport();
        return new ByteArrayResource(byteArrayOutputStream.toByteArray());
    }

    private static ByteArrayResource printToXlsReport(JasperPrint jasperPrint) throws JRException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        JRXlsExporter exporter = new JRXlsExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArrayOutputStream));
        exporter.exportReport();
        return new ByteArrayResource(byteArrayOutputStream.toByteArray());
    }

    private static ByteArrayResource printToXlsxReport(JasperPrint jasperPrint) throws JRException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        JRXlsxExporter exporter = new JRXlsxExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArrayOutputStream));
        exporter.exportReport();
        return new ByteArrayResource(byteArrayOutputStream.toByteArray());
    }

    private static ByteArrayResource printToRtfReport(JasperPrint jasperPrint) throws JRException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        JRRtfExporter exporter = new JRRtfExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleWriterExporterOutput(byteArrayOutputStream));
        exporter.exportReport();
        return new ByteArrayResource(byteArrayOutputStream.toByteArray());
    }

    private static ByteArrayResource printToHtmlReport(JasperPrint jasperPrint) throws JRException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        HtmlExporter exporter = new HtmlExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleHtmlExporterOutput(byteArrayOutputStream));
        exporter.exportReport();
        return new ByteArrayResource(byteArrayOutputStream.toByteArray());
    }

}
