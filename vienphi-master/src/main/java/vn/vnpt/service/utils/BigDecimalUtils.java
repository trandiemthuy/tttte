package vn.vnpt.service.utils;

import vn.vnpt.service.constant.VienPhiConstants;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public final class BigDecimalUtils {

    public static BigDecimal round(BigDecimal value) {
        int scale = VienPhiConstants.bigDecimalScale;
        return BigDecimalUtils.round(value, scale);
    }

    private static BigDecimal round(BigDecimal value, int scale) {
        return value.setScale(scale, RoundingMode.HALF_UP);
    }

    public static BigDecimal percent(BigDecimal value) {
        return nvl(value, BigDecimal.ZERO).divide(BigDecimal.valueOf(100), MathContext.DECIMAL64);
    }

    public static BigDecimal nvl(BigDecimal value, BigDecimal defaultValue) {
        return value == null ? defaultValue : value;
    }

}
