package vn.vnpt.service;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.BangKe;
import vn.vnpt.domain.DichVuChiDinh;
import vn.vnpt.domain.DuLieuBenhNhan;
import vn.vnpt.repository.BangKeRepository;
import vn.vnpt.service.constant.VienPhiConstants;
import vn.vnpt.service.dto.LanKcbDto;
import vn.vnpt.service.dto.bangke.*;
import vn.vnpt.service.mapper.BangKeJasperParamettersMapper;
import vn.vnpt.service.mapper.BangKeMapper;
import vn.vnpt.service.mapper.LanKcbDtoMapper;
import vn.vnpt.service.utils.BigDecimalUtils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BangKeServices {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String ENTITY_NAME = "BangKe";

    @Autowired
    BangKeRepository bangKeRepository;

    @Autowired
    TransactionLogService transactionLogService;

    @Autowired
    DuLieuBenhNhanService duLieuBenhNhanService;

    @Autowired
    DichVuChiDinhService dichVuChiDinhService;

    @Autowired
    BangKeJasperParamettersMapper bangKeJasperParamettersMapper;

    @Autowired
    BangKeMapper bangKeMapper;

    @Autowired
    LanKcbDtoMapper lanKcbDtoMapper;

    private boolean isVatTuChungGoi(BangKe bangKe) {
        return bangKe.getVatTuChungGoi() != null && bangKe.getVatTuChungGoi() == 1;
    }

    /* Tính cột 10 11 12 13 trên bảng kê 6556 */
    private BangKe tinhNguonThanhToan(BangKe bangKe) {
        BigDecimal mucHuong = bangKe.getMucHuong();
        BigDecimal thanhTienBh = bangKe.getThanhTienBh();
        BigDecimal thanhTienBv = bangKe.getThanhTienBv();

        BigDecimal quyBhyt = BigDecimalUtils.round(
            thanhTienBh.multiply(BigDecimalUtils.percent(mucHuong))
        );
        BigDecimal cungChiTra = thanhTienBh.subtract(quyBhyt);
        BigDecimal khac = BigDecimal.valueOf(0);
        BigDecimal nguoiBenh = thanhTienBv.subtract(thanhTienBh);

        bangKe.setQuyBhyt(quyBhyt);
        bangKe.setCungChiTra(cungChiTra);
        bangKe.setKhac(khac);
        bangKe.setNguoiBenh(nguoiBenh);

        return bangKe;
    }

    /* Tính cột 7 9 trên bảng kê 6556 */
    private BangKe tinhThanhTien(BangKe bangKe) {
        BigDecimal soLuong = bangKe.getSoLuong();
        BigDecimal donGiaBv = bangKe.getDonGiaBv();
        BigDecimal donGiaBh = bangKe.getDonGiaBh();
        BigDecimal tyLeThanhToanTheoDv = bangKe.getTyLeThanhToanTheoDv();
        BigDecimal tyLeThanhToanBhyt = bangKe.getTyLeThanhToanBhyt();

        BigDecimal thanhTienBv = BigDecimalUtils.round(
            soLuong
                .multiply(donGiaBv)
                .multiply(BigDecimalUtils.percent(tyLeThanhToanTheoDv))
        );
        BigDecimal thanhTienBh = BigDecimalUtils.round(
            soLuong
                .multiply(donGiaBh)
                .multiply(BigDecimalUtils.percent(tyLeThanhToanTheoDv))
                .multiply(BigDecimalUtils.percent(tyLeThanhToanBhyt))
        );
        bangKe.setThanhTienBv(thanhTienBv);
        bangKe.setThanhTienBh(thanhTienBh);

        return isVatTuChungGoi(bangKe) ? bangKe : tinhNguonThanhToan(bangKe);
    }

    private BangKe tinhNguonThanhToanVtytThuocGoi(BangKe vtyt, BigDecimal k) {
        BigDecimal mucHuong = vtyt.getMucHuong();
        BigDecimal thanhTienBh = vtyt.getThanhTienBh();
        BigDecimal thanhTienBv = vtyt.getThanhTienBv();

        BigDecimal quyBhyt = BigDecimalUtils.round(
            thanhTienBh.multiply(k)
                .multiply(BigDecimalUtils.percent(mucHuong))
        );
        BigDecimal cungChiTra = BigDecimalUtils.round(
            thanhTienBh.multiply(k).subtract(quyBhyt)
        );
        BigDecimal khac = BigDecimal.valueOf(0);
        BigDecimal nguoiBenh = BigDecimalUtils.round(
            thanhTienBv.subtract(thanhTienBh.multiply(k))
        );

        vtyt.setQuyBhyt(quyBhyt);
        vtyt.setCungChiTra(cungChiTra);
        vtyt.setKhac(khac);
        vtyt.setNguoiBenh(nguoiBenh);

        return vtyt;
    }

    private BangKe tinhNguonThanhToanVtytCuoiThuocGoi(BangKe vtytCuoi, BigDecimal tongQuyBhytVtytDau, BigDecimal tongCungChiTraVtytDau, BigDecimal tien45tlcs) {
        BigDecimal mucHuong = vtytCuoi.getMucHuong();
        BigDecimal thanhTienBh = vtytCuoi.getThanhTienBh();
        BigDecimal thanhTienBv = vtytCuoi.getThanhTienBv();

        BigDecimal quyBhyt = BigDecimalUtils.round(
            tien45tlcs
                .multiply(BigDecimalUtils.percent(mucHuong))
                .subtract(tongQuyBhytVtytDau)
        );
        BigDecimal cungChiTra = BigDecimalUtils.round(
            tien45tlcs
                .subtract(tongQuyBhytVtytDau.add(quyBhyt))
                .subtract(tongCungChiTraVtytDau)
        );
        BigDecimal khac = BigDecimal.valueOf(0);

        BigDecimal nguoiBenh = BigDecimalUtils.round(
            thanhTienBv.subtract(quyBhyt).subtract(cungChiTra).subtract(khac)
        );

        vtytCuoi.setQuyBhyt(quyBhyt);
        vtytCuoi.setCungChiTra(cungChiTra);
        vtytCuoi.setKhac(khac);
        vtytCuoi.setNguoiBenh(nguoiBenh);

        return vtytCuoi;
    }

    private BangKe tinhNguonThanhToanVatTuStent2(BangKe vatTuStent2) {
        BigDecimal mucHuong = vatTuStent2.getMucHuong();
        BigDecimal thanhTienBh = vatTuStent2.getThanhTienBh();
        BigDecimal thanhTienBv = vatTuStent2.getThanhTienBv();

        BigDecimal quyBhyt = BigDecimalUtils.round(
            thanhTienBh.multiply(BigDecimalUtils.percent(mucHuong))
        );
        BigDecimal cungChiTra = BigDecimal.valueOf(0);
        BigDecimal khac = BigDecimal.valueOf(0);
        BigDecimal nguoiBenh = thanhTienBv.subtract(quyBhyt);

        vatTuStent2.setQuyBhyt(quyBhyt);
        vatTuStent2.setCungChiTra(cungChiTra);
        vatTuStent2.setKhac(khac);
        vatTuStent2.setNguoiBenh(nguoiBenh);

        return vatTuStent2;
    }

    private List<BangKe> tinhThanhTienGoiVtyt(List<BangKe> goiVtyt, BigDecimal tien45tlcs) {
        goiVtyt = goiVtyt.stream()
            .sorted(Comparator.comparing(BangKe::getDonGiaBh).reversed())
            .collect(Collectors.toList());

        BigDecimal tongTienBhGoiVtyt = goiVtyt
            .stream()
            .filter(e -> e.getSttStent() == null || e.getSttStent() < 2)
            .map(BangKe::getThanhTienBh)
            .reduce(BigDecimal.ZERO, BigDecimal::add);

        List<BangKe> goiVtytTemp = new ArrayList<>();

        /* Nếu tổng tiền BH gói vtyt < 45 tháng lương cơ sở -> tính như thông thường */
        if (tongTienBhGoiVtyt.compareTo(tien45tlcs) < 0) {
            goiVtytTemp.addAll(
                goiVtyt.stream()
                .map(this::tinhNguonThanhToan)
                .collect(Collectors.toList())
            );
        }
        /* Nếu tổng tiền BH gói vtyt >= 45 tháng luong cơ sở */
        else {
            BigDecimal k = tien45tlcs.divide(tongTienBhGoiVtyt, MathContext.DECIMAL64);
            BigDecimal tongQuyBhytVtytDauTemp = BigDecimal.ZERO;
            BigDecimal tongCungChiTraVtytDauTemp = BigDecimal.ZERO;
            List<BangKe> dsVtytDauTemp = new ArrayList<>();

            for (int i=0; i<goiVtyt.size()-1; i++) {
                BangKe e = goiVtyt.get(i);
                if (e.getSttStent() == null || e.getSttStent() < 2) {
                    BangKe vtytDauTemp = this.tinhNguonThanhToanVtytThuocGoi(e, k);
                    tongQuyBhytVtytDauTemp = tongQuyBhytVtytDauTemp.add(vtytDauTemp.getQuyBhyt());
                    tongCungChiTraVtytDauTemp = tongCungChiTraVtytDauTemp.add(vtytDauTemp.getCungChiTra());
                    dsVtytDauTemp.add(vtytDauTemp);
                }
            }
            BangKe vtytCuoi = this.tinhNguonThanhToanVtytCuoiThuocGoi(
                goiVtyt.get(goiVtyt.size() - 1),
                tongQuyBhytVtytDauTemp,
                tongCungChiTraVtytDauTemp,
                tien45tlcs
            );

            goiVtytTemp.addAll(dsVtytDauTemp);
            goiVtytTemp.add(vtytCuoi);

            /* Vật tư Stent > 1 */
            goiVtytTemp.addAll(
                goiVtyt.stream()
                .filter(e -> e.getSttStent() != null && e.getSttStent() > 1)
                .map(e -> e.getSttStent() == 2 ? this.tinhNguonThanhToanVatTuStent2(e) : this.tinhNguonThanhToan(e))
                .collect(Collectors.toList())
            );
        }

        return goiVtytTemp;
    }

    /* Hàm tạo bảng kê */
    @Transactional
    public void createBangKe(LanKcbDto lanKcbDto) {

        logger.debug("Create BangKe ...");
        logger.debug("Getting data KCB ...");
        DuLieuBenhNhan duLieuBenhNhan = duLieuBenhNhanService.getDuLieuBenhNhan(lanKcbDto);

        if (duLieuBenhNhan == null) {
            logger.error("No data found !");
            throw new NoSuchElementException("No data found !");
        }
        else {
            createBangKe(duLieuBenhNhan);
        }
    }

    @Transactional
    public void createBangKe(DuLieuBenhNhan duLieuBenhNhan) {
        logger.debug("Begin caculate ...");
        // Xoa du lieu cu
        this.deleteBangKe(lanKcbDtoMapper.toDto(duLieuBenhNhan));

        boolean coBhyt = duLieuBenhNhan.getCoBhyt() == 1;
        List<DichVuChiDinh> dsDichVuChiDinh = duLieuBenhNhan.getDsDichVuChiDinh();
        List<BangKe> result = new ArrayList<>();

        int soLuongGoiVtyt = !coBhyt ? 0 :
            dsDichVuChiDinh.parallelStream()
                .map(e -> e.getSttGoiVatTu() == null ? 0 : e.getSttGoiVatTu())
                .max(Comparator.comparingInt(e -> e))
                .orElse(0);
        Map<String, List<BangKe>> dsGoiVtyt = new HashMap<>();
        for (int i=1; i<=soLuongGoiVtyt; i++) {
            String key = "GOI_VTYT_" + i;
            dsGoiVtyt.put(key, new ArrayList<>());
        }

        BigDecimal tongChiPhiDieuTri = dsDichVuChiDinh
            .parallelStream()
            .map(e -> e.getDonGia().multiply(e.getSoLuong()))
            .reduce(BigDecimal.ZERO, BigDecimal::add);
        boolean vuot15phanTramLuongCs =
            tongChiPhiDieuTri.compareTo(VienPhiConstants.tienThangLuongCoSo.multiply(BigDecimal.valueOf(15))) >= 0;
        BigDecimal tien45tlcs = VienPhiConstants.tienThangLuongCoSo.multiply(BigDecimal.valueOf(45));

        for (DichVuChiDinh e : dsDichVuChiDinh) {
            BangKe bangKe = bangKeMapper.initBangKe(duLieuBenhNhan, e, vuot15phanTramLuongCs);
            /* Nếu là vật tư thuộc gói */
            if (soLuongGoiVtyt > 0 && isVatTuChungGoi(bangKe)) {
                String key = "GOI_VTYT_" + bangKe.getSttGoiVatTu();
                List<BangKe> goiVtytTemp = dsGoiVtyt.get(key);
                goiVtytTemp.add(this.tinhThanhTien(bangKe));
                dsGoiVtyt.put(key, goiVtytTemp);
            }
            /* Dịch vụ, vật tư thông thường */
            else {
                result.add(this.tinhThanhTien(bangKe));
            }
        }

        for (int i=1; i<=soLuongGoiVtyt; i++) {
            String key = "GOI_VTYT_" + i;
            List<BangKe> goiVtytTemp = dsGoiVtyt.get(key);
            result.addAll(this.tinhThanhTienGoiVtyt(goiVtytTemp, tien45tlcs));
        }

        logger.debug("Created BangKe, saving to database ...");
        bangKeRepository.saveAll(result);
    }

    /* Xóa dữ liệu bảng kê */
    @Transactional
    public void deleteBangKe(LanKcbDto lanKcbDto) {
        long soVaoVien = lanKcbDto.getSoVaoVien();
        long soVaoVienDt = lanKcbDto.getSoVaoVienDt();
        int loaiKcb = lanKcbDto.getLoaiKcb();
        String dvtt = lanKcbDto.getDvtt();
        bangKeRepository.deleteAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvtt(soVaoVien, soVaoVienDt, loaiKcb, dvtt);
    }

    /* Hàm tạo bảng kê hàng loạt */
    @Transactional
    public int createBangKeHangLoat(TaoBangKeHangLoatDto taoBangKeHangLoatDto) {
        Date tuNgay = taoBangKeHangLoatDto.getTuNgay();
        Date denNgay = taoBangKeHangLoatDto.getDenNgay();
        String dvtt = taoBangKeHangLoatDto.getDvtt();
        int loaiKcb = taoBangKeHangLoatDto.getLoaiKcb();
        List<DuLieuBenhNhan> dsBenhNhan = duLieuBenhNhanService.dsDuLieuBenhNhanHoanTatKham(tuNgay, denNgay, loaiKcb, dvtt);
        for (DuLieuBenhNhan benhNhan : dsBenhNhan) {
            this.createBangKe(benhNhan);
        };
        return dsBenhNhan.size();
    }

    @Transactional
    public List<BangKe> getBangKe(LanKcbDto lanKcbDto) {
        long soVaoVien = lanKcbDto.getSoVaoVien();
        long soVaoVienDt = lanKcbDto.getSoVaoVienDt();
        int loaiKcb = lanKcbDto.getLoaiKcb();
        String dvtt = lanKcbDto.getDvtt();
        return bangKeRepository
            .findAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvtt(soVaoVien, soVaoVienDt, loaiKcb, dvtt);
    }

    @Transactional
    public List<BangKeDto> getBangKeDto(LanKcbDto lanKcbDto) {
        List<BangKeDto> temp = this.getBangKe(lanKcbDto)
            .parallelStream()
            .map(e -> bangKeMapper.toDto(e))
            .collect(Collectors.toList());
        BangKeTongTienNhomDto bangKeTongTienNhomDto = new BangKeTongTienNhomDto(temp);
        return temp.parallelStream()
            .map(e -> bangKeMapper.setTongTienNhom(e, bangKeTongTienNhomDto))
            .collect(Collectors.toList());
    }

    public BangKeTongTienDto getTongTien(LanKcbDto lanKcbDto) {
        long soVaoVien = lanKcbDto.getSoVaoVien();
        long soVaoVienDt = lanKcbDto.getSoVaoVienDt();
        int loaiKcb = lanKcbDto.getLoaiKcb();
        String dvtt = lanKcbDto.getDvtt();
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(bangKeRepository.getTongTien(soVaoVien, soVaoVienDt, loaiKcb, dvtt), BangKeTongTienDto.class);
    }

    /* Hàm lấy params in bảng kê */
    @Transactional
    public BangKeJasperParametters getBangKeJasperParametters(long soVaoVien, long soVaoVienDt, int loaiKcb, String dvtt) {
        DuLieuBenhNhan duLieuBenhNhan = duLieuBenhNhanService.getDuLieuBenhNhan(new LanKcbDto(soVaoVien, soVaoVienDt, loaiKcb, dvtt));
        duLieuBenhNhanService.updateInBangKe(duLieuBenhNhan);
        return bangKeJasperParamettersMapper.toParams(duLieuBenhNhan);
    }
}
