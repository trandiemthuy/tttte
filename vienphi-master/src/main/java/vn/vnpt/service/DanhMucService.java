package vn.vnpt.service;

import org.springframework.stereotype.Service;
import vn.vnpt.service.dto.KhoaDto;
import vn.vnpt.service.dto.NhanVienDto;
import vn.vnpt.service.dto.UserDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author NguyenDucAnhKhoi
 */
@Service
public class DanhMucService {

    public List<KhoaDto> dsKhoa(String dvtt) {
        List<KhoaDto> dsKhoa = new ArrayList<>();
        dsKhoa.add(0, new KhoaDto("-1", "Tất cả"));
        return dsKhoa;
    }

    public List<NhanVienDto> dsNhanVien(String dvtt) {
        List<NhanVienDto> dsNhanVien = new ArrayList<>();
        dsNhanVien.add(new NhanVienDto(-1, "Tất cả"));
        return dsNhanVien;
    }

}
