package vn.vnpt.service.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author NguyenDucAnhKhoi
 */
@Getter
@AllArgsConstructor
public enum TrangThaiQuyenBienLaiEnum {
    HOAT_DONG(1),
    VO_HIEU(2);

    private final int value;
}
