package vn.vnpt.service.constant.bangke;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.lang.Nullable;
import vn.vnpt.service.constant.MaNhom4210Enum;

@AllArgsConstructor
public enum BangKeTenNhomEnum {

    KHAM_BENH("1. Khám bệnh:"),
    NGAY_GIUONG("2. Ngày giường:"),
    XET_NGHIEM("3. Xét nghiệm:"),
    CHAN_DOAN_HINH_ANH("4. Chẩn đoán hình ảnh:"),
    THAM_DO_CHUC_NANG("5. Thăm dò chức năng:"),
    THU_THUAT_PHAU_THUAT("6. Thủ thuật, phẩu thuật:"),
    MAU("7. Máu, chế phẩm máu, vận chuyển máu: "),
    THUOC("8. Thuốc, dịch truyền:"),
    VAT_TU_Y_TE("9. Vật tư y tế:"),
    GOI_VTYT("10. Gói vật tư y tế:"),
    VAN_CHUYEN("11. Vận chuyển người bệnh"),
    DV_KHAC("12. Dịch vụ khác");

    @Getter
    private final String value;

}
