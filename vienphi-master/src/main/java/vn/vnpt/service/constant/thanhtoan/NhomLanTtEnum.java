package vn.vnpt.service.constant.thanhtoan;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author NguyenDucAnhKhoi
 */
@AllArgsConstructor
@Getter
public enum NhomLanTtEnum {
    CONGKHAM("CONGKHAM"),
    XETNGHIEM("XETNGHIEM"),
    CHANDOANHINHANH("CHANDOANHINHANH"),
    THAMDOCHUCNANG("THAMDOCHUCNANG"),
    THUTHUATPHAUTHUAT("THUTHUATPHAUTHUAT"),
    THUOC("THUOC")
    ;

    private final String value;
}
