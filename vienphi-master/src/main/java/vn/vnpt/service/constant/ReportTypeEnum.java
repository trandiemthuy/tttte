package vn.vnpt.service.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
public enum ReportTypeEnum {

    VIEW_PDF(-1),
    PDF(0),
    XLS(1),
    XLSX(2),
    RTF(3),
    HTML(4),
    VIEW_HTML(5)
    ;

    @Getter
    private final int value;

    public static ReportTypeEnum valueOf(int value) {
        return Arrays.stream(values())
            .filter(reportTypeEnum -> reportTypeEnum.value == value)
            .findFirst()
            .orElse(null);
    }

}
