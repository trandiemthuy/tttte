package vn.vnpt.service.constant.thanhtoan;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author NguyenDucAnhKhoi
 */
@AllArgsConstructor
@Getter
public enum TtLanThanhToanEnum {
    DA_THANH_TOAN(1),
    DA_HUY_THANH_TOAN(2)
    ;
    private final int value;
}
