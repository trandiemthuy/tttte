package vn.vnpt.service.constant.thanhtoan;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author NguyenDucAnhKhoi
 */
@AllArgsConstructor
@Getter
public enum HinhThucTtEnum {
    TIEN_MAT(1)
    ;
    private final int value;
}
