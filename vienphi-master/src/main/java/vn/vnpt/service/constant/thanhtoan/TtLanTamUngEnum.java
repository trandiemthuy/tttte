package vn.vnpt.service.constant.thanhtoan;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum TtLanTamUngEnum {

    DA_TAM_UNG(1),
    DA_HUY_TAM_UNG(2),
    DA_CHUYEN_VAO_NOI_TRU(3);

    @Getter
    private final int value;

}
