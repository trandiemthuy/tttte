package vn.vnpt.service.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
public enum MaLyDoVvienEnum {

    DUNG_TUYEN(1),
    CAP_CUU(2),
    TRAI_TUYEN(3),
    THONG_TUYEN(4);

    @Getter
    private final int value;

    public static MaLyDoVvienEnum valueOf(int value) {
        return Arrays.stream(values())
            .filter(maLyDoVvienEnum -> maLyDoVvienEnum.value == value)
            .findFirst()
            .orElse(null);
    }

}
