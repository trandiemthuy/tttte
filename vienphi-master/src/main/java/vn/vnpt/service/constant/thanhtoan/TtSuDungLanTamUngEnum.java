package vn.vnpt.service.constant.thanhtoan;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author NguyenDucAnhKhoi
 */
@AllArgsConstructor
@Getter
public enum TtSuDungLanTamUngEnum {
    CHUA_SU_DUNG(0),
    DA_SU_DUNG(1)
    ;
    private final int value;
}
