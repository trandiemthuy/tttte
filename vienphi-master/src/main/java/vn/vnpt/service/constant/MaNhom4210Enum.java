package vn.vnpt.service.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
public enum MaNhom4210Enum {
    XET_NGHIEM(1),
    CHAN_DOAN_HINH_ANH(2),
    THAM_DO_CHUC_NANG(3),
    THUOC_TRONG_DANH_MUC(4),
    THUOC_NGOAI_DANH_MUC(5),
    THUOC_TT_THEO_TY_LE(6),
    MAU(7),
    THU_THUAT_PHAU_THUAT(8),
    DVKT_TT_THEO_TY_LE(9),
    VTYT_TRONG_DANH_MUC(10),
    VTYT_TT_THEO_TY_LE(11),
    VAN_CHUYEN(12),
    KHAM_BENH(13),
    NGAY_GIUONG_BAN_NGAY(14),
    NGAY_GIUONG_NOU_TRU_LUU(15)
    ;

    @Getter
    private final int value;

    public static MaNhom4210Enum valueOf(int value) {
        return Arrays.stream(values())
            .filter(maNhom4210Enum -> maNhom4210Enum.value == value)
            .findFirst()
            .orElse(null);
    }

}
