package vn.vnpt.service.constant;

import java.math.BigDecimal;

public final class VienPhiConstants {
    public static final BigDecimal tienThangLuongCoSo = BigDecimal.valueOf(1490000);
    public static int bigDecimalScale = 2;
}
