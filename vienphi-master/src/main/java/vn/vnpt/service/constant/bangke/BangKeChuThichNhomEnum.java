package vn.vnpt.service.constant.bangke;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum BangKeChuThichNhomEnum {

    NGAY_GIUONG_LUU("<style isBold='false' isItalic='true'>Áp dụng đối với Phòng khám đa khoa khu vực và Trạm y tế tuyến xã</style>"),
    VTYT("<style isBold='false' isItalic='true'>Vật tư y tế chưa bao gồm với dịch vụ kỹ thuật nào, Ví dụ: Bơm cho ăn 50ml, dây truyền dịch...</style>"),
    GOI_VTYT("<style isBold='false' isItalic='true'>(Các vật tư y tế đi kèm trong một lần thực hiện dịch vụ kỹ thuật, không ghi các vật tư y tế đã tính kết cấu trong giá dịch vụ kỹ thuật đó)</style>")
    ;

    @Getter
    private final String value;
}
