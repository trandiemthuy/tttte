package vn.vnpt.service.constant.bangke;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum BangKeTenNhomConEnum {

    /* 2. Ngày giường */
    NGAY_GIUONG_BAN_NGAY("2.1. Ngày giường điều trị ban ngày: "),
    NGAY_GIUONG_NOI_TRU("2.2. Ngày giường điều trị nội trú: "),
    NGAY_GIUONG_LUU("2.3. Ngày giường lưu: "),

    /*10. Gói vật tư y tế */
    GOI_VTYT("10.%d. Gói vật tư y tế %d: ")
    ;

    @Getter
    private final String value;

}
