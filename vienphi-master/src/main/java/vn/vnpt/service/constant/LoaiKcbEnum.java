package vn.vnpt.service.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
public enum LoaiKcbEnum {
    NGOAI_TRU(1),
    BANT(2),
    NOI_TRU(3);

    @Getter
    private final int value;

    public static LoaiKcbEnum valueOf(int value) {
        return Arrays.stream(values())
            .filter(loaiKcbEnum -> loaiKcbEnum.value == value)
            .findFirst()
            .orElse(null);
    }

}
