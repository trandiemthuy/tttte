package vn.vnpt.service.constant;

import org.springframework.http.MediaType;
import org.springframework.util.MimeType;

/**
 * @author NguyenDucAnhKhoi
 */
public class ReportMediaType {

    private static final MediaType PDF = MediaType.APPLICATION_PDF;
    private static final MediaType XLS = MediaType.asMediaType(MimeType.valueOf("application/vnd.ms-excel"));
    private static final MediaType XLSX = MediaType.asMediaType(MimeType.valueOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    private static final MediaType RTF = MediaType.asMediaType(MimeType.valueOf("application/rtf"));
    private static final MediaType HTML = MediaType.TEXT_HTML;

    public static MediaType getMediaType(ReportTypeEnum type) {
        switch (type) {
            case VIEW_PDF:
            case PDF:
                return PDF;
            case XLS:
                return XLS;
            case XLSX:
                return XLSX;
            case RTF:
                return RTF;
            case HTML:
            case VIEW_HTML:
                return HTML;
            default:
                return null;
        }
    }

    public static String getExtension(ReportTypeEnum type) {
        switch (type) {
            case VIEW_PDF:
            case PDF:
                return ".pdf";
            case XLS:
                return ".xls";
            case XLSX:
                return ".xlsx";
            case RTF:
                return ".rtf";
            case HTML:
            case VIEW_HTML:
                return ".html";
            default:
                return null;
        }
    }

}
