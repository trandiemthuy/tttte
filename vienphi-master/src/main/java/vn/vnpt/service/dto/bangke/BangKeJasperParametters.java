package vn.vnpt.service.dto.bangke;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @author NguyenDucAnhKhoi
 */
@NoArgsConstructor
@Getter
@Setter
public class BangKeJasperParametters {

    private String soYteNganh;
    private String tenCskcb;
    private String tenKhoa;
    private String maKhoaBc;
    private String maSoNguoiBenh;
    private String soKhamBenh;
    private String maLoaiKcb;
    private String tieuDeBangKe;

    private String hoTenNguoiBenh;
    private String ngayThangNamSinh;
    private String gioiTinh;
    private String diaChi;
    private String maKhuVuc;
    private String maThe12;
    private String maThe3;
    private String maThe45;
    private String maThe615;
    private String gtTheTu;
    private String gtTheDen;
    private String tenDkbd;
    private String maDkbd;
    private String gioPhutDenKham;
    private String ngayDenKham;
    private String gioPhutDtTu;
    private String ngayDtTu;
    private String gioPhutKt;
    private String ngayKt;
    private String tinhTrangRaVien;
    private String capCuu;
    private String dungTuyen;
    private String thongTuyen;
    private String traiTuyen;
    private String maNoiChuyenDi;
    private String maNoiChuyenDen;
    private String chanDoanXacDinh;
    private String maBenh;
    private String benhKemTheo;
    private String benhKemTheo2;
    private String maBenhKemTheo;
    private String thoiDiem5NamLienTuc;
    private String mienCungCtTu;

    private String tongChiPhiBangChu;
    private String quyBhytBangChu;
    private String nguoiBenhTraBangChu;
    private String cungChiTraBangChu;
    private String nguoiBenhTraKhacBangChu;
    private String nguonKhacBangChu;

    private String ngayLapBangKe;
    private String nguoiLapBangKe;
    private String keToanVienPhi;
}
