package vn.vnpt.service.dto.bangke;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author NguyenDucAnhKhoi
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class BangKeTongTienDto {

    private BigDecimal tongThanhTienBv;
    private BigDecimal tongThanhTienBh;
    private BigDecimal tongQuyBhyt;
    private BigDecimal tongCungChiTra;
    private BigDecimal tongKhac;
    private BigDecimal tongNguoiBenh;

    public BangKeTongTienDto(List<BangKeDto> dsBangKe) {
         BigDecimal tongThanhTienBv = BigDecimal.ZERO;
         BigDecimal tongThanhTienBh = BigDecimal.ZERO;
         BigDecimal tongQuyBhyt = BigDecimal.ZERO;
         BigDecimal tongCungChiTra = BigDecimal.ZERO;
         BigDecimal tongKhac = BigDecimal.ZERO;
         BigDecimal tongNguoiBenh = BigDecimal.ZERO;
        for (BangKeDto bangKe : dsBangKe) {
            tongThanhTienBv = tongThanhTienBv.add(bangKe.getThanhTienBv());
            tongThanhTienBh = tongThanhTienBh.add(bangKe.getThanhTienBh());
            tongQuyBhyt = tongQuyBhyt.add(bangKe.getQuyBhyt());
            tongCungChiTra = tongCungChiTra.add(bangKe.getCungChiTra());
            tongKhac = tongKhac.add(bangKe.getKhac());
            tongNguoiBenh = tongNguoiBenh.add(bangKe.getNguoiBenh());
        }
        this.tongThanhTienBv = tongThanhTienBv;
        this.tongThanhTienBh = tongThanhTienBh;
        this.tongQuyBhyt = tongQuyBhyt;
        this.tongCungChiTra = tongCungChiTra;
        this.tongKhac = tongKhac;
        this.tongNguoiBenh = tongNguoiBenh;
    }

}
