package vn.vnpt.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LanKcbDto {
    private long soVaoVien;
    private long soVaoVienDt;
    private int loaiKcb;
    private String dvtt;
}
