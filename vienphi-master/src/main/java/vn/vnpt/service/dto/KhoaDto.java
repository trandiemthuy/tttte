package vn.vnpt.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author NguyenDucAnhKhoi
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class KhoaDto {
    private String maKhoa;
    private String tenKhoa;
}
