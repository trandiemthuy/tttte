package vn.vnpt.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author NguyenDucAnhKhoi
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VienPhiLanTtDto {
    private long soVaoVien;
    private long soVaoVienDt;
    private int loaiKcb;
    private String dvtt;
    private long maBn;
    private long maNvTao;
    private String tenNvTao;
    private String maPhongBanTt;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date ngayThu;
    private BigDecimal soTienPhaiTt;
    private BigDecimal soTienMienGiam;
    private BigDecimal soTienTamUng;
    private BigDecimal soTienThucThu;
    private BigDecimal soTienThoiLai;
    private Long maQuyenBienLai;
    private Integer soBienLai;
    private int hinhThucTt;
    private String ghiChu;
    private List<String> dsSoPhieuMaDv;
}
