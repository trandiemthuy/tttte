package vn.vnpt.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author NguyenDucAnhKhoi
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NhanVienDto {

    private long maNhanVien;
    private String tenNhanVien;

}
