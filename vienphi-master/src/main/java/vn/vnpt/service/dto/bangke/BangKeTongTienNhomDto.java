package vn.vnpt.service.dto.bangke;

import vn.vnpt.domain.BangKe;
import vn.vnpt.service.constant.VienPhiConstants;
import vn.vnpt.service.utils.BigDecimalUtils;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author NguyenDucAnhKhoi
 * Object chứa giá trị tổng thành tiền của các nhóm trên bảng kê
 * thanhTienBv[1][0]: giá trị tổng thành tiền Bv của nhóm 1.
 * thanhTienBv [2][1] gía trị tổng thành tiền Bv của nhóm con 2.1
 *  các cột khác tương tự
 */
public class BangKeTongTienNhomDto {

    private static final int maxSttNhom = 12;
    private static final int maxSttNhomCon = 3;

    private final Map<String, BigDecimal[][]> instance;
    private final List<BangKeDto> listBangKeDto;

    public BangKeTongTienNhomDto(List<BangKeDto> listBangKeDto) {
        this.instance = initializeInstance();
        this.listBangKeDto = listBangKeDto;
        this.calculateGroupSum();
    }

    public BigDecimal getValue(int sttNhom, int sttNhomCon, String colName) {
        return this.instance.get(colName)[sttNhom][sttNhomCon];
    }

    private void calculateGroupSum() {
        int soLuongGoiVtyt = this.getSoLuongGoiVtyt();
        listBangKeDto.forEach(e -> {
            int sttNhom = e.getSttNhom();
            int sttNhomCon =  e.getSttNhomCon();
            if (sttNhom != 10) {
                this.addValue(sttNhom, 0, e);
                if (sttNhomCon != -1) {
                    this.addValue(sttNhom, sttNhomCon, e);
                }
            }
            // Trườmg hợp gói VTYT
            else {
                // Tính tổng từng gói (không tính stent 2 trở đi)
                int sttStent = e.getVatTuStent() == null || e.getVatTuStent() != 1 || e.getSttStent() == null ? 0 : e.getSttStent();
                if (sttStent < 2) {
                    this.addValue(sttNhom, sttNhomCon, e);
                }
            }
        });
        if (soLuongGoiVtyt > 0) {
            BigDecimal tien45tlcs = VienPhiConstants.tienThangLuongCoSo.multiply(BigDecimal.valueOf(45));
            for (int sttGoiVtyt = 1; sttGoiVtyt <= soLuongGoiVtyt; sttGoiVtyt++) {
                if (this.getValue(10, sttGoiVtyt, "tongThanhTienBh").compareTo(tien45tlcs) >= 0) {
                    this.setValue(10, sttGoiVtyt, "tongThanhTienBh", tien45tlcs);
                }
                BigDecimal tongThanhTienBvGoiVtyt = this.getValue(10, sttGoiVtyt, "tongThanhTienBv");
                BigDecimal tongThanhTienBhGoiVtyt = this.getValue(10, sttGoiVtyt, "tongThanhTienBh");
                BigDecimal tongQuyBhytGoiVtyt = this.getValue(10, sttGoiVtyt, "tongQuyBhyt");
                BigDecimal tongCungChiTraGoiVtyt = this.getValue(10, sttGoiVtyt, "tongCungChiTra");
                BigDecimal tongKhacGoiVtyt = this.getValue(10, sttGoiVtyt, "tongKhac");
                BigDecimal tongNguoiBenhGoiVtyt = this.getValue(10, sttGoiVtyt, "tongNguoiBenh");

                /* Tinh tổng thành tiền nhóm 10 = tổng các gói*/
                this.addValue(10, 0, "tongThanhTienBv", tongThanhTienBvGoiVtyt);
                this.addValue(10, 0, "tongThanhTienBh", tongThanhTienBhGoiVtyt);
                this.addValue(10, 0, "tongQuyBhyt", tongQuyBhytGoiVtyt);
                this.addValue(10, 0, "tongCungChiTra", tongCungChiTraGoiVtyt);
                this.addValue(10, 0, "tongKhac", tongKhacGoiVtyt);
                this.addValue(10, 0, "tongNguoiBenh", tongNguoiBenhGoiVtyt);
            }

            /* Cộng stent >= 2 vào tổng thành tiền nhóm 10. */
            listBangKeDto.stream()
                .filter(e -> e.getSttStent() != null && e.getSttStent() >= 2)
                .forEach(e -> this.addValue(10, 0, e));
        }
    }

    private int getSoLuongGoiVtyt() {
        BangKeDto temp = listBangKeDto.stream()
            .filter(e -> e.getSttNhom() == 10)
            .max(Comparator.comparingInt(BangKeDto::getSttNhomCon))
            .orElse(null);
        if (temp != null)
            return temp.getSttNhomCon();
        return 0;
    }

    private Map<String, BigDecimal[][]> initializeInstance() {
        int lengthX = maxSttNhom + 1;
        int lengthY = maxSttNhomCon + 1;

        BigDecimal[][] tongThanhTienBv = new BigDecimal[lengthX][lengthY];
        BigDecimal[][] tongThanhTienBh = new BigDecimal[lengthX][lengthY];
        BigDecimal[][] tongQuyBhyt = new BigDecimal[lengthX][lengthY];
        BigDecimal[][] tongCungChiTra = new BigDecimal[lengthX][lengthY];
        BigDecimal[][] tongKhac = new BigDecimal[lengthX][lengthY];
        BigDecimal[][] tongNguoiBenh = new BigDecimal[lengthX][lengthY];

        for (int x=0; x<lengthX; x++)
            for (int y=0; y<lengthY; y++) {
                tongThanhTienBv[x][y] = BigDecimal.ZERO;
                tongThanhTienBh[x][y] = BigDecimal.ZERO;
                tongQuyBhyt[x][y] = BigDecimal.ZERO;
                tongCungChiTra[x][y] = BigDecimal.ZERO;
                tongKhac[x][y] = BigDecimal.ZERO;
                tongNguoiBenh[x][y] = BigDecimal.ZERO;
            }

        Map<String, BigDecimal[][]> instance = new HashMap<>();
        instance.put("tongThanhTienBv", tongThanhTienBv);
        instance.put("tongThanhTienBh", tongThanhTienBh);
        instance.put("tongQuyBhyt", tongQuyBhyt);
        instance.put("tongCungChiTra", tongCungChiTra);
        instance.put("tongKhac", tongKhac);
        instance.put("tongNguoiBenh", tongNguoiBenh);

        return instance;
    }

    private void setValue(int sttNhom, int sttNhomCon, String colName, BigDecimal value) {
        BigDecimal[][] col = this.instance.get(colName);
        if (col != null) {
            col[sttNhom][sttNhomCon] = value;
            this.instance.put(colName, col);
        }
    }

    private void addValue(int sttNhom, int sttNhomCon, String colName, BigDecimal value) {
        BigDecimal[][] col = this.instance.get(colName);
        if (col != null) {
            col[sttNhom][sttNhomCon] = col[sttNhom][sttNhomCon].add(value);
            this.instance.put(colName, col);
        }
    }

    private void addValue(int sttNhom, int sttNhomCon, BangKeDto e) {
        this.addValue(sttNhom, sttNhomCon, "tongThanhTienBv", e.getThanhTienBv());
        this.addValue(sttNhom, sttNhomCon, "tongThanhTienBh", e.getThanhTienBh());
        this.addValue(sttNhom, sttNhomCon, "tongQuyBhyt", e.getQuyBhyt());
        this.addValue(sttNhom, sttNhomCon, "tongCungChiTra", e.getCungChiTra());
        this.addValue(sttNhom, sttNhomCon, "tongKhac", e.getKhac());
        this.addValue(sttNhom, sttNhomCon, "tongNguoiBenh", e.getNguoiBenh());
    }

}
