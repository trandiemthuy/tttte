package vn.vnpt.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * @author NguyenDucAnhKhoi
 */
public class BaoCaoBhxhDto {
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date tuNgay;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date denNgay;

}
