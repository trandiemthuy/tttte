package vn.vnpt.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author NguyenDucAnhKhoi
 */

@NoArgsConstructor
@Getter
@Setter
public class DuLieuBenhNhanDto {
    private long soVaoVien;
    private long soVaoVienDt;
    private int loaiKcb;
    private String dvtt;
    private long maBn;
    private String hoTen;
    @JsonFormat(pattern="yyyyMMdd")
    private Date ngaySinh;
    private Integer gioiTinh;
    private String diaChi;
    private Integer sttDieuTri;
    private Integer coBhyt;
    private String maThe;
    private String maDkbd;
    private String tenDkbd;
    @JsonFormat(pattern="yyyyMMdd")
    private Date gtTheTu;
    @JsonFormat(pattern="yyyyMMdd")
    private Date gtTheDen;
    private BigDecimal phanTramThe;
    @JsonFormat(pattern="yyyyMMdd")
    private Date ngayDu5nam;
    @JsonFormat(pattern="yyyyMMdd")
    private Date mienCungCt;
    private String maKhuVuc;
    private BigDecimal canNang;
    private Integer trangThaiBn;
    private String maBenh;
    private String tenBenh;
    private String maBenhKhac;
    private String tenBenhKhac;
    private Integer maLyDoVvien;
    private String maNoiChuyen;
    private Integer maTaiNan;
    @JsonFormat(pattern="yyyyMMddHHmm")
    private Date ngayVao;
    @JsonFormat(pattern="yyyyMMddHHmm")
    private Date ngayKham;
    @JsonFormat(pattern="yyyyMMddHHmm")
    private Date ngayRa;
    private Integer ketQuaDtri;
    private Integer tinhTrangRv;
    private String maKhoa;
    private String maKhoaBc;
    private String tenKhoa;
    private String tenCskcb;
    private Integer tuyenDonVi;     // 1 trung uong, 2 tinh
    private String tenTinh;

    private List<DichVuChiDinhDto> dsDvChiDinh;
}
