package vn.vnpt.service.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author NguyenDucAnhKhoi
 */
@Data
public class ThongTinVpTongHopDto {
    private BigDecimal tongDaThu;
    private BigDecimal thanhToan;
    private BigDecimal tamUng;
    private BigDecimal tamUngConLai;
    private BigDecimal tongChiPhi;
    private BigDecimal tongBh;
    private BigDecimal bhChiTra;
    private BigDecimal cungChiTra;
    private BigDecimal bnPhaiTra;
    private BigDecimal hoanUng;
    private BigDecimal conLai;
}
