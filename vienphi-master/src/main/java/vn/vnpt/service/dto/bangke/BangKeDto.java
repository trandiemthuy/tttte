package vn.vnpt.service.dto.bangke;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author NguyenDucAnhKhoi
 */
@Data
public class BangKeDto {
    private String tenNhom;
    private String tenNhomCon;
    private int ngoaiDanhMuc;
    private String noiDung;
    private String dvt;
    private BigDecimal soLuong;
    private BigDecimal donGiaBv;
    private BigDecimal donGiaBh;
    private BigDecimal tyLeThanhToanTheoDv;
    private BigDecimal thanhTienBv;
    private BigDecimal tyLeThanhToanBhyt;
    private BigDecimal thanhTienBh;
    private BigDecimal quyBhyt;
    private BigDecimal cungChiTra;
    private BigDecimal khac;
    private BigDecimal nguoiBenh;

    private Integer ttThanhToan;
    private String soPhieuMaDv;

    private Integer vatTuChungGoi;
    private Integer sttGoiVatTu;
    private Integer vatTuStent;
    private Integer sttStent;
    private BigDecimal tranTt;

    private String maThe;
    private String gtTheTu;
    private String gtTheDen;
    private BigDecimal mucHuong;
    private String chiPhiKcbTu;
    private String chiPhiKcbDen;
    private Integer sttDieuTri;

    private BigDecimal tongThanhTienBvGroup;
    private BigDecimal tongThanhTienBhGroup;
    private BigDecimal tongQuyBhytGroup;
    private BigDecimal tongCungChiTraGroup;
    private BigDecimal tongKhacGroup;
    private BigDecimal tongNguoiBenhGroup;

    private BigDecimal tongThanhTienBvGroupCon;
    private BigDecimal tongThanhTienBhGroupCon;
    private BigDecimal tongQuyBhytGroupCon;
    private BigDecimal tongCungChiTraGroupCon;
    private BigDecimal tongKhacGroupCon;
    private BigDecimal tongNguoiBenhGroupCon;

    public int getSttNhom() {
        return getTenNhom() == null ? -1 :
            Integer.parseInt(getTenNhom().split("\\.")[0]);    }

    public int getSttNhomCon() {
        return getTenNhomCon() == null ? -1 :
            Integer.parseInt(getTenNhomCon().split("\\.")[1]);
    }
}
