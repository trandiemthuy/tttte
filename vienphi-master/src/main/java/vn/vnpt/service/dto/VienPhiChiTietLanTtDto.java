package vn.vnpt.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @author NguyenDucAnhKhoi
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VienPhiChiTietLanTtDto {

    private String soPhieuMaDv;
    private String nhomLanTt;
    private int maNhom4210;
    private String noiDung;
    private String dvt;
    private BigDecimal soLuong;
    private BigDecimal donGia;
    private BigDecimal thanhTien;
    private BigDecimal quyBhyt;
    private BigDecimal cungChiTra;
    private BigDecimal nguoiBenh;
    private BigDecimal khac;
    private BigDecimal tranTt;
    private BigDecimal phaiTt;
    private String ghiChuChiTietLanTt;
    private String tenNhomBangKe;

}
