package vn.vnpt.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VienPhiLanTamUngDto {
    private long soVaoVien;
    private long soVaoVienDt;
    private int loaiKcb;
    private String dvtt;
    private long maBn;
    private long maNvTao;
    private String tenNvTao;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING)
    private Date ngayThu;
    private String maPhongBanThu;
    private BigDecimal soTienTamUng;
    private BigDecimal soTienBnTra;
    private BigDecimal soTienThoiLai;
    private Long maQuyenBienLai;
    private Integer soBienLai;
    private String ghiChu;
}
