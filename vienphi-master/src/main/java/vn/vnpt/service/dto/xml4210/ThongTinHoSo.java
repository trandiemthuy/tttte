package vn.vnpt.service.dto.xml4210;

import java.util.Date;
import java.util.List;

/**
 * @author NguyenDucAnhKhoi
 */
public class ThongTinHoSo {
    private Date ngayLap;
    private int soLuongHoSo;
    private List<HoSo> danhSachHoSo;
}
