package vn.vnpt.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author NguyenDucAnhKhoi
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BenhNhanDto {
    private long soVaoVien;
    private long soVaoVienDt;
    private int loaiKcb;
    private String dvtt;

    private String maKhoa;

    private long maBn;
    private String hoTen;
    private Date ngaySinh;
    private int gioiTinh;
    private String diaChi;

    private int coBhyt;
    private String maThe;
    private BigDecimal phanTramThe;
    private int mienCungCt;
    private Date gtTheTu;
    private Date gtTheDen;
    private String noiDkbd;
    private int dungTuyen;

    private Date ngayVao;
    private Date ngayRa;

    private String tenBenh;
    private String trangThaiBn;
}
