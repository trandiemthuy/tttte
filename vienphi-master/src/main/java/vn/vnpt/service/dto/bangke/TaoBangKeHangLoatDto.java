package vn.vnpt.service.dto.bangke;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * @author NguyenDucAnhKhoi
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaoBangKeHangLoatDto {

    private Integer loaiKcb;
    private String dvtt;

    @JsonFormat(pattern="yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private Date tuNgay;

    @JsonFormat(pattern="yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private Date denNgay;
}
