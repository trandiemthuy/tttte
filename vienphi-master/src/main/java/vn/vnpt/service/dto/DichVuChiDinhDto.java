package vn.vnpt.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author NguyenDucAnhKhoi
 */

@NoArgsConstructor
@Getter
@Setter
public class DichVuChiDinhDto {

    private String soPhieuMaDv;
    private String ghiChu;

    private String maThuoc;
    private String tenThuoc;
    private String maDichVu;
    private String tenDichVu;
    private String maVatTu;
    private String tenVatTu;
    private String maGiuong;

    private String dvt;
    private BigDecimal soLuong;
    private BigDecimal donGia;
    private Integer ngoaiDanhMuc;
    private Integer maNhom4210;


    private String hamLuong;
    private String duongDung;
    private String lieuDung;
    private String soDangKy;
    private String ttThau;

    private String maKhoa;
    private String maBacSi;
    private String maBenh;
    @JsonFormat(pattern="yyyyMMddHHmm")
    private Date ngayYl;
    @JsonFormat(pattern="yyyyMMddHHmm")
    private Date ngayKq;
    private Integer maPttt;

    private Integer namCungGiuong;
    private Integer soNguoiNam;

    private Integer dvktPhatSinh;
    private Integer cungEkipPt;

    private BigDecimal tyLeTtBhytCls;
    private BigDecimal tyLeThuocVtyt;

    private Integer vatTuChungGoi;
    private Integer sttGoiVatTu;
    private Integer vatTuStent;
    private BigDecimal tranTt;

}
