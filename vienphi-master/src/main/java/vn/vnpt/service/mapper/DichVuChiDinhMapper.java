package vn.vnpt.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import vn.vnpt.domain.DichVuChiDinh;
import vn.vnpt.service.dto.DichVuChiDinhDto;

import java.util.Date;

/**
 * @author NguyenDucAnhKhoi
 */
@Mapper(componentModel = "spring")
public abstract class DichVuChiDinhMapper {

    public abstract DichVuChiDinh toEntity(DichVuChiDinhDto dichVuChiDinhDto);

}
