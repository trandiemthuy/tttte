package vn.vnpt.service.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.vnpt.domain.*;
import vn.vnpt.service.BangKeServices;
import vn.vnpt.service.DuLieuBenhNhanService;
import vn.vnpt.service.TransactionLogService;
import vn.vnpt.service.VienPhiQuyenBienLaiService;
import vn.vnpt.service.dto.LanKcbDto;
import vn.vnpt.service.dto.VienPhiLanTtDto;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author NguyenDucAnhKhoi
 */
@Component
public class VienPhiLanTtMapper {

    private final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    VienPhiQuyenBienLaiService vienPhiQuyenBienLaiService;

    @Autowired
    BangKeServices bangKeServices;

    @Autowired
    DuLieuBenhNhanService duLieuBenhNhanService;

    @Autowired
    TransactionLogService transactionLogService;

    @Autowired
    VienPhiChiTietLanTtMapper vienPhiChiTietLanTtMapper;

    @Autowired
    LanKcbDtoMapper lanKcbDtoMapper;

    public VienPhiLanTt toEntity(VienPhiLanTtDto vienPhiLanTtDto) {
        VienPhiLanTt entity = modelMapper.map(vienPhiLanTtDto, VienPhiLanTt.class);

        Long maQuyenBienLai = vienPhiLanTtDto.getMaQuyenBienLai();
        if (maQuyenBienLai != null) {
            VienPhiQuyenBienLai vienPhiQuyenBienLai = vienPhiQuyenBienLaiService.getQuyenBienLai(maQuyenBienLai);
            entity.setVienPhiQuyenBienLai(vienPhiQuyenBienLai);
        }

        LanKcbDto lanKcbDto = lanKcbDtoMapper.toDto(vienPhiLanTtDto);
        List<BangKe> dsBangKe = bangKeServices.getBangKe(lanKcbDto);

        DuLieuBenhNhan duLieuBenhNhan = duLieuBenhNhanService.getDuLieuBenhNhan(lanKcbDto);
        List<DichVuChiDinh> dsDichVuChiDinh = duLieuBenhNhan.getDsDichVuChiDinh();

//        entity.setTransactionLog(dsBangKe.get(0).getTransactionLog());

        List<VienPhiChiTietLanTt> listVienPhiChiTietLanTt =  vienPhiLanTtDto.getDsSoPhieuMaDv().stream()
            .map(soPhieuMaDv -> {
                boolean soPhieuMaDvHopLe = dsDichVuChiDinh.stream().anyMatch(e -> e.getSoPhieuMaDv().equals(soPhieuMaDv));
                if (!soPhieuMaDvHopLe) {
                    throw new IllegalStateException("Một hoặc vài chỉ định có thể đã bị xóa. Vui lòng kiểm tra lại !");
                }
                BangKe bangKe = dsBangKe.stream().filter(e -> e.getSoPhieuMaDv().equals(soPhieuMaDv)).findFirst().orElseThrow(IllegalStateException::new);
                VienPhiChiTietLanTt temp = vienPhiChiTietLanTtMapper.toEntity(vienPhiChiTietLanTtMapper.toDto(bangKe));
                temp.setMaBn(entity.getMaBn());
                return temp;
            })
            .collect(Collectors.toList());

        entity.setListVienPhiChiTietLanTt(listVienPhiChiTietLanTt);

        return entity;
    }

}
