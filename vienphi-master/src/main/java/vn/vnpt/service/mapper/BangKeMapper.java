package vn.vnpt.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import vn.vnpt.domain.BangKe;
import vn.vnpt.domain.DichVuChiDinh;
import vn.vnpt.domain.DuLieuBenhNhan;
import vn.vnpt.service.constant.LoaiKcbEnum;
import vn.vnpt.service.constant.MaLyDoVvienEnum;
import vn.vnpt.service.constant.MaNhom4210Enum;
import vn.vnpt.service.constant.bangke.BangKeTenNhomConEnum;
import vn.vnpt.service.constant.bangke.BangKeTenNhomEnum;
import vn.vnpt.service.dto.bangke.*;
import vn.vnpt.service.utils.BigDecimalUtils;
import vn.vnpt.service.utils.ReadNumberUtils;

import java.math.BigDecimal;
import java.math.MathContext;

@Mapper(componentModel = "spring")
public abstract class BangKeMapper {

    @Mappings({
        @Mapping(source = "gtTheTu", target = "gtTheTu", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "gtTheDen", target = "gtTheDen", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "chiPhiKcbTu", target = "chiPhiKcbTu", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "chiPhiKcbDen", target = "chiPhiKcbDen", dateFormat = "dd/MM/yyyy"),
    })
    public abstract BangKeDto toDto(BangKe entity);

    public BangKeDto setTongTienNhom(BangKeDto bangKeDto, BangKeTongTienNhomDto bangKeTongTienNhomDto) {
        int sttNhom = bangKeDto.getSttNhom();
        int sttNhomCon =  bangKeDto.getSttNhomCon();
        bangKeDto.setTongThanhTienBvGroup(bangKeTongTienNhomDto.getValue(sttNhom, 0, "tongThanhTienBv"));
        bangKeDto.setTongThanhTienBhGroup(bangKeTongTienNhomDto.getValue(sttNhom, 0, "tongThanhTienBh"));
        bangKeDto.setTongQuyBhytGroup(bangKeTongTienNhomDto.getValue(sttNhom, 0, "tongQuyBhyt"));
        bangKeDto.setTongCungChiTraGroup(bangKeTongTienNhomDto.getValue(sttNhom, 0, "tongCungChiTra"));
        bangKeDto.setTongKhacGroup(bangKeTongTienNhomDto.getValue(sttNhom, 0, "tongKhac"));
        bangKeDto.setTongNguoiBenhGroup(bangKeTongTienNhomDto.getValue(sttNhom, 0, "tongNguoiBenh"));
        if (sttNhomCon != -1) {
            bangKeDto.setTongThanhTienBvGroupCon(bangKeTongTienNhomDto.getValue(sttNhom, sttNhomCon, "tongThanhTienBv"));
            bangKeDto.setTongThanhTienBhGroupCon(bangKeTongTienNhomDto.getValue(sttNhom, sttNhomCon, "tongThanhTienBh"));
            bangKeDto.setTongQuyBhytGroupCon(bangKeTongTienNhomDto.getValue(sttNhom, sttNhomCon, "tongQuyBhyt"));
            bangKeDto.setTongCungChiTraGroupCon(bangKeTongTienNhomDto.getValue(sttNhom, sttNhomCon, "tongCungChiTra"));
            bangKeDto.setTongKhacGroupCon(bangKeTongTienNhomDto.getValue(sttNhom, sttNhomCon, "tongKhac"));
            bangKeDto.setTongNguoiBenhGroupCon(bangKeTongTienNhomDto.getValue(sttNhom, sttNhomCon, "tongNguoiBenh"));
        }
        return bangKeDto;
    }

    private BigDecimal tinhMucHuong(BigDecimal phanTramThe, boolean vuot15phanTramLuongCs, LoaiKcbEnum loaiKcb, MaLyDoVvienEnum maLyDoVvien, String maKhuVuc, int tuyenDonVi) {
        BigDecimal temp = vuot15phanTramLuongCs ? phanTramThe : BigDecimal.valueOf(100);
        if (maLyDoVvien == MaLyDoVvienEnum.TRAI_TUYEN) {
            BigDecimal traiTuyen =
                loaiKcb == LoaiKcbEnum.NOI_TRU && (maKhuVuc != null && (maKhuVuc.equals("K1") || maKhuVuc.equals("K2") || maKhuVuc.equals("K3"))) ?
                    BigDecimal.valueOf(100) :
                    loaiKcb == LoaiKcbEnum.NOI_TRU && tuyenDonVi == 2 ?
                        BigDecimal.valueOf(60) :
                        loaiKcb == LoaiKcbEnum.NOI_TRU && tuyenDonVi == 1 ?
                            BigDecimal.valueOf(40) :
                            BigDecimal.ZERO;
            return BigDecimalUtils.round(temp.multiply(traiTuyen.divide(BigDecimal.valueOf(100), MathContext.DECIMAL64)));
        }
        return temp;
    }

    public BangKe initBangKe(DuLieuBenhNhan duLieuBenhNhan, DichVuChiDinh dichVuChiDinh, boolean vuot15phanTramLuongCs) {
        long soVaoVien = duLieuBenhNhan.getSoVaoVien();
        long soVaoVienDt = duLieuBenhNhan.getSoVaoVienDt();
        int loaiKcb = duLieuBenhNhan.getLoaiKcb();
        String dvtt = duLieuBenhNhan.getDvtt();
        boolean coBhyt = duLieuBenhNhan.getCoBhyt() == 1;
        boolean ngoaiDanhMuc = dichVuChiDinh.getNgoaiDanhMuc() == 1;
        boolean bhytKhongChi = ngoaiDanhMuc || !coBhyt;

        BangKe bangKe = new BangKe();
        bangKe.setTransactionLog(dichVuChiDinh.getTransactionLog());
        bangKe.setSoVaoVien(soVaoVien);
        bangKe.setSoVaoVienDt(soVaoVienDt);
        bangKe.setLoaiKcb(loaiKcb);
        bangKe.setDvtt(dvtt);
        bangKe.setMaBn(duLieuBenhNhan.getMaBn());

        bangKe.setMaNhom4210(dichVuChiDinh.getMaNhom4210());
        bangKe.setTtThanhToan(dichVuChiDinh.getTtThanhToan());
        bangKe.setSoPhieuMaDv(dichVuChiDinh.getSoPhieuMaDv());
        bangKe.setNgoaiDanhMuc(dichVuChiDinh.getNgoaiDanhMuc());
        bangKe.setDvt(dichVuChiDinh.getDvt());
        bangKe.setSoLuong(dichVuChiDinh.getSoLuong());
        bangKe.setDonGiaBv(dichVuChiDinh.getDonGia());
        /* Không có trần thanh toán */
        if (dichVuChiDinh.getTranTt() == null || dichVuChiDinh.getTranTt().compareTo(BigDecimal.ZERO) == 0) {
            bangKe.setDonGiaBh(dichVuChiDinh.getDonGia());
        }
        /* Có trần thanh toán */
        else {
            /* đơn giá < trần thanh toán */
            if (dichVuChiDinh.getDonGia().compareTo(dichVuChiDinh.getTranTt()) < 0) {
                bangKe.setDonGiaBh(dichVuChiDinh.getDonGia());
            }
            else {
                bangKe.setDonGiaBh(dichVuChiDinh.getTranTt());
            }
        }

        bangKe.setSttDieuTri(duLieuBenhNhan.getSttDieuTri());
        bangKe.setChiPhiKcbTu(duLieuBenhNhan.getNgayVao());
        bangKe.setChiPhiKcbDen(duLieuBenhNhan.getNgayRa());

        if (coBhyt) {
            bangKe.setMaThe(duLieuBenhNhan.getMaThe());
            bangKe.setGtTheTu(duLieuBenhNhan.getGtTheTu());
            bangKe.setGtTheDen(duLieuBenhNhan.getGtTheDen());
            bangKe.setMucHuong(
                this.tinhMucHuong(
                    duLieuBenhNhan.getPhanTramThe(),
                    vuot15phanTramLuongCs,
                    LoaiKcbEnum.valueOf(loaiKcb),
                    MaLyDoVvienEnum.valueOf(duLieuBenhNhan.getMaLyDoVvien()),
                    duLieuBenhNhan.getMaKhuVuc(),
                    duLieuBenhNhan.getTuyenDonVi()
                )
            );
        }
        else {
            bangKe.setMucHuong(BigDecimal.ZERO);
        }

        MaNhom4210Enum maNhom = MaNhom4210Enum.valueOf(dichVuChiDinh.getMaNhom4210());
        /* 1. Khám bệnh: */
        if (maNhom == MaNhom4210Enum.KHAM_BENH) {
            bangKe.setNoiDung(dichVuChiDinh.getTenDichVu());
            bangKe.setTenNhom(BangKeTenNhomEnum.KHAM_BENH.getValue());
            Integer sttCongKham = dichVuChiDinh.getSttCongKham();
            bangKe.setTyLeThanhToanTheoDv(
                sttCongKham == 1 ? BigDecimal.valueOf(100) :
                    sttCongKham >= 2 && sttCongKham <= 4 ? BigDecimal.valueOf(30) :
                        sttCongKham == 5 ? BigDecimal.valueOf(10) :
                            BigDecimal.ZERO
            );
            bangKe.setTyLeThanhToanBhyt(bhytKhongChi ? BigDecimal.ZERO : BigDecimal.valueOf(100));
        }
        /* 2. Ngày giường: */
        else if (maNhom == MaNhom4210Enum.NGAY_GIUONG_BAN_NGAY || maNhom == MaNhom4210Enum.NGAY_GIUONG_NOU_TRU_LUU) {
            bangKe.setNoiDung(dichVuChiDinh.getTenDichVu());
            bangKe.setTenNhom(BangKeTenNhomEnum.NGAY_GIUONG.getValue());
            bangKe.setTenNhomCon(
                MaNhom4210Enum.valueOf(dichVuChiDinh.getMaNhom4210()) == MaNhom4210Enum.NGAY_GIUONG_BAN_NGAY ? BangKeTenNhomConEnum.NGAY_GIUONG_BAN_NGAY.getValue() :
                    BangKeTenNhomConEnum.NGAY_GIUONG_NOI_TRU.getValue()
            );
            bangKe.setTyLeThanhToanTheoDv(
                dichVuChiDinh.getNamCungGiuong() == null || dichVuChiDinh.getNamCungGiuong() == 0 ? BigDecimal.valueOf(100) :
                    dichVuChiDinh.getSoNguoiNam() == 2 ? BigDecimal.valueOf(50) : BigDecimal.valueOf(33)
            );
            bangKe.setTyLeThanhToanBhyt(bhytKhongChi ? BigDecimal.ZERO : BigDecimal.valueOf(100));
        }
                /* 3. Xét nghiệm
                   4. Chẩn đoán hình ảnh
                   5. Thăm dò chức năng
                   6. Thủ thuật, phẩu thuật */
        else if (maNhom == MaNhom4210Enum.XET_NGHIEM || maNhom == MaNhom4210Enum.CHAN_DOAN_HINH_ANH || maNhom == MaNhom4210Enum.THAM_DO_CHUC_NANG || maNhom == MaNhom4210Enum.THU_THUAT_PHAU_THUAT || maNhom == MaNhom4210Enum.DVKT_TT_THEO_TY_LE ) {
            bangKe.setNoiDung(dichVuChiDinh.getTenDichVu());
            bangKe.setTenNhom(
                maNhom == MaNhom4210Enum.XET_NGHIEM ? BangKeTenNhomEnum.XET_NGHIEM.getValue() :
                    maNhom == MaNhom4210Enum.THU_THUAT_PHAU_THUAT ? BangKeTenNhomEnum.THU_THUAT_PHAU_THUAT.getValue() :
                        maNhom == MaNhom4210Enum.CHAN_DOAN_HINH_ANH ? BangKeTenNhomEnum.CHAN_DOAN_HINH_ANH.getValue():
                            maNhom == MaNhom4210Enum.THAM_DO_CHUC_NANG ? BangKeTenNhomEnum.THAM_DO_CHUC_NANG.getValue() :
                                null
            );
            bangKe.setTyLeThanhToanTheoDv(
                dichVuChiDinh.getDvktPhatSinh() != null && dichVuChiDinh.getDvktPhatSinh() == 1 ?
                    dichVuChiDinh.getCungEkipPt() == 1 ? BigDecimal.valueOf(50) : BigDecimal.valueOf(80) :
                    BigDecimal.valueOf(100)
            );
            bangKe.setTyLeThanhToanBhyt(
                bhytKhongChi ? BigDecimal.ZERO :
                    dichVuChiDinh.getTyLeTtBhytCls() == null ? BigDecimal.valueOf(100) :
                        dichVuChiDinh.getTyLeTtBhytCls()
            );
        }
        /* 7. Máu, chế phẩm máu, vận chuyển máu */
        else if (maNhom == MaNhom4210Enum.MAU) {
            bangKe.setNoiDung(dichVuChiDinh.getTenThuoc());
            bangKe.setTenNhom(BangKeTenNhomEnum.MAU.getValue());
            bangKe.setTyLeThanhToanTheoDv(BigDecimal.valueOf(100));
            bangKe.setTyLeThanhToanBhyt(bhytKhongChi ? BigDecimal.ZERO : BigDecimal.valueOf(100));
        }
        /* 8. Thuốc, dịch truyền */
        else if (maNhom == MaNhom4210Enum.THUOC_TRONG_DANH_MUC || maNhom == MaNhom4210Enum.THUOC_NGOAI_DANH_MUC || maNhom == MaNhom4210Enum.THUOC_TT_THEO_TY_LE) {
            bangKe.setNoiDung(dichVuChiDinh.getTenThuoc());
            bangKe.setTenNhom(BangKeTenNhomEnum.THUOC.getValue());
            bangKe.setTyLeThanhToanTheoDv(BigDecimal.valueOf(100));
            bangKe.setTyLeThanhToanBhyt(
                bhytKhongChi ? BigDecimal.ZERO :
                    dichVuChiDinh.getTyLeThuocVtyt() == null ? BigDecimal.valueOf(100) :
                        dichVuChiDinh.getTyLeThuocVtyt()
            );
        }
        else if (maNhom == MaNhom4210Enum.VTYT_TRONG_DANH_MUC || maNhom == MaNhom4210Enum.VTYT_TT_THEO_TY_LE) {
            /* 9. Vật tư y tế: */
            if (dichVuChiDinh.getVatTuChungGoi() != 1) {
                bangKe.setNoiDung(dichVuChiDinh.getTenVatTu());
                bangKe.setTenNhom(BangKeTenNhomEnum.VAT_TU_Y_TE.getValue());
                bangKe.setTyLeThanhToanTheoDv(BigDecimal.valueOf(100));
                bangKe.setTyLeThanhToanBhyt(
                    bhytKhongChi ? BigDecimal.ZERO :
                        dichVuChiDinh.getTyLeThuocVtyt() == null ? BigDecimal.valueOf(100) :
                            dichVuChiDinh.getTyLeThuocVtyt()
                );
            }
            /* 10. Gói vật tư y tế */
            else {
                String stentPosfix = "";
                if (dichVuChiDinh.getVatTuStent() != null && dichVuChiDinh.getVatTuStent() == 1 && dichVuChiDinh.getSttStent() != null && dichVuChiDinh.getSttStent() > 0) {
                    stentPosfix = String.format(" (Stent thứ %s)" , ReadNumberUtils.readStentNumber(dichVuChiDinh.getSttStent()));
                }
                bangKe.setNoiDung(dichVuChiDinh.getTenVatTu() + stentPosfix);
                bangKe.setTenNhom(BangKeTenNhomEnum.GOI_VTYT.getValue());
                bangKe.setTenNhomCon(BangKeTenNhomConEnum.GOI_VTYT.getValue().replaceAll("%d", dichVuChiDinh.getSttGoiVatTu().toString()));
                bangKe.setTyLeThanhToanTheoDv(BigDecimal.valueOf(100));
                bangKe.setTyLeThanhToanBhyt(
                    bhytKhongChi ? BigDecimal.ZERO :
                        dichVuChiDinh.getVatTuStent() == null || dichVuChiDinh.getVatTuStent() == 0 ?  (dichVuChiDinh.getTyLeThuocVtyt() == null ? BigDecimal.valueOf(100) : dichVuChiDinh.getTyLeThuocVtyt()) :
                            dichVuChiDinh.getSttStent() == 1 ? BigDecimal.valueOf(100) :
                                dichVuChiDinh.getSttStent() == 2 ? BigDecimal.valueOf(50) :
                                    BigDecimal.valueOf(0)
                );
                if (dichVuChiDinh.getVatTuStent() != null && dichVuChiDinh.getVatTuStent() == 1 && dichVuChiDinh.getSttStent() != null && dichVuChiDinh.getSttStent() > 3) {
                    bangKe.setNgoaiDanhMuc(1);
                }
                bangKe.setTranTt(dichVuChiDinh.getTranTt());
                bangKe.setVatTuStent(dichVuChiDinh.getVatTuStent());
                bangKe.setSttStent(dichVuChiDinh.getSttStent());
                bangKe.setVatTuChungGoi(dichVuChiDinh.getVatTuChungGoi());
                bangKe.setSttGoiVatTu(dichVuChiDinh.getSttGoiVatTu());
            }
        }
        /* 11. Vận chuyển người bệnh */
        else if (maNhom == MaNhom4210Enum.VAN_CHUYEN) {
            bangKe.setNoiDung(dichVuChiDinh.getTenDichVu());
            bangKe.setTenNhom(BangKeTenNhomEnum.VAN_CHUYEN.getValue());
            bangKe.setTyLeThanhToanTheoDv(BigDecimal.valueOf(100));
            bangKe.setTyLeThanhToanBhyt(bhytKhongChi ? BigDecimal.ZERO : BigDecimal.valueOf(100));
        }
        /* 12. Dịch vụ khác */
        else {
            bangKe.setNoiDung(dichVuChiDinh.getTenDichVu());
            bangKe.setTenNhom(BangKeTenNhomEnum.DV_KHAC.getValue());
            bangKe.setTyLeThanhToanTheoDv(BigDecimal.valueOf(100));
            bangKe.setTyLeThanhToanBhyt(bhytKhongChi ? BigDecimal.ZERO : BigDecimal.valueOf(100));
        }

        return bangKe;
    }

}
