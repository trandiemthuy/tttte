package vn.vnpt.service.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.vnpt.domain.VienPhiLanTamUng;
import vn.vnpt.domain.VienPhiQuyenBienLai;
import vn.vnpt.service.VienPhiQuyenBienLaiService;
import vn.vnpt.service.dto.VienPhiLanTamUngDto;

@Component
public class VienPhiLanTamUngMapper {

    private final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    VienPhiQuyenBienLaiService vienPhiQuyenBienLaiService;

    public VienPhiLanTamUng toEntity(VienPhiLanTamUngDto dto) {
        VienPhiLanTamUng entity = modelMapper.map(dto, VienPhiLanTamUng.class);
        Long maQuyenBienLai = dto.getMaQuyenBienLai();
        if (maQuyenBienLai != null) {
            VienPhiQuyenBienLai vienPhiQuyenBienLai = vienPhiQuyenBienLaiService.getQuyenBienLai(maQuyenBienLai);
            entity.setVienPhiQuyenBienLai(vienPhiQuyenBienLai);
        }
        return entity;
    }

}
