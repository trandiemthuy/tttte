package vn.vnpt.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import vn.vnpt.domain.DuLieuBenhNhan;
import vn.vnpt.service.dto.BenhNhanDto;
import vn.vnpt.service.dto.DuLieuBenhNhanDto;

import java.util.Date;

/**
 * @author NguyenDucAnhKhoi
 */
@Mapper(componentModel = "spring")
public abstract class DuLieuBenhNhanMapper {

    @Mapping(target = "dsDichVuChiDinh", ignore = true)
    public abstract DuLieuBenhNhan toEntity(DuLieuBenhNhanDto duLieuBenhNhanDto);

    @Mappings({
        @Mapping(source = "mienCungCt", target = "mienCungCt", qualifiedByName = "mapMienCungCt"),
        @Mapping(source = "tenDkbd", target = "noiDkbd")
    })
    public abstract BenhNhanDto toBenhNhanDto(DuLieuBenhNhan duLieuBenhNhan);

    public int mapMienCungCt(Date mienCungCt) {
        return mienCungCt != null ? 1 : 0;
    }

}
