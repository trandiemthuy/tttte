package vn.vnpt.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import vn.vnpt.domain.DuLieuBenhNhan;
import vn.vnpt.service.constant.MaLyDoVvienEnum;
import vn.vnpt.service.dto.bangke.BangKeJasperParametters;
import vn.vnpt.service.utils.DateTimeUtils;

import java.util.Date;

/**
 * @author NguyenDucAnhKhoi
 */
@Mapper(componentModel = "spring")
public abstract class BangKeJasperParamettersMapper {

    private final int viTriCatChuoi = 135;

    @Mappings({
        @Mapping(source = "tenTinh", target = "soYteNganh", qualifiedByName = "mapSoYte"),
        @Mapping(source = "tenCskcb", target = "tenCskcb"),
        @Mapping(source = "tenKhoa", target = "tenKhoa"),
        @Mapping(source = "maKhoaBc", target = "maKhoaBc"),
        @Mapping(source = "maBn", target = "maSoNguoiBenh"),
        @Mapping(source = "maLk", target = "soKhamBenh"),
        @Mapping(source = "loaiKcb", target = "maLoaiKcb"),
        @Mapping(source = "loaiKcb", target = "tieuDeBangKe", qualifiedByName = "mapTitle"),
        @Mapping(source = "hoTen", target = "hoTenNguoiBenh"),
        @Mapping(source = "ngaySinh", target = "ngayThangNamSinh", qualifiedByName = "mapDate"),
        @Mapping(source = "gioiTinh", target = "gioiTinh"),
        @Mapping(source = "diaChi", target = "diaChi"),
        @Mapping(source = "maKhuVuc", target = "maKhuVuc"),
        @Mapping(source = "maThe", target = "maThe12", qualifiedByName = "splitMaThe12"),
        @Mapping(source = "maThe", target = "maThe3", qualifiedByName = "splitMaThe3"),
        @Mapping(source = "maThe", target = "maThe45", qualifiedByName = "splitMaThe45"),
        @Mapping(source = "maThe", target = "maThe615", qualifiedByName = "splitMaThe615"),
        @Mapping(source = "gtTheTu", target = "gtTheTu", qualifiedByName = "mapDate"),
        @Mapping(source = "gtTheDen", target = "gtTheDen", qualifiedByName = "mapDate"),
        @Mapping(source = "tenDkbd", target = "tenDkbd"),
        @Mapping(source = "maDkbd", target = "maDkbd"),
        @Mapping(source = "ngayVao", target = "gioPhutDenKham", qualifiedByName = "mapTimeFromDateTime"),
        @Mapping(source = "ngayVao", target = "ngayDenKham", qualifiedByName = "mapDateWFromDateTimeithPrefix"),
        @Mapping(source = "ngayKham", target = "gioPhutDtTu", qualifiedByName = "mapTimeFromDateTime"),
        @Mapping(source = "ngayKham", target = "ngayDtTu", qualifiedByName = "mapDateWFromDateTimeithPrefix"),
        @Mapping(source = "ngayRa", target = "gioPhutKt", qualifiedByName = "mapTimeFromDateTime"),
        @Mapping(source = "ngayRa", target = "ngayKt", qualifiedByName = "mapDateWFromDateTimeithPrefix"),
        @Mapping(source = "tinhTrangRv", target = "tinhTrangRaVien"),
        @Mapping(source = "maLyDoVvien", target = "capCuu", qualifiedByName = "mapLyDoVvienCapCuu"),
        @Mapping(source = "maLyDoVvien", target = "dungTuyen", qualifiedByName = "mapLyDoVvienDungTuyen"),
        @Mapping(source = "maLyDoVvien", target = "thongTuyen", qualifiedByName = "mapLyDoVvienThongTuyen"),
        @Mapping(source = "maLyDoVvien", target = "traiTuyen", qualifiedByName = "mapLyDoVvienTraiTuyen"),
        @Mapping(source = "", target = "maNoiChuyenDi"),
        @Mapping(source = "maNoiChuyen", target = "maNoiChuyenDen"),
        @Mapping(source = "tenBenh", target = "chanDoanXacDinh"),
        @Mapping(source = "maBenh", target = "maBenh"),
        @Mapping(source = "tenBenhKhac", target = "benhKemTheo", qualifiedByName = "mapChanDoanKemTheo"),
        @Mapping(source = "tenBenhKhac", target = "benhKemTheo2", qualifiedByName = "mapChanDoanKemTheo2"),
        @Mapping(source = "maBenhKhac", target = "maBenhKemTheo"),
        @Mapping(source = "ngayDu5nam", target = "thoiDiem5NamLienTuc", qualifiedByName = "mapDate"),
        @Mapping(source = "mienCungCt", target = "mienCungCtTu", qualifiedByName = "mapDate")
    })
    public abstract BangKeJasperParametters toParams(DuLieuBenhNhan duLieuBenhNhan);


    @Named("mapSoYte")
    public String mapSoYte(String tenTinh) {
        if (tenTinh == null || tenTinh.isEmpty())
            return "";
        return "SỞ Y TẾ " + tenTinh.toUpperCase();
    }

    @Named("mapTitle")
    public String mapTitle(Integer loaiKcb) {
        return "BẢNG KÊ CHI PHÍ " +
            (loaiKcb == 1 ? "KHÁM BỆNH" :
                loaiKcb  == 2 ? "ĐIỀU TRỊ NGOẠI TRÚ" :
                    loaiKcb  == 3 ? "ĐIỀU TRỊ NỘI TRÚ" :
                        "");
    }

    @Named("mapDateWFromDateTimeithPrefix")
    public String mapDateWFromDateTimeithPrefix(Date date) {
        return "ngày " + mapDateFormDateTime(date);
    }

    @Named("mapDate")
    public String mapDate(Date date) {
        return DateTimeUtils.toString(date, DateTimeUtils.VIEN_PHI_DATE_FORMAT);
    }

    @Named("mapDateFormDateTime")
    public String mapDateFormDateTime(Date date) {
        return DateTimeUtils.toString(date, DateTimeUtils.VIEN_PHI_DATE_FORMAT);
    }

    @Named("mapTimeFromDateTime")
    public String mapTimeFromDateTime(Date date) {
        String strValue = DateTimeUtils.toString(date, DateTimeUtils.B_4210_DATE_TIME_FORMAT);
        String hmValue = strValue.substring(8, 10) + ":" + strValue.substring(10, 12);
        return DateTimeUtils.readHourMinute(hmValue);
    }

    @Named("splitMaThe12")
    public String splitMaThe12(String maThe) {
        if (maThe == null || maThe.isEmpty())
            return "";
        return maThe.substring(0, 2);
    }

    @Named("splitMaThe3")
    public String splitMaThe3(String maThe) {
        if (maThe == null || maThe.isEmpty())
            return "";
        return maThe.substring(2, 3);
    }

    @Named("splitMaThe45")
    public String splitMaThe45(String maThe) {
        if (maThe == null || maThe.isEmpty())
            return "";
        return maThe.substring(3, 5);
    }

    @Named("splitMaThe615")
    public String splitMaThe615(String maThe) {
        if (maThe == null || maThe.isEmpty())
            return "";
        return maThe.substring(5);
    }

    @Named("mapLyDoVvienCapCuu")
    public String mapLyDoVvienCapCuu(Integer lyDo) {
      return MaLyDoVvienEnum.valueOf(lyDo) == MaLyDoVvienEnum.CAP_CUU ? "x" : "";
    }

    @Named("mapLyDoVvienDungTuyen")
    public String mapLyDoVvienDungTuyen(Integer lyDo) {
        return MaLyDoVvienEnum.valueOf(lyDo) == MaLyDoVvienEnum.DUNG_TUYEN ? "x" : "";
    }

    @Named("mapLyDoVvienThongTuyen")
    public String mapLyDoVvienThongTuyen(Integer lyDo) {
        return MaLyDoVvienEnum.valueOf(lyDo) == MaLyDoVvienEnum.THONG_TUYEN ? "x" : "";
    }

    @Named("mapLyDoVvienTraiTuyen")
    public String mapLyDoVvienTraiTuyen(Integer lyDo) {
        return MaLyDoVvienEnum.valueOf(lyDo) == MaLyDoVvienEnum.TRAI_TUYEN ? "x" : "";
    }

    @Named("mapChanDoanKemTheo")
    public String mapChanDoanKemTheo(String tenBenhKhac) {
        if (tenBenhKhac == null || tenBenhKhac.isEmpty())
            return "";
        if (tenBenhKhac.length() > viTriCatChuoi) {
            int p = tenBenhKhac.indexOf(" ", viTriCatChuoi);
            return tenBenhKhac.substring(0, p);
        }
        return tenBenhKhac;
    }

    @Named("mapChanDoanKemTheo2")
    public String mapChanDoanKemTheo2(String tenBenhKhac) {
        if (tenBenhKhac == null || tenBenhKhac.isEmpty())
            return "";
        if (tenBenhKhac.length() > viTriCatChuoi) {
            int p = tenBenhKhac.indexOf(" ", viTriCatChuoi);
            return tenBenhKhac.substring(p);
        }
        return "";
    }

}
