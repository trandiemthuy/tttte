package vn.vnpt.service.mapper;

import org.mapstruct.Mapper;
import vn.vnpt.domain.DuLieuBenhNhan;
import vn.vnpt.domain.VienPhiLanTamUng;
import vn.vnpt.domain.VienPhiLanTt;
import vn.vnpt.service.dto.BenhNhanDto;
import vn.vnpt.service.dto.LanKcbDto;
import vn.vnpt.service.dto.VienPhiLanTamUngDto;
import vn.vnpt.service.dto.VienPhiLanTtDto;

/**
 * @author NguyenDucAnhKhoi
 */
@Mapper(componentModel = "spring")
public abstract class LanKcbDtoMapper {

    public abstract LanKcbDto toDto(VienPhiLanTt vienPhiLanTt);

    public abstract LanKcbDto toDto(VienPhiLanTamUng vienPhiLanTamUng);

    public abstract LanKcbDto toDto(BenhNhanDto benhNhanDto);

    public abstract LanKcbDto toDto(DuLieuBenhNhan duLieuBenhNhan);

    public abstract LanKcbDto toDto(VienPhiLanTtDto vienPhiLanTtDto);

    public abstract LanKcbDto toDto(VienPhiLanTamUngDto vienPhiLanTamUngDto);

}
