package vn.vnpt.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import vn.vnpt.domain.BangKe;
import vn.vnpt.domain.VienPhiChiTietLanTt;
import vn.vnpt.service.constant.MaNhom4210Enum;
import vn.vnpt.service.dto.VienPhiChiTietLanTtDto;
import vn.vnpt.service.dto.bangke.BangKeDto;

/**
 * @author NguyenDucAnhKhoi
 */

@Mapper(componentModel = "spring")
public abstract class VienPhiChiTietLanTtMapper {

    @Mappings({
        @Mapping(source = "soPhieuMaDv", target = "soPhieuMaDv"),
        @Mapping(source = "maNhom4210", target = "nhomLanTt", qualifiedByName = "mapNhomLanTt"),
        @Mapping(source = "maNhom4210", target = "maNhom4210"),
        @Mapping(source = "noiDung", target = "noiDung"),
        @Mapping(source = "dvt", target = "dvt"),
        @Mapping(source = "soLuong", target = "soLuong"),
        @Mapping(source = "donGiaBv", target = "donGia"),
        @Mapping(source = "thanhTienBv", target = "thanhTien"),
        @Mapping(source = "quyBhyt", target = "quyBhyt"),
        @Mapping(source = "cungChiTra", target = "cungChiTra"),
        @Mapping(source = "nguoiBenh", target = "nguoiBenh"),
        @Mapping(source = "khac", target = "khac"),
        @Mapping(source = "tranTt", target = "tranTt"),
        @Mapping(target = "phaiTt", expression = "java(bangKe.getCungChiTra().add(bangKe.getNguoiBenh()))"),
        @Mapping(source = "ghiChu", target = "ghiChuChiTietLanTt"),
        @Mapping(source = "tenNhom", target = "tenNhomBangKe")
    })
    public abstract VienPhiChiTietLanTtDto toDto(BangKe bangKe);

    @Named("mapNhomLanTt")
    public String mapNhomLanTt(int maNhom4210) {
        MaNhom4210Enum maNhom4210Enum = MaNhom4210Enum.valueOf(maNhom4210);
        return maNhom4210Enum.toString();
    }

    public abstract VienPhiChiTietLanTt toEntity(VienPhiChiTietLanTtDto dto);

}
