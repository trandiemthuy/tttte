package vn.vnpt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.DuLieuBenhNhan;
import vn.vnpt.domain.TransactionLog;
import vn.vnpt.repository.DuLieuBenhNhanRepository;
import vn.vnpt.service.dto.BenhNhanDto;
import vn.vnpt.service.dto.DichVuChiDinhDto;
import vn.vnpt.service.dto.DuLieuBenhNhanDto;
import vn.vnpt.service.dto.LanKcbDto;
import vn.vnpt.service.mapper.DuLieuBenhNhanMapper;
import vn.vnpt.service.mapper.LanKcbDtoMapper;
import vn.vnpt.service.utils.DateTimeUtils;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * @author NguyenDucAnhKhoi
 */
@Service
public class DuLieuBenhNhanService {

    @Autowired
    DuLieuBenhNhanRepository duLieuBenhNhanRepository;

    @Autowired
    DuLieuBenhNhanMapper duLieuBenhNhanMapper;

    @Autowired
    LanKcbDtoMapper lanKcbDtoMapper;

    @Autowired
    DichVuChiDinhService dichVuChiDinhService;

    @Autowired
    TransactionLogService transactionLogService;

    @Transactional
    public DuLieuBenhNhan getDuLieuBenhNhan(LanKcbDto lanKcbDto) {
        long soVaoVien = lanKcbDto.getSoVaoVien();
        long soVaoVienDt = lanKcbDto.getSoVaoVienDt();
        int loaiKcb = lanKcbDto.getLoaiKcb();
        String dvtt = lanKcbDto.getDvtt();
        return duLieuBenhNhanRepository.findAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvtt(soVaoVien, soVaoVienDt, loaiKcb, dvtt)
            .orElseThrow(NoSuchElementException::new);
    }

    public String generateMaLk(DuLieuBenhNhanDto duLieuBenhNhanDto) {
        long year = Long.parseLong(DateTimeUtils.getYear(duLieuBenhNhanDto.getNgayVao()));
        long maBn = duLieuBenhNhanDto.getMaBn();
        long soVaoVien = duLieuBenhNhanDto.getSoVaoVien();
        long soVaoVienDt = duLieuBenhNhanDto.getSoVaoVienDt();
        long loaiKcb = duLieuBenhNhanDto.getLoaiKcb();

        long maxSttMaLk = duLieuBenhNhanRepository.getMaxSttMaLkByDvttAndYear(duLieuBenhNhanDto.getDvtt(), year);

        String pattern = "%d/%d.%d.%d.%d-%d";
        return String.format(pattern, maxSttMaLk + 1, year, maBn, loaiKcb, soVaoVien, soVaoVienDt);
    }

    @Transactional
    public DuLieuBenhNhan createDuLieuBenhNhan(DuLieuBenhNhanDto duLieuBenhNhanDto) {
        long soVaoVien = duLieuBenhNhanDto.getSoVaoVien();
        long soVaoVienDt = duLieuBenhNhanDto.getSoVaoVienDt();
        int loaiKcb = duLieuBenhNhanDto.getLoaiKcb();
        String dvtt = duLieuBenhNhanDto.getDvtt();
        if (duLieuBenhNhanRepository.findAllBySoVaoVienAndSoVaoVienDtAndLoaiKcbAndDvtt(soVaoVien, soVaoVienDt, loaiKcb, dvtt).isPresent()) {
            throw new IllegalStateException("Bộ khóa lần KCB này đã tồn tại trong hệ thống !");
        }

        DuLieuBenhNhan duLieuBenhNhan = duLieuBenhNhanMapper.toEntity(duLieuBenhNhanDto);
        TransactionLog transactionLog = transactionLogService.saveLog(duLieuBenhNhanDto);
        duLieuBenhNhan.setTransactionLog(transactionLog);
        duLieuBenhNhan.setMaLk(generateMaLk(duLieuBenhNhanDto));
        duLieuBenhNhanRepository.save(duLieuBenhNhan);
        List<DichVuChiDinhDto> dsDichVuChiDinh = duLieuBenhNhanDto.getDsDvChiDinh();
        dichVuChiDinhService.initDsDichVuChiDinh(dsDichVuChiDinh, duLieuBenhNhan);
        dichVuChiDinhService.afterUpdateDsDichVuChiDinh(soVaoVien, soVaoVienDt, loaiKcb, dvtt);
        return duLieuBenhNhan;
    }

    @Transactional
    public void updateSoLanThanhToanDuLieuBenhNhan(DuLieuBenhNhan duLieuBenhNhan, int soLanThanhToan) {
        duLieuBenhNhan.setTtThanhToan(soLanThanhToan);
        duLieuBenhNhanRepository.save(duLieuBenhNhan);
    };

    @Transactional
    public void updateSoLanTamUngDuLieuBenhNhan(DuLieuBenhNhan duLieuBenhNhan, int soLanTamUng) {
        duLieuBenhNhan.setTtTamUng(soLanTamUng);
        duLieuBenhNhanRepository.save(duLieuBenhNhan);
    };

    @Transactional
    public void updateInBangKe(DuLieuBenhNhan duLieuBenhNhan) {
        int soLanInBangKe = duLieuBenhNhan.getTtInBangKe() == null ? 0 : duLieuBenhNhan.getTtInBangKe();
        duLieuBenhNhan.setTtInBangKe(soLanInBangKe + 1);
        duLieuBenhNhanRepository.save(duLieuBenhNhan);
    }

    @Transactional
    public List<BenhNhanDto> dsBnThuTien(Date tuNgay, Date denNgay, int loaiKcb, String dvtt, String maKhoa, int coBhyt, int daThanhToan, int daTamUng) {
        List<DuLieuBenhNhan> dsDuLieuBenhNhan = duLieuBenhNhanRepository.getDanhSachBenhNhan(tuNgay, denNgay, loaiKcb, dvtt);
        return dsDuLieuBenhNhan.parallelStream()
            .filter(bn -> maKhoa.equals("-1") || maKhoa.equals(bn.getMaKhoa()))
            .filter(bn -> coBhyt == -1 || coBhyt == bn.getCoBhyt())
            .filter(bn -> daThanhToan == -1 || daThanhToan == bn.getTtThanhToan())
            .filter(bn -> daTamUng == -1 || daTamUng == bn.getTtTamUng())
            .map(duLieuBenhNhan -> duLieuBenhNhanMapper.toBenhNhanDto(duLieuBenhNhan))
            .collect(Collectors.toList());
    }

    @Transactional
    public BenhNhanDto searchBenhNhan(Date tuNgay, Date denNgay, int loaiKcb, String dvtt, long maBn) {
        return duLieuBenhNhanRepository.timBenhNhan(tuNgay, denNgay, loaiKcb, dvtt, maBn)
            .map(lieuBenhNhan -> duLieuBenhNhanMapper.toBenhNhanDto(lieuBenhNhan))
            .orElse(null);
    }

    public List<BenhNhanDto> dsbnHoanTatKham(Date tuNgay, Date denNgay, int loaiKcb, String dvtt, String maKhoa, int coBhyt, int daInBangKe) {
        return this.dsDuLieuBenhNhanHoanTatKham(tuNgay, denNgay, loaiKcb, dvtt)
            .parallelStream()
            .filter(e -> maKhoa.equals("-1") || maKhoa.equals(e.getMaKhoa()))
            .filter(e -> coBhyt == -1 || coBhyt == e.getCoBhyt())
            .filter(e -> daInBangKe == -1 || daInBangKe == e.getTtInBangKe())
            .map(duLieuBenhNhan -> duLieuBenhNhanMapper.toBenhNhanDto(duLieuBenhNhan))
            .collect(Collectors.toList());
    }

    public List<DuLieuBenhNhan> dsDuLieuBenhNhanHoanTatKham(Date tuNgay, Date denNgay, int loaiKcb, String dvtt) {
        return duLieuBenhNhanRepository.getDanhSachBenhNhanHoanTatKham(tuNgay, denNgay, loaiKcb, dvtt);
    }
}
