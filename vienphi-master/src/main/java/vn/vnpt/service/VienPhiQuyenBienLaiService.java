package vn.vnpt.service;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.VienPhiQuyenBienLai;
import vn.vnpt.repository.VienPhiQuyenBienLaiRepository;
import vn.vnpt.service.constant.TrangThaiQuyenBienLaiEnum;
import vn.vnpt.service.utils.GeneralUtils;
import vn.vnpt.web.rest.errors.BadRequestAlertException;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class VienPhiQuyenBienLaiService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String ENTITY_NAME = "VienPhiQuyenBienLai";

    @Autowired
    VienPhiQuyenBienLaiRepository vienPhiQuyenBienLaiRepository;

    @Transactional
    public VienPhiQuyenBienLai getQuyenBienLai(long maQuyenBienLai) {
        return vienPhiQuyenBienLaiRepository.findById(maQuyenBienLai)
            .orElseThrow(NoSuchElementException::new);
    }

    @Transactional
    public List<VienPhiQuyenBienLai> getDsQuyenBienLaiThuTien(String dvtt, long nhanVien, int ngoaiTru, int tamUng, int bhyt) {
        return vienPhiQuyenBienLaiRepository.findAllByDvttAndTrangThaiOrderByTenQuyenBienLai(dvtt, TrangThaiQuyenBienLaiEnum.HOAT_DONG.getValue())
            .parallelStream()
            .filter(e ->
                /*Lọc theo nhân viên*/
                (e.getNhanVien() == -1 || e.getNhanVien() == nhanVien)
                /*Lọc theo loại kcb */
                && (e.getNgoaiTru() == -1 || e.getNgoaiTru() == ngoaiTru)
                /*Lọc theo nghiệp vụ */
                && (e.getTamUng() == -1 || e.getTamUng() == tamUng)
                /*Lọc theo bhyt */
                && (e.getBhyt() == -1 || e.getBhyt() == bhyt)
            )
            .collect(Collectors.toList());
    }

    @Transactional
    public VienPhiQuyenBienLai createQuyenBienLai(VienPhiQuyenBienLai vienPhiQuyenBienLai) {
        if (vienPhiQuyenBienLai.getMaQuyenBienLai() != null) {
            throw new BadRequestAlertException("Id must be empty !", ENTITY_NAME, "idexits");
        }
        vienPhiQuyenBienLai.setNgayGioTao(new Date());
        return vienPhiQuyenBienLaiRepository.save(vienPhiQuyenBienLai);
    }

    @Transactional
    public VienPhiQuyenBienLai updateQuyenBienLai(VienPhiQuyenBienLai vienPhiQuyenBienLai) {
        if (vienPhiQuyenBienLaiRepository.findById(vienPhiQuyenBienLai.getMaQuyenBienLai()).isPresent()) {
            return vienPhiQuyenBienLaiRepository.save(vienPhiQuyenBienLai);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    @Transactional
    public VienPhiQuyenBienLai updateQuyenBienLai(Map<String, Object> dto) {
        long key = Long.parseLong(dto.get("key").toString());
        Map<String, Object> values = (Map) dto.get("values");

        VienPhiQuyenBienLai vienPhiQuyenBienLai = vienPhiQuyenBienLaiRepository.findById(key)
            .orElseThrow(NoSuchElementException::new);
        ModelMapper modelMapper = new ModelMapper();
        Map<String, Object> map = GeneralUtils.convertToMap(vienPhiQuyenBienLai);
        values.keySet().forEach(e -> {
            map.put(e, values.get(e));
        });
        vienPhiQuyenBienLai = modelMapper.map(map, VienPhiQuyenBienLai.class);
        vienPhiQuyenBienLaiRepository.save(vienPhiQuyenBienLai);
        return vienPhiQuyenBienLai;
    }

    @Transactional
    public void disableQuyenBienLai(long maQuyenBienLai) {
        VienPhiQuyenBienLai vienPhiQuyenBienLai = this.getQuyenBienLai(maQuyenBienLai);
        vienPhiQuyenBienLai.setTrangThai(TrangThaiQuyenBienLaiEnum.VO_HIEU.getValue());
        vienPhiQuyenBienLaiRepository.save(vienPhiQuyenBienLai);
    }

    @Transactional
    public List<VienPhiQuyenBienLai> getListQuyenBienLai(String dvtt) {
        return vienPhiQuyenBienLaiRepository.findAllByDvttOrderByTenQuyenBienLai(dvtt);
    }

}
