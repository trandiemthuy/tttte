package vn.vnpt.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.BangKe;
import vn.vnpt.domain.VienPhiChiTietLanTt;
import vn.vnpt.domain.VienPhiLanTt;
import vn.vnpt.repository.VienPhiChiTietLanTtRepository;
import vn.vnpt.service.dto.LanKcbDto;
import vn.vnpt.service.dto.VienPhiChiTietLanTtDto;
import vn.vnpt.service.dto.bangke.BangKeDto;
import vn.vnpt.service.mapper.VienPhiChiTietLanTtMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author NguyenDucAnhKhoi
 */
@Service
public class VienPhiChiTietLanTtService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String ENTITY_NAME = "VienPhiChiTietLanTt";

    @Autowired
    BangKeServices bangKeServices;

    @Autowired
    VienPhiLanTtService vienPhiLanTtService;

    @Autowired
    VienPhiChiTietLanTtRepository vienPhiChiTietLanTtRepository;

    @Autowired
    VienPhiChiTietLanTtMapper vienPhiChiTietLanTtMapper;

    @Transactional
    public List<VienPhiChiTietLanTt> getDaThanhToan(LanKcbDto lanKcbDto) {
        List<VienPhiLanTt> listVienPhiLanTt = vienPhiLanTtService.getListVienPhiLanTt(lanKcbDto);
        List<VienPhiChiTietLanTt> result = new ArrayList<>();
        listVienPhiLanTt.forEach(e -> {
            result.addAll(e.getListVienPhiChiTietLanTt());
        });
        return result;
    }

    @Transactional
    public List<VienPhiChiTietLanTtDto> getChuaThanhToan(LanKcbDto lanKcbDto) {
        return bangKeServices.getBangKe(lanKcbDto)
            .stream()
            .filter(e -> e.getTtThanhToan() == 0)
            .map(e -> vienPhiChiTietLanTtMapper.toDto(e))
            .collect(Collectors.toList());
    }

    @Transactional
    public void saveAll(List<VienPhiChiTietLanTt> listVienPhiChiTietLanTt, VienPhiLanTt vienPhiLanTt) {
        AtomicInteger sttChiTietLanTt = new AtomicInteger(1);
        vienPhiChiTietLanTtRepository.saveAll(
            listVienPhiChiTietLanTt.stream()
                .peek(e -> {
                    e.setVienPhiLanTt(vienPhiLanTt);
                    e.setSttChiTietLanTt(vienPhiLanTt.getSttLanTt() + "_" + sttChiTietLanTt.getAndIncrement());
                })
                .collect(Collectors.toList())
        );
    }

}
