package vn.vnpt.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.TransactionLog;
import vn.vnpt.repository.TransactionLogRepository;
import vn.vnpt.service.dto.DuLieuBenhNhanDto;
import vn.vnpt.service.utils.DateTimeUtils;

import java.util.NoSuchElementException;

/**
 * @author NguyenDucAnhKhoi
 */
@Service
public class TransactionLogService {

    @Autowired
    TransactionLogRepository transactionLogRepository;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String ENTITY_NAME = "TransactionLog";

    public TransactionLog findById(long id) {
        return transactionLogRepository.findById(id)
            .orElseThrow(() -> {
                String message = "No Transaction log found with this id !";
                logger.error(message);
                return new NoSuchElementException(message);
            });
    }

    @Transactional
    public TransactionLog saveLog(DuLieuBenhNhanDto duLieuBenhNhanDto) {
        TransactionLog transactionLog = new TransactionLog();
        transactionLogRepository.save(transactionLog);
        logger.debug("Stored data log to TransactionLog with ID: " + transactionLog.getIdTransaction());
        return transactionLog;
    }

}
