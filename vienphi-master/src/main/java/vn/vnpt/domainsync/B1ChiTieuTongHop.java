package vn.vnpt.domainsync;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "BSGD_SYNC", name = "B1_CHITIEUTONGHOP",
    uniqueConstraints = @UniqueConstraint(columnNames = {"MA_LK", "SOVAOVIEN", "SOVAOVIEN_DT", "LOAIKCB", "DVTT"}),
    indexes = {
        @Index(columnList = "SOVAOVIEN, SOVAOVIEN_DT, LOAIKCB, DVTT")
    }
)
public class B1ChiTieuTongHop implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "MA_LK", length = 100)
    private String maLk;

    @Column(name = "MA_BN", length = 100)
    private String maBn;

    @Column(name = "HO_TEN", length = 255)
    private String hoTen;

    @Temporal(TemporalType.DATE)
    @Column(name = "NGAY_SINH")
    private Date ngaySinh;

    @Column(name = "GIOI_TINH", columnDefinition = "NUMBER(1)")
    private int gioiTinh;

    @Column(name = "DIA_CHI", length = 1024)
    private String diaChi;

    @Column(name = "MA_THE", length = 15)
    private String maThe;

    @Column(name = "MA_DKBD", length = 50)
    private String maDkbd;

    @Temporal(TemporalType.DATE)
    @Column(name = "GT_THE_TU")
    private Date gtTheTu;

    @Temporal(TemporalType.DATE)
    @Column(name = "GT_THE_DEN")
    private Date gtTheDen;

    @Temporal(TemporalType.DATE)
    @Column(name = "MIEN_CUNG_CT")
    private Date mienCungCt;

    @Column(name = "TEN_BENH", length = 500)
    private String tenBenh;

    @Column(name = "MA_BENH", length = 15)
    private String maBenh;

    @Column(name = "MA_BENHKHAC", length = 255)
    private String maBenhKhac;

    @Column(name = "MA_LYDO_VVIEN", columnDefinition = "NUMBER(1)")
    private int maLyDoVvien;

    @Column(name = "MA_NOI_CHUYEN", length = 5)
    private String maNoiChuyen;

    @Column(name = "MA_TAI_NAN", columnDefinition = "NUMBER(1)")
    private int maTaiNan;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_VAO")
    private Date ngayVao;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_RA")
    private Date ngayRa;

    @Column(name = "SO_NGAY_DTRI", columnDefinition = "NUMBER(3)")
    private int soNgayDTri;

    @Column(name = "KET_QUA_DTRI", columnDefinition = "NUMBER(1)")
    private int ketQuaDtri;

    @Column(name = "TINH_TRANG_RV", columnDefinition = "NUMBER(1)")
    private int tinhTrangRv;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_TTOAN", length = 12)
    private Date ngayTtoan;

    @Column(name = "T_THUOC", precision = 15, scale = 4)
    private BigDecimal tThuoc;

    @Column(name = "T_VTYT", precision = 15, scale = 4)
    private BigDecimal tVtyt;

    @Column(name = "T_TONGCHI", precision = 15, scale = 4)
    private BigDecimal tTongChi;

    @Column(name = "T_BNTT", precision = 15, scale = 4)
    private BigDecimal tBntt;

    @Column(name = "T_BNCCT", precision = 15, scale = 4)
    private BigDecimal tBncct;

    @Column(name = "T_BHTT", precision = 15, scale = 4)
    private BigDecimal tBhtt;

    @Column(name = "T_NGUONKHAC", precision = 15, scale = 4)
    private BigDecimal tNguonKhac;

    @Column(name = "T_NGOAIDS", precision = 15, scale = 4)
    private BigDecimal tNgoaiDs;

    @Column(name = "NAM_QT", columnDefinition = "NUMBER(4)")
    private int namQt;

    @Column(name = "THANG_QT", columnDefinition = "NUMBER(2)")
    private int thangQt;

    @Column(name = "MA_LOAI_KCB", columnDefinition = "NUMBER(1)")
    private int maLoaiKcb;

    @Column(name = "MA_KHOA", precision = 15)
    private String maKhoa;

    @Column(name = "MA_CSKCB", precision = 5)
    private String maCskcb;

    @Column(name = "MA_KHUVUC", length = 2)
    private String maKhuVuc;

    @Column(name = "MA_PTTT_QT", length = 255)
    private String maPtttQt;

    @Column(name = "CAN_NANG", precision = 5, scale = 4)
    private BigDecimal canNang;

    @Column(name = "ID_TRANSACTION")
    private long idTransaction;

    @Column(name = "SOVAOVIEN")
    private long soVaoVien;

    @Column(name = "SOVAOVIEN_DT")
    private long soVaoVienDt;

    @Column(name = "LOAIKCB", columnDefinition = "NUMBER(1)")
    private int loaiKcb;

    @Column(name = "DVTT", length = 50)
    private String dvtt;

    @Column(name = "TT_PHIEU", columnDefinition = "NUMBER(1)")
    private int ttPhieu;

    @Column(name = "CHOT_BAOCAO", columnDefinition = "NUMBER(1)")
    private int chotBaoCao;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "ID_B1")
    private long idB1;

}
