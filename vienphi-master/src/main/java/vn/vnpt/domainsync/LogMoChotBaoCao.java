package vn.vnpt.domainsync;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "BSGD_SYNC", name = "LOG_MOCHOTBAOCAO")
public class LogMoChotBaoCao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "ID_MOCHOT")
    private long idMoChot;

    @Column(name = "THOIGIAN_MOCHOT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date thoiGianMoChot;

    @Column(name = "NHANVIEN_MOCHOT", length = 50)
    private String nhanVienMoChot;

    @Column(name = "OBJECT_ID", length = 100)
    private String objectId;

}
