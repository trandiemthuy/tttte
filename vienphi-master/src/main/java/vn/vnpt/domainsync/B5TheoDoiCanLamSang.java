package vn.vnpt.domainsync;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "BSGD_SYNC", name = "B5_THEODOICANLAMSANG",
    indexes = {
        @Index(columnList = "SOVAOVIEN, SOVAOVIEN_DT, LOAIKCB, DVTT")
    })
public class B5TheoDoiCanLamSang implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "DIEN_BIEN", length = 1024)
    private String dienBien;

    @Column(name = "HOI_CHAN", length = 500)
    private String hoiChan;

    @Column(name = "PHAU_THUAT", length = 1024)
    private String phauThuat;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_YL")
    private Date ngayYl;

    @Column(name = "ID_TRANSACTION")
    private long idTransaction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "MA_LK", referencedColumnName = "MA_LK"),
        @JoinColumn(name = "SOVAOVIEN", referencedColumnName = "SOVAOVIEN"),
        @JoinColumn(name = "SOVAOVIEN_DT", referencedColumnName = "SOVAOVIEN_DT"),
        @JoinColumn(name = "LOAIKCB", referencedColumnName = "LOAIKCB"),
        @JoinColumn(name = "DVTT", referencedColumnName = "DVTT"),
    })
    private B1ChiTieuTongHop b1ChiTieuTongHop;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "ID_B5")
    private long idB5;

}
