package vn.vnpt.domainsync;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "BSGD_SYNC", name = "B4_KETQUACANLAMSANG",
    indexes = {
        @Index(columnList = "SOVAOVIEN, SOVAOVIEN_DT, LOAIKCB, DVTT")
    })
public class B4KetQuaCanLamSang implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "MA_DICH_VU", length = 15)
    private String maDichVu;

    @Column(name = "MA_CHI_SO", length = 50)
    private String maChiSo;

    @Column(name = "TEN_CHI_SO", length = 255)
    private String tenChiSo;

    @Column(name = "GIA_TRI", length = 50)
    private String giaTri;

    @Column(name = "MA_MAY", length = 50)
    private String maMay;

    @Column(name = "MO_TA", length = 1024)
    private String moTa;

    @Column(name = "KET_LUAN", length = 1024)
    private String ketLaun;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_KQ")
    private Date ngayKq;

    @Column(name = "ID_TRANSACTION")
    private long idTransaction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "MA_LK", referencedColumnName = "MA_LK"),
        @JoinColumn(name = "SOVAOVIEN", referencedColumnName = "SOVAOVIEN"),
        @JoinColumn(name = "SOVAOVIEN_DT", referencedColumnName = "SOVAOVIEN_DT"),
        @JoinColumn(name = "LOAIKCB", referencedColumnName = "LOAIKCB"),
        @JoinColumn(name = "DVTT", referencedColumnName = "DVTT"),
    })
    private B1ChiTieuTongHop b1ChiTieuTongHop;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "ID_B4")
    private long idB4;

}
