package vn.vnpt.domainsync;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "BSGD_SYNC", name = "B3_KYTHUATVATTU",
    indexes = {
        @Index(columnList = "SOVAOVIEN, SOVAOVIEN_DT, LOAIKCB, DVTT")
    })
public class B3KyThuatVatTu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "MA_DICH_VU", length = 20)
    private String maDichVu;

    @Column(name = "MA_VAT_TU", length = 255)
    private String maVatTu;

    @Column(name = "MA_NHOM", columnDefinition = "NUMBER(2)")
    private int maNhom;

    @Column(name = "GOI_VTYT", length = 2)
    private String goiVtyt;

    @Column(name = "TEN_VAT_TU", length = 1024)
    private String tenVatTu;

    @Column(name = "TEN_DICH_VU", length = 1024)
    private String tenDichVu;

    @Column(name = "DON_VI_TINH", length = 50)
    private String donViTinh;

    @Column(name = "PHAM_VI", columnDefinition = "NUMBER(1)")
    private int phamVi;

    @Column(name = "SO_LUONG", precision = 10, scale = 4)
    private BigDecimal soLuong;

    @Column(name = "DON_GIA", precision = 15, scale = 4)
    private BigDecimal donGia;

    @Column(name = "TT_THAU", length = 50)
    private String ttThau;

    @Column(name = "TYLE_TT", precision = 3)
    private BigDecimal tyLeTt;

    @Column(name = "THANH_TIEN", precision = 15, scale = 4)
    private BigDecimal thanhTien;

    @Column(name = "T_TRANTT", precision = 15, scale = 4)
    private BigDecimal tTranTt;

    @Column(name = "MUC_HUONG", precision = 3)
    private BigDecimal mucHuong;

    @Column(name = "T_NGUONKHAC", precision = 15, scale = 4)
    private BigDecimal tNguonKhac;

    @Column(name = "T_BNTT", precision = 15, scale = 4)
    private BigDecimal tBntt;

    @Column(name = "T_BHTT", precision = 15, scale = 4)
    private BigDecimal tBhtt;

    @Column(name = "T_BNCCT", precision = 15, scale = 4)
    private BigDecimal tBncct;

    @Column(name = "T_NGOAIDS", precision = 15, scale = 4)
    private BigDecimal tNgoaiDs;

    @Column(name = "MA_KHOA", length = 15)
    private String maKhoa;

    @Column(name = "MA_GIUONG", length = 14)
    private String maGiuong;

    @Column(name = "MA_BAC_SI", length = 255)
    private String maBacSi;

    @Column(name = "MA_BENH", length = 255)
    private String maBenh;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_YL")
    private Date ngayYl;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NGAY_KQ")
    private Date ngayKq;

    @Column(name = "MA_PTTT", columnDefinition = "NUMBER(1)")
    private int maPttt;

    @Column(name = "SOPHIEU_MADV", length = 50)
    private String soPhieuMaDv;

    @Column(name = "ID_TRANSACTION")
    private long idTransaction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "MA_LK", referencedColumnName = "MA_LK"),
        @JoinColumn(name = "SOVAOVIEN", referencedColumnName = "SOVAOVIEN"),
        @JoinColumn(name = "SOVAOVIEN_DT", referencedColumnName = "SOVAOVIEN_DT"),
        @JoinColumn(name = "LOAIKCB", referencedColumnName = "LOAIKCB"),
        @JoinColumn(name = "DVTT", referencedColumnName = "DVTT"),
    })
    private B1ChiTieuTongHop b1ChiTieuTongHop;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "ID_B3")
    private long idB3;
}
