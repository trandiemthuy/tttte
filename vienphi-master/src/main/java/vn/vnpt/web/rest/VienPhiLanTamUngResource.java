package vn.vnpt.web.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.vnpt.domain.VienPhiLanTamUng;
import vn.vnpt.service.VienPhiLanTamUngService;
import vn.vnpt.service.dto.LanKcbDto;
import vn.vnpt.service.dto.VienPhiLanTamUngDto;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/vien-phi/lan-tam-ung")
public class VienPhiLanTamUngResource {

    @Autowired
    VienPhiLanTamUngService vienPhiLanTamUngService;

    @ApiOperation(value = "Thu tạm ứng")
    @PostMapping("/")
        public ResponseEntity<VienPhiLanTamUng> createLanTamUng(@RequestBody VienPhiLanTamUngDto vienPhiLanTamUngDto) throws URISyntaxException {
        VienPhiLanTamUng result = vienPhiLanTamUngService.createLanTamUng(vienPhiLanTamUngDto);
        return ResponseEntity.created(new URI("/api/vien-phi/lan-tam-ung/" + result.getIdLanTamUng()))
            .body(result);
    }

    @ApiOperation(value = "Get thông tin lần tạm ứng")
    @GetMapping("/{idLanTamUng}")
    public ResponseEntity<VienPhiLanTamUng> getLanTamUng(@PathVariable(value = "idLanTamUng") long idLanTamUng){
        VienPhiLanTamUng result = vienPhiLanTamUngService.getLanTamUng(idLanTamUng);
        return ResponseEntity.ok()
            .body(result);
    }

    @ApiOperation(value = "Hủy tạm ứng")
    @DeleteMapping
    public ResponseEntity<Object> deleteLanTamUng(
        @RequestParam(value = "soVaoVien") long soVaoVien,
        @RequestParam(value = "soVaoVienDt") long soVaoVienDt,
        @RequestParam(value = "loaiKcb") int loaiKcb,
        @RequestParam(value = "dvtt") String dvtt,
        @RequestParam(value = "sttLanTamUng") String sttLanTamUng
    ) {
        vienPhiLanTamUngService.deleteLanTamUng(soVaoVien, soVaoVienDt, loaiKcb, dvtt, sttLanTamUng);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Get danh sách các lần tạm ứng của bệnh nhân")
    @GetMapping("/get-list")
    public ResponseEntity<List> getListLanTamUng(LanKcbDto lanKcbDto) {
        List<VienPhiLanTamUng> result = vienPhiLanTamUngService.getListLanTamUng(lanKcbDto);
        return ResponseEntity.ok()
            .body(result);
    }

}
