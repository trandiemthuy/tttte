package vn.vnpt.web.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import vn.vnpt.service.BangKeServices;
import vn.vnpt.service.constant.ReportTypeEnum;
import vn.vnpt.service.dto.LanKcbDto;
import vn.vnpt.service.dto.bangke.BangKeDto;
import vn.vnpt.service.dto.bangke.BangKeJasperParametters;
import vn.vnpt.service.dto.bangke.BangKeTongTienDto;
import vn.vnpt.service.dto.bangke.TaoBangKeHangLoatDto;
import vn.vnpt.service.utils.DateTimeUtils;
import vn.vnpt.service.utils.ReadNumberUtils;
import vn.vnpt.service.utils.GeneralUtils;
import vn.vnpt.service.utils.ReportUtils;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/bang-ke")
public class BangKeResource {

    @Autowired
    BangKeServices bangKeServices;

    @ApiOperation(value = "Tạo số liệu bảng kê")
    @PostMapping("/")
    public ResponseEntity createBangKe(@RequestBody LanKcbDto lanKcbDto) throws URISyntaxException {
        bangKeServices.createBangKe(lanKcbDto);
        URI createdAt = UriComponentsBuilder.fromUriString("/api/bang-ke")
            .queryParam("soVaoVien", lanKcbDto.getSoVaoVien())
            .queryParam("soVaoVienDt", lanKcbDto.getSoVaoVienDt())
            .queryParam("loaiKcb", lanKcbDto.getLoaiKcb())
            .queryParam("dvtt", lanKcbDto.getDvtt())
            .build()
            .toUri();
        return ResponseEntity.created(createdAt)
            .build();
    }

    @ApiOperation(value = "Tạo số liệu hàng bảng kê hàng loạt")
    @PostMapping("/tao-so-lieu-hang-loat")
    public ResponseEntity<Integer> createBangKeHangLoat(@RequestBody TaoBangKeHangLoatDto taoBangKeHangLoatDto) {
        int result = bangKeServices.createBangKeHangLoat(taoBangKeHangLoatDto);
        return ResponseEntity.ok()
            .body(result);
    }

    @ApiOperation(value = "Get bảng kê")
    @GetMapping
    public ResponseEntity<List> getBangKe(LanKcbDto lanKcbDto) {
        List<BangKeDto> result = bangKeServices.getBangKeDto(lanKcbDto);
        return ResponseEntity.ok()
            .body(result);
    }

    @ApiOperation(value = "In bảng kê")
    @GetMapping(value = "/print")
    public ResponseEntity<ByteArrayResource> printBangKe(
        @RequestParam(value = "soVaoVien") long soVaoVien,
        @RequestParam(value = "soVaoVienDt") long soVaoVienDt,
        @RequestParam(value = "loaiKcb") int loaiKcb,
        @RequestParam(value = "dvtt") String dvtt,
        @RequestParam(value = "kieuFile", required = false, defaultValue = "0") int kieuFile
    ) {
        String reportPath = "report/bangke/BangKeChiPhi6556.jasper";

        BangKeJasperParametters bangKeJasperParametters = bangKeServices.getBangKeJasperParametters(soVaoVien, soVaoVienDt, loaiKcb, dvtt);
        List<BangKeDto> dsBangKe = bangKeServices.getBangKeDto(new LanKcbDto(soVaoVien, soVaoVienDt, loaiKcb, dvtt));

        BangKeTongTienDto bangKeTongTienDto = new BangKeTongTienDto(dsBangKe);
        BigDecimal tongChiPhiBangSo =  bangKeTongTienDto.getTongThanhTienBv();
        BigDecimal quyBhytBangSo =  bangKeTongTienDto.getTongQuyBhyt();
        BigDecimal nguoiBenhTraBangSo =  bangKeTongTienDto.getTongNguoiBenh().add(bangKeTongTienDto.getTongCungChiTra());
        BigDecimal cungChiTraBangSo =  bangKeTongTienDto.getTongCungChiTra();
        BigDecimal nguoiBenhTraKhacBangSo = bangKeTongTienDto.getTongNguoiBenh();
        BigDecimal nguonKhacBangSo = bangKeTongTienDto.getTongKhac();

        bangKeJasperParametters.setTongChiPhiBangChu(ReadNumberUtils.readCurrency(tongChiPhiBangSo));
        bangKeJasperParametters.setQuyBhytBangChu(ReadNumberUtils.readCurrency(quyBhytBangSo));
        bangKeJasperParametters.setNguoiBenhTraBangChu(ReadNumberUtils.readCurrency(nguoiBenhTraBangSo));
        bangKeJasperParametters.setCungChiTraBangChu(ReadNumberUtils.readCurrency(cungChiTraBangSo));
        bangKeJasperParametters.setNguoiBenhTraKhacBangChu(ReadNumberUtils.readCurrency(nguoiBenhTraKhacBangSo));
        bangKeJasperParametters.setNguonKhacBangChu(ReadNumberUtils.readCurrency(nguonKhacBangSo));

        bangKeJasperParametters.setNgayLapBangKe(DateTimeUtils.readDate(DateTimeUtils.getCurrentDate()));

        Map<String, Object> parametters = GeneralUtils.convertToMap(bangKeJasperParametters);
        return ReportUtils.responseEntityReport(reportPath, parametters, dsBangKe, ReportTypeEnum.valueOf(kieuFile));
    }

}
