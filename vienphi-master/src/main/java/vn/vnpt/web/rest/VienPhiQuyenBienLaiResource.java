package vn.vnpt.web.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.vnpt.domain.VienPhiQuyenBienLai;
import vn.vnpt.service.VienPhiQuyenBienLaiService;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/vien-phi/quyen-bien-lai")
public class VienPhiQuyenBienLaiResource {

    @Autowired
    VienPhiQuyenBienLaiService vienPhiQuyenBienLaiService;

    @ApiOperation(value = "Lấy danh sách quyển biên lai trong form thu tiền")
    @GetMapping("/theo-nghiep-vu")
    public ResponseEntity<List> getDsQuyenBienLaiThuTien(
        @RequestParam(value = "dvtt") String dvtt,
        @RequestParam(value = "nhanVien") long nhanVien,
        @RequestParam(value = "ngoaiTru")  int ngoaiTru,
        @RequestParam(value = "tamUng") int tamUng,
        @RequestParam(value = "bhyt") int bhyt
    ) {
        List<VienPhiQuyenBienLai> result = vienPhiQuyenBienLaiService.getDsQuyenBienLaiThuTien(dvtt, nhanVien, ngoaiTru, tamUng, bhyt);
        return ResponseEntity.ok()
            .body(result);
    }

    @ApiOperation(value = "Thêm quyển biên lai")
    @PostMapping("/")
    public ResponseEntity<VienPhiQuyenBienLai> createQuyenBienLai(@RequestBody VienPhiQuyenBienLai vienPhiQuyenBienLai) throws URISyntaxException {
        VienPhiQuyenBienLai result = vienPhiQuyenBienLaiService.createQuyenBienLai(vienPhiQuyenBienLai);
        return ResponseEntity.created(new URI("/api/quyenbienlai/" + result.getMaQuyenBienLai()))
            .body(result);
    }

    @ApiOperation(value = "Chỉnh sửa quyển biên lai")
    @PutMapping("/")
    public ResponseEntity<VienPhiQuyenBienLai> updateQuyenBienLai(@RequestBody VienPhiQuyenBienLai vienPhiQuyenBienLai) {
        VienPhiQuyenBienLai result = vienPhiQuyenBienLaiService.updateQuyenBienLai(vienPhiQuyenBienLai);
        return ResponseEntity.ok()
            .body(result);
    }

    @ApiOperation(value = "Chỉnh sửa quyển biên lai (1 hoặc nhiều trường)")
    @PutMapping("/edit")
    public ResponseEntity<VienPhiQuyenBienLai> updateQuyenBienLai(@RequestBody Map<String, Object> dto) {
        VienPhiQuyenBienLai result = vienPhiQuyenBienLaiService.updateQuyenBienLai(dto);
        return ResponseEntity.ok()
            .body(result);
    }

    @ApiOperation(value = "Vô hiệu quyển biên lai")
    @DeleteMapping("/{maQuyenBienLai}")
    public ResponseEntity<VienPhiQuyenBienLai> disableQuyenBienLai(@PathVariable(value = "maQuyenBienLai") long maQuyenBienLai) {
        vienPhiQuyenBienLaiService.disableQuyenBienLai(maQuyenBienLai);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Lấy danh sách quyển biên lai theo đơn vị")
    @GetMapping("/")
    public ResponseEntity<List> getListQuyenBienLai(@RequestParam(value = "dvtt") String dvtt) {
        List<VienPhiQuyenBienLai> result = vienPhiQuyenBienLaiService.getListQuyenBienLai(dvtt);
        return ResponseEntity.ok()
            .body(result);
    }

}
