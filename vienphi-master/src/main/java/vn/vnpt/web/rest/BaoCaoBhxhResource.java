package vn.vnpt.web.rest;

import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.vnpt.service.BaoCaoBhxhService;
import vn.vnpt.service.constant.ReportMediaType;
import vn.vnpt.service.constant.ReportTypeEnum;
import vn.vnpt.service.utils.ReportUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author NguyenDucAnhKhoi
 */
@RestController
@RequestMapping("/api/bao-cao/bhxh")
public class BaoCaoBhxhResource {

    @Autowired
    BaoCaoBhxhService baoCaoBhxhService;

    @GetMapping("/test")
    public ResponseEntity<ByteArrayResource> testReport(@RequestParam(value = "type", required = false, defaultValue = "-1") int type) {
        String path = "report/test/Blank_A4.jasper";
        Map<String, Object> parameters = new HashMap<>();
        List<Map<String, Object>> dataSource = new ArrayList<>();
        return ReportUtils.responseEntityReport(path, parameters, dataSource, ReportTypeEnum.valueOf(type));
    }

}
