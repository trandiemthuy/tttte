package vn.vnpt.web.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.vnpt.service.DanhMucService;
import vn.vnpt.service.DuLieuBenhNhanService;

import java.util.List;

/**
 * @author NguyenDucAnhKhoi
 */
@RestController
@RequestMapping("/api")
public class DanhMucResource {

    @Autowired
    DanhMucService danhMucService;

    @ApiOperation(value = "Lấy danh sách khoa")
    @GetMapping("/danh-sach-khoa")
    public ResponseEntity<List> dsKhoa(@RequestParam("dvtt") String dvtt) {
        return ResponseEntity.ok()
            .body(danhMucService.dsKhoa(dvtt));
    }

    @ApiOperation(value = "Lấy danh sách nhân viên")
    @GetMapping("/ds-nhan-vien")
    public ResponseEntity<List> getDsNhanVien(@RequestParam(value = "dvtt") String dvtt) {
        List result = danhMucService.dsNhanVien(dvtt);
        return ResponseEntity.ok()
            .body(result);
    }

}
