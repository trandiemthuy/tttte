package vn.vnpt.web.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.vnpt.domain.VienPhiLanTt;
import vn.vnpt.service.VienPhiChiTietLanTtService;
import vn.vnpt.service.VienPhiLanTtService;
import vn.vnpt.service.dto.LanKcbDto;
import vn.vnpt.service.dto.ThongTinVpTongHopDto;
import vn.vnpt.service.dto.VienPhiChiTietLanTtDto;
import vn.vnpt.service.dto.VienPhiLanTtDto;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/vien-phi")
public class VienPhiLanTtResource {

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @Autowired
    VienPhiLanTtService vienPhiLanTtService;

    @Autowired
    VienPhiChiTietLanTtService vienPhiChiTietLanTtService;

    @ApiOperation(value = "Thông tin viện phí tổng hợp của bệnh nhân")
    @GetMapping("/thong-tin-tong-hop")
    public ResponseEntity<ThongTinVpTongHopDto> ttVienPhiTongHop(LanKcbDto lanKcbDto) {
        ThongTinVpTongHopDto result = vienPhiLanTtService.ttVienPhiTongHop(lanKcbDto);
        return ResponseEntity.ok()
            .body(result);
    }

    @ApiOperation(value = "Get danh sách các dịch vụ chưa thanh toán")
    @GetMapping("/chua-thanh-toan")
    public ResponseEntity<List> getChuaThanhToan(LanKcbDto lanKcbDto) {
        List<VienPhiChiTietLanTtDto> result = vienPhiChiTietLanTtService.getChuaThanhToan(lanKcbDto);
        return ResponseEntity.ok()
            .body(result);
    }

    @ApiOperation(value = "Thanh toán viện phí")
    @PostMapping("/thanh-toan")
    public ResponseEntity<VienPhiLanTt> createVienPhiLanTt(@RequestBody VienPhiLanTtDto vienPhiLanTtDto) throws URISyntaxException {
        VienPhiLanTt result = vienPhiLanTtService.createVienPhiLanTt(vienPhiLanTtDto);
        return ResponseEntity.created(new URI("/api/vien-phi/lan-thanh-toan/" + result.getIdLanTt()))
            .body(result);
    }

    @ApiOperation(value = "Lấy danh sách các lần thanh toán")
    @GetMapping("/ds-lan-thanh-toan")
    public ResponseEntity<List> getListVienPhiLanTt(LanKcbDto lanKcbDto) {
        List<VienPhiLanTt> result = vienPhiLanTtService.getListVienPhiLanTt(lanKcbDto);
        return ResponseEntity.ok()
            .body(result);
    }

    @ApiOperation(value = "Hủy thanh toán")
    @DeleteMapping("/lan-thanh-toan")
    public ResponseEntity deleteVienPhiLanTt(
        @RequestParam(value = "soVaoVien") long soVaoVien,
        @RequestParam(value = "soVaoVienDt") long soVaoVienDt,
        @RequestParam(value = "loaiKcb") int loaiKcb,
        @RequestParam(value = "dvtt") String dvtt,
        @RequestParam(value = "sttLanTt") String sttLanTt
    ) {
        vienPhiLanTtService.deleteVienPhiLanTt(soVaoVien, soVaoVienDt, loaiKcb, dvtt, sttLanTt);
        return ResponseEntity.noContent()
            .build();
    }

}
