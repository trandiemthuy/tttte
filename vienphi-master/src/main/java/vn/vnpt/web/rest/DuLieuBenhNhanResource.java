package vn.vnpt.web.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import vn.vnpt.domain.DuLieuBenhNhan;
import vn.vnpt.service.DuLieuBenhNhanService;
import vn.vnpt.service.dto.BenhNhanDto;
import vn.vnpt.service.dto.DuLieuBenhNhanDto;
import vn.vnpt.service.dto.LanKcbDto;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

/**
 * @author NguyenDucAnhKhoi
 */
@RestController
@RequestMapping("/api")
public class DuLieuBenhNhanResource {

    @Autowired
    DuLieuBenhNhanService duLieuBenhNhanService;

    @ApiOperation(value = "Đẩy dữ liệu bệnh nhân vào")
    @PostMapping("/du-lieu-benh-nhan/")
    public ResponseEntity<DuLieuBenhNhan> createDuLieuBenhNhan(@RequestBody DuLieuBenhNhanDto duLieuBenhNhanDto) throws URISyntaxException {
        DuLieuBenhNhan result = duLieuBenhNhanService.createDuLieuBenhNhan(duLieuBenhNhanDto);
        URI createdAt = UriComponentsBuilder.fromUriString("/api/du-lieu-benh-nhan")
            .queryParam("soVaoVien", result.getSoVaoVien())
            .queryParam("soVaoVienDt", result.getSoVaoVienDt())
            .queryParam("loaiKcb", result.getLoaiKcb())
            .queryParam("dvtt", result.getDvtt())
            .build()
            .toUri();
        return ResponseEntity.created(createdAt)
            .body(result);
    }

    @ApiOperation(value = "Xem dữ liệu bệnh nhân")
    @GetMapping("/du-lieu-benh-nhan")
    public ResponseEntity<DuLieuBenhNhan> getDuLieuBenhNhan(LanKcbDto lanKcbDto) {
        DuLieuBenhNhan result = duLieuBenhNhanService.getDuLieuBenhNhan(lanKcbDto);
        int forceLoad = result.getDsDichVuChiDinh().size();
        return ResponseEntity.ok()
            .body(result);
    }

    @ApiOperation(value = "Danh sách bệnh nhân cần thu tiền viện phí - tạm ứng")
    @GetMapping("/danh-sach-benh-nhan/thu-tien")
    public ResponseEntity<List> dsBnThuTien(
        @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "tuNgay") Date tuNgay,
        @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "denNgay") Date denNgay,
        @RequestParam(value = "loaiKcb") int loaiKcb,
        @RequestParam(value = "dvtt") String dvtt,
        @RequestParam(value = "maKhoa", required = false, defaultValue = "-1") String maKhoa,
        @RequestParam(value = "coBhyt", required = false, defaultValue = "-1") int coBhyt,
        @RequestParam(value = "daThanhToan", required = false, defaultValue = "-1") int daThanhToan,
        @RequestParam(value = "daTamUng", required = false, defaultValue = "-1") int daTamUng
    ) {
        List<BenhNhanDto> result = duLieuBenhNhanService.dsBnThuTien(tuNgay, denNgay, loaiKcb, dvtt, maKhoa, coBhyt, daThanhToan, daTamUng);
        return ResponseEntity.ok()
            .body(result);
    }

    @ApiOperation(value = "Tìm bệnh nhân cần thu viện phí - tạm ứng")
    @GetMapping("/danh-sach-benh-nhan/tim-kiem")
    public ResponseEntity<BenhNhanDto> searchBenhNhan(
        @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "tuNgay") Date tuNgay,
        @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "denNgay") Date denNgay,
        @RequestParam(value = "loaiKcb") int loaiKcb,
        @RequestParam(value = "dvtt") String dvtt,
        @RequestParam(value = "maBn") long maBn
    ) {
        BenhNhanDto result = duLieuBenhNhanService.searchBenhNhan(tuNgay, denNgay, loaiKcb, dvtt, maBn);
        return ResponseEntity.ok()
            .body(result);
    }

    @ApiOperation(value = "Danh sách bệnh nhân hoàn tất khám")
    @GetMapping("/danh-sach-benh-nhan/hoan-tat-kham")
    public ResponseEntity<List> dsbnHoanTatKham(
        @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "tuNgay") Date tuNgay,
        @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "denNgay") Date denNgay,
        @RequestParam(value = "loaiKcb") int loaiKcb,
        @RequestParam(value = "maKhoa", required = false, defaultValue = "-1") String maKhoa,
        @RequestParam(value = "dvtt") String dvtt,
        @RequestParam(value = "coBhyt") int coBhyt,
        @RequestParam(value = "daInBangKe", required = false, defaultValue = "-1") int daInBangKe
    ) {
        List<BenhNhanDto> result = duLieuBenhNhanService.dsbnHoanTatKham(tuNgay, denNgay, loaiKcb, dvtt, maKhoa, coBhyt, daInBangKe );
        return ResponseEntity.ok()
            .body(result);
    }

}
