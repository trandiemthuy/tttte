package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.DoiTuongBhyt;
import vn.vnpt.domain.NhomDoiTuongBhyt;
import vn.vnpt.repository.DoiTuongBhytRepository;
import vn.vnpt.service.DoiTuongBhytService;
import vn.vnpt.service.dto.DoiTuongBhytDTO;
import vn.vnpt.service.mapper.DoiTuongBhytMapper;
import vn.vnpt.service.dto.DoiTuongBhytCriteria;
import vn.vnpt.service.DoiTuongBhytQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DoiTuongBhytResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class DoiTuongBhytResourceIT {

    private static final Integer DEFAULT_CAN_TREN = 1;
    private static final Integer UPDATED_CAN_TREN = 2;
    private static final Integer SMALLER_CAN_TREN = 1 - 1;

    private static final Integer DEFAULT_CAN_TREN_KTC = 1;
    private static final Integer UPDATED_CAN_TREN_KTC = 2;
    private static final Integer SMALLER_CAN_TREN_KTC = 1 - 1;

    private static final String DEFAULT_MA = "AAAAAAAAAA";
    private static final String UPDATED_MA = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final Integer DEFAULT_TY_LE_MIEN_GIAM = 1;
    private static final Integer UPDATED_TY_LE_MIEN_GIAM = 2;
    private static final Integer SMALLER_TY_LE_MIEN_GIAM = 1 - 1;

    private static final Integer DEFAULT_TY_LE_MIEN_GIAM_KTC = 1;
    private static final Integer UPDATED_TY_LE_MIEN_GIAM_KTC = 2;
    private static final Integer SMALLER_TY_LE_MIEN_GIAM_KTC = 1 - 1;

    @Autowired
    private DoiTuongBhytRepository doiTuongBhytRepository;

    @Autowired
    private DoiTuongBhytMapper doiTuongBhytMapper;

    @Autowired
    private DoiTuongBhytService doiTuongBhytService;

    @Autowired
    private DoiTuongBhytQueryService doiTuongBhytQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDoiTuongBhytMockMvc;

    private DoiTuongBhyt doiTuongBhyt;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DoiTuongBhyt createEntity(EntityManager em) {
        DoiTuongBhyt doiTuongBhyt = new DoiTuongBhyt()
            .canTren(DEFAULT_CAN_TREN)
            .canTrenKtc(DEFAULT_CAN_TREN_KTC)
            .ma(DEFAULT_MA)
            .ten(DEFAULT_TEN)
            .tyLeMienGiam(DEFAULT_TY_LE_MIEN_GIAM)
            .tyLeMienGiamKtc(DEFAULT_TY_LE_MIEN_GIAM_KTC);
        // Add required entity
        NhomDoiTuongBhyt nhomDoiTuongBhyt;
        if (TestUtil.findAll(em, NhomDoiTuongBhyt.class).isEmpty()) {
            nhomDoiTuongBhyt = NhomDoiTuongBhytResourceIT.createEntity(em);
            em.persist(nhomDoiTuongBhyt);
            em.flush();
        } else {
            nhomDoiTuongBhyt = TestUtil.findAll(em, NhomDoiTuongBhyt.class).get(0);
        }
        doiTuongBhyt.setNhomDoiTuongBhyt(nhomDoiTuongBhyt);
        return doiTuongBhyt;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DoiTuongBhyt createUpdatedEntity(EntityManager em) {
        DoiTuongBhyt doiTuongBhyt = new DoiTuongBhyt()
            .canTren(UPDATED_CAN_TREN)
            .canTrenKtc(UPDATED_CAN_TREN_KTC)
            .ma(UPDATED_MA)
            .ten(UPDATED_TEN)
            .tyLeMienGiam(UPDATED_TY_LE_MIEN_GIAM)
            .tyLeMienGiamKtc(UPDATED_TY_LE_MIEN_GIAM_KTC);
        // Add required entity
        NhomDoiTuongBhyt nhomDoiTuongBhyt;
        if (TestUtil.findAll(em, NhomDoiTuongBhyt.class).isEmpty()) {
            nhomDoiTuongBhyt = NhomDoiTuongBhytResourceIT.createUpdatedEntity(em);
            em.persist(nhomDoiTuongBhyt);
            em.flush();
        } else {
            nhomDoiTuongBhyt = TestUtil.findAll(em, NhomDoiTuongBhyt.class).get(0);
        }
        doiTuongBhyt.setNhomDoiTuongBhyt(nhomDoiTuongBhyt);
        return doiTuongBhyt;
    }

    @BeforeEach
    public void initTest() {
        doiTuongBhyt = createEntity(em);
    }

    @Test
    @Transactional
    public void createDoiTuongBhyt() throws Exception {
        int databaseSizeBeforeCreate = doiTuongBhytRepository.findAll().size();

        // Create the DoiTuongBhyt
        DoiTuongBhytDTO doiTuongBhytDTO = doiTuongBhytMapper.toDto(doiTuongBhyt);
        restDoiTuongBhytMockMvc.perform(post("/api/doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(doiTuongBhytDTO)))
            .andExpect(status().isCreated());

        // Validate the DoiTuongBhyt in the database
        List<DoiTuongBhyt> doiTuongBhytList = doiTuongBhytRepository.findAll();
        assertThat(doiTuongBhytList).hasSize(databaseSizeBeforeCreate + 1);
        DoiTuongBhyt testDoiTuongBhyt = doiTuongBhytList.get(doiTuongBhytList.size() - 1);
        assertThat(testDoiTuongBhyt.getCanTren()).isEqualTo(DEFAULT_CAN_TREN);
        assertThat(testDoiTuongBhyt.getCanTrenKtc()).isEqualTo(DEFAULT_CAN_TREN_KTC);
        assertThat(testDoiTuongBhyt.getMa()).isEqualTo(DEFAULT_MA);
        assertThat(testDoiTuongBhyt.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testDoiTuongBhyt.getTyLeMienGiam()).isEqualTo(DEFAULT_TY_LE_MIEN_GIAM);
        assertThat(testDoiTuongBhyt.getTyLeMienGiamKtc()).isEqualTo(DEFAULT_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void createDoiTuongBhytWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = doiTuongBhytRepository.findAll().size();

        // Create the DoiTuongBhyt with an existing ID
        doiTuongBhyt.setId(1L);
        DoiTuongBhytDTO doiTuongBhytDTO = doiTuongBhytMapper.toDto(doiTuongBhyt);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDoiTuongBhytMockMvc.perform(post("/api/doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(doiTuongBhytDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DoiTuongBhyt in the database
        List<DoiTuongBhyt> doiTuongBhytList = doiTuongBhytRepository.findAll();
        assertThat(doiTuongBhytList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCanTrenIsRequired() throws Exception {
        int databaseSizeBeforeTest = doiTuongBhytRepository.findAll().size();
        // set the field null
        doiTuongBhyt.setCanTren(null);

        // Create the DoiTuongBhyt, which fails.
        DoiTuongBhytDTO doiTuongBhytDTO = doiTuongBhytMapper.toDto(doiTuongBhyt);

        restDoiTuongBhytMockMvc.perform(post("/api/doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(doiTuongBhytDTO)))
            .andExpect(status().isBadRequest());

        List<DoiTuongBhyt> doiTuongBhytList = doiTuongBhytRepository.findAll();
        assertThat(doiTuongBhytList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCanTrenKtcIsRequired() throws Exception {
        int databaseSizeBeforeTest = doiTuongBhytRepository.findAll().size();
        // set the field null
        doiTuongBhyt.setCanTrenKtc(null);

        // Create the DoiTuongBhyt, which fails.
        DoiTuongBhytDTO doiTuongBhytDTO = doiTuongBhytMapper.toDto(doiTuongBhyt);

        restDoiTuongBhytMockMvc.perform(post("/api/doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(doiTuongBhytDTO)))
            .andExpect(status().isBadRequest());

        List<DoiTuongBhyt> doiTuongBhytList = doiTuongBhytRepository.findAll();
        assertThat(doiTuongBhytList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaIsRequired() throws Exception {
        int databaseSizeBeforeTest = doiTuongBhytRepository.findAll().size();
        // set the field null
        doiTuongBhyt.setMa(null);

        // Create the DoiTuongBhyt, which fails.
        DoiTuongBhytDTO doiTuongBhytDTO = doiTuongBhytMapper.toDto(doiTuongBhyt);

        restDoiTuongBhytMockMvc.perform(post("/api/doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(doiTuongBhytDTO)))
            .andExpect(status().isBadRequest());

        List<DoiTuongBhyt> doiTuongBhytList = doiTuongBhytRepository.findAll();
        assertThat(doiTuongBhytList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = doiTuongBhytRepository.findAll().size();
        // set the field null
        doiTuongBhyt.setTen(null);

        // Create the DoiTuongBhyt, which fails.
        DoiTuongBhytDTO doiTuongBhytDTO = doiTuongBhytMapper.toDto(doiTuongBhyt);

        restDoiTuongBhytMockMvc.perform(post("/api/doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(doiTuongBhytDTO)))
            .andExpect(status().isBadRequest());

        List<DoiTuongBhyt> doiTuongBhytList = doiTuongBhytRepository.findAll();
        assertThat(doiTuongBhytList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTyLeMienGiamIsRequired() throws Exception {
        int databaseSizeBeforeTest = doiTuongBhytRepository.findAll().size();
        // set the field null
        doiTuongBhyt.setTyLeMienGiam(null);

        // Create the DoiTuongBhyt, which fails.
        DoiTuongBhytDTO doiTuongBhytDTO = doiTuongBhytMapper.toDto(doiTuongBhyt);

        restDoiTuongBhytMockMvc.perform(post("/api/doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(doiTuongBhytDTO)))
            .andExpect(status().isBadRequest());

        List<DoiTuongBhyt> doiTuongBhytList = doiTuongBhytRepository.findAll();
        assertThat(doiTuongBhytList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTyLeMienGiamKtcIsRequired() throws Exception {
        int databaseSizeBeforeTest = doiTuongBhytRepository.findAll().size();
        // set the field null
        doiTuongBhyt.setTyLeMienGiamKtc(null);

        // Create the DoiTuongBhyt, which fails.
        DoiTuongBhytDTO doiTuongBhytDTO = doiTuongBhytMapper.toDto(doiTuongBhyt);

        restDoiTuongBhytMockMvc.perform(post("/api/doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(doiTuongBhytDTO)))
            .andExpect(status().isBadRequest());

        List<DoiTuongBhyt> doiTuongBhytList = doiTuongBhytRepository.findAll();
        assertThat(doiTuongBhytList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhyts() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList
        restDoiTuongBhytMockMvc.perform(get("/api/doi-tuong-bhyts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(doiTuongBhyt.getId().intValue())))
            .andExpect(jsonPath("$.[*].canTren").value(hasItem(DEFAULT_CAN_TREN)))
            .andExpect(jsonPath("$.[*].canTrenKtc").value(hasItem(DEFAULT_CAN_TREN_KTC)))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tyLeMienGiam").value(hasItem(DEFAULT_TY_LE_MIEN_GIAM)))
            .andExpect(jsonPath("$.[*].tyLeMienGiamKtc").value(hasItem(DEFAULT_TY_LE_MIEN_GIAM_KTC)));
    }
    
    @Test
    @Transactional
    public void getDoiTuongBhyt() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get the doiTuongBhyt
        restDoiTuongBhytMockMvc.perform(get("/api/doi-tuong-bhyts/{id}", doiTuongBhyt.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(doiTuongBhyt.getId().intValue()))
            .andExpect(jsonPath("$.canTren").value(DEFAULT_CAN_TREN))
            .andExpect(jsonPath("$.canTrenKtc").value(DEFAULT_CAN_TREN_KTC))
            .andExpect(jsonPath("$.ma").value(DEFAULT_MA))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.tyLeMienGiam").value(DEFAULT_TY_LE_MIEN_GIAM))
            .andExpect(jsonPath("$.tyLeMienGiamKtc").value(DEFAULT_TY_LE_MIEN_GIAM_KTC));
    }


    @Test
    @Transactional
    public void getDoiTuongBhytsByIdFiltering() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        Long id = doiTuongBhyt.getId();

        defaultDoiTuongBhytShouldBeFound("id.equals=" + id);
        defaultDoiTuongBhytShouldNotBeFound("id.notEquals=" + id);

        defaultDoiTuongBhytShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDoiTuongBhytShouldNotBeFound("id.greaterThan=" + id);

        defaultDoiTuongBhytShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDoiTuongBhytShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenIsEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTren equals to DEFAULT_CAN_TREN
        defaultDoiTuongBhytShouldBeFound("canTren.equals=" + DEFAULT_CAN_TREN);

        // Get all the doiTuongBhytList where canTren equals to UPDATED_CAN_TREN
        defaultDoiTuongBhytShouldNotBeFound("canTren.equals=" + UPDATED_CAN_TREN);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTren not equals to DEFAULT_CAN_TREN
        defaultDoiTuongBhytShouldNotBeFound("canTren.notEquals=" + DEFAULT_CAN_TREN);

        // Get all the doiTuongBhytList where canTren not equals to UPDATED_CAN_TREN
        defaultDoiTuongBhytShouldBeFound("canTren.notEquals=" + UPDATED_CAN_TREN);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenIsInShouldWork() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTren in DEFAULT_CAN_TREN or UPDATED_CAN_TREN
        defaultDoiTuongBhytShouldBeFound("canTren.in=" + DEFAULT_CAN_TREN + "," + UPDATED_CAN_TREN);

        // Get all the doiTuongBhytList where canTren equals to UPDATED_CAN_TREN
        defaultDoiTuongBhytShouldNotBeFound("canTren.in=" + UPDATED_CAN_TREN);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenIsNullOrNotNull() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTren is not null
        defaultDoiTuongBhytShouldBeFound("canTren.specified=true");

        // Get all the doiTuongBhytList where canTren is null
        defaultDoiTuongBhytShouldNotBeFound("canTren.specified=false");
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTren is greater than or equal to DEFAULT_CAN_TREN
        defaultDoiTuongBhytShouldBeFound("canTren.greaterThanOrEqual=" + DEFAULT_CAN_TREN);

        // Get all the doiTuongBhytList where canTren is greater than or equal to UPDATED_CAN_TREN
        defaultDoiTuongBhytShouldNotBeFound("canTren.greaterThanOrEqual=" + UPDATED_CAN_TREN);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTren is less than or equal to DEFAULT_CAN_TREN
        defaultDoiTuongBhytShouldBeFound("canTren.lessThanOrEqual=" + DEFAULT_CAN_TREN);

        // Get all the doiTuongBhytList where canTren is less than or equal to SMALLER_CAN_TREN
        defaultDoiTuongBhytShouldNotBeFound("canTren.lessThanOrEqual=" + SMALLER_CAN_TREN);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenIsLessThanSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTren is less than DEFAULT_CAN_TREN
        defaultDoiTuongBhytShouldNotBeFound("canTren.lessThan=" + DEFAULT_CAN_TREN);

        // Get all the doiTuongBhytList where canTren is less than UPDATED_CAN_TREN
        defaultDoiTuongBhytShouldBeFound("canTren.lessThan=" + UPDATED_CAN_TREN);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTren is greater than DEFAULT_CAN_TREN
        defaultDoiTuongBhytShouldNotBeFound("canTren.greaterThan=" + DEFAULT_CAN_TREN);

        // Get all the doiTuongBhytList where canTren is greater than SMALLER_CAN_TREN
        defaultDoiTuongBhytShouldBeFound("canTren.greaterThan=" + SMALLER_CAN_TREN);
    }


    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenKtcIsEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTrenKtc equals to DEFAULT_CAN_TREN_KTC
        defaultDoiTuongBhytShouldBeFound("canTrenKtc.equals=" + DEFAULT_CAN_TREN_KTC);

        // Get all the doiTuongBhytList where canTrenKtc equals to UPDATED_CAN_TREN_KTC
        defaultDoiTuongBhytShouldNotBeFound("canTrenKtc.equals=" + UPDATED_CAN_TREN_KTC);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenKtcIsNotEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTrenKtc not equals to DEFAULT_CAN_TREN_KTC
        defaultDoiTuongBhytShouldNotBeFound("canTrenKtc.notEquals=" + DEFAULT_CAN_TREN_KTC);

        // Get all the doiTuongBhytList where canTrenKtc not equals to UPDATED_CAN_TREN_KTC
        defaultDoiTuongBhytShouldBeFound("canTrenKtc.notEquals=" + UPDATED_CAN_TREN_KTC);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenKtcIsInShouldWork() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTrenKtc in DEFAULT_CAN_TREN_KTC or UPDATED_CAN_TREN_KTC
        defaultDoiTuongBhytShouldBeFound("canTrenKtc.in=" + DEFAULT_CAN_TREN_KTC + "," + UPDATED_CAN_TREN_KTC);

        // Get all the doiTuongBhytList where canTrenKtc equals to UPDATED_CAN_TREN_KTC
        defaultDoiTuongBhytShouldNotBeFound("canTrenKtc.in=" + UPDATED_CAN_TREN_KTC);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenKtcIsNullOrNotNull() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTrenKtc is not null
        defaultDoiTuongBhytShouldBeFound("canTrenKtc.specified=true");

        // Get all the doiTuongBhytList where canTrenKtc is null
        defaultDoiTuongBhytShouldNotBeFound("canTrenKtc.specified=false");
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenKtcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTrenKtc is greater than or equal to DEFAULT_CAN_TREN_KTC
        defaultDoiTuongBhytShouldBeFound("canTrenKtc.greaterThanOrEqual=" + DEFAULT_CAN_TREN_KTC);

        // Get all the doiTuongBhytList where canTrenKtc is greater than or equal to UPDATED_CAN_TREN_KTC
        defaultDoiTuongBhytShouldNotBeFound("canTrenKtc.greaterThanOrEqual=" + UPDATED_CAN_TREN_KTC);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenKtcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTrenKtc is less than or equal to DEFAULT_CAN_TREN_KTC
        defaultDoiTuongBhytShouldBeFound("canTrenKtc.lessThanOrEqual=" + DEFAULT_CAN_TREN_KTC);

        // Get all the doiTuongBhytList where canTrenKtc is less than or equal to SMALLER_CAN_TREN_KTC
        defaultDoiTuongBhytShouldNotBeFound("canTrenKtc.lessThanOrEqual=" + SMALLER_CAN_TREN_KTC);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenKtcIsLessThanSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTrenKtc is less than DEFAULT_CAN_TREN_KTC
        defaultDoiTuongBhytShouldNotBeFound("canTrenKtc.lessThan=" + DEFAULT_CAN_TREN_KTC);

        // Get all the doiTuongBhytList where canTrenKtc is less than UPDATED_CAN_TREN_KTC
        defaultDoiTuongBhytShouldBeFound("canTrenKtc.lessThan=" + UPDATED_CAN_TREN_KTC);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByCanTrenKtcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where canTrenKtc is greater than DEFAULT_CAN_TREN_KTC
        defaultDoiTuongBhytShouldNotBeFound("canTrenKtc.greaterThan=" + DEFAULT_CAN_TREN_KTC);

        // Get all the doiTuongBhytList where canTrenKtc is greater than SMALLER_CAN_TREN_KTC
        defaultDoiTuongBhytShouldBeFound("canTrenKtc.greaterThan=" + SMALLER_CAN_TREN_KTC);
    }


    @Test
    @Transactional
    public void getAllDoiTuongBhytsByMaIsEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where ma equals to DEFAULT_MA
        defaultDoiTuongBhytShouldBeFound("ma.equals=" + DEFAULT_MA);

        // Get all the doiTuongBhytList where ma equals to UPDATED_MA
        defaultDoiTuongBhytShouldNotBeFound("ma.equals=" + UPDATED_MA);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByMaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where ma not equals to DEFAULT_MA
        defaultDoiTuongBhytShouldNotBeFound("ma.notEquals=" + DEFAULT_MA);

        // Get all the doiTuongBhytList where ma not equals to UPDATED_MA
        defaultDoiTuongBhytShouldBeFound("ma.notEquals=" + UPDATED_MA);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByMaIsInShouldWork() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where ma in DEFAULT_MA or UPDATED_MA
        defaultDoiTuongBhytShouldBeFound("ma.in=" + DEFAULT_MA + "," + UPDATED_MA);

        // Get all the doiTuongBhytList where ma equals to UPDATED_MA
        defaultDoiTuongBhytShouldNotBeFound("ma.in=" + UPDATED_MA);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByMaIsNullOrNotNull() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where ma is not null
        defaultDoiTuongBhytShouldBeFound("ma.specified=true");

        // Get all the doiTuongBhytList where ma is null
        defaultDoiTuongBhytShouldNotBeFound("ma.specified=false");
    }
                @Test
    @Transactional
    public void getAllDoiTuongBhytsByMaContainsSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where ma contains DEFAULT_MA
        defaultDoiTuongBhytShouldBeFound("ma.contains=" + DEFAULT_MA);

        // Get all the doiTuongBhytList where ma contains UPDATED_MA
        defaultDoiTuongBhytShouldNotBeFound("ma.contains=" + UPDATED_MA);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByMaNotContainsSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where ma does not contain DEFAULT_MA
        defaultDoiTuongBhytShouldNotBeFound("ma.doesNotContain=" + DEFAULT_MA);

        // Get all the doiTuongBhytList where ma does not contain UPDATED_MA
        defaultDoiTuongBhytShouldBeFound("ma.doesNotContain=" + UPDATED_MA);
    }


    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where ten equals to DEFAULT_TEN
        defaultDoiTuongBhytShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the doiTuongBhytList where ten equals to UPDATED_TEN
        defaultDoiTuongBhytShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where ten not equals to DEFAULT_TEN
        defaultDoiTuongBhytShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the doiTuongBhytList where ten not equals to UPDATED_TEN
        defaultDoiTuongBhytShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultDoiTuongBhytShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the doiTuongBhytList where ten equals to UPDATED_TEN
        defaultDoiTuongBhytShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where ten is not null
        defaultDoiTuongBhytShouldBeFound("ten.specified=true");

        // Get all the doiTuongBhytList where ten is null
        defaultDoiTuongBhytShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllDoiTuongBhytsByTenContainsSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where ten contains DEFAULT_TEN
        defaultDoiTuongBhytShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the doiTuongBhytList where ten contains UPDATED_TEN
        defaultDoiTuongBhytShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where ten does not contain DEFAULT_TEN
        defaultDoiTuongBhytShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the doiTuongBhytList where ten does not contain UPDATED_TEN
        defaultDoiTuongBhytShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamIsEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiam equals to DEFAULT_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiam.equals=" + DEFAULT_TY_LE_MIEN_GIAM);

        // Get all the doiTuongBhytList where tyLeMienGiam equals to UPDATED_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiam.equals=" + UPDATED_TY_LE_MIEN_GIAM);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiam not equals to DEFAULT_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiam.notEquals=" + DEFAULT_TY_LE_MIEN_GIAM);

        // Get all the doiTuongBhytList where tyLeMienGiam not equals to UPDATED_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiam.notEquals=" + UPDATED_TY_LE_MIEN_GIAM);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamIsInShouldWork() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiam in DEFAULT_TY_LE_MIEN_GIAM or UPDATED_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiam.in=" + DEFAULT_TY_LE_MIEN_GIAM + "," + UPDATED_TY_LE_MIEN_GIAM);

        // Get all the doiTuongBhytList where tyLeMienGiam equals to UPDATED_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiam.in=" + UPDATED_TY_LE_MIEN_GIAM);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamIsNullOrNotNull() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiam is not null
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiam.specified=true");

        // Get all the doiTuongBhytList where tyLeMienGiam is null
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiam.specified=false");
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiam is greater than or equal to DEFAULT_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiam.greaterThanOrEqual=" + DEFAULT_TY_LE_MIEN_GIAM);

        // Get all the doiTuongBhytList where tyLeMienGiam is greater than or equal to UPDATED_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiam.greaterThanOrEqual=" + UPDATED_TY_LE_MIEN_GIAM);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiam is less than or equal to DEFAULT_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiam.lessThanOrEqual=" + DEFAULT_TY_LE_MIEN_GIAM);

        // Get all the doiTuongBhytList where tyLeMienGiam is less than or equal to SMALLER_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiam.lessThanOrEqual=" + SMALLER_TY_LE_MIEN_GIAM);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamIsLessThanSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiam is less than DEFAULT_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiam.lessThan=" + DEFAULT_TY_LE_MIEN_GIAM);

        // Get all the doiTuongBhytList where tyLeMienGiam is less than UPDATED_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiam.lessThan=" + UPDATED_TY_LE_MIEN_GIAM);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiam is greater than DEFAULT_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiam.greaterThan=" + DEFAULT_TY_LE_MIEN_GIAM);

        // Get all the doiTuongBhytList where tyLeMienGiam is greater than SMALLER_TY_LE_MIEN_GIAM
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiam.greaterThan=" + SMALLER_TY_LE_MIEN_GIAM);
    }


    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamKtcIsEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc equals to DEFAULT_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiamKtc.equals=" + DEFAULT_TY_LE_MIEN_GIAM_KTC);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc equals to UPDATED_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiamKtc.equals=" + UPDATED_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamKtcIsNotEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc not equals to DEFAULT_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiamKtc.notEquals=" + DEFAULT_TY_LE_MIEN_GIAM_KTC);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc not equals to UPDATED_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiamKtc.notEquals=" + UPDATED_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamKtcIsInShouldWork() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc in DEFAULT_TY_LE_MIEN_GIAM_KTC or UPDATED_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiamKtc.in=" + DEFAULT_TY_LE_MIEN_GIAM_KTC + "," + UPDATED_TY_LE_MIEN_GIAM_KTC);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc equals to UPDATED_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiamKtc.in=" + UPDATED_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamKtcIsNullOrNotNull() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc is not null
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiamKtc.specified=true");

        // Get all the doiTuongBhytList where tyLeMienGiamKtc is null
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiamKtc.specified=false");
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamKtcIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc is greater than or equal to DEFAULT_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiamKtc.greaterThanOrEqual=" + DEFAULT_TY_LE_MIEN_GIAM_KTC);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc is greater than or equal to UPDATED_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiamKtc.greaterThanOrEqual=" + UPDATED_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamKtcIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc is less than or equal to DEFAULT_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiamKtc.lessThanOrEqual=" + DEFAULT_TY_LE_MIEN_GIAM_KTC);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc is less than or equal to SMALLER_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiamKtc.lessThanOrEqual=" + SMALLER_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamKtcIsLessThanSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc is less than DEFAULT_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiamKtc.lessThan=" + DEFAULT_TY_LE_MIEN_GIAM_KTC);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc is less than UPDATED_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiamKtc.lessThan=" + UPDATED_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void getAllDoiTuongBhytsByTyLeMienGiamKtcIsGreaterThanSomething() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc is greater than DEFAULT_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldNotBeFound("tyLeMienGiamKtc.greaterThan=" + DEFAULT_TY_LE_MIEN_GIAM_KTC);

        // Get all the doiTuongBhytList where tyLeMienGiamKtc is greater than SMALLER_TY_LE_MIEN_GIAM_KTC
        defaultDoiTuongBhytShouldBeFound("tyLeMienGiamKtc.greaterThan=" + SMALLER_TY_LE_MIEN_GIAM_KTC);
    }


    @Test
    @Transactional
    public void getAllDoiTuongBhytsByNhomDoiTuongBhytIsEqualToSomething() throws Exception {
        // Get already existing entity
        NhomDoiTuongBhyt nhomDoiTuongBhyt = doiTuongBhyt.getNhomDoiTuongBhyt();
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);
        Long nhomDoiTuongBhytId = nhomDoiTuongBhyt.getId();

        // Get all the doiTuongBhytList where nhomDoiTuongBhyt equals to nhomDoiTuongBhytId
        defaultDoiTuongBhytShouldBeFound("nhomDoiTuongBhytId.equals=" + nhomDoiTuongBhytId);

        // Get all the doiTuongBhytList where nhomDoiTuongBhyt equals to nhomDoiTuongBhytId + 1
        defaultDoiTuongBhytShouldNotBeFound("nhomDoiTuongBhytId.equals=" + (nhomDoiTuongBhytId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDoiTuongBhytShouldBeFound(String filter) throws Exception {
        restDoiTuongBhytMockMvc.perform(get("/api/doi-tuong-bhyts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(doiTuongBhyt.getId().intValue())))
            .andExpect(jsonPath("$.[*].canTren").value(hasItem(DEFAULT_CAN_TREN)))
            .andExpect(jsonPath("$.[*].canTrenKtc").value(hasItem(DEFAULT_CAN_TREN_KTC)))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tyLeMienGiam").value(hasItem(DEFAULT_TY_LE_MIEN_GIAM)))
            .andExpect(jsonPath("$.[*].tyLeMienGiamKtc").value(hasItem(DEFAULT_TY_LE_MIEN_GIAM_KTC)));

        // Check, that the count call also returns 1
        restDoiTuongBhytMockMvc.perform(get("/api/doi-tuong-bhyts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDoiTuongBhytShouldNotBeFound(String filter) throws Exception {
        restDoiTuongBhytMockMvc.perform(get("/api/doi-tuong-bhyts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDoiTuongBhytMockMvc.perform(get("/api/doi-tuong-bhyts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDoiTuongBhyt() throws Exception {
        // Get the doiTuongBhyt
        restDoiTuongBhytMockMvc.perform(get("/api/doi-tuong-bhyts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDoiTuongBhyt() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        int databaseSizeBeforeUpdate = doiTuongBhytRepository.findAll().size();

        // Update the doiTuongBhyt
        DoiTuongBhyt updatedDoiTuongBhyt = doiTuongBhytRepository.findById(doiTuongBhyt.getId()).get();
        // Disconnect from session so that the updates on updatedDoiTuongBhyt are not directly saved in db
        em.detach(updatedDoiTuongBhyt);
        updatedDoiTuongBhyt
            .canTren(UPDATED_CAN_TREN)
            .canTrenKtc(UPDATED_CAN_TREN_KTC)
            .ma(UPDATED_MA)
            .ten(UPDATED_TEN)
            .tyLeMienGiam(UPDATED_TY_LE_MIEN_GIAM)
            .tyLeMienGiamKtc(UPDATED_TY_LE_MIEN_GIAM_KTC);
        DoiTuongBhytDTO doiTuongBhytDTO = doiTuongBhytMapper.toDto(updatedDoiTuongBhyt);

        restDoiTuongBhytMockMvc.perform(put("/api/doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(doiTuongBhytDTO)))
            .andExpect(status().isOk());

        // Validate the DoiTuongBhyt in the database
        List<DoiTuongBhyt> doiTuongBhytList = doiTuongBhytRepository.findAll();
        assertThat(doiTuongBhytList).hasSize(databaseSizeBeforeUpdate);
        DoiTuongBhyt testDoiTuongBhyt = doiTuongBhytList.get(doiTuongBhytList.size() - 1);
        assertThat(testDoiTuongBhyt.getCanTren()).isEqualTo(UPDATED_CAN_TREN);
        assertThat(testDoiTuongBhyt.getCanTrenKtc()).isEqualTo(UPDATED_CAN_TREN_KTC);
        assertThat(testDoiTuongBhyt.getMa()).isEqualTo(UPDATED_MA);
        assertThat(testDoiTuongBhyt.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testDoiTuongBhyt.getTyLeMienGiam()).isEqualTo(UPDATED_TY_LE_MIEN_GIAM);
        assertThat(testDoiTuongBhyt.getTyLeMienGiamKtc()).isEqualTo(UPDATED_TY_LE_MIEN_GIAM_KTC);
    }

    @Test
    @Transactional
    public void updateNonExistingDoiTuongBhyt() throws Exception {
        int databaseSizeBeforeUpdate = doiTuongBhytRepository.findAll().size();

        // Create the DoiTuongBhyt
        DoiTuongBhytDTO doiTuongBhytDTO = doiTuongBhytMapper.toDto(doiTuongBhyt);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDoiTuongBhytMockMvc.perform(put("/api/doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(doiTuongBhytDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DoiTuongBhyt in the database
        List<DoiTuongBhyt> doiTuongBhytList = doiTuongBhytRepository.findAll();
        assertThat(doiTuongBhytList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDoiTuongBhyt() throws Exception {
        // Initialize the database
        doiTuongBhytRepository.saveAndFlush(doiTuongBhyt);

        int databaseSizeBeforeDelete = doiTuongBhytRepository.findAll().size();

        // Delete the doiTuongBhyt
        restDoiTuongBhytMockMvc.perform(delete("/api/doi-tuong-bhyts/{id}", doiTuongBhyt.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DoiTuongBhyt> doiTuongBhytList = doiTuongBhytRepository.findAll();
        assertThat(doiTuongBhytList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
