package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.BenhNhan;
import vn.vnpt.domain.DanToc;
import vn.vnpt.domain.NgheNghiep;
import vn.vnpt.domain.Country;
import vn.vnpt.domain.PhuongXa;
import vn.vnpt.domain.QuanHuyen;
import vn.vnpt.domain.TinhThanhPho;
import vn.vnpt.domain.UserExtra;
import vn.vnpt.repository.BenhNhanRepository;
import vn.vnpt.service.BenhNhanService;
import vn.vnpt.service.dto.BenhNhanDTO;
import vn.vnpt.service.mapper.BenhNhanMapper;
import vn.vnpt.service.dto.BenhNhanCriteria;
import vn.vnpt.service.BenhNhanQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BenhNhanResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class BenhNhanResourceIT {

    private static final Boolean DEFAULT_CHI_CO_NAM_SINH = false;
    private static final Boolean UPDATED_CHI_CO_NAM_SINH = true;

    private static final String DEFAULT_CMND = "AAAAAAAAAA";
    private static final String UPDATED_CMND = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ENABLED = false;
    private static final Boolean UPDATED_ENABLED = true;

    private static final Integer DEFAULT_GIOI_TINH = 1;
    private static final Integer UPDATED_GIOI_TINH = 2;
    private static final Integer SMALLER_GIOI_TINH = 1 - 1;

    private static final String DEFAULT_KHANG_THE = "AAAAA";
    private static final String UPDATED_KHANG_THE = "BBBBB";

    private static final String DEFAULT_MA_SO_THUE = "AAAAAAAAAA";
    private static final String UPDATED_MA_SO_THUE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_CAP_CMND = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_CAP_CMND = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_CAP_CMND = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_NGAY_SINH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_SINH = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_SINH = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_NHOM_MAU = "AAAAA";
    private static final String UPDATED_NHOM_MAU = "BBBBB";

    private static final String DEFAULT_NOI_CAP_CMND = "AAAAAAAAAA";
    private static final String UPDATED_NOI_CAP_CMND = "BBBBBBBBBB";

    private static final String DEFAULT_NOI_LAM_VIEC = "AAAAAAAAAA";
    private static final String UPDATED_NOI_LAM_VIEC = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_SO_NHA_XOM = "AAAAAAAAAA";
    private static final String UPDATED_SO_NHA_XOM = "BBBBBBBBBB";

    private static final String DEFAULT_AP_THON = "AAAAAAAAAA";
    private static final String UPDATED_AP_THON = "BBBBBBBBBB";

    private static final Long DEFAULT_DIA_PHUONG_ID = 1L;
    private static final Long UPDATED_DIA_PHUONG_ID = 2L;
    private static final Long SMALLER_DIA_PHUONG_ID = 1L - 1L;

    private static final String DEFAULT_DIA_CHI_THUONG_TRU = "AAAAAAAAAA";
    private static final String UPDATED_DIA_CHI_THUONG_TRU = "BBBBBBBBBB";

    @Autowired
    private BenhNhanRepository benhNhanRepository;

    @Autowired
    private BenhNhanMapper benhNhanMapper;

    @Autowired
    private BenhNhanService benhNhanService;

    @Autowired
    private BenhNhanQueryService benhNhanQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBenhNhanMockMvc;

    private BenhNhan benhNhan;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BenhNhan createEntity(EntityManager em) {
        BenhNhan benhNhan = new BenhNhan()
            .chiCoNamSinh(DEFAULT_CHI_CO_NAM_SINH)
            .cmnd(DEFAULT_CMND)
            .email(DEFAULT_EMAIL)
            .enabled(DEFAULT_ENABLED)
            .gioiTinh(DEFAULT_GIOI_TINH)
            .khangThe(DEFAULT_KHANG_THE)
            .maSoThue(DEFAULT_MA_SO_THUE)
            .ngayCapCmnd(DEFAULT_NGAY_CAP_CMND)
            .ngaySinh(DEFAULT_NGAY_SINH)
            .nhomMau(DEFAULT_NHOM_MAU)
            .noiCapCmnd(DEFAULT_NOI_CAP_CMND)
            .noiLamViec(DEFAULT_NOI_LAM_VIEC)
            .phone(DEFAULT_PHONE)
            .ten(DEFAULT_TEN)
            .soNhaXom(DEFAULT_SO_NHA_XOM)
            .apThon(DEFAULT_AP_THON)
            .diaPhuongId(DEFAULT_DIA_PHUONG_ID)
            .diaChiThuongTru(DEFAULT_DIA_CHI_THUONG_TRU);
        // Add required entity
        Country country;
        if (TestUtil.findAll(em, Country.class).isEmpty()) {
            country = CountryResourceIT.createEntity(em);
            em.persist(country);
            em.flush();
        } else {
            country = TestUtil.findAll(em, Country.class).get(0);
        }
        benhNhan.setQuocTich(country);
        return benhNhan;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BenhNhan createUpdatedEntity(EntityManager em) {
        BenhNhan benhNhan = new BenhNhan()
            .chiCoNamSinh(UPDATED_CHI_CO_NAM_SINH)
            .cmnd(UPDATED_CMND)
            .email(UPDATED_EMAIL)
            .enabled(UPDATED_ENABLED)
            .gioiTinh(UPDATED_GIOI_TINH)
            .khangThe(UPDATED_KHANG_THE)
            .maSoThue(UPDATED_MA_SO_THUE)
            .ngayCapCmnd(UPDATED_NGAY_CAP_CMND)
            .ngaySinh(UPDATED_NGAY_SINH)
            .nhomMau(UPDATED_NHOM_MAU)
            .noiCapCmnd(UPDATED_NOI_CAP_CMND)
            .noiLamViec(UPDATED_NOI_LAM_VIEC)
            .phone(UPDATED_PHONE)
            .ten(UPDATED_TEN)
            .soNhaXom(UPDATED_SO_NHA_XOM)
            .apThon(UPDATED_AP_THON)
            .diaPhuongId(UPDATED_DIA_PHUONG_ID)
            .diaChiThuongTru(UPDATED_DIA_CHI_THUONG_TRU);
        // Add required entity
        Country country;
        if (TestUtil.findAll(em, Country.class).isEmpty()) {
            country = CountryResourceIT.createUpdatedEntity(em);
            em.persist(country);
            em.flush();
        } else {
            country = TestUtil.findAll(em, Country.class).get(0);
        }
        benhNhan.setQuocTich(country);
        return benhNhan;
    }

    @BeforeEach
    public void initTest() {
        benhNhan = createEntity(em);
    }

    @Test
    @Transactional
    public void createBenhNhan() throws Exception {
        int databaseSizeBeforeCreate = benhNhanRepository.findAll().size();
        // Create the BenhNhan
        BenhNhanDTO benhNhanDTO = benhNhanMapper.toDto(benhNhan);
        restBenhNhanMockMvc.perform(post("/api/benh-nhans").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhNhanDTO)))
            .andExpect(status().isCreated());

        // Validate the BenhNhan in the database
        List<BenhNhan> benhNhanList = benhNhanRepository.findAll();
        assertThat(benhNhanList).hasSize(databaseSizeBeforeCreate + 1);
        BenhNhan testBenhNhan = benhNhanList.get(benhNhanList.size() - 1);
        assertThat(testBenhNhan.isChiCoNamSinh()).isEqualTo(DEFAULT_CHI_CO_NAM_SINH);
        assertThat(testBenhNhan.getCmnd()).isEqualTo(DEFAULT_CMND);
        assertThat(testBenhNhan.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testBenhNhan.isEnabled()).isEqualTo(DEFAULT_ENABLED);
        assertThat(testBenhNhan.getGioiTinh()).isEqualTo(DEFAULT_GIOI_TINH);
        assertThat(testBenhNhan.getKhangThe()).isEqualTo(DEFAULT_KHANG_THE);
        assertThat(testBenhNhan.getMaSoThue()).isEqualTo(DEFAULT_MA_SO_THUE);
        assertThat(testBenhNhan.getNgayCapCmnd()).isEqualTo(DEFAULT_NGAY_CAP_CMND);
        assertThat(testBenhNhan.getNgaySinh()).isEqualTo(DEFAULT_NGAY_SINH);
        assertThat(testBenhNhan.getNhomMau()).isEqualTo(DEFAULT_NHOM_MAU);
        assertThat(testBenhNhan.getNoiCapCmnd()).isEqualTo(DEFAULT_NOI_CAP_CMND);
        assertThat(testBenhNhan.getNoiLamViec()).isEqualTo(DEFAULT_NOI_LAM_VIEC);
        assertThat(testBenhNhan.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testBenhNhan.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testBenhNhan.getSoNhaXom()).isEqualTo(DEFAULT_SO_NHA_XOM);
        assertThat(testBenhNhan.getApThon()).isEqualTo(DEFAULT_AP_THON);
        assertThat(testBenhNhan.getDiaPhuongId()).isEqualTo(DEFAULT_DIA_PHUONG_ID);
        assertThat(testBenhNhan.getDiaChiThuongTru()).isEqualTo(DEFAULT_DIA_CHI_THUONG_TRU);
    }

    @Test
    @Transactional
    public void createBenhNhanWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = benhNhanRepository.findAll().size();

        // Create the BenhNhan with an existing ID
        benhNhan.setId(1L);
        BenhNhanDTO benhNhanDTO = benhNhanMapper.toDto(benhNhan);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBenhNhanMockMvc.perform(post("/api/benh-nhans").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhNhanDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BenhNhan in the database
        List<BenhNhan> benhNhanList = benhNhanRepository.findAll();
        assertThat(benhNhanList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkChiCoNamSinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhNhanRepository.findAll().size();
        // set the field null
        benhNhan.setChiCoNamSinh(null);

        // Create the BenhNhan, which fails.
        BenhNhanDTO benhNhanDTO = benhNhanMapper.toDto(benhNhan);


        restBenhNhanMockMvc.perform(post("/api/benh-nhans").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhNhanDTO)))
            .andExpect(status().isBadRequest());

        List<BenhNhan> benhNhanList = benhNhanRepository.findAll();
        assertThat(benhNhanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhNhanRepository.findAll().size();
        // set the field null
        benhNhan.setEnabled(null);

        // Create the BenhNhan, which fails.
        BenhNhanDTO benhNhanDTO = benhNhanMapper.toDto(benhNhan);


        restBenhNhanMockMvc.perform(post("/api/benh-nhans").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhNhanDTO)))
            .andExpect(status().isBadRequest());

        List<BenhNhan> benhNhanList = benhNhanRepository.findAll();
        assertThat(benhNhanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGioiTinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhNhanRepository.findAll().size();
        // set the field null
        benhNhan.setGioiTinh(null);

        // Create the BenhNhan, which fails.
        BenhNhanDTO benhNhanDTO = benhNhanMapper.toDto(benhNhan);


        restBenhNhanMockMvc.perform(post("/api/benh-nhans").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhNhanDTO)))
            .andExpect(status().isBadRequest());

        List<BenhNhan> benhNhanList = benhNhanRepository.findAll();
        assertThat(benhNhanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNgaySinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhNhanRepository.findAll().size();
        // set the field null
        benhNhan.setNgaySinh(null);

        // Create the BenhNhan, which fails.
        BenhNhanDTO benhNhanDTO = benhNhanMapper.toDto(benhNhan);


        restBenhNhanMockMvc.perform(post("/api/benh-nhans").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhNhanDTO)))
            .andExpect(status().isBadRequest());

        List<BenhNhan> benhNhanList = benhNhanRepository.findAll();
        assertThat(benhNhanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhNhanRepository.findAll().size();
        // set the field null
        benhNhan.setTen(null);

        // Create the BenhNhan, which fails.
        BenhNhanDTO benhNhanDTO = benhNhanMapper.toDto(benhNhan);


        restBenhNhanMockMvc.perform(post("/api/benh-nhans").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhNhanDTO)))
            .andExpect(status().isBadRequest());

        List<BenhNhan> benhNhanList = benhNhanRepository.findAll();
        assertThat(benhNhanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDiaChiThuongTruIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhNhanRepository.findAll().size();
        // set the field null
        benhNhan.setDiaChiThuongTru(null);

        // Create the BenhNhan, which fails.
        BenhNhanDTO benhNhanDTO = benhNhanMapper.toDto(benhNhan);


        restBenhNhanMockMvc.perform(post("/api/benh-nhans").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhNhanDTO)))
            .andExpect(status().isBadRequest());

        List<BenhNhan> benhNhanList = benhNhanRepository.findAll();
        assertThat(benhNhanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBenhNhans() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList
        restBenhNhanMockMvc.perform(get("/api/benh-nhans?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(benhNhan.getId().intValue())))
            .andExpect(jsonPath("$.[*].chiCoNamSinh").value(hasItem(DEFAULT_CHI_CO_NAM_SINH.booleanValue())))
            .andExpect(jsonPath("$.[*].cmnd").value(hasItem(DEFAULT_CMND)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].gioiTinh").value(hasItem(DEFAULT_GIOI_TINH)))
            .andExpect(jsonPath("$.[*].khangThe").value(hasItem(DEFAULT_KHANG_THE)))
            .andExpect(jsonPath("$.[*].maSoThue").value(hasItem(DEFAULT_MA_SO_THUE)))
            .andExpect(jsonPath("$.[*].ngayCapCmnd").value(hasItem(DEFAULT_NGAY_CAP_CMND.toString())))
            .andExpect(jsonPath("$.[*].ngaySinh").value(hasItem(DEFAULT_NGAY_SINH.toString())))
            .andExpect(jsonPath("$.[*].nhomMau").value(hasItem(DEFAULT_NHOM_MAU)))
            .andExpect(jsonPath("$.[*].noiCapCmnd").value(hasItem(DEFAULT_NOI_CAP_CMND)))
            .andExpect(jsonPath("$.[*].noiLamViec").value(hasItem(DEFAULT_NOI_LAM_VIEC)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].soNhaXom").value(hasItem(DEFAULT_SO_NHA_XOM)))
            .andExpect(jsonPath("$.[*].apThon").value(hasItem(DEFAULT_AP_THON)))
            .andExpect(jsonPath("$.[*].diaPhuongId").value(hasItem(DEFAULT_DIA_PHUONG_ID.intValue())))
            .andExpect(jsonPath("$.[*].diaChiThuongTru").value(hasItem(DEFAULT_DIA_CHI_THUONG_TRU)));
    }
    
    @Test
    @Transactional
    public void getBenhNhan() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get the benhNhan
        restBenhNhanMockMvc.perform(get("/api/benh-nhans/{id}", benhNhan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(benhNhan.getId().intValue()))
            .andExpect(jsonPath("$.chiCoNamSinh").value(DEFAULT_CHI_CO_NAM_SINH.booleanValue()))
            .andExpect(jsonPath("$.cmnd").value(DEFAULT_CMND))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()))
            .andExpect(jsonPath("$.gioiTinh").value(DEFAULT_GIOI_TINH))
            .andExpect(jsonPath("$.khangThe").value(DEFAULT_KHANG_THE))
            .andExpect(jsonPath("$.maSoThue").value(DEFAULT_MA_SO_THUE))
            .andExpect(jsonPath("$.ngayCapCmnd").value(DEFAULT_NGAY_CAP_CMND.toString()))
            .andExpect(jsonPath("$.ngaySinh").value(DEFAULT_NGAY_SINH.toString()))
            .andExpect(jsonPath("$.nhomMau").value(DEFAULT_NHOM_MAU))
            .andExpect(jsonPath("$.noiCapCmnd").value(DEFAULT_NOI_CAP_CMND))
            .andExpect(jsonPath("$.noiLamViec").value(DEFAULT_NOI_LAM_VIEC))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.soNhaXom").value(DEFAULT_SO_NHA_XOM))
            .andExpect(jsonPath("$.apThon").value(DEFAULT_AP_THON))
            .andExpect(jsonPath("$.diaPhuongId").value(DEFAULT_DIA_PHUONG_ID.intValue()))
            .andExpect(jsonPath("$.diaChiThuongTru").value(DEFAULT_DIA_CHI_THUONG_TRU));
    }


    @Test
    @Transactional
    public void getBenhNhansByIdFiltering() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        Long id = benhNhan.getId();

        defaultBenhNhanShouldBeFound("id.equals=" + id);
        defaultBenhNhanShouldNotBeFound("id.notEquals=" + id);

        defaultBenhNhanShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultBenhNhanShouldNotBeFound("id.greaterThan=" + id);

        defaultBenhNhanShouldBeFound("id.lessThanOrEqual=" + id);
        defaultBenhNhanShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByChiCoNamSinhIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where chiCoNamSinh equals to DEFAULT_CHI_CO_NAM_SINH
        defaultBenhNhanShouldBeFound("chiCoNamSinh.equals=" + DEFAULT_CHI_CO_NAM_SINH);

        // Get all the benhNhanList where chiCoNamSinh equals to UPDATED_CHI_CO_NAM_SINH
        defaultBenhNhanShouldNotBeFound("chiCoNamSinh.equals=" + UPDATED_CHI_CO_NAM_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByChiCoNamSinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where chiCoNamSinh not equals to DEFAULT_CHI_CO_NAM_SINH
        defaultBenhNhanShouldNotBeFound("chiCoNamSinh.notEquals=" + DEFAULT_CHI_CO_NAM_SINH);

        // Get all the benhNhanList where chiCoNamSinh not equals to UPDATED_CHI_CO_NAM_SINH
        defaultBenhNhanShouldBeFound("chiCoNamSinh.notEquals=" + UPDATED_CHI_CO_NAM_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByChiCoNamSinhIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where chiCoNamSinh in DEFAULT_CHI_CO_NAM_SINH or UPDATED_CHI_CO_NAM_SINH
        defaultBenhNhanShouldBeFound("chiCoNamSinh.in=" + DEFAULT_CHI_CO_NAM_SINH + "," + UPDATED_CHI_CO_NAM_SINH);

        // Get all the benhNhanList where chiCoNamSinh equals to UPDATED_CHI_CO_NAM_SINH
        defaultBenhNhanShouldNotBeFound("chiCoNamSinh.in=" + UPDATED_CHI_CO_NAM_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByChiCoNamSinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where chiCoNamSinh is not null
        defaultBenhNhanShouldBeFound("chiCoNamSinh.specified=true");

        // Get all the benhNhanList where chiCoNamSinh is null
        defaultBenhNhanShouldNotBeFound("chiCoNamSinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhNhansByCmndIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where cmnd equals to DEFAULT_CMND
        defaultBenhNhanShouldBeFound("cmnd.equals=" + DEFAULT_CMND);

        // Get all the benhNhanList where cmnd equals to UPDATED_CMND
        defaultBenhNhanShouldNotBeFound("cmnd.equals=" + UPDATED_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByCmndIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where cmnd not equals to DEFAULT_CMND
        defaultBenhNhanShouldNotBeFound("cmnd.notEquals=" + DEFAULT_CMND);

        // Get all the benhNhanList where cmnd not equals to UPDATED_CMND
        defaultBenhNhanShouldBeFound("cmnd.notEquals=" + UPDATED_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByCmndIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where cmnd in DEFAULT_CMND or UPDATED_CMND
        defaultBenhNhanShouldBeFound("cmnd.in=" + DEFAULT_CMND + "," + UPDATED_CMND);

        // Get all the benhNhanList where cmnd equals to UPDATED_CMND
        defaultBenhNhanShouldNotBeFound("cmnd.in=" + UPDATED_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByCmndIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where cmnd is not null
        defaultBenhNhanShouldBeFound("cmnd.specified=true");

        // Get all the benhNhanList where cmnd is null
        defaultBenhNhanShouldNotBeFound("cmnd.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhNhansByCmndContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where cmnd contains DEFAULT_CMND
        defaultBenhNhanShouldBeFound("cmnd.contains=" + DEFAULT_CMND);

        // Get all the benhNhanList where cmnd contains UPDATED_CMND
        defaultBenhNhanShouldNotBeFound("cmnd.contains=" + UPDATED_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByCmndNotContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where cmnd does not contain DEFAULT_CMND
        defaultBenhNhanShouldNotBeFound("cmnd.doesNotContain=" + DEFAULT_CMND);

        // Get all the benhNhanList where cmnd does not contain UPDATED_CMND
        defaultBenhNhanShouldBeFound("cmnd.doesNotContain=" + UPDATED_CMND);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where email equals to DEFAULT_EMAIL
        defaultBenhNhanShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the benhNhanList where email equals to UPDATED_EMAIL
        defaultBenhNhanShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where email not equals to DEFAULT_EMAIL
        defaultBenhNhanShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the benhNhanList where email not equals to UPDATED_EMAIL
        defaultBenhNhanShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultBenhNhanShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the benhNhanList where email equals to UPDATED_EMAIL
        defaultBenhNhanShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where email is not null
        defaultBenhNhanShouldBeFound("email.specified=true");

        // Get all the benhNhanList where email is null
        defaultBenhNhanShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhNhansByEmailContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where email contains DEFAULT_EMAIL
        defaultBenhNhanShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the benhNhanList where email contains UPDATED_EMAIL
        defaultBenhNhanShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where email does not contain DEFAULT_EMAIL
        defaultBenhNhanShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the benhNhanList where email does not contain UPDATED_EMAIL
        defaultBenhNhanShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where enabled equals to DEFAULT_ENABLED
        defaultBenhNhanShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the benhNhanList where enabled equals to UPDATED_ENABLED
        defaultBenhNhanShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where enabled not equals to DEFAULT_ENABLED
        defaultBenhNhanShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the benhNhanList where enabled not equals to UPDATED_ENABLED
        defaultBenhNhanShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultBenhNhanShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the benhNhanList where enabled equals to UPDATED_ENABLED
        defaultBenhNhanShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where enabled is not null
        defaultBenhNhanShouldBeFound("enabled.specified=true");

        // Get all the benhNhanList where enabled is null
        defaultBenhNhanShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhNhansByGioiTinhIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where gioiTinh equals to DEFAULT_GIOI_TINH
        defaultBenhNhanShouldBeFound("gioiTinh.equals=" + DEFAULT_GIOI_TINH);

        // Get all the benhNhanList where gioiTinh equals to UPDATED_GIOI_TINH
        defaultBenhNhanShouldNotBeFound("gioiTinh.equals=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByGioiTinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where gioiTinh not equals to DEFAULT_GIOI_TINH
        defaultBenhNhanShouldNotBeFound("gioiTinh.notEquals=" + DEFAULT_GIOI_TINH);

        // Get all the benhNhanList where gioiTinh not equals to UPDATED_GIOI_TINH
        defaultBenhNhanShouldBeFound("gioiTinh.notEquals=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByGioiTinhIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where gioiTinh in DEFAULT_GIOI_TINH or UPDATED_GIOI_TINH
        defaultBenhNhanShouldBeFound("gioiTinh.in=" + DEFAULT_GIOI_TINH + "," + UPDATED_GIOI_TINH);

        // Get all the benhNhanList where gioiTinh equals to UPDATED_GIOI_TINH
        defaultBenhNhanShouldNotBeFound("gioiTinh.in=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByGioiTinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where gioiTinh is not null
        defaultBenhNhanShouldBeFound("gioiTinh.specified=true");

        // Get all the benhNhanList where gioiTinh is null
        defaultBenhNhanShouldNotBeFound("gioiTinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhNhansByGioiTinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where gioiTinh is greater than or equal to DEFAULT_GIOI_TINH
        defaultBenhNhanShouldBeFound("gioiTinh.greaterThanOrEqual=" + DEFAULT_GIOI_TINH);

        // Get all the benhNhanList where gioiTinh is greater than or equal to UPDATED_GIOI_TINH
        defaultBenhNhanShouldNotBeFound("gioiTinh.greaterThanOrEqual=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByGioiTinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where gioiTinh is less than or equal to DEFAULT_GIOI_TINH
        defaultBenhNhanShouldBeFound("gioiTinh.lessThanOrEqual=" + DEFAULT_GIOI_TINH);

        // Get all the benhNhanList where gioiTinh is less than or equal to SMALLER_GIOI_TINH
        defaultBenhNhanShouldNotBeFound("gioiTinh.lessThanOrEqual=" + SMALLER_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByGioiTinhIsLessThanSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where gioiTinh is less than DEFAULT_GIOI_TINH
        defaultBenhNhanShouldNotBeFound("gioiTinh.lessThan=" + DEFAULT_GIOI_TINH);

        // Get all the benhNhanList where gioiTinh is less than UPDATED_GIOI_TINH
        defaultBenhNhanShouldBeFound("gioiTinh.lessThan=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByGioiTinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where gioiTinh is greater than DEFAULT_GIOI_TINH
        defaultBenhNhanShouldNotBeFound("gioiTinh.greaterThan=" + DEFAULT_GIOI_TINH);

        // Get all the benhNhanList where gioiTinh is greater than SMALLER_GIOI_TINH
        defaultBenhNhanShouldBeFound("gioiTinh.greaterThan=" + SMALLER_GIOI_TINH);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByKhangTheIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where khangThe equals to DEFAULT_KHANG_THE
        defaultBenhNhanShouldBeFound("khangThe.equals=" + DEFAULT_KHANG_THE);

        // Get all the benhNhanList where khangThe equals to UPDATED_KHANG_THE
        defaultBenhNhanShouldNotBeFound("khangThe.equals=" + UPDATED_KHANG_THE);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByKhangTheIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where khangThe not equals to DEFAULT_KHANG_THE
        defaultBenhNhanShouldNotBeFound("khangThe.notEquals=" + DEFAULT_KHANG_THE);

        // Get all the benhNhanList where khangThe not equals to UPDATED_KHANG_THE
        defaultBenhNhanShouldBeFound("khangThe.notEquals=" + UPDATED_KHANG_THE);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByKhangTheIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where khangThe in DEFAULT_KHANG_THE or UPDATED_KHANG_THE
        defaultBenhNhanShouldBeFound("khangThe.in=" + DEFAULT_KHANG_THE + "," + UPDATED_KHANG_THE);

        // Get all the benhNhanList where khangThe equals to UPDATED_KHANG_THE
        defaultBenhNhanShouldNotBeFound("khangThe.in=" + UPDATED_KHANG_THE);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByKhangTheIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where khangThe is not null
        defaultBenhNhanShouldBeFound("khangThe.specified=true");

        // Get all the benhNhanList where khangThe is null
        defaultBenhNhanShouldNotBeFound("khangThe.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhNhansByKhangTheContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where khangThe contains DEFAULT_KHANG_THE
        defaultBenhNhanShouldBeFound("khangThe.contains=" + DEFAULT_KHANG_THE);

        // Get all the benhNhanList where khangThe contains UPDATED_KHANG_THE
        defaultBenhNhanShouldNotBeFound("khangThe.contains=" + UPDATED_KHANG_THE);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByKhangTheNotContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where khangThe does not contain DEFAULT_KHANG_THE
        defaultBenhNhanShouldNotBeFound("khangThe.doesNotContain=" + DEFAULT_KHANG_THE);

        // Get all the benhNhanList where khangThe does not contain UPDATED_KHANG_THE
        defaultBenhNhanShouldBeFound("khangThe.doesNotContain=" + UPDATED_KHANG_THE);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByMaSoThueIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where maSoThue equals to DEFAULT_MA_SO_THUE
        defaultBenhNhanShouldBeFound("maSoThue.equals=" + DEFAULT_MA_SO_THUE);

        // Get all the benhNhanList where maSoThue equals to UPDATED_MA_SO_THUE
        defaultBenhNhanShouldNotBeFound("maSoThue.equals=" + UPDATED_MA_SO_THUE);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByMaSoThueIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where maSoThue not equals to DEFAULT_MA_SO_THUE
        defaultBenhNhanShouldNotBeFound("maSoThue.notEquals=" + DEFAULT_MA_SO_THUE);

        // Get all the benhNhanList where maSoThue not equals to UPDATED_MA_SO_THUE
        defaultBenhNhanShouldBeFound("maSoThue.notEquals=" + UPDATED_MA_SO_THUE);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByMaSoThueIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where maSoThue in DEFAULT_MA_SO_THUE or UPDATED_MA_SO_THUE
        defaultBenhNhanShouldBeFound("maSoThue.in=" + DEFAULT_MA_SO_THUE + "," + UPDATED_MA_SO_THUE);

        // Get all the benhNhanList where maSoThue equals to UPDATED_MA_SO_THUE
        defaultBenhNhanShouldNotBeFound("maSoThue.in=" + UPDATED_MA_SO_THUE);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByMaSoThueIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where maSoThue is not null
        defaultBenhNhanShouldBeFound("maSoThue.specified=true");

        // Get all the benhNhanList where maSoThue is null
        defaultBenhNhanShouldNotBeFound("maSoThue.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhNhansByMaSoThueContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where maSoThue contains DEFAULT_MA_SO_THUE
        defaultBenhNhanShouldBeFound("maSoThue.contains=" + DEFAULT_MA_SO_THUE);

        // Get all the benhNhanList where maSoThue contains UPDATED_MA_SO_THUE
        defaultBenhNhanShouldNotBeFound("maSoThue.contains=" + UPDATED_MA_SO_THUE);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByMaSoThueNotContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where maSoThue does not contain DEFAULT_MA_SO_THUE
        defaultBenhNhanShouldNotBeFound("maSoThue.doesNotContain=" + DEFAULT_MA_SO_THUE);

        // Get all the benhNhanList where maSoThue does not contain UPDATED_MA_SO_THUE
        defaultBenhNhanShouldBeFound("maSoThue.doesNotContain=" + UPDATED_MA_SO_THUE);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByNgayCapCmndIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngayCapCmnd equals to DEFAULT_NGAY_CAP_CMND
        defaultBenhNhanShouldBeFound("ngayCapCmnd.equals=" + DEFAULT_NGAY_CAP_CMND);

        // Get all the benhNhanList where ngayCapCmnd equals to UPDATED_NGAY_CAP_CMND
        defaultBenhNhanShouldNotBeFound("ngayCapCmnd.equals=" + UPDATED_NGAY_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgayCapCmndIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngayCapCmnd not equals to DEFAULT_NGAY_CAP_CMND
        defaultBenhNhanShouldNotBeFound("ngayCapCmnd.notEquals=" + DEFAULT_NGAY_CAP_CMND);

        // Get all the benhNhanList where ngayCapCmnd not equals to UPDATED_NGAY_CAP_CMND
        defaultBenhNhanShouldBeFound("ngayCapCmnd.notEquals=" + UPDATED_NGAY_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgayCapCmndIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngayCapCmnd in DEFAULT_NGAY_CAP_CMND or UPDATED_NGAY_CAP_CMND
        defaultBenhNhanShouldBeFound("ngayCapCmnd.in=" + DEFAULT_NGAY_CAP_CMND + "," + UPDATED_NGAY_CAP_CMND);

        // Get all the benhNhanList where ngayCapCmnd equals to UPDATED_NGAY_CAP_CMND
        defaultBenhNhanShouldNotBeFound("ngayCapCmnd.in=" + UPDATED_NGAY_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgayCapCmndIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngayCapCmnd is not null
        defaultBenhNhanShouldBeFound("ngayCapCmnd.specified=true");

        // Get all the benhNhanList where ngayCapCmnd is null
        defaultBenhNhanShouldNotBeFound("ngayCapCmnd.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgayCapCmndIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngayCapCmnd is greater than or equal to DEFAULT_NGAY_CAP_CMND
        defaultBenhNhanShouldBeFound("ngayCapCmnd.greaterThanOrEqual=" + DEFAULT_NGAY_CAP_CMND);

        // Get all the benhNhanList where ngayCapCmnd is greater than or equal to UPDATED_NGAY_CAP_CMND
        defaultBenhNhanShouldNotBeFound("ngayCapCmnd.greaterThanOrEqual=" + UPDATED_NGAY_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgayCapCmndIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngayCapCmnd is less than or equal to DEFAULT_NGAY_CAP_CMND
        defaultBenhNhanShouldBeFound("ngayCapCmnd.lessThanOrEqual=" + DEFAULT_NGAY_CAP_CMND);

        // Get all the benhNhanList where ngayCapCmnd is less than or equal to SMALLER_NGAY_CAP_CMND
        defaultBenhNhanShouldNotBeFound("ngayCapCmnd.lessThanOrEqual=" + SMALLER_NGAY_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgayCapCmndIsLessThanSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngayCapCmnd is less than DEFAULT_NGAY_CAP_CMND
        defaultBenhNhanShouldNotBeFound("ngayCapCmnd.lessThan=" + DEFAULT_NGAY_CAP_CMND);

        // Get all the benhNhanList where ngayCapCmnd is less than UPDATED_NGAY_CAP_CMND
        defaultBenhNhanShouldBeFound("ngayCapCmnd.lessThan=" + UPDATED_NGAY_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgayCapCmndIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngayCapCmnd is greater than DEFAULT_NGAY_CAP_CMND
        defaultBenhNhanShouldNotBeFound("ngayCapCmnd.greaterThan=" + DEFAULT_NGAY_CAP_CMND);

        // Get all the benhNhanList where ngayCapCmnd is greater than SMALLER_NGAY_CAP_CMND
        defaultBenhNhanShouldBeFound("ngayCapCmnd.greaterThan=" + SMALLER_NGAY_CAP_CMND);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByNgaySinhIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngaySinh equals to DEFAULT_NGAY_SINH
        defaultBenhNhanShouldBeFound("ngaySinh.equals=" + DEFAULT_NGAY_SINH);

        // Get all the benhNhanList where ngaySinh equals to UPDATED_NGAY_SINH
        defaultBenhNhanShouldNotBeFound("ngaySinh.equals=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgaySinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngaySinh not equals to DEFAULT_NGAY_SINH
        defaultBenhNhanShouldNotBeFound("ngaySinh.notEquals=" + DEFAULT_NGAY_SINH);

        // Get all the benhNhanList where ngaySinh not equals to UPDATED_NGAY_SINH
        defaultBenhNhanShouldBeFound("ngaySinh.notEquals=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgaySinhIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngaySinh in DEFAULT_NGAY_SINH or UPDATED_NGAY_SINH
        defaultBenhNhanShouldBeFound("ngaySinh.in=" + DEFAULT_NGAY_SINH + "," + UPDATED_NGAY_SINH);

        // Get all the benhNhanList where ngaySinh equals to UPDATED_NGAY_SINH
        defaultBenhNhanShouldNotBeFound("ngaySinh.in=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgaySinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngaySinh is not null
        defaultBenhNhanShouldBeFound("ngaySinh.specified=true");

        // Get all the benhNhanList where ngaySinh is null
        defaultBenhNhanShouldNotBeFound("ngaySinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgaySinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngaySinh is greater than or equal to DEFAULT_NGAY_SINH
        defaultBenhNhanShouldBeFound("ngaySinh.greaterThanOrEqual=" + DEFAULT_NGAY_SINH);

        // Get all the benhNhanList where ngaySinh is greater than or equal to UPDATED_NGAY_SINH
        defaultBenhNhanShouldNotBeFound("ngaySinh.greaterThanOrEqual=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgaySinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngaySinh is less than or equal to DEFAULT_NGAY_SINH
        defaultBenhNhanShouldBeFound("ngaySinh.lessThanOrEqual=" + DEFAULT_NGAY_SINH);

        // Get all the benhNhanList where ngaySinh is less than or equal to SMALLER_NGAY_SINH
        defaultBenhNhanShouldNotBeFound("ngaySinh.lessThanOrEqual=" + SMALLER_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgaySinhIsLessThanSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngaySinh is less than DEFAULT_NGAY_SINH
        defaultBenhNhanShouldNotBeFound("ngaySinh.lessThan=" + DEFAULT_NGAY_SINH);

        // Get all the benhNhanList where ngaySinh is less than UPDATED_NGAY_SINH
        defaultBenhNhanShouldBeFound("ngaySinh.lessThan=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNgaySinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ngaySinh is greater than DEFAULT_NGAY_SINH
        defaultBenhNhanShouldNotBeFound("ngaySinh.greaterThan=" + DEFAULT_NGAY_SINH);

        // Get all the benhNhanList where ngaySinh is greater than SMALLER_NGAY_SINH
        defaultBenhNhanShouldBeFound("ngaySinh.greaterThan=" + SMALLER_NGAY_SINH);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByNhomMauIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where nhomMau equals to DEFAULT_NHOM_MAU
        defaultBenhNhanShouldBeFound("nhomMau.equals=" + DEFAULT_NHOM_MAU);

        // Get all the benhNhanList where nhomMau equals to UPDATED_NHOM_MAU
        defaultBenhNhanShouldNotBeFound("nhomMau.equals=" + UPDATED_NHOM_MAU);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNhomMauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where nhomMau not equals to DEFAULT_NHOM_MAU
        defaultBenhNhanShouldNotBeFound("nhomMau.notEquals=" + DEFAULT_NHOM_MAU);

        // Get all the benhNhanList where nhomMau not equals to UPDATED_NHOM_MAU
        defaultBenhNhanShouldBeFound("nhomMau.notEquals=" + UPDATED_NHOM_MAU);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNhomMauIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where nhomMau in DEFAULT_NHOM_MAU or UPDATED_NHOM_MAU
        defaultBenhNhanShouldBeFound("nhomMau.in=" + DEFAULT_NHOM_MAU + "," + UPDATED_NHOM_MAU);

        // Get all the benhNhanList where nhomMau equals to UPDATED_NHOM_MAU
        defaultBenhNhanShouldNotBeFound("nhomMau.in=" + UPDATED_NHOM_MAU);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNhomMauIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where nhomMau is not null
        defaultBenhNhanShouldBeFound("nhomMau.specified=true");

        // Get all the benhNhanList where nhomMau is null
        defaultBenhNhanShouldNotBeFound("nhomMau.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhNhansByNhomMauContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where nhomMau contains DEFAULT_NHOM_MAU
        defaultBenhNhanShouldBeFound("nhomMau.contains=" + DEFAULT_NHOM_MAU);

        // Get all the benhNhanList where nhomMau contains UPDATED_NHOM_MAU
        defaultBenhNhanShouldNotBeFound("nhomMau.contains=" + UPDATED_NHOM_MAU);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNhomMauNotContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where nhomMau does not contain DEFAULT_NHOM_MAU
        defaultBenhNhanShouldNotBeFound("nhomMau.doesNotContain=" + DEFAULT_NHOM_MAU);

        // Get all the benhNhanList where nhomMau does not contain UPDATED_NHOM_MAU
        defaultBenhNhanShouldBeFound("nhomMau.doesNotContain=" + UPDATED_NHOM_MAU);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByNoiCapCmndIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where noiCapCmnd equals to DEFAULT_NOI_CAP_CMND
        defaultBenhNhanShouldBeFound("noiCapCmnd.equals=" + DEFAULT_NOI_CAP_CMND);

        // Get all the benhNhanList where noiCapCmnd equals to UPDATED_NOI_CAP_CMND
        defaultBenhNhanShouldNotBeFound("noiCapCmnd.equals=" + UPDATED_NOI_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNoiCapCmndIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where noiCapCmnd not equals to DEFAULT_NOI_CAP_CMND
        defaultBenhNhanShouldNotBeFound("noiCapCmnd.notEquals=" + DEFAULT_NOI_CAP_CMND);

        // Get all the benhNhanList where noiCapCmnd not equals to UPDATED_NOI_CAP_CMND
        defaultBenhNhanShouldBeFound("noiCapCmnd.notEquals=" + UPDATED_NOI_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNoiCapCmndIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where noiCapCmnd in DEFAULT_NOI_CAP_CMND or UPDATED_NOI_CAP_CMND
        defaultBenhNhanShouldBeFound("noiCapCmnd.in=" + DEFAULT_NOI_CAP_CMND + "," + UPDATED_NOI_CAP_CMND);

        // Get all the benhNhanList where noiCapCmnd equals to UPDATED_NOI_CAP_CMND
        defaultBenhNhanShouldNotBeFound("noiCapCmnd.in=" + UPDATED_NOI_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNoiCapCmndIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where noiCapCmnd is not null
        defaultBenhNhanShouldBeFound("noiCapCmnd.specified=true");

        // Get all the benhNhanList where noiCapCmnd is null
        defaultBenhNhanShouldNotBeFound("noiCapCmnd.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhNhansByNoiCapCmndContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where noiCapCmnd contains DEFAULT_NOI_CAP_CMND
        defaultBenhNhanShouldBeFound("noiCapCmnd.contains=" + DEFAULT_NOI_CAP_CMND);

        // Get all the benhNhanList where noiCapCmnd contains UPDATED_NOI_CAP_CMND
        defaultBenhNhanShouldNotBeFound("noiCapCmnd.contains=" + UPDATED_NOI_CAP_CMND);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNoiCapCmndNotContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where noiCapCmnd does not contain DEFAULT_NOI_CAP_CMND
        defaultBenhNhanShouldNotBeFound("noiCapCmnd.doesNotContain=" + DEFAULT_NOI_CAP_CMND);

        // Get all the benhNhanList where noiCapCmnd does not contain UPDATED_NOI_CAP_CMND
        defaultBenhNhanShouldBeFound("noiCapCmnd.doesNotContain=" + UPDATED_NOI_CAP_CMND);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByNoiLamViecIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where noiLamViec equals to DEFAULT_NOI_LAM_VIEC
        defaultBenhNhanShouldBeFound("noiLamViec.equals=" + DEFAULT_NOI_LAM_VIEC);

        // Get all the benhNhanList where noiLamViec equals to UPDATED_NOI_LAM_VIEC
        defaultBenhNhanShouldNotBeFound("noiLamViec.equals=" + UPDATED_NOI_LAM_VIEC);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNoiLamViecIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where noiLamViec not equals to DEFAULT_NOI_LAM_VIEC
        defaultBenhNhanShouldNotBeFound("noiLamViec.notEquals=" + DEFAULT_NOI_LAM_VIEC);

        // Get all the benhNhanList where noiLamViec not equals to UPDATED_NOI_LAM_VIEC
        defaultBenhNhanShouldBeFound("noiLamViec.notEquals=" + UPDATED_NOI_LAM_VIEC);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNoiLamViecIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where noiLamViec in DEFAULT_NOI_LAM_VIEC or UPDATED_NOI_LAM_VIEC
        defaultBenhNhanShouldBeFound("noiLamViec.in=" + DEFAULT_NOI_LAM_VIEC + "," + UPDATED_NOI_LAM_VIEC);

        // Get all the benhNhanList where noiLamViec equals to UPDATED_NOI_LAM_VIEC
        defaultBenhNhanShouldNotBeFound("noiLamViec.in=" + UPDATED_NOI_LAM_VIEC);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNoiLamViecIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where noiLamViec is not null
        defaultBenhNhanShouldBeFound("noiLamViec.specified=true");

        // Get all the benhNhanList where noiLamViec is null
        defaultBenhNhanShouldNotBeFound("noiLamViec.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhNhansByNoiLamViecContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where noiLamViec contains DEFAULT_NOI_LAM_VIEC
        defaultBenhNhanShouldBeFound("noiLamViec.contains=" + DEFAULT_NOI_LAM_VIEC);

        // Get all the benhNhanList where noiLamViec contains UPDATED_NOI_LAM_VIEC
        defaultBenhNhanShouldNotBeFound("noiLamViec.contains=" + UPDATED_NOI_LAM_VIEC);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByNoiLamViecNotContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where noiLamViec does not contain DEFAULT_NOI_LAM_VIEC
        defaultBenhNhanShouldNotBeFound("noiLamViec.doesNotContain=" + DEFAULT_NOI_LAM_VIEC);

        // Get all the benhNhanList where noiLamViec does not contain UPDATED_NOI_LAM_VIEC
        defaultBenhNhanShouldBeFound("noiLamViec.doesNotContain=" + UPDATED_NOI_LAM_VIEC);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where phone equals to DEFAULT_PHONE
        defaultBenhNhanShouldBeFound("phone.equals=" + DEFAULT_PHONE);

        // Get all the benhNhanList where phone equals to UPDATED_PHONE
        defaultBenhNhanShouldNotBeFound("phone.equals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByPhoneIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where phone not equals to DEFAULT_PHONE
        defaultBenhNhanShouldNotBeFound("phone.notEquals=" + DEFAULT_PHONE);

        // Get all the benhNhanList where phone not equals to UPDATED_PHONE
        defaultBenhNhanShouldBeFound("phone.notEquals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where phone in DEFAULT_PHONE or UPDATED_PHONE
        defaultBenhNhanShouldBeFound("phone.in=" + DEFAULT_PHONE + "," + UPDATED_PHONE);

        // Get all the benhNhanList where phone equals to UPDATED_PHONE
        defaultBenhNhanShouldNotBeFound("phone.in=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where phone is not null
        defaultBenhNhanShouldBeFound("phone.specified=true");

        // Get all the benhNhanList where phone is null
        defaultBenhNhanShouldNotBeFound("phone.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhNhansByPhoneContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where phone contains DEFAULT_PHONE
        defaultBenhNhanShouldBeFound("phone.contains=" + DEFAULT_PHONE);

        // Get all the benhNhanList where phone contains UPDATED_PHONE
        defaultBenhNhanShouldNotBeFound("phone.contains=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByPhoneNotContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where phone does not contain DEFAULT_PHONE
        defaultBenhNhanShouldNotBeFound("phone.doesNotContain=" + DEFAULT_PHONE);

        // Get all the benhNhanList where phone does not contain UPDATED_PHONE
        defaultBenhNhanShouldBeFound("phone.doesNotContain=" + UPDATED_PHONE);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ten equals to DEFAULT_TEN
        defaultBenhNhanShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the benhNhanList where ten equals to UPDATED_TEN
        defaultBenhNhanShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ten not equals to DEFAULT_TEN
        defaultBenhNhanShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the benhNhanList where ten not equals to UPDATED_TEN
        defaultBenhNhanShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByTenIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultBenhNhanShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the benhNhanList where ten equals to UPDATED_TEN
        defaultBenhNhanShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ten is not null
        defaultBenhNhanShouldBeFound("ten.specified=true");

        // Get all the benhNhanList where ten is null
        defaultBenhNhanShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhNhansByTenContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ten contains DEFAULT_TEN
        defaultBenhNhanShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the benhNhanList where ten contains UPDATED_TEN
        defaultBenhNhanShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByTenNotContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where ten does not contain DEFAULT_TEN
        defaultBenhNhanShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the benhNhanList where ten does not contain UPDATED_TEN
        defaultBenhNhanShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllBenhNhansBySoNhaXomIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where soNhaXom equals to DEFAULT_SO_NHA_XOM
        defaultBenhNhanShouldBeFound("soNhaXom.equals=" + DEFAULT_SO_NHA_XOM);

        // Get all the benhNhanList where soNhaXom equals to UPDATED_SO_NHA_XOM
        defaultBenhNhanShouldNotBeFound("soNhaXom.equals=" + UPDATED_SO_NHA_XOM);
    }

    @Test
    @Transactional
    public void getAllBenhNhansBySoNhaXomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where soNhaXom not equals to DEFAULT_SO_NHA_XOM
        defaultBenhNhanShouldNotBeFound("soNhaXom.notEquals=" + DEFAULT_SO_NHA_XOM);

        // Get all the benhNhanList where soNhaXom not equals to UPDATED_SO_NHA_XOM
        defaultBenhNhanShouldBeFound("soNhaXom.notEquals=" + UPDATED_SO_NHA_XOM);
    }

    @Test
    @Transactional
    public void getAllBenhNhansBySoNhaXomIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where soNhaXom in DEFAULT_SO_NHA_XOM or UPDATED_SO_NHA_XOM
        defaultBenhNhanShouldBeFound("soNhaXom.in=" + DEFAULT_SO_NHA_XOM + "," + UPDATED_SO_NHA_XOM);

        // Get all the benhNhanList where soNhaXom equals to UPDATED_SO_NHA_XOM
        defaultBenhNhanShouldNotBeFound("soNhaXom.in=" + UPDATED_SO_NHA_XOM);
    }

    @Test
    @Transactional
    public void getAllBenhNhansBySoNhaXomIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where soNhaXom is not null
        defaultBenhNhanShouldBeFound("soNhaXom.specified=true");

        // Get all the benhNhanList where soNhaXom is null
        defaultBenhNhanShouldNotBeFound("soNhaXom.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhNhansBySoNhaXomContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where soNhaXom contains DEFAULT_SO_NHA_XOM
        defaultBenhNhanShouldBeFound("soNhaXom.contains=" + DEFAULT_SO_NHA_XOM);

        // Get all the benhNhanList where soNhaXom contains UPDATED_SO_NHA_XOM
        defaultBenhNhanShouldNotBeFound("soNhaXom.contains=" + UPDATED_SO_NHA_XOM);
    }

    @Test
    @Transactional
    public void getAllBenhNhansBySoNhaXomNotContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where soNhaXom does not contain DEFAULT_SO_NHA_XOM
        defaultBenhNhanShouldNotBeFound("soNhaXom.doesNotContain=" + DEFAULT_SO_NHA_XOM);

        // Get all the benhNhanList where soNhaXom does not contain UPDATED_SO_NHA_XOM
        defaultBenhNhanShouldBeFound("soNhaXom.doesNotContain=" + UPDATED_SO_NHA_XOM);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByApThonIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where apThon equals to DEFAULT_AP_THON
        defaultBenhNhanShouldBeFound("apThon.equals=" + DEFAULT_AP_THON);

        // Get all the benhNhanList where apThon equals to UPDATED_AP_THON
        defaultBenhNhanShouldNotBeFound("apThon.equals=" + UPDATED_AP_THON);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByApThonIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where apThon not equals to DEFAULT_AP_THON
        defaultBenhNhanShouldNotBeFound("apThon.notEquals=" + DEFAULT_AP_THON);

        // Get all the benhNhanList where apThon not equals to UPDATED_AP_THON
        defaultBenhNhanShouldBeFound("apThon.notEquals=" + UPDATED_AP_THON);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByApThonIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where apThon in DEFAULT_AP_THON or UPDATED_AP_THON
        defaultBenhNhanShouldBeFound("apThon.in=" + DEFAULT_AP_THON + "," + UPDATED_AP_THON);

        // Get all the benhNhanList where apThon equals to UPDATED_AP_THON
        defaultBenhNhanShouldNotBeFound("apThon.in=" + UPDATED_AP_THON);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByApThonIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where apThon is not null
        defaultBenhNhanShouldBeFound("apThon.specified=true");

        // Get all the benhNhanList where apThon is null
        defaultBenhNhanShouldNotBeFound("apThon.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhNhansByApThonContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where apThon contains DEFAULT_AP_THON
        defaultBenhNhanShouldBeFound("apThon.contains=" + DEFAULT_AP_THON);

        // Get all the benhNhanList where apThon contains UPDATED_AP_THON
        defaultBenhNhanShouldNotBeFound("apThon.contains=" + UPDATED_AP_THON);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByApThonNotContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where apThon does not contain DEFAULT_AP_THON
        defaultBenhNhanShouldNotBeFound("apThon.doesNotContain=" + DEFAULT_AP_THON);

        // Get all the benhNhanList where apThon does not contain UPDATED_AP_THON
        defaultBenhNhanShouldBeFound("apThon.doesNotContain=" + UPDATED_AP_THON);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByDiaPhuongIdIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaPhuongId equals to DEFAULT_DIA_PHUONG_ID
        defaultBenhNhanShouldBeFound("diaPhuongId.equals=" + DEFAULT_DIA_PHUONG_ID);

        // Get all the benhNhanList where diaPhuongId equals to UPDATED_DIA_PHUONG_ID
        defaultBenhNhanShouldNotBeFound("diaPhuongId.equals=" + UPDATED_DIA_PHUONG_ID);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByDiaPhuongIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaPhuongId not equals to DEFAULT_DIA_PHUONG_ID
        defaultBenhNhanShouldNotBeFound("diaPhuongId.notEquals=" + DEFAULT_DIA_PHUONG_ID);

        // Get all the benhNhanList where diaPhuongId not equals to UPDATED_DIA_PHUONG_ID
        defaultBenhNhanShouldBeFound("diaPhuongId.notEquals=" + UPDATED_DIA_PHUONG_ID);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByDiaPhuongIdIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaPhuongId in DEFAULT_DIA_PHUONG_ID or UPDATED_DIA_PHUONG_ID
        defaultBenhNhanShouldBeFound("diaPhuongId.in=" + DEFAULT_DIA_PHUONG_ID + "," + UPDATED_DIA_PHUONG_ID);

        // Get all the benhNhanList where diaPhuongId equals to UPDATED_DIA_PHUONG_ID
        defaultBenhNhanShouldNotBeFound("diaPhuongId.in=" + UPDATED_DIA_PHUONG_ID);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByDiaPhuongIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaPhuongId is not null
        defaultBenhNhanShouldBeFound("diaPhuongId.specified=true");

        // Get all the benhNhanList where diaPhuongId is null
        defaultBenhNhanShouldNotBeFound("diaPhuongId.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhNhansByDiaPhuongIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaPhuongId is greater than or equal to DEFAULT_DIA_PHUONG_ID
        defaultBenhNhanShouldBeFound("diaPhuongId.greaterThanOrEqual=" + DEFAULT_DIA_PHUONG_ID);

        // Get all the benhNhanList where diaPhuongId is greater than or equal to UPDATED_DIA_PHUONG_ID
        defaultBenhNhanShouldNotBeFound("diaPhuongId.greaterThanOrEqual=" + UPDATED_DIA_PHUONG_ID);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByDiaPhuongIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaPhuongId is less than or equal to DEFAULT_DIA_PHUONG_ID
        defaultBenhNhanShouldBeFound("diaPhuongId.lessThanOrEqual=" + DEFAULT_DIA_PHUONG_ID);

        // Get all the benhNhanList where diaPhuongId is less than or equal to SMALLER_DIA_PHUONG_ID
        defaultBenhNhanShouldNotBeFound("diaPhuongId.lessThanOrEqual=" + SMALLER_DIA_PHUONG_ID);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByDiaPhuongIdIsLessThanSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaPhuongId is less than DEFAULT_DIA_PHUONG_ID
        defaultBenhNhanShouldNotBeFound("diaPhuongId.lessThan=" + DEFAULT_DIA_PHUONG_ID);

        // Get all the benhNhanList where diaPhuongId is less than UPDATED_DIA_PHUONG_ID
        defaultBenhNhanShouldBeFound("diaPhuongId.lessThan=" + UPDATED_DIA_PHUONG_ID);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByDiaPhuongIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaPhuongId is greater than DEFAULT_DIA_PHUONG_ID
        defaultBenhNhanShouldNotBeFound("diaPhuongId.greaterThan=" + DEFAULT_DIA_PHUONG_ID);

        // Get all the benhNhanList where diaPhuongId is greater than SMALLER_DIA_PHUONG_ID
        defaultBenhNhanShouldBeFound("diaPhuongId.greaterThan=" + SMALLER_DIA_PHUONG_ID);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByDiaChiThuongTruIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaChiThuongTru equals to DEFAULT_DIA_CHI_THUONG_TRU
        defaultBenhNhanShouldBeFound("diaChiThuongTru.equals=" + DEFAULT_DIA_CHI_THUONG_TRU);

        // Get all the benhNhanList where diaChiThuongTru equals to UPDATED_DIA_CHI_THUONG_TRU
        defaultBenhNhanShouldNotBeFound("diaChiThuongTru.equals=" + UPDATED_DIA_CHI_THUONG_TRU);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByDiaChiThuongTruIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaChiThuongTru not equals to DEFAULT_DIA_CHI_THUONG_TRU
        defaultBenhNhanShouldNotBeFound("diaChiThuongTru.notEquals=" + DEFAULT_DIA_CHI_THUONG_TRU);

        // Get all the benhNhanList where diaChiThuongTru not equals to UPDATED_DIA_CHI_THUONG_TRU
        defaultBenhNhanShouldBeFound("diaChiThuongTru.notEquals=" + UPDATED_DIA_CHI_THUONG_TRU);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByDiaChiThuongTruIsInShouldWork() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaChiThuongTru in DEFAULT_DIA_CHI_THUONG_TRU or UPDATED_DIA_CHI_THUONG_TRU
        defaultBenhNhanShouldBeFound("diaChiThuongTru.in=" + DEFAULT_DIA_CHI_THUONG_TRU + "," + UPDATED_DIA_CHI_THUONG_TRU);

        // Get all the benhNhanList where diaChiThuongTru equals to UPDATED_DIA_CHI_THUONG_TRU
        defaultBenhNhanShouldNotBeFound("diaChiThuongTru.in=" + UPDATED_DIA_CHI_THUONG_TRU);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByDiaChiThuongTruIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaChiThuongTru is not null
        defaultBenhNhanShouldBeFound("diaChiThuongTru.specified=true");

        // Get all the benhNhanList where diaChiThuongTru is null
        defaultBenhNhanShouldNotBeFound("diaChiThuongTru.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhNhansByDiaChiThuongTruContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaChiThuongTru contains DEFAULT_DIA_CHI_THUONG_TRU
        defaultBenhNhanShouldBeFound("diaChiThuongTru.contains=" + DEFAULT_DIA_CHI_THUONG_TRU);

        // Get all the benhNhanList where diaChiThuongTru contains UPDATED_DIA_CHI_THUONG_TRU
        defaultBenhNhanShouldNotBeFound("diaChiThuongTru.contains=" + UPDATED_DIA_CHI_THUONG_TRU);
    }

    @Test
    @Transactional
    public void getAllBenhNhansByDiaChiThuongTruNotContainsSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        // Get all the benhNhanList where diaChiThuongTru does not contain DEFAULT_DIA_CHI_THUONG_TRU
        defaultBenhNhanShouldNotBeFound("diaChiThuongTru.doesNotContain=" + DEFAULT_DIA_CHI_THUONG_TRU);

        // Get all the benhNhanList where diaChiThuongTru does not contain UPDATED_DIA_CHI_THUONG_TRU
        defaultBenhNhanShouldBeFound("diaChiThuongTru.doesNotContain=" + UPDATED_DIA_CHI_THUONG_TRU);
    }


    @Test
    @Transactional
    public void getAllBenhNhansByDanTocIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);
        DanToc danToc = DanTocResourceIT.createEntity(em);
        em.persist(danToc);
        em.flush();
        benhNhan.setDanToc(danToc);
        benhNhanRepository.saveAndFlush(benhNhan);
        Long danTocId = danToc.getId();

        // Get all the benhNhanList where danToc equals to danTocId
        defaultBenhNhanShouldBeFound("danTocId.equals=" + danTocId);

        // Get all the benhNhanList where danToc equals to danTocId + 1
        defaultBenhNhanShouldNotBeFound("danTocId.equals=" + (danTocId + 1));
    }


    @Test
    @Transactional
    public void getAllBenhNhansByNgheNghiepIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);
        NgheNghiep ngheNghiep = NgheNghiepResourceIT.createEntity(em);
        em.persist(ngheNghiep);
        em.flush();
        benhNhan.setNgheNghiep(ngheNghiep);
        benhNhanRepository.saveAndFlush(benhNhan);
        Long ngheNghiepId = ngheNghiep.getId();

        // Get all the benhNhanList where ngheNghiep equals to ngheNghiepId
        defaultBenhNhanShouldBeFound("ngheNghiepId.equals=" + ngheNghiepId);

        // Get all the benhNhanList where ngheNghiep equals to ngheNghiepId + 1
        defaultBenhNhanShouldNotBeFound("ngheNghiepId.equals=" + (ngheNghiepId + 1));
    }


    @Test
    @Transactional
    public void getAllBenhNhansByQuocTichIsEqualToSomething() throws Exception {
        // Get already existing entity
        Country quocTich = benhNhan.getQuocTich();
        benhNhanRepository.saveAndFlush(benhNhan);
        Long quocTichId = quocTich.getId();

        // Get all the benhNhanList where quocTich equals to quocTichId
        defaultBenhNhanShouldBeFound("quocTichId.equals=" + quocTichId);

        // Get all the benhNhanList where quocTich equals to quocTichId + 1
        defaultBenhNhanShouldNotBeFound("quocTichId.equals=" + (quocTichId + 1));
    }


    @Test
    @Transactional
    public void getAllBenhNhansByPhuongXaIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);
        PhuongXa phuongXa = PhuongXaResourceIT.createEntity(em);
        em.persist(phuongXa);
        em.flush();
        benhNhan.setPhuongXa(phuongXa);
        benhNhanRepository.saveAndFlush(benhNhan);
        Long phuongXaId = phuongXa.getId();

        // Get all the benhNhanList where phuongXa equals to phuongXaId
        defaultBenhNhanShouldBeFound("phuongXaId.equals=" + phuongXaId);

        // Get all the benhNhanList where phuongXa equals to phuongXaId + 1
        defaultBenhNhanShouldNotBeFound("phuongXaId.equals=" + (phuongXaId + 1));
    }


    @Test
    @Transactional
    public void getAllBenhNhansByQuanHuyenIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);
        QuanHuyen quanHuyen = QuanHuyenResourceIT.createEntity(em);
        em.persist(quanHuyen);
        em.flush();
        benhNhan.setQuanHuyen(quanHuyen);
        benhNhanRepository.saveAndFlush(benhNhan);
        Long quanHuyenId = quanHuyen.getId();

        // Get all the benhNhanList where quanHuyen equals to quanHuyenId
        defaultBenhNhanShouldBeFound("quanHuyenId.equals=" + quanHuyenId);

        // Get all the benhNhanList where quanHuyen equals to quanHuyenId + 1
        defaultBenhNhanShouldNotBeFound("quanHuyenId.equals=" + (quanHuyenId + 1));
    }


    @Test
    @Transactional
    public void getAllBenhNhansByTinhThanhPhoIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);
        TinhThanhPho tinhThanhPho = TinhThanhPhoResourceIT.createEntity(em);
        em.persist(tinhThanhPho);
        em.flush();
        benhNhan.setTinhThanhPho(tinhThanhPho);
        benhNhanRepository.saveAndFlush(benhNhan);
        Long tinhThanhPhoId = tinhThanhPho.getId();

        // Get all the benhNhanList where tinhThanhPho equals to tinhThanhPhoId
        defaultBenhNhanShouldBeFound("tinhThanhPhoId.equals=" + tinhThanhPhoId);

        // Get all the benhNhanList where tinhThanhPho equals to tinhThanhPhoId + 1
        defaultBenhNhanShouldNotBeFound("tinhThanhPhoId.equals=" + (tinhThanhPhoId + 1));
    }


    @Test
    @Transactional
    public void getAllBenhNhansByUserExtraIsEqualToSomething() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);
        UserExtra userExtra = UserExtraResourceIT.createEntity(em);
        em.persist(userExtra);
        em.flush();
        benhNhan.setUserExtra(userExtra);
        benhNhanRepository.saveAndFlush(benhNhan);
        Long userExtraId = userExtra.getId();

        // Get all the benhNhanList where userExtra equals to userExtraId
        defaultBenhNhanShouldBeFound("userExtraId.equals=" + userExtraId);

        // Get all the benhNhanList where userExtra equals to userExtraId + 1
        defaultBenhNhanShouldNotBeFound("userExtraId.equals=" + (userExtraId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBenhNhanShouldBeFound(String filter) throws Exception {
        restBenhNhanMockMvc.perform(get("/api/benh-nhans?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(benhNhan.getId().intValue())))
            .andExpect(jsonPath("$.[*].chiCoNamSinh").value(hasItem(DEFAULT_CHI_CO_NAM_SINH.booleanValue())))
            .andExpect(jsonPath("$.[*].cmnd").value(hasItem(DEFAULT_CMND)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].gioiTinh").value(hasItem(DEFAULT_GIOI_TINH)))
            .andExpect(jsonPath("$.[*].khangThe").value(hasItem(DEFAULT_KHANG_THE)))
            .andExpect(jsonPath("$.[*].maSoThue").value(hasItem(DEFAULT_MA_SO_THUE)))
            .andExpect(jsonPath("$.[*].ngayCapCmnd").value(hasItem(DEFAULT_NGAY_CAP_CMND.toString())))
            .andExpect(jsonPath("$.[*].ngaySinh").value(hasItem(DEFAULT_NGAY_SINH.toString())))
            .andExpect(jsonPath("$.[*].nhomMau").value(hasItem(DEFAULT_NHOM_MAU)))
            .andExpect(jsonPath("$.[*].noiCapCmnd").value(hasItem(DEFAULT_NOI_CAP_CMND)))
            .andExpect(jsonPath("$.[*].noiLamViec").value(hasItem(DEFAULT_NOI_LAM_VIEC)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].soNhaXom").value(hasItem(DEFAULT_SO_NHA_XOM)))
            .andExpect(jsonPath("$.[*].apThon").value(hasItem(DEFAULT_AP_THON)))
            .andExpect(jsonPath("$.[*].diaPhuongId").value(hasItem(DEFAULT_DIA_PHUONG_ID.intValue())))
            .andExpect(jsonPath("$.[*].diaChiThuongTru").value(hasItem(DEFAULT_DIA_CHI_THUONG_TRU)));

        // Check, that the count call also returns 1
        restBenhNhanMockMvc.perform(get("/api/benh-nhans/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBenhNhanShouldNotBeFound(String filter) throws Exception {
        restBenhNhanMockMvc.perform(get("/api/benh-nhans?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBenhNhanMockMvc.perform(get("/api/benh-nhans/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingBenhNhan() throws Exception {
        // Get the benhNhan
        restBenhNhanMockMvc.perform(get("/api/benh-nhans/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBenhNhan() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        int databaseSizeBeforeUpdate = benhNhanRepository.findAll().size();

        // Update the benhNhan
        BenhNhan updatedBenhNhan = benhNhanRepository.findById(benhNhan.getId()).get();
        // Disconnect from session so that the updates on updatedBenhNhan are not directly saved in db
        em.detach(updatedBenhNhan);
        updatedBenhNhan
            .chiCoNamSinh(UPDATED_CHI_CO_NAM_SINH)
            .cmnd(UPDATED_CMND)
            .email(UPDATED_EMAIL)
            .enabled(UPDATED_ENABLED)
            .gioiTinh(UPDATED_GIOI_TINH)
            .khangThe(UPDATED_KHANG_THE)
            .maSoThue(UPDATED_MA_SO_THUE)
            .ngayCapCmnd(UPDATED_NGAY_CAP_CMND)
            .ngaySinh(UPDATED_NGAY_SINH)
            .nhomMau(UPDATED_NHOM_MAU)
            .noiCapCmnd(UPDATED_NOI_CAP_CMND)
            .noiLamViec(UPDATED_NOI_LAM_VIEC)
            .phone(UPDATED_PHONE)
            .ten(UPDATED_TEN)
            .soNhaXom(UPDATED_SO_NHA_XOM)
            .apThon(UPDATED_AP_THON)
            .diaPhuongId(UPDATED_DIA_PHUONG_ID)
            .diaChiThuongTru(UPDATED_DIA_CHI_THUONG_TRU);
        BenhNhanDTO benhNhanDTO = benhNhanMapper.toDto(updatedBenhNhan);

        restBenhNhanMockMvc.perform(put("/api/benh-nhans").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhNhanDTO)))
            .andExpect(status().isOk());

        // Validate the BenhNhan in the database
        List<BenhNhan> benhNhanList = benhNhanRepository.findAll();
        assertThat(benhNhanList).hasSize(databaseSizeBeforeUpdate);
        BenhNhan testBenhNhan = benhNhanList.get(benhNhanList.size() - 1);
        assertThat(testBenhNhan.isChiCoNamSinh()).isEqualTo(UPDATED_CHI_CO_NAM_SINH);
        assertThat(testBenhNhan.getCmnd()).isEqualTo(UPDATED_CMND);
        assertThat(testBenhNhan.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testBenhNhan.isEnabled()).isEqualTo(UPDATED_ENABLED);
        assertThat(testBenhNhan.getGioiTinh()).isEqualTo(UPDATED_GIOI_TINH);
        assertThat(testBenhNhan.getKhangThe()).isEqualTo(UPDATED_KHANG_THE);
        assertThat(testBenhNhan.getMaSoThue()).isEqualTo(UPDATED_MA_SO_THUE);
        assertThat(testBenhNhan.getNgayCapCmnd()).isEqualTo(UPDATED_NGAY_CAP_CMND);
        assertThat(testBenhNhan.getNgaySinh()).isEqualTo(UPDATED_NGAY_SINH);
        assertThat(testBenhNhan.getNhomMau()).isEqualTo(UPDATED_NHOM_MAU);
        assertThat(testBenhNhan.getNoiCapCmnd()).isEqualTo(UPDATED_NOI_CAP_CMND);
        assertThat(testBenhNhan.getNoiLamViec()).isEqualTo(UPDATED_NOI_LAM_VIEC);
        assertThat(testBenhNhan.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testBenhNhan.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testBenhNhan.getSoNhaXom()).isEqualTo(UPDATED_SO_NHA_XOM);
        assertThat(testBenhNhan.getApThon()).isEqualTo(UPDATED_AP_THON);
        assertThat(testBenhNhan.getDiaPhuongId()).isEqualTo(UPDATED_DIA_PHUONG_ID);
        assertThat(testBenhNhan.getDiaChiThuongTru()).isEqualTo(UPDATED_DIA_CHI_THUONG_TRU);
    }

    @Test
    @Transactional
    public void updateNonExistingBenhNhan() throws Exception {
        int databaseSizeBeforeUpdate = benhNhanRepository.findAll().size();

        // Create the BenhNhan
        BenhNhanDTO benhNhanDTO = benhNhanMapper.toDto(benhNhan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBenhNhanMockMvc.perform(put("/api/benh-nhans").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhNhanDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BenhNhan in the database
        List<BenhNhan> benhNhanList = benhNhanRepository.findAll();
        assertThat(benhNhanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBenhNhan() throws Exception {
        // Initialize the database
        benhNhanRepository.saveAndFlush(benhNhan);

        int databaseSizeBeforeDelete = benhNhanRepository.findAll().size();

        // Delete the benhNhan
        restBenhNhanMockMvc.perform(delete("/api/benh-nhans/{id}", benhNhan.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BenhNhan> benhNhanList = benhNhanRepository.findAll();
        assertThat(benhNhanList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
