package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.ChiDinhCDHA;
import vn.vnpt.domain.PhieuChiDinhCDHA;
import vn.vnpt.domain.Phong;
import vn.vnpt.domain.ChanDoanHinhAnh;
import vn.vnpt.repository.ChiDinhCDHARepository;
import vn.vnpt.service.ChiDinhCDHAService;
import vn.vnpt.service.dto.ChiDinhCDHADTO;
import vn.vnpt.service.mapper.ChiDinhCDHAMapper;
import vn.vnpt.service.dto.ChiDinhCDHACriteria;
import vn.vnpt.service.ChiDinhCDHAQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ChiDinhCDHAResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class ChiDinhCDHAResourceIT {

    private static final Boolean DEFAULT_CO_BAO_HIEM = false;
    private static final Boolean UPDATED_CO_BAO_HIEM = true;

    private static final Boolean DEFAULT_CO_KET_QUA = false;
    private static final Boolean UPDATED_CO_KET_QUA = true;

    private static final Integer DEFAULT_DA_THANH_TOAN = 1;
    private static final Integer UPDATED_DA_THANH_TOAN = 2;
    private static final Integer SMALLER_DA_THANH_TOAN = 1 - 1;

    private static final Integer DEFAULT_DA_THANH_TOAN_CHENH_LECH = 1;
    private static final Integer UPDATED_DA_THANH_TOAN_CHENH_LECH = 2;
    private static final Integer SMALLER_DA_THANH_TOAN_CHENH_LECH = 1 - 1;

    private static final Integer DEFAULT_DA_THUC_HIEN = 1;
    private static final Integer UPDATED_DA_THUC_HIEN = 2;
    private static final Integer SMALLER_DA_THUC_HIEN = 1 - 1;

    private static final BigDecimal DEFAULT_DON_GIA = new BigDecimal(1);
    private static final BigDecimal UPDATED_DON_GIA = new BigDecimal(2);
    private static final BigDecimal SMALLER_DON_GIA = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_DON_GIA_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_DON_GIA_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_DON_GIA_BHYT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_DON_GIA_KHONG_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_DON_GIA_KHONG_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_DON_GIA_KHONG_BHYT = new BigDecimal(1 - 1);

    private static final String DEFAULT_GHI_CHU_CHI_DINH = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU_CHI_DINH = "BBBBBBBBBB";

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final String DEFAULT_MO_TA_XM_15 = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA_XM_15 = "BBBBBBBBBB";

    private static final Long DEFAULT_NGUOI_CHI_DINH_ID = 1L;
    private static final Long UPDATED_NGUOI_CHI_DINH_ID = 2L;
    private static final Long SMALLER_NGUOI_CHI_DINH_ID = 1L - 1L;

    private static final String DEFAULT_NGUOI_CHI_DINH = "AAAAAAAAAA";
    private static final String UPDATED_NGUOI_CHI_DINH = "BBBBBBBBBB";

    private static final Integer DEFAULT_SO_LAN_CHUP = 1;
    private static final Integer UPDATED_SO_LAN_CHUP = 2;
    private static final Integer SMALLER_SO_LAN_CHUP = 1 - 1;

    private static final BigDecimal DEFAULT_SO_LUONG = new BigDecimal(1);
    private static final BigDecimal UPDATED_SO_LUONG = new BigDecimal(2);
    private static final BigDecimal SMALLER_SO_LUONG = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_SO_LUONG_CO_FILM = new BigDecimal(1);
    private static final BigDecimal UPDATED_SO_LUONG_CO_FILM = new BigDecimal(2);
    private static final BigDecimal SMALLER_SO_LUONG_CO_FILM = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_SO_LUONG_CO_FILM_1318 = new BigDecimal(1);
    private static final BigDecimal UPDATED_SO_LUONG_CO_FILM_1318 = new BigDecimal(2);
    private static final BigDecimal SMALLER_SO_LUONG_CO_FILM_1318 = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_SO_LUONG_CO_FILM_1820 = new BigDecimal(1);
    private static final BigDecimal UPDATED_SO_LUONG_CO_FILM_1820 = new BigDecimal(2);
    private static final BigDecimal SMALLER_SO_LUONG_CO_FILM_1820 = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_SO_LUONG_CO_FILM_2025 = new BigDecimal(1);
    private static final BigDecimal UPDATED_SO_LUONG_CO_FILM_2025 = new BigDecimal(2);
    private static final BigDecimal SMALLER_SO_LUONG_CO_FILM_2025 = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_SO_LUONG_CO_FILM_2430 = new BigDecimal(1);
    private static final BigDecimal UPDATED_SO_LUONG_CO_FILM_2430 = new BigDecimal(2);
    private static final BigDecimal SMALLER_SO_LUONG_CO_FILM_2430 = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_SO_LUONG_CO_FILM_2530 = new BigDecimal(1);
    private static final BigDecimal UPDATED_SO_LUONG_CO_FILM_2530 = new BigDecimal(2);
    private static final BigDecimal SMALLER_SO_LUONG_CO_FILM_2530 = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_SO_LUONG_CO_FILM_3040 = new BigDecimal(1);
    private static final BigDecimal UPDATED_SO_LUONG_CO_FILM_3040 = new BigDecimal(2);
    private static final BigDecimal SMALLER_SO_LUONG_CO_FILM_3040 = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_SO_LUONG_FILM = new BigDecimal(1);
    private static final BigDecimal UPDATED_SO_LUONG_FILM = new BigDecimal(2);
    private static final BigDecimal SMALLER_SO_LUONG_FILM = new BigDecimal(1 - 1);

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_THANH_TIEN = new BigDecimal(1);
    private static final BigDecimal UPDATED_THANH_TIEN = new BigDecimal(2);
    private static final BigDecimal SMALLER_THANH_TIEN = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_THANH_TIEN_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_THANH_TIEN_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_THANH_TIEN_BHYT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_THANH_TIEN_KHONG_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_THANH_TIEN_KHONG_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_THANH_TIEN_KHONG_BHYT = new BigDecimal(1 - 1);

    private static final LocalDate DEFAULT_THOI_GIAN_CHI_DINH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_THOI_GIAN_CHI_DINH = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_THOI_GIAN_CHI_DINH = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_THOI_GIAN_TAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_THOI_GIAN_TAO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_THOI_GIAN_TAO = LocalDate.ofEpochDay(-1L);

    private static final BigDecimal DEFAULT_TIEN_NGOAI_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TIEN_NGOAI_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_TIEN_NGOAI_BHYT = new BigDecimal(1 - 1);

    private static final Boolean DEFAULT_THANH_TOAN_CHENH_LECH = false;
    private static final Boolean UPDATED_THANH_TOAN_CHENH_LECH = true;

    private static final Integer DEFAULT_TY_LE_THANH_TOAN = 1;
    private static final Integer UPDATED_TY_LE_THANH_TOAN = 2;
    private static final Integer SMALLER_TY_LE_THANH_TOAN = 1 - 1;

    private static final String DEFAULT_MA_DUNG_CHUNG = "AAAAAAAAAA";
    private static final String UPDATED_MA_DUNG_CHUNG = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DICH_VU_YEU_CAU = false;
    private static final Boolean UPDATED_DICH_VU_YEU_CAU = true;

    private static final Integer DEFAULT_NAM = 1;
    private static final Integer UPDATED_NAM = 2;
    private static final Integer SMALLER_NAM = 1 - 1;

    @Autowired
    private ChiDinhCDHARepository chiDinhCDHARepository;

    @Autowired
    private ChiDinhCDHAMapper chiDinhCDHAMapper;

    @Autowired
    private ChiDinhCDHAService chiDinhCDHAService;

    @Autowired
    private ChiDinhCDHAQueryService chiDinhCDHAQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChiDinhCDHAMockMvc;

    private ChiDinhCDHA chiDinhCDHA;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiDinhCDHA createEntity(EntityManager em) {
        ChiDinhCDHA chiDinhCDHA = new ChiDinhCDHA()
            .coBaoHiem(DEFAULT_CO_BAO_HIEM)
            .coKetQua(DEFAULT_CO_KET_QUA)
            .daThanhToan(DEFAULT_DA_THANH_TOAN)
            .daThanhToanChenhLech(DEFAULT_DA_THANH_TOAN_CHENH_LECH)
            .daThucHien(DEFAULT_DA_THUC_HIEN)
            .donGia(DEFAULT_DON_GIA)
            .donGiaBhyt(DEFAULT_DON_GIA_BHYT)
            .donGiaKhongBhyt(DEFAULT_DON_GIA_KHONG_BHYT)
            .ghiChuChiDinh(DEFAULT_GHI_CHU_CHI_DINH)
            .moTa(DEFAULT_MO_TA)
            .moTaXm15(DEFAULT_MO_TA_XM_15)
            .nguoiChiDinhId(DEFAULT_NGUOI_CHI_DINH_ID)
            .nguoiChiDinh(DEFAULT_NGUOI_CHI_DINH)
            .soLanChup(DEFAULT_SO_LAN_CHUP)
            .soLuong(DEFAULT_SO_LUONG)
            .soLuongCoFilm(DEFAULT_SO_LUONG_CO_FILM)
            .soLuongCoFilm1318(DEFAULT_SO_LUONG_CO_FILM_1318)
            .soLuongCoFilm1820(DEFAULT_SO_LUONG_CO_FILM_1820)
            .soLuongCoFilm2025(DEFAULT_SO_LUONG_CO_FILM_2025)
            .soLuongCoFilm2430(DEFAULT_SO_LUONG_CO_FILM_2430)
            .soLuongCoFilm2530(DEFAULT_SO_LUONG_CO_FILM_2530)
            .soLuongCoFilm3040(DEFAULT_SO_LUONG_CO_FILM_3040)
            .soLuongFilm(DEFAULT_SO_LUONG_FILM)
            .ten(DEFAULT_TEN)
            .thanhTien(DEFAULT_THANH_TIEN)
            .thanhTienBhyt(DEFAULT_THANH_TIEN_BHYT)
            .thanhTienKhongBHYT(DEFAULT_THANH_TIEN_KHONG_BHYT)
            .thoiGianChiDinh(DEFAULT_THOI_GIAN_CHI_DINH)
            .thoiGianTao(DEFAULT_THOI_GIAN_TAO)
            .tienNgoaiBHYT(DEFAULT_TIEN_NGOAI_BHYT)
            .thanhToanChenhLech(DEFAULT_THANH_TOAN_CHENH_LECH)
            .tyLeThanhToan(DEFAULT_TY_LE_THANH_TOAN)
            .maDungChung(DEFAULT_MA_DUNG_CHUNG)
            .dichVuYeuCau(DEFAULT_DICH_VU_YEU_CAU)
            .nam(DEFAULT_NAM);
        // Add required entity
        PhieuChiDinhCDHA phieuChiDinhCDHA;
        if (TestUtil.findAll(em, PhieuChiDinhCDHA.class).isEmpty()) {
            phieuChiDinhCDHA = PhieuChiDinhCDHAResourceIT.createEntity(em);
            em.persist(phieuChiDinhCDHA);
            em.flush();
        } else {
            phieuChiDinhCDHA = TestUtil.findAll(em, PhieuChiDinhCDHA.class).get(0);
        }
        chiDinhCDHA.setDonVi(phieuChiDinhCDHA);
        // Add required entity
        chiDinhCDHA.setBenhNhan(phieuChiDinhCDHA);
        // Add required entity
        chiDinhCDHA.setBakb(phieuChiDinhCDHA);
        // Add required entity
        chiDinhCDHA.setPhieuCD(phieuChiDinhCDHA);
        // Add required entity
        Phong phong;
        if (TestUtil.findAll(em, Phong.class).isEmpty()) {
            phong = PhongResourceIT.createEntity(em);
            em.persist(phong);
            em.flush();
        } else {
            phong = TestUtil.findAll(em, Phong.class).get(0);
        }
        chiDinhCDHA.setPhong(phong);
        // Add required entity
        ChanDoanHinhAnh chanDoanHinhAnh;
        if (TestUtil.findAll(em, ChanDoanHinhAnh.class).isEmpty()) {
            chanDoanHinhAnh = ChanDoanHinhAnhResourceIT.createEntity(em);
            em.persist(chanDoanHinhAnh);
            em.flush();
        } else {
            chanDoanHinhAnh = TestUtil.findAll(em, ChanDoanHinhAnh.class).get(0);
        }
        chiDinhCDHA.setCdha(chanDoanHinhAnh);
        return chiDinhCDHA;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiDinhCDHA createUpdatedEntity(EntityManager em) {
        ChiDinhCDHA chiDinhCDHA = new ChiDinhCDHA()
            .coBaoHiem(UPDATED_CO_BAO_HIEM)
            .coKetQua(UPDATED_CO_KET_QUA)
            .daThanhToan(UPDATED_DA_THANH_TOAN)
            .daThanhToanChenhLech(UPDATED_DA_THANH_TOAN_CHENH_LECH)
            .daThucHien(UPDATED_DA_THUC_HIEN)
            .donGia(UPDATED_DON_GIA)
            .donGiaBhyt(UPDATED_DON_GIA_BHYT)
            .donGiaKhongBhyt(UPDATED_DON_GIA_KHONG_BHYT)
            .ghiChuChiDinh(UPDATED_GHI_CHU_CHI_DINH)
            .moTa(UPDATED_MO_TA)
            .moTaXm15(UPDATED_MO_TA_XM_15)
            .nguoiChiDinhId(UPDATED_NGUOI_CHI_DINH_ID)
            .nguoiChiDinh(UPDATED_NGUOI_CHI_DINH)
            .soLanChup(UPDATED_SO_LAN_CHUP)
            .soLuong(UPDATED_SO_LUONG)
            .soLuongCoFilm(UPDATED_SO_LUONG_CO_FILM)
            .soLuongCoFilm1318(UPDATED_SO_LUONG_CO_FILM_1318)
            .soLuongCoFilm1820(UPDATED_SO_LUONG_CO_FILM_1820)
            .soLuongCoFilm2025(UPDATED_SO_LUONG_CO_FILM_2025)
            .soLuongCoFilm2430(UPDATED_SO_LUONG_CO_FILM_2430)
            .soLuongCoFilm2530(UPDATED_SO_LUONG_CO_FILM_2530)
            .soLuongCoFilm3040(UPDATED_SO_LUONG_CO_FILM_3040)
            .soLuongFilm(UPDATED_SO_LUONG_FILM)
            .ten(UPDATED_TEN)
            .thanhTien(UPDATED_THANH_TIEN)
            .thanhTienBhyt(UPDATED_THANH_TIEN_BHYT)
            .thanhTienKhongBHYT(UPDATED_THANH_TIEN_KHONG_BHYT)
            .thoiGianChiDinh(UPDATED_THOI_GIAN_CHI_DINH)
            .thoiGianTao(UPDATED_THOI_GIAN_TAO)
            .tienNgoaiBHYT(UPDATED_TIEN_NGOAI_BHYT)
            .thanhToanChenhLech(UPDATED_THANH_TOAN_CHENH_LECH)
            .tyLeThanhToan(UPDATED_TY_LE_THANH_TOAN)
            .maDungChung(UPDATED_MA_DUNG_CHUNG)
            .dichVuYeuCau(UPDATED_DICH_VU_YEU_CAU)
            .nam(UPDATED_NAM);
        // Add required entity
        PhieuChiDinhCDHA phieuChiDinhCDHA;
        if (TestUtil.findAll(em, PhieuChiDinhCDHA.class).isEmpty()) {
            phieuChiDinhCDHA = PhieuChiDinhCDHAResourceIT.createUpdatedEntity(em);
            em.persist(phieuChiDinhCDHA);
            em.flush();
        } else {
            phieuChiDinhCDHA = TestUtil.findAll(em, PhieuChiDinhCDHA.class).get(0);
        }
        chiDinhCDHA.setDonVi(phieuChiDinhCDHA);
        // Add required entity
        chiDinhCDHA.setBenhNhan(phieuChiDinhCDHA);
        // Add required entity
        chiDinhCDHA.setBakb(phieuChiDinhCDHA);
        // Add required entity
        chiDinhCDHA.setPhieuCD(phieuChiDinhCDHA);
        // Add required entity
        Phong phong;
        if (TestUtil.findAll(em, Phong.class).isEmpty()) {
            phong = PhongResourceIT.createUpdatedEntity(em);
            em.persist(phong);
            em.flush();
        } else {
            phong = TestUtil.findAll(em, Phong.class).get(0);
        }
        chiDinhCDHA.setPhong(phong);
        // Add required entity
        ChanDoanHinhAnh chanDoanHinhAnh;
        if (TestUtil.findAll(em, ChanDoanHinhAnh.class).isEmpty()) {
            chanDoanHinhAnh = ChanDoanHinhAnhResourceIT.createUpdatedEntity(em);
            em.persist(chanDoanHinhAnh);
            em.flush();
        } else {
            chanDoanHinhAnh = TestUtil.findAll(em, ChanDoanHinhAnh.class).get(0);
        }
        chiDinhCDHA.setCdha(chanDoanHinhAnh);
        return chiDinhCDHA;
    }

    @BeforeEach
    public void initTest() {
        chiDinhCDHA = createEntity(em);
    }

    @Test
    @Transactional
    public void createChiDinhCDHA() throws Exception {
        int databaseSizeBeforeCreate = chiDinhCDHARepository.findAll().size();

        // Create the ChiDinhCDHA
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);
        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isCreated());

        // Validate the ChiDinhCDHA in the database
        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeCreate + 1);
        ChiDinhCDHA testChiDinhCDHA = chiDinhCDHAList.get(chiDinhCDHAList.size() - 1);
        assertThat(testChiDinhCDHA.isCoBaoHiem()).isEqualTo(DEFAULT_CO_BAO_HIEM);
        assertThat(testChiDinhCDHA.isCoKetQua()).isEqualTo(DEFAULT_CO_KET_QUA);
        assertThat(testChiDinhCDHA.getDaThanhToan()).isEqualTo(DEFAULT_DA_THANH_TOAN);
        assertThat(testChiDinhCDHA.getDaThanhToanChenhLech()).isEqualTo(DEFAULT_DA_THANH_TOAN_CHENH_LECH);
        assertThat(testChiDinhCDHA.getDaThucHien()).isEqualTo(DEFAULT_DA_THUC_HIEN);
        assertThat(testChiDinhCDHA.getDonGia()).isEqualTo(DEFAULT_DON_GIA);
        assertThat(testChiDinhCDHA.getDonGiaBhyt()).isEqualTo(DEFAULT_DON_GIA_BHYT);
        assertThat(testChiDinhCDHA.getDonGiaKhongBhyt()).isEqualTo(DEFAULT_DON_GIA_KHONG_BHYT);
        assertThat(testChiDinhCDHA.getGhiChuChiDinh()).isEqualTo(DEFAULT_GHI_CHU_CHI_DINH);
        assertThat(testChiDinhCDHA.getMoTa()).isEqualTo(DEFAULT_MO_TA);
        assertThat(testChiDinhCDHA.getMoTaXm15()).isEqualTo(DEFAULT_MO_TA_XM_15);
        assertThat(testChiDinhCDHA.getNguoiChiDinhId()).isEqualTo(DEFAULT_NGUOI_CHI_DINH_ID);
        assertThat(testChiDinhCDHA.getNguoiChiDinh()).isEqualTo(DEFAULT_NGUOI_CHI_DINH);
        assertThat(testChiDinhCDHA.getSoLanChup()).isEqualTo(DEFAULT_SO_LAN_CHUP);
        assertThat(testChiDinhCDHA.getSoLuong()).isEqualTo(DEFAULT_SO_LUONG);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm()).isEqualTo(DEFAULT_SO_LUONG_CO_FILM);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm1318()).isEqualTo(DEFAULT_SO_LUONG_CO_FILM_1318);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm1820()).isEqualTo(DEFAULT_SO_LUONG_CO_FILM_1820);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm2025()).isEqualTo(DEFAULT_SO_LUONG_CO_FILM_2025);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm2430()).isEqualTo(DEFAULT_SO_LUONG_CO_FILM_2430);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm2530()).isEqualTo(DEFAULT_SO_LUONG_CO_FILM_2530);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm3040()).isEqualTo(DEFAULT_SO_LUONG_CO_FILM_3040);
        assertThat(testChiDinhCDHA.getSoLuongFilm()).isEqualTo(DEFAULT_SO_LUONG_FILM);
        assertThat(testChiDinhCDHA.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testChiDinhCDHA.getThanhTien()).isEqualTo(DEFAULT_THANH_TIEN);
        assertThat(testChiDinhCDHA.getThanhTienBhyt()).isEqualTo(DEFAULT_THANH_TIEN_BHYT);
        assertThat(testChiDinhCDHA.getThanhTienKhongBHYT()).isEqualTo(DEFAULT_THANH_TIEN_KHONG_BHYT);
        assertThat(testChiDinhCDHA.getThoiGianChiDinh()).isEqualTo(DEFAULT_THOI_GIAN_CHI_DINH);
        assertThat(testChiDinhCDHA.getThoiGianTao()).isEqualTo(DEFAULT_THOI_GIAN_TAO);
        assertThat(testChiDinhCDHA.getTienNgoaiBHYT()).isEqualTo(DEFAULT_TIEN_NGOAI_BHYT);
        assertThat(testChiDinhCDHA.isThanhToanChenhLech()).isEqualTo(DEFAULT_THANH_TOAN_CHENH_LECH);
        assertThat(testChiDinhCDHA.getTyLeThanhToan()).isEqualTo(DEFAULT_TY_LE_THANH_TOAN);
        assertThat(testChiDinhCDHA.getMaDungChung()).isEqualTo(DEFAULT_MA_DUNG_CHUNG);
        assertThat(testChiDinhCDHA.isDichVuYeuCau()).isEqualTo(DEFAULT_DICH_VU_YEU_CAU);
        assertThat(testChiDinhCDHA.getNam()).isEqualTo(DEFAULT_NAM);
    }

    @Test
    @Transactional
    public void createChiDinhCDHAWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chiDinhCDHARepository.findAll().size();

        // Create the ChiDinhCDHA with an existing ID
        chiDinhCDHA.setId(1L);
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChiDinhCDHA in the database
        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCoBaoHiemIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhCDHARepository.findAll().size();
        // set the field null
        chiDinhCDHA.setCoBaoHiem(null);

        // Create the ChiDinhCDHA, which fails.
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCoKetQuaIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhCDHARepository.findAll().size();
        // set the field null
        chiDinhCDHA.setCoKetQua(null);

        // Create the ChiDinhCDHA, which fails.
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDaThanhToanIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhCDHARepository.findAll().size();
        // set the field null
        chiDinhCDHA.setDaThanhToan(null);

        // Create the ChiDinhCDHA, which fails.
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDaThanhToanChenhLechIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhCDHARepository.findAll().size();
        // set the field null
        chiDinhCDHA.setDaThanhToanChenhLech(null);

        // Create the ChiDinhCDHA, which fails.
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDaThucHienIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhCDHARepository.findAll().size();
        // set the field null
        chiDinhCDHA.setDaThucHien(null);

        // Create the ChiDinhCDHA, which fails.
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDonGiaIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhCDHARepository.findAll().size();
        // set the field null
        chiDinhCDHA.setDonGia(null);

        // Create the ChiDinhCDHA, which fails.
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDonGiaBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhCDHARepository.findAll().size();
        // set the field null
        chiDinhCDHA.setDonGiaBhyt(null);

        // Create the ChiDinhCDHA, which fails.
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkThanhTienIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhCDHARepository.findAll().size();
        // set the field null
        chiDinhCDHA.setThanhTien(null);

        // Create the ChiDinhCDHA, which fails.
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkThoiGianTaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhCDHARepository.findAll().size();
        // set the field null
        chiDinhCDHA.setThoiGianTao(null);

        // Create the ChiDinhCDHA, which fails.
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTienNgoaiBHYTIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhCDHARepository.findAll().size();
        // set the field null
        chiDinhCDHA.setTienNgoaiBHYT(null);

        // Create the ChiDinhCDHA, which fails.
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkThanhToanChenhLechIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhCDHARepository.findAll().size();
        // set the field null
        chiDinhCDHA.setThanhToanChenhLech(null);

        // Create the ChiDinhCDHA, which fails.
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhCDHARepository.findAll().size();
        // set the field null
        chiDinhCDHA.setNam(null);

        // Create the ChiDinhCDHA, which fails.
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        restChiDinhCDHAMockMvc.perform(post("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHAS() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList
        restChiDinhCDHAMockMvc.perform(get("/api/chi-dinh-cdhas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiDinhCDHA.getId().intValue())))
            .andExpect(jsonPath("$.[*].coBaoHiem").value(hasItem(DEFAULT_CO_BAO_HIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].coKetQua").value(hasItem(DEFAULT_CO_KET_QUA.booleanValue())))
            .andExpect(jsonPath("$.[*].daThanhToan").value(hasItem(DEFAULT_DA_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].daThanhToanChenhLech").value(hasItem(DEFAULT_DA_THANH_TOAN_CHENH_LECH)))
            .andExpect(jsonPath("$.[*].daThucHien").value(hasItem(DEFAULT_DA_THUC_HIEN)))
            .andExpect(jsonPath("$.[*].donGia").value(hasItem(DEFAULT_DON_GIA.intValue())))
            .andExpect(jsonPath("$.[*].donGiaBhyt").value(hasItem(DEFAULT_DON_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].donGiaKhongBhyt").value(hasItem(DEFAULT_DON_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].ghiChuChiDinh").value(hasItem(DEFAULT_GHI_CHU_CHI_DINH)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].moTaXm15").value(hasItem(DEFAULT_MO_TA_XM_15)))
            .andExpect(jsonPath("$.[*].nguoiChiDinhId").value(hasItem(DEFAULT_NGUOI_CHI_DINH_ID.intValue())))
            .andExpect(jsonPath("$.[*].nguoiChiDinh").value(hasItem(DEFAULT_NGUOI_CHI_DINH)))
            .andExpect(jsonPath("$.[*].soLanChup").value(hasItem(DEFAULT_SO_LAN_CHUP)))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm").value(hasItem(DEFAULT_SO_LUONG_CO_FILM.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm1318").value(hasItem(DEFAULT_SO_LUONG_CO_FILM_1318.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm1820").value(hasItem(DEFAULT_SO_LUONG_CO_FILM_1820.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm2025").value(hasItem(DEFAULT_SO_LUONG_CO_FILM_2025.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm2430").value(hasItem(DEFAULT_SO_LUONG_CO_FILM_2430.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm2530").value(hasItem(DEFAULT_SO_LUONG_CO_FILM_2530.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm3040").value(hasItem(DEFAULT_SO_LUONG_CO_FILM_3040.intValue())))
            .andExpect(jsonPath("$.[*].soLuongFilm").value(hasItem(DEFAULT_SO_LUONG_FILM.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].thanhTien").value(hasItem(DEFAULT_THANH_TIEN.intValue())))
            .andExpect(jsonPath("$.[*].thanhTienBhyt").value(hasItem(DEFAULT_THANH_TIEN_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thanhTienKhongBHYT").value(hasItem(DEFAULT_THANH_TIEN_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thoiGianChiDinh").value(hasItem(DEFAULT_THOI_GIAN_CHI_DINH.toString())))
            .andExpect(jsonPath("$.[*].thoiGianTao").value(hasItem(DEFAULT_THOI_GIAN_TAO.toString())))
            .andExpect(jsonPath("$.[*].tienNgoaiBHYT").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thanhToanChenhLech").value(hasItem(DEFAULT_THANH_TOAN_CHENH_LECH.booleanValue())))
            .andExpect(jsonPath("$.[*].tyLeThanhToan").value(hasItem(DEFAULT_TY_LE_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)))
            .andExpect(jsonPath("$.[*].dichVuYeuCau").value(hasItem(DEFAULT_DICH_VU_YEU_CAU.booleanValue())))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));
    }
    
    @Test
    @Transactional
    public void getChiDinhCDHA() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get the chiDinhCDHA
        restChiDinhCDHAMockMvc.perform(get("/api/chi-dinh-cdhas/{id}", chiDinhCDHA.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chiDinhCDHA.getId().intValue()))
            .andExpect(jsonPath("$.coBaoHiem").value(DEFAULT_CO_BAO_HIEM.booleanValue()))
            .andExpect(jsonPath("$.coKetQua").value(DEFAULT_CO_KET_QUA.booleanValue()))
            .andExpect(jsonPath("$.daThanhToan").value(DEFAULT_DA_THANH_TOAN))
            .andExpect(jsonPath("$.daThanhToanChenhLech").value(DEFAULT_DA_THANH_TOAN_CHENH_LECH))
            .andExpect(jsonPath("$.daThucHien").value(DEFAULT_DA_THUC_HIEN))
            .andExpect(jsonPath("$.donGia").value(DEFAULT_DON_GIA.intValue()))
            .andExpect(jsonPath("$.donGiaBhyt").value(DEFAULT_DON_GIA_BHYT.intValue()))
            .andExpect(jsonPath("$.donGiaKhongBhyt").value(DEFAULT_DON_GIA_KHONG_BHYT.intValue()))
            .andExpect(jsonPath("$.ghiChuChiDinh").value(DEFAULT_GHI_CHU_CHI_DINH))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA))
            .andExpect(jsonPath("$.moTaXm15").value(DEFAULT_MO_TA_XM_15))
            .andExpect(jsonPath("$.nguoiChiDinhId").value(DEFAULT_NGUOI_CHI_DINH_ID.intValue()))
            .andExpect(jsonPath("$.nguoiChiDinh").value(DEFAULT_NGUOI_CHI_DINH))
            .andExpect(jsonPath("$.soLanChup").value(DEFAULT_SO_LAN_CHUP))
            .andExpect(jsonPath("$.soLuong").value(DEFAULT_SO_LUONG.intValue()))
            .andExpect(jsonPath("$.soLuongCoFilm").value(DEFAULT_SO_LUONG_CO_FILM.intValue()))
            .andExpect(jsonPath("$.soLuongCoFilm1318").value(DEFAULT_SO_LUONG_CO_FILM_1318.intValue()))
            .andExpect(jsonPath("$.soLuongCoFilm1820").value(DEFAULT_SO_LUONG_CO_FILM_1820.intValue()))
            .andExpect(jsonPath("$.soLuongCoFilm2025").value(DEFAULT_SO_LUONG_CO_FILM_2025.intValue()))
            .andExpect(jsonPath("$.soLuongCoFilm2430").value(DEFAULT_SO_LUONG_CO_FILM_2430.intValue()))
            .andExpect(jsonPath("$.soLuongCoFilm2530").value(DEFAULT_SO_LUONG_CO_FILM_2530.intValue()))
            .andExpect(jsonPath("$.soLuongCoFilm3040").value(DEFAULT_SO_LUONG_CO_FILM_3040.intValue()))
            .andExpect(jsonPath("$.soLuongFilm").value(DEFAULT_SO_LUONG_FILM.intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.thanhTien").value(DEFAULT_THANH_TIEN.intValue()))
            .andExpect(jsonPath("$.thanhTienBhyt").value(DEFAULT_THANH_TIEN_BHYT.intValue()))
            .andExpect(jsonPath("$.thanhTienKhongBHYT").value(DEFAULT_THANH_TIEN_KHONG_BHYT.intValue()))
            .andExpect(jsonPath("$.thoiGianChiDinh").value(DEFAULT_THOI_GIAN_CHI_DINH.toString()))
            .andExpect(jsonPath("$.thoiGianTao").value(DEFAULT_THOI_GIAN_TAO.toString()))
            .andExpect(jsonPath("$.tienNgoaiBHYT").value(DEFAULT_TIEN_NGOAI_BHYT.intValue()))
            .andExpect(jsonPath("$.thanhToanChenhLech").value(DEFAULT_THANH_TOAN_CHENH_LECH.booleanValue()))
            .andExpect(jsonPath("$.tyLeThanhToan").value(DEFAULT_TY_LE_THANH_TOAN))
            .andExpect(jsonPath("$.maDungChung").value(DEFAULT_MA_DUNG_CHUNG))
            .andExpect(jsonPath("$.dichVuYeuCau").value(DEFAULT_DICH_VU_YEU_CAU.booleanValue()))
            .andExpect(jsonPath("$.nam").value(DEFAULT_NAM));
    }


    @Test
    @Transactional
    public void getChiDinhCDHASByIdFiltering() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        Long id = chiDinhCDHA.getId();

        defaultChiDinhCDHAShouldBeFound("id.equals=" + id);
        defaultChiDinhCDHAShouldNotBeFound("id.notEquals=" + id);

        defaultChiDinhCDHAShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultChiDinhCDHAShouldNotBeFound("id.greaterThan=" + id);

        defaultChiDinhCDHAShouldBeFound("id.lessThanOrEqual=" + id);
        defaultChiDinhCDHAShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByCoBaoHiemIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where coBaoHiem equals to DEFAULT_CO_BAO_HIEM
        defaultChiDinhCDHAShouldBeFound("coBaoHiem.equals=" + DEFAULT_CO_BAO_HIEM);

        // Get all the chiDinhCDHAList where coBaoHiem equals to UPDATED_CO_BAO_HIEM
        defaultChiDinhCDHAShouldNotBeFound("coBaoHiem.equals=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByCoBaoHiemIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where coBaoHiem not equals to DEFAULT_CO_BAO_HIEM
        defaultChiDinhCDHAShouldNotBeFound("coBaoHiem.notEquals=" + DEFAULT_CO_BAO_HIEM);

        // Get all the chiDinhCDHAList where coBaoHiem not equals to UPDATED_CO_BAO_HIEM
        defaultChiDinhCDHAShouldBeFound("coBaoHiem.notEquals=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByCoBaoHiemIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where coBaoHiem in DEFAULT_CO_BAO_HIEM or UPDATED_CO_BAO_HIEM
        defaultChiDinhCDHAShouldBeFound("coBaoHiem.in=" + DEFAULT_CO_BAO_HIEM + "," + UPDATED_CO_BAO_HIEM);

        // Get all the chiDinhCDHAList where coBaoHiem equals to UPDATED_CO_BAO_HIEM
        defaultChiDinhCDHAShouldNotBeFound("coBaoHiem.in=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByCoBaoHiemIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where coBaoHiem is not null
        defaultChiDinhCDHAShouldBeFound("coBaoHiem.specified=true");

        // Get all the chiDinhCDHAList where coBaoHiem is null
        defaultChiDinhCDHAShouldNotBeFound("coBaoHiem.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByCoKetQuaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where coKetQua equals to DEFAULT_CO_KET_QUA
        defaultChiDinhCDHAShouldBeFound("coKetQua.equals=" + DEFAULT_CO_KET_QUA);

        // Get all the chiDinhCDHAList where coKetQua equals to UPDATED_CO_KET_QUA
        defaultChiDinhCDHAShouldNotBeFound("coKetQua.equals=" + UPDATED_CO_KET_QUA);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByCoKetQuaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where coKetQua not equals to DEFAULT_CO_KET_QUA
        defaultChiDinhCDHAShouldNotBeFound("coKetQua.notEquals=" + DEFAULT_CO_KET_QUA);

        // Get all the chiDinhCDHAList where coKetQua not equals to UPDATED_CO_KET_QUA
        defaultChiDinhCDHAShouldBeFound("coKetQua.notEquals=" + UPDATED_CO_KET_QUA);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByCoKetQuaIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where coKetQua in DEFAULT_CO_KET_QUA or UPDATED_CO_KET_QUA
        defaultChiDinhCDHAShouldBeFound("coKetQua.in=" + DEFAULT_CO_KET_QUA + "," + UPDATED_CO_KET_QUA);

        // Get all the chiDinhCDHAList where coKetQua equals to UPDATED_CO_KET_QUA
        defaultChiDinhCDHAShouldNotBeFound("coKetQua.in=" + UPDATED_CO_KET_QUA);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByCoKetQuaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where coKetQua is not null
        defaultChiDinhCDHAShouldBeFound("coKetQua.specified=true");

        // Get all the chiDinhCDHAList where coKetQua is null
        defaultChiDinhCDHAShouldNotBeFound("coKetQua.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToan equals to DEFAULT_DA_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("daThanhToan.equals=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhCDHAList where daThanhToan equals to UPDATED_DA_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("daThanhToan.equals=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToan not equals to DEFAULT_DA_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("daThanhToan.notEquals=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhCDHAList where daThanhToan not equals to UPDATED_DA_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("daThanhToan.notEquals=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToan in DEFAULT_DA_THANH_TOAN or UPDATED_DA_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("daThanhToan.in=" + DEFAULT_DA_THANH_TOAN + "," + UPDATED_DA_THANH_TOAN);

        // Get all the chiDinhCDHAList where daThanhToan equals to UPDATED_DA_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("daThanhToan.in=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToan is not null
        defaultChiDinhCDHAShouldBeFound("daThanhToan.specified=true");

        // Get all the chiDinhCDHAList where daThanhToan is null
        defaultChiDinhCDHAShouldNotBeFound("daThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToan is greater than or equal to DEFAULT_DA_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("daThanhToan.greaterThanOrEqual=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhCDHAList where daThanhToan is greater than or equal to UPDATED_DA_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("daThanhToan.greaterThanOrEqual=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToan is less than or equal to DEFAULT_DA_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("daThanhToan.lessThanOrEqual=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhCDHAList where daThanhToan is less than or equal to SMALLER_DA_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("daThanhToan.lessThanOrEqual=" + SMALLER_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToan is less than DEFAULT_DA_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("daThanhToan.lessThan=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhCDHAList where daThanhToan is less than UPDATED_DA_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("daThanhToan.lessThan=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToan is greater than DEFAULT_DA_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("daThanhToan.greaterThan=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhCDHAList where daThanhToan is greater than SMALLER_DA_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("daThanhToan.greaterThan=" + SMALLER_DA_THANH_TOAN);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanChenhLechIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech equals to DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldBeFound("daThanhToanChenhLech.equals=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech equals to UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldNotBeFound("daThanhToanChenhLech.equals=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanChenhLechIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech not equals to DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldNotBeFound("daThanhToanChenhLech.notEquals=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech not equals to UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldBeFound("daThanhToanChenhLech.notEquals=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanChenhLechIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech in DEFAULT_DA_THANH_TOAN_CHENH_LECH or UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldBeFound("daThanhToanChenhLech.in=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH + "," + UPDATED_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech equals to UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldNotBeFound("daThanhToanChenhLech.in=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanChenhLechIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech is not null
        defaultChiDinhCDHAShouldBeFound("daThanhToanChenhLech.specified=true");

        // Get all the chiDinhCDHAList where daThanhToanChenhLech is null
        defaultChiDinhCDHAShouldNotBeFound("daThanhToanChenhLech.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanChenhLechIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech is greater than or equal to DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldBeFound("daThanhToanChenhLech.greaterThanOrEqual=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech is greater than or equal to UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldNotBeFound("daThanhToanChenhLech.greaterThanOrEqual=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanChenhLechIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech is less than or equal to DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldBeFound("daThanhToanChenhLech.lessThanOrEqual=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech is less than or equal to SMALLER_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldNotBeFound("daThanhToanChenhLech.lessThanOrEqual=" + SMALLER_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanChenhLechIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech is less than DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldNotBeFound("daThanhToanChenhLech.lessThan=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech is less than UPDATED_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldBeFound("daThanhToanChenhLech.lessThan=" + UPDATED_DA_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThanhToanChenhLechIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech is greater than DEFAULT_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldNotBeFound("daThanhToanChenhLech.greaterThan=" + DEFAULT_DA_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhCDHAList where daThanhToanChenhLech is greater than SMALLER_DA_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldBeFound("daThanhToanChenhLech.greaterThan=" + SMALLER_DA_THANH_TOAN_CHENH_LECH);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThucHienIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThucHien equals to DEFAULT_DA_THUC_HIEN
        defaultChiDinhCDHAShouldBeFound("daThucHien.equals=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhCDHAList where daThucHien equals to UPDATED_DA_THUC_HIEN
        defaultChiDinhCDHAShouldNotBeFound("daThucHien.equals=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThucHienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThucHien not equals to DEFAULT_DA_THUC_HIEN
        defaultChiDinhCDHAShouldNotBeFound("daThucHien.notEquals=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhCDHAList where daThucHien not equals to UPDATED_DA_THUC_HIEN
        defaultChiDinhCDHAShouldBeFound("daThucHien.notEquals=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThucHienIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThucHien in DEFAULT_DA_THUC_HIEN or UPDATED_DA_THUC_HIEN
        defaultChiDinhCDHAShouldBeFound("daThucHien.in=" + DEFAULT_DA_THUC_HIEN + "," + UPDATED_DA_THUC_HIEN);

        // Get all the chiDinhCDHAList where daThucHien equals to UPDATED_DA_THUC_HIEN
        defaultChiDinhCDHAShouldNotBeFound("daThucHien.in=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThucHienIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThucHien is not null
        defaultChiDinhCDHAShouldBeFound("daThucHien.specified=true");

        // Get all the chiDinhCDHAList where daThucHien is null
        defaultChiDinhCDHAShouldNotBeFound("daThucHien.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThucHienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThucHien is greater than or equal to DEFAULT_DA_THUC_HIEN
        defaultChiDinhCDHAShouldBeFound("daThucHien.greaterThanOrEqual=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhCDHAList where daThucHien is greater than or equal to UPDATED_DA_THUC_HIEN
        defaultChiDinhCDHAShouldNotBeFound("daThucHien.greaterThanOrEqual=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThucHienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThucHien is less than or equal to DEFAULT_DA_THUC_HIEN
        defaultChiDinhCDHAShouldBeFound("daThucHien.lessThanOrEqual=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhCDHAList where daThucHien is less than or equal to SMALLER_DA_THUC_HIEN
        defaultChiDinhCDHAShouldNotBeFound("daThucHien.lessThanOrEqual=" + SMALLER_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThucHienIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThucHien is less than DEFAULT_DA_THUC_HIEN
        defaultChiDinhCDHAShouldNotBeFound("daThucHien.lessThan=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhCDHAList where daThucHien is less than UPDATED_DA_THUC_HIEN
        defaultChiDinhCDHAShouldBeFound("daThucHien.lessThan=" + UPDATED_DA_THUC_HIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDaThucHienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where daThucHien is greater than DEFAULT_DA_THUC_HIEN
        defaultChiDinhCDHAShouldNotBeFound("daThucHien.greaterThan=" + DEFAULT_DA_THUC_HIEN);

        // Get all the chiDinhCDHAList where daThucHien is greater than SMALLER_DA_THUC_HIEN
        defaultChiDinhCDHAShouldBeFound("daThucHien.greaterThan=" + SMALLER_DA_THUC_HIEN);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGia equals to DEFAULT_DON_GIA
        defaultChiDinhCDHAShouldBeFound("donGia.equals=" + DEFAULT_DON_GIA);

        // Get all the chiDinhCDHAList where donGia equals to UPDATED_DON_GIA
        defaultChiDinhCDHAShouldNotBeFound("donGia.equals=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGia not equals to DEFAULT_DON_GIA
        defaultChiDinhCDHAShouldNotBeFound("donGia.notEquals=" + DEFAULT_DON_GIA);

        // Get all the chiDinhCDHAList where donGia not equals to UPDATED_DON_GIA
        defaultChiDinhCDHAShouldBeFound("donGia.notEquals=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGia in DEFAULT_DON_GIA or UPDATED_DON_GIA
        defaultChiDinhCDHAShouldBeFound("donGia.in=" + DEFAULT_DON_GIA + "," + UPDATED_DON_GIA);

        // Get all the chiDinhCDHAList where donGia equals to UPDATED_DON_GIA
        defaultChiDinhCDHAShouldNotBeFound("donGia.in=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGia is not null
        defaultChiDinhCDHAShouldBeFound("donGia.specified=true");

        // Get all the chiDinhCDHAList where donGia is null
        defaultChiDinhCDHAShouldNotBeFound("donGia.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGia is greater than or equal to DEFAULT_DON_GIA
        defaultChiDinhCDHAShouldBeFound("donGia.greaterThanOrEqual=" + DEFAULT_DON_GIA);

        // Get all the chiDinhCDHAList where donGia is greater than or equal to UPDATED_DON_GIA
        defaultChiDinhCDHAShouldNotBeFound("donGia.greaterThanOrEqual=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGia is less than or equal to DEFAULT_DON_GIA
        defaultChiDinhCDHAShouldBeFound("donGia.lessThanOrEqual=" + DEFAULT_DON_GIA);

        // Get all the chiDinhCDHAList where donGia is less than or equal to SMALLER_DON_GIA
        defaultChiDinhCDHAShouldNotBeFound("donGia.lessThanOrEqual=" + SMALLER_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGia is less than DEFAULT_DON_GIA
        defaultChiDinhCDHAShouldNotBeFound("donGia.lessThan=" + DEFAULT_DON_GIA);

        // Get all the chiDinhCDHAList where donGia is less than UPDATED_DON_GIA
        defaultChiDinhCDHAShouldBeFound("donGia.lessThan=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGia is greater than DEFAULT_DON_GIA
        defaultChiDinhCDHAShouldNotBeFound("donGia.greaterThan=" + DEFAULT_DON_GIA);

        // Get all the chiDinhCDHAList where donGia is greater than SMALLER_DON_GIA
        defaultChiDinhCDHAShouldBeFound("donGia.greaterThan=" + SMALLER_DON_GIA);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaBhyt equals to DEFAULT_DON_GIA_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaBhyt.equals=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhCDHAList where donGiaBhyt equals to UPDATED_DON_GIA_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaBhyt.equals=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaBhyt not equals to DEFAULT_DON_GIA_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaBhyt.notEquals=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhCDHAList where donGiaBhyt not equals to UPDATED_DON_GIA_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaBhyt.notEquals=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaBhyt in DEFAULT_DON_GIA_BHYT or UPDATED_DON_GIA_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaBhyt.in=" + DEFAULT_DON_GIA_BHYT + "," + UPDATED_DON_GIA_BHYT);

        // Get all the chiDinhCDHAList where donGiaBhyt equals to UPDATED_DON_GIA_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaBhyt.in=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaBhyt is not null
        defaultChiDinhCDHAShouldBeFound("donGiaBhyt.specified=true");

        // Get all the chiDinhCDHAList where donGiaBhyt is null
        defaultChiDinhCDHAShouldNotBeFound("donGiaBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaBhyt is greater than or equal to DEFAULT_DON_GIA_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaBhyt.greaterThanOrEqual=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhCDHAList where donGiaBhyt is greater than or equal to UPDATED_DON_GIA_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaBhyt.greaterThanOrEqual=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaBhyt is less than or equal to DEFAULT_DON_GIA_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaBhyt.lessThanOrEqual=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhCDHAList where donGiaBhyt is less than or equal to SMALLER_DON_GIA_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaBhyt.lessThanOrEqual=" + SMALLER_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaBhyt is less than DEFAULT_DON_GIA_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaBhyt.lessThan=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhCDHAList where donGiaBhyt is less than UPDATED_DON_GIA_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaBhyt.lessThan=" + UPDATED_DON_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaBhyt is greater than DEFAULT_DON_GIA_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaBhyt.greaterThan=" + DEFAULT_DON_GIA_BHYT);

        // Get all the chiDinhCDHAList where donGiaBhyt is greater than SMALLER_DON_GIA_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaBhyt.greaterThan=" + SMALLER_DON_GIA_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaKhongBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt equals to DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaKhongBhyt.equals=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt equals to UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaKhongBhyt.equals=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaKhongBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt not equals to DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaKhongBhyt.notEquals=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt not equals to UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaKhongBhyt.notEquals=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaKhongBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt in DEFAULT_DON_GIA_KHONG_BHYT or UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaKhongBhyt.in=" + DEFAULT_DON_GIA_KHONG_BHYT + "," + UPDATED_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt equals to UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaKhongBhyt.in=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaKhongBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt is not null
        defaultChiDinhCDHAShouldBeFound("donGiaKhongBhyt.specified=true");

        // Get all the chiDinhCDHAList where donGiaKhongBhyt is null
        defaultChiDinhCDHAShouldNotBeFound("donGiaKhongBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaKhongBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt is greater than or equal to DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaKhongBhyt.greaterThanOrEqual=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt is greater than or equal to UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaKhongBhyt.greaterThanOrEqual=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaKhongBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt is less than or equal to DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaKhongBhyt.lessThanOrEqual=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt is less than or equal to SMALLER_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaKhongBhyt.lessThanOrEqual=" + SMALLER_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaKhongBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt is less than DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaKhongBhyt.lessThan=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt is less than UPDATED_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaKhongBhyt.lessThan=" + UPDATED_DON_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonGiaKhongBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt is greater than DEFAULT_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("donGiaKhongBhyt.greaterThan=" + DEFAULT_DON_GIA_KHONG_BHYT);

        // Get all the chiDinhCDHAList where donGiaKhongBhyt is greater than SMALLER_DON_GIA_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("donGiaKhongBhyt.greaterThan=" + SMALLER_DON_GIA_KHONG_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByGhiChuChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where ghiChuChiDinh equals to DEFAULT_GHI_CHU_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("ghiChuChiDinh.equals=" + DEFAULT_GHI_CHU_CHI_DINH);

        // Get all the chiDinhCDHAList where ghiChuChiDinh equals to UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("ghiChuChiDinh.equals=" + UPDATED_GHI_CHU_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByGhiChuChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where ghiChuChiDinh not equals to DEFAULT_GHI_CHU_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("ghiChuChiDinh.notEquals=" + DEFAULT_GHI_CHU_CHI_DINH);

        // Get all the chiDinhCDHAList where ghiChuChiDinh not equals to UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("ghiChuChiDinh.notEquals=" + UPDATED_GHI_CHU_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByGhiChuChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where ghiChuChiDinh in DEFAULT_GHI_CHU_CHI_DINH or UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("ghiChuChiDinh.in=" + DEFAULT_GHI_CHU_CHI_DINH + "," + UPDATED_GHI_CHU_CHI_DINH);

        // Get all the chiDinhCDHAList where ghiChuChiDinh equals to UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("ghiChuChiDinh.in=" + UPDATED_GHI_CHU_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByGhiChuChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where ghiChuChiDinh is not null
        defaultChiDinhCDHAShouldBeFound("ghiChuChiDinh.specified=true");

        // Get all the chiDinhCDHAList where ghiChuChiDinh is null
        defaultChiDinhCDHAShouldNotBeFound("ghiChuChiDinh.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhCDHASByGhiChuChiDinhContainsSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where ghiChuChiDinh contains DEFAULT_GHI_CHU_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("ghiChuChiDinh.contains=" + DEFAULT_GHI_CHU_CHI_DINH);

        // Get all the chiDinhCDHAList where ghiChuChiDinh contains UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("ghiChuChiDinh.contains=" + UPDATED_GHI_CHU_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByGhiChuChiDinhNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where ghiChuChiDinh does not contain DEFAULT_GHI_CHU_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("ghiChuChiDinh.doesNotContain=" + DEFAULT_GHI_CHU_CHI_DINH);

        // Get all the chiDinhCDHAList where ghiChuChiDinh does not contain UPDATED_GHI_CHU_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("ghiChuChiDinh.doesNotContain=" + UPDATED_GHI_CHU_CHI_DINH);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where moTa equals to DEFAULT_MO_TA
        defaultChiDinhCDHAShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the chiDinhCDHAList where moTa equals to UPDATED_MO_TA
        defaultChiDinhCDHAShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where moTa not equals to DEFAULT_MO_TA
        defaultChiDinhCDHAShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the chiDinhCDHAList where moTa not equals to UPDATED_MO_TA
        defaultChiDinhCDHAShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultChiDinhCDHAShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the chiDinhCDHAList where moTa equals to UPDATED_MO_TA
        defaultChiDinhCDHAShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where moTa is not null
        defaultChiDinhCDHAShouldBeFound("moTa.specified=true");

        // Get all the chiDinhCDHAList where moTa is null
        defaultChiDinhCDHAShouldNotBeFound("moTa.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhCDHASByMoTaContainsSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where moTa contains DEFAULT_MO_TA
        defaultChiDinhCDHAShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the chiDinhCDHAList where moTa contains UPDATED_MO_TA
        defaultChiDinhCDHAShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where moTa does not contain DEFAULT_MO_TA
        defaultChiDinhCDHAShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the chiDinhCDHAList where moTa does not contain UPDATED_MO_TA
        defaultChiDinhCDHAShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByMoTaXm15IsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where moTaXm15 equals to DEFAULT_MO_TA_XM_15
        defaultChiDinhCDHAShouldBeFound("moTaXm15.equals=" + DEFAULT_MO_TA_XM_15);

        // Get all the chiDinhCDHAList where moTaXm15 equals to UPDATED_MO_TA_XM_15
        defaultChiDinhCDHAShouldNotBeFound("moTaXm15.equals=" + UPDATED_MO_TA_XM_15);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByMoTaXm15IsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where moTaXm15 not equals to DEFAULT_MO_TA_XM_15
        defaultChiDinhCDHAShouldNotBeFound("moTaXm15.notEquals=" + DEFAULT_MO_TA_XM_15);

        // Get all the chiDinhCDHAList where moTaXm15 not equals to UPDATED_MO_TA_XM_15
        defaultChiDinhCDHAShouldBeFound("moTaXm15.notEquals=" + UPDATED_MO_TA_XM_15);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByMoTaXm15IsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where moTaXm15 in DEFAULT_MO_TA_XM_15 or UPDATED_MO_TA_XM_15
        defaultChiDinhCDHAShouldBeFound("moTaXm15.in=" + DEFAULT_MO_TA_XM_15 + "," + UPDATED_MO_TA_XM_15);

        // Get all the chiDinhCDHAList where moTaXm15 equals to UPDATED_MO_TA_XM_15
        defaultChiDinhCDHAShouldNotBeFound("moTaXm15.in=" + UPDATED_MO_TA_XM_15);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByMoTaXm15IsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where moTaXm15 is not null
        defaultChiDinhCDHAShouldBeFound("moTaXm15.specified=true");

        // Get all the chiDinhCDHAList where moTaXm15 is null
        defaultChiDinhCDHAShouldNotBeFound("moTaXm15.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhCDHASByMoTaXm15ContainsSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where moTaXm15 contains DEFAULT_MO_TA_XM_15
        defaultChiDinhCDHAShouldBeFound("moTaXm15.contains=" + DEFAULT_MO_TA_XM_15);

        // Get all the chiDinhCDHAList where moTaXm15 contains UPDATED_MO_TA_XM_15
        defaultChiDinhCDHAShouldNotBeFound("moTaXm15.contains=" + UPDATED_MO_TA_XM_15);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByMoTaXm15NotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where moTaXm15 does not contain DEFAULT_MO_TA_XM_15
        defaultChiDinhCDHAShouldNotBeFound("moTaXm15.doesNotContain=" + DEFAULT_MO_TA_XM_15);

        // Get all the chiDinhCDHAList where moTaXm15 does not contain UPDATED_MO_TA_XM_15
        defaultChiDinhCDHAShouldBeFound("moTaXm15.doesNotContain=" + UPDATED_MO_TA_XM_15);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhIdIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinhId equals to DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinhId.equals=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhCDHAList where nguoiChiDinhId equals to UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinhId.equals=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinhId not equals to DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinhId.notEquals=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhCDHAList where nguoiChiDinhId not equals to UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinhId.notEquals=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhIdIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinhId in DEFAULT_NGUOI_CHI_DINH_ID or UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinhId.in=" + DEFAULT_NGUOI_CHI_DINH_ID + "," + UPDATED_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhCDHAList where nguoiChiDinhId equals to UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinhId.in=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinhId is not null
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinhId.specified=true");

        // Get all the chiDinhCDHAList where nguoiChiDinhId is null
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinhId.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinhId is greater than or equal to DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinhId.greaterThanOrEqual=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhCDHAList where nguoiChiDinhId is greater than or equal to UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinhId.greaterThanOrEqual=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinhId is less than or equal to DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinhId.lessThanOrEqual=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhCDHAList where nguoiChiDinhId is less than or equal to SMALLER_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinhId.lessThanOrEqual=" + SMALLER_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhIdIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinhId is less than DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinhId.lessThan=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhCDHAList where nguoiChiDinhId is less than UPDATED_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinhId.lessThan=" + UPDATED_NGUOI_CHI_DINH_ID);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinhId is greater than DEFAULT_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinhId.greaterThan=" + DEFAULT_NGUOI_CHI_DINH_ID);

        // Get all the chiDinhCDHAList where nguoiChiDinhId is greater than SMALLER_NGUOI_CHI_DINH_ID
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinhId.greaterThan=" + SMALLER_NGUOI_CHI_DINH_ID);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinh equals to DEFAULT_NGUOI_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinh.equals=" + DEFAULT_NGUOI_CHI_DINH);

        // Get all the chiDinhCDHAList where nguoiChiDinh equals to UPDATED_NGUOI_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinh.equals=" + UPDATED_NGUOI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinh not equals to DEFAULT_NGUOI_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinh.notEquals=" + DEFAULT_NGUOI_CHI_DINH);

        // Get all the chiDinhCDHAList where nguoiChiDinh not equals to UPDATED_NGUOI_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinh.notEquals=" + UPDATED_NGUOI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinh in DEFAULT_NGUOI_CHI_DINH or UPDATED_NGUOI_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinh.in=" + DEFAULT_NGUOI_CHI_DINH + "," + UPDATED_NGUOI_CHI_DINH);

        // Get all the chiDinhCDHAList where nguoiChiDinh equals to UPDATED_NGUOI_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinh.in=" + UPDATED_NGUOI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinh is not null
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinh.specified=true");

        // Get all the chiDinhCDHAList where nguoiChiDinh is null
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinh.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhContainsSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinh contains DEFAULT_NGUOI_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinh.contains=" + DEFAULT_NGUOI_CHI_DINH);

        // Get all the chiDinhCDHAList where nguoiChiDinh contains UPDATED_NGUOI_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinh.contains=" + UPDATED_NGUOI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNguoiChiDinhNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nguoiChiDinh does not contain DEFAULT_NGUOI_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("nguoiChiDinh.doesNotContain=" + DEFAULT_NGUOI_CHI_DINH);

        // Get all the chiDinhCDHAList where nguoiChiDinh does not contain UPDATED_NGUOI_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("nguoiChiDinh.doesNotContain=" + UPDATED_NGUOI_CHI_DINH);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLanChupIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLanChup equals to DEFAULT_SO_LAN_CHUP
        defaultChiDinhCDHAShouldBeFound("soLanChup.equals=" + DEFAULT_SO_LAN_CHUP);

        // Get all the chiDinhCDHAList where soLanChup equals to UPDATED_SO_LAN_CHUP
        defaultChiDinhCDHAShouldNotBeFound("soLanChup.equals=" + UPDATED_SO_LAN_CHUP);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLanChupIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLanChup not equals to DEFAULT_SO_LAN_CHUP
        defaultChiDinhCDHAShouldNotBeFound("soLanChup.notEquals=" + DEFAULT_SO_LAN_CHUP);

        // Get all the chiDinhCDHAList where soLanChup not equals to UPDATED_SO_LAN_CHUP
        defaultChiDinhCDHAShouldBeFound("soLanChup.notEquals=" + UPDATED_SO_LAN_CHUP);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLanChupIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLanChup in DEFAULT_SO_LAN_CHUP or UPDATED_SO_LAN_CHUP
        defaultChiDinhCDHAShouldBeFound("soLanChup.in=" + DEFAULT_SO_LAN_CHUP + "," + UPDATED_SO_LAN_CHUP);

        // Get all the chiDinhCDHAList where soLanChup equals to UPDATED_SO_LAN_CHUP
        defaultChiDinhCDHAShouldNotBeFound("soLanChup.in=" + UPDATED_SO_LAN_CHUP);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLanChupIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLanChup is not null
        defaultChiDinhCDHAShouldBeFound("soLanChup.specified=true");

        // Get all the chiDinhCDHAList where soLanChup is null
        defaultChiDinhCDHAShouldNotBeFound("soLanChup.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLanChupIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLanChup is greater than or equal to DEFAULT_SO_LAN_CHUP
        defaultChiDinhCDHAShouldBeFound("soLanChup.greaterThanOrEqual=" + DEFAULT_SO_LAN_CHUP);

        // Get all the chiDinhCDHAList where soLanChup is greater than or equal to UPDATED_SO_LAN_CHUP
        defaultChiDinhCDHAShouldNotBeFound("soLanChup.greaterThanOrEqual=" + UPDATED_SO_LAN_CHUP);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLanChupIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLanChup is less than or equal to DEFAULT_SO_LAN_CHUP
        defaultChiDinhCDHAShouldBeFound("soLanChup.lessThanOrEqual=" + DEFAULT_SO_LAN_CHUP);

        // Get all the chiDinhCDHAList where soLanChup is less than or equal to SMALLER_SO_LAN_CHUP
        defaultChiDinhCDHAShouldNotBeFound("soLanChup.lessThanOrEqual=" + SMALLER_SO_LAN_CHUP);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLanChupIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLanChup is less than DEFAULT_SO_LAN_CHUP
        defaultChiDinhCDHAShouldNotBeFound("soLanChup.lessThan=" + DEFAULT_SO_LAN_CHUP);

        // Get all the chiDinhCDHAList where soLanChup is less than UPDATED_SO_LAN_CHUP
        defaultChiDinhCDHAShouldBeFound("soLanChup.lessThan=" + UPDATED_SO_LAN_CHUP);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLanChupIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLanChup is greater than DEFAULT_SO_LAN_CHUP
        defaultChiDinhCDHAShouldNotBeFound("soLanChup.greaterThan=" + DEFAULT_SO_LAN_CHUP);

        // Get all the chiDinhCDHAList where soLanChup is greater than SMALLER_SO_LAN_CHUP
        defaultChiDinhCDHAShouldBeFound("soLanChup.greaterThan=" + SMALLER_SO_LAN_CHUP);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuong equals to DEFAULT_SO_LUONG
        defaultChiDinhCDHAShouldBeFound("soLuong.equals=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhCDHAList where soLuong equals to UPDATED_SO_LUONG
        defaultChiDinhCDHAShouldNotBeFound("soLuong.equals=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuong not equals to DEFAULT_SO_LUONG
        defaultChiDinhCDHAShouldNotBeFound("soLuong.notEquals=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhCDHAList where soLuong not equals to UPDATED_SO_LUONG
        defaultChiDinhCDHAShouldBeFound("soLuong.notEquals=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuong in DEFAULT_SO_LUONG or UPDATED_SO_LUONG
        defaultChiDinhCDHAShouldBeFound("soLuong.in=" + DEFAULT_SO_LUONG + "," + UPDATED_SO_LUONG);

        // Get all the chiDinhCDHAList where soLuong equals to UPDATED_SO_LUONG
        defaultChiDinhCDHAShouldNotBeFound("soLuong.in=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuong is not null
        defaultChiDinhCDHAShouldBeFound("soLuong.specified=true");

        // Get all the chiDinhCDHAList where soLuong is null
        defaultChiDinhCDHAShouldNotBeFound("soLuong.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuong is greater than or equal to DEFAULT_SO_LUONG
        defaultChiDinhCDHAShouldBeFound("soLuong.greaterThanOrEqual=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhCDHAList where soLuong is greater than or equal to UPDATED_SO_LUONG
        defaultChiDinhCDHAShouldNotBeFound("soLuong.greaterThanOrEqual=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuong is less than or equal to DEFAULT_SO_LUONG
        defaultChiDinhCDHAShouldBeFound("soLuong.lessThanOrEqual=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhCDHAList where soLuong is less than or equal to SMALLER_SO_LUONG
        defaultChiDinhCDHAShouldNotBeFound("soLuong.lessThanOrEqual=" + SMALLER_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuong is less than DEFAULT_SO_LUONG
        defaultChiDinhCDHAShouldNotBeFound("soLuong.lessThan=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhCDHAList where soLuong is less than UPDATED_SO_LUONG
        defaultChiDinhCDHAShouldBeFound("soLuong.lessThan=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuong is greater than DEFAULT_SO_LUONG
        defaultChiDinhCDHAShouldNotBeFound("soLuong.greaterThan=" + DEFAULT_SO_LUONG);

        // Get all the chiDinhCDHAList where soLuong is greater than SMALLER_SO_LUONG
        defaultChiDinhCDHAShouldBeFound("soLuong.greaterThan=" + SMALLER_SO_LUONG);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilmIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm equals to DEFAULT_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm.equals=" + DEFAULT_SO_LUONG_CO_FILM);

        // Get all the chiDinhCDHAList where soLuongCoFilm equals to UPDATED_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm.equals=" + UPDATED_SO_LUONG_CO_FILM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilmIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm not equals to DEFAULT_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm.notEquals=" + DEFAULT_SO_LUONG_CO_FILM);

        // Get all the chiDinhCDHAList where soLuongCoFilm not equals to UPDATED_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm.notEquals=" + UPDATED_SO_LUONG_CO_FILM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilmIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm in DEFAULT_SO_LUONG_CO_FILM or UPDATED_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm.in=" + DEFAULT_SO_LUONG_CO_FILM + "," + UPDATED_SO_LUONG_CO_FILM);

        // Get all the chiDinhCDHAList where soLuongCoFilm equals to UPDATED_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm.in=" + UPDATED_SO_LUONG_CO_FILM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilmIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm is not null
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm.specified=true");

        // Get all the chiDinhCDHAList where soLuongCoFilm is null
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilmIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm is greater than or equal to DEFAULT_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm.greaterThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM);

        // Get all the chiDinhCDHAList where soLuongCoFilm is greater than or equal to UPDATED_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm.greaterThanOrEqual=" + UPDATED_SO_LUONG_CO_FILM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilmIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm is less than or equal to DEFAULT_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm.lessThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM);

        // Get all the chiDinhCDHAList where soLuongCoFilm is less than or equal to SMALLER_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm.lessThanOrEqual=" + SMALLER_SO_LUONG_CO_FILM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilmIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm is less than DEFAULT_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm.lessThan=" + DEFAULT_SO_LUONG_CO_FILM);

        // Get all the chiDinhCDHAList where soLuongCoFilm is less than UPDATED_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm.lessThan=" + UPDATED_SO_LUONG_CO_FILM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilmIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm is greater than DEFAULT_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm.greaterThan=" + DEFAULT_SO_LUONG_CO_FILM);

        // Get all the chiDinhCDHAList where soLuongCoFilm is greater than SMALLER_SO_LUONG_CO_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm.greaterThan=" + SMALLER_SO_LUONG_CO_FILM);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1318IsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 equals to DEFAULT_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1318.equals=" + DEFAULT_SO_LUONG_CO_FILM_1318);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 equals to UPDATED_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1318.equals=" + UPDATED_SO_LUONG_CO_FILM_1318);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1318IsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 not equals to DEFAULT_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1318.notEquals=" + DEFAULT_SO_LUONG_CO_FILM_1318);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 not equals to UPDATED_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1318.notEquals=" + UPDATED_SO_LUONG_CO_FILM_1318);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1318IsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 in DEFAULT_SO_LUONG_CO_FILM_1318 or UPDATED_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1318.in=" + DEFAULT_SO_LUONG_CO_FILM_1318 + "," + UPDATED_SO_LUONG_CO_FILM_1318);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 equals to UPDATED_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1318.in=" + UPDATED_SO_LUONG_CO_FILM_1318);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1318IsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 is not null
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1318.specified=true");

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 is null
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1318.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1318IsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 is greater than or equal to DEFAULT_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1318.greaterThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM_1318);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 is greater than or equal to UPDATED_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1318.greaterThanOrEqual=" + UPDATED_SO_LUONG_CO_FILM_1318);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1318IsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 is less than or equal to DEFAULT_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1318.lessThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM_1318);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 is less than or equal to SMALLER_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1318.lessThanOrEqual=" + SMALLER_SO_LUONG_CO_FILM_1318);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1318IsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 is less than DEFAULT_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1318.lessThan=" + DEFAULT_SO_LUONG_CO_FILM_1318);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 is less than UPDATED_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1318.lessThan=" + UPDATED_SO_LUONG_CO_FILM_1318);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1318IsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 is greater than DEFAULT_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1318.greaterThan=" + DEFAULT_SO_LUONG_CO_FILM_1318);

        // Get all the chiDinhCDHAList where soLuongCoFilm1318 is greater than SMALLER_SO_LUONG_CO_FILM_1318
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1318.greaterThan=" + SMALLER_SO_LUONG_CO_FILM_1318);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1820IsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 equals to DEFAULT_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1820.equals=" + DEFAULT_SO_LUONG_CO_FILM_1820);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 equals to UPDATED_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1820.equals=" + UPDATED_SO_LUONG_CO_FILM_1820);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1820IsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 not equals to DEFAULT_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1820.notEquals=" + DEFAULT_SO_LUONG_CO_FILM_1820);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 not equals to UPDATED_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1820.notEquals=" + UPDATED_SO_LUONG_CO_FILM_1820);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1820IsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 in DEFAULT_SO_LUONG_CO_FILM_1820 or UPDATED_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1820.in=" + DEFAULT_SO_LUONG_CO_FILM_1820 + "," + UPDATED_SO_LUONG_CO_FILM_1820);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 equals to UPDATED_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1820.in=" + UPDATED_SO_LUONG_CO_FILM_1820);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1820IsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 is not null
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1820.specified=true");

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 is null
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1820.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1820IsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 is greater than or equal to DEFAULT_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1820.greaterThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM_1820);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 is greater than or equal to UPDATED_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1820.greaterThanOrEqual=" + UPDATED_SO_LUONG_CO_FILM_1820);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1820IsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 is less than or equal to DEFAULT_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1820.lessThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM_1820);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 is less than or equal to SMALLER_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1820.lessThanOrEqual=" + SMALLER_SO_LUONG_CO_FILM_1820);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1820IsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 is less than DEFAULT_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1820.lessThan=" + DEFAULT_SO_LUONG_CO_FILM_1820);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 is less than UPDATED_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1820.lessThan=" + UPDATED_SO_LUONG_CO_FILM_1820);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm1820IsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 is greater than DEFAULT_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm1820.greaterThan=" + DEFAULT_SO_LUONG_CO_FILM_1820);

        // Get all the chiDinhCDHAList where soLuongCoFilm1820 is greater than SMALLER_SO_LUONG_CO_FILM_1820
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm1820.greaterThan=" + SMALLER_SO_LUONG_CO_FILM_1820);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2025IsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 equals to DEFAULT_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2025.equals=" + DEFAULT_SO_LUONG_CO_FILM_2025);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 equals to UPDATED_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2025.equals=" + UPDATED_SO_LUONG_CO_FILM_2025);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2025IsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 not equals to DEFAULT_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2025.notEquals=" + DEFAULT_SO_LUONG_CO_FILM_2025);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 not equals to UPDATED_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2025.notEquals=" + UPDATED_SO_LUONG_CO_FILM_2025);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2025IsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 in DEFAULT_SO_LUONG_CO_FILM_2025 or UPDATED_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2025.in=" + DEFAULT_SO_LUONG_CO_FILM_2025 + "," + UPDATED_SO_LUONG_CO_FILM_2025);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 equals to UPDATED_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2025.in=" + UPDATED_SO_LUONG_CO_FILM_2025);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2025IsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 is not null
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2025.specified=true");

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 is null
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2025.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2025IsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 is greater than or equal to DEFAULT_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2025.greaterThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM_2025);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 is greater than or equal to UPDATED_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2025.greaterThanOrEqual=" + UPDATED_SO_LUONG_CO_FILM_2025);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2025IsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 is less than or equal to DEFAULT_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2025.lessThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM_2025);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 is less than or equal to SMALLER_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2025.lessThanOrEqual=" + SMALLER_SO_LUONG_CO_FILM_2025);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2025IsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 is less than DEFAULT_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2025.lessThan=" + DEFAULT_SO_LUONG_CO_FILM_2025);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 is less than UPDATED_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2025.lessThan=" + UPDATED_SO_LUONG_CO_FILM_2025);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2025IsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 is greater than DEFAULT_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2025.greaterThan=" + DEFAULT_SO_LUONG_CO_FILM_2025);

        // Get all the chiDinhCDHAList where soLuongCoFilm2025 is greater than SMALLER_SO_LUONG_CO_FILM_2025
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2025.greaterThan=" + SMALLER_SO_LUONG_CO_FILM_2025);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2430IsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 equals to DEFAULT_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2430.equals=" + DEFAULT_SO_LUONG_CO_FILM_2430);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 equals to UPDATED_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2430.equals=" + UPDATED_SO_LUONG_CO_FILM_2430);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2430IsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 not equals to DEFAULT_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2430.notEquals=" + DEFAULT_SO_LUONG_CO_FILM_2430);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 not equals to UPDATED_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2430.notEquals=" + UPDATED_SO_LUONG_CO_FILM_2430);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2430IsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 in DEFAULT_SO_LUONG_CO_FILM_2430 or UPDATED_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2430.in=" + DEFAULT_SO_LUONG_CO_FILM_2430 + "," + UPDATED_SO_LUONG_CO_FILM_2430);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 equals to UPDATED_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2430.in=" + UPDATED_SO_LUONG_CO_FILM_2430);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2430IsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 is not null
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2430.specified=true");

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 is null
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2430.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2430IsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 is greater than or equal to DEFAULT_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2430.greaterThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM_2430);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 is greater than or equal to UPDATED_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2430.greaterThanOrEqual=" + UPDATED_SO_LUONG_CO_FILM_2430);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2430IsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 is less than or equal to DEFAULT_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2430.lessThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM_2430);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 is less than or equal to SMALLER_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2430.lessThanOrEqual=" + SMALLER_SO_LUONG_CO_FILM_2430);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2430IsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 is less than DEFAULT_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2430.lessThan=" + DEFAULT_SO_LUONG_CO_FILM_2430);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 is less than UPDATED_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2430.lessThan=" + UPDATED_SO_LUONG_CO_FILM_2430);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2430IsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 is greater than DEFAULT_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2430.greaterThan=" + DEFAULT_SO_LUONG_CO_FILM_2430);

        // Get all the chiDinhCDHAList where soLuongCoFilm2430 is greater than SMALLER_SO_LUONG_CO_FILM_2430
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2430.greaterThan=" + SMALLER_SO_LUONG_CO_FILM_2430);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2530IsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 equals to DEFAULT_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2530.equals=" + DEFAULT_SO_LUONG_CO_FILM_2530);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 equals to UPDATED_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2530.equals=" + UPDATED_SO_LUONG_CO_FILM_2530);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2530IsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 not equals to DEFAULT_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2530.notEquals=" + DEFAULT_SO_LUONG_CO_FILM_2530);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 not equals to UPDATED_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2530.notEquals=" + UPDATED_SO_LUONG_CO_FILM_2530);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2530IsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 in DEFAULT_SO_LUONG_CO_FILM_2530 or UPDATED_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2530.in=" + DEFAULT_SO_LUONG_CO_FILM_2530 + "," + UPDATED_SO_LUONG_CO_FILM_2530);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 equals to UPDATED_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2530.in=" + UPDATED_SO_LUONG_CO_FILM_2530);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2530IsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 is not null
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2530.specified=true");

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 is null
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2530.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2530IsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 is greater than or equal to DEFAULT_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2530.greaterThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM_2530);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 is greater than or equal to UPDATED_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2530.greaterThanOrEqual=" + UPDATED_SO_LUONG_CO_FILM_2530);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2530IsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 is less than or equal to DEFAULT_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2530.lessThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM_2530);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 is less than or equal to SMALLER_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2530.lessThanOrEqual=" + SMALLER_SO_LUONG_CO_FILM_2530);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2530IsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 is less than DEFAULT_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2530.lessThan=" + DEFAULT_SO_LUONG_CO_FILM_2530);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 is less than UPDATED_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2530.lessThan=" + UPDATED_SO_LUONG_CO_FILM_2530);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm2530IsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 is greater than DEFAULT_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm2530.greaterThan=" + DEFAULT_SO_LUONG_CO_FILM_2530);

        // Get all the chiDinhCDHAList where soLuongCoFilm2530 is greater than SMALLER_SO_LUONG_CO_FILM_2530
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm2530.greaterThan=" + SMALLER_SO_LUONG_CO_FILM_2530);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm3040IsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 equals to DEFAULT_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm3040.equals=" + DEFAULT_SO_LUONG_CO_FILM_3040);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 equals to UPDATED_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm3040.equals=" + UPDATED_SO_LUONG_CO_FILM_3040);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm3040IsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 not equals to DEFAULT_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm3040.notEquals=" + DEFAULT_SO_LUONG_CO_FILM_3040);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 not equals to UPDATED_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm3040.notEquals=" + UPDATED_SO_LUONG_CO_FILM_3040);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm3040IsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 in DEFAULT_SO_LUONG_CO_FILM_3040 or UPDATED_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm3040.in=" + DEFAULT_SO_LUONG_CO_FILM_3040 + "," + UPDATED_SO_LUONG_CO_FILM_3040);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 equals to UPDATED_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm3040.in=" + UPDATED_SO_LUONG_CO_FILM_3040);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm3040IsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 is not null
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm3040.specified=true");

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 is null
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm3040.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm3040IsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 is greater than or equal to DEFAULT_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm3040.greaterThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM_3040);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 is greater than or equal to UPDATED_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm3040.greaterThanOrEqual=" + UPDATED_SO_LUONG_CO_FILM_3040);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm3040IsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 is less than or equal to DEFAULT_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm3040.lessThanOrEqual=" + DEFAULT_SO_LUONG_CO_FILM_3040);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 is less than or equal to SMALLER_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm3040.lessThanOrEqual=" + SMALLER_SO_LUONG_CO_FILM_3040);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm3040IsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 is less than DEFAULT_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm3040.lessThan=" + DEFAULT_SO_LUONG_CO_FILM_3040);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 is less than UPDATED_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm3040.lessThan=" + UPDATED_SO_LUONG_CO_FILM_3040);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongCoFilm3040IsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 is greater than DEFAULT_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldNotBeFound("soLuongCoFilm3040.greaterThan=" + DEFAULT_SO_LUONG_CO_FILM_3040);

        // Get all the chiDinhCDHAList where soLuongCoFilm3040 is greater than SMALLER_SO_LUONG_CO_FILM_3040
        defaultChiDinhCDHAShouldBeFound("soLuongCoFilm3040.greaterThan=" + SMALLER_SO_LUONG_CO_FILM_3040);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongFilmIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongFilm equals to DEFAULT_SO_LUONG_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongFilm.equals=" + DEFAULT_SO_LUONG_FILM);

        // Get all the chiDinhCDHAList where soLuongFilm equals to UPDATED_SO_LUONG_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongFilm.equals=" + UPDATED_SO_LUONG_FILM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongFilmIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongFilm not equals to DEFAULT_SO_LUONG_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongFilm.notEquals=" + DEFAULT_SO_LUONG_FILM);

        // Get all the chiDinhCDHAList where soLuongFilm not equals to UPDATED_SO_LUONG_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongFilm.notEquals=" + UPDATED_SO_LUONG_FILM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongFilmIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongFilm in DEFAULT_SO_LUONG_FILM or UPDATED_SO_LUONG_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongFilm.in=" + DEFAULT_SO_LUONG_FILM + "," + UPDATED_SO_LUONG_FILM);

        // Get all the chiDinhCDHAList where soLuongFilm equals to UPDATED_SO_LUONG_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongFilm.in=" + UPDATED_SO_LUONG_FILM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongFilmIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongFilm is not null
        defaultChiDinhCDHAShouldBeFound("soLuongFilm.specified=true");

        // Get all the chiDinhCDHAList where soLuongFilm is null
        defaultChiDinhCDHAShouldNotBeFound("soLuongFilm.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongFilmIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongFilm is greater than or equal to DEFAULT_SO_LUONG_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongFilm.greaterThanOrEqual=" + DEFAULT_SO_LUONG_FILM);

        // Get all the chiDinhCDHAList where soLuongFilm is greater than or equal to UPDATED_SO_LUONG_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongFilm.greaterThanOrEqual=" + UPDATED_SO_LUONG_FILM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongFilmIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongFilm is less than or equal to DEFAULT_SO_LUONG_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongFilm.lessThanOrEqual=" + DEFAULT_SO_LUONG_FILM);

        // Get all the chiDinhCDHAList where soLuongFilm is less than or equal to SMALLER_SO_LUONG_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongFilm.lessThanOrEqual=" + SMALLER_SO_LUONG_FILM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongFilmIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongFilm is less than DEFAULT_SO_LUONG_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongFilm.lessThan=" + DEFAULT_SO_LUONG_FILM);

        // Get all the chiDinhCDHAList where soLuongFilm is less than UPDATED_SO_LUONG_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongFilm.lessThan=" + UPDATED_SO_LUONG_FILM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASBySoLuongFilmIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where soLuongFilm is greater than DEFAULT_SO_LUONG_FILM
        defaultChiDinhCDHAShouldNotBeFound("soLuongFilm.greaterThan=" + DEFAULT_SO_LUONG_FILM);

        // Get all the chiDinhCDHAList where soLuongFilm is greater than SMALLER_SO_LUONG_FILM
        defaultChiDinhCDHAShouldBeFound("soLuongFilm.greaterThan=" + SMALLER_SO_LUONG_FILM);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where ten equals to DEFAULT_TEN
        defaultChiDinhCDHAShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the chiDinhCDHAList where ten equals to UPDATED_TEN
        defaultChiDinhCDHAShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where ten not equals to DEFAULT_TEN
        defaultChiDinhCDHAShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the chiDinhCDHAList where ten not equals to UPDATED_TEN
        defaultChiDinhCDHAShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTenIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultChiDinhCDHAShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the chiDinhCDHAList where ten equals to UPDATED_TEN
        defaultChiDinhCDHAShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where ten is not null
        defaultChiDinhCDHAShouldBeFound("ten.specified=true");

        // Get all the chiDinhCDHAList where ten is null
        defaultChiDinhCDHAShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhCDHASByTenContainsSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where ten contains DEFAULT_TEN
        defaultChiDinhCDHAShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the chiDinhCDHAList where ten contains UPDATED_TEN
        defaultChiDinhCDHAShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTenNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where ten does not contain DEFAULT_TEN
        defaultChiDinhCDHAShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the chiDinhCDHAList where ten does not contain UPDATED_TEN
        defaultChiDinhCDHAShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTien equals to DEFAULT_THANH_TIEN
        defaultChiDinhCDHAShouldBeFound("thanhTien.equals=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhCDHAList where thanhTien equals to UPDATED_THANH_TIEN
        defaultChiDinhCDHAShouldNotBeFound("thanhTien.equals=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTien not equals to DEFAULT_THANH_TIEN
        defaultChiDinhCDHAShouldNotBeFound("thanhTien.notEquals=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhCDHAList where thanhTien not equals to UPDATED_THANH_TIEN
        defaultChiDinhCDHAShouldBeFound("thanhTien.notEquals=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTien in DEFAULT_THANH_TIEN or UPDATED_THANH_TIEN
        defaultChiDinhCDHAShouldBeFound("thanhTien.in=" + DEFAULT_THANH_TIEN + "," + UPDATED_THANH_TIEN);

        // Get all the chiDinhCDHAList where thanhTien equals to UPDATED_THANH_TIEN
        defaultChiDinhCDHAShouldNotBeFound("thanhTien.in=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTien is not null
        defaultChiDinhCDHAShouldBeFound("thanhTien.specified=true");

        // Get all the chiDinhCDHAList where thanhTien is null
        defaultChiDinhCDHAShouldNotBeFound("thanhTien.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTien is greater than or equal to DEFAULT_THANH_TIEN
        defaultChiDinhCDHAShouldBeFound("thanhTien.greaterThanOrEqual=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhCDHAList where thanhTien is greater than or equal to UPDATED_THANH_TIEN
        defaultChiDinhCDHAShouldNotBeFound("thanhTien.greaterThanOrEqual=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTien is less than or equal to DEFAULT_THANH_TIEN
        defaultChiDinhCDHAShouldBeFound("thanhTien.lessThanOrEqual=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhCDHAList where thanhTien is less than or equal to SMALLER_THANH_TIEN
        defaultChiDinhCDHAShouldNotBeFound("thanhTien.lessThanOrEqual=" + SMALLER_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTien is less than DEFAULT_THANH_TIEN
        defaultChiDinhCDHAShouldNotBeFound("thanhTien.lessThan=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhCDHAList where thanhTien is less than UPDATED_THANH_TIEN
        defaultChiDinhCDHAShouldBeFound("thanhTien.lessThan=" + UPDATED_THANH_TIEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTien is greater than DEFAULT_THANH_TIEN
        defaultChiDinhCDHAShouldNotBeFound("thanhTien.greaterThan=" + DEFAULT_THANH_TIEN);

        // Get all the chiDinhCDHAList where thanhTien is greater than SMALLER_THANH_TIEN
        defaultChiDinhCDHAShouldBeFound("thanhTien.greaterThan=" + SMALLER_THANH_TIEN);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienBhyt equals to DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienBhyt.equals=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhCDHAList where thanhTienBhyt equals to UPDATED_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienBhyt.equals=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienBhyt not equals to DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienBhyt.notEquals=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhCDHAList where thanhTienBhyt not equals to UPDATED_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienBhyt.notEquals=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienBhyt in DEFAULT_THANH_TIEN_BHYT or UPDATED_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienBhyt.in=" + DEFAULT_THANH_TIEN_BHYT + "," + UPDATED_THANH_TIEN_BHYT);

        // Get all the chiDinhCDHAList where thanhTienBhyt equals to UPDATED_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienBhyt.in=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienBhyt is not null
        defaultChiDinhCDHAShouldBeFound("thanhTienBhyt.specified=true");

        // Get all the chiDinhCDHAList where thanhTienBhyt is null
        defaultChiDinhCDHAShouldNotBeFound("thanhTienBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienBhyt is greater than or equal to DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienBhyt.greaterThanOrEqual=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhCDHAList where thanhTienBhyt is greater than or equal to UPDATED_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienBhyt.greaterThanOrEqual=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienBhyt is less than or equal to DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienBhyt.lessThanOrEqual=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhCDHAList where thanhTienBhyt is less than or equal to SMALLER_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienBhyt.lessThanOrEqual=" + SMALLER_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienBhyt is less than DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienBhyt.lessThan=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhCDHAList where thanhTienBhyt is less than UPDATED_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienBhyt.lessThan=" + UPDATED_THANH_TIEN_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienBhyt is greater than DEFAULT_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienBhyt.greaterThan=" + DEFAULT_THANH_TIEN_BHYT);

        // Get all the chiDinhCDHAList where thanhTienBhyt is greater than SMALLER_THANH_TIEN_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienBhyt.greaterThan=" + SMALLER_THANH_TIEN_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienKhongBHYTIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT equals to DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienKhongBHYT.equals=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT equals to UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienKhongBHYT.equals=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienKhongBHYTIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT not equals to DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienKhongBHYT.notEquals=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT not equals to UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienKhongBHYT.notEquals=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienKhongBHYTIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT in DEFAULT_THANH_TIEN_KHONG_BHYT or UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienKhongBHYT.in=" + DEFAULT_THANH_TIEN_KHONG_BHYT + "," + UPDATED_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT equals to UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienKhongBHYT.in=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienKhongBHYTIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT is not null
        defaultChiDinhCDHAShouldBeFound("thanhTienKhongBHYT.specified=true");

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT is null
        defaultChiDinhCDHAShouldNotBeFound("thanhTienKhongBHYT.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienKhongBHYTIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT is greater than or equal to DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienKhongBHYT.greaterThanOrEqual=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT is greater than or equal to UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienKhongBHYT.greaterThanOrEqual=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienKhongBHYTIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT is less than or equal to DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienKhongBHYT.lessThanOrEqual=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT is less than or equal to SMALLER_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienKhongBHYT.lessThanOrEqual=" + SMALLER_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienKhongBHYTIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT is less than DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienKhongBHYT.lessThan=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT is less than UPDATED_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienKhongBHYT.lessThan=" + UPDATED_THANH_TIEN_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhTienKhongBHYTIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT is greater than DEFAULT_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldNotBeFound("thanhTienKhongBHYT.greaterThan=" + DEFAULT_THANH_TIEN_KHONG_BHYT);

        // Get all the chiDinhCDHAList where thanhTienKhongBHYT is greater than SMALLER_THANH_TIEN_KHONG_BHYT
        defaultChiDinhCDHAShouldBeFound("thanhTienKhongBHYT.greaterThan=" + SMALLER_THANH_TIEN_KHONG_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianChiDinh equals to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("thoiGianChiDinh.equals=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhCDHAList where thoiGianChiDinh equals to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("thoiGianChiDinh.equals=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianChiDinh not equals to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("thoiGianChiDinh.notEquals=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhCDHAList where thoiGianChiDinh not equals to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("thoiGianChiDinh.notEquals=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianChiDinh in DEFAULT_THOI_GIAN_CHI_DINH or UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("thoiGianChiDinh.in=" + DEFAULT_THOI_GIAN_CHI_DINH + "," + UPDATED_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhCDHAList where thoiGianChiDinh equals to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("thoiGianChiDinh.in=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianChiDinh is not null
        defaultChiDinhCDHAShouldBeFound("thoiGianChiDinh.specified=true");

        // Get all the chiDinhCDHAList where thoiGianChiDinh is null
        defaultChiDinhCDHAShouldNotBeFound("thoiGianChiDinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianChiDinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianChiDinh is greater than or equal to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("thoiGianChiDinh.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhCDHAList where thoiGianChiDinh is greater than or equal to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("thoiGianChiDinh.greaterThanOrEqual=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianChiDinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianChiDinh is less than or equal to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("thoiGianChiDinh.lessThanOrEqual=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhCDHAList where thoiGianChiDinh is less than or equal to SMALLER_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("thoiGianChiDinh.lessThanOrEqual=" + SMALLER_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianChiDinhIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianChiDinh is less than DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("thoiGianChiDinh.lessThan=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhCDHAList where thoiGianChiDinh is less than UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("thoiGianChiDinh.lessThan=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianChiDinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianChiDinh is greater than DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldNotBeFound("thoiGianChiDinh.greaterThan=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhCDHAList where thoiGianChiDinh is greater than SMALLER_THOI_GIAN_CHI_DINH
        defaultChiDinhCDHAShouldBeFound("thoiGianChiDinh.greaterThan=" + SMALLER_THOI_GIAN_CHI_DINH);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianTaoIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianTao equals to DEFAULT_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldBeFound("thoiGianTao.equals=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhCDHAList where thoiGianTao equals to UPDATED_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldNotBeFound("thoiGianTao.equals=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianTaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianTao not equals to DEFAULT_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldNotBeFound("thoiGianTao.notEquals=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhCDHAList where thoiGianTao not equals to UPDATED_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldBeFound("thoiGianTao.notEquals=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianTaoIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianTao in DEFAULT_THOI_GIAN_TAO or UPDATED_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldBeFound("thoiGianTao.in=" + DEFAULT_THOI_GIAN_TAO + "," + UPDATED_THOI_GIAN_TAO);

        // Get all the chiDinhCDHAList where thoiGianTao equals to UPDATED_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldNotBeFound("thoiGianTao.in=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianTaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianTao is not null
        defaultChiDinhCDHAShouldBeFound("thoiGianTao.specified=true");

        // Get all the chiDinhCDHAList where thoiGianTao is null
        defaultChiDinhCDHAShouldNotBeFound("thoiGianTao.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianTaoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianTao is greater than or equal to DEFAULT_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldBeFound("thoiGianTao.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhCDHAList where thoiGianTao is greater than or equal to UPDATED_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldNotBeFound("thoiGianTao.greaterThanOrEqual=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianTaoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianTao is less than or equal to DEFAULT_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldBeFound("thoiGianTao.lessThanOrEqual=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhCDHAList where thoiGianTao is less than or equal to SMALLER_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldNotBeFound("thoiGianTao.lessThanOrEqual=" + SMALLER_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianTaoIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianTao is less than DEFAULT_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldNotBeFound("thoiGianTao.lessThan=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhCDHAList where thoiGianTao is less than UPDATED_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldBeFound("thoiGianTao.lessThan=" + UPDATED_THOI_GIAN_TAO);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThoiGianTaoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thoiGianTao is greater than DEFAULT_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldNotBeFound("thoiGianTao.greaterThan=" + DEFAULT_THOI_GIAN_TAO);

        // Get all the chiDinhCDHAList where thoiGianTao is greater than SMALLER_THOI_GIAN_TAO
        defaultChiDinhCDHAShouldBeFound("thoiGianTao.greaterThan=" + SMALLER_THOI_GIAN_TAO);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByTienNgoaiBHYTIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldBeFound("tienNgoaiBHYT.equals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT equals to UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldNotBeFound("tienNgoaiBHYT.equals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTienNgoaiBHYTIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT not equals to DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldNotBeFound("tienNgoaiBHYT.notEquals=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT not equals to UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldBeFound("tienNgoaiBHYT.notEquals=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTienNgoaiBHYTIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT in DEFAULT_TIEN_NGOAI_BHYT or UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldBeFound("tienNgoaiBHYT.in=" + DEFAULT_TIEN_NGOAI_BHYT + "," + UPDATED_TIEN_NGOAI_BHYT);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT equals to UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldNotBeFound("tienNgoaiBHYT.in=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTienNgoaiBHYTIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT is not null
        defaultChiDinhCDHAShouldBeFound("tienNgoaiBHYT.specified=true");

        // Get all the chiDinhCDHAList where tienNgoaiBHYT is null
        defaultChiDinhCDHAShouldNotBeFound("tienNgoaiBHYT.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTienNgoaiBHYTIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT is greater than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldBeFound("tienNgoaiBHYT.greaterThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT is greater than or equal to UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldNotBeFound("tienNgoaiBHYT.greaterThanOrEqual=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTienNgoaiBHYTIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT is less than or equal to DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldBeFound("tienNgoaiBHYT.lessThanOrEqual=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT is less than or equal to SMALLER_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldNotBeFound("tienNgoaiBHYT.lessThanOrEqual=" + SMALLER_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTienNgoaiBHYTIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT is less than DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldNotBeFound("tienNgoaiBHYT.lessThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT is less than UPDATED_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldBeFound("tienNgoaiBHYT.lessThan=" + UPDATED_TIEN_NGOAI_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTienNgoaiBHYTIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT is greater than DEFAULT_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldNotBeFound("tienNgoaiBHYT.greaterThan=" + DEFAULT_TIEN_NGOAI_BHYT);

        // Get all the chiDinhCDHAList where tienNgoaiBHYT is greater than SMALLER_TIEN_NGOAI_BHYT
        defaultChiDinhCDHAShouldBeFound("tienNgoaiBHYT.greaterThan=" + SMALLER_TIEN_NGOAI_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhToanChenhLechIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhToanChenhLech equals to DEFAULT_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldBeFound("thanhToanChenhLech.equals=" + DEFAULT_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhCDHAList where thanhToanChenhLech equals to UPDATED_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldNotBeFound("thanhToanChenhLech.equals=" + UPDATED_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhToanChenhLechIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhToanChenhLech not equals to DEFAULT_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldNotBeFound("thanhToanChenhLech.notEquals=" + DEFAULT_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhCDHAList where thanhToanChenhLech not equals to UPDATED_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldBeFound("thanhToanChenhLech.notEquals=" + UPDATED_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhToanChenhLechIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhToanChenhLech in DEFAULT_THANH_TOAN_CHENH_LECH or UPDATED_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldBeFound("thanhToanChenhLech.in=" + DEFAULT_THANH_TOAN_CHENH_LECH + "," + UPDATED_THANH_TOAN_CHENH_LECH);

        // Get all the chiDinhCDHAList where thanhToanChenhLech equals to UPDATED_THANH_TOAN_CHENH_LECH
        defaultChiDinhCDHAShouldNotBeFound("thanhToanChenhLech.in=" + UPDATED_THANH_TOAN_CHENH_LECH);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByThanhToanChenhLechIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where thanhToanChenhLech is not null
        defaultChiDinhCDHAShouldBeFound("thanhToanChenhLech.specified=true");

        // Get all the chiDinhCDHAList where thanhToanChenhLech is null
        defaultChiDinhCDHAShouldNotBeFound("thanhToanChenhLech.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTyLeThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tyLeThanhToan equals to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("tyLeThanhToan.equals=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhCDHAList where tyLeThanhToan equals to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("tyLeThanhToan.equals=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTyLeThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tyLeThanhToan not equals to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("tyLeThanhToan.notEquals=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhCDHAList where tyLeThanhToan not equals to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("tyLeThanhToan.notEquals=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTyLeThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tyLeThanhToan in DEFAULT_TY_LE_THANH_TOAN or UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("tyLeThanhToan.in=" + DEFAULT_TY_LE_THANH_TOAN + "," + UPDATED_TY_LE_THANH_TOAN);

        // Get all the chiDinhCDHAList where tyLeThanhToan equals to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("tyLeThanhToan.in=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTyLeThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tyLeThanhToan is not null
        defaultChiDinhCDHAShouldBeFound("tyLeThanhToan.specified=true");

        // Get all the chiDinhCDHAList where tyLeThanhToan is null
        defaultChiDinhCDHAShouldNotBeFound("tyLeThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTyLeThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tyLeThanhToan is greater than or equal to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("tyLeThanhToan.greaterThanOrEqual=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhCDHAList where tyLeThanhToan is greater than or equal to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("tyLeThanhToan.greaterThanOrEqual=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTyLeThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tyLeThanhToan is less than or equal to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("tyLeThanhToan.lessThanOrEqual=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhCDHAList where tyLeThanhToan is less than or equal to SMALLER_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("tyLeThanhToan.lessThanOrEqual=" + SMALLER_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTyLeThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tyLeThanhToan is less than DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("tyLeThanhToan.lessThan=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhCDHAList where tyLeThanhToan is less than UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("tyLeThanhToan.lessThan=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByTyLeThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where tyLeThanhToan is greater than DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldNotBeFound("tyLeThanhToan.greaterThan=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhCDHAList where tyLeThanhToan is greater than SMALLER_TY_LE_THANH_TOAN
        defaultChiDinhCDHAShouldBeFound("tyLeThanhToan.greaterThan=" + SMALLER_TY_LE_THANH_TOAN);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByMaDungChungIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where maDungChung equals to DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhCDHAShouldBeFound("maDungChung.equals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhCDHAList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultChiDinhCDHAShouldNotBeFound("maDungChung.equals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByMaDungChungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where maDungChung not equals to DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhCDHAShouldNotBeFound("maDungChung.notEquals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhCDHAList where maDungChung not equals to UPDATED_MA_DUNG_CHUNG
        defaultChiDinhCDHAShouldBeFound("maDungChung.notEquals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByMaDungChungIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where maDungChung in DEFAULT_MA_DUNG_CHUNG or UPDATED_MA_DUNG_CHUNG
        defaultChiDinhCDHAShouldBeFound("maDungChung.in=" + DEFAULT_MA_DUNG_CHUNG + "," + UPDATED_MA_DUNG_CHUNG);

        // Get all the chiDinhCDHAList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultChiDinhCDHAShouldNotBeFound("maDungChung.in=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByMaDungChungIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where maDungChung is not null
        defaultChiDinhCDHAShouldBeFound("maDungChung.specified=true");

        // Get all the chiDinhCDHAList where maDungChung is null
        defaultChiDinhCDHAShouldNotBeFound("maDungChung.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhCDHASByMaDungChungContainsSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where maDungChung contains DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhCDHAShouldBeFound("maDungChung.contains=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhCDHAList where maDungChung contains UPDATED_MA_DUNG_CHUNG
        defaultChiDinhCDHAShouldNotBeFound("maDungChung.contains=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByMaDungChungNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where maDungChung does not contain DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhCDHAShouldNotBeFound("maDungChung.doesNotContain=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhCDHAList where maDungChung does not contain UPDATED_MA_DUNG_CHUNG
        defaultChiDinhCDHAShouldBeFound("maDungChung.doesNotContain=" + UPDATED_MA_DUNG_CHUNG);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByDichVuYeuCauIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where dichVuYeuCau equals to DEFAULT_DICH_VU_YEU_CAU
        defaultChiDinhCDHAShouldBeFound("dichVuYeuCau.equals=" + DEFAULT_DICH_VU_YEU_CAU);

        // Get all the chiDinhCDHAList where dichVuYeuCau equals to UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhCDHAShouldNotBeFound("dichVuYeuCau.equals=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDichVuYeuCauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where dichVuYeuCau not equals to DEFAULT_DICH_VU_YEU_CAU
        defaultChiDinhCDHAShouldNotBeFound("dichVuYeuCau.notEquals=" + DEFAULT_DICH_VU_YEU_CAU);

        // Get all the chiDinhCDHAList where dichVuYeuCau not equals to UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhCDHAShouldBeFound("dichVuYeuCau.notEquals=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDichVuYeuCauIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where dichVuYeuCau in DEFAULT_DICH_VU_YEU_CAU or UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhCDHAShouldBeFound("dichVuYeuCau.in=" + DEFAULT_DICH_VU_YEU_CAU + "," + UPDATED_DICH_VU_YEU_CAU);

        // Get all the chiDinhCDHAList where dichVuYeuCau equals to UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhCDHAShouldNotBeFound("dichVuYeuCau.in=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByDichVuYeuCauIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where dichVuYeuCau is not null
        defaultChiDinhCDHAShouldBeFound("dichVuYeuCau.specified=true");

        // Get all the chiDinhCDHAList where dichVuYeuCau is null
        defaultChiDinhCDHAShouldNotBeFound("dichVuYeuCau.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNamIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nam equals to DEFAULT_NAM
        defaultChiDinhCDHAShouldBeFound("nam.equals=" + DEFAULT_NAM);

        // Get all the chiDinhCDHAList where nam equals to UPDATED_NAM
        defaultChiDinhCDHAShouldNotBeFound("nam.equals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nam not equals to DEFAULT_NAM
        defaultChiDinhCDHAShouldNotBeFound("nam.notEquals=" + DEFAULT_NAM);

        // Get all the chiDinhCDHAList where nam not equals to UPDATED_NAM
        defaultChiDinhCDHAShouldBeFound("nam.notEquals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNamIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nam in DEFAULT_NAM or UPDATED_NAM
        defaultChiDinhCDHAShouldBeFound("nam.in=" + DEFAULT_NAM + "," + UPDATED_NAM);

        // Get all the chiDinhCDHAList where nam equals to UPDATED_NAM
        defaultChiDinhCDHAShouldNotBeFound("nam.in=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nam is not null
        defaultChiDinhCDHAShouldBeFound("nam.specified=true");

        // Get all the chiDinhCDHAList where nam is null
        defaultChiDinhCDHAShouldNotBeFound("nam.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nam is greater than or equal to DEFAULT_NAM
        defaultChiDinhCDHAShouldBeFound("nam.greaterThanOrEqual=" + DEFAULT_NAM);

        // Get all the chiDinhCDHAList where nam is greater than or equal to UPDATED_NAM
        defaultChiDinhCDHAShouldNotBeFound("nam.greaterThanOrEqual=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nam is less than or equal to DEFAULT_NAM
        defaultChiDinhCDHAShouldBeFound("nam.lessThanOrEqual=" + DEFAULT_NAM);

        // Get all the chiDinhCDHAList where nam is less than or equal to SMALLER_NAM
        defaultChiDinhCDHAShouldNotBeFound("nam.lessThanOrEqual=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNamIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nam is less than DEFAULT_NAM
        defaultChiDinhCDHAShouldNotBeFound("nam.lessThan=" + DEFAULT_NAM);

        // Get all the chiDinhCDHAList where nam is less than UPDATED_NAM
        defaultChiDinhCDHAShouldBeFound("nam.lessThan=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhCDHASByNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        // Get all the chiDinhCDHAList where nam is greater than DEFAULT_NAM
        defaultChiDinhCDHAShouldNotBeFound("nam.greaterThan=" + DEFAULT_NAM);

        // Get all the chiDinhCDHAList where nam is greater than SMALLER_NAM
        defaultChiDinhCDHAShouldBeFound("nam.greaterThan=" + SMALLER_NAM);
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        PhieuChiDinhCDHA donVi = chiDinhCDHA.getDonVi();
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);
        Long donViId = donVi.getId();

        // Get all the chiDinhCDHAList where donVi equals to donViId
        defaultChiDinhCDHAShouldBeFound("donViId.equals=" + donViId);

        // Get all the chiDinhCDHAList where donVi equals to donViId + 1
        defaultChiDinhCDHAShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByBenhNhanIsEqualToSomething() throws Exception {
        // Get already existing entity
        PhieuChiDinhCDHA benhNhan = chiDinhCDHA.getBenhNhan();
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);
        Long benhNhanId = benhNhan.getId();

        // Get all the chiDinhCDHAList where benhNhan equals to benhNhanId
        defaultChiDinhCDHAShouldBeFound("benhNhanId.equals=" + benhNhanId);

        // Get all the chiDinhCDHAList where benhNhan equals to benhNhanId + 1
        defaultChiDinhCDHAShouldNotBeFound("benhNhanId.equals=" + (benhNhanId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByBakbIsEqualToSomething() throws Exception {
        // Get already existing entity
        PhieuChiDinhCDHA bakb = chiDinhCDHA.getBakb();
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);
        Long bakbId = bakb.getId();

        // Get all the chiDinhCDHAList where bakb equals to bakbId
        defaultChiDinhCDHAShouldBeFound("bakbId.equals=" + bakbId);

        // Get all the chiDinhCDHAList where bakb equals to bakbId + 1
        defaultChiDinhCDHAShouldNotBeFound("bakbId.equals=" + (bakbId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByPhieuCDIsEqualToSomething() throws Exception {
        // Get already existing entity
        PhieuChiDinhCDHA phieuCD = chiDinhCDHA.getPhieuCD();
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);
        Long phieuCDId = phieuCD.getId();

        // Get all the chiDinhCDHAList where phieuCD equals to phieuCDId
        defaultChiDinhCDHAShouldBeFound("phieuCDId.equals=" + phieuCDId);

        // Get all the chiDinhCDHAList where phieuCD equals to phieuCDId + 1
        defaultChiDinhCDHAShouldNotBeFound("phieuCDId.equals=" + (phieuCDId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByPhongIsEqualToSomething() throws Exception {
        // Get already existing entity
        Phong phong = chiDinhCDHA.getPhong();
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);
        Long phongId = phong.getId();

        // Get all the chiDinhCDHAList where phong equals to phongId
        defaultChiDinhCDHAShouldBeFound("phongId.equals=" + phongId);

        // Get all the chiDinhCDHAList where phong equals to phongId + 1
        defaultChiDinhCDHAShouldNotBeFound("phongId.equals=" + (phongId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhCDHASByCdhaIsEqualToSomething() throws Exception {
        // Get already existing entity
        ChanDoanHinhAnh cdha = chiDinhCDHA.getCdha();
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);
        Long cdhaId = cdha.getId();

        // Get all the chiDinhCDHAList where cdha equals to cdhaId
        defaultChiDinhCDHAShouldBeFound("cdhaId.equals=" + cdhaId);

        // Get all the chiDinhCDHAList where cdha equals to cdhaId + 1
        defaultChiDinhCDHAShouldNotBeFound("cdhaId.equals=" + (cdhaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultChiDinhCDHAShouldBeFound(String filter) throws Exception {
        restChiDinhCDHAMockMvc.perform(get("/api/chi-dinh-cdhas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiDinhCDHA.getId().intValue())))
            .andExpect(jsonPath("$.[*].coBaoHiem").value(hasItem(DEFAULT_CO_BAO_HIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].coKetQua").value(hasItem(DEFAULT_CO_KET_QUA.booleanValue())))
            .andExpect(jsonPath("$.[*].daThanhToan").value(hasItem(DEFAULT_DA_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].daThanhToanChenhLech").value(hasItem(DEFAULT_DA_THANH_TOAN_CHENH_LECH)))
            .andExpect(jsonPath("$.[*].daThucHien").value(hasItem(DEFAULT_DA_THUC_HIEN)))
            .andExpect(jsonPath("$.[*].donGia").value(hasItem(DEFAULT_DON_GIA.intValue())))
            .andExpect(jsonPath("$.[*].donGiaBhyt").value(hasItem(DEFAULT_DON_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].donGiaKhongBhyt").value(hasItem(DEFAULT_DON_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].ghiChuChiDinh").value(hasItem(DEFAULT_GHI_CHU_CHI_DINH)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].moTaXm15").value(hasItem(DEFAULT_MO_TA_XM_15)))
            .andExpect(jsonPath("$.[*].nguoiChiDinhId").value(hasItem(DEFAULT_NGUOI_CHI_DINH_ID.intValue())))
            .andExpect(jsonPath("$.[*].nguoiChiDinh").value(hasItem(DEFAULT_NGUOI_CHI_DINH)))
            .andExpect(jsonPath("$.[*].soLanChup").value(hasItem(DEFAULT_SO_LAN_CHUP)))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm").value(hasItem(DEFAULT_SO_LUONG_CO_FILM.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm1318").value(hasItem(DEFAULT_SO_LUONG_CO_FILM_1318.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm1820").value(hasItem(DEFAULT_SO_LUONG_CO_FILM_1820.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm2025").value(hasItem(DEFAULT_SO_LUONG_CO_FILM_2025.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm2430").value(hasItem(DEFAULT_SO_LUONG_CO_FILM_2430.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm2530").value(hasItem(DEFAULT_SO_LUONG_CO_FILM_2530.intValue())))
            .andExpect(jsonPath("$.[*].soLuongCoFilm3040").value(hasItem(DEFAULT_SO_LUONG_CO_FILM_3040.intValue())))
            .andExpect(jsonPath("$.[*].soLuongFilm").value(hasItem(DEFAULT_SO_LUONG_FILM.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].thanhTien").value(hasItem(DEFAULT_THANH_TIEN.intValue())))
            .andExpect(jsonPath("$.[*].thanhTienBhyt").value(hasItem(DEFAULT_THANH_TIEN_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thanhTienKhongBHYT").value(hasItem(DEFAULT_THANH_TIEN_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thoiGianChiDinh").value(hasItem(DEFAULT_THOI_GIAN_CHI_DINH.toString())))
            .andExpect(jsonPath("$.[*].thoiGianTao").value(hasItem(DEFAULT_THOI_GIAN_TAO.toString())))
            .andExpect(jsonPath("$.[*].tienNgoaiBHYT").value(hasItem(DEFAULT_TIEN_NGOAI_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].thanhToanChenhLech").value(hasItem(DEFAULT_THANH_TOAN_CHENH_LECH.booleanValue())))
            .andExpect(jsonPath("$.[*].tyLeThanhToan").value(hasItem(DEFAULT_TY_LE_THANH_TOAN)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)))
            .andExpect(jsonPath("$.[*].dichVuYeuCau").value(hasItem(DEFAULT_DICH_VU_YEU_CAU.booleanValue())))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));

        // Check, that the count call also returns 1
        restChiDinhCDHAMockMvc.perform(get("/api/chi-dinh-cdhas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultChiDinhCDHAShouldNotBeFound(String filter) throws Exception {
        restChiDinhCDHAMockMvc.perform(get("/api/chi-dinh-cdhas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restChiDinhCDHAMockMvc.perform(get("/api/chi-dinh-cdhas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingChiDinhCDHA() throws Exception {
        // Get the chiDinhCDHA
        restChiDinhCDHAMockMvc.perform(get("/api/chi-dinh-cdhas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChiDinhCDHA() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        int databaseSizeBeforeUpdate = chiDinhCDHARepository.findAll().size();

        // Update the chiDinhCDHA
        ChiDinhCDHA updatedChiDinhCDHA = chiDinhCDHARepository.findById(chiDinhCDHA.getId()).get();
        // Disconnect from session so that the updates on updatedChiDinhCDHA are not directly saved in db
        em.detach(updatedChiDinhCDHA);
        updatedChiDinhCDHA
            .coBaoHiem(UPDATED_CO_BAO_HIEM)
            .coKetQua(UPDATED_CO_KET_QUA)
            .daThanhToan(UPDATED_DA_THANH_TOAN)
            .daThanhToanChenhLech(UPDATED_DA_THANH_TOAN_CHENH_LECH)
            .daThucHien(UPDATED_DA_THUC_HIEN)
            .donGia(UPDATED_DON_GIA)
            .donGiaBhyt(UPDATED_DON_GIA_BHYT)
            .donGiaKhongBhyt(UPDATED_DON_GIA_KHONG_BHYT)
            .ghiChuChiDinh(UPDATED_GHI_CHU_CHI_DINH)
            .moTa(UPDATED_MO_TA)
            .moTaXm15(UPDATED_MO_TA_XM_15)
            .nguoiChiDinhId(UPDATED_NGUOI_CHI_DINH_ID)
            .nguoiChiDinh(UPDATED_NGUOI_CHI_DINH)
            .soLanChup(UPDATED_SO_LAN_CHUP)
            .soLuong(UPDATED_SO_LUONG)
            .soLuongCoFilm(UPDATED_SO_LUONG_CO_FILM)
            .soLuongCoFilm1318(UPDATED_SO_LUONG_CO_FILM_1318)
            .soLuongCoFilm1820(UPDATED_SO_LUONG_CO_FILM_1820)
            .soLuongCoFilm2025(UPDATED_SO_LUONG_CO_FILM_2025)
            .soLuongCoFilm2430(UPDATED_SO_LUONG_CO_FILM_2430)
            .soLuongCoFilm2530(UPDATED_SO_LUONG_CO_FILM_2530)
            .soLuongCoFilm3040(UPDATED_SO_LUONG_CO_FILM_3040)
            .soLuongFilm(UPDATED_SO_LUONG_FILM)
            .ten(UPDATED_TEN)
            .thanhTien(UPDATED_THANH_TIEN)
            .thanhTienBhyt(UPDATED_THANH_TIEN_BHYT)
            .thanhTienKhongBHYT(UPDATED_THANH_TIEN_KHONG_BHYT)
            .thoiGianChiDinh(UPDATED_THOI_GIAN_CHI_DINH)
            .thoiGianTao(UPDATED_THOI_GIAN_TAO)
            .tienNgoaiBHYT(UPDATED_TIEN_NGOAI_BHYT)
            .thanhToanChenhLech(UPDATED_THANH_TOAN_CHENH_LECH)
            .tyLeThanhToan(UPDATED_TY_LE_THANH_TOAN)
            .maDungChung(UPDATED_MA_DUNG_CHUNG)
            .dichVuYeuCau(UPDATED_DICH_VU_YEU_CAU)
            .nam(UPDATED_NAM);
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(updatedChiDinhCDHA);

        restChiDinhCDHAMockMvc.perform(put("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isOk());

        // Validate the ChiDinhCDHA in the database
        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeUpdate);
        ChiDinhCDHA testChiDinhCDHA = chiDinhCDHAList.get(chiDinhCDHAList.size() - 1);
        assertThat(testChiDinhCDHA.isCoBaoHiem()).isEqualTo(UPDATED_CO_BAO_HIEM);
        assertThat(testChiDinhCDHA.isCoKetQua()).isEqualTo(UPDATED_CO_KET_QUA);
        assertThat(testChiDinhCDHA.getDaThanhToan()).isEqualTo(UPDATED_DA_THANH_TOAN);
        assertThat(testChiDinhCDHA.getDaThanhToanChenhLech()).isEqualTo(UPDATED_DA_THANH_TOAN_CHENH_LECH);
        assertThat(testChiDinhCDHA.getDaThucHien()).isEqualTo(UPDATED_DA_THUC_HIEN);
        assertThat(testChiDinhCDHA.getDonGia()).isEqualTo(UPDATED_DON_GIA);
        assertThat(testChiDinhCDHA.getDonGiaBhyt()).isEqualTo(UPDATED_DON_GIA_BHYT);
        assertThat(testChiDinhCDHA.getDonGiaKhongBhyt()).isEqualTo(UPDATED_DON_GIA_KHONG_BHYT);
        assertThat(testChiDinhCDHA.getGhiChuChiDinh()).isEqualTo(UPDATED_GHI_CHU_CHI_DINH);
        assertThat(testChiDinhCDHA.getMoTa()).isEqualTo(UPDATED_MO_TA);
        assertThat(testChiDinhCDHA.getMoTaXm15()).isEqualTo(UPDATED_MO_TA_XM_15);
        assertThat(testChiDinhCDHA.getNguoiChiDinhId()).isEqualTo(UPDATED_NGUOI_CHI_DINH_ID);
        assertThat(testChiDinhCDHA.getNguoiChiDinh()).isEqualTo(UPDATED_NGUOI_CHI_DINH);
        assertThat(testChiDinhCDHA.getSoLanChup()).isEqualTo(UPDATED_SO_LAN_CHUP);
        assertThat(testChiDinhCDHA.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm()).isEqualTo(UPDATED_SO_LUONG_CO_FILM);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm1318()).isEqualTo(UPDATED_SO_LUONG_CO_FILM_1318);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm1820()).isEqualTo(UPDATED_SO_LUONG_CO_FILM_1820);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm2025()).isEqualTo(UPDATED_SO_LUONG_CO_FILM_2025);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm2430()).isEqualTo(UPDATED_SO_LUONG_CO_FILM_2430);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm2530()).isEqualTo(UPDATED_SO_LUONG_CO_FILM_2530);
        assertThat(testChiDinhCDHA.getSoLuongCoFilm3040()).isEqualTo(UPDATED_SO_LUONG_CO_FILM_3040);
        assertThat(testChiDinhCDHA.getSoLuongFilm()).isEqualTo(UPDATED_SO_LUONG_FILM);
        assertThat(testChiDinhCDHA.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testChiDinhCDHA.getThanhTien()).isEqualTo(UPDATED_THANH_TIEN);
        assertThat(testChiDinhCDHA.getThanhTienBhyt()).isEqualTo(UPDATED_THANH_TIEN_BHYT);
        assertThat(testChiDinhCDHA.getThanhTienKhongBHYT()).isEqualTo(UPDATED_THANH_TIEN_KHONG_BHYT);
        assertThat(testChiDinhCDHA.getThoiGianChiDinh()).isEqualTo(UPDATED_THOI_GIAN_CHI_DINH);
        assertThat(testChiDinhCDHA.getThoiGianTao()).isEqualTo(UPDATED_THOI_GIAN_TAO);
        assertThat(testChiDinhCDHA.getTienNgoaiBHYT()).isEqualTo(UPDATED_TIEN_NGOAI_BHYT);
        assertThat(testChiDinhCDHA.isThanhToanChenhLech()).isEqualTo(UPDATED_THANH_TOAN_CHENH_LECH);
        assertThat(testChiDinhCDHA.getTyLeThanhToan()).isEqualTo(UPDATED_TY_LE_THANH_TOAN);
        assertThat(testChiDinhCDHA.getMaDungChung()).isEqualTo(UPDATED_MA_DUNG_CHUNG);
        assertThat(testChiDinhCDHA.isDichVuYeuCau()).isEqualTo(UPDATED_DICH_VU_YEU_CAU);
        assertThat(testChiDinhCDHA.getNam()).isEqualTo(UPDATED_NAM);
    }

    @Test
    @Transactional
    public void updateNonExistingChiDinhCDHA() throws Exception {
        int databaseSizeBeforeUpdate = chiDinhCDHARepository.findAll().size();

        // Create the ChiDinhCDHA
        ChiDinhCDHADTO chiDinhCDHADTO = chiDinhCDHAMapper.toDto(chiDinhCDHA);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChiDinhCDHAMockMvc.perform(put("/api/chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChiDinhCDHA in the database
        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChiDinhCDHA() throws Exception {
        // Initialize the database
        chiDinhCDHARepository.saveAndFlush(chiDinhCDHA);

        int databaseSizeBeforeDelete = chiDinhCDHARepository.findAll().size();

        // Delete the chiDinhCDHA
        restChiDinhCDHAMockMvc.perform(delete("/api/chi-dinh-cdhas/{id}", chiDinhCDHA.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChiDinhCDHA> chiDinhCDHAList = chiDinhCDHARepository.findAll();
        assertThat(chiDinhCDHAList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
