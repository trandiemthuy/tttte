package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.BenhYhct;
import vn.vnpt.domain.BenhLy;
import vn.vnpt.repository.BenhYhctRepository;
import vn.vnpt.service.BenhYhctService;
import vn.vnpt.service.dto.BenhYhctDTO;
import vn.vnpt.service.mapper.BenhYhctMapper;
import vn.vnpt.service.dto.BenhYhctCriteria;
import vn.vnpt.service.BenhYhctQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BenhYhctResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class BenhYhctResourceIT {

    private static final String DEFAULT_MA = "AAAAAAAAAA";
    private static final String UPDATED_MA = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    @Autowired
    private BenhYhctRepository benhYhctRepository;

    @Autowired
    private BenhYhctMapper benhYhctMapper;

    @Autowired
    private BenhYhctService benhYhctService;

    @Autowired
    private BenhYhctQueryService benhYhctQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBenhYhctMockMvc;

    private BenhYhct benhYhct;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BenhYhct createEntity(EntityManager em) {
        BenhYhct benhYhct = new BenhYhct()
            .ma(DEFAULT_MA)
            .ten(DEFAULT_TEN);
        return benhYhct;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BenhYhct createUpdatedEntity(EntityManager em) {
        BenhYhct benhYhct = new BenhYhct()
            .ma(UPDATED_MA)
            .ten(UPDATED_TEN);
        return benhYhct;
    }

    @BeforeEach
    public void initTest() {
        benhYhct = createEntity(em);
    }

    @Test
    @Transactional
    public void createBenhYhct() throws Exception {
        int databaseSizeBeforeCreate = benhYhctRepository.findAll().size();

        // Create the BenhYhct
        BenhYhctDTO benhYhctDTO = benhYhctMapper.toDto(benhYhct);
        restBenhYhctMockMvc.perform(post("/api/benh-yhcts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhYhctDTO)))
            .andExpect(status().isCreated());

        // Validate the BenhYhct in the database
        List<BenhYhct> benhYhctList = benhYhctRepository.findAll();
        assertThat(benhYhctList).hasSize(databaseSizeBeforeCreate + 1);
        BenhYhct testBenhYhct = benhYhctList.get(benhYhctList.size() - 1);
        assertThat(testBenhYhct.getMa()).isEqualTo(DEFAULT_MA);
        assertThat(testBenhYhct.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    public void createBenhYhctWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = benhYhctRepository.findAll().size();

        // Create the BenhYhct with an existing ID
        benhYhct.setId(1L);
        BenhYhctDTO benhYhctDTO = benhYhctMapper.toDto(benhYhct);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBenhYhctMockMvc.perform(post("/api/benh-yhcts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhYhctDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BenhYhct in the database
        List<BenhYhct> benhYhctList = benhYhctRepository.findAll();
        assertThat(benhYhctList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMaIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhYhctRepository.findAll().size();
        // set the field null
        benhYhct.setMa(null);

        // Create the BenhYhct, which fails.
        BenhYhctDTO benhYhctDTO = benhYhctMapper.toDto(benhYhct);

        restBenhYhctMockMvc.perform(post("/api/benh-yhcts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhYhctDTO)))
            .andExpect(status().isBadRequest());

        List<BenhYhct> benhYhctList = benhYhctRepository.findAll();
        assertThat(benhYhctList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhYhctRepository.findAll().size();
        // set the field null
        benhYhct.setTen(null);

        // Create the BenhYhct, which fails.
        BenhYhctDTO benhYhctDTO = benhYhctMapper.toDto(benhYhct);

        restBenhYhctMockMvc.perform(post("/api/benh-yhcts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhYhctDTO)))
            .andExpect(status().isBadRequest());

        List<BenhYhct> benhYhctList = benhYhctRepository.findAll();
        assertThat(benhYhctList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBenhYhcts() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get all the benhYhctList
        restBenhYhctMockMvc.perform(get("/api/benh-yhcts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(benhYhct.getId().intValue())))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }
    
    @Test
    @Transactional
    public void getBenhYhct() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get the benhYhct
        restBenhYhctMockMvc.perform(get("/api/benh-yhcts/{id}", benhYhct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(benhYhct.getId().intValue()))
            .andExpect(jsonPath("$.ma").value(DEFAULT_MA))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }


    @Test
    @Transactional
    public void getBenhYhctsByIdFiltering() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        Long id = benhYhct.getId();

        defaultBenhYhctShouldBeFound("id.equals=" + id);
        defaultBenhYhctShouldNotBeFound("id.notEquals=" + id);

        defaultBenhYhctShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultBenhYhctShouldNotBeFound("id.greaterThan=" + id);

        defaultBenhYhctShouldBeFound("id.lessThanOrEqual=" + id);
        defaultBenhYhctShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllBenhYhctsByMaIsEqualToSomething() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get all the benhYhctList where ma equals to DEFAULT_MA
        defaultBenhYhctShouldBeFound("ma.equals=" + DEFAULT_MA);

        // Get all the benhYhctList where ma equals to UPDATED_MA
        defaultBenhYhctShouldNotBeFound("ma.equals=" + UPDATED_MA);
    }

    @Test
    @Transactional
    public void getAllBenhYhctsByMaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get all the benhYhctList where ma not equals to DEFAULT_MA
        defaultBenhYhctShouldNotBeFound("ma.notEquals=" + DEFAULT_MA);

        // Get all the benhYhctList where ma not equals to UPDATED_MA
        defaultBenhYhctShouldBeFound("ma.notEquals=" + UPDATED_MA);
    }

    @Test
    @Transactional
    public void getAllBenhYhctsByMaIsInShouldWork() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get all the benhYhctList where ma in DEFAULT_MA or UPDATED_MA
        defaultBenhYhctShouldBeFound("ma.in=" + DEFAULT_MA + "," + UPDATED_MA);

        // Get all the benhYhctList where ma equals to UPDATED_MA
        defaultBenhYhctShouldNotBeFound("ma.in=" + UPDATED_MA);
    }

    @Test
    @Transactional
    public void getAllBenhYhctsByMaIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get all the benhYhctList where ma is not null
        defaultBenhYhctShouldBeFound("ma.specified=true");

        // Get all the benhYhctList where ma is null
        defaultBenhYhctShouldNotBeFound("ma.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhYhctsByMaContainsSomething() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get all the benhYhctList where ma contains DEFAULT_MA
        defaultBenhYhctShouldBeFound("ma.contains=" + DEFAULT_MA);

        // Get all the benhYhctList where ma contains UPDATED_MA
        defaultBenhYhctShouldNotBeFound("ma.contains=" + UPDATED_MA);
    }

    @Test
    @Transactional
    public void getAllBenhYhctsByMaNotContainsSomething() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get all the benhYhctList where ma does not contain DEFAULT_MA
        defaultBenhYhctShouldNotBeFound("ma.doesNotContain=" + DEFAULT_MA);

        // Get all the benhYhctList where ma does not contain UPDATED_MA
        defaultBenhYhctShouldBeFound("ma.doesNotContain=" + UPDATED_MA);
    }


    @Test
    @Transactional
    public void getAllBenhYhctsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get all the benhYhctList where ten equals to DEFAULT_TEN
        defaultBenhYhctShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the benhYhctList where ten equals to UPDATED_TEN
        defaultBenhYhctShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllBenhYhctsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get all the benhYhctList where ten not equals to DEFAULT_TEN
        defaultBenhYhctShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the benhYhctList where ten not equals to UPDATED_TEN
        defaultBenhYhctShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllBenhYhctsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get all the benhYhctList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultBenhYhctShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the benhYhctList where ten equals to UPDATED_TEN
        defaultBenhYhctShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllBenhYhctsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get all the benhYhctList where ten is not null
        defaultBenhYhctShouldBeFound("ten.specified=true");

        // Get all the benhYhctList where ten is null
        defaultBenhYhctShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhYhctsByTenContainsSomething() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get all the benhYhctList where ten contains DEFAULT_TEN
        defaultBenhYhctShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the benhYhctList where ten contains UPDATED_TEN
        defaultBenhYhctShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllBenhYhctsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        // Get all the benhYhctList where ten does not contain DEFAULT_TEN
        defaultBenhYhctShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the benhYhctList where ten does not contain UPDATED_TEN
        defaultBenhYhctShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllBenhYhctsByBenhLyIsEqualToSomething() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);
        BenhLy benhLy = BenhLyResourceIT.createEntity(em);
        em.persist(benhLy);
        em.flush();
        benhYhct.setBenhLy(benhLy);
        benhYhctRepository.saveAndFlush(benhYhct);
        Long benhLyId = benhLy.getId();

        // Get all the benhYhctList where benhLy equals to benhLyId
        defaultBenhYhctShouldBeFound("benhLyId.equals=" + benhLyId);

        // Get all the benhYhctList where benhLy equals to benhLyId + 1
        defaultBenhYhctShouldNotBeFound("benhLyId.equals=" + (benhLyId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBenhYhctShouldBeFound(String filter) throws Exception {
        restBenhYhctMockMvc.perform(get("/api/benh-yhcts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(benhYhct.getId().intValue())))
            .andExpect(jsonPath("$.[*].ma").value(hasItem(DEFAULT_MA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restBenhYhctMockMvc.perform(get("/api/benh-yhcts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBenhYhctShouldNotBeFound(String filter) throws Exception {
        restBenhYhctMockMvc.perform(get("/api/benh-yhcts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBenhYhctMockMvc.perform(get("/api/benh-yhcts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingBenhYhct() throws Exception {
        // Get the benhYhct
        restBenhYhctMockMvc.perform(get("/api/benh-yhcts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBenhYhct() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        int databaseSizeBeforeUpdate = benhYhctRepository.findAll().size();

        // Update the benhYhct
        BenhYhct updatedBenhYhct = benhYhctRepository.findById(benhYhct.getId()).get();
        // Disconnect from session so that the updates on updatedBenhYhct are not directly saved in db
        em.detach(updatedBenhYhct);
        updatedBenhYhct
            .ma(UPDATED_MA)
            .ten(UPDATED_TEN);
        BenhYhctDTO benhYhctDTO = benhYhctMapper.toDto(updatedBenhYhct);

        restBenhYhctMockMvc.perform(put("/api/benh-yhcts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhYhctDTO)))
            .andExpect(status().isOk());

        // Validate the BenhYhct in the database
        List<BenhYhct> benhYhctList = benhYhctRepository.findAll();
        assertThat(benhYhctList).hasSize(databaseSizeBeforeUpdate);
        BenhYhct testBenhYhct = benhYhctList.get(benhYhctList.size() - 1);
        assertThat(testBenhYhct.getMa()).isEqualTo(UPDATED_MA);
        assertThat(testBenhYhct.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    public void updateNonExistingBenhYhct() throws Exception {
        int databaseSizeBeforeUpdate = benhYhctRepository.findAll().size();

        // Create the BenhYhct
        BenhYhctDTO benhYhctDTO = benhYhctMapper.toDto(benhYhct);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBenhYhctMockMvc.perform(put("/api/benh-yhcts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhYhctDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BenhYhct in the database
        List<BenhYhct> benhYhctList = benhYhctRepository.findAll();
        assertThat(benhYhctList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBenhYhct() throws Exception {
        // Initialize the database
        benhYhctRepository.saveAndFlush(benhYhct);

        int databaseSizeBeforeDelete = benhYhctRepository.findAll().size();

        // Delete the benhYhct
        restBenhYhctMockMvc.perform(delete("/api/benh-yhcts/{id}", benhYhct.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BenhYhct> benhYhctList = benhYhctRepository.findAll();
        assertThat(benhYhctList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
