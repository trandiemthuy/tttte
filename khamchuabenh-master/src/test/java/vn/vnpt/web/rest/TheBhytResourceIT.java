package vn.vnpt.web.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.BenhNhan;
import vn.vnpt.domain.DoiTuongBhyt;
import vn.vnpt.domain.TheBhyt;
import vn.vnpt.domain.ThongTinBhxh;
import vn.vnpt.repository.TheBhytRepository;
import vn.vnpt.service.TheBhytQueryService;
import vn.vnpt.service.TheBhytService;
import vn.vnpt.service.dto.TheBhytDTO;
import vn.vnpt.service.mapper.TheBhytMapper;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TheBhytResource} REST controller.
 */
@SpringBootTest(classes = {KhamchuabenhApp.class, TestSecurityConfiguration.class})

@AutoConfigureMockMvc
@WithMockUser
public class TheBhytResourceIT {

    private static final Boolean DEFAULT_ENABLED = false;
    private static final Boolean UPDATED_ENABLED = true;

    private static final String DEFAULT_MA_KHU_VUC = "AAAAAAAAAA";
    private static final String UPDATED_MA_KHU_VUC = "BBBBBBBBBB";

    private static final String DEFAULT_MA_SO_BHXH = "AAAAAAAAAA";
    private static final String UPDATED_MA_SO_BHXH = "BBBBBBBBBB";

    private static final String DEFAULT_MA_THE_CU = "AAAAAAAAAA";
    private static final String UPDATED_MA_THE_CU = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_BAT_DAU = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_BAT_DAU = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_BAT_DAU = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_NGAY_DU_NAM_NAM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_DU_NAM_NAM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_DU_NAM_NAM = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_NGAY_HET_HAN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_HET_HAN = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_HET_HAN = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_QR = "AAAAAAAAAA";
    private static final String UPDATED_QR = "BBBBBBBBBB";

    private static final String DEFAULT_SO_THE = "AAAAAAAAAA";
    private static final String UPDATED_SO_THE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_MIEN_CUNG_CHI_TRA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_MIEN_CUNG_CHI_TRA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_MIEN_CUNG_CHI_TRA = LocalDate.ofEpochDay(-1L);

    @Autowired
    private TheBhytRepository theBhytRepository;

    @Autowired
    private TheBhytMapper theBhytMapper;

    @Autowired
    private TheBhytService theBhytService;

    @Autowired
    private TheBhytQueryService theBhytQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTheBhytMockMvc;

    private TheBhyt theBhyt;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TheBhyt createEntity(EntityManager em) {
        TheBhyt theBhyt = new TheBhyt()
            .enabled(DEFAULT_ENABLED)
            .maKhuVuc(DEFAULT_MA_KHU_VUC)
            .maSoBhxh(DEFAULT_MA_SO_BHXH)
            .maTheCu(DEFAULT_MA_THE_CU)
            .ngayBatDau(DEFAULT_NGAY_BAT_DAU)
            .ngayDuNamNam(DEFAULT_NGAY_DU_NAM_NAM)
            .ngayHetHan(DEFAULT_NGAY_HET_HAN)
            .qr(DEFAULT_QR)
            .soThe(DEFAULT_SO_THE)
            .ngayMienCungChiTra(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);
        // Add required entity
        DoiTuongBhyt doiTuongBhyt;
        if (TestUtil.findAll(em, DoiTuongBhyt.class).isEmpty()) {
            doiTuongBhyt = DoiTuongBhytResourceIT.createEntity(em);
            em.persist(doiTuongBhyt);
            em.flush();
        } else {
            doiTuongBhyt = TestUtil.findAll(em, DoiTuongBhyt.class).get(0);
        }
        theBhyt.setDoiTuongBhyt(doiTuongBhyt);
        // Add required entity
        ThongTinBhxh thongTinBhxh;
        if (TestUtil.findAll(em, ThongTinBhxh.class).isEmpty()) {
            thongTinBhxh = ThongTinBhxhResourceIT.createEntity(em);
            em.persist(thongTinBhxh);
            em.flush();
        } else {
            thongTinBhxh = TestUtil.findAll(em, ThongTinBhxh.class).get(0);
        }
        theBhyt.setThongTinBhxh(thongTinBhxh);
        return theBhyt;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TheBhyt createUpdatedEntity(EntityManager em) {
        TheBhyt theBhyt = new TheBhyt()
            .enabled(UPDATED_ENABLED)
            .maKhuVuc(UPDATED_MA_KHU_VUC)
            .maSoBhxh(UPDATED_MA_SO_BHXH)
            .maTheCu(UPDATED_MA_THE_CU)
            .ngayBatDau(UPDATED_NGAY_BAT_DAU)
            .ngayDuNamNam(UPDATED_NGAY_DU_NAM_NAM)
            .ngayHetHan(UPDATED_NGAY_HET_HAN)
            .qr(UPDATED_QR)
            .soThe(UPDATED_SO_THE)
            .ngayMienCungChiTra(UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
        // Add required entity
        DoiTuongBhyt doiTuongBhyt;
        if (TestUtil.findAll(em, DoiTuongBhyt.class).isEmpty()) {
            doiTuongBhyt = DoiTuongBhytResourceIT.createUpdatedEntity(em);
            em.persist(doiTuongBhyt);
            em.flush();
        } else {
            doiTuongBhyt = TestUtil.findAll(em, DoiTuongBhyt.class).get(0);
        }
        theBhyt.setDoiTuongBhyt(doiTuongBhyt);
        // Add required entity
        ThongTinBhxh thongTinBhxh;
        if (TestUtil.findAll(em, ThongTinBhxh.class).isEmpty()) {
            thongTinBhxh = ThongTinBhxhResourceIT.createUpdatedEntity(em);
            em.persist(thongTinBhxh);
            em.flush();
        } else {
            thongTinBhxh = TestUtil.findAll(em, ThongTinBhxh.class).get(0);
        }
        theBhyt.setThongTinBhxh(thongTinBhxh);
        return theBhyt;
    }

    @BeforeEach
    public void initTest() {
        theBhyt = createEntity(em);
    }

    @Test
    @Transactional
    public void createTheBhyt() throws Exception {
        int databaseSizeBeforeCreate = theBhytRepository.findAll().size();

        // Create the TheBhyt
        TheBhytDTO theBhytDTO = theBhytMapper.toDto(theBhyt);
        restTheBhytMockMvc.perform(post("/api/the-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(theBhytDTO)))
            .andExpect(status().isCreated());

        // Validate the TheBhyt in the database
        List<TheBhyt> theBhytList = theBhytRepository.findAll();
        assertThat(theBhytList).hasSize(databaseSizeBeforeCreate + 1);
        TheBhyt testTheBhyt = theBhytList.get(theBhytList.size() - 1);
        assertThat(testTheBhyt.isEnabled()).isEqualTo(DEFAULT_ENABLED);
        assertThat(testTheBhyt.getMaKhuVuc()).isEqualTo(DEFAULT_MA_KHU_VUC);
        assertThat(testTheBhyt.getMaSoBhxh()).isEqualTo(DEFAULT_MA_SO_BHXH);
        assertThat(testTheBhyt.getMaTheCu()).isEqualTo(DEFAULT_MA_THE_CU);
        assertThat(testTheBhyt.getNgayBatDau()).isEqualTo(DEFAULT_NGAY_BAT_DAU);
        assertThat(testTheBhyt.getNgayDuNamNam()).isEqualTo(DEFAULT_NGAY_DU_NAM_NAM);
        assertThat(testTheBhyt.getNgayHetHan()).isEqualTo(DEFAULT_NGAY_HET_HAN);
        assertThat(testTheBhyt.getQr()).isEqualTo(DEFAULT_QR);
        assertThat(testTheBhyt.getSoThe()).isEqualTo(DEFAULT_SO_THE);
        assertThat(testTheBhyt.getNgayMienCungChiTra()).isEqualTo(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void createTheBhytWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = theBhytRepository.findAll().size();

        // Create the TheBhyt with an existing ID
        theBhyt.setId(1L);
        TheBhytDTO theBhytDTO = theBhytMapper.toDto(theBhyt);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTheBhytMockMvc.perform(post("/api/the-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(theBhytDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TheBhyt in the database
        List<TheBhyt> theBhytList = theBhytRepository.findAll();
        assertThat(theBhytList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = theBhytRepository.findAll().size();
        // set the field null
        theBhyt.setEnabled(null);

        // Create the TheBhyt, which fails.
        TheBhytDTO theBhytDTO = theBhytMapper.toDto(theBhyt);

        restTheBhytMockMvc.perform(post("/api/the-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(theBhytDTO)))
            .andExpect(status().isBadRequest());

        List<TheBhyt> theBhytList = theBhytRepository.findAll();
        assertThat(theBhytList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNgayBatDauIsRequired() throws Exception {
        int databaseSizeBeforeTest = theBhytRepository.findAll().size();
        // set the field null
        theBhyt.setNgayBatDau(null);

        // Create the TheBhyt, which fails.
        TheBhytDTO theBhytDTO = theBhytMapper.toDto(theBhyt);

        restTheBhytMockMvc.perform(post("/api/the-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(theBhytDTO)))
            .andExpect(status().isBadRequest());

        List<TheBhyt> theBhytList = theBhytRepository.findAll();
        assertThat(theBhytList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNgayDuNamNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = theBhytRepository.findAll().size();
        // set the field null
        theBhyt.setNgayDuNamNam(null);

        // Create the TheBhyt, which fails.
        TheBhytDTO theBhytDTO = theBhytMapper.toDto(theBhyt);

        restTheBhytMockMvc.perform(post("/api/the-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(theBhytDTO)))
            .andExpect(status().isBadRequest());

        List<TheBhyt> theBhytList = theBhytRepository.findAll();
        assertThat(theBhytList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNgayHetHanIsRequired() throws Exception {
        int databaseSizeBeforeTest = theBhytRepository.findAll().size();
        // set the field null
        theBhyt.setNgayHetHan(null);

        // Create the TheBhyt, which fails.
        TheBhytDTO theBhytDTO = theBhytMapper.toDto(theBhyt);

        restTheBhytMockMvc.perform(post("/api/the-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(theBhytDTO)))
            .andExpect(status().isBadRequest());

        List<TheBhyt> theBhytList = theBhytRepository.findAll();
        assertThat(theBhytList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSoTheIsRequired() throws Exception {
        int databaseSizeBeforeTest = theBhytRepository.findAll().size();
        // set the field null
        theBhyt.setSoThe(null);

        // Create the TheBhyt, which fails.
        TheBhytDTO theBhytDTO = theBhytMapper.toDto(theBhyt);

        restTheBhytMockMvc.perform(post("/api/the-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(theBhytDTO)))
            .andExpect(status().isBadRequest());

        List<TheBhyt> theBhytList = theBhytRepository.findAll();
        assertThat(theBhytList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTheBhyts() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList
        restTheBhytMockMvc.perform(get("/api/the-bhyts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(theBhyt.getId().intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].maKhuVuc").value(hasItem(DEFAULT_MA_KHU_VUC)))
            .andExpect(jsonPath("$.[*].maSoBhxh").value(hasItem(DEFAULT_MA_SO_BHXH)))
            .andExpect(jsonPath("$.[*].maTheCu").value(hasItem(DEFAULT_MA_THE_CU)))
            .andExpect(jsonPath("$.[*].ngayBatDau").value(hasItem(DEFAULT_NGAY_BAT_DAU.toString())))
            .andExpect(jsonPath("$.[*].ngayDuNamNam").value(hasItem(DEFAULT_NGAY_DU_NAM_NAM.toString())))
            .andExpect(jsonPath("$.[*].ngayHetHan").value(hasItem(DEFAULT_NGAY_HET_HAN.toString())))
            .andExpect(jsonPath("$.[*].qr").value(hasItem(DEFAULT_QR)))
            .andExpect(jsonPath("$.[*].soThe").value(hasItem(DEFAULT_SO_THE)))
            .andExpect(jsonPath("$.[*].ngayMienCungChiTra").value(hasItem(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA.toString())));
    }

    @Test
    @Transactional
    public void getTheBhyt() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get the theBhyt
        restTheBhytMockMvc.perform(get("/api/the-bhyts/{id}", theBhyt.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(theBhyt.getId().intValue()))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()))
            .andExpect(jsonPath("$.maKhuVuc").value(DEFAULT_MA_KHU_VUC))
            .andExpect(jsonPath("$.maSoBhxh").value(DEFAULT_MA_SO_BHXH))
            .andExpect(jsonPath("$.maTheCu").value(DEFAULT_MA_THE_CU))
            .andExpect(jsonPath("$.ngayBatDau").value(DEFAULT_NGAY_BAT_DAU.toString()))
            .andExpect(jsonPath("$.ngayDuNamNam").value(DEFAULT_NGAY_DU_NAM_NAM.toString()))
            .andExpect(jsonPath("$.ngayHetHan").value(DEFAULT_NGAY_HET_HAN.toString()))
            .andExpect(jsonPath("$.qr").value(DEFAULT_QR))
            .andExpect(jsonPath("$.soThe").value(DEFAULT_SO_THE))
            .andExpect(jsonPath("$.ngayMienCungChiTra").value(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA.toString()));
    }


    @Test
    @Transactional
    public void getTheBhytsByIdFiltering() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        Long id = theBhyt.getId();

        defaultTheBhytShouldBeFound("id.equals=" + id);
        defaultTheBhytShouldNotBeFound("id.notEquals=" + id);

        defaultTheBhytShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTheBhytShouldNotBeFound("id.greaterThan=" + id);

        defaultTheBhytShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTheBhytShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTheBhytsByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where enabled equals to DEFAULT_ENABLED
        defaultTheBhytShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the theBhytList where enabled equals to UPDATED_ENABLED
        defaultTheBhytShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where enabled not equals to DEFAULT_ENABLED
        defaultTheBhytShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the theBhytList where enabled not equals to UPDATED_ENABLED
        defaultTheBhytShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultTheBhytShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the theBhytList where enabled equals to UPDATED_ENABLED
        defaultTheBhytShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where enabled is not null
        defaultTheBhytShouldBeFound("enabled.specified=true");

        // Get all the theBhytList where enabled is null
        defaultTheBhytShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllTheBhytsByMaKhuVucIsEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maKhuVuc equals to DEFAULT_MA_KHU_VUC
        defaultTheBhytShouldBeFound("maKhuVuc.equals=" + DEFAULT_MA_KHU_VUC);

        // Get all the theBhytList where maKhuVuc equals to UPDATED_MA_KHU_VUC
        defaultTheBhytShouldNotBeFound("maKhuVuc.equals=" + UPDATED_MA_KHU_VUC);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByMaKhuVucIsNotEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maKhuVuc not equals to DEFAULT_MA_KHU_VUC
        defaultTheBhytShouldNotBeFound("maKhuVuc.notEquals=" + DEFAULT_MA_KHU_VUC);

        // Get all the theBhytList where maKhuVuc not equals to UPDATED_MA_KHU_VUC
        defaultTheBhytShouldBeFound("maKhuVuc.notEquals=" + UPDATED_MA_KHU_VUC);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByMaKhuVucIsInShouldWork() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maKhuVuc in DEFAULT_MA_KHU_VUC or UPDATED_MA_KHU_VUC
        defaultTheBhytShouldBeFound("maKhuVuc.in=" + DEFAULT_MA_KHU_VUC + "," + UPDATED_MA_KHU_VUC);

        // Get all the theBhytList where maKhuVuc equals to UPDATED_MA_KHU_VUC
        defaultTheBhytShouldNotBeFound("maKhuVuc.in=" + UPDATED_MA_KHU_VUC);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByMaKhuVucIsNullOrNotNull() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maKhuVuc is not null
        defaultTheBhytShouldBeFound("maKhuVuc.specified=true");

        // Get all the theBhytList where maKhuVuc is null
        defaultTheBhytShouldNotBeFound("maKhuVuc.specified=false");
    }
                @Test
    @Transactional
    public void getAllTheBhytsByMaKhuVucContainsSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maKhuVuc contains DEFAULT_MA_KHU_VUC
        defaultTheBhytShouldBeFound("maKhuVuc.contains=" + DEFAULT_MA_KHU_VUC);

        // Get all the theBhytList where maKhuVuc contains UPDATED_MA_KHU_VUC
        defaultTheBhytShouldNotBeFound("maKhuVuc.contains=" + UPDATED_MA_KHU_VUC);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByMaKhuVucNotContainsSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maKhuVuc does not contain DEFAULT_MA_KHU_VUC
        defaultTheBhytShouldNotBeFound("maKhuVuc.doesNotContain=" + DEFAULT_MA_KHU_VUC);

        // Get all the theBhytList where maKhuVuc does not contain UPDATED_MA_KHU_VUC
        defaultTheBhytShouldBeFound("maKhuVuc.doesNotContain=" + UPDATED_MA_KHU_VUC);
    }


    @Test
    @Transactional
    public void getAllTheBhytsByMaSoBhxhIsEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maSoBhxh equals to DEFAULT_MA_SO_BHXH
        defaultTheBhytShouldBeFound("maSoBhxh.equals=" + DEFAULT_MA_SO_BHXH);

        // Get all the theBhytList where maSoBhxh equals to UPDATED_MA_SO_BHXH
        defaultTheBhytShouldNotBeFound("maSoBhxh.equals=" + UPDATED_MA_SO_BHXH);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByMaSoBhxhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maSoBhxh not equals to DEFAULT_MA_SO_BHXH
        defaultTheBhytShouldNotBeFound("maSoBhxh.notEquals=" + DEFAULT_MA_SO_BHXH);

        // Get all the theBhytList where maSoBhxh not equals to UPDATED_MA_SO_BHXH
        defaultTheBhytShouldBeFound("maSoBhxh.notEquals=" + UPDATED_MA_SO_BHXH);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByMaSoBhxhIsInShouldWork() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maSoBhxh in DEFAULT_MA_SO_BHXH or UPDATED_MA_SO_BHXH
        defaultTheBhytShouldBeFound("maSoBhxh.in=" + DEFAULT_MA_SO_BHXH + "," + UPDATED_MA_SO_BHXH);

        // Get all the theBhytList where maSoBhxh equals to UPDATED_MA_SO_BHXH
        defaultTheBhytShouldNotBeFound("maSoBhxh.in=" + UPDATED_MA_SO_BHXH);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByMaSoBhxhIsNullOrNotNull() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maSoBhxh is not null
        defaultTheBhytShouldBeFound("maSoBhxh.specified=true");

        // Get all the theBhytList where maSoBhxh is null
        defaultTheBhytShouldNotBeFound("maSoBhxh.specified=false");
    }
                @Test
    @Transactional
    public void getAllTheBhytsByMaSoBhxhContainsSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maSoBhxh contains DEFAULT_MA_SO_BHXH
        defaultTheBhytShouldBeFound("maSoBhxh.contains=" + DEFAULT_MA_SO_BHXH);

        // Get all the theBhytList where maSoBhxh contains UPDATED_MA_SO_BHXH
        defaultTheBhytShouldNotBeFound("maSoBhxh.contains=" + UPDATED_MA_SO_BHXH);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByMaSoBhxhNotContainsSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maSoBhxh does not contain DEFAULT_MA_SO_BHXH
        defaultTheBhytShouldNotBeFound("maSoBhxh.doesNotContain=" + DEFAULT_MA_SO_BHXH);

        // Get all the theBhytList where maSoBhxh does not contain UPDATED_MA_SO_BHXH
        defaultTheBhytShouldBeFound("maSoBhxh.doesNotContain=" + UPDATED_MA_SO_BHXH);
    }


    @Test
    @Transactional
    public void getAllTheBhytsByMaTheCuIsEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maTheCu equals to DEFAULT_MA_THE_CU
        defaultTheBhytShouldBeFound("maTheCu.equals=" + DEFAULT_MA_THE_CU);

        // Get all the theBhytList where maTheCu equals to UPDATED_MA_THE_CU
        defaultTheBhytShouldNotBeFound("maTheCu.equals=" + UPDATED_MA_THE_CU);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByMaTheCuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maTheCu not equals to DEFAULT_MA_THE_CU
        defaultTheBhytShouldNotBeFound("maTheCu.notEquals=" + DEFAULT_MA_THE_CU);

        // Get all the theBhytList where maTheCu not equals to UPDATED_MA_THE_CU
        defaultTheBhytShouldBeFound("maTheCu.notEquals=" + UPDATED_MA_THE_CU);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByMaTheCuIsInShouldWork() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maTheCu in DEFAULT_MA_THE_CU or UPDATED_MA_THE_CU
        defaultTheBhytShouldBeFound("maTheCu.in=" + DEFAULT_MA_THE_CU + "," + UPDATED_MA_THE_CU);

        // Get all the theBhytList where maTheCu equals to UPDATED_MA_THE_CU
        defaultTheBhytShouldNotBeFound("maTheCu.in=" + UPDATED_MA_THE_CU);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByMaTheCuIsNullOrNotNull() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maTheCu is not null
        defaultTheBhytShouldBeFound("maTheCu.specified=true");

        // Get all the theBhytList where maTheCu is null
        defaultTheBhytShouldNotBeFound("maTheCu.specified=false");
    }
                @Test
    @Transactional
    public void getAllTheBhytsByMaTheCuContainsSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maTheCu contains DEFAULT_MA_THE_CU
        defaultTheBhytShouldBeFound("maTheCu.contains=" + DEFAULT_MA_THE_CU);

        // Get all the theBhytList where maTheCu contains UPDATED_MA_THE_CU
        defaultTheBhytShouldNotBeFound("maTheCu.contains=" + UPDATED_MA_THE_CU);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByMaTheCuNotContainsSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where maTheCu does not contain DEFAULT_MA_THE_CU
        defaultTheBhytShouldNotBeFound("maTheCu.doesNotContain=" + DEFAULT_MA_THE_CU);

        // Get all the theBhytList where maTheCu does not contain UPDATED_MA_THE_CU
        defaultTheBhytShouldBeFound("maTheCu.doesNotContain=" + UPDATED_MA_THE_CU);
    }


    @Test
    @Transactional
    public void getAllTheBhytsByNgayBatDauIsEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayBatDau equals to DEFAULT_NGAY_BAT_DAU
        defaultTheBhytShouldBeFound("ngayBatDau.equals=" + DEFAULT_NGAY_BAT_DAU);

        // Get all the theBhytList where ngayBatDau equals to UPDATED_NGAY_BAT_DAU
        defaultTheBhytShouldNotBeFound("ngayBatDau.equals=" + UPDATED_NGAY_BAT_DAU);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayBatDauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayBatDau not equals to DEFAULT_NGAY_BAT_DAU
        defaultTheBhytShouldNotBeFound("ngayBatDau.notEquals=" + DEFAULT_NGAY_BAT_DAU);

        // Get all the theBhytList where ngayBatDau not equals to UPDATED_NGAY_BAT_DAU
        defaultTheBhytShouldBeFound("ngayBatDau.notEquals=" + UPDATED_NGAY_BAT_DAU);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayBatDauIsInShouldWork() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayBatDau in DEFAULT_NGAY_BAT_DAU or UPDATED_NGAY_BAT_DAU
        defaultTheBhytShouldBeFound("ngayBatDau.in=" + DEFAULT_NGAY_BAT_DAU + "," + UPDATED_NGAY_BAT_DAU);

        // Get all the theBhytList where ngayBatDau equals to UPDATED_NGAY_BAT_DAU
        defaultTheBhytShouldNotBeFound("ngayBatDau.in=" + UPDATED_NGAY_BAT_DAU);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayBatDauIsNullOrNotNull() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayBatDau is not null
        defaultTheBhytShouldBeFound("ngayBatDau.specified=true");

        // Get all the theBhytList where ngayBatDau is null
        defaultTheBhytShouldNotBeFound("ngayBatDau.specified=false");
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayBatDauIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayBatDau is greater than or equal to DEFAULT_NGAY_BAT_DAU
        defaultTheBhytShouldBeFound("ngayBatDau.greaterThanOrEqual=" + DEFAULT_NGAY_BAT_DAU);

        // Get all the theBhytList where ngayBatDau is greater than or equal to UPDATED_NGAY_BAT_DAU
        defaultTheBhytShouldNotBeFound("ngayBatDau.greaterThanOrEqual=" + UPDATED_NGAY_BAT_DAU);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayBatDauIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayBatDau is less than or equal to DEFAULT_NGAY_BAT_DAU
        defaultTheBhytShouldBeFound("ngayBatDau.lessThanOrEqual=" + DEFAULT_NGAY_BAT_DAU);

        // Get all the theBhytList where ngayBatDau is less than or equal to SMALLER_NGAY_BAT_DAU
        defaultTheBhytShouldNotBeFound("ngayBatDau.lessThanOrEqual=" + SMALLER_NGAY_BAT_DAU);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayBatDauIsLessThanSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayBatDau is less than DEFAULT_NGAY_BAT_DAU
        defaultTheBhytShouldNotBeFound("ngayBatDau.lessThan=" + DEFAULT_NGAY_BAT_DAU);

        // Get all the theBhytList where ngayBatDau is less than UPDATED_NGAY_BAT_DAU
        defaultTheBhytShouldBeFound("ngayBatDau.lessThan=" + UPDATED_NGAY_BAT_DAU);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayBatDauIsGreaterThanSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayBatDau is greater than DEFAULT_NGAY_BAT_DAU
        defaultTheBhytShouldNotBeFound("ngayBatDau.greaterThan=" + DEFAULT_NGAY_BAT_DAU);

        // Get all the theBhytList where ngayBatDau is greater than SMALLER_NGAY_BAT_DAU
        defaultTheBhytShouldBeFound("ngayBatDau.greaterThan=" + SMALLER_NGAY_BAT_DAU);
    }


    @Test
    @Transactional
    public void getAllTheBhytsByNgayDuNamNamIsEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayDuNamNam equals to DEFAULT_NGAY_DU_NAM_NAM
        defaultTheBhytShouldBeFound("ngayDuNamNam.equals=" + DEFAULT_NGAY_DU_NAM_NAM);

        // Get all the theBhytList where ngayDuNamNam equals to UPDATED_NGAY_DU_NAM_NAM
        defaultTheBhytShouldNotBeFound("ngayDuNamNam.equals=" + UPDATED_NGAY_DU_NAM_NAM);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayDuNamNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayDuNamNam not equals to DEFAULT_NGAY_DU_NAM_NAM
        defaultTheBhytShouldNotBeFound("ngayDuNamNam.notEquals=" + DEFAULT_NGAY_DU_NAM_NAM);

        // Get all the theBhytList where ngayDuNamNam not equals to UPDATED_NGAY_DU_NAM_NAM
        defaultTheBhytShouldBeFound("ngayDuNamNam.notEquals=" + UPDATED_NGAY_DU_NAM_NAM);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayDuNamNamIsInShouldWork() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayDuNamNam in DEFAULT_NGAY_DU_NAM_NAM or UPDATED_NGAY_DU_NAM_NAM
        defaultTheBhytShouldBeFound("ngayDuNamNam.in=" + DEFAULT_NGAY_DU_NAM_NAM + "," + UPDATED_NGAY_DU_NAM_NAM);

        // Get all the theBhytList where ngayDuNamNam equals to UPDATED_NGAY_DU_NAM_NAM
        defaultTheBhytShouldNotBeFound("ngayDuNamNam.in=" + UPDATED_NGAY_DU_NAM_NAM);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayDuNamNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayDuNamNam is not null
        defaultTheBhytShouldBeFound("ngayDuNamNam.specified=true");

        // Get all the theBhytList where ngayDuNamNam is null
        defaultTheBhytShouldNotBeFound("ngayDuNamNam.specified=false");
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayDuNamNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayDuNamNam is greater than or equal to DEFAULT_NGAY_DU_NAM_NAM
        defaultTheBhytShouldBeFound("ngayDuNamNam.greaterThanOrEqual=" + DEFAULT_NGAY_DU_NAM_NAM);

        // Get all the theBhytList where ngayDuNamNam is greater than or equal to UPDATED_NGAY_DU_NAM_NAM
        defaultTheBhytShouldNotBeFound("ngayDuNamNam.greaterThanOrEqual=" + UPDATED_NGAY_DU_NAM_NAM);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayDuNamNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayDuNamNam is less than or equal to DEFAULT_NGAY_DU_NAM_NAM
        defaultTheBhytShouldBeFound("ngayDuNamNam.lessThanOrEqual=" + DEFAULT_NGAY_DU_NAM_NAM);

        // Get all the theBhytList where ngayDuNamNam is less than or equal to SMALLER_NGAY_DU_NAM_NAM
        defaultTheBhytShouldNotBeFound("ngayDuNamNam.lessThanOrEqual=" + SMALLER_NGAY_DU_NAM_NAM);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayDuNamNamIsLessThanSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayDuNamNam is less than DEFAULT_NGAY_DU_NAM_NAM
        defaultTheBhytShouldNotBeFound("ngayDuNamNam.lessThan=" + DEFAULT_NGAY_DU_NAM_NAM);

        // Get all the theBhytList where ngayDuNamNam is less than UPDATED_NGAY_DU_NAM_NAM
        defaultTheBhytShouldBeFound("ngayDuNamNam.lessThan=" + UPDATED_NGAY_DU_NAM_NAM);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayDuNamNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayDuNamNam is greater than DEFAULT_NGAY_DU_NAM_NAM
        defaultTheBhytShouldNotBeFound("ngayDuNamNam.greaterThan=" + DEFAULT_NGAY_DU_NAM_NAM);

        // Get all the theBhytList where ngayDuNamNam is greater than SMALLER_NGAY_DU_NAM_NAM
        defaultTheBhytShouldBeFound("ngayDuNamNam.greaterThan=" + SMALLER_NGAY_DU_NAM_NAM);
    }


    @Test
    @Transactional
    public void getAllTheBhytsByNgayHetHanIsEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayHetHan equals to DEFAULT_NGAY_HET_HAN
        defaultTheBhytShouldBeFound("ngayHetHan.equals=" + DEFAULT_NGAY_HET_HAN);

        // Get all the theBhytList where ngayHetHan equals to UPDATED_NGAY_HET_HAN
        defaultTheBhytShouldNotBeFound("ngayHetHan.equals=" + UPDATED_NGAY_HET_HAN);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayHetHanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayHetHan not equals to DEFAULT_NGAY_HET_HAN
        defaultTheBhytShouldNotBeFound("ngayHetHan.notEquals=" + DEFAULT_NGAY_HET_HAN);

        // Get all the theBhytList where ngayHetHan not equals to UPDATED_NGAY_HET_HAN
        defaultTheBhytShouldBeFound("ngayHetHan.notEquals=" + UPDATED_NGAY_HET_HAN);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayHetHanIsInShouldWork() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayHetHan in DEFAULT_NGAY_HET_HAN or UPDATED_NGAY_HET_HAN
        defaultTheBhytShouldBeFound("ngayHetHan.in=" + DEFAULT_NGAY_HET_HAN + "," + UPDATED_NGAY_HET_HAN);

        // Get all the theBhytList where ngayHetHan equals to UPDATED_NGAY_HET_HAN
        defaultTheBhytShouldNotBeFound("ngayHetHan.in=" + UPDATED_NGAY_HET_HAN);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayHetHanIsNullOrNotNull() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayHetHan is not null
        defaultTheBhytShouldBeFound("ngayHetHan.specified=true");

        // Get all the theBhytList where ngayHetHan is null
        defaultTheBhytShouldNotBeFound("ngayHetHan.specified=false");
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayHetHanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayHetHan is greater than or equal to DEFAULT_NGAY_HET_HAN
        defaultTheBhytShouldBeFound("ngayHetHan.greaterThanOrEqual=" + DEFAULT_NGAY_HET_HAN);

        // Get all the theBhytList where ngayHetHan is greater than or equal to UPDATED_NGAY_HET_HAN
        defaultTheBhytShouldNotBeFound("ngayHetHan.greaterThanOrEqual=" + UPDATED_NGAY_HET_HAN);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayHetHanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayHetHan is less than or equal to DEFAULT_NGAY_HET_HAN
        defaultTheBhytShouldBeFound("ngayHetHan.lessThanOrEqual=" + DEFAULT_NGAY_HET_HAN);

        // Get all the theBhytList where ngayHetHan is less than or equal to SMALLER_NGAY_HET_HAN
        defaultTheBhytShouldNotBeFound("ngayHetHan.lessThanOrEqual=" + SMALLER_NGAY_HET_HAN);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayHetHanIsLessThanSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayHetHan is less than DEFAULT_NGAY_HET_HAN
        defaultTheBhytShouldNotBeFound("ngayHetHan.lessThan=" + DEFAULT_NGAY_HET_HAN);

        // Get all the theBhytList where ngayHetHan is less than UPDATED_NGAY_HET_HAN
        defaultTheBhytShouldBeFound("ngayHetHan.lessThan=" + UPDATED_NGAY_HET_HAN);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayHetHanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayHetHan is greater than DEFAULT_NGAY_HET_HAN
        defaultTheBhytShouldNotBeFound("ngayHetHan.greaterThan=" + DEFAULT_NGAY_HET_HAN);

        // Get all the theBhytList where ngayHetHan is greater than SMALLER_NGAY_HET_HAN
        defaultTheBhytShouldBeFound("ngayHetHan.greaterThan=" + SMALLER_NGAY_HET_HAN);
    }


    @Test
    @Transactional
    public void getAllTheBhytsByQrIsEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where qr equals to DEFAULT_QR
        defaultTheBhytShouldBeFound("qr.equals=" + DEFAULT_QR);

        // Get all the theBhytList where qr equals to UPDATED_QR
        defaultTheBhytShouldNotBeFound("qr.equals=" + UPDATED_QR);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByQrIsNotEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where qr not equals to DEFAULT_QR
        defaultTheBhytShouldNotBeFound("qr.notEquals=" + DEFAULT_QR);

        // Get all the theBhytList where qr not equals to UPDATED_QR
        defaultTheBhytShouldBeFound("qr.notEquals=" + UPDATED_QR);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByQrIsInShouldWork() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where qr in DEFAULT_QR or UPDATED_QR
        defaultTheBhytShouldBeFound("qr.in=" + DEFAULT_QR + "," + UPDATED_QR);

        // Get all the theBhytList where qr equals to UPDATED_QR
        defaultTheBhytShouldNotBeFound("qr.in=" + UPDATED_QR);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByQrIsNullOrNotNull() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where qr is not null
        defaultTheBhytShouldBeFound("qr.specified=true");

        // Get all the theBhytList where qr is null
        defaultTheBhytShouldNotBeFound("qr.specified=false");
    }
                @Test
    @Transactional
    public void getAllTheBhytsByQrContainsSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where qr contains DEFAULT_QR
        defaultTheBhytShouldBeFound("qr.contains=" + DEFAULT_QR);

        // Get all the theBhytList where qr contains UPDATED_QR
        defaultTheBhytShouldNotBeFound("qr.contains=" + UPDATED_QR);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByQrNotContainsSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where qr does not contain DEFAULT_QR
        defaultTheBhytShouldNotBeFound("qr.doesNotContain=" + DEFAULT_QR);

        // Get all the theBhytList where qr does not contain UPDATED_QR
        defaultTheBhytShouldBeFound("qr.doesNotContain=" + UPDATED_QR);
    }


    @Test
    @Transactional
    public void getAllTheBhytsBySoTheIsEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where soThe equals to DEFAULT_SO_THE
        defaultTheBhytShouldBeFound("soThe.equals=" + DEFAULT_SO_THE);

        // Get all the theBhytList where soThe equals to UPDATED_SO_THE
        defaultTheBhytShouldNotBeFound("soThe.equals=" + UPDATED_SO_THE);
    }

    @Test
    @Transactional
    public void getAllTheBhytsBySoTheIsNotEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where soThe not equals to DEFAULT_SO_THE
        defaultTheBhytShouldNotBeFound("soThe.notEquals=" + DEFAULT_SO_THE);

        // Get all the theBhytList where soThe not equals to UPDATED_SO_THE
        defaultTheBhytShouldBeFound("soThe.notEquals=" + UPDATED_SO_THE);
    }

    @Test
    @Transactional
    public void getAllTheBhytsBySoTheIsInShouldWork() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where soThe in DEFAULT_SO_THE or UPDATED_SO_THE
        defaultTheBhytShouldBeFound("soThe.in=" + DEFAULT_SO_THE + "," + UPDATED_SO_THE);

        // Get all the theBhytList where soThe equals to UPDATED_SO_THE
        defaultTheBhytShouldNotBeFound("soThe.in=" + UPDATED_SO_THE);
    }

    @Test
    @Transactional
    public void getAllTheBhytsBySoTheIsNullOrNotNull() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where soThe is not null
        defaultTheBhytShouldBeFound("soThe.specified=true");

        // Get all the theBhytList where soThe is null
        defaultTheBhytShouldNotBeFound("soThe.specified=false");
    }
                @Test
    @Transactional
    public void getAllTheBhytsBySoTheContainsSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where soThe contains DEFAULT_SO_THE
        defaultTheBhytShouldBeFound("soThe.contains=" + DEFAULT_SO_THE);

        // Get all the theBhytList where soThe contains UPDATED_SO_THE
        defaultTheBhytShouldNotBeFound("soThe.contains=" + UPDATED_SO_THE);
    }

    @Test
    @Transactional
    public void getAllTheBhytsBySoTheNotContainsSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where soThe does not contain DEFAULT_SO_THE
        defaultTheBhytShouldNotBeFound("soThe.doesNotContain=" + DEFAULT_SO_THE);

        // Get all the theBhytList where soThe does not contain UPDATED_SO_THE
        defaultTheBhytShouldBeFound("soThe.doesNotContain=" + UPDATED_SO_THE);
    }


    @Test
    @Transactional
    public void getAllTheBhytsByNgayMienCungChiTraIsEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayMienCungChiTra equals to DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldBeFound("ngayMienCungChiTra.equals=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the theBhytList where ngayMienCungChiTra equals to UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldNotBeFound("ngayMienCungChiTra.equals=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayMienCungChiTraIsNotEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayMienCungChiTra not equals to DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldNotBeFound("ngayMienCungChiTra.notEquals=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the theBhytList where ngayMienCungChiTra not equals to UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldBeFound("ngayMienCungChiTra.notEquals=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayMienCungChiTraIsInShouldWork() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayMienCungChiTra in DEFAULT_NGAY_MIEN_CUNG_CHI_TRA or UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldBeFound("ngayMienCungChiTra.in=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA + "," + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the theBhytList where ngayMienCungChiTra equals to UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldNotBeFound("ngayMienCungChiTra.in=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayMienCungChiTraIsNullOrNotNull() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayMienCungChiTra is not null
        defaultTheBhytShouldBeFound("ngayMienCungChiTra.specified=true");

        // Get all the theBhytList where ngayMienCungChiTra is null
        defaultTheBhytShouldNotBeFound("ngayMienCungChiTra.specified=false");
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayMienCungChiTraIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayMienCungChiTra is greater than or equal to DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldBeFound("ngayMienCungChiTra.greaterThanOrEqual=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the theBhytList where ngayMienCungChiTra is greater than or equal to UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldNotBeFound("ngayMienCungChiTra.greaterThanOrEqual=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayMienCungChiTraIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayMienCungChiTra is less than or equal to DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldBeFound("ngayMienCungChiTra.lessThanOrEqual=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the theBhytList where ngayMienCungChiTra is less than or equal to SMALLER_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldNotBeFound("ngayMienCungChiTra.lessThanOrEqual=" + SMALLER_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayMienCungChiTraIsLessThanSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayMienCungChiTra is less than DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldNotBeFound("ngayMienCungChiTra.lessThan=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the theBhytList where ngayMienCungChiTra is less than UPDATED_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldBeFound("ngayMienCungChiTra.lessThan=" + UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void getAllTheBhytsByNgayMienCungChiTraIsGreaterThanSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        // Get all the theBhytList where ngayMienCungChiTra is greater than DEFAULT_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldNotBeFound("ngayMienCungChiTra.greaterThan=" + DEFAULT_NGAY_MIEN_CUNG_CHI_TRA);

        // Get all the theBhytList where ngayMienCungChiTra is greater than SMALLER_NGAY_MIEN_CUNG_CHI_TRA
        defaultTheBhytShouldBeFound("ngayMienCungChiTra.greaterThan=" + SMALLER_NGAY_MIEN_CUNG_CHI_TRA);
    }


    @Test
    @Transactional
    public void getAllTheBhytsByBenhNhanIsEqualToSomething() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);
        BenhNhan benhNhan = BenhNhanResourceIT.createEntity(em);
        em.persist(benhNhan);
        em.flush();
        theBhyt.setBenhNhan(benhNhan);
        theBhytRepository.saveAndFlush(theBhyt);
        Long benhNhanId = benhNhan.getId();

        // Get all the theBhytList where benhNhan equals to benhNhanId
        defaultTheBhytShouldBeFound("benhNhanId.equals=" + benhNhanId);

        // Get all the theBhytList where benhNhan equals to benhNhanId + 1
        defaultTheBhytShouldNotBeFound("benhNhanId.equals=" + (benhNhanId + 1));
    }


    @Test
    @Transactional
    public void getAllTheBhytsByDoiTuongBhytIsEqualToSomething() throws Exception {
        // Get already existing entity
        DoiTuongBhyt doiTuongBhyt = theBhyt.getDoiTuongBhyt();
        theBhytRepository.saveAndFlush(theBhyt);
        Long doiTuongBhytId = doiTuongBhyt.getId();

        // Get all the theBhytList where doiTuongBhyt equals to doiTuongBhytId
        defaultTheBhytShouldBeFound("doiTuongBhytId.equals=" + doiTuongBhytId);

        // Get all the theBhytList where doiTuongBhyt equals to doiTuongBhytId + 1
        defaultTheBhytShouldNotBeFound("doiTuongBhytId.equals=" + (doiTuongBhytId + 1));
    }


    @Test
    @Transactional
    public void getAllTheBhytsByThongTinBhxhIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThongTinBhxh thongTinBhxh = theBhyt.getThongTinBhxh();
        theBhytRepository.saveAndFlush(theBhyt);
        Long thongTinBhxhId = thongTinBhxh.getId();

        // Get all the theBhytList where thongTinBhxh equals to thongTinBhxhId
        defaultTheBhytShouldBeFound("thongTinBhxhId.equals=" + thongTinBhxhId);

        // Get all the theBhytList where thongTinBhxh equals to thongTinBhxhId + 1
        defaultTheBhytShouldNotBeFound("thongTinBhxhId.equals=" + (thongTinBhxhId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTheBhytShouldBeFound(String filter) throws Exception {
        restTheBhytMockMvc.perform(get("/api/the-bhyts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(theBhyt.getId().intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].maKhuVuc").value(hasItem(DEFAULT_MA_KHU_VUC)))
            .andExpect(jsonPath("$.[*].maSoBhxh").value(hasItem(DEFAULT_MA_SO_BHXH)))
            .andExpect(jsonPath("$.[*].maTheCu").value(hasItem(DEFAULT_MA_THE_CU)))
            .andExpect(jsonPath("$.[*].ngayBatDau").value(hasItem(DEFAULT_NGAY_BAT_DAU.toString())))
            .andExpect(jsonPath("$.[*].ngayDuNamNam").value(hasItem(DEFAULT_NGAY_DU_NAM_NAM.toString())))
            .andExpect(jsonPath("$.[*].ngayHetHan").value(hasItem(DEFAULT_NGAY_HET_HAN.toString())))
            .andExpect(jsonPath("$.[*].qr").value(hasItem(DEFAULT_QR)))
            .andExpect(jsonPath("$.[*].soThe").value(hasItem(DEFAULT_SO_THE)))
            .andExpect(jsonPath("$.[*].ngayMienCungChiTra").value(hasItem(DEFAULT_NGAY_MIEN_CUNG_CHI_TRA.toString())));

        // Check, that the count call also returns 1
        restTheBhytMockMvc.perform(get("/api/the-bhyts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTheBhytShouldNotBeFound(String filter) throws Exception {
        restTheBhytMockMvc.perform(get("/api/the-bhyts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTheBhytMockMvc.perform(get("/api/the-bhyts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTheBhyt() throws Exception {
        // Get the theBhyt
        restTheBhytMockMvc.perform(get("/api/the-bhyts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTheBhyt() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        int databaseSizeBeforeUpdate = theBhytRepository.findAll().size();

        // Update the theBhyt
        TheBhyt updatedTheBhyt = theBhytRepository.findById(theBhyt.getId()).get();
        // Disconnect from session so that the updates on updatedTheBhyt are not directly saved in db
        em.detach(updatedTheBhyt);
        updatedTheBhyt
            .enabled(UPDATED_ENABLED)
            .maKhuVuc(UPDATED_MA_KHU_VUC)
            .maSoBhxh(UPDATED_MA_SO_BHXH)
            .maTheCu(UPDATED_MA_THE_CU)
            .ngayBatDau(UPDATED_NGAY_BAT_DAU)
            .ngayDuNamNam(UPDATED_NGAY_DU_NAM_NAM)
            .ngayHetHan(UPDATED_NGAY_HET_HAN)
            .qr(UPDATED_QR)
            .soThe(UPDATED_SO_THE)
            .ngayMienCungChiTra(UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
        TheBhytDTO theBhytDTO = theBhytMapper.toDto(updatedTheBhyt);

        restTheBhytMockMvc.perform(put("/api/the-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(theBhytDTO)))
            .andExpect(status().isOk());

        // Validate the TheBhyt in the database
        List<TheBhyt> theBhytList = theBhytRepository.findAll();
        assertThat(theBhytList).hasSize(databaseSizeBeforeUpdate);
        TheBhyt testTheBhyt = theBhytList.get(theBhytList.size() - 1);
        assertThat(testTheBhyt.isEnabled()).isEqualTo(UPDATED_ENABLED);
        assertThat(testTheBhyt.getMaKhuVuc()).isEqualTo(UPDATED_MA_KHU_VUC);
        assertThat(testTheBhyt.getMaSoBhxh()).isEqualTo(UPDATED_MA_SO_BHXH);
        assertThat(testTheBhyt.getMaTheCu()).isEqualTo(UPDATED_MA_THE_CU);
        assertThat(testTheBhyt.getNgayBatDau()).isEqualTo(UPDATED_NGAY_BAT_DAU);
        assertThat(testTheBhyt.getNgayDuNamNam()).isEqualTo(UPDATED_NGAY_DU_NAM_NAM);
        assertThat(testTheBhyt.getNgayHetHan()).isEqualTo(UPDATED_NGAY_HET_HAN);
        assertThat(testTheBhyt.getQr()).isEqualTo(UPDATED_QR);
        assertThat(testTheBhyt.getSoThe()).isEqualTo(UPDATED_SO_THE);
        assertThat(testTheBhyt.getNgayMienCungChiTra()).isEqualTo(UPDATED_NGAY_MIEN_CUNG_CHI_TRA);
    }

    @Test
    @Transactional
    public void updateNonExistingTheBhyt() throws Exception {
        int databaseSizeBeforeUpdate = theBhytRepository.findAll().size();

        // Create the TheBhyt
        TheBhytDTO theBhytDTO = theBhytMapper.toDto(theBhyt);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTheBhytMockMvc.perform(put("/api/the-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(theBhytDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TheBhyt in the database
        List<TheBhyt> theBhytList = theBhytRepository.findAll();
        assertThat(theBhytList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTheBhyt() throws Exception {
        // Initialize the database
        theBhytRepository.saveAndFlush(theBhyt);

        int databaseSizeBeforeDelete = theBhytRepository.findAll().size();

        // Delete the theBhyt
        restTheBhytMockMvc.perform(delete("/api/the-bhyts/{id}", theBhyt.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TheBhyt> theBhytList = theBhytRepository.findAll();
        assertThat(theBhytList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
