package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.ChiDinhDichVuKham;
import vn.vnpt.domain.ThongTinKhamBenh;
import vn.vnpt.domain.DichVuKham;
import vn.vnpt.repository.ChiDinhDichVuKhamRepository;
import vn.vnpt.service.ChiDinhDichVuKhamService;
import vn.vnpt.service.dto.ChiDinhDichVuKhamDTO;
import vn.vnpt.service.mapper.ChiDinhDichVuKhamMapper;
import vn.vnpt.service.dto.ChiDinhDichVuKhamCriteria;
import vn.vnpt.service.ChiDinhDichVuKhamQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ChiDinhDichVuKhamResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class ChiDinhDichVuKhamResourceIT {

    private static final Boolean DEFAULT_CONG_KHAM_BAN_DAU = false;
    private static final Boolean UPDATED_CONG_KHAM_BAN_DAU = true;

    private static final Boolean DEFAULT_DA_THANH_TOAN = false;
    private static final Boolean UPDATED_DA_THANH_TOAN = true;

    private static final BigDecimal DEFAULT_DON_GIA = new BigDecimal(1);
    private static final BigDecimal UPDATED_DON_GIA = new BigDecimal(2);
    private static final BigDecimal SMALLER_DON_GIA = new BigDecimal(1 - 1);

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_THOI_GIAN_CHI_DINH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_THOI_GIAN_CHI_DINH = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_THOI_GIAN_CHI_DINH = LocalDate.ofEpochDay(-1L);

    private static final BigDecimal DEFAULT_TY_LE_THANH_TOAN = new BigDecimal(1);
    private static final BigDecimal UPDATED_TY_LE_THANH_TOAN = new BigDecimal(2);
    private static final BigDecimal SMALLER_TY_LE_THANH_TOAN = new BigDecimal(1 - 1);

    private static final Boolean DEFAULT_CO_BAO_HIEM = false;
    private static final Boolean UPDATED_CO_BAO_HIEM = true;

    private static final BigDecimal DEFAULT_GIA_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_GIA_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_GIA_BHYT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_GIA_KHONG_BHYT = new BigDecimal(1);
    private static final BigDecimal UPDATED_GIA_KHONG_BHYT = new BigDecimal(2);
    private static final BigDecimal SMALLER_GIA_KHONG_BHYT = new BigDecimal(1 - 1);

    private static final String DEFAULT_MA_DUNG_CHUNG = "AAAAAAAAAA";
    private static final String UPDATED_MA_DUNG_CHUNG = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DICH_VU_YEU_CAU = false;
    private static final Boolean UPDATED_DICH_VU_YEU_CAU = true;

    private static final Integer DEFAULT_NAM = 1;
    private static final Integer UPDATED_NAM = 2;
    private static final Integer SMALLER_NAM = 1 - 1;

    @Autowired
    private ChiDinhDichVuKhamRepository chiDinhDichVuKhamRepository;

    @Autowired
    private ChiDinhDichVuKhamMapper chiDinhDichVuKhamMapper;

    @Autowired
    private ChiDinhDichVuKhamService chiDinhDichVuKhamService;

    @Autowired
    private ChiDinhDichVuKhamQueryService chiDinhDichVuKhamQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChiDinhDichVuKhamMockMvc;

    private ChiDinhDichVuKham chiDinhDichVuKham;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiDinhDichVuKham createEntity(EntityManager em) {
        ChiDinhDichVuKham chiDinhDichVuKham = new ChiDinhDichVuKham()
            .congKhamBanDau(DEFAULT_CONG_KHAM_BAN_DAU)
            .daThanhToan(DEFAULT_DA_THANH_TOAN)
            .donGia(DEFAULT_DON_GIA)
            .ten(DEFAULT_TEN)
            .thoiGianChiDinh(DEFAULT_THOI_GIAN_CHI_DINH)
            .tyLeThanhToan(DEFAULT_TY_LE_THANH_TOAN)
            .coBaoHiem(DEFAULT_CO_BAO_HIEM)
            .giaBhyt(DEFAULT_GIA_BHYT)
            .giaKhongBhyt(DEFAULT_GIA_KHONG_BHYT)
            .maDungChung(DEFAULT_MA_DUNG_CHUNG)
            .dichVuYeuCau(DEFAULT_DICH_VU_YEU_CAU)
            .nam(DEFAULT_NAM);
        // Add required entity
        ThongTinKhamBenh thongTinKhamBenh;
        if (TestUtil.findAll(em, ThongTinKhamBenh.class).isEmpty()) {
            thongTinKhamBenh = ThongTinKhamBenhResourceIT.createEntity(em);
            em.persist(thongTinKhamBenh);
            em.flush();
        } else {
            thongTinKhamBenh = TestUtil.findAll(em, ThongTinKhamBenh.class).get(0);
        }
        chiDinhDichVuKham.setTtkb(thongTinKhamBenh);
        // Add required entity
        DichVuKham dichVuKham;
        if (TestUtil.findAll(em, DichVuKham.class).isEmpty()) {
            dichVuKham = DichVuKhamResourceIT.createEntity(em);
            em.persist(dichVuKham);
            em.flush();
        } else {
            dichVuKham = TestUtil.findAll(em, DichVuKham.class).get(0);
        }
        chiDinhDichVuKham.setDichVuKham(dichVuKham);
        return chiDinhDichVuKham;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiDinhDichVuKham createUpdatedEntity(EntityManager em) {
        ChiDinhDichVuKham chiDinhDichVuKham = new ChiDinhDichVuKham()
            .congKhamBanDau(UPDATED_CONG_KHAM_BAN_DAU)
            .daThanhToan(UPDATED_DA_THANH_TOAN)
            .donGia(UPDATED_DON_GIA)
            .ten(UPDATED_TEN)
            .thoiGianChiDinh(UPDATED_THOI_GIAN_CHI_DINH)
            .tyLeThanhToan(UPDATED_TY_LE_THANH_TOAN)
            .coBaoHiem(UPDATED_CO_BAO_HIEM)
            .giaBhyt(UPDATED_GIA_BHYT)
            .giaKhongBhyt(UPDATED_GIA_KHONG_BHYT)
            .maDungChung(UPDATED_MA_DUNG_CHUNG)
            .dichVuYeuCau(UPDATED_DICH_VU_YEU_CAU)
            .nam(UPDATED_NAM);
        // Add required entity
        ThongTinKhamBenh thongTinKhamBenh;
        if (TestUtil.findAll(em, ThongTinKhamBenh.class).isEmpty()) {
            thongTinKhamBenh = ThongTinKhamBenhResourceIT.createUpdatedEntity(em);
            em.persist(thongTinKhamBenh);
            em.flush();
        } else {
            thongTinKhamBenh = TestUtil.findAll(em, ThongTinKhamBenh.class).get(0);
        }
        chiDinhDichVuKham.setTtkb(thongTinKhamBenh);
        // Add required entity
        DichVuKham dichVuKham;
        if (TestUtil.findAll(em, DichVuKham.class).isEmpty()) {
            dichVuKham = DichVuKhamResourceIT.createUpdatedEntity(em);
            em.persist(dichVuKham);
            em.flush();
        } else {
            dichVuKham = TestUtil.findAll(em, DichVuKham.class).get(0);
        }
        chiDinhDichVuKham.setDichVuKham(dichVuKham);
        return chiDinhDichVuKham;
    }

    @BeforeEach
    public void initTest() {
        chiDinhDichVuKham = createEntity(em);
    }

    @Test
    @Transactional
    public void createChiDinhDichVuKham() throws Exception {
        int databaseSizeBeforeCreate = chiDinhDichVuKhamRepository.findAll().size();

        // Create the ChiDinhDichVuKham
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);
        restChiDinhDichVuKhamMockMvc.perform(post("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isCreated());

        // Validate the ChiDinhDichVuKham in the database
        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeCreate + 1);
        ChiDinhDichVuKham testChiDinhDichVuKham = chiDinhDichVuKhamList.get(chiDinhDichVuKhamList.size() - 1);
        assertThat(testChiDinhDichVuKham.isCongKhamBanDau()).isEqualTo(DEFAULT_CONG_KHAM_BAN_DAU);
        assertThat(testChiDinhDichVuKham.isDaThanhToan()).isEqualTo(DEFAULT_DA_THANH_TOAN);
        assertThat(testChiDinhDichVuKham.getDonGia()).isEqualTo(DEFAULT_DON_GIA);
        assertThat(testChiDinhDichVuKham.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testChiDinhDichVuKham.getThoiGianChiDinh()).isEqualTo(DEFAULT_THOI_GIAN_CHI_DINH);
        assertThat(testChiDinhDichVuKham.getTyLeThanhToan()).isEqualTo(DEFAULT_TY_LE_THANH_TOAN);
        assertThat(testChiDinhDichVuKham.isCoBaoHiem()).isEqualTo(DEFAULT_CO_BAO_HIEM);
        assertThat(testChiDinhDichVuKham.getGiaBhyt()).isEqualTo(DEFAULT_GIA_BHYT);
        assertThat(testChiDinhDichVuKham.getGiaKhongBhyt()).isEqualTo(DEFAULT_GIA_KHONG_BHYT);
        assertThat(testChiDinhDichVuKham.getMaDungChung()).isEqualTo(DEFAULT_MA_DUNG_CHUNG);
        assertThat(testChiDinhDichVuKham.isDichVuYeuCau()).isEqualTo(DEFAULT_DICH_VU_YEU_CAU);
        assertThat(testChiDinhDichVuKham.getNam()).isEqualTo(DEFAULT_NAM);
    }

    @Test
    @Transactional
    public void createChiDinhDichVuKhamWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chiDinhDichVuKhamRepository.findAll().size();

        // Create the ChiDinhDichVuKham with an existing ID
        chiDinhDichVuKham.setId(1L);
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChiDinhDichVuKhamMockMvc.perform(post("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChiDinhDichVuKham in the database
        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCongKhamBanDauIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhDichVuKhamRepository.findAll().size();
        // set the field null
        chiDinhDichVuKham.setCongKhamBanDau(null);

        // Create the ChiDinhDichVuKham, which fails.
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);

        restChiDinhDichVuKhamMockMvc.perform(post("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDaThanhToanIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhDichVuKhamRepository.findAll().size();
        // set the field null
        chiDinhDichVuKham.setDaThanhToan(null);

        // Create the ChiDinhDichVuKham, which fails.
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);

        restChiDinhDichVuKhamMockMvc.perform(post("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDonGiaIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhDichVuKhamRepository.findAll().size();
        // set the field null
        chiDinhDichVuKham.setDonGia(null);

        // Create the ChiDinhDichVuKham, which fails.
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);

        restChiDinhDichVuKhamMockMvc.perform(post("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhDichVuKhamRepository.findAll().size();
        // set the field null
        chiDinhDichVuKham.setTen(null);

        // Create the ChiDinhDichVuKham, which fails.
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);

        restChiDinhDichVuKhamMockMvc.perform(post("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkThoiGianChiDinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhDichVuKhamRepository.findAll().size();
        // set the field null
        chiDinhDichVuKham.setThoiGianChiDinh(null);

        // Create the ChiDinhDichVuKham, which fails.
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);

        restChiDinhDichVuKhamMockMvc.perform(post("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCoBaoHiemIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhDichVuKhamRepository.findAll().size();
        // set the field null
        chiDinhDichVuKham.setCoBaoHiem(null);

        // Create the ChiDinhDichVuKham, which fails.
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);

        restChiDinhDichVuKhamMockMvc.perform(post("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGiaBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhDichVuKhamRepository.findAll().size();
        // set the field null
        chiDinhDichVuKham.setGiaBhyt(null);

        // Create the ChiDinhDichVuKham, which fails.
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);

        restChiDinhDichVuKhamMockMvc.perform(post("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGiaKhongBhytIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhDichVuKhamRepository.findAll().size();
        // set the field null
        chiDinhDichVuKham.setGiaKhongBhyt(null);

        // Create the ChiDinhDichVuKham, which fails.
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);

        restChiDinhDichVuKhamMockMvc.perform(post("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDichVuYeuCauIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhDichVuKhamRepository.findAll().size();
        // set the field null
        chiDinhDichVuKham.setDichVuYeuCau(null);

        // Create the ChiDinhDichVuKham, which fails.
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);

        restChiDinhDichVuKhamMockMvc.perform(post("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiDinhDichVuKhamRepository.findAll().size();
        // set the field null
        chiDinhDichVuKham.setNam(null);

        // Create the ChiDinhDichVuKham, which fails.
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);

        restChiDinhDichVuKhamMockMvc.perform(post("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhams() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList
        restChiDinhDichVuKhamMockMvc.perform(get("/api/chi-dinh-dich-vu-khams?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiDinhDichVuKham.getId().intValue())))
            .andExpect(jsonPath("$.[*].congKhamBanDau").value(hasItem(DEFAULT_CONG_KHAM_BAN_DAU.booleanValue())))
            .andExpect(jsonPath("$.[*].daThanhToan").value(hasItem(DEFAULT_DA_THANH_TOAN.booleanValue())))
            .andExpect(jsonPath("$.[*].donGia").value(hasItem(DEFAULT_DON_GIA.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].thoiGianChiDinh").value(hasItem(DEFAULT_THOI_GIAN_CHI_DINH.toString())))
            .andExpect(jsonPath("$.[*].tyLeThanhToan").value(hasItem(DEFAULT_TY_LE_THANH_TOAN.intValue())))
            .andExpect(jsonPath("$.[*].coBaoHiem").value(hasItem(DEFAULT_CO_BAO_HIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].giaBhyt").value(hasItem(DEFAULT_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].giaKhongBhyt").value(hasItem(DEFAULT_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)))
            .andExpect(jsonPath("$.[*].dichVuYeuCau").value(hasItem(DEFAULT_DICH_VU_YEU_CAU.booleanValue())))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));
    }
    
    @Test
    @Transactional
    public void getChiDinhDichVuKham() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get the chiDinhDichVuKham
        restChiDinhDichVuKhamMockMvc.perform(get("/api/chi-dinh-dich-vu-khams/{id}", chiDinhDichVuKham.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chiDinhDichVuKham.getId().intValue()))
            .andExpect(jsonPath("$.congKhamBanDau").value(DEFAULT_CONG_KHAM_BAN_DAU.booleanValue()))
            .andExpect(jsonPath("$.daThanhToan").value(DEFAULT_DA_THANH_TOAN.booleanValue()))
            .andExpect(jsonPath("$.donGia").value(DEFAULT_DON_GIA.intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.thoiGianChiDinh").value(DEFAULT_THOI_GIAN_CHI_DINH.toString()))
            .andExpect(jsonPath("$.tyLeThanhToan").value(DEFAULT_TY_LE_THANH_TOAN.intValue()))
            .andExpect(jsonPath("$.coBaoHiem").value(DEFAULT_CO_BAO_HIEM.booleanValue()))
            .andExpect(jsonPath("$.giaBhyt").value(DEFAULT_GIA_BHYT.intValue()))
            .andExpect(jsonPath("$.giaKhongBhyt").value(DEFAULT_GIA_KHONG_BHYT.intValue()))
            .andExpect(jsonPath("$.maDungChung").value(DEFAULT_MA_DUNG_CHUNG))
            .andExpect(jsonPath("$.dichVuYeuCau").value(DEFAULT_DICH_VU_YEU_CAU.booleanValue()))
            .andExpect(jsonPath("$.nam").value(DEFAULT_NAM));
    }


    @Test
    @Transactional
    public void getChiDinhDichVuKhamsByIdFiltering() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        Long id = chiDinhDichVuKham.getId();

        defaultChiDinhDichVuKhamShouldBeFound("id.equals=" + id);
        defaultChiDinhDichVuKhamShouldNotBeFound("id.notEquals=" + id);

        defaultChiDinhDichVuKhamShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultChiDinhDichVuKhamShouldNotBeFound("id.greaterThan=" + id);

        defaultChiDinhDichVuKhamShouldBeFound("id.lessThanOrEqual=" + id);
        defaultChiDinhDichVuKhamShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByCongKhamBanDauIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where congKhamBanDau equals to DEFAULT_CONG_KHAM_BAN_DAU
        defaultChiDinhDichVuKhamShouldBeFound("congKhamBanDau.equals=" + DEFAULT_CONG_KHAM_BAN_DAU);

        // Get all the chiDinhDichVuKhamList where congKhamBanDau equals to UPDATED_CONG_KHAM_BAN_DAU
        defaultChiDinhDichVuKhamShouldNotBeFound("congKhamBanDau.equals=" + UPDATED_CONG_KHAM_BAN_DAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByCongKhamBanDauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where congKhamBanDau not equals to DEFAULT_CONG_KHAM_BAN_DAU
        defaultChiDinhDichVuKhamShouldNotBeFound("congKhamBanDau.notEquals=" + DEFAULT_CONG_KHAM_BAN_DAU);

        // Get all the chiDinhDichVuKhamList where congKhamBanDau not equals to UPDATED_CONG_KHAM_BAN_DAU
        defaultChiDinhDichVuKhamShouldBeFound("congKhamBanDau.notEquals=" + UPDATED_CONG_KHAM_BAN_DAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByCongKhamBanDauIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where congKhamBanDau in DEFAULT_CONG_KHAM_BAN_DAU or UPDATED_CONG_KHAM_BAN_DAU
        defaultChiDinhDichVuKhamShouldBeFound("congKhamBanDau.in=" + DEFAULT_CONG_KHAM_BAN_DAU + "," + UPDATED_CONG_KHAM_BAN_DAU);

        // Get all the chiDinhDichVuKhamList where congKhamBanDau equals to UPDATED_CONG_KHAM_BAN_DAU
        defaultChiDinhDichVuKhamShouldNotBeFound("congKhamBanDau.in=" + UPDATED_CONG_KHAM_BAN_DAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByCongKhamBanDauIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where congKhamBanDau is not null
        defaultChiDinhDichVuKhamShouldBeFound("congKhamBanDau.specified=true");

        // Get all the chiDinhDichVuKhamList where congKhamBanDau is null
        defaultChiDinhDichVuKhamShouldNotBeFound("congKhamBanDau.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDaThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where daThanhToan equals to DEFAULT_DA_THANH_TOAN
        defaultChiDinhDichVuKhamShouldBeFound("daThanhToan.equals=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhDichVuKhamList where daThanhToan equals to UPDATED_DA_THANH_TOAN
        defaultChiDinhDichVuKhamShouldNotBeFound("daThanhToan.equals=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDaThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where daThanhToan not equals to DEFAULT_DA_THANH_TOAN
        defaultChiDinhDichVuKhamShouldNotBeFound("daThanhToan.notEquals=" + DEFAULT_DA_THANH_TOAN);

        // Get all the chiDinhDichVuKhamList where daThanhToan not equals to UPDATED_DA_THANH_TOAN
        defaultChiDinhDichVuKhamShouldBeFound("daThanhToan.notEquals=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDaThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where daThanhToan in DEFAULT_DA_THANH_TOAN or UPDATED_DA_THANH_TOAN
        defaultChiDinhDichVuKhamShouldBeFound("daThanhToan.in=" + DEFAULT_DA_THANH_TOAN + "," + UPDATED_DA_THANH_TOAN);

        // Get all the chiDinhDichVuKhamList where daThanhToan equals to UPDATED_DA_THANH_TOAN
        defaultChiDinhDichVuKhamShouldNotBeFound("daThanhToan.in=" + UPDATED_DA_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDaThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where daThanhToan is not null
        defaultChiDinhDichVuKhamShouldBeFound("daThanhToan.specified=true");

        // Get all the chiDinhDichVuKhamList where daThanhToan is null
        defaultChiDinhDichVuKhamShouldNotBeFound("daThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDonGiaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where donGia equals to DEFAULT_DON_GIA
        defaultChiDinhDichVuKhamShouldBeFound("donGia.equals=" + DEFAULT_DON_GIA);

        // Get all the chiDinhDichVuKhamList where donGia equals to UPDATED_DON_GIA
        defaultChiDinhDichVuKhamShouldNotBeFound("donGia.equals=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDonGiaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where donGia not equals to DEFAULT_DON_GIA
        defaultChiDinhDichVuKhamShouldNotBeFound("donGia.notEquals=" + DEFAULT_DON_GIA);

        // Get all the chiDinhDichVuKhamList where donGia not equals to UPDATED_DON_GIA
        defaultChiDinhDichVuKhamShouldBeFound("donGia.notEquals=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDonGiaIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where donGia in DEFAULT_DON_GIA or UPDATED_DON_GIA
        defaultChiDinhDichVuKhamShouldBeFound("donGia.in=" + DEFAULT_DON_GIA + "," + UPDATED_DON_GIA);

        // Get all the chiDinhDichVuKhamList where donGia equals to UPDATED_DON_GIA
        defaultChiDinhDichVuKhamShouldNotBeFound("donGia.in=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDonGiaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where donGia is not null
        defaultChiDinhDichVuKhamShouldBeFound("donGia.specified=true");

        // Get all the chiDinhDichVuKhamList where donGia is null
        defaultChiDinhDichVuKhamShouldNotBeFound("donGia.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDonGiaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where donGia is greater than or equal to DEFAULT_DON_GIA
        defaultChiDinhDichVuKhamShouldBeFound("donGia.greaterThanOrEqual=" + DEFAULT_DON_GIA);

        // Get all the chiDinhDichVuKhamList where donGia is greater than or equal to UPDATED_DON_GIA
        defaultChiDinhDichVuKhamShouldNotBeFound("donGia.greaterThanOrEqual=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDonGiaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where donGia is less than or equal to DEFAULT_DON_GIA
        defaultChiDinhDichVuKhamShouldBeFound("donGia.lessThanOrEqual=" + DEFAULT_DON_GIA);

        // Get all the chiDinhDichVuKhamList where donGia is less than or equal to SMALLER_DON_GIA
        defaultChiDinhDichVuKhamShouldNotBeFound("donGia.lessThanOrEqual=" + SMALLER_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDonGiaIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where donGia is less than DEFAULT_DON_GIA
        defaultChiDinhDichVuKhamShouldNotBeFound("donGia.lessThan=" + DEFAULT_DON_GIA);

        // Get all the chiDinhDichVuKhamList where donGia is less than UPDATED_DON_GIA
        defaultChiDinhDichVuKhamShouldBeFound("donGia.lessThan=" + UPDATED_DON_GIA);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDonGiaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where donGia is greater than DEFAULT_DON_GIA
        defaultChiDinhDichVuKhamShouldNotBeFound("donGia.greaterThan=" + DEFAULT_DON_GIA);

        // Get all the chiDinhDichVuKhamList where donGia is greater than SMALLER_DON_GIA
        defaultChiDinhDichVuKhamShouldBeFound("donGia.greaterThan=" + SMALLER_DON_GIA);
    }


    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where ten equals to DEFAULT_TEN
        defaultChiDinhDichVuKhamShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the chiDinhDichVuKhamList where ten equals to UPDATED_TEN
        defaultChiDinhDichVuKhamShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where ten not equals to DEFAULT_TEN
        defaultChiDinhDichVuKhamShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the chiDinhDichVuKhamList where ten not equals to UPDATED_TEN
        defaultChiDinhDichVuKhamShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultChiDinhDichVuKhamShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the chiDinhDichVuKhamList where ten equals to UPDATED_TEN
        defaultChiDinhDichVuKhamShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where ten is not null
        defaultChiDinhDichVuKhamShouldBeFound("ten.specified=true");

        // Get all the chiDinhDichVuKhamList where ten is null
        defaultChiDinhDichVuKhamShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTenContainsSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where ten contains DEFAULT_TEN
        defaultChiDinhDichVuKhamShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the chiDinhDichVuKhamList where ten contains UPDATED_TEN
        defaultChiDinhDichVuKhamShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where ten does not contain DEFAULT_TEN
        defaultChiDinhDichVuKhamShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the chiDinhDichVuKhamList where ten does not contain UPDATED_TEN
        defaultChiDinhDichVuKhamShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByThoiGianChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh equals to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldBeFound("thoiGianChiDinh.equals=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh equals to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldNotBeFound("thoiGianChiDinh.equals=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByThoiGianChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh not equals to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldNotBeFound("thoiGianChiDinh.notEquals=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh not equals to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldBeFound("thoiGianChiDinh.notEquals=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByThoiGianChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh in DEFAULT_THOI_GIAN_CHI_DINH or UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldBeFound("thoiGianChiDinh.in=" + DEFAULT_THOI_GIAN_CHI_DINH + "," + UPDATED_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh equals to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldNotBeFound("thoiGianChiDinh.in=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByThoiGianChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh is not null
        defaultChiDinhDichVuKhamShouldBeFound("thoiGianChiDinh.specified=true");

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh is null
        defaultChiDinhDichVuKhamShouldNotBeFound("thoiGianChiDinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByThoiGianChiDinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh is greater than or equal to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldBeFound("thoiGianChiDinh.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh is greater than or equal to UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldNotBeFound("thoiGianChiDinh.greaterThanOrEqual=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByThoiGianChiDinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh is less than or equal to DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldBeFound("thoiGianChiDinh.lessThanOrEqual=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh is less than or equal to SMALLER_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldNotBeFound("thoiGianChiDinh.lessThanOrEqual=" + SMALLER_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByThoiGianChiDinhIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh is less than DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldNotBeFound("thoiGianChiDinh.lessThan=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh is less than UPDATED_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldBeFound("thoiGianChiDinh.lessThan=" + UPDATED_THOI_GIAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByThoiGianChiDinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh is greater than DEFAULT_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldNotBeFound("thoiGianChiDinh.greaterThan=" + DEFAULT_THOI_GIAN_CHI_DINH);

        // Get all the chiDinhDichVuKhamList where thoiGianChiDinh is greater than SMALLER_THOI_GIAN_CHI_DINH
        defaultChiDinhDichVuKhamShouldBeFound("thoiGianChiDinh.greaterThan=" + SMALLER_THOI_GIAN_CHI_DINH);
    }


    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTyLeThanhToanIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan equals to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldBeFound("tyLeThanhToan.equals=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan equals to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldNotBeFound("tyLeThanhToan.equals=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTyLeThanhToanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan not equals to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldNotBeFound("tyLeThanhToan.notEquals=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan not equals to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldBeFound("tyLeThanhToan.notEquals=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTyLeThanhToanIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan in DEFAULT_TY_LE_THANH_TOAN or UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldBeFound("tyLeThanhToan.in=" + DEFAULT_TY_LE_THANH_TOAN + "," + UPDATED_TY_LE_THANH_TOAN);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan equals to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldNotBeFound("tyLeThanhToan.in=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTyLeThanhToanIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan is not null
        defaultChiDinhDichVuKhamShouldBeFound("tyLeThanhToan.specified=true");

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan is null
        defaultChiDinhDichVuKhamShouldNotBeFound("tyLeThanhToan.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTyLeThanhToanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan is greater than or equal to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldBeFound("tyLeThanhToan.greaterThanOrEqual=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan is greater than or equal to UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldNotBeFound("tyLeThanhToan.greaterThanOrEqual=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTyLeThanhToanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan is less than or equal to DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldBeFound("tyLeThanhToan.lessThanOrEqual=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan is less than or equal to SMALLER_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldNotBeFound("tyLeThanhToan.lessThanOrEqual=" + SMALLER_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTyLeThanhToanIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan is less than DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldNotBeFound("tyLeThanhToan.lessThan=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan is less than UPDATED_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldBeFound("tyLeThanhToan.lessThan=" + UPDATED_TY_LE_THANH_TOAN);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTyLeThanhToanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan is greater than DEFAULT_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldNotBeFound("tyLeThanhToan.greaterThan=" + DEFAULT_TY_LE_THANH_TOAN);

        // Get all the chiDinhDichVuKhamList where tyLeThanhToan is greater than SMALLER_TY_LE_THANH_TOAN
        defaultChiDinhDichVuKhamShouldBeFound("tyLeThanhToan.greaterThan=" + SMALLER_TY_LE_THANH_TOAN);
    }


    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByCoBaoHiemIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where coBaoHiem equals to DEFAULT_CO_BAO_HIEM
        defaultChiDinhDichVuKhamShouldBeFound("coBaoHiem.equals=" + DEFAULT_CO_BAO_HIEM);

        // Get all the chiDinhDichVuKhamList where coBaoHiem equals to UPDATED_CO_BAO_HIEM
        defaultChiDinhDichVuKhamShouldNotBeFound("coBaoHiem.equals=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByCoBaoHiemIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where coBaoHiem not equals to DEFAULT_CO_BAO_HIEM
        defaultChiDinhDichVuKhamShouldNotBeFound("coBaoHiem.notEquals=" + DEFAULT_CO_BAO_HIEM);

        // Get all the chiDinhDichVuKhamList where coBaoHiem not equals to UPDATED_CO_BAO_HIEM
        defaultChiDinhDichVuKhamShouldBeFound("coBaoHiem.notEquals=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByCoBaoHiemIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where coBaoHiem in DEFAULT_CO_BAO_HIEM or UPDATED_CO_BAO_HIEM
        defaultChiDinhDichVuKhamShouldBeFound("coBaoHiem.in=" + DEFAULT_CO_BAO_HIEM + "," + UPDATED_CO_BAO_HIEM);

        // Get all the chiDinhDichVuKhamList where coBaoHiem equals to UPDATED_CO_BAO_HIEM
        defaultChiDinhDichVuKhamShouldNotBeFound("coBaoHiem.in=" + UPDATED_CO_BAO_HIEM);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByCoBaoHiemIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where coBaoHiem is not null
        defaultChiDinhDichVuKhamShouldBeFound("coBaoHiem.specified=true");

        // Get all the chiDinhDichVuKhamList where coBaoHiem is null
        defaultChiDinhDichVuKhamShouldNotBeFound("coBaoHiem.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaBhyt equals to DEFAULT_GIA_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaBhyt.equals=" + DEFAULT_GIA_BHYT);

        // Get all the chiDinhDichVuKhamList where giaBhyt equals to UPDATED_GIA_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaBhyt.equals=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaBhyt not equals to DEFAULT_GIA_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaBhyt.notEquals=" + DEFAULT_GIA_BHYT);

        // Get all the chiDinhDichVuKhamList where giaBhyt not equals to UPDATED_GIA_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaBhyt.notEquals=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaBhyt in DEFAULT_GIA_BHYT or UPDATED_GIA_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaBhyt.in=" + DEFAULT_GIA_BHYT + "," + UPDATED_GIA_BHYT);

        // Get all the chiDinhDichVuKhamList where giaBhyt equals to UPDATED_GIA_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaBhyt.in=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaBhyt is not null
        defaultChiDinhDichVuKhamShouldBeFound("giaBhyt.specified=true");

        // Get all the chiDinhDichVuKhamList where giaBhyt is null
        defaultChiDinhDichVuKhamShouldNotBeFound("giaBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaBhyt is greater than or equal to DEFAULT_GIA_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaBhyt.greaterThanOrEqual=" + DEFAULT_GIA_BHYT);

        // Get all the chiDinhDichVuKhamList where giaBhyt is greater than or equal to UPDATED_GIA_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaBhyt.greaterThanOrEqual=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaBhyt is less than or equal to DEFAULT_GIA_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaBhyt.lessThanOrEqual=" + DEFAULT_GIA_BHYT);

        // Get all the chiDinhDichVuKhamList where giaBhyt is less than or equal to SMALLER_GIA_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaBhyt.lessThanOrEqual=" + SMALLER_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaBhyt is less than DEFAULT_GIA_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaBhyt.lessThan=" + DEFAULT_GIA_BHYT);

        // Get all the chiDinhDichVuKhamList where giaBhyt is less than UPDATED_GIA_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaBhyt.lessThan=" + UPDATED_GIA_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaBhyt is greater than DEFAULT_GIA_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaBhyt.greaterThan=" + DEFAULT_GIA_BHYT);

        // Get all the chiDinhDichVuKhamList where giaBhyt is greater than SMALLER_GIA_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaBhyt.greaterThan=" + SMALLER_GIA_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaKhongBhytIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt equals to DEFAULT_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaKhongBhyt.equals=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt equals to UPDATED_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaKhongBhyt.equals=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaKhongBhytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt not equals to DEFAULT_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaKhongBhyt.notEquals=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt not equals to UPDATED_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaKhongBhyt.notEquals=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaKhongBhytIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt in DEFAULT_GIA_KHONG_BHYT or UPDATED_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaKhongBhyt.in=" + DEFAULT_GIA_KHONG_BHYT + "," + UPDATED_GIA_KHONG_BHYT);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt equals to UPDATED_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaKhongBhyt.in=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaKhongBhytIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt is not null
        defaultChiDinhDichVuKhamShouldBeFound("giaKhongBhyt.specified=true");

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt is null
        defaultChiDinhDichVuKhamShouldNotBeFound("giaKhongBhyt.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaKhongBhytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt is greater than or equal to DEFAULT_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaKhongBhyt.greaterThanOrEqual=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt is greater than or equal to UPDATED_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaKhongBhyt.greaterThanOrEqual=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaKhongBhytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt is less than or equal to DEFAULT_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaKhongBhyt.lessThanOrEqual=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt is less than or equal to SMALLER_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaKhongBhyt.lessThanOrEqual=" + SMALLER_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaKhongBhytIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt is less than DEFAULT_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaKhongBhyt.lessThan=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt is less than UPDATED_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaKhongBhyt.lessThan=" + UPDATED_GIA_KHONG_BHYT);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByGiaKhongBhytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt is greater than DEFAULT_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldNotBeFound("giaKhongBhyt.greaterThan=" + DEFAULT_GIA_KHONG_BHYT);

        // Get all the chiDinhDichVuKhamList where giaKhongBhyt is greater than SMALLER_GIA_KHONG_BHYT
        defaultChiDinhDichVuKhamShouldBeFound("giaKhongBhyt.greaterThan=" + SMALLER_GIA_KHONG_BHYT);
    }


    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByMaDungChungIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where maDungChung equals to DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhDichVuKhamShouldBeFound("maDungChung.equals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhDichVuKhamList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultChiDinhDichVuKhamShouldNotBeFound("maDungChung.equals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByMaDungChungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where maDungChung not equals to DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhDichVuKhamShouldNotBeFound("maDungChung.notEquals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhDichVuKhamList where maDungChung not equals to UPDATED_MA_DUNG_CHUNG
        defaultChiDinhDichVuKhamShouldBeFound("maDungChung.notEquals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByMaDungChungIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where maDungChung in DEFAULT_MA_DUNG_CHUNG or UPDATED_MA_DUNG_CHUNG
        defaultChiDinhDichVuKhamShouldBeFound("maDungChung.in=" + DEFAULT_MA_DUNG_CHUNG + "," + UPDATED_MA_DUNG_CHUNG);

        // Get all the chiDinhDichVuKhamList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultChiDinhDichVuKhamShouldNotBeFound("maDungChung.in=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByMaDungChungIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where maDungChung is not null
        defaultChiDinhDichVuKhamShouldBeFound("maDungChung.specified=true");

        // Get all the chiDinhDichVuKhamList where maDungChung is null
        defaultChiDinhDichVuKhamShouldNotBeFound("maDungChung.specified=false");
    }
                @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByMaDungChungContainsSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where maDungChung contains DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhDichVuKhamShouldBeFound("maDungChung.contains=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhDichVuKhamList where maDungChung contains UPDATED_MA_DUNG_CHUNG
        defaultChiDinhDichVuKhamShouldNotBeFound("maDungChung.contains=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByMaDungChungNotContainsSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where maDungChung does not contain DEFAULT_MA_DUNG_CHUNG
        defaultChiDinhDichVuKhamShouldNotBeFound("maDungChung.doesNotContain=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the chiDinhDichVuKhamList where maDungChung does not contain UPDATED_MA_DUNG_CHUNG
        defaultChiDinhDichVuKhamShouldBeFound("maDungChung.doesNotContain=" + UPDATED_MA_DUNG_CHUNG);
    }


    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDichVuYeuCauIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where dichVuYeuCau equals to DEFAULT_DICH_VU_YEU_CAU
        defaultChiDinhDichVuKhamShouldBeFound("dichVuYeuCau.equals=" + DEFAULT_DICH_VU_YEU_CAU);

        // Get all the chiDinhDichVuKhamList where dichVuYeuCau equals to UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhDichVuKhamShouldNotBeFound("dichVuYeuCau.equals=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDichVuYeuCauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where dichVuYeuCau not equals to DEFAULT_DICH_VU_YEU_CAU
        defaultChiDinhDichVuKhamShouldNotBeFound("dichVuYeuCau.notEquals=" + DEFAULT_DICH_VU_YEU_CAU);

        // Get all the chiDinhDichVuKhamList where dichVuYeuCau not equals to UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhDichVuKhamShouldBeFound("dichVuYeuCau.notEquals=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDichVuYeuCauIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where dichVuYeuCau in DEFAULT_DICH_VU_YEU_CAU or UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhDichVuKhamShouldBeFound("dichVuYeuCau.in=" + DEFAULT_DICH_VU_YEU_CAU + "," + UPDATED_DICH_VU_YEU_CAU);

        // Get all the chiDinhDichVuKhamList where dichVuYeuCau equals to UPDATED_DICH_VU_YEU_CAU
        defaultChiDinhDichVuKhamShouldNotBeFound("dichVuYeuCau.in=" + UPDATED_DICH_VU_YEU_CAU);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDichVuYeuCauIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where dichVuYeuCau is not null
        defaultChiDinhDichVuKhamShouldBeFound("dichVuYeuCau.specified=true");

        // Get all the chiDinhDichVuKhamList where dichVuYeuCau is null
        defaultChiDinhDichVuKhamShouldNotBeFound("dichVuYeuCau.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByNamIsEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where nam equals to DEFAULT_NAM
        defaultChiDinhDichVuKhamShouldBeFound("nam.equals=" + DEFAULT_NAM);

        // Get all the chiDinhDichVuKhamList where nam equals to UPDATED_NAM
        defaultChiDinhDichVuKhamShouldNotBeFound("nam.equals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where nam not equals to DEFAULT_NAM
        defaultChiDinhDichVuKhamShouldNotBeFound("nam.notEquals=" + DEFAULT_NAM);

        // Get all the chiDinhDichVuKhamList where nam not equals to UPDATED_NAM
        defaultChiDinhDichVuKhamShouldBeFound("nam.notEquals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByNamIsInShouldWork() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where nam in DEFAULT_NAM or UPDATED_NAM
        defaultChiDinhDichVuKhamShouldBeFound("nam.in=" + DEFAULT_NAM + "," + UPDATED_NAM);

        // Get all the chiDinhDichVuKhamList where nam equals to UPDATED_NAM
        defaultChiDinhDichVuKhamShouldNotBeFound("nam.in=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where nam is not null
        defaultChiDinhDichVuKhamShouldBeFound("nam.specified=true");

        // Get all the chiDinhDichVuKhamList where nam is null
        defaultChiDinhDichVuKhamShouldNotBeFound("nam.specified=false");
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where nam is greater than or equal to DEFAULT_NAM
        defaultChiDinhDichVuKhamShouldBeFound("nam.greaterThanOrEqual=" + DEFAULT_NAM);

        // Get all the chiDinhDichVuKhamList where nam is greater than or equal to UPDATED_NAM
        defaultChiDinhDichVuKhamShouldNotBeFound("nam.greaterThanOrEqual=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where nam is less than or equal to DEFAULT_NAM
        defaultChiDinhDichVuKhamShouldBeFound("nam.lessThanOrEqual=" + DEFAULT_NAM);

        // Get all the chiDinhDichVuKhamList where nam is less than or equal to SMALLER_NAM
        defaultChiDinhDichVuKhamShouldNotBeFound("nam.lessThanOrEqual=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByNamIsLessThanSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where nam is less than DEFAULT_NAM
        defaultChiDinhDichVuKhamShouldNotBeFound("nam.lessThan=" + DEFAULT_NAM);

        // Get all the chiDinhDichVuKhamList where nam is less than UPDATED_NAM
        defaultChiDinhDichVuKhamShouldBeFound("nam.lessThan=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        // Get all the chiDinhDichVuKhamList where nam is greater than DEFAULT_NAM
        defaultChiDinhDichVuKhamShouldNotBeFound("nam.greaterThan=" + DEFAULT_NAM);

        // Get all the chiDinhDichVuKhamList where nam is greater than SMALLER_NAM
        defaultChiDinhDichVuKhamShouldBeFound("nam.greaterThan=" + SMALLER_NAM);
    }


    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByTtkbIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThongTinKhamBenh ttkb = chiDinhDichVuKham.getTtkb();
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);
        Long ttkbId = ttkb.getId();

        // Get all the chiDinhDichVuKhamList where ttkb equals to ttkbId
        defaultChiDinhDichVuKhamShouldBeFound("ttkbId.equals=" + ttkbId);

        // Get all the chiDinhDichVuKhamList where ttkb equals to ttkbId + 1
        defaultChiDinhDichVuKhamShouldNotBeFound("ttkbId.equals=" + (ttkbId + 1));
    }


    @Test
    @Transactional
    public void getAllChiDinhDichVuKhamsByDichVuKhamIsEqualToSomething() throws Exception {
        // Get already existing entity
        DichVuKham dichVuKham = chiDinhDichVuKham.getDichVuKham();
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);
        Long dichVuKhamId = dichVuKham.getId();

        // Get all the chiDinhDichVuKhamList where dichVuKham equals to dichVuKhamId
        defaultChiDinhDichVuKhamShouldBeFound("dichVuKhamId.equals=" + dichVuKhamId);

        // Get all the chiDinhDichVuKhamList where dichVuKham equals to dichVuKhamId + 1
        defaultChiDinhDichVuKhamShouldNotBeFound("dichVuKhamId.equals=" + (dichVuKhamId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultChiDinhDichVuKhamShouldBeFound(String filter) throws Exception {
        restChiDinhDichVuKhamMockMvc.perform(get("/api/chi-dinh-dich-vu-khams?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiDinhDichVuKham.getId().intValue())))
            .andExpect(jsonPath("$.[*].congKhamBanDau").value(hasItem(DEFAULT_CONG_KHAM_BAN_DAU.booleanValue())))
            .andExpect(jsonPath("$.[*].daThanhToan").value(hasItem(DEFAULT_DA_THANH_TOAN.booleanValue())))
            .andExpect(jsonPath("$.[*].donGia").value(hasItem(DEFAULT_DON_GIA.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].thoiGianChiDinh").value(hasItem(DEFAULT_THOI_GIAN_CHI_DINH.toString())))
            .andExpect(jsonPath("$.[*].tyLeThanhToan").value(hasItem(DEFAULT_TY_LE_THANH_TOAN.intValue())))
            .andExpect(jsonPath("$.[*].coBaoHiem").value(hasItem(DEFAULT_CO_BAO_HIEM.booleanValue())))
            .andExpect(jsonPath("$.[*].giaBhyt").value(hasItem(DEFAULT_GIA_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].giaKhongBhyt").value(hasItem(DEFAULT_GIA_KHONG_BHYT.intValue())))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)))
            .andExpect(jsonPath("$.[*].dichVuYeuCau").value(hasItem(DEFAULT_DICH_VU_YEU_CAU.booleanValue())))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));

        // Check, that the count call also returns 1
        restChiDinhDichVuKhamMockMvc.perform(get("/api/chi-dinh-dich-vu-khams/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultChiDinhDichVuKhamShouldNotBeFound(String filter) throws Exception {
        restChiDinhDichVuKhamMockMvc.perform(get("/api/chi-dinh-dich-vu-khams?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restChiDinhDichVuKhamMockMvc.perform(get("/api/chi-dinh-dich-vu-khams/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingChiDinhDichVuKham() throws Exception {
        // Get the chiDinhDichVuKham
        restChiDinhDichVuKhamMockMvc.perform(get("/api/chi-dinh-dich-vu-khams/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChiDinhDichVuKham() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        int databaseSizeBeforeUpdate = chiDinhDichVuKhamRepository.findAll().size();

        // Update the chiDinhDichVuKham
        ChiDinhDichVuKham updatedChiDinhDichVuKham = chiDinhDichVuKhamRepository.findById(chiDinhDichVuKham.getId()).get();
        // Disconnect from session so that the updates on updatedChiDinhDichVuKham are not directly saved in db
        em.detach(updatedChiDinhDichVuKham);
        updatedChiDinhDichVuKham
            .congKhamBanDau(UPDATED_CONG_KHAM_BAN_DAU)
            .daThanhToan(UPDATED_DA_THANH_TOAN)
            .donGia(UPDATED_DON_GIA)
            .ten(UPDATED_TEN)
            .thoiGianChiDinh(UPDATED_THOI_GIAN_CHI_DINH)
            .tyLeThanhToan(UPDATED_TY_LE_THANH_TOAN)
            .coBaoHiem(UPDATED_CO_BAO_HIEM)
            .giaBhyt(UPDATED_GIA_BHYT)
            .giaKhongBhyt(UPDATED_GIA_KHONG_BHYT)
            .maDungChung(UPDATED_MA_DUNG_CHUNG)
            .dichVuYeuCau(UPDATED_DICH_VU_YEU_CAU)
            .nam(UPDATED_NAM);
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(updatedChiDinhDichVuKham);

        restChiDinhDichVuKhamMockMvc.perform(put("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isOk());

        // Validate the ChiDinhDichVuKham in the database
        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeUpdate);
        ChiDinhDichVuKham testChiDinhDichVuKham = chiDinhDichVuKhamList.get(chiDinhDichVuKhamList.size() - 1);
        assertThat(testChiDinhDichVuKham.isCongKhamBanDau()).isEqualTo(UPDATED_CONG_KHAM_BAN_DAU);
        assertThat(testChiDinhDichVuKham.isDaThanhToan()).isEqualTo(UPDATED_DA_THANH_TOAN);
        assertThat(testChiDinhDichVuKham.getDonGia()).isEqualTo(UPDATED_DON_GIA);
        assertThat(testChiDinhDichVuKham.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testChiDinhDichVuKham.getThoiGianChiDinh()).isEqualTo(UPDATED_THOI_GIAN_CHI_DINH);
        assertThat(testChiDinhDichVuKham.getTyLeThanhToan()).isEqualTo(UPDATED_TY_LE_THANH_TOAN);
        assertThat(testChiDinhDichVuKham.isCoBaoHiem()).isEqualTo(UPDATED_CO_BAO_HIEM);
        assertThat(testChiDinhDichVuKham.getGiaBhyt()).isEqualTo(UPDATED_GIA_BHYT);
        assertThat(testChiDinhDichVuKham.getGiaKhongBhyt()).isEqualTo(UPDATED_GIA_KHONG_BHYT);
        assertThat(testChiDinhDichVuKham.getMaDungChung()).isEqualTo(UPDATED_MA_DUNG_CHUNG);
        assertThat(testChiDinhDichVuKham.isDichVuYeuCau()).isEqualTo(UPDATED_DICH_VU_YEU_CAU);
        assertThat(testChiDinhDichVuKham.getNam()).isEqualTo(UPDATED_NAM);
    }

    @Test
    @Transactional
    public void updateNonExistingChiDinhDichVuKham() throws Exception {
        int databaseSizeBeforeUpdate = chiDinhDichVuKhamRepository.findAll().size();

        // Create the ChiDinhDichVuKham
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO = chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKham);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChiDinhDichVuKhamMockMvc.perform(put("/api/chi-dinh-dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chiDinhDichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChiDinhDichVuKham in the database
        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChiDinhDichVuKham() throws Exception {
        // Initialize the database
        chiDinhDichVuKhamRepository.saveAndFlush(chiDinhDichVuKham);

        int databaseSizeBeforeDelete = chiDinhDichVuKhamRepository.findAll().size();

        // Delete the chiDinhDichVuKham
        restChiDinhDichVuKhamMockMvc.perform(delete("/api/chi-dinh-dich-vu-khams/{id}", chiDinhDichVuKham.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChiDinhDichVuKham> chiDinhDichVuKhamList = chiDinhDichVuKhamRepository.findAll();
        assertThat(chiDinhDichVuKhamList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
