package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.NgheNghiep;
import vn.vnpt.repository.NgheNghiepRepository;
import vn.vnpt.service.NgheNghiepService;
import vn.vnpt.service.dto.NgheNghiepDTO;
import vn.vnpt.service.mapper.NgheNghiepMapper;
import vn.vnpt.service.dto.NgheNghiepCriteria;
import vn.vnpt.service.NgheNghiepQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link NgheNghiepResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class NgheNghiepResourceIT {

    private static final Integer DEFAULT_MA_4069 = 1;
    private static final Integer UPDATED_MA_4069 = 2;
    private static final Integer SMALLER_MA_4069 = 1 - 1;

    private static final String DEFAULT_TEN_4069 = "AAAAAAAAAA";
    private static final String UPDATED_TEN_4069 = "BBBBBBBBBB";

    @Autowired
    private NgheNghiepRepository ngheNghiepRepository;

    @Autowired
    private NgheNghiepMapper ngheNghiepMapper;

    @Autowired
    private NgheNghiepService ngheNghiepService;

    @Autowired
    private NgheNghiepQueryService ngheNghiepQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNgheNghiepMockMvc;

    private NgheNghiep ngheNghiep;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NgheNghiep createEntity(EntityManager em) {
        NgheNghiep ngheNghiep = new NgheNghiep()
            .ma4069(DEFAULT_MA_4069)
            .ten4069(DEFAULT_TEN_4069);
        return ngheNghiep;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NgheNghiep createUpdatedEntity(EntityManager em) {
        NgheNghiep ngheNghiep = new NgheNghiep()
            .ma4069(UPDATED_MA_4069)
            .ten4069(UPDATED_TEN_4069);
        return ngheNghiep;
    }

    @BeforeEach
    public void initTest() {
        ngheNghiep = createEntity(em);
    }

    @Test
    @Transactional
    public void createNgheNghiep() throws Exception {
        int databaseSizeBeforeCreate = ngheNghiepRepository.findAll().size();

        // Create the NgheNghiep
        NgheNghiepDTO ngheNghiepDTO = ngheNghiepMapper.toDto(ngheNghiep);
        restNgheNghiepMockMvc.perform(post("/api/nghe-nghieps").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ngheNghiepDTO)))
            .andExpect(status().isCreated());

        // Validate the NgheNghiep in the database
        List<NgheNghiep> ngheNghiepList = ngheNghiepRepository.findAll();
        assertThat(ngheNghiepList).hasSize(databaseSizeBeforeCreate + 1);
        NgheNghiep testNgheNghiep = ngheNghiepList.get(ngheNghiepList.size() - 1);
        assertThat(testNgheNghiep.getMa4069()).isEqualTo(DEFAULT_MA_4069);
        assertThat(testNgheNghiep.getTen4069()).isEqualTo(DEFAULT_TEN_4069);
    }

    @Test
    @Transactional
    public void createNgheNghiepWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ngheNghiepRepository.findAll().size();

        // Create the NgheNghiep with an existing ID
        ngheNghiep.setId(1L);
        NgheNghiepDTO ngheNghiepDTO = ngheNghiepMapper.toDto(ngheNghiep);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNgheNghiepMockMvc.perform(post("/api/nghe-nghieps").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ngheNghiepDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NgheNghiep in the database
        List<NgheNghiep> ngheNghiepList = ngheNghiepRepository.findAll();
        assertThat(ngheNghiepList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMa4069IsRequired() throws Exception {
        int databaseSizeBeforeTest = ngheNghiepRepository.findAll().size();
        // set the field null
        ngheNghiep.setMa4069(null);

        // Create the NgheNghiep, which fails.
        NgheNghiepDTO ngheNghiepDTO = ngheNghiepMapper.toDto(ngheNghiep);

        restNgheNghiepMockMvc.perform(post("/api/nghe-nghieps").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ngheNghiepDTO)))
            .andExpect(status().isBadRequest());

        List<NgheNghiep> ngheNghiepList = ngheNghiepRepository.findAll();
        assertThat(ngheNghiepList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTen4069IsRequired() throws Exception {
        int databaseSizeBeforeTest = ngheNghiepRepository.findAll().size();
        // set the field null
        ngheNghiep.setTen4069(null);

        // Create the NgheNghiep, which fails.
        NgheNghiepDTO ngheNghiepDTO = ngheNghiepMapper.toDto(ngheNghiep);

        restNgheNghiepMockMvc.perform(post("/api/nghe-nghieps").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ngheNghiepDTO)))
            .andExpect(status().isBadRequest());

        List<NgheNghiep> ngheNghiepList = ngheNghiepRepository.findAll();
        assertThat(ngheNghiepList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNgheNghieps() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList
        restNgheNghiepMockMvc.perform(get("/api/nghe-nghieps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ngheNghiep.getId().intValue())))
            .andExpect(jsonPath("$.[*].ma4069").value(hasItem(DEFAULT_MA_4069)))
            .andExpect(jsonPath("$.[*].ten4069").value(hasItem(DEFAULT_TEN_4069)));
    }
    
    @Test
    @Transactional
    public void getNgheNghiep() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get the ngheNghiep
        restNgheNghiepMockMvc.perform(get("/api/nghe-nghieps/{id}", ngheNghiep.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ngheNghiep.getId().intValue()))
            .andExpect(jsonPath("$.ma4069").value(DEFAULT_MA_4069))
            .andExpect(jsonPath("$.ten4069").value(DEFAULT_TEN_4069));
    }


    @Test
    @Transactional
    public void getNgheNghiepsByIdFiltering() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        Long id = ngheNghiep.getId();

        defaultNgheNghiepShouldBeFound("id.equals=" + id);
        defaultNgheNghiepShouldNotBeFound("id.notEquals=" + id);

        defaultNgheNghiepShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultNgheNghiepShouldNotBeFound("id.greaterThan=" + id);

        defaultNgheNghiepShouldBeFound("id.lessThanOrEqual=" + id);
        defaultNgheNghiepShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllNgheNghiepsByMa4069IsEqualToSomething() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ma4069 equals to DEFAULT_MA_4069
        defaultNgheNghiepShouldBeFound("ma4069.equals=" + DEFAULT_MA_4069);

        // Get all the ngheNghiepList where ma4069 equals to UPDATED_MA_4069
        defaultNgheNghiepShouldNotBeFound("ma4069.equals=" + UPDATED_MA_4069);
    }

    @Test
    @Transactional
    public void getAllNgheNghiepsByMa4069IsNotEqualToSomething() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ma4069 not equals to DEFAULT_MA_4069
        defaultNgheNghiepShouldNotBeFound("ma4069.notEquals=" + DEFAULT_MA_4069);

        // Get all the ngheNghiepList where ma4069 not equals to UPDATED_MA_4069
        defaultNgheNghiepShouldBeFound("ma4069.notEquals=" + UPDATED_MA_4069);
    }

    @Test
    @Transactional
    public void getAllNgheNghiepsByMa4069IsInShouldWork() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ma4069 in DEFAULT_MA_4069 or UPDATED_MA_4069
        defaultNgheNghiepShouldBeFound("ma4069.in=" + DEFAULT_MA_4069 + "," + UPDATED_MA_4069);

        // Get all the ngheNghiepList where ma4069 equals to UPDATED_MA_4069
        defaultNgheNghiepShouldNotBeFound("ma4069.in=" + UPDATED_MA_4069);
    }

    @Test
    @Transactional
    public void getAllNgheNghiepsByMa4069IsNullOrNotNull() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ma4069 is not null
        defaultNgheNghiepShouldBeFound("ma4069.specified=true");

        // Get all the ngheNghiepList where ma4069 is null
        defaultNgheNghiepShouldNotBeFound("ma4069.specified=false");
    }

    @Test
    @Transactional
    public void getAllNgheNghiepsByMa4069IsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ma4069 is greater than or equal to DEFAULT_MA_4069
        defaultNgheNghiepShouldBeFound("ma4069.greaterThanOrEqual=" + DEFAULT_MA_4069);

        // Get all the ngheNghiepList where ma4069 is greater than or equal to UPDATED_MA_4069
        defaultNgheNghiepShouldNotBeFound("ma4069.greaterThanOrEqual=" + UPDATED_MA_4069);
    }

    @Test
    @Transactional
    public void getAllNgheNghiepsByMa4069IsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ma4069 is less than or equal to DEFAULT_MA_4069
        defaultNgheNghiepShouldBeFound("ma4069.lessThanOrEqual=" + DEFAULT_MA_4069);

        // Get all the ngheNghiepList where ma4069 is less than or equal to SMALLER_MA_4069
        defaultNgheNghiepShouldNotBeFound("ma4069.lessThanOrEqual=" + SMALLER_MA_4069);
    }

    @Test
    @Transactional
    public void getAllNgheNghiepsByMa4069IsLessThanSomething() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ma4069 is less than DEFAULT_MA_4069
        defaultNgheNghiepShouldNotBeFound("ma4069.lessThan=" + DEFAULT_MA_4069);

        // Get all the ngheNghiepList where ma4069 is less than UPDATED_MA_4069
        defaultNgheNghiepShouldBeFound("ma4069.lessThan=" + UPDATED_MA_4069);
    }

    @Test
    @Transactional
    public void getAllNgheNghiepsByMa4069IsGreaterThanSomething() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ma4069 is greater than DEFAULT_MA_4069
        defaultNgheNghiepShouldNotBeFound("ma4069.greaterThan=" + DEFAULT_MA_4069);

        // Get all the ngheNghiepList where ma4069 is greater than SMALLER_MA_4069
        defaultNgheNghiepShouldBeFound("ma4069.greaterThan=" + SMALLER_MA_4069);
    }


    @Test
    @Transactional
    public void getAllNgheNghiepsByTen4069IsEqualToSomething() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ten4069 equals to DEFAULT_TEN_4069
        defaultNgheNghiepShouldBeFound("ten4069.equals=" + DEFAULT_TEN_4069);

        // Get all the ngheNghiepList where ten4069 equals to UPDATED_TEN_4069
        defaultNgheNghiepShouldNotBeFound("ten4069.equals=" + UPDATED_TEN_4069);
    }

    @Test
    @Transactional
    public void getAllNgheNghiepsByTen4069IsNotEqualToSomething() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ten4069 not equals to DEFAULT_TEN_4069
        defaultNgheNghiepShouldNotBeFound("ten4069.notEquals=" + DEFAULT_TEN_4069);

        // Get all the ngheNghiepList where ten4069 not equals to UPDATED_TEN_4069
        defaultNgheNghiepShouldBeFound("ten4069.notEquals=" + UPDATED_TEN_4069);
    }

    @Test
    @Transactional
    public void getAllNgheNghiepsByTen4069IsInShouldWork() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ten4069 in DEFAULT_TEN_4069 or UPDATED_TEN_4069
        defaultNgheNghiepShouldBeFound("ten4069.in=" + DEFAULT_TEN_4069 + "," + UPDATED_TEN_4069);

        // Get all the ngheNghiepList where ten4069 equals to UPDATED_TEN_4069
        defaultNgheNghiepShouldNotBeFound("ten4069.in=" + UPDATED_TEN_4069);
    }

    @Test
    @Transactional
    public void getAllNgheNghiepsByTen4069IsNullOrNotNull() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ten4069 is not null
        defaultNgheNghiepShouldBeFound("ten4069.specified=true");

        // Get all the ngheNghiepList where ten4069 is null
        defaultNgheNghiepShouldNotBeFound("ten4069.specified=false");
    }
                @Test
    @Transactional
    public void getAllNgheNghiepsByTen4069ContainsSomething() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ten4069 contains DEFAULT_TEN_4069
        defaultNgheNghiepShouldBeFound("ten4069.contains=" + DEFAULT_TEN_4069);

        // Get all the ngheNghiepList where ten4069 contains UPDATED_TEN_4069
        defaultNgheNghiepShouldNotBeFound("ten4069.contains=" + UPDATED_TEN_4069);
    }

    @Test
    @Transactional
    public void getAllNgheNghiepsByTen4069NotContainsSomething() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        // Get all the ngheNghiepList where ten4069 does not contain DEFAULT_TEN_4069
        defaultNgheNghiepShouldNotBeFound("ten4069.doesNotContain=" + DEFAULT_TEN_4069);

        // Get all the ngheNghiepList where ten4069 does not contain UPDATED_TEN_4069
        defaultNgheNghiepShouldBeFound("ten4069.doesNotContain=" + UPDATED_TEN_4069);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNgheNghiepShouldBeFound(String filter) throws Exception {
        restNgheNghiepMockMvc.perform(get("/api/nghe-nghieps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ngheNghiep.getId().intValue())))
            .andExpect(jsonPath("$.[*].ma4069").value(hasItem(DEFAULT_MA_4069)))
            .andExpect(jsonPath("$.[*].ten4069").value(hasItem(DEFAULT_TEN_4069)));

        // Check, that the count call also returns 1
        restNgheNghiepMockMvc.perform(get("/api/nghe-nghieps/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNgheNghiepShouldNotBeFound(String filter) throws Exception {
        restNgheNghiepMockMvc.perform(get("/api/nghe-nghieps?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNgheNghiepMockMvc.perform(get("/api/nghe-nghieps/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingNgheNghiep() throws Exception {
        // Get the ngheNghiep
        restNgheNghiepMockMvc.perform(get("/api/nghe-nghieps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNgheNghiep() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        int databaseSizeBeforeUpdate = ngheNghiepRepository.findAll().size();

        // Update the ngheNghiep
        NgheNghiep updatedNgheNghiep = ngheNghiepRepository.findById(ngheNghiep.getId()).get();
        // Disconnect from session so that the updates on updatedNgheNghiep are not directly saved in db
        em.detach(updatedNgheNghiep);
        updatedNgheNghiep
            .ma4069(UPDATED_MA_4069)
            .ten4069(UPDATED_TEN_4069);
        NgheNghiepDTO ngheNghiepDTO = ngheNghiepMapper.toDto(updatedNgheNghiep);

        restNgheNghiepMockMvc.perform(put("/api/nghe-nghieps").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ngheNghiepDTO)))
            .andExpect(status().isOk());

        // Validate the NgheNghiep in the database
        List<NgheNghiep> ngheNghiepList = ngheNghiepRepository.findAll();
        assertThat(ngheNghiepList).hasSize(databaseSizeBeforeUpdate);
        NgheNghiep testNgheNghiep = ngheNghiepList.get(ngheNghiepList.size() - 1);
        assertThat(testNgheNghiep.getMa4069()).isEqualTo(UPDATED_MA_4069);
        assertThat(testNgheNghiep.getTen4069()).isEqualTo(UPDATED_TEN_4069);
    }

    @Test
    @Transactional
    public void updateNonExistingNgheNghiep() throws Exception {
        int databaseSizeBeforeUpdate = ngheNghiepRepository.findAll().size();

        // Create the NgheNghiep
        NgheNghiepDTO ngheNghiepDTO = ngheNghiepMapper.toDto(ngheNghiep);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNgheNghiepMockMvc.perform(put("/api/nghe-nghieps").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ngheNghiepDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NgheNghiep in the database
        List<NgheNghiep> ngheNghiepList = ngheNghiepRepository.findAll();
        assertThat(ngheNghiepList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNgheNghiep() throws Exception {
        // Initialize the database
        ngheNghiepRepository.saveAndFlush(ngheNghiep);

        int databaseSizeBeforeDelete = ngheNghiepRepository.findAll().size();

        // Delete the ngheNghiep
        restNgheNghiepMockMvc.perform(delete("/api/nghe-nghieps/{id}", ngheNghiep.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NgheNghiep> ngheNghiepList = ngheNghiepRepository.findAll();
        assertThat(ngheNghiepList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
