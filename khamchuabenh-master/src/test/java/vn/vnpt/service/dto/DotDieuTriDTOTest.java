package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class DotDieuTriDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DotDieuTriDTO.class);
        DotDieuTriDTO dotDieuTriDTO1 = new DotDieuTriDTO();
        dotDieuTriDTO1.setBakbBenhNhanId(1L);
        dotDieuTriDTO1.setBakbDonViId(1L);
        DotDieuTriDTO dotDieuTriDTO2 = new DotDieuTriDTO();
        assertThat(dotDieuTriDTO1).isNotEqualTo(dotDieuTriDTO2);
        dotDieuTriDTO2.setBakbBenhNhanId(dotDieuTriDTO1.getBakbBenhNhanId());
        dotDieuTriDTO2.setBakbDonViId(dotDieuTriDTO1.getBakbDonViId());
        assertThat(dotDieuTriDTO1).isEqualTo(dotDieuTriDTO2);
        dotDieuTriDTO2.setBakbBenhNhanId(2L);
        dotDieuTriDTO2.setBakbDonViId(2L);
        assertThat(dotDieuTriDTO1).isNotEqualTo(dotDieuTriDTO2);
        dotDieuTriDTO1.setBakbDonViId(null);
        assertThat(dotDieuTriDTO1).isNotEqualTo(dotDieuTriDTO2);
    }
}
