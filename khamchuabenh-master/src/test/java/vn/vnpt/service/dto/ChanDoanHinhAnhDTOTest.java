package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChanDoanHinhAnhDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChanDoanHinhAnhDTO.class);
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO1 = new ChanDoanHinhAnhDTO();
        chanDoanHinhAnhDTO1.setId(1L);
        ChanDoanHinhAnhDTO chanDoanHinhAnhDTO2 = new ChanDoanHinhAnhDTO();
        assertThat(chanDoanHinhAnhDTO1).isNotEqualTo(chanDoanHinhAnhDTO2);
        chanDoanHinhAnhDTO2.setId(chanDoanHinhAnhDTO1.getId());
        assertThat(chanDoanHinhAnhDTO1).isEqualTo(chanDoanHinhAnhDTO2);
        chanDoanHinhAnhDTO2.setId(2L);
        assertThat(chanDoanHinhAnhDTO1).isNotEqualTo(chanDoanHinhAnhDTO2);
        chanDoanHinhAnhDTO1.setId(null);
        assertThat(chanDoanHinhAnhDTO1).isNotEqualTo(chanDoanHinhAnhDTO2);
    }
}
