package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class BenhLyDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BenhLyDTO.class);
        BenhLyDTO benhLyDTO1 = new BenhLyDTO();
        benhLyDTO1.setId(1L);
        BenhLyDTO benhLyDTO2 = new BenhLyDTO();
        assertThat(benhLyDTO1).isNotEqualTo(benhLyDTO2);
        benhLyDTO2.setId(benhLyDTO1.getId());
        assertThat(benhLyDTO1).isEqualTo(benhLyDTO2);
        benhLyDTO2.setId(2L);
        assertThat(benhLyDTO1).isNotEqualTo(benhLyDTO2);
        benhLyDTO1.setId(null);
        assertThat(benhLyDTO1).isNotEqualTo(benhLyDTO2);
    }
}
