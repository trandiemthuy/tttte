package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChiDinhXetNghiemDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiDinhXetNghiemDTO.class);
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO1 = new ChiDinhXetNghiemDTO();
        chiDinhXetNghiemDTO1.setId(1L);
        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO2 = new ChiDinhXetNghiemDTO();
        assertThat(chiDinhXetNghiemDTO1).isNotEqualTo(chiDinhXetNghiemDTO2);
        chiDinhXetNghiemDTO2.setId(chiDinhXetNghiemDTO1.getId());
        assertThat(chiDinhXetNghiemDTO1).isEqualTo(chiDinhXetNghiemDTO2);
        chiDinhXetNghiemDTO2.setId(2L);
        assertThat(chiDinhXetNghiemDTO1).isNotEqualTo(chiDinhXetNghiemDTO2);
        chiDinhXetNghiemDTO1.setId(null);
        assertThat(chiDinhXetNghiemDTO1).isNotEqualTo(chiDinhXetNghiemDTO2);
    }
}
