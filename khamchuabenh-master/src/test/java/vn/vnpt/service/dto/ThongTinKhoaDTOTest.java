package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ThongTinKhoaDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ThongTinKhoaDTO.class);
        ThongTinKhoaDTO thongTinKhoaDTO1 = new ThongTinKhoaDTO();
        thongTinKhoaDTO1.setId(1L);
        ThongTinKhoaDTO thongTinKhoaDTO2 = new ThongTinKhoaDTO();
        assertThat(thongTinKhoaDTO1).isNotEqualTo(thongTinKhoaDTO2);
        thongTinKhoaDTO2.setId(thongTinKhoaDTO1.getId());
        assertThat(thongTinKhoaDTO1).isEqualTo(thongTinKhoaDTO2);
        thongTinKhoaDTO2.setId(2L);
        assertThat(thongTinKhoaDTO1).isNotEqualTo(thongTinKhoaDTO2);
        thongTinKhoaDTO1.setId(null);
        assertThat(thongTinKhoaDTO1).isNotEqualTo(thongTinKhoaDTO2);
    }
}
