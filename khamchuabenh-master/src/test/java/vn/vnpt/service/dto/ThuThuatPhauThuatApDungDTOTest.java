package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ThuThuatPhauThuatApDungDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ThuThuatPhauThuatApDungDTO.class);
        ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO1 = new ThuThuatPhauThuatApDungDTO();
        thuThuatPhauThuatApDungDTO1.setId(1L);
        ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO2 = new ThuThuatPhauThuatApDungDTO();
        assertThat(thuThuatPhauThuatApDungDTO1).isNotEqualTo(thuThuatPhauThuatApDungDTO2);
        thuThuatPhauThuatApDungDTO2.setId(thuThuatPhauThuatApDungDTO1.getId());
        assertThat(thuThuatPhauThuatApDungDTO1).isEqualTo(thuThuatPhauThuatApDungDTO2);
        thuThuatPhauThuatApDungDTO2.setId(2L);
        assertThat(thuThuatPhauThuatApDungDTO1).isNotEqualTo(thuThuatPhauThuatApDungDTO2);
        thuThuatPhauThuatApDungDTO1.setId(null);
        assertThat(thuThuatPhauThuatApDungDTO1).isNotEqualTo(thuThuatPhauThuatApDungDTO2);
    }
}
