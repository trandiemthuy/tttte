package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChiDinhDichVuKhamDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiDinhDichVuKhamDTO.class);
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO1 = new ChiDinhDichVuKhamDTO();
        chiDinhDichVuKhamDTO1.setId(1L);
        ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO2 = new ChiDinhDichVuKhamDTO();
        assertThat(chiDinhDichVuKhamDTO1).isNotEqualTo(chiDinhDichVuKhamDTO2);
        chiDinhDichVuKhamDTO2.setId(chiDinhDichVuKhamDTO1.getId());
        assertThat(chiDinhDichVuKhamDTO1).isEqualTo(chiDinhDichVuKhamDTO2);
        chiDinhDichVuKhamDTO2.setId(2L);
        assertThat(chiDinhDichVuKhamDTO1).isNotEqualTo(chiDinhDichVuKhamDTO2);
        chiDinhDichVuKhamDTO1.setId(null);
        assertThat(chiDinhDichVuKhamDTO1).isNotEqualTo(chiDinhDichVuKhamDTO2);
    }
}
