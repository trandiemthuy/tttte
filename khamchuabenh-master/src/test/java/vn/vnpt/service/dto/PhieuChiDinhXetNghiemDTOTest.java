package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class PhieuChiDinhXetNghiemDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhieuChiDinhXetNghiemDTO.class);
        PhieuChiDinhXetNghiemDTO phieuChiDinhXetNghiemDTO1 = new PhieuChiDinhXetNghiemDTO();
        phieuChiDinhXetNghiemDTO1.setId(1L);
        PhieuChiDinhXetNghiemDTO phieuChiDinhXetNghiemDTO2 = new PhieuChiDinhXetNghiemDTO();
        assertThat(phieuChiDinhXetNghiemDTO1).isNotEqualTo(phieuChiDinhXetNghiemDTO2);
        phieuChiDinhXetNghiemDTO2.setId(phieuChiDinhXetNghiemDTO1.getId());
        assertThat(phieuChiDinhXetNghiemDTO1).isEqualTo(phieuChiDinhXetNghiemDTO2);
        phieuChiDinhXetNghiemDTO2.setId(2L);
        assertThat(phieuChiDinhXetNghiemDTO1).isNotEqualTo(phieuChiDinhXetNghiemDTO2);
        phieuChiDinhXetNghiemDTO1.setId(null);
        assertThat(phieuChiDinhXetNghiemDTO1).isNotEqualTo(phieuChiDinhXetNghiemDTO2);
    }
}
