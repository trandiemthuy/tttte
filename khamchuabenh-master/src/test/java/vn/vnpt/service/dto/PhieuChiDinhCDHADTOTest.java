package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class PhieuChiDinhCDHADTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhieuChiDinhCDHADTO.class);
        PhieuChiDinhCDHADTO phieuChiDinhCDHADTO1 = new PhieuChiDinhCDHADTO();
        phieuChiDinhCDHADTO1.setId(1L);
        PhieuChiDinhCDHADTO phieuChiDinhCDHADTO2 = new PhieuChiDinhCDHADTO();
        assertThat(phieuChiDinhCDHADTO1).isNotEqualTo(phieuChiDinhCDHADTO2);
        phieuChiDinhCDHADTO2.setId(phieuChiDinhCDHADTO1.getId());
        assertThat(phieuChiDinhCDHADTO1).isEqualTo(phieuChiDinhCDHADTO2);
        phieuChiDinhCDHADTO2.setId(2L);
        assertThat(phieuChiDinhCDHADTO1).isNotEqualTo(phieuChiDinhCDHADTO2);
        phieuChiDinhCDHADTO1.setId(null);
        assertThat(phieuChiDinhCDHADTO1).isNotEqualTo(phieuChiDinhCDHADTO2);
    }
}
