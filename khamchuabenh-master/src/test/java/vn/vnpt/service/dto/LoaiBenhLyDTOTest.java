package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class LoaiBenhLyDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoaiBenhLyDTO.class);
        LoaiBenhLyDTO loaiBenhLyDTO1 = new LoaiBenhLyDTO();
        loaiBenhLyDTO1.setId(1L);
        LoaiBenhLyDTO loaiBenhLyDTO2 = new LoaiBenhLyDTO();
        assertThat(loaiBenhLyDTO1).isNotEqualTo(loaiBenhLyDTO2);
        loaiBenhLyDTO2.setId(loaiBenhLyDTO1.getId());
        assertThat(loaiBenhLyDTO1).isEqualTo(loaiBenhLyDTO2);
        loaiBenhLyDTO2.setId(2L);
        assertThat(loaiBenhLyDTO1).isNotEqualTo(loaiBenhLyDTO2);
        loaiBenhLyDTO1.setId(null);
        assertThat(loaiBenhLyDTO1).isNotEqualTo(loaiBenhLyDTO2);
    }
}
