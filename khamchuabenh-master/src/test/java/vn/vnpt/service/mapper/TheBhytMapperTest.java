package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TheBhytMapperTest {

    private TheBhytMapper theBhytMapper;

    @BeforeEach
    public void setUp() {
        theBhytMapper = new TheBhytMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(theBhytMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(theBhytMapper.fromId(null)).isNull();
    }
}
