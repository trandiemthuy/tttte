package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ChanDoanHinhAnhMapperTest {

    private ChanDoanHinhAnhMapper chanDoanHinhAnhMapper;

    @BeforeEach
    public void setUp() {
        chanDoanHinhAnhMapper = new ChanDoanHinhAnhMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(chanDoanHinhAnhMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(chanDoanHinhAnhMapper.fromId(null)).isNull();
    }
}
