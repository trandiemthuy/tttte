package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class XetNghiemMapperTest {

    private XetNghiemMapper xetNghiemMapper;

    @BeforeEach
    public void setUp() {
        xetNghiemMapper = new XetNghiemMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(xetNghiemMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(xetNghiemMapper.fromId(null)).isNull();
    }
}
