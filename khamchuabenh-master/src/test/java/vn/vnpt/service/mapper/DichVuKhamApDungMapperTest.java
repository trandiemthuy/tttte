package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DichVuKhamApDungMapperTest {

    private DichVuKhamApDungMapper dichVuKhamApDungMapper;

    @BeforeEach
    public void setUp() {
        dichVuKhamApDungMapper = new DichVuKhamApDungMapperImpl();
    }

    @Test
        public void testEntityFromId() {
        assertThat(dichVuKhamApDungMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(dichVuKhamApDungMapper.fromId(null)).isNull();
    }
}
