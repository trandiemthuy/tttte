package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TPhanNhomXetNghiemMapperTest {

    private TPhanNhomXetNghiemMapper tPhanNhomXetNghiemMapper;

    @BeforeEach
    public void setUp() {
        tPhanNhomXetNghiemMapper = new TPhanNhomXetNghiemMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(tPhanNhomXetNghiemMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(tPhanNhomXetNghiemMapper.fromId(null)).isNull();
    }
}
