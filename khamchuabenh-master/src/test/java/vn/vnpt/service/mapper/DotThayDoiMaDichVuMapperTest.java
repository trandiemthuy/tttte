package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DotThayDoiMaDichVuMapperTest {

    private DotThayDoiMaDichVuMapper dotThayDoiMaDichVuMapper;

    @BeforeEach
    public void setUp() {
        dotThayDoiMaDichVuMapper = new DotThayDoiMaDichVuMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(dotThayDoiMaDichVuMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(dotThayDoiMaDichVuMapper.fromId(null)).isNull();
    }
}
