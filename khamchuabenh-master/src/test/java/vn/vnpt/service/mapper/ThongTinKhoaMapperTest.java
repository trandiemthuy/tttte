package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ThongTinKhoaMapperTest {

    private ThongTinKhoaMapper thongTinKhoaMapper;

    @BeforeEach
    public void setUp() {
        thongTinKhoaMapper = new ThongTinKhoaMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(thongTinKhoaMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(thongTinKhoaMapper.fromId(null)).isNull();
    }
}
