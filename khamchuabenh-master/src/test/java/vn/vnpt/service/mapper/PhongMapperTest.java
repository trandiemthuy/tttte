package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PhongMapperTest {

    private PhongMapper phongMapper;

    @BeforeEach
    public void setUp() {
        phongMapper = new PhongMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(phongMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(phongMapper.fromId(null)).isNull();
    }
}
