package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class XetNghiemApDungMapperTest {

    private XetNghiemApDungMapper xetNghiemApDungMapper;

    @BeforeEach
    public void setUp() {
        xetNghiemApDungMapper = new XetNghiemApDungMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(xetNghiemApDungMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(xetNghiemApDungMapper.fromId(null)).isNull();
    }
}
