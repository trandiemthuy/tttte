package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ChucDanhMapperTest {

    private ChucDanhMapper chucDanhMapper;

    @BeforeEach
    public void setUp() {
        chucDanhMapper = new ChucDanhMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(chucDanhMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(chucDanhMapper.fromId(null)).isNull();
    }
}
