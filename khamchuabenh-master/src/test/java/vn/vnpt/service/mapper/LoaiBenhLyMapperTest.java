package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LoaiBenhLyMapperTest {

    private LoaiBenhLyMapper loaiBenhLyMapper;

    @BeforeEach
    public void setUp() {
        loaiBenhLyMapper = new LoaiBenhLyMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(loaiBenhLyMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(loaiBenhLyMapper.fromId(null)).isNull();
    }
}
