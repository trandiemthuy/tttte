package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class NgheNghiepMapperTest {

    private NgheNghiepMapper ngheNghiepMapper;

    @BeforeEach
    public void setUp() {
        ngheNghiepMapper = new NgheNghiepMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(ngheNghiepMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(ngheNghiepMapper.fromId(null)).isNull();
    }
}
