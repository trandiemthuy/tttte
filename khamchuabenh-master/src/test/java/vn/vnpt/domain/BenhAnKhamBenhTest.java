package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class BenhAnKhamBenhTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BenhAnKhamBenh.class);
        BenhAnKhamBenh benhAnKhamBenh1 = new BenhAnKhamBenh();
        benhAnKhamBenh1.setId(new BenhAnKhamBenhId(1L, 1L));
        BenhAnKhamBenh benhAnKhamBenh2 = new BenhAnKhamBenh();
        benhAnKhamBenh2.setId(new BenhAnKhamBenhId(1L, 1L));
        assertThat(benhAnKhamBenh1).isEqualTo(benhAnKhamBenh2);
        benhAnKhamBenh2.setId(new BenhAnKhamBenhId(2L, 2L));
        assertThat(benhAnKhamBenh1).isNotEqualTo(benhAnKhamBenh2);
        benhAnKhamBenh1.setId(null);
        assertThat(benhAnKhamBenh1).isNotEqualTo(benhAnKhamBenh2);
    }
}
