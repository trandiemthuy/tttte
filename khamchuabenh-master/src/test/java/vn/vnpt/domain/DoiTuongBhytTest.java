package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class DoiTuongBhytTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DoiTuongBhyt.class);
        DoiTuongBhyt doiTuongBhyt1 = new DoiTuongBhyt();
        doiTuongBhyt1.setId(1L);
        DoiTuongBhyt doiTuongBhyt2 = new DoiTuongBhyt();
        doiTuongBhyt2.setId(doiTuongBhyt1.getId());
        assertThat(doiTuongBhyt1).isEqualTo(doiTuongBhyt2);
        doiTuongBhyt2.setId(2L);
        assertThat(doiTuongBhyt1).isNotEqualTo(doiTuongBhyt2);
        doiTuongBhyt1.setId(null);
        assertThat(doiTuongBhyt1).isNotEqualTo(doiTuongBhyt2);
    }
}
