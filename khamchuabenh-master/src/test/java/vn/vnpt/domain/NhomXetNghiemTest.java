package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class NhomXetNghiemTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NhomXetNghiem.class);
        NhomXetNghiem nhomXetNghiem1 = new NhomXetNghiem();
        nhomXetNghiem1.setId(1L);
        NhomXetNghiem nhomXetNghiem2 = new NhomXetNghiem();
        nhomXetNghiem2.setId(nhomXetNghiem1.getId());
        assertThat(nhomXetNghiem1).isEqualTo(nhomXetNghiem2);
        nhomXetNghiem2.setId(2L);
        assertThat(nhomXetNghiem1).isNotEqualTo(nhomXetNghiem2);
        nhomXetNghiem1.setId(null);
        assertThat(nhomXetNghiem1).isNotEqualTo(nhomXetNghiem2);
    }
}
