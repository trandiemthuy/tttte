package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class BenhLyTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BenhLy.class);
        BenhLy benhLy1 = new BenhLy();
        benhLy1.setId(1L);
        BenhLy benhLy2 = new BenhLy();
        benhLy2.setId(benhLy1.getId());
        assertThat(benhLy1).isEqualTo(benhLy2);
        benhLy2.setId(2L);
        assertThat(benhLy1).isNotEqualTo(benhLy2);
        benhLy1.setId(null);
        assertThat(benhLy1).isNotEqualTo(benhLy2);
    }
}
