package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChiDinhThuThuatPhauThuatTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiDinhThuThuatPhauThuat.class);
        ChiDinhThuThuatPhauThuat chiDinhThuThuatPhauThuat1 = new ChiDinhThuThuatPhauThuat();
        chiDinhThuThuatPhauThuat1.setId(1L);
        ChiDinhThuThuatPhauThuat chiDinhThuThuatPhauThuat2 = new ChiDinhThuThuatPhauThuat();
        chiDinhThuThuatPhauThuat2.setId(chiDinhThuThuatPhauThuat1.getId());
        assertThat(chiDinhThuThuatPhauThuat1).isEqualTo(chiDinhThuThuatPhauThuat2);
        chiDinhThuThuatPhauThuat2.setId(2L);
        assertThat(chiDinhThuThuatPhauThuat1).isNotEqualTo(chiDinhThuThuatPhauThuat2);
        chiDinhThuThuatPhauThuat1.setId(null);
        assertThat(chiDinhThuThuatPhauThuat1).isNotEqualTo(chiDinhThuThuatPhauThuat2);
    }
}
