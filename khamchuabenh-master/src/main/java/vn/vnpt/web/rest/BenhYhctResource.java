package vn.vnpt.web.rest;

import vn.vnpt.service.BenhYhctService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.BenhYhctDTO;
import vn.vnpt.service.dto.BenhYhctCriteria;
import vn.vnpt.service.BenhYhctQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.BenhYhct}.
 */
@RestController
@RequestMapping("/api")
public class BenhYhctResource {

    private final Logger log = LoggerFactory.getLogger(BenhYhctResource.class);

    private static final String ENTITY_NAME = "khamchuabenhBenhYhct";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BenhYhctService benhYhctService;

    private final BenhYhctQueryService benhYhctQueryService;

    public BenhYhctResource(BenhYhctService benhYhctService, BenhYhctQueryService benhYhctQueryService) {
        this.benhYhctService = benhYhctService;
        this.benhYhctQueryService = benhYhctQueryService;
    }

    /**
     * {@code POST  /benh-yhcts} : Create a new benhYhct.
     *
     * @param benhYhctDTO the benhYhctDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new benhYhctDTO, or with status {@code 400 (Bad Request)} if the benhYhct has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/benh-yhcts")
    public ResponseEntity<BenhYhctDTO> createBenhYhct(@Valid @RequestBody BenhYhctDTO benhYhctDTO) throws URISyntaxException {
        log.debug("REST request to save BenhYhct : {}", benhYhctDTO);
        if (benhYhctDTO.getId() != null) {
            throw new BadRequestAlertException("A new benhYhct cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BenhYhctDTO result = benhYhctService.save(benhYhctDTO);
        return ResponseEntity.created(new URI("/api/benh-yhcts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /benh-yhcts} : Updates an existing benhYhct.
     *
     * @param benhYhctDTO the benhYhctDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated benhYhctDTO,
     * or with status {@code 400 (Bad Request)} if the benhYhctDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the benhYhctDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/benh-yhcts")
    public ResponseEntity<BenhYhctDTO> updateBenhYhct(@Valid @RequestBody BenhYhctDTO benhYhctDTO) throws URISyntaxException {
        log.debug("REST request to update BenhYhct : {}", benhYhctDTO);
        if (benhYhctDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BenhYhctDTO result = benhYhctService.save(benhYhctDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, benhYhctDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /benh-yhcts} : get all the benhYhcts.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of benhYhcts in body.
     */
    @GetMapping("/benh-yhcts")
    public ResponseEntity<List<BenhYhctDTO>> getAllBenhYhcts(BenhYhctCriteria criteria, Pageable pageable) {
        log.debug("REST request to get BenhYhcts by criteria: {}", criteria);
        Page<BenhYhctDTO> page = benhYhctQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /benh-yhcts/count} : count all the benhYhcts.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/benh-yhcts/count")
    public ResponseEntity<Long> countBenhYhcts(BenhYhctCriteria criteria) {
        log.debug("REST request to count BenhYhcts by criteria: {}", criteria);
        return ResponseEntity.ok().body(benhYhctQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /benh-yhcts/:id} : get the "id" benhYhct.
     *
     * @param id the id of the benhYhctDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the benhYhctDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/benh-yhcts/{id}")
    public ResponseEntity<BenhYhctDTO> getBenhYhct(@PathVariable Long id) {
        log.debug("REST request to get BenhYhct : {}", id);
        Optional<BenhYhctDTO> benhYhctDTO = benhYhctService.findOne(id);
        return ResponseUtil.wrapOrNotFound(benhYhctDTO);
    }

    /**
     * {@code DELETE  /benh-yhcts/:id} : delete the "id" benhYhct.
     *
     * @param id the id of the benhYhctDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/benh-yhcts/{id}")
    public ResponseEntity<Void> deleteBenhYhct(@PathVariable Long id) {
        log.debug("REST request to delete BenhYhct : {}", id);
        benhYhctService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
