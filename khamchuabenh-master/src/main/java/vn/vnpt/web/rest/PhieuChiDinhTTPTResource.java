package vn.vnpt.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import vn.vnpt.domain.PhieuChiDinhTTPTId;
import vn.vnpt.domain.PhieuChiDinhXetNghiemId;
import vn.vnpt.service.PhieuChiDinhTTPTService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.PhieuChiDinhTTPTDTO;
import vn.vnpt.service.dto.PhieuChiDinhTTPTCriteria;
import vn.vnpt.service.PhieuChiDinhTTPTQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.PhieuChiDinhTTPT}.
 */
@RestController
@RequestMapping("/api")
public class PhieuChiDinhTTPTResource {

    private final Logger log = LoggerFactory.getLogger(PhieuChiDinhTTPTResource.class);

    private static final String ENTITY_NAME = "khamchuabenhPhieuChiDinhTtpt";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhieuChiDinhTTPTService phieuChiDinhTTPTService;

    private final PhieuChiDinhTTPTQueryService phieuChiDinhTTPTQueryService;

    public PhieuChiDinhTTPTResource(PhieuChiDinhTTPTService phieuChiDinhTTPTService, PhieuChiDinhTTPTQueryService phieuChiDinhTTPTQueryService) {
        this.phieuChiDinhTTPTService = phieuChiDinhTTPTService;
        this.phieuChiDinhTTPTQueryService = phieuChiDinhTTPTQueryService;
    }

    /**
     * {@code POST  /phieu-chi-dinh-ttpts} : Create a new phieuChiDinhTTPT.
     *
     * @param phieuChiDinhTTPTDTO the phieuChiDinhTTPTDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new phieuChiDinhTTPTDTO, or with status {@code 400 (Bad Request)} if the phieuChiDinhTTPT has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/phieu-chi-dinh-ttpts")
    public ResponseEntity<PhieuChiDinhTTPTDTO> createPhieuChiDinhTTPT(@Valid @RequestBody PhieuChiDinhTTPTDTO phieuChiDinhTTPTDTO) throws URISyntaxException {
        log.debug("REST request to save PhieuChiDinhTTPT : {}", phieuChiDinhTTPTDTO);
        if (phieuChiDinhTTPTDTO.getId() != null) {
            throw new BadRequestAlertException("A new phieuChiDinhTTPT cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhieuChiDinhTTPTDTO result = phieuChiDinhTTPTService.save(phieuChiDinhTTPTDTO);
        return ResponseEntity.created(new URI("/api/phieu-chi-dinh-ttpts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /phieu-chi-dinh-ttpts} : Updates an existing phieuChiDinhTTPT.
     *
     * @param phieuChiDinhTTPTDTO the phieuChiDinhTTPTDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phieuChiDinhTTPTDTO,
     * or with status {@code 400 (Bad Request)} if the phieuChiDinhTTPTDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the phieuChiDinhTTPTDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/phieu-chi-dinh-ttpts")
    public ResponseEntity<PhieuChiDinhTTPTDTO> updatePhieuChiDinhTTPT(@Valid @RequestBody PhieuChiDinhTTPTDTO phieuChiDinhTTPTDTO) throws URISyntaxException {
        log.debug("REST request to update PhieuChiDinhTTPT : {}", phieuChiDinhTTPTDTO);
        if (phieuChiDinhTTPTDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PhieuChiDinhTTPTDTO result = phieuChiDinhTTPTService.save(phieuChiDinhTTPTDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phieuChiDinhTTPTDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /phieu-chi-dinh-ttpts} : get all the phieuChiDinhTTPTS.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of phieuChiDinhTTPTS in body.
     */
    @GetMapping("/phieu-chi-dinh-ttpts")
    public ResponseEntity<List<PhieuChiDinhTTPTDTO>> getAllPhieuChiDinhTTPTS(PhieuChiDinhTTPTCriteria criteria, Pageable pageable) {
        log.debug("REST request to get PhieuChiDinhTTPTS by criteria: {}", criteria);
        Page<PhieuChiDinhTTPTDTO> page = phieuChiDinhTTPTQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /phieu-chi-dinh-ttpts/count} : count all the phieuChiDinhTTPTS.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/phieu-chi-dinh-ttpts/count")
    public ResponseEntity<Long> countPhieuChiDinhTTPTS(PhieuChiDinhTTPTCriteria criteria) {
        log.debug("REST request to count PhieuChiDinhTTPTS by criteria: {}", criteria);
        return ResponseEntity.ok().body(phieuChiDinhTTPTQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /phieu-chi-dinh-ttpts/:id} : get the "id" phieuChiDinhTTPT.
     *
//     * @param id the id of the phieuChiDinhTTPTDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the phieuChiDinhTTPTDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/phieu-chi-dinh-ttpts/{id}")
    public ResponseEntity<PhieuChiDinhTTPTDTO> getPhieuChiDinhTTPT(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final PhieuChiDinhTTPTId id = mapper.convertValue(idMap, PhieuChiDinhTTPTId.class);
        log.debug("REST request to get PhieuChiDinhTTPT : {}", id);
        Optional<PhieuChiDinhTTPTDTO> phieuChiDinhTTPTDTO = phieuChiDinhTTPTService.findOne(id);
        return ResponseUtil.wrapOrNotFound(phieuChiDinhTTPTDTO);
    }

    /**
     * {@code DELETE  /phieu-chi-dinh-ttpts/:id} : delete the "id" phieuChiDinhTTPT.
     *
//     * @param id the id of the phieuChiDinhTTPTDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/phieu-chi-dinh-ttpts/{id}")
    public ResponseEntity<Void> deletePhieuChiDinhTTPT(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final PhieuChiDinhTTPTId id = mapper.convertValue(idMap, PhieuChiDinhTTPTId.class);
        log.debug("REST request to delete PhieuChiDinhTTPT : {}", id);
        phieuChiDinhTTPTService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
