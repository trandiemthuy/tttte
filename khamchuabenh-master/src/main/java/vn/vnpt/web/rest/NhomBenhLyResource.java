package vn.vnpt.web.rest;

import vn.vnpt.service.NhomBenhLyService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.NhomBenhLyDTO;
import vn.vnpt.service.dto.NhomBenhLyCriteria;
import vn.vnpt.service.NhomBenhLyQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.NhomBenhLy}.
 */
@RestController
@RequestMapping("/api")
public class NhomBenhLyResource {

    private final Logger log = LoggerFactory.getLogger(NhomBenhLyResource.class);

    private static final String ENTITY_NAME = "khamchuabenhNhomBenhLy";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NhomBenhLyService nhomBenhLyService;

    private final NhomBenhLyQueryService nhomBenhLyQueryService;

    public NhomBenhLyResource(NhomBenhLyService nhomBenhLyService, NhomBenhLyQueryService nhomBenhLyQueryService) {
        this.nhomBenhLyService = nhomBenhLyService;
        this.nhomBenhLyQueryService = nhomBenhLyQueryService;
    }

    /**
     * {@code POST  /nhom-benh-lies} : Create a new nhomBenhLy.
     *
     * @param nhomBenhLyDTO the nhomBenhLyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new nhomBenhLyDTO, or with status {@code 400 (Bad Request)} if the nhomBenhLy has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nhom-benh-lies")
    public ResponseEntity<NhomBenhLyDTO> createNhomBenhLy(@Valid @RequestBody NhomBenhLyDTO nhomBenhLyDTO) throws URISyntaxException {
        log.debug("REST request to save NhomBenhLy : {}", nhomBenhLyDTO);
        if (nhomBenhLyDTO.getId() != null) {
            throw new BadRequestAlertException("A new nhomBenhLy cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NhomBenhLyDTO result = nhomBenhLyService.save(nhomBenhLyDTO);
        return ResponseEntity.created(new URI("/api/nhom-benh-lies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nhom-benh-lies} : Updates an existing nhomBenhLy.
     *
     * @param nhomBenhLyDTO the nhomBenhLyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nhomBenhLyDTO,
     * or with status {@code 400 (Bad Request)} if the nhomBenhLyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the nhomBenhLyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nhom-benh-lies")
    public ResponseEntity<NhomBenhLyDTO> updateNhomBenhLy(@Valid @RequestBody NhomBenhLyDTO nhomBenhLyDTO) throws URISyntaxException {
        log.debug("REST request to update NhomBenhLy : {}", nhomBenhLyDTO);
        if (nhomBenhLyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NhomBenhLyDTO result = nhomBenhLyService.save(nhomBenhLyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nhomBenhLyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /nhom-benh-lies} : get all the nhomBenhLies.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nhomBenhLies in body.
     */
    @GetMapping("/nhom-benh-lies")
    public ResponseEntity<List<NhomBenhLyDTO>> getAllNhomBenhLies(NhomBenhLyCriteria criteria, Pageable pageable) {
        log.debug("REST request to get NhomBenhLies by criteria: {}", criteria);
        Page<NhomBenhLyDTO> page = nhomBenhLyQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /nhom-benh-lies/count} : count all the nhomBenhLies.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/nhom-benh-lies/count")
    public ResponseEntity<Long> countNhomBenhLies(NhomBenhLyCriteria criteria) {
        log.debug("REST request to count NhomBenhLies by criteria: {}", criteria);
        return ResponseEntity.ok().body(nhomBenhLyQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /nhom-benh-lies/:id} : get the "id" nhomBenhLy.
     *
     * @param id the id of the nhomBenhLyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nhomBenhLyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nhom-benh-lies/{id}")
    public ResponseEntity<NhomBenhLyDTO> getNhomBenhLy(@PathVariable Long id) {
        log.debug("REST request to get NhomBenhLy : {}", id);
        Optional<NhomBenhLyDTO> nhomBenhLyDTO = nhomBenhLyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nhomBenhLyDTO);
    }

    /**
     * {@code DELETE  /nhom-benh-lies/:id} : delete the "id" nhomBenhLy.
     *
     * @param id the id of the nhomBenhLyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nhom-benh-lies/{id}")
    public ResponseEntity<Void> deleteNhomBenhLy(@PathVariable Long id) {
        log.debug("REST request to delete NhomBenhLy : {}", id);
        nhomBenhLyService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
