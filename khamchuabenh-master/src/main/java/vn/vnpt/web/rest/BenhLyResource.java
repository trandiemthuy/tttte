package vn.vnpt.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.BenhLySearch;
import vn.vnpt.repository.BenhLySearchRepository;
import vn.vnpt.service.BenhLyService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.BenhLyDTO;
import vn.vnpt.service.dto.BenhLyCriteria;
import vn.vnpt.service.BenhLyQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.BenhLy}.
 */
@RestController
@RequestMapping("/api")
public class BenhLyResource {

    private final Logger log = LoggerFactory.getLogger(BenhLyResource.class);

    private static final String ENTITY_NAME = "khamchuabenhBenhLy";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BenhLyService benhLyService;

    private final BenhLyQueryService benhLyQueryService;

    public BenhLyResource(BenhLyService benhLyService, BenhLyQueryService benhLyQueryService) {
        this.benhLyService = benhLyService;
        this.benhLyQueryService = benhLyQueryService;
    }

    /**
     * {@code POST  /benh-lies} : Create a new benhLy.
     *
     * @param benhLyDTO the benhLyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new benhLyDTO, or with status {@code 400 (Bad Request)} if the benhLy has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @Autowired
    private BenhLySearchRepository benhLySearchRepository;
    @PostMapping("/benh-lies")
    public ResponseEntity<BenhLyDTO> createBenhLy(@Valid @RequestBody BenhLyDTO benhLyDTO) throws URISyntaxException {
        log.debug("REST request to save BenhLy : {}", benhLyDTO);
        if (benhLyDTO.getId() != null) {
            throw new BadRequestAlertException("A new benhLy cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BenhLyDTO result = benhLyService.save(benhLyDTO);
        BenhLySearch benhLySearch = new BenhLySearch();
        benhLySearch.setMoTa(benhLyDTO.getMoTa());
        benhLySearch.setId(benhLyDTO.getId());
        benhLySearch.setEnabled(true);
        benhLySearch.setIcd(benhLyDTO.getIcd());
        benhLySearch.setMoTaTiengAnh(benhLyDTO.getMoTaTiengAnh());
        benhLySearch.setNhomBenhLyId(benhLyDTO.getNhomBenhLyId());
        benhLySearch.setTenKhongDau(benhLyDTO.getTenKhongDau());
        benhLySearch.setVietTat(benhLyDTO.getVietTat());
        benhLySearch.setVncode(benhLyDTO.getVncode());
        benhLySearchRepository.save(benhLySearch);
        return ResponseEntity.created(new URI("/api/benh-lies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /benh-lies} : Updates an existing benhLy.
     *
     * @param benhLyDTO the benhLyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated benhLyDTO,
     * or with status {@code 400 (Bad Request)} if the benhLyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the benhLyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/benh-lies")
    public ResponseEntity<BenhLyDTO> updateBenhLy(@Valid @RequestBody BenhLyDTO benhLyDTO) throws URISyntaxException {
        log.debug("REST request to update BenhLy : {}", benhLyDTO);
        if (benhLyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BenhLyDTO result = benhLyService.save(benhLyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, benhLyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /benh-lies} : get all the benhLies.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of benhLies in body.
     */
    @GetMapping("/benh-lies")
    public ResponseEntity<List<BenhLyDTO>> getAllBenhLies(BenhLyCriteria criteria, Pageable pageable) {
        log.debug("REST request to get BenhLies by criteria: {}", criteria);
        Page<BenhLyDTO> page = benhLyQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /benh-lies/count} : count all the benhLies.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/benh-lies/count")
    public ResponseEntity<Long> countBenhLies(BenhLyCriteria criteria) {
        log.debug("REST request to count BenhLies by criteria: {}", criteria);
        return ResponseEntity.ok().body(benhLyQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /benh-lies/:id} : get the "id" benhLy.
     *
     * @param id the id of the benhLyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the benhLyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/benh-lies/{id}")
    public ResponseEntity<BenhLyDTO> getBenhLy(@PathVariable Long id) {
        log.debug("REST request to get BenhLy : {}", id);
        Optional<BenhLyDTO> benhLyDTO = benhLyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(benhLyDTO);
    }

    /**
     * {@code DELETE  /benh-lies/:id} : delete the "id" benhLy.
     *
     * @param id the id of the benhLyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/benh-lies/{id}")
    public ResponseEntity<Void> deleteBenhLy(@PathVariable Long id) {
        log.debug("REST request to delete BenhLy : {}", id);
        benhLyService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
