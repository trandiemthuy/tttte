package vn.vnpt.web.rest;

import vn.vnpt.service.TinhThanhPhoService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.TinhThanhPhoDTO;
import vn.vnpt.service.dto.TinhThanhPhoCriteria;
import vn.vnpt.service.TinhThanhPhoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.TinhThanhPho}.
 */
@RestController
@RequestMapping("/api")
public class TinhThanhPhoResource {

    private final Logger log = LoggerFactory.getLogger(TinhThanhPhoResource.class);

    private static final String ENTITY_NAME = "khamchuabenhTinhThanhPho";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TinhThanhPhoService tinhThanhPhoService;

    private final TinhThanhPhoQueryService tinhThanhPhoQueryService;

    public TinhThanhPhoResource(TinhThanhPhoService tinhThanhPhoService, TinhThanhPhoQueryService tinhThanhPhoQueryService) {
        this.tinhThanhPhoService = tinhThanhPhoService;
        this.tinhThanhPhoQueryService = tinhThanhPhoQueryService;
    }

    /**
     * {@code POST  /tinh-thanh-phos} : Create a new tinhThanhPho.
     *
     * @param tinhThanhPhoDTO the tinhThanhPhoDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tinhThanhPhoDTO, or with status {@code 400 (Bad Request)} if the tinhThanhPho has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tinh-thanh-phos")
    public ResponseEntity<TinhThanhPhoDTO> createTinhThanhPho(@Valid @RequestBody TinhThanhPhoDTO tinhThanhPhoDTO) throws URISyntaxException {
        log.debug("REST request to save TinhThanhPho : {}", tinhThanhPhoDTO);
        if (tinhThanhPhoDTO.getId() != null) {
            throw new BadRequestAlertException("A new tinhThanhPho cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TinhThanhPhoDTO result = tinhThanhPhoService.save(tinhThanhPhoDTO);
        return ResponseEntity.created(new URI("/api/tinh-thanh-phos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tinh-thanh-phos} : Updates an existing tinhThanhPho.
     *
     * @param tinhThanhPhoDTO the tinhThanhPhoDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tinhThanhPhoDTO,
     * or with status {@code 400 (Bad Request)} if the tinhThanhPhoDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tinhThanhPhoDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tinh-thanh-phos")
    public ResponseEntity<TinhThanhPhoDTO> updateTinhThanhPho(@Valid @RequestBody TinhThanhPhoDTO tinhThanhPhoDTO) throws URISyntaxException {
        log.debug("REST request to update TinhThanhPho : {}", tinhThanhPhoDTO);
        if (tinhThanhPhoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TinhThanhPhoDTO result = tinhThanhPhoService.save(tinhThanhPhoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tinhThanhPhoDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tinh-thanh-phos} : get all the tinhThanhPhos.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tinhThanhPhos in body.
     */
    @GetMapping("/tinh-thanh-phos")
    public ResponseEntity<List<TinhThanhPhoDTO>> getAllTinhThanhPhos(TinhThanhPhoCriteria criteria) {
        log.debug("REST request to get TinhThanhPhos by criteria: {}", criteria);
        List<TinhThanhPhoDTO> entityList = tinhThanhPhoQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /tinh-thanh-phos/count} : count all the tinhThanhPhos.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/tinh-thanh-phos/count")
    public ResponseEntity<Long> countTinhThanhPhos(TinhThanhPhoCriteria criteria) {
        log.debug("REST request to count TinhThanhPhos by criteria: {}", criteria);
        return ResponseEntity.ok().body(tinhThanhPhoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tinh-thanh-phos/:id} : get the "id" tinhThanhPho.
     *
     * @param id the id of the tinhThanhPhoDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tinhThanhPhoDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tinh-thanh-phos/{id}")
    public ResponseEntity<TinhThanhPhoDTO> getTinhThanhPho(@PathVariable Long id) {
        log.debug("REST request to get TinhThanhPho : {}", id);
        Optional<TinhThanhPhoDTO> tinhThanhPhoDTO = tinhThanhPhoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tinhThanhPhoDTO);
    }

    /**
     * {@code DELETE  /tinh-thanh-phos/:id} : delete the "id" tinhThanhPho.
     *
     * @param id the id of the tinhThanhPhoDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tinh-thanh-phos/{id}")
    public ResponseEntity<Void> deleteTinhThanhPho(@PathVariable Long id) {
        log.debug("REST request to delete TinhThanhPho : {}", id);
        tinhThanhPhoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
