package vn.vnpt.web.rest;

import vn.vnpt.service.ChucDanhService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.ChucDanhDTO;
import vn.vnpt.service.dto.ChucDanhCriteria;
import vn.vnpt.service.ChucDanhQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.ChucDanh}.
 */
@RestController
@RequestMapping("/api")
public class ChucDanhResource {

    private final Logger log = LoggerFactory.getLogger(ChucDanhResource.class);

    private static final String ENTITY_NAME = "khamchuabenhChucDanh";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChucDanhService chucDanhService;

    private final ChucDanhQueryService chucDanhQueryService;

    public ChucDanhResource(ChucDanhService chucDanhService, ChucDanhQueryService chucDanhQueryService) {
        this.chucDanhService = chucDanhService;
        this.chucDanhQueryService = chucDanhQueryService;
    }

    /**
     * {@code POST  /chuc-danhs} : Create a new chucDanh.
     *
     * @param chucDanhDTO the chucDanhDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chucDanhDTO, or with status {@code 400 (Bad Request)} if the chucDanh has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chuc-danhs")
    public ResponseEntity<ChucDanhDTO> createChucDanh(@Valid @RequestBody ChucDanhDTO chucDanhDTO) throws URISyntaxException {
        log.debug("REST request to save ChucDanh : {}", chucDanhDTO);
        if (chucDanhDTO.getId() != null) {
            throw new BadRequestAlertException("A new chucDanh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChucDanhDTO result = chucDanhService.save(chucDanhDTO);
        return ResponseEntity.created(new URI("/api/chuc-danhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /chuc-danhs} : Updates an existing chucDanh.
     *
     * @param chucDanhDTO the chucDanhDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chucDanhDTO,
     * or with status {@code 400 (Bad Request)} if the chucDanhDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chucDanhDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chuc-danhs")
    public ResponseEntity<ChucDanhDTO> updateChucDanh(@Valid @RequestBody ChucDanhDTO chucDanhDTO) throws URISyntaxException {
        log.debug("REST request to update ChucDanh : {}", chucDanhDTO);
        if (chucDanhDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChucDanhDTO result = chucDanhService.save(chucDanhDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chucDanhDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /chuc-danhs} : get all the chucDanhs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chucDanhs in body.
     */
    @GetMapping("/chuc-danhs")
    public ResponseEntity<List<ChucDanhDTO>> getAllChucDanhs(ChucDanhCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ChucDanhs by criteria: {}", criteria);
        Page<ChucDanhDTO> page = chucDanhQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /chuc-danhs/count} : count all the chucDanhs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/chuc-danhs/count")
    public ResponseEntity<Long> countChucDanhs(ChucDanhCriteria criteria) {
        log.debug("REST request to count ChucDanhs by criteria: {}", criteria);
        return ResponseEntity.ok().body(chucDanhQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /chuc-danhs/:id} : get the "id" chucDanh.
     *
     * @param id the id of the chucDanhDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chucDanhDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/chuc-danhs/{id}")
    public ResponseEntity<ChucDanhDTO> getChucDanh(@PathVariable Long id) {
        log.debug("REST request to get ChucDanh : {}", id);
        Optional<ChucDanhDTO> chucDanhDTO = chucDanhService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chucDanhDTO);
    }

    /**
     * {@code DELETE  /chuc-danhs/:id} : delete the "id" chucDanh.
     *
     * @param id the id of the chucDanhDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chuc-danhs/{id}")
    public ResponseEntity<Void> deleteChucDanh(@PathVariable Long id) {
        log.debug("REST request to delete ChucDanh : {}", id);
        chucDanhService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
