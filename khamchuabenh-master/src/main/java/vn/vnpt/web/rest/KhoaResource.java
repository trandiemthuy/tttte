package vn.vnpt.web.rest;

import vn.vnpt.service.KhoaService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.KhoaDTO;
import vn.vnpt.service.dto.KhoaCriteria;
import vn.vnpt.service.KhoaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.Khoa}.
 */
@RestController
@RequestMapping("/api")
public class KhoaResource {

    private final Logger log = LoggerFactory.getLogger(KhoaResource.class);

    private static final String ENTITY_NAME = "khamchuabenhKhoa";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KhoaService khoaService;

    private final KhoaQueryService khoaQueryService;

    public KhoaResource(KhoaService khoaService, KhoaQueryService khoaQueryService) {
        this.khoaService = khoaService;
        this.khoaQueryService = khoaQueryService;
    }

    /**
     * {@code POST  /khoas} : Create a new khoa.
     *
     * @param khoaDTO the khoaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new khoaDTO, or with status {@code 400 (Bad Request)} if the khoa has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/khoas")
    public ResponseEntity<KhoaDTO> createKhoa(@Valid @RequestBody KhoaDTO khoaDTO) throws URISyntaxException {
        log.debug("REST request to save Khoa : {}", khoaDTO);
        if (khoaDTO.getId() != null) {
            throw new BadRequestAlertException("A new khoa cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KhoaDTO result = khoaService.save(khoaDTO);
        return ResponseEntity.created(new URI("/api/khoas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /khoas} : Updates an existing khoa.
     *
     * @param khoaDTO the khoaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated khoaDTO,
     * or with status {@code 400 (Bad Request)} if the khoaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the khoaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/khoas")
    public ResponseEntity<KhoaDTO> updateKhoa(@Valid @RequestBody KhoaDTO khoaDTO) throws URISyntaxException {
        log.debug("REST request to update Khoa : {}", khoaDTO);
        if (khoaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        KhoaDTO result = khoaService.save(khoaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, khoaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /khoas} : get all the khoas.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of khoas in body.
     */
    @GetMapping("/khoas")
    public ResponseEntity<List<KhoaDTO>> getAllKhoas(KhoaCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Khoas by criteria: {}", criteria);
        Page<KhoaDTO> page = khoaQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /khoas/count} : count all the khoas.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/khoas/count")
    public ResponseEntity<Long> countKhoas(KhoaCriteria criteria) {
        log.debug("REST request to count Khoas by criteria: {}", criteria);
        return ResponseEntity.ok().body(khoaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /khoas/:id} : get the "id" khoa.
     *
     * @param id the id of the khoaDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the khoaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/khoas/{id}")
    public ResponseEntity<KhoaDTO> getKhoa(@PathVariable Long id) {
        log.debug("REST request to get Khoa : {}", id);
        Optional<KhoaDTO> khoaDTO = khoaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(khoaDTO);
    }

    /**
     * {@code DELETE  /khoas/:id} : delete the "id" khoa.
     *
     * @param id the id of the khoaDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/khoas/{id}")
    public ResponseEntity<Void> deleteKhoa(@PathVariable Long id) {
        log.debug("REST request to delete Khoa : {}", id);
        khoaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
