package vn.vnpt.web.rest;

import vn.vnpt.service.TPhanNhomCDHAService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.TPhanNhomCDHADTO;
import vn.vnpt.service.dto.TPhanNhomCDHACriteria;
import vn.vnpt.service.TPhanNhomCDHAQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.TPhanNhomCDHA}.
 */
@RestController
@RequestMapping("/api")
public class TPhanNhomCDHAResource {

    private final Logger log = LoggerFactory.getLogger(TPhanNhomCDHAResource.class);

    private static final String ENTITY_NAME = "khamchuabenhTPhanNhomCdha";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TPhanNhomCDHAService tPhanNhomCDHAService;

    private final TPhanNhomCDHAQueryService tPhanNhomCDHAQueryService;

    public TPhanNhomCDHAResource(TPhanNhomCDHAService tPhanNhomCDHAService, TPhanNhomCDHAQueryService tPhanNhomCDHAQueryService) {
        this.tPhanNhomCDHAService = tPhanNhomCDHAService;
        this.tPhanNhomCDHAQueryService = tPhanNhomCDHAQueryService;
    }

    /**
     * {@code POST  /t-phan-nhom-cdhas} : Create a new tPhanNhomCDHA.
     *
     * @param tPhanNhomCDHADTO the tPhanNhomCDHADTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tPhanNhomCDHADTO, or with status {@code 400 (Bad Request)} if the tPhanNhomCDHA has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/t-phan-nhom-cdhas")
    public ResponseEntity<TPhanNhomCDHADTO> createTPhanNhomCDHA(@Valid @RequestBody TPhanNhomCDHADTO tPhanNhomCDHADTO) throws URISyntaxException {
        log.debug("REST request to save TPhanNhomCDHA : {}", tPhanNhomCDHADTO);
        if (tPhanNhomCDHADTO.getId() != null) {
            throw new BadRequestAlertException("A new tPhanNhomCDHA cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TPhanNhomCDHADTO result = tPhanNhomCDHAService.save(tPhanNhomCDHADTO);
        return ResponseEntity.created(new URI("/api/t-phan-nhom-cdhas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /t-phan-nhom-cdhas} : Updates an existing tPhanNhomCDHA.
     *
     * @param tPhanNhomCDHADTO the tPhanNhomCDHADTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tPhanNhomCDHADTO,
     * or with status {@code 400 (Bad Request)} if the tPhanNhomCDHADTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tPhanNhomCDHADTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/t-phan-nhom-cdhas")
    public ResponseEntity<TPhanNhomCDHADTO> updateTPhanNhomCDHA(@Valid @RequestBody TPhanNhomCDHADTO tPhanNhomCDHADTO) throws URISyntaxException {
        log.debug("REST request to update TPhanNhomCDHA : {}", tPhanNhomCDHADTO);
        if (tPhanNhomCDHADTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TPhanNhomCDHADTO result = tPhanNhomCDHAService.save(tPhanNhomCDHADTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tPhanNhomCDHADTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /t-phan-nhom-cdhas} : get all the tPhanNhomCDHAS.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tPhanNhomCDHAS in body.
     */
    @GetMapping("/t-phan-nhom-cdhas")
    public ResponseEntity<List<TPhanNhomCDHADTO>> getAllTPhanNhomCDHAS(TPhanNhomCDHACriteria criteria, Pageable pageable) {
        log.debug("REST request to get TPhanNhomCDHAS by criteria: {}", criteria);
        Page<TPhanNhomCDHADTO> page = tPhanNhomCDHAQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /t-phan-nhom-cdhas/count} : count all the tPhanNhomCDHAS.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/t-phan-nhom-cdhas/count")
    public ResponseEntity<Long> countTPhanNhomCDHAS(TPhanNhomCDHACriteria criteria) {
        log.debug("REST request to count TPhanNhomCDHAS by criteria: {}", criteria);
        return ResponseEntity.ok().body(tPhanNhomCDHAQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /t-phan-nhom-cdhas/:id} : get the "id" tPhanNhomCDHA.
     *
     * @param id the id of the tPhanNhomCDHADTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tPhanNhomCDHADTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/t-phan-nhom-cdhas/{id}")
    public ResponseEntity<TPhanNhomCDHADTO> getTPhanNhomCDHA(@PathVariable Long id) {
        log.debug("REST request to get TPhanNhomCDHA : {}", id);
        Optional<TPhanNhomCDHADTO> tPhanNhomCDHADTO = tPhanNhomCDHAService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tPhanNhomCDHADTO);
    }

    /**
     * {@code DELETE  /t-phan-nhom-cdhas/:id} : delete the "id" tPhanNhomCDHA.
     *
     * @param id the id of the tPhanNhomCDHADTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/t-phan-nhom-cdhas/{id}")
    public ResponseEntity<Void> deleteTPhanNhomCDHA(@PathVariable Long id) {
        log.debug("REST request to delete TPhanNhomCDHA : {}", id);
        tPhanNhomCDHAService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
