package vn.vnpt.web.rest;

import vn.vnpt.service.NhanVienService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.NhanVienDTO;
import vn.vnpt.service.dto.NhanVienCriteria;
import vn.vnpt.service.NhanVienQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.NhanVien}.
 */
@RestController
@RequestMapping("/api")
public class NhanVienResource {

    private final Logger log = LoggerFactory.getLogger(NhanVienResource.class);

    private static final String ENTITY_NAME = "khamchuabenhNhanVien";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NhanVienService nhanVienService;

    private final NhanVienQueryService nhanVienQueryService;

    public NhanVienResource(NhanVienService nhanVienService, NhanVienQueryService nhanVienQueryService) {
        this.nhanVienService = nhanVienService;
        this.nhanVienQueryService = nhanVienQueryService;
    }

    /**
     * {@code POST  /nhan-viens} : Create a new nhanVien.
     *
     * @param nhanVienDTO the nhanVienDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new nhanVienDTO, or with status {@code 400 (Bad Request)} if the nhanVien has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nhan-viens")
    public ResponseEntity<NhanVienDTO> createNhanVien(@Valid @RequestBody NhanVienDTO nhanVienDTO) throws URISyntaxException {
        log.debug("REST request to save NhanVien : {}", nhanVienDTO);
        if (nhanVienDTO.getId() != null) {
            throw new BadRequestAlertException("A new nhanVien cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NhanVienDTO result = nhanVienService.save(nhanVienDTO);
        return ResponseEntity.created(new URI("/api/nhan-viens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nhan-viens} : Updates an existing nhanVien.
     *
     * @param nhanVienDTO the nhanVienDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nhanVienDTO,
     * or with status {@code 400 (Bad Request)} if the nhanVienDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the nhanVienDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nhan-viens")
    public ResponseEntity<NhanVienDTO> updateNhanVien(@Valid @RequestBody NhanVienDTO nhanVienDTO) throws URISyntaxException {
        log.debug("REST request to update NhanVien : {}", nhanVienDTO);
        if (nhanVienDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NhanVienDTO result = nhanVienService.save(nhanVienDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nhanVienDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /nhan-viens} : get all the nhanViens.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nhanViens in body.
     */
    @GetMapping("/nhan-viens")
    public ResponseEntity<List<NhanVienDTO>> getAllNhanViens(NhanVienCriteria criteria, Pageable pageable) {
        log.debug("REST request to get NhanViens by criteria: {}", criteria);
        Page<NhanVienDTO> page = nhanVienQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /nhan-viens/count} : count all the nhanViens.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/nhan-viens/count")
    public ResponseEntity<Long> countNhanViens(NhanVienCriteria criteria) {
        log.debug("REST request to count NhanViens by criteria: {}", criteria);
        return ResponseEntity.ok().body(nhanVienQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /nhan-viens/:id} : get the "id" nhanVien.
     *
     * @param id the id of the nhanVienDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nhanVienDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nhan-viens/{id}")
    public ResponseEntity<NhanVienDTO> getNhanVien(@PathVariable Long id) {
        log.debug("REST request to get NhanVien : {}", id);
        Optional<NhanVienDTO> nhanVienDTO = nhanVienService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nhanVienDTO);
    }

    @GetMapping("/nhan-viens/khoa/{khoaId}")
    public ResponseEntity<List<NhanVienDTO>> getNhanVienByKhoa(@PathVariable Long khoaId){
        log.debug("Request lấy nhân viên theo khoa có Id ", khoaId);
        List<NhanVienDTO> result = nhanVienQueryService.findByKhoaId(khoaId);
        return ResponseEntity.ok().body(result);
    }

    /**
     * {@code DELETE  /nhan-viens/:id} : delete the "id" nhanVien.
     *
     * @param id the id of the nhanVienDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nhan-viens/{id}")
    public ResponseEntity<Void> deleteNhanVien(@PathVariable Long id) {
        log.debug("REST request to delete NhanVien : {}", id);
        nhanVienService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
