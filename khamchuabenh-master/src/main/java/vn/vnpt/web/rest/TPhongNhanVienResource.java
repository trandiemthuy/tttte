package vn.vnpt.web.rest;

import vn.vnpt.service.TPhongNhanVienService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.TPhongNhanVienDTO;
import vn.vnpt.service.dto.TPhongNhanVienCriteria;
import vn.vnpt.service.TPhongNhanVienQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.TPhongNhanVien}.
 */
@RestController
@RequestMapping("/api")
public class TPhongNhanVienResource {

    private final Logger log = LoggerFactory.getLogger(TPhongNhanVienResource.class);

    private static final String ENTITY_NAME = "khamchuabenhTPhongNhanVien";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TPhongNhanVienService tPhongNhanVienService;

    private final TPhongNhanVienQueryService tPhongNhanVienQueryService;

    public TPhongNhanVienResource(TPhongNhanVienService tPhongNhanVienService, TPhongNhanVienQueryService tPhongNhanVienQueryService) {
        this.tPhongNhanVienService = tPhongNhanVienService;
        this.tPhongNhanVienQueryService = tPhongNhanVienQueryService;
    }

    /**
     * {@code POST  /t-phong-nhan-viens} : Create a new tPhongNhanVien.
     *
     * @param tPhongNhanVienDTO the tPhongNhanVienDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tPhongNhanVienDTO, or with status {@code 400 (Bad Request)} if the tPhongNhanVien has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/t-phong-nhan-viens")
    public ResponseEntity<TPhongNhanVienDTO> createTPhongNhanVien(@Valid @RequestBody TPhongNhanVienDTO tPhongNhanVienDTO) throws URISyntaxException {
        log.debug("REST request to save TPhongNhanVien : {}", tPhongNhanVienDTO);
        if (tPhongNhanVienDTO.getId() != null) {
            throw new BadRequestAlertException("A new tPhongNhanVien cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TPhongNhanVienDTO result = tPhongNhanVienService.save(tPhongNhanVienDTO);
        return ResponseEntity.created(new URI("/api/t-phong-nhan-viens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /t-phong-nhan-viens} : Updates an existing tPhongNhanVien.
     *
     * @param tPhongNhanVienDTO the tPhongNhanVienDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tPhongNhanVienDTO,
     * or with status {@code 400 (Bad Request)} if the tPhongNhanVienDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tPhongNhanVienDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/t-phong-nhan-viens")
    public ResponseEntity<TPhongNhanVienDTO> updateTPhongNhanVien(@Valid @RequestBody TPhongNhanVienDTO tPhongNhanVienDTO) throws URISyntaxException {
        log.debug("REST request to update TPhongNhanVien : {}", tPhongNhanVienDTO);
        if (tPhongNhanVienDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TPhongNhanVienDTO result = tPhongNhanVienService.save(tPhongNhanVienDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tPhongNhanVienDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /t-phong-nhan-viens} : get all the tPhongNhanViens.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tPhongNhanViens in body.
     */
    @GetMapping("/t-phong-nhan-viens")
    public ResponseEntity<List<TPhongNhanVienDTO>> getAllTPhongNhanViens(TPhongNhanVienCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TPhongNhanViens by criteria: {}", criteria);
        Page<TPhongNhanVienDTO> page = tPhongNhanVienQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /t-phong-nhan-viens/count} : count all the tPhongNhanViens.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/t-phong-nhan-viens/count")
    public ResponseEntity<Long> countTPhongNhanViens(TPhongNhanVienCriteria criteria) {
        log.debug("REST request to count TPhongNhanViens by criteria: {}", criteria);
        return ResponseEntity.ok().body(tPhongNhanVienQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /t-phong-nhan-viens/:id} : get the "id" tPhongNhanVien.
     *
     * @param id the id of the tPhongNhanVienDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tPhongNhanVienDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/t-phong-nhan-viens/{id}")
    public ResponseEntity<TPhongNhanVienDTO> getTPhongNhanVien(@PathVariable Long id) {
        log.debug("REST request to get TPhongNhanVien : {}", id);
        Optional<TPhongNhanVienDTO> tPhongNhanVienDTO = tPhongNhanVienService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tPhongNhanVienDTO);
    }

    /**
     * {@code DELETE  /t-phong-nhan-viens/:id} : delete the "id" tPhongNhanVien.
     *
     * @param id the id of the tPhongNhanVienDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/t-phong-nhan-viens/{id}")
    public ResponseEntity<Void> deleteTPhongNhanVien(@PathVariable Long id) {
        log.debug("REST request to delete TPhongNhanVien : {}", id);
        tPhongNhanVienService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
