package vn.vnpt.web.rest;

import vn.vnpt.service.LoaiBenhLyService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.LoaiBenhLyDTO;
import vn.vnpt.service.dto.LoaiBenhLyCriteria;
import vn.vnpt.service.LoaiBenhLyQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.LoaiBenhLy}.
 */
@RestController
@RequestMapping("/api")
public class LoaiBenhLyResource {

    private final Logger log = LoggerFactory.getLogger(LoaiBenhLyResource.class);

    private static final String ENTITY_NAME = "khamchuabenhLoaiBenhLy";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LoaiBenhLyService loaiBenhLyService;

    private final LoaiBenhLyQueryService loaiBenhLyQueryService;

    public LoaiBenhLyResource(LoaiBenhLyService loaiBenhLyService, LoaiBenhLyQueryService loaiBenhLyQueryService) {
        this.loaiBenhLyService = loaiBenhLyService;
        this.loaiBenhLyQueryService = loaiBenhLyQueryService;
    }

    /**
     * {@code POST  /loai-benh-lies} : Create a new loaiBenhLy.
     *
     * @param loaiBenhLyDTO the loaiBenhLyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new loaiBenhLyDTO, or with status {@code 400 (Bad Request)} if the loaiBenhLy has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loai-benh-lies")
    public ResponseEntity<LoaiBenhLyDTO> createLoaiBenhLy(@Valid @RequestBody LoaiBenhLyDTO loaiBenhLyDTO) throws URISyntaxException {
        log.debug("REST request to save LoaiBenhLy : {}", loaiBenhLyDTO);
        if (loaiBenhLyDTO.getId() != null) {
            throw new BadRequestAlertException("A new loaiBenhLy cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LoaiBenhLyDTO result = loaiBenhLyService.save(loaiBenhLyDTO);
        return ResponseEntity.created(new URI("/api/loai-benh-lies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loai-benh-lies} : Updates an existing loaiBenhLy.
     *
     * @param loaiBenhLyDTO the loaiBenhLyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loaiBenhLyDTO,
     * or with status {@code 400 (Bad Request)} if the loaiBenhLyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the loaiBenhLyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loai-benh-lies")
    public ResponseEntity<LoaiBenhLyDTO> updateLoaiBenhLy(@Valid @RequestBody LoaiBenhLyDTO loaiBenhLyDTO) throws URISyntaxException {
        log.debug("REST request to update LoaiBenhLy : {}", loaiBenhLyDTO);
        if (loaiBenhLyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LoaiBenhLyDTO result = loaiBenhLyService.save(loaiBenhLyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, loaiBenhLyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /loai-benh-lies} : get all the loaiBenhLies.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of loaiBenhLies in body.
     */
    @GetMapping("/loai-benh-lies")
    public ResponseEntity<List<LoaiBenhLyDTO>> getAllLoaiBenhLies(LoaiBenhLyCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LoaiBenhLies by criteria: {}", criteria);
        Page<LoaiBenhLyDTO> page = loaiBenhLyQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loai-benh-lies/count} : count all the loaiBenhLies.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loai-benh-lies/count")
    public ResponseEntity<Long> countLoaiBenhLies(LoaiBenhLyCriteria criteria) {
        log.debug("REST request to count LoaiBenhLies by criteria: {}", criteria);
        return ResponseEntity.ok().body(loaiBenhLyQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loai-benh-lies/:id} : get the "id" loaiBenhLy.
     *
     * @param id the id of the loaiBenhLyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the loaiBenhLyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loai-benh-lies/{id}")
    public ResponseEntity<LoaiBenhLyDTO> getLoaiBenhLy(@PathVariable Long id) {
        log.debug("REST request to get LoaiBenhLy : {}", id);
        Optional<LoaiBenhLyDTO> loaiBenhLyDTO = loaiBenhLyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(loaiBenhLyDTO);
    }

    /**
     * {@code DELETE  /loai-benh-lies/:id} : delete the "id" loaiBenhLy.
     *
     * @param id the id of the loaiBenhLyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loai-benh-lies/{id}")
    public ResponseEntity<Void> deleteLoaiBenhLy(@PathVariable Long id) {
        log.debug("REST request to delete LoaiBenhLy : {}", id);
        loaiBenhLyService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
