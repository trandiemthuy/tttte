package vn.vnpt.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.DotDieuTri;
import vn.vnpt.service.DotDieuTriService;
import vn.vnpt.service.mapper.BenhAnKhamBenhMapper;
import vn.vnpt.service.mapper.BenhNhanMapper;
import vn.vnpt.service.mapper.DonViMapper;
import vn.vnpt.service.mapper.DotDieuTriMapper;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.DotDieuTriDTO;
import vn.vnpt.service.dto.DotDieuTriCriteria;
import vn.vnpt.service.DotDieuTriQueryService;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import java.util.Map;import java.util.Optional;
import vn.vnpt.domain.DotDieuTriId;

/**
 * REST controller for managing {@link vn.vnpt.domain.DotDieuTri}.
 */
@RestController
@RequestMapping("/api")
public class DotDieuTriResource {

    private final Logger log = LoggerFactory.getLogger(DotDieuTriResource.class);

    private static final String ENTITY_NAME = "khamchuabenhDotDieuTri";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DotDieuTriService dotDieuTriService;

    private final DotDieuTriQueryService dotDieuTriQueryService;
    @Autowired
    private DotDieuTriMapper dotDieuTriMapper;
//    @Autowired
//    private BenhAnKhamBenhMapper benhAnKhamBenhMapper;
//    @Autowired
//    private BenhNhanMapper benhNhanMapper;
//    @Autowired
//    private DonViMapper donViMapper;
    public DotDieuTriResource(DotDieuTriService dotDieuTriService, DotDieuTriQueryService dotDieuTriQueryService) {
        this.dotDieuTriService = dotDieuTriService;
        this.dotDieuTriQueryService = dotDieuTriQueryService;
    }

    /**
     * {@code POST  /dot-dieu-tris} : Create a new dotDieuTri.
     *
     * @param dotDieuTriDTO the dotDieuTriDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dotDieuTriDTO, or with status {@code 400 (Bad Request)} if the dotDieuTri has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dot-dieu-tris")
    public ResponseEntity<DotDieuTriDTO> createDotDieuTri(@Valid @RequestBody DotDieuTriDTO dotDieuTriDTO) throws URISyntaxException {
        log.debug("REST request to save DotDieuTri : {}", dotDieuTriDTO);
        if (dotDieuTriService.findOne(dotDieuTriDTO.getCompositeId()).isPresent()) {
            throw new BadRequestAlertException("This dotDieuTri already exists", ENTITY_NAME, "idexists");
        }
        DotDieuTriDTO result = dotDieuTriService.save(dotDieuTriDTO);
        return ResponseEntity.created(new URI("/api/dot-dieu-tris/" + "bakbBenhNhanId=" + result.getBenhNhanId() + ";" + "bakbDonViId=" + result.getDonViId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, "bakbBenhNhanId=" + result.getBenhNhanId() + ";" + "bakbDonViId=" + result.getDonViId()))
            .body(result);
    }

    /**
     * {@code PUT  /dot-dieu-tris} : Updates an existing dotDieuTri.
     *
     * @param dotDieuTriDTO the dotDieuTriDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dotDieuTriDTO,
     * or with status {@code 400 (Bad Request)} if the dotDieuTriDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dotDieuTriDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dot-dieu-tris")
    public ResponseEntity<DotDieuTriDTO> updateDotDieuTri(@Valid @RequestBody DotDieuTriDTO dotDieuTriDTO) throws URISyntaxException {
        log.debug("REST request to update DotDieuTri : {}", dotDieuTriDTO);
        if (!dotDieuTriService.findOne(dotDieuTriDTO.getCompositeId()).isPresent()) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DotDieuTriDTO result = dotDieuTriService.save(dotDieuTriDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, "bakbBenhNhanId=" + result.getBenhNhanId() + ";" + "bakbDonViId=" + result.getDonViId()))
            .body(result);
    }

    /**
     * {@code GET  /dot-dieu-tris} : get all the dotDieuTris.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dotDieuTris in body.
     */
    @GetMapping("/dot-dieu-tris")
    public ResponseEntity<List<DotDieuTriDTO>> getAllDotDieuTris(DotDieuTriCriteria criteria, Pageable pageable) {
        log.debug("REST request to get DotDieuTris by criteria: {}", criteria);
        Page<DotDieuTriDTO> page = dotDieuTriQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /dot-dieu-tris/count} : count all the dotDieuTris.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/dot-dieu-tris/count")
    public ResponseEntity<Long> countDotDieuTris(DotDieuTriCriteria criteria) {
        log.debug("REST request to count DotDieuTris by criteria: {}", criteria);
        return ResponseEntity.ok().body(dotDieuTriQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /dot-dieu-tris/:id} : get the "id" dotDieuTri.
     *
     * @param idMap a Map representation of the id of the dotDieuTriDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dotDieuTriDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dot-dieu-tris/{id}")
    public ResponseEntity<DotDieuTriDTO> getDotDieuTri(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final DotDieuTriId id = mapper.convertValue(idMap, DotDieuTriId.class);
        log.debug("REST request to get DotDieuTri : {}", id);
        Optional<DotDieuTriDTO> dotDieuTriDTO = dotDieuTriService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dotDieuTriDTO);
    }

    /**
     * {@code DELETE  /dot-dieu-tris/:id} : delete the "id" dotDieuTri.
     *
     * @param idMap a Map representation of the id of the dotDieuTriDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/dot-dieu-tris/{id}")
    public ResponseEntity<Void> deleteDotDieuTri(@MatrixVariable(pathVar = "id") Map<String, String> idMap) {
        final ObjectMapper mapper = new ObjectMapper();
        final DotDieuTriId id = mapper.convertValue(idMap, DotDieuTriId.class);
        log.debug("REST request to delete DotDieuTri : {}", id);
        dotDieuTriService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
