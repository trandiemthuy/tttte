package vn.vnpt.web.rest;

import vn.vnpt.service.NhomChanDoanHinhAnhService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.NhomChanDoanHinhAnhDTO;
import vn.vnpt.service.dto.NhomChanDoanHinhAnhCriteria;
import vn.vnpt.service.NhomChanDoanHinhAnhQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.NhomChanDoanHinhAnh}.
 */
@RestController
@RequestMapping("/api")
public class NhomChanDoanHinhAnhResource {

    private final Logger log = LoggerFactory.getLogger(NhomChanDoanHinhAnhResource.class);

    private static final String ENTITY_NAME = "khamchuabenhNhomChanDoanHinhAnh";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NhomChanDoanHinhAnhService nhomChanDoanHinhAnhService;

    private final NhomChanDoanHinhAnhQueryService nhomChanDoanHinhAnhQueryService;

    public NhomChanDoanHinhAnhResource(NhomChanDoanHinhAnhService nhomChanDoanHinhAnhService, NhomChanDoanHinhAnhQueryService nhomChanDoanHinhAnhQueryService) {
        this.nhomChanDoanHinhAnhService = nhomChanDoanHinhAnhService;
        this.nhomChanDoanHinhAnhQueryService = nhomChanDoanHinhAnhQueryService;
    }

    /**
     * {@code POST  /nhom-chan-doan-hinh-anhs} : Create a new nhomChanDoanHinhAnh.
     *
     * @param nhomChanDoanHinhAnhDTO the nhomChanDoanHinhAnhDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new nhomChanDoanHinhAnhDTO, or with status {@code 400 (Bad Request)} if the nhomChanDoanHinhAnh has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nhom-chan-doan-hinh-anhs")
    public ResponseEntity<NhomChanDoanHinhAnhDTO> createNhomChanDoanHinhAnh(@Valid @RequestBody NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO) throws URISyntaxException {
        log.debug("REST request to save NhomChanDoanHinhAnh : {}", nhomChanDoanHinhAnhDTO);
        if (nhomChanDoanHinhAnhDTO.getId() != null) {
            throw new BadRequestAlertException("A new nhomChanDoanHinhAnh cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NhomChanDoanHinhAnhDTO result = nhomChanDoanHinhAnhService.save(nhomChanDoanHinhAnhDTO);
        return ResponseEntity.created(new URI("/api/nhom-chan-doan-hinh-anhs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nhom-chan-doan-hinh-anhs} : Updates an existing nhomChanDoanHinhAnh.
     *
     * @param nhomChanDoanHinhAnhDTO the nhomChanDoanHinhAnhDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nhomChanDoanHinhAnhDTO,
     * or with status {@code 400 (Bad Request)} if the nhomChanDoanHinhAnhDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the nhomChanDoanHinhAnhDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nhom-chan-doan-hinh-anhs")
    public ResponseEntity<NhomChanDoanHinhAnhDTO> updateNhomChanDoanHinhAnh(@Valid @RequestBody NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO) throws URISyntaxException {
        log.debug("REST request to update NhomChanDoanHinhAnh : {}", nhomChanDoanHinhAnhDTO);
        if (nhomChanDoanHinhAnhDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NhomChanDoanHinhAnhDTO result = nhomChanDoanHinhAnhService.save(nhomChanDoanHinhAnhDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nhomChanDoanHinhAnhDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /nhom-chan-doan-hinh-anhs} : get all the nhomChanDoanHinhAnhs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nhomChanDoanHinhAnhs in body.
     */
    @GetMapping("/nhom-chan-doan-hinh-anhs")
    public ResponseEntity<List<NhomChanDoanHinhAnhDTO>> getAllNhomChanDoanHinhAnhs(NhomChanDoanHinhAnhCriteria criteria, Pageable pageable) {
        log.debug("REST request to get NhomChanDoanHinhAnhs by criteria: {}", criteria);
        Page<NhomChanDoanHinhAnhDTO> page = nhomChanDoanHinhAnhQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /nhom-chan-doan-hinh-anhs/count} : count all the nhomChanDoanHinhAnhs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/nhom-chan-doan-hinh-anhs/count")
    public ResponseEntity<Long> countNhomChanDoanHinhAnhs(NhomChanDoanHinhAnhCriteria criteria) {
        log.debug("REST request to count NhomChanDoanHinhAnhs by criteria: {}", criteria);
        return ResponseEntity.ok().body(nhomChanDoanHinhAnhQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /nhom-chan-doan-hinh-anhs/:id} : get the "id" nhomChanDoanHinhAnh.
     *
     * @param id the id of the nhomChanDoanHinhAnhDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nhomChanDoanHinhAnhDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nhom-chan-doan-hinh-anhs/{id}")
    public ResponseEntity<NhomChanDoanHinhAnhDTO> getNhomChanDoanHinhAnh(@PathVariable Long id) {
        log.debug("REST request to get NhomChanDoanHinhAnh : {}", id);
        Optional<NhomChanDoanHinhAnhDTO> nhomChanDoanHinhAnhDTO = nhomChanDoanHinhAnhService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nhomChanDoanHinhAnhDTO);
    }

    /**
     * {@code DELETE  /nhom-chan-doan-hinh-anhs/:id} : delete the "id" nhomChanDoanHinhAnh.
     *
     * @param id the id of the nhomChanDoanHinhAnhDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nhom-chan-doan-hinh-anhs/{id}")
    public ResponseEntity<Void> deleteNhomChanDoanHinhAnh(@PathVariable Long id) {
        log.debug("REST request to delete NhomChanDoanHinhAnh : {}", id);
        nhomChanDoanHinhAnhService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
