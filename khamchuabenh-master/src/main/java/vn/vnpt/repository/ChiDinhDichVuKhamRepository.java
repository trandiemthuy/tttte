package vn.vnpt.repository;

import vn.vnpt.domain.ChiDinhDichVuKham;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ChiDinhDichVuKham entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChiDinhDichVuKhamRepository extends JpaRepository<ChiDinhDichVuKham, Long>, JpaSpecificationExecutor<ChiDinhDichVuKham> {
}
