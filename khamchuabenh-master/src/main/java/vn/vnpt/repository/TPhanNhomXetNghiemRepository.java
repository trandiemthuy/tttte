package vn.vnpt.repository;

import vn.vnpt.domain.TPhanNhomXetNghiem;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TPhanNhomXetNghiem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TPhanNhomXetNghiemRepository extends JpaRepository<TPhanNhomXetNghiem, Long>, JpaSpecificationExecutor<TPhanNhomXetNghiem> {
}
