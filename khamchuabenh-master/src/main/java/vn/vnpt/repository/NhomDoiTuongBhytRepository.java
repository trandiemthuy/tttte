package vn.vnpt.repository;

import vn.vnpt.domain.NhomDoiTuongBhyt;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the NhomDoiTuongBhyt entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NhomDoiTuongBhytRepository extends JpaRepository<NhomDoiTuongBhyt, Long>, JpaSpecificationExecutor<NhomDoiTuongBhyt> {
}
