package vn.vnpt.repository;

import vn.vnpt.domain.ChiDinhXetNghiem;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.vnpt.domain.ChiDinhXetNghiemId;

/**
 * Spring Data  repository for the ChiDinhXetNghiem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChiDinhXetNghiemRepository extends JpaRepository<ChiDinhXetNghiem, ChiDinhXetNghiemId>, JpaSpecificationExecutor<ChiDinhXetNghiem> {
}
