package vn.vnpt.repository;

import vn.vnpt.domain.ThuThuatPhauThuat;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ThuThuatPhauThuat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ThuThuatPhauThuatRepository extends JpaRepository<ThuThuatPhauThuat, Long>, JpaSpecificationExecutor<ThuThuatPhauThuat> {
}
