package vn.vnpt.repository;

import vn.vnpt.domain.DotDieuTri;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.vnpt.domain.DotDieuTriId;
import vn.vnpt.service.dto.DotDieuTriDTO;

/**
 * Spring Data  repository for the DotDieuTri entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DotDieuTriRepository extends JpaRepository<DotDieuTri, DotDieuTriId>, JpaSpecificationExecutor<DotDieuTri> {
}
