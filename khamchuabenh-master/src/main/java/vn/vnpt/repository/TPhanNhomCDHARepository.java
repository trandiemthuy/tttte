package vn.vnpt.repository;

import vn.vnpt.domain.TPhanNhomCDHA;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TPhanNhomCDHA entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TPhanNhomCDHARepository extends JpaRepository<TPhanNhomCDHA, Long>, JpaSpecificationExecutor<TPhanNhomCDHA> {
}
