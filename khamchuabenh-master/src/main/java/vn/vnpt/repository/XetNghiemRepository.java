package vn.vnpt.repository;

import vn.vnpt.domain.XetNghiem;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the XetNghiem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface XetNghiemRepository extends JpaRepository<XetNghiem, Long>, JpaSpecificationExecutor<XetNghiem> {
}
