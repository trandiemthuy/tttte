package vn.vnpt.repository;

import vn.vnpt.domain.UserType;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the UserType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserTypeRepository extends JpaRepository<UserType, Long>, JpaSpecificationExecutor<UserType> {
}
