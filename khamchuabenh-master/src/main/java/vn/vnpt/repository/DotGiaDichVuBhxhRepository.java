package vn.vnpt.repository;

import vn.vnpt.domain.DotGiaDichVuBhxh;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the DotGiaDichVuBhxh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DotGiaDichVuBhxhRepository extends JpaRepository<DotGiaDichVuBhxh, Long>, JpaSpecificationExecutor<DotGiaDichVuBhxh> {
}
