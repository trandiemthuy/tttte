package vn.vnpt.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.domain.Specification;
import vn.vnpt.domain.BenhLy;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.vnpt.domain.BenhLySearch;
import vn.vnpt.service.mapper.BenhLyMapper;
import vn.vnpt.service.mapper.ChiDinhDichVuKhamMapper;

/**
 * Spring Data  repository for the BenhLy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BenhLySearchRepository extends ElasticsearchRepository<BenhLySearch, Long>{
}
