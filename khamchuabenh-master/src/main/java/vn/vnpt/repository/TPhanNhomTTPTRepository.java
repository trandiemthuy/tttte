package vn.vnpt.repository;

import vn.vnpt.domain.TPhanNhomTTPT;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TPhanNhomTTPT entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TPhanNhomTTPTRepository extends JpaRepository<TPhanNhomTTPT, Long>, JpaSpecificationExecutor<TPhanNhomTTPT> {
}
