package vn.vnpt.service;

import vn.vnpt.domain.BenhNhan;
import vn.vnpt.service.dto.TheBhytDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.TheBhyt}.
 */
public interface TheBhytService {

    /**
     * Save a theBhyt.
     *
     * @param theBhytDTO the entity to save.
     * @return the persisted entity.
     */
    TheBhytDTO save(TheBhytDTO theBhytDTO);

    /**
     * Get all the theBhyts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TheBhytDTO> findAll(Pageable pageable);

    /**
     * Get the "id" theBhyt.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TheBhytDTO> findOne(Long id);
    List<TheBhytDTO> findAllByBenhNhanId(Long benhNhanId);
    /**
     * Delete the "id" theBhyt.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
   // Optional<TheBhytDTO> getOnlyOneBhytByBenhNhan(BenhNhan benhNhan);
    Optional<TheBhytDTO> findOneBySoThe(String soThe);
}
