package vn.vnpt.service;

import vn.vnpt.service.dto.ChucDanhDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.ChucDanh}.
 */
public interface ChucDanhService {

    /**
     * Save a chucDanh.
     *
     * @param chucDanhDTO the entity to save.
     * @return the persisted entity.
     */
    ChucDanhDTO save(ChucDanhDTO chucDanhDTO);

    /**
     * Get all the chucDanhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ChucDanhDTO> findAll(Pageable pageable);

    /**
     * Get the "id" chucDanh.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChucDanhDTO> findOne(Long id);

    /**
     * Delete the "id" chucDanh.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
