package vn.vnpt.service;

import vn.vnpt.service.dto.DichVuKhamApDungDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.DichVuKhamApDung}.
 */
public interface DichVuKhamApDungService {

    /**
     * Save a dichVuKhamApDung.
     *
     * @param dichVuKhamApDungDTO the entity to save.
     * @return the persisted entity.
     */
    DichVuKhamApDungDTO save(DichVuKhamApDungDTO dichVuKhamApDungDTO);

    /**
     * Get all the dichVuKhamApDungs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DichVuKhamApDungDTO> findAll(Pageable pageable);


    /**
     * Get the "id" dichVuKhamApDung.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DichVuKhamApDungDTO> findOne(Long id);

    /**
     * Delete the "id" dichVuKhamApDung.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
