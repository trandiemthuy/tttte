package vn.vnpt.service;

import vn.vnpt.domain.PhieuChiDinhCDHAId;
import vn.vnpt.service.dto.PhieuChiDinhCDHADTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.PhieuChiDinhCDHA}.
 */
public interface PhieuChiDinhCDHAService {

    /**
     * Save a phieuChiDinhCDHA.
     *
     * @param phieuChiDinhCDHADTO the entity to save.
     * @return the persisted entity.
     */
    PhieuChiDinhCDHADTO save(PhieuChiDinhCDHADTO phieuChiDinhCDHADTO);

    /**
     * Get all the phieuChiDinhCDHAS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PhieuChiDinhCDHADTO> findAll(Pageable pageable);

    /**
     * Get the "id" phieuChiDinhCDHA.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PhieuChiDinhCDHADTO> findOne(PhieuChiDinhCDHAId id);

    /**
     * Delete the "id" phieuChiDinhCDHA.
     *
//     * @param id the id of the entity.
     */
    void delete(PhieuChiDinhCDHAId id);
}
