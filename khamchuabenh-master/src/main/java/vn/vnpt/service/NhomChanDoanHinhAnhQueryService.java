package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.NhomChanDoanHinhAnh;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.NhomChanDoanHinhAnhRepository;
import vn.vnpt.service.dto.NhomChanDoanHinhAnhCriteria;
import vn.vnpt.service.dto.NhomChanDoanHinhAnhDTO;
import vn.vnpt.service.mapper.NhomChanDoanHinhAnhMapper;

/**
 * Service for executing complex queries for {@link NhomChanDoanHinhAnh} entities in the database.
 * The main input is a {@link NhomChanDoanHinhAnhCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NhomChanDoanHinhAnhDTO} or a {@link Page} of {@link NhomChanDoanHinhAnhDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NhomChanDoanHinhAnhQueryService extends QueryService<NhomChanDoanHinhAnh> {

    private final Logger log = LoggerFactory.getLogger(NhomChanDoanHinhAnhQueryService.class);

    private final NhomChanDoanHinhAnhRepository nhomChanDoanHinhAnhRepository;

    private final NhomChanDoanHinhAnhMapper nhomChanDoanHinhAnhMapper;

    public NhomChanDoanHinhAnhQueryService(NhomChanDoanHinhAnhRepository nhomChanDoanHinhAnhRepository, NhomChanDoanHinhAnhMapper nhomChanDoanHinhAnhMapper) {
        this.nhomChanDoanHinhAnhRepository = nhomChanDoanHinhAnhRepository;
        this.nhomChanDoanHinhAnhMapper = nhomChanDoanHinhAnhMapper;
    }

    /**
     * Return a {@link List} of {@link NhomChanDoanHinhAnhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NhomChanDoanHinhAnhDTO> findByCriteria(NhomChanDoanHinhAnhCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<NhomChanDoanHinhAnh> specification = createSpecification(criteria);
        return nhomChanDoanHinhAnhMapper.toDto(nhomChanDoanHinhAnhRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NhomChanDoanHinhAnhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NhomChanDoanHinhAnhDTO> findByCriteria(NhomChanDoanHinhAnhCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<NhomChanDoanHinhAnh> specification = createSpecification(criteria);
        return nhomChanDoanHinhAnhRepository.findAll(specification, page)
            .map(nhomChanDoanHinhAnhMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NhomChanDoanHinhAnhCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<NhomChanDoanHinhAnh> specification = createSpecification(criteria);
        return nhomChanDoanHinhAnhRepository.count(specification);
    }

    /**
     * Function to convert {@link NhomChanDoanHinhAnhCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<NhomChanDoanHinhAnh> createSpecification(NhomChanDoanHinhAnhCriteria criteria) {
        Specification<NhomChanDoanHinhAnh> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), NhomChanDoanHinhAnh_.id));
            }
            if (criteria.getEnable() != null) {
                specification = specification.and(buildSpecification(criteria.getEnable(), NhomChanDoanHinhAnh_.enable));
            }
            if (criteria.getLevel() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLevel(), NhomChanDoanHinhAnh_.level));
            }
            if (criteria.getParentId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getParentId(), NhomChanDoanHinhAnh_.parentId));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), NhomChanDoanHinhAnh_.ten));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(NhomChanDoanHinhAnh_.donVi, JoinType.LEFT).get(DonVi_.id)));
            }
        }
        return specification;
    }
}
