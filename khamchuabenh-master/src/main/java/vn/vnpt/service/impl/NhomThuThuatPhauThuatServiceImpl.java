package vn.vnpt.service.impl;

import vn.vnpt.service.NhomThuThuatPhauThuatService;
import vn.vnpt.domain.NhomThuThuatPhauThuat;
import vn.vnpt.repository.NhomThuThuatPhauThuatRepository;
import vn.vnpt.service.dto.NhomThuThuatPhauThuatDTO;
import vn.vnpt.service.mapper.NhomThuThuatPhauThuatMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link NhomThuThuatPhauThuat}.
 */
@Service
@Transactional
public class NhomThuThuatPhauThuatServiceImpl implements NhomThuThuatPhauThuatService {

    private final Logger log = LoggerFactory.getLogger(NhomThuThuatPhauThuatServiceImpl.class);

    private final NhomThuThuatPhauThuatRepository nhomThuThuatPhauThuatRepository;

    private final NhomThuThuatPhauThuatMapper nhomThuThuatPhauThuatMapper;

    public NhomThuThuatPhauThuatServiceImpl(NhomThuThuatPhauThuatRepository nhomThuThuatPhauThuatRepository, NhomThuThuatPhauThuatMapper nhomThuThuatPhauThuatMapper) {
        this.nhomThuThuatPhauThuatRepository = nhomThuThuatPhauThuatRepository;
        this.nhomThuThuatPhauThuatMapper = nhomThuThuatPhauThuatMapper;
    }

    /**
     * Save a nhomThuThuatPhauThuat.
     *
     * @param nhomThuThuatPhauThuatDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public NhomThuThuatPhauThuatDTO save(NhomThuThuatPhauThuatDTO nhomThuThuatPhauThuatDTO) {
        log.debug("Request to save NhomThuThuatPhauThuat : {}", nhomThuThuatPhauThuatDTO);
        NhomThuThuatPhauThuat nhomThuThuatPhauThuat = nhomThuThuatPhauThuatMapper.toEntity(nhomThuThuatPhauThuatDTO);
        nhomThuThuatPhauThuat = nhomThuThuatPhauThuatRepository.save(nhomThuThuatPhauThuat);
        return nhomThuThuatPhauThuatMapper.toDto(nhomThuThuatPhauThuat);
    }

    /**
     * Get all the nhomThuThuatPhauThuats.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<NhomThuThuatPhauThuatDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NhomThuThuatPhauThuats");
        return nhomThuThuatPhauThuatRepository.findAll(pageable)
            .map(nhomThuThuatPhauThuatMapper::toDto);
    }

    /**
     * Get one nhomThuThuatPhauThuat by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NhomThuThuatPhauThuatDTO> findOne(Long id) {
        log.debug("Request to get NhomThuThuatPhauThuat : {}", id);
        return nhomThuThuatPhauThuatRepository.findById(id)
            .map(nhomThuThuatPhauThuatMapper::toDto);
    }

    /**
     * Delete the nhomThuThuatPhauThuat by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NhomThuThuatPhauThuat : {}", id);
        nhomThuThuatPhauThuatRepository.deleteById(id);
    }
}
