package vn.vnpt.service.impl;

import vn.vnpt.domain.ChiDinhCDHAId;
import vn.vnpt.service.ChiDinhCDHAService;
import vn.vnpt.domain.ChiDinhCDHA;
import vn.vnpt.repository.ChiDinhCDHARepository;
import vn.vnpt.service.dto.ChiDinhCDHADTO;
import vn.vnpt.service.mapper.ChiDinhCDHAMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ChiDinhCDHA}.
 */
@Service
@Transactional
public class ChiDinhCDHAServiceImpl implements ChiDinhCDHAService {

    private final Logger log = LoggerFactory.getLogger(ChiDinhCDHAServiceImpl.class);

    private final ChiDinhCDHARepository chiDinhCDHARepository;

    private final ChiDinhCDHAMapper chiDinhCDHAMapper;

    public ChiDinhCDHAServiceImpl(ChiDinhCDHARepository chiDinhCDHARepository, ChiDinhCDHAMapper chiDinhCDHAMapper) {
        this.chiDinhCDHARepository = chiDinhCDHARepository;
        this.chiDinhCDHAMapper = chiDinhCDHAMapper;
    }

    /**
     * Save a chiDinhCDHA.
     *
     * @param chiDinhCDHADTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ChiDinhCDHADTO save(ChiDinhCDHADTO chiDinhCDHADTO) {
        log.debug("Request to save ChiDinhCDHA : {}", chiDinhCDHADTO);
        ChiDinhCDHA chiDinhCDHA = chiDinhCDHAMapper.toEntity(chiDinhCDHADTO);
        chiDinhCDHA = chiDinhCDHARepository.save(chiDinhCDHA);
        return chiDinhCDHAMapper.toDto(chiDinhCDHA);
    }

    /**
     * Get all the chiDinhCDHAS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ChiDinhCDHADTO> findAll(Pageable pageable) {
        log.debug("Request to get all ChiDinhCDHAS");
        return chiDinhCDHARepository.findAll(pageable)
            .map(chiDinhCDHAMapper::toDto);
    }

    /**
     * Get one chiDinhCDHA by id.
     *
//     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ChiDinhCDHADTO> findOne(ChiDinhCDHAId id) {
        log.debug("Request to get ChiDinhCDHA : {}", id);
        return chiDinhCDHARepository.findById(id)
            .map(chiDinhCDHAMapper::toDto);
    }

    /**
     * Delete the chiDinhCDHA by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(ChiDinhCDHAId id) {
        log.debug("Request to delete ChiDinhCDHA : {}", id);
        chiDinhCDHARepository.deleteById(id);
    }
}
