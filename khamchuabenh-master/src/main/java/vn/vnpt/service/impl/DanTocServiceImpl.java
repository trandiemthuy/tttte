package vn.vnpt.service.impl;

import vn.vnpt.service.DanTocService;
import vn.vnpt.domain.DanToc;
import vn.vnpt.repository.DanTocRepository;
import vn.vnpt.service.dto.DanTocDTO;
import vn.vnpt.service.mapper.DanTocMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link DanToc}.
 */
@Service
@Transactional
public class DanTocServiceImpl implements DanTocService {

    private final Logger log = LoggerFactory.getLogger(DanTocServiceImpl.class);

    private final DanTocRepository danTocRepository;

    private final DanTocMapper danTocMapper;

    public DanTocServiceImpl(DanTocRepository danTocRepository, DanTocMapper danTocMapper) {
        this.danTocRepository = danTocRepository;
        this.danTocMapper = danTocMapper;
    }

    /**
     * Save a danToc.
     *
     * @param danTocDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DanTocDTO save(DanTocDTO danTocDTO) {
        log.debug("Request to save DanToc : {}", danTocDTO);
        DanToc danToc = danTocMapper.toEntity(danTocDTO);
        danToc = danTocRepository.save(danToc);
        return danTocMapper.toDto(danToc);
    }

    /**
     * Get all the danTocs.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<DanTocDTO> findAll() {
        log.debug("Request to get all DanTocs");
        return danTocRepository.findAll().stream()
            .map(danTocMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one danToc by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DanTocDTO> findOne(Long id) {
        log.debug("Request to get DanToc : {}", id);
        return danTocRepository.findById(id)
            .map(danTocMapper::toDto);
    }

    /**
     * Delete the danToc by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DanToc : {}", id);
        danTocRepository.deleteById(id);
    }
}
