package vn.vnpt.service.impl;

import vn.vnpt.domain.ChiDinhXetNghiemId;
import vn.vnpt.service.ChiDinhXetNghiemService;
import vn.vnpt.domain.ChiDinhXetNghiem;
import vn.vnpt.repository.ChiDinhXetNghiemRepository;
import vn.vnpt.service.dto.ChiDinhXetNghiemDTO;
import vn.vnpt.service.mapper.ChiDinhXetNghiemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ChiDinhXetNghiem}.
 */
@Service
@Transactional
public class ChiDinhXetNghiemServiceImpl implements ChiDinhXetNghiemService {

    private final Logger log = LoggerFactory.getLogger(ChiDinhXetNghiemServiceImpl.class);

    private final ChiDinhXetNghiemRepository chiDinhXetNghiemRepository;

    private final ChiDinhXetNghiemMapper chiDinhXetNghiemMapper;

    public ChiDinhXetNghiemServiceImpl(ChiDinhXetNghiemRepository chiDinhXetNghiemRepository, ChiDinhXetNghiemMapper chiDinhXetNghiemMapper) {
        this.chiDinhXetNghiemRepository = chiDinhXetNghiemRepository;
        this.chiDinhXetNghiemMapper = chiDinhXetNghiemMapper;
    }

    /**
     * Save a chiDinhXetNghiem.
     *
     * @param chiDinhXetNghiemDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ChiDinhXetNghiemDTO save(ChiDinhXetNghiemDTO chiDinhXetNghiemDTO) {
        log.debug("Request to save ChiDinhXetNghiem : {}", chiDinhXetNghiemDTO);
        ChiDinhXetNghiem chiDinhXetNghiem = chiDinhXetNghiemMapper.toEntity(chiDinhXetNghiemDTO);
        chiDinhXetNghiem = chiDinhXetNghiemRepository.save(chiDinhXetNghiem);
        return chiDinhXetNghiemMapper.toDto(chiDinhXetNghiem);
    }

    /**
     * Get all the chiDinhXetNghiems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ChiDinhXetNghiemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ChiDinhXetNghiems");
        return chiDinhXetNghiemRepository.findAll(pageable)
            .map(chiDinhXetNghiemMapper::toDto);
    }

    /**
     * Get one chiDinhXetNghiem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ChiDinhXetNghiemDTO> findOne(ChiDinhXetNghiemId id) {
        log.debug("Request to get ChiDinhXetNghiem : {}", id);
        return chiDinhXetNghiemRepository.findById(id)
            .map(chiDinhXetNghiemMapper::toDto);
    }

    /**
     * Delete the chiDinhXetNghiem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(ChiDinhXetNghiemId id) {
        log.debug("Request to delete ChiDinhXetNghiem : {}", id);
        chiDinhXetNghiemRepository.deleteById(id);
    }
}
