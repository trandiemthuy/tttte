package vn.vnpt.service.impl;

import vn.vnpt.service.XetNghiemService;
import vn.vnpt.domain.XetNghiem;
import vn.vnpt.repository.XetNghiemRepository;
import vn.vnpt.service.dto.XetNghiemDTO;
import vn.vnpt.service.mapper.XetNghiemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link XetNghiem}.
 */
@Service
@Transactional
public class XetNghiemServiceImpl implements XetNghiemService {

    private final Logger log = LoggerFactory.getLogger(XetNghiemServiceImpl.class);

    private final XetNghiemRepository xetNghiemRepository;

    private final XetNghiemMapper xetNghiemMapper;

    public XetNghiemServiceImpl(XetNghiemRepository xetNghiemRepository, XetNghiemMapper xetNghiemMapper) {
        this.xetNghiemRepository = xetNghiemRepository;
        this.xetNghiemMapper = xetNghiemMapper;
    }

    /**
     * Save a xetNghiem.
     *
     * @param xetNghiemDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public XetNghiemDTO save(XetNghiemDTO xetNghiemDTO) {
        log.debug("Request to save XetNghiem : {}", xetNghiemDTO);
        XetNghiem xetNghiem = xetNghiemMapper.toEntity(xetNghiemDTO);
        xetNghiem = xetNghiemRepository.save(xetNghiem);
        return xetNghiemMapper.toDto(xetNghiem);
    }

    /**
     * Get all the xetNghiems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<XetNghiemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all XetNghiems");
        return xetNghiemRepository.findAll(pageable)
            .map(xetNghiemMapper::toDto);
    }

    /**
     * Get one xetNghiem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<XetNghiemDTO> findOne(Long id) {
        log.debug("Request to get XetNghiem : {}", id);
        return xetNghiemRepository.findById(id)
            .map(xetNghiemMapper::toDto);
    }

    /**
     * Delete the xetNghiem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete XetNghiem : {}", id);
        xetNghiemRepository.deleteById(id);
    }
}
