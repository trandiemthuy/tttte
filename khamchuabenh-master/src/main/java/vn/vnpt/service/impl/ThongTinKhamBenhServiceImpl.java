package vn.vnpt.service.impl;

import vn.vnpt.domain.ThongTinKhamBenhId;
import vn.vnpt.service.ThongTinKhamBenhService;
import vn.vnpt.domain.ThongTinKhamBenh;
import vn.vnpt.repository.ThongTinKhamBenhRepository;
import vn.vnpt.service.dto.ThongTinKhamBenhDTO;
import vn.vnpt.service.mapper.ThongTinKhamBenhMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ThongTinKhamBenh}.
 */
@Service
@Transactional
public class ThongTinKhamBenhServiceImpl implements ThongTinKhamBenhService {

    private final Logger log = LoggerFactory.getLogger(ThongTinKhamBenhServiceImpl.class);

    private final ThongTinKhamBenhRepository thongTinKhamBenhRepository;

    private final ThongTinKhamBenhMapper thongTinKhamBenhMapper;

    public ThongTinKhamBenhServiceImpl(ThongTinKhamBenhRepository thongTinKhamBenhRepository, ThongTinKhamBenhMapper thongTinKhamBenhMapper) {
        this.thongTinKhamBenhRepository = thongTinKhamBenhRepository;
        this.thongTinKhamBenhMapper = thongTinKhamBenhMapper;
    }

    /**
     * Save a thongTinKhamBenh.
     *
     * @param thongTinKhamBenhDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ThongTinKhamBenhDTO save(ThongTinKhamBenhDTO thongTinKhamBenhDTO) {
        log.debug("Request to save ThongTinKhamBenh : {}", thongTinKhamBenhDTO);
        ThongTinKhamBenh thongTinKhamBenh = thongTinKhamBenhMapper.toEntity(thongTinKhamBenhDTO);
        log.debug("Thông tin khám bệnh sau khi map : {}", thongTinKhamBenh);
        thongTinKhamBenh = thongTinKhamBenhRepository.save(thongTinKhamBenh);
        thongTinKhamBenhRepository.flush();
        log.debug("Thông tin khám bệnh sau khi save : {}", thongTinKhamBenh);
        return thongTinKhamBenhMapper.toDto(thongTinKhamBenh);
    }

    /**
     * Get all the thongTinKhamBenhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ThongTinKhamBenhDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ThongTinKhamBenhs");
        return thongTinKhamBenhRepository.findAll(pageable)
            .map(thongTinKhamBenhMapper::toDto);
    }


    /**
     * Get one thongTinKhamBenh by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ThongTinKhamBenhDTO> findOne(ThongTinKhamBenhId id) {
        log.debug("Request to get ThongTinKhamBenh : {}", id);
        return thongTinKhamBenhRepository.findById(id)
            .map(thongTinKhamBenhMapper::toDto);
    }

    /**
     * Delete the thongTinKhamBenh by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(ThongTinKhamBenhId id) {
        log.debug("Request to delete ThongTinKhamBenh : {}", id);
        thongTinKhamBenhRepository.deleteById(id);
        thongTinKhamBenhRepository.flush();
    }
}
