package vn.vnpt.service.impl;

import vn.vnpt.service.HuongDieuTriService;
import vn.vnpt.domain.HuongDieuTri;
import vn.vnpt.repository.HuongDieuTriRepository;
import vn.vnpt.service.dto.HuongDieuTriDTO;
import vn.vnpt.service.mapper.HuongDieuTriMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link HuongDieuTri}.
 */
@Service
@Transactional
public class HuongDieuTriServiceImpl implements HuongDieuTriService {

    private final Logger log = LoggerFactory.getLogger(HuongDieuTriServiceImpl.class);

    private final HuongDieuTriRepository huongDieuTriRepository;

    private final HuongDieuTriMapper huongDieuTriMapper;

    public HuongDieuTriServiceImpl(HuongDieuTriRepository huongDieuTriRepository, HuongDieuTriMapper huongDieuTriMapper) {
        this.huongDieuTriRepository = huongDieuTriRepository;
        this.huongDieuTriMapper = huongDieuTriMapper;
    }

    /**
     * Save a huongDieuTri.
     *
     * @param huongDieuTriDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public HuongDieuTriDTO save(HuongDieuTriDTO huongDieuTriDTO) {
        log.debug("Request to save HuongDieuTri : {}", huongDieuTriDTO);
        HuongDieuTri huongDieuTri = huongDieuTriMapper.toEntity(huongDieuTriDTO);
        huongDieuTri = huongDieuTriRepository.save(huongDieuTri);
        return huongDieuTriMapper.toDto(huongDieuTri);
    }

    /**
     * Get all the huongDieuTris.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<HuongDieuTriDTO> findAll(Pageable pageable) {
        log.debug("Request to get all HuongDieuTris");
        return huongDieuTriRepository.findAll(pageable)
            .map(huongDieuTriMapper::toDto);
    }


    /**
     * Get one huongDieuTri by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<HuongDieuTriDTO> findOne(Long id) {
        log.debug("Request to get HuongDieuTri : {}", id);
        return huongDieuTriRepository.findById(id)
            .map(huongDieuTriMapper::toDto);
    }

    /**
     * Delete the huongDieuTri by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete HuongDieuTri : {}", id);
        huongDieuTriRepository.deleteById(id);
    }
}
