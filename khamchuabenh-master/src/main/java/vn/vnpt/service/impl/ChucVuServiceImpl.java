package vn.vnpt.service.impl;

import vn.vnpt.service.ChucVuService;
import vn.vnpt.domain.ChucVu;
import vn.vnpt.repository.ChucVuRepository;
import vn.vnpt.service.dto.ChucVuDTO;
import vn.vnpt.service.mapper.ChucVuMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ChucVu}.
 */
@Service
@Transactional
public class ChucVuServiceImpl implements ChucVuService {

    private final Logger log = LoggerFactory.getLogger(ChucVuServiceImpl.class);

    private final ChucVuRepository chucVuRepository;

    private final ChucVuMapper chucVuMapper;

    public ChucVuServiceImpl(ChucVuRepository chucVuRepository, ChucVuMapper chucVuMapper) {
        this.chucVuRepository = chucVuRepository;
        this.chucVuMapper = chucVuMapper;
    }

    /**
     * Save a chucVu.
     *
     * @param chucVuDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ChucVuDTO save(ChucVuDTO chucVuDTO) {
        log.debug("Request to save ChucVu : {}", chucVuDTO);
        ChucVu chucVu = chucVuMapper.toEntity(chucVuDTO);
        chucVu = chucVuRepository.save(chucVu);
        return chucVuMapper.toDto(chucVu);
    }

    /**
     * Get all the chucVus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ChucVuDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ChucVus");
        return chucVuRepository.findAll(pageable)
            .map(chucVuMapper::toDto);
    }

    /**
     * Get one chucVu by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ChucVuDTO> findOne(Long id) {
        log.debug("Request to get ChucVu : {}", id);
        return chucVuRepository.findById(id)
            .map(chucVuMapper::toDto);
    }

    /**
     * Delete the chucVu by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ChucVu : {}", id);
        chucVuRepository.deleteById(id);
    }
}
