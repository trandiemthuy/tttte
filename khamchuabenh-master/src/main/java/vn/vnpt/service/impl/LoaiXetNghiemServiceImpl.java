package vn.vnpt.service.impl;

import vn.vnpt.service.LoaiXetNghiemService;
import vn.vnpt.domain.LoaiXetNghiem;
import vn.vnpt.repository.LoaiXetNghiemRepository;
import vn.vnpt.service.dto.LoaiXetNghiemDTO;
import vn.vnpt.service.mapper.LoaiXetNghiemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link LoaiXetNghiem}.
 */
@Service
@Transactional
public class LoaiXetNghiemServiceImpl implements LoaiXetNghiemService {

    private final Logger log = LoggerFactory.getLogger(LoaiXetNghiemServiceImpl.class);

    private final LoaiXetNghiemRepository loaiXetNghiemRepository;

    private final LoaiXetNghiemMapper loaiXetNghiemMapper;

    public LoaiXetNghiemServiceImpl(LoaiXetNghiemRepository loaiXetNghiemRepository, LoaiXetNghiemMapper loaiXetNghiemMapper) {
        this.loaiXetNghiemRepository = loaiXetNghiemRepository;
        this.loaiXetNghiemMapper = loaiXetNghiemMapper;
    }

    /**
     * Save a loaiXetNghiem.
     *
     * @param loaiXetNghiemDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public LoaiXetNghiemDTO save(LoaiXetNghiemDTO loaiXetNghiemDTO) {
        log.debug("Request to save LoaiXetNghiem : {}", loaiXetNghiemDTO);
        LoaiXetNghiem loaiXetNghiem = loaiXetNghiemMapper.toEntity(loaiXetNghiemDTO);
        loaiXetNghiem = loaiXetNghiemRepository.save(loaiXetNghiem);
        return loaiXetNghiemMapper.toDto(loaiXetNghiem);
    }

    /**
     * Get all the loaiXetNghiems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LoaiXetNghiemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LoaiXetNghiems");
        return loaiXetNghiemRepository.findAll(pageable)
            .map(loaiXetNghiemMapper::toDto);
    }

    /**
     * Get one loaiXetNghiem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LoaiXetNghiemDTO> findOne(Long id) {
        log.debug("Request to get LoaiXetNghiem : {}", id);
        return loaiXetNghiemRepository.findById(id)
            .map(loaiXetNghiemMapper::toDto);
    }

    /**
     * Delete the loaiXetNghiem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LoaiXetNghiem : {}", id);
        loaiXetNghiemRepository.deleteById(id);
    }
}
