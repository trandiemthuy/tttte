package vn.vnpt.service.impl;

import vn.vnpt.service.ThongTinBhxhService;
import vn.vnpt.domain.ThongTinBhxh;
import vn.vnpt.repository.ThongTinBhxhRepository;
import vn.vnpt.service.dto.ThongTinBhxhDTO;
import vn.vnpt.service.mapper.ThongTinBhxhMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ThongTinBhxh}.
 */
@Service
@Transactional
public class ThongTinBhxhServiceImpl implements ThongTinBhxhService {

    private final Logger log = LoggerFactory.getLogger(ThongTinBhxhServiceImpl.class);

    private final ThongTinBhxhRepository thongTinBhxhRepository;

    private final ThongTinBhxhMapper thongTinBhxhMapper;

    public ThongTinBhxhServiceImpl(ThongTinBhxhRepository thongTinBhxhRepository, ThongTinBhxhMapper thongTinBhxhMapper) {
        this.thongTinBhxhRepository = thongTinBhxhRepository;
        this.thongTinBhxhMapper = thongTinBhxhMapper;
    }

    /**
     * Save a thongTinBhxh.
     *
     * @param thongTinBhxhDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ThongTinBhxhDTO save(ThongTinBhxhDTO thongTinBhxhDTO) {
        log.debug("Request to save ThongTinBhxh : {}", thongTinBhxhDTO);
        ThongTinBhxh thongTinBhxh = thongTinBhxhMapper.toEntity(thongTinBhxhDTO);
        thongTinBhxh = thongTinBhxhRepository.save(thongTinBhxh);
        return thongTinBhxhMapper.toDto(thongTinBhxh);
    }

    /**
     * Get all the thongTinBhxhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ThongTinBhxhDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ThongTinBhxhs");
        return thongTinBhxhRepository.findAll(pageable)
            .map(thongTinBhxhMapper::toDto);
    }

    /**
     * Get one thongTinBhxh by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ThongTinBhxhDTO> findOne(Long id) {
        log.debug("Request to get ThongTinBhxh : {}", id);
        return thongTinBhxhRepository.findById(id)
            .map(thongTinBhxhMapper::toDto);
    }

    /**
     * Delete the thongTinBhxh by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ThongTinBhxh : {}", id);
        thongTinBhxhRepository.deleteById(id);
    }
}
