package vn.vnpt.service.impl;

import vn.vnpt.service.PhongService;
import vn.vnpt.domain.Phong;
import vn.vnpt.repository.PhongRepository;
import vn.vnpt.service.dto.PhongDTO;
import vn.vnpt.service.mapper.PhongMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Phong}.
 */
@Service
@Transactional
public class PhongServiceImpl implements PhongService {

    private final Logger log = LoggerFactory.getLogger(PhongServiceImpl.class);

    private final PhongRepository phongRepository;

    private final PhongMapper phongMapper;

    public PhongServiceImpl(PhongRepository phongRepository, PhongMapper phongMapper) {
        this.phongRepository = phongRepository;
        this.phongMapper = phongMapper;
    }

    /**
     * Save a phong.
     *
     * @param phongDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PhongDTO save(PhongDTO phongDTO) {
        log.debug("Request to save Phong : {}", phongDTO);
        Phong phong = phongMapper.toEntity(phongDTO);
        phong = phongRepository.save(phong);
        return phongMapper.toDto(phong);
    }

    /**
     * Get all the phongs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PhongDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Phongs");
        return phongRepository.findAll(pageable)
            .map(phongMapper::toDto);
    }

    /**
     * Get one phong by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PhongDTO> findOne(Long id) {
        log.debug("Request to get Phong : {}", id);
        return phongRepository.findById(id)
            .map(phongMapper::toDto);
    }

    /**
     * Delete the phong by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Phong : {}", id);
        phongRepository.deleteById(id);
    }
}
