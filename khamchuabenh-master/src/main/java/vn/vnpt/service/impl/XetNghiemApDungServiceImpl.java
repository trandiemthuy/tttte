package vn.vnpt.service.impl;

import vn.vnpt.service.XetNghiemApDungService;
import vn.vnpt.domain.XetNghiemApDung;
import vn.vnpt.repository.XetNghiemApDungRepository;
import vn.vnpt.service.dto.XetNghiemApDungDTO;
import vn.vnpt.service.mapper.XetNghiemApDungMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link XetNghiemApDung}.
 */
@Service
@Transactional
public class XetNghiemApDungServiceImpl implements XetNghiemApDungService {

    private final Logger log = LoggerFactory.getLogger(XetNghiemApDungServiceImpl.class);

    private final XetNghiemApDungRepository xetNghiemApDungRepository;

    private final XetNghiemApDungMapper xetNghiemApDungMapper;

    public XetNghiemApDungServiceImpl(XetNghiemApDungRepository xetNghiemApDungRepository, XetNghiemApDungMapper xetNghiemApDungMapper) {
        this.xetNghiemApDungRepository = xetNghiemApDungRepository;
        this.xetNghiemApDungMapper = xetNghiemApDungMapper;
    }

    /**
     * Save a xetNghiemApDung.
     *
     * @param xetNghiemApDungDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public XetNghiemApDungDTO save(XetNghiemApDungDTO xetNghiemApDungDTO) {
        log.debug("Request to save XetNghiemApDung : {}", xetNghiemApDungDTO);
        XetNghiemApDung xetNghiemApDung = xetNghiemApDungMapper.toEntity(xetNghiemApDungDTO);
        xetNghiemApDung = xetNghiemApDungRepository.save(xetNghiemApDung);
        return xetNghiemApDungMapper.toDto(xetNghiemApDung);
    }

    /**
     * Get all the xetNghiemApDungs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<XetNghiemApDungDTO> findAll(Pageable pageable) {
        log.debug("Request to get all XetNghiemApDungs");
        return xetNghiemApDungRepository.findAll(pageable)
            .map(xetNghiemApDungMapper::toDto);
    }

    /**
     * Get one xetNghiemApDung by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<XetNghiemApDungDTO> findOne(Long id) {
        log.debug("Request to get XetNghiemApDung : {}", id);
        return xetNghiemApDungRepository.findById(id)
            .map(xetNghiemApDungMapper::toDto);
    }

    /**
     * Delete the xetNghiemApDung by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete XetNghiemApDung : {}", id);
        xetNghiemApDungRepository.deleteById(id);
    }
}
