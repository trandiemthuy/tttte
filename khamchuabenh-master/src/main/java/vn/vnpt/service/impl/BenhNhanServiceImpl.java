package vn.vnpt.service.impl;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.LongFilter;
import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.service.BenhNhanService;
import vn.vnpt.domain.BenhNhan;
import vn.vnpt.repository.BenhNhanRepository;
import vn.vnpt.service.DoiTuongBhytQueryService;
import vn.vnpt.service.DoiTuongBhytService;
import vn.vnpt.service.TheBhytQueryService;
import vn.vnpt.service.dto.*;
import vn.vnpt.service.dto.customdto.TTBenhNhanTongHopDTO;
import vn.vnpt.service.mapper.BenhNhanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Implementation for managing {@link BenhNhan}.
 */
@Service
@Transactional
public class BenhNhanServiceImpl implements BenhNhanService {

    private final Logger log = LoggerFactory.getLogger(BenhNhanServiceImpl.class);

    private final BenhNhanRepository benhNhanRepository;

    private final BenhNhanMapper benhNhanMapper;

    private final TheBhytQueryService theBhytQueryService;

    @Autowired
    private DoiTuongBhytService doiTuongBhytService;
    @Autowired
    private DoiTuongBhytQueryService doiTuongBhytQueryService;

    public BenhNhanServiceImpl(BenhNhanRepository benhNhanRepository, BenhNhanMapper benhNhanMapper, TheBhytQueryService theBhytQueryService) {
        this.benhNhanRepository = benhNhanRepository;
        this.benhNhanMapper = benhNhanMapper;
        this.theBhytQueryService = theBhytQueryService;
    }

    /**
     * Save a benhNhan.
     *
     * @param benhNhanDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BenhNhanDTO save(BenhNhanDTO benhNhanDTO) {
        log.debug("Request to save BenhNhan : {}", benhNhanDTO);
        BenhNhan benhNhan = benhNhanMapper.toEntity(benhNhanDTO);
        benhNhan = benhNhanRepository.save(benhNhan);
        return benhNhanMapper.toDto(benhNhan);
    }

    /**
     * Get all the benhNhans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BenhNhanDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BenhNhans");
        return benhNhanRepository.findAll(pageable)
            .map(benhNhanMapper::toDto);
    }


    /**
     * Get one benhNhan by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BenhNhanDTO> findOne(Long id) {
        log.debug("Request to get BenhNhan : {}", id);
        return benhNhanRepository.findById(id)
            .map(benhNhanMapper::toDto);
    }

    /**
     * Delete the benhNhan by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BenhNhan : {}", id);
        benhNhanRepository.deleteById(id);
    }

    @Override
    public Boolean hasBHYT(BenhNhanDTO benhNhanDTO){
        LongFilter filter = new LongFilter();
        filter.setEquals(benhNhanDTO.getId());
        TheBhytCriteria criteria = new TheBhytCriteria();
        criteria.setBenhNhanId(filter);
        if(theBhytQueryService.countByCriteria(criteria) != 0)
            return true;
        return false;
    }

    @Override
    public BenhNhanDTO getThongTinLienQuanBhyt(BenhNhanDTO benhNhanDTO){
        BenhNhanDTO result = new BenhNhanDTO();
        result.setId(benhNhanDTO.getId());
        result.setTen(benhNhanDTO.getTen());
        result.setNgaySinh(benhNhanDTO.getNgaySinh());
        result.setChiCoNamSinh(benhNhanDTO.isChiCoNamSinh());
        result.setGioiTinh(benhNhanDTO.getGioiTinh());
//        result.setDiaChiBhyt(benhNhanDTO.getDiaChiBhyt());
        return  result;
    }

    @Override
    public Optional<TTBenhNhanTongHopDTO> getThongTinBenhNhanTongHopDTO(Long benhNhanId) {
        BenhNhanDTO benhNhanDTO = findOne(benhNhanId).get();

        LongFilter benhNhanFilter = new LongFilter();
        benhNhanFilter.setEquals(benhNhanId);
        BooleanFilter enabledFilter = new BooleanFilter();
        enabledFilter.setEquals(true);
        TheBhytCriteria theBhytCriteria = new TheBhytCriteria();
        theBhytCriteria.setBenhNhanId(benhNhanFilter);
        theBhytCriteria.setEnabled(enabledFilter);
        Long soLuongBhyt = theBhytQueryService.countByCriteria(theBhytCriteria);

        if ( soLuongBhyt != 0){
            if (soLuongBhyt == 1){
                TheBhytDTO theBhytDTO = theBhytQueryService.findByCriteria(theBhytCriteria).get(0);
                DoiTuongBhytDTO doiTuongBhytDTO = doiTuongBhytService.findOne(theBhytDTO.getDoiTuongBhytId()).get();
                return Optional.of(new TTBenhNhanTongHopDTO(benhNhanDTO,theBhytDTO,doiTuongBhytDTO));
            }
            else {
                int tyLeMax = 0;
                TheBhytDTO theBhytDTOResult = new TheBhytDTO();
                DoiTuongBhytDTO doiTuongBhytDTOResult = new DoiTuongBhytDTO();
                List<TheBhytDTO> theBhytDTOList = theBhytQueryService.findByCriteria(theBhytCriteria);
                for (TheBhytDTO theBhytDTO : theBhytDTOList) {
                    DoiTuongBhytDTO doiTuongBhytDTO = doiTuongBhytService.findOne(theBhytDTO.getDoiTuongBhytId()).get();
                    if (doiTuongBhytDTO.getTyLeMienGiam() > tyLeMax){
                        theBhytDTOResult = theBhytDTO;
                        doiTuongBhytDTOResult = doiTuongBhytDTO;
                        tyLeMax = doiTuongBhytDTO.getTyLeMienGiam();
                    }
                }
                return Optional.of(new TTBenhNhanTongHopDTO(benhNhanDTO,theBhytDTOResult,doiTuongBhytDTOResult));
            }
        }
        return Optional.of(new TTBenhNhanTongHopDTO(benhNhanDTO,null,null));
    }
}
