package vn.vnpt.service.impl;

import io.github.jhipster.service.filter.LongFilter;
import vn.vnpt.service.DotDieuTriService;
import vn.vnpt.domain.DotDieuTri;
import vn.vnpt.repository.DotDieuTriRepository;
import vn.vnpt.service.dto.DotDieuTriCriteria;
import vn.vnpt.service.dto.DotDieuTriDTO;
import vn.vnpt.service.mapper.DotDieuTriMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import vn.vnpt.domain.DotDieuTriId;

/**
 * Service Implementation for managing {@link DotDieuTri}.
 */
@Service
@Transactional
public class DotDieuTriServiceImpl implements DotDieuTriService {

    private final Logger log = LoggerFactory.getLogger(DotDieuTriServiceImpl.class);

    private final DotDieuTriRepository dotDieuTriRepository;

    private final DotDieuTriMapper dotDieuTriMapper;

    public DotDieuTriServiceImpl(DotDieuTriRepository dotDieuTriRepository, DotDieuTriMapper dotDieuTriMapper) {
        this.dotDieuTriRepository = dotDieuTriRepository;
        this.dotDieuTriMapper = dotDieuTriMapper;
    }

    /**
     * Save a dotDieuTri.
     *
     * @param dotDieuTriDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DotDieuTriDTO save(DotDieuTriDTO dotDieuTriDTO) {
        log.debug("Request to save DotDieuTri : {}", dotDieuTriDTO);
        DotDieuTri dotDieuTri = dotDieuTriMapper.toEntity(dotDieuTriDTO);
        log.debug("Dot dieu tri sau khi map: {}", dotDieuTri);
        dotDieuTri = dotDieuTriRepository.save(dotDieuTri);
        log.debug("Dot dieu tri sau khi save: {}", dotDieuTri);
        return dotDieuTriMapper.toDto(dotDieuTri);
    }

    /**
     * Get all the dotDieuTris.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DotDieuTriDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DotDieuTris");
        return dotDieuTriRepository.findAll(pageable)
            .map(dotDieuTriMapper::toDto);
    }

    /**
     * Get one dotDieuTri by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DotDieuTriDTO> findOne(DotDieuTriId id) {
        log.debug("Request to get DotDieuTri : {}", id);
        return dotDieuTriRepository.findById(id)
            .map(dotDieuTriMapper::toDto);
    }

    /**
     * Delete the dotDieuTri by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(DotDieuTriId id) {
        log.debug("Request to delete DotDieuTri : {}", id);
        dotDieuTriRepository.deleteById(id);
    }

    @Override
    public Optional<DotDieuTriDTO> makeDotDieuTriDefault(Boolean coBaoHiem, Map thongTin) {
        DotDieuTriDTO  result = new DotDieuTriDTO();
        if(coBaoHiem){
            result.setCoBaoHiem(coBaoHiem);
            result.setBhytTyLeMienGiam((Integer)(thongTin.get("tyLeMienGiam")));
            result.setBhytTyLeMienGiamKtc((Integer)(thongTin.get("tyLeMienGiamKtc")));
            result.setBhytCanTren((Integer) thongTin.get("canTren"));
            result.setBhytCanTrenKtc((Integer) thongTin.get("canTrenKtc"));
            result.setDoiTuongBhytTen(thongTin.get("doiTuongBhyt").toString());
        }
        return Optional.of(result);
    }

    @Override
    public DotDieuTriDTO skipThongTinBhyt(DotDieuTriDTO dotDieuTriDTO) {
        DotDieuTriDTO result = new DotDieuTriDTO();
        result.setCompositeId(dotDieuTriDTO.getCompositeId());
        result.setSoThuTu(dotDieuTriDTO.getSoThuTu());
        result.setGiayToTreEm(dotDieuTriDTO.getGiayToTreEm());
        result.setLoaiGiayToTreEm(dotDieuTriDTO.getLoaiGiayToTreEm());
        result.setTrangThai(dotDieuTriDTO.getTrangThai());
        result.setLoai(dotDieuTriDTO.getLoai());
        result.setCoBaoHiem(dotDieuTriDTO.isCoBaoHiem());
        result.setNam(dotDieuTriDTO.getNam());
        return result;
    }
}
