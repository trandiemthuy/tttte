package vn.vnpt.service.impl;

import vn.vnpt.service.TPhanNhomXetNghiemService;
import vn.vnpt.domain.TPhanNhomXetNghiem;
import vn.vnpt.repository.TPhanNhomXetNghiemRepository;
import vn.vnpt.service.dto.TPhanNhomXetNghiemDTO;
import vn.vnpt.service.mapper.TPhanNhomXetNghiemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TPhanNhomXetNghiem}.
 */
@Service
@Transactional
public class TPhanNhomXetNghiemServiceImpl implements TPhanNhomXetNghiemService {

    private final Logger log = LoggerFactory.getLogger(TPhanNhomXetNghiemServiceImpl.class);

    private final TPhanNhomXetNghiemRepository tPhanNhomXetNghiemRepository;

    private final TPhanNhomXetNghiemMapper tPhanNhomXetNghiemMapper;

    public TPhanNhomXetNghiemServiceImpl(TPhanNhomXetNghiemRepository tPhanNhomXetNghiemRepository, TPhanNhomXetNghiemMapper tPhanNhomXetNghiemMapper) {
        this.tPhanNhomXetNghiemRepository = tPhanNhomXetNghiemRepository;
        this.tPhanNhomXetNghiemMapper = tPhanNhomXetNghiemMapper;
    }

    /**
     * Save a tPhanNhomXetNghiem.
     *
     * @param tPhanNhomXetNghiemDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TPhanNhomXetNghiemDTO save(TPhanNhomXetNghiemDTO tPhanNhomXetNghiemDTO) {
        log.debug("Request to save TPhanNhomXetNghiem : {}", tPhanNhomXetNghiemDTO);
        TPhanNhomXetNghiem tPhanNhomXetNghiem = tPhanNhomXetNghiemMapper.toEntity(tPhanNhomXetNghiemDTO);
        tPhanNhomXetNghiem = tPhanNhomXetNghiemRepository.save(tPhanNhomXetNghiem);
        return tPhanNhomXetNghiemMapper.toDto(tPhanNhomXetNghiem);
    }

    /**
     * Get all the tPhanNhomXetNghiems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TPhanNhomXetNghiemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TPhanNhomXetNghiems");
        return tPhanNhomXetNghiemRepository.findAll(pageable)
            .map(tPhanNhomXetNghiemMapper::toDto);
    }

    /**
     * Get one tPhanNhomXetNghiem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TPhanNhomXetNghiemDTO> findOne(Long id) {
        log.debug("Request to get TPhanNhomXetNghiem : {}", id);
        return tPhanNhomXetNghiemRepository.findById(id)
            .map(tPhanNhomXetNghiemMapper::toDto);
    }

    /**
     * Delete the tPhanNhomXetNghiem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TPhanNhomXetNghiem : {}", id);
        tPhanNhomXetNghiemRepository.deleteById(id);
    }
}
