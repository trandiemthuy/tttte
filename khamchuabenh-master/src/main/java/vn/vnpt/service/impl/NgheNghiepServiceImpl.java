package vn.vnpt.service.impl;

import vn.vnpt.service.NgheNghiepService;
import vn.vnpt.domain.NgheNghiep;
import vn.vnpt.repository.NgheNghiepRepository;
import vn.vnpt.service.dto.NgheNghiepDTO;
import vn.vnpt.service.mapper.NgheNghiepMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link NgheNghiep}.
 */
@Service
@Transactional
public class NgheNghiepServiceImpl implements NgheNghiepService {

    private final Logger log = LoggerFactory.getLogger(NgheNghiepServiceImpl.class);

    private final NgheNghiepRepository ngheNghiepRepository;

    private final NgheNghiepMapper ngheNghiepMapper;

    public NgheNghiepServiceImpl(NgheNghiepRepository ngheNghiepRepository, NgheNghiepMapper ngheNghiepMapper) {
        this.ngheNghiepRepository = ngheNghiepRepository;
        this.ngheNghiepMapper = ngheNghiepMapper;
    }

    /**
     * Save a ngheNghiep.
     *
     * @param ngheNghiepDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public NgheNghiepDTO save(NgheNghiepDTO ngheNghiepDTO) {
        log.debug("Request to save NgheNghiep : {}", ngheNghiepDTO);
        NgheNghiep ngheNghiep = ngheNghiepMapper.toEntity(ngheNghiepDTO);
        ngheNghiep = ngheNghiepRepository.save(ngheNghiep);
        return ngheNghiepMapper.toDto(ngheNghiep);
    }

    /**
     * Get all the ngheNghieps.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<NgheNghiepDTO> findAll() {
        log.debug("Request to get all NgheNghieps");
        return ngheNghiepRepository.findAll().stream()
            .map(ngheNghiepMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one ngheNghiep by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NgheNghiepDTO> findOne(Long id) {
        log.debug("Request to get NgheNghiep : {}", id);
        return ngheNghiepRepository.findById(id)
            .map(ngheNghiepMapper::toDto);
    }

    /**
     * Delete the ngheNghiep by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NgheNghiep : {}", id);
        ngheNghiepRepository.deleteById(id);
    }
}
