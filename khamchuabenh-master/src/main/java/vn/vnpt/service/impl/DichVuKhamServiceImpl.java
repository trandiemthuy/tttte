package vn.vnpt.service.impl;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.DichVuKhamApDung;
import vn.vnpt.service.DichVuKhamApDungQueryService;
import vn.vnpt.service.DichVuKhamQueryService;
import vn.vnpt.service.DichVuKhamService;
import vn.vnpt.domain.DichVuKham;
import vn.vnpt.repository.DichVuKhamRepository;
import vn.vnpt.service.dto.DichVuKhamApDungCriteria;
import vn.vnpt.service.dto.DichVuKhamApDungDTO;
import vn.vnpt.service.dto.DichVuKhamCriteria;
import vn.vnpt.service.dto.DichVuKhamDTO;
import vn.vnpt.service.dto.customdto.ThongTinDichVuKhamDTO;
import vn.vnpt.service.mapper.DichVuKhamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.service.mapper.custommapper.ThongTinDichVuKhamMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link DichVuKham}.
 */
@Service
@Transactional
public class DichVuKhamServiceImpl implements DichVuKhamService {

    private final Logger log = LoggerFactory.getLogger(DichVuKhamServiceImpl.class);

    private final DichVuKhamRepository dichVuKhamRepository;

    private final DichVuKhamMapper dichVuKhamMapper;
    @Autowired
    private DichVuKhamQueryService dichVuKhamQueryService;
    @Autowired
    private DichVuKhamApDungQueryService dichVuKhamApDungQueryService;
    @Autowired
    private ThongTinDichVuKhamMapper thongTinDichVuKhamMapper;
    public DichVuKhamServiceImpl(DichVuKhamRepository dichVuKhamRepository, DichVuKhamMapper dichVuKhamMapper) {
        this.dichVuKhamRepository = dichVuKhamRepository;
        this.dichVuKhamMapper = dichVuKhamMapper;
    }

    /**
     * Save a dichVuKham.
     *
     * @param dichVuKhamDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DichVuKhamDTO save(DichVuKhamDTO dichVuKhamDTO) {
        log.debug("Request to save DichVuKham : {}", dichVuKhamDTO);
        DichVuKham dichVuKham = dichVuKhamMapper.toEntity(dichVuKhamDTO);
        dichVuKham = dichVuKhamRepository.save(dichVuKham);
        return dichVuKhamMapper.toDto(dichVuKham);
    }

    /**
     * Get all the dichVuKhams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DichVuKhamDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DichVuKhams");
        return dichVuKhamRepository.findAll(pageable)
            .map(dichVuKhamMapper::toDto);
    }

    /**
     * Get one dichVuKham by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DichVuKhamDTO> findOne(Long id) {
        log.debug("Request to get DichVuKham : {}", id);
        return dichVuKhamRepository.findById(id)
            .map(dichVuKhamMapper::toDto);
    }

    /**
     * Delete the dichVuKham by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DichVuKham : {}", id);
        dichVuKhamRepository.deleteById(id);
    }

    @Override
    public List<ThongTinDichVuKhamDTO> getDanhSachDichVuKham() {
        IntegerFilter enabledFilter = new IntegerFilter();
        enabledFilter.setEquals(1);
        DichVuKhamCriteria criteria = new DichVuKhamCriteria();
        criteria.setEnabled(enabledFilter);
        List<DichVuKhamDTO> dichVuKhamDTOList = dichVuKhamQueryService.findByCriteria(criteria);
        LongFilter dichVuKhamIdFilter = new LongFilter();
        DichVuKhamApDungCriteria dichVuKhamApDungCriteria = new DichVuKhamApDungCriteria();
        dichVuKhamApDungCriteria.setEnabled(enabledFilter);

        List<ThongTinDichVuKhamDTO> result = new ArrayList<>();
        for (DichVuKhamDTO dichVuKhamDTO : dichVuKhamDTOList){
            dichVuKhamIdFilter.setEquals(dichVuKhamDTO.getId());
            dichVuKhamApDungCriteria.setDichVuKhamId(dichVuKhamIdFilter);
            DichVuKhamApDungDTO dichVuKhamApDungDTO = new DichVuKhamApDungDTO();
            if (dichVuKhamApDungQueryService.countByCriteria(dichVuKhamApDungCriteria) > 0)
                dichVuKhamApDungDTO = dichVuKhamApDungQueryService.findByCriteria(dichVuKhamApDungCriteria).get(0);
            result.add(thongTinDichVuKhamMapper.toThongTinDichVuKhamDTO(dichVuKhamDTO,dichVuKhamApDungDTO));
        }
        return result;
    }
}
