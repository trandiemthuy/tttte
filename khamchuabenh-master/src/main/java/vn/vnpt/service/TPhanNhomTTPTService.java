package vn.vnpt.service;

import vn.vnpt.service.dto.TPhanNhomTTPTDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.TPhanNhomTTPT}.
 */
public interface TPhanNhomTTPTService {

    /**
     * Save a tPhanNhomTTPT.
     *
     * @param tPhanNhomTTPTDTO the entity to save.
     * @return the persisted entity.
     */
    TPhanNhomTTPTDTO save(TPhanNhomTTPTDTO tPhanNhomTTPTDTO);

    /**
     * Get all the tPhanNhomTTPTS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TPhanNhomTTPTDTO> findAll(Pageable pageable);

    /**
     * Get the "id" tPhanNhomTTPT.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TPhanNhomTTPTDTO> findOne(Long id);

    /**
     * Delete the "id" tPhanNhomTTPT.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
