package vn.vnpt.service;

import vn.vnpt.service.dto.LoaiChanDoanHinhAnhDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.LoaiChanDoanHinhAnh}.
 */
public interface LoaiChanDoanHinhAnhService {

    /**
     * Save a loaiChanDoanHinhAnh.
     *
     * @param loaiChanDoanHinhAnhDTO the entity to save.
     * @return the persisted entity.
     */
    LoaiChanDoanHinhAnhDTO save(LoaiChanDoanHinhAnhDTO loaiChanDoanHinhAnhDTO);

    /**
     * Get all the loaiChanDoanHinhAnhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LoaiChanDoanHinhAnhDTO> findAll(Pageable pageable);

    /**
     * Get the "id" loaiChanDoanHinhAnh.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LoaiChanDoanHinhAnhDTO> findOne(Long id);

    /**
     * Delete the "id" loaiChanDoanHinhAnh.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
