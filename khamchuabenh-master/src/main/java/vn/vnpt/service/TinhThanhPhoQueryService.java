package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.TinhThanhPho;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.TinhThanhPhoRepository;
import vn.vnpt.service.dto.TinhThanhPhoCriteria;
import vn.vnpt.service.dto.TinhThanhPhoDTO;
import vn.vnpt.service.mapper.TinhThanhPhoMapper;

/**
 * Service for executing complex queries for {@link TinhThanhPho} entities in the database.
 * The main input is a {@link TinhThanhPhoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TinhThanhPhoDTO} or a {@link Page} of {@link TinhThanhPhoDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TinhThanhPhoQueryService extends QueryService<TinhThanhPho> {

    private final Logger log = LoggerFactory.getLogger(TinhThanhPhoQueryService.class);

    private final TinhThanhPhoRepository tinhThanhPhoRepository;

    private final TinhThanhPhoMapper tinhThanhPhoMapper;

    public TinhThanhPhoQueryService(TinhThanhPhoRepository tinhThanhPhoRepository, TinhThanhPhoMapper tinhThanhPhoMapper) {
        this.tinhThanhPhoRepository = tinhThanhPhoRepository;
        this.tinhThanhPhoMapper = tinhThanhPhoMapper;
    }

    /**
     * Return a {@link List} of {@link TinhThanhPhoDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TinhThanhPhoDTO> findByCriteria(TinhThanhPhoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TinhThanhPho> specification = createSpecification(criteria);
        return tinhThanhPhoMapper.toDto(tinhThanhPhoRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TinhThanhPhoDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TinhThanhPhoDTO> findByCriteria(TinhThanhPhoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TinhThanhPho> specification = createSpecification(criteria);
        return tinhThanhPhoRepository.findAll(specification, page)
            .map(tinhThanhPhoMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TinhThanhPhoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TinhThanhPho> specification = createSpecification(criteria);
        return tinhThanhPhoRepository.count(specification);
    }

    /**
     * Function to convert {@link TinhThanhPhoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TinhThanhPho> createSpecification(TinhThanhPhoCriteria criteria) {
        Specification<TinhThanhPho> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TinhThanhPho_.id));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), TinhThanhPho_.cap));
            }
            if (criteria.getGuessPhrase() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGuessPhrase(), TinhThanhPho_.guessPhrase));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), TinhThanhPho_.ten));
            }
            if (criteria.getTenKhongDau() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenKhongDau(), TinhThanhPho_.tenKhongDau));
            }
        }
        return specification;
    }
}
