package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.TPhanNhomXetNghiemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TPhanNhomXetNghiem} and its DTO {@link TPhanNhomXetNghiemDTO}.
 */
@Mapper(componentModel = "spring", uses = {NhomXetNghiemMapper.class, XetNghiemMapper.class})
public interface TPhanNhomXetNghiemMapper extends EntityMapper<TPhanNhomXetNghiemDTO, TPhanNhomXetNghiem> {

    @Mapping(source = "nhomXN.id", target = "nhomXNId")
    @Mapping(source = "XN.id", target = "XNId")
    TPhanNhomXetNghiemDTO toDto(TPhanNhomXetNghiem tPhanNhomXetNghiem);

    @Mapping(source = "nhomXNId", target = "nhomXN")
    @Mapping(source = "XNId", target = "XN")
    TPhanNhomXetNghiem toEntity(TPhanNhomXetNghiemDTO tPhanNhomXetNghiemDTO);

    default TPhanNhomXetNghiem fromId(Long id) {
        if (id == null) {
            return null;
        }
        TPhanNhomXetNghiem tPhanNhomXetNghiem = new TPhanNhomXetNghiem();
        tPhanNhomXetNghiem.setId(id);
        return tPhanNhomXetNghiem;
    }
}
