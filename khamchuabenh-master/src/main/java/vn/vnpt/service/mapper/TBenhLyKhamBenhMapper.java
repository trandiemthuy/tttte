package vn.vnpt.service.mapper;


import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.*;
import vn.vnpt.service.dto.TBenhLyKhamBenhDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TBenhLyKhamBenh} and its DTO {@link TBenhLyKhamBenhDTO}.
 */
@Mapper(componentModel = "spring", uses = {ThongTinKhamBenhMapper.class, BenhLyMapper.class})
public abstract class TBenhLyKhamBenhMapper implements EntityMapper<TBenhLyKhamBenhDTO, TBenhLyKhamBenh> {

    @Autowired
    ThongTinKhamBenhMapper thongTinKhamBenhMapper;
    @Mapping(source = "thongTinKhamBenh.bakbId", target = "bakbId")
    @Mapping(source = "thongTinKhamBenh.donViId", target = "donViId")
    @Mapping(source = "thongTinKhamBenh.benhNhanId", target = "benhNhanId")
    @Mapping(source = "thongTinKhamBenh.dotDieuTriId", target = "dotDieuTriId")
    @Mapping(source = "thongTinKhamBenh.thongTinKhoaId", target = "thongTinKhoaId")
    @Mapping(source = "thongTinKhamBenh.id", target = "thongTinKhamBenhId")
    @Mapping(source = "benhLy.id", target = "benhLyId")
    public abstract TBenhLyKhamBenhDTO toDto(TBenhLyKhamBenh tBenhLyKhamBenh);

    @Mapping(expression = "java(" +
        "this.thongTinKhamBenhMapper.fromId(" +
        "new vn.vnpt.domain.ThongTinKhamBenhId(" +
        "tBenhLyKhamBenhDTO.getThongTinKhamBenhId(), " +
        "tBenhLyKhamBenhDTO.getThongTinKhoaId(), " +
        "tBenhLyKhamBenhDTO.getDotDieuTriId(), " +
        "tBenhLyKhamBenhDTO.getBakbId(), " +
        "tBenhLyKhamBenhDTO.getBenhNhanId(), " +
        "tBenhLyKhamBenhDTO.getDonViId()" +
        ")" +
        ")" +
        ")", target = "thongTinKhamBenh")
    @Mapping(source = "benhLyId", target = "benhLy.id")
    public abstract  TBenhLyKhamBenh toEntity(TBenhLyKhamBenhDTO tBenhLyKhamBenhDTO);

    public TBenhLyKhamBenh fromId(Long id) {
        if (id == null) {
            return null;
        }
        TBenhLyKhamBenh tBenhLyKhamBenh = new TBenhLyKhamBenh();
        tBenhLyKhamBenh.setId(id);
        return tBenhLyKhamBenh;
    }
}
