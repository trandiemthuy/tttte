package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.BenhNhanDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BenhNhan} and its DTO {@link BenhNhanDTO}.
 */
@Mapper(componentModel = "spring", uses = {DanTocMapper.class, NgheNghiepMapper.class, CountryMapper.class, PhuongXaMapper.class, QuanHuyenMapper.class, TinhThanhPhoMapper.class, UserExtraMapper.class})
public interface BenhNhanMapper extends EntityMapper<BenhNhanDTO, BenhNhan> {

    @Mapping(source = "danToc.id", target = "danTocId")
    @Mapping(source = "ngheNghiep.id", target = "ngheNghiepId")
    @Mapping(source = "quocTich.id", target = "quocTichId")
    @Mapping(source = "phuongXa.id", target = "phuongXaId")
    @Mapping(source = "quanHuyen.id", target = "quanHuyenId")
    @Mapping(source = "tinhThanhPho.id", target = "tinhThanhPhoId")
    @Mapping(source = "userExtra.id", target = "userExtraId")
    BenhNhanDTO toDto(BenhNhan benhNhan);

    @Mapping(source = "danTocId", target = "danToc")
    @Mapping(source = "ngheNghiepId", target = "ngheNghiep")
    @Mapping(source = "quocTichId", target = "quocTich")
    @Mapping(source = "phuongXaId", target = "phuongXa")
    @Mapping(source = "quanHuyenId", target = "quanHuyen")
    @Mapping(source = "tinhThanhPhoId", target = "tinhThanhPho")
    @Mapping(source = "userExtraId", target = "userExtra")
    BenhNhan toEntity(BenhNhanDTO benhNhanDTO);

    default BenhNhan fromId(Long id) {
        if (id == null) {
            return null;
        }
        BenhNhan benhNhan = new BenhNhan();
        benhNhan.setId(id);
        return benhNhan;
    }
}
