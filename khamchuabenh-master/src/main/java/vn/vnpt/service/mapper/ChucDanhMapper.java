package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.ChucDanhDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ChucDanh} and its DTO {@link ChucDanhDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ChucDanhMapper extends EntityMapper<ChucDanhDTO, ChucDanh> {



    default ChucDanh fromId(Long id) {
        if (id == null) {
            return null;
        }
        ChucDanh chucDanh = new ChucDanh();
        chucDanh.setId(id);
        return chucDanh;
    }
}
