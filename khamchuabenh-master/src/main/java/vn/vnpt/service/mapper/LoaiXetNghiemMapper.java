package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.LoaiXetNghiemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link LoaiXetNghiem} and its DTO {@link LoaiXetNghiemDTO}.
 */
@Mapper(componentModel = "spring", uses = {DonViMapper.class})
public interface LoaiXetNghiemMapper extends EntityMapper<LoaiXetNghiemDTO, LoaiXetNghiem> {

    @Mapping(source = "donVi.id", target = "donViId")
    LoaiXetNghiemDTO toDto(LoaiXetNghiem loaiXetNghiem);

    @Mapping(source = "donViId", target = "donVi")
    LoaiXetNghiem toEntity(LoaiXetNghiemDTO loaiXetNghiemDTO);

    default LoaiXetNghiem fromId(Long id) {
        if (id == null) {
            return null;
        }
        LoaiXetNghiem loaiXetNghiem = new LoaiXetNghiem();
        loaiXetNghiem.setId(id);
        return loaiXetNghiem;
    }
}
