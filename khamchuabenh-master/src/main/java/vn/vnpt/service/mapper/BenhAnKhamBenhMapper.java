package vn.vnpt.service.mapper;

import vn.vnpt.domain.*;
import vn.vnpt.service.dto.BenhAnKhamBenhDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BenhAnKhamBenh} and its DTO {@link BenhAnKhamBenhDTO}.
 */
@Mapper(componentModel = "spring", uses = {BenhNhanMapper.class, DonViMapper.class, NhanVienMapper.class, PhongMapper.class})
public interface BenhAnKhamBenhMapper extends EntityMapper<BenhAnKhamBenhDTO, BenhAnKhamBenh> {

//////    @Mapping(source = "id.id", target = "id")
////    @Mapping(source = "id.benhNhanId", target = "benhNhanId")
//    @Mapping(source = "id.donViId", target = "donViId")
//    @Mapping(source = "nhanVienTiepNhan.id", target = "nhanVienTiepNhanId")
//    @Mapping(source = "phongTiepNhan.id", target = "phongTiepNhanId")
    @Mapping(target = "benhNhanId", source = "benhNhanId")
    @Mapping(target = "donViId", source = "donViId")
    @Mapping(target = "phongTiepNhanId", source = "phongTiepNhan.id")
    @Mapping(target = "nhanVienTiepNhanId", source = "nhanVienTiepNhan.id")
    BenhAnKhamBenhDTO toDto(BenhAnKhamBenh benhAnKhamBenh);
//
//    @Mapping(source = "id", target = "id.id")
//    @Mapping(source = "benhNhanId", target = "id.benhNhanId")
//    @Mapping(source = "donViId", target = "id.donViId")
//    @Mapping(source = "nhanVienTiepNhanId", target = "nhanVienTiepNhan")
//    @Mapping(source = "phongTiepNhanId", target = "phongTiepNhan")
    @Mapping(target = "benhNhan.id", source = "benhNhanId")
    @Mapping(target = "donVi.id", source = "donViId")
    @Mapping(target = "benhNhanId", source = "benhNhanId")
    @Mapping(target = "donViId", source = "donViId")
    @Mapping(target = "phongTiepNhan.id", source = "phongTiepNhanId")
    @Mapping(target = "nhanVienTiepNhan.id", source = "nhanVienTiepNhanId")
    BenhAnKhamBenh toEntity(BenhAnKhamBenhDTO benhAnKhamBenhDTO);

    default BenhAnKhamBenh fromId(BenhAnKhamBenhId id) {
        if (id == null) {
            return null;
        }
        BenhAnKhamBenh benhAnKhamBenh = new BenhAnKhamBenh();
        benhAnKhamBenh.setId(id.getId());
        benhAnKhamBenh.setBenhNhanId(id.getBenhNhanId());
        benhAnKhamBenh.setDonViId(id.getDonViId());
        return benhAnKhamBenh;
    }
}
