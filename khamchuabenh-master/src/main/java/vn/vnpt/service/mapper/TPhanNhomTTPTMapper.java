package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.TPhanNhomTTPTDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TPhanNhomTTPT} and its DTO {@link TPhanNhomTTPTDTO}.
 */
@Mapper(componentModel = "spring", uses = {NhomThuThuatPhauThuatMapper.class, ThuThuatPhauThuatMapper.class})
public interface TPhanNhomTTPTMapper extends EntityMapper<TPhanNhomTTPTDTO, TPhanNhomTTPT> {

    @Mapping(source = "nhomttpt.id", target = "nhomttptId")
    @Mapping(source = "ttpt.id", target = "ttptId")
    TPhanNhomTTPTDTO toDto(TPhanNhomTTPT tPhanNhomTTPT);

    @Mapping(source = "nhomttptId", target = "nhomttpt")
    @Mapping(source = "ttptId", target = "ttpt")
    TPhanNhomTTPT toEntity(TPhanNhomTTPTDTO tPhanNhomTTPTDTO);

    default TPhanNhomTTPT fromId(Long id) {
        if (id == null) {
            return null;
        }
        TPhanNhomTTPT tPhanNhomTTPT = new TPhanNhomTTPT();
        tPhanNhomTTPT.setId(id);
        return tPhanNhomTTPT;
    }
}
