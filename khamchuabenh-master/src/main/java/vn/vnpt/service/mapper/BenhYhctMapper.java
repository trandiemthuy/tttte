package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.BenhYhctDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BenhYhct} and its DTO {@link BenhYhctDTO}.
 */
@Mapper(componentModel = "spring", uses = {BenhLyMapper.class})
public interface BenhYhctMapper extends EntityMapper<BenhYhctDTO, BenhYhct> {

    @Mapping(source = "benhLy.id", target = "benhLyId")
    BenhYhctDTO toDto(BenhYhct benhYhct);

    @Mapping(source = "benhLyId", target = "benhLy")
    BenhYhct toEntity(BenhYhctDTO benhYhctDTO);

    default BenhYhct fromId(Long id) {
        if (id == null) {
            return null;
        }
        BenhYhct benhYhct = new BenhYhct();
        benhYhct.setId(id);
        return benhYhct;
    }
}
