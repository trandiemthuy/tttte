package vn.vnpt.service.mapper;


import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.*;
import vn.vnpt.service.dto.ThongTinKhoaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ThongTinKhoa} and its DTO {@link ThongTinKhoaDTO}.
 */
@Mapper(componentModel = "spring", uses = {DotDieuTriMapper.class})
public abstract class ThongTinKhoaMapper implements EntityMapper<ThongTinKhoaDTO, ThongTinKhoa> {

    @Autowired
    DotDieuTriMapper dotDieuTriMapper;

    public abstract ThongTinKhoaDTO toDto(ThongTinKhoa thongTinKhoa);


    @Mapping(expression = "java(" +
        "this.dotDieuTriMapper.fromId(" +
        "new vn.vnpt.domain.DotDieuTriId(" +
        "thongTinKhoaDTO.getDotDieuTriId(), "+
        "thongTinKhoaDTO.getBakbId(), " +
        "thongTinKhoaDTO.getBenhNhanId(), " +
        "thongTinKhoaDTO.getDonViId()" +
        ")" +
        ")" +
        ")", target = "dotDieuTri")
    @Mapping(target = "dotDieuTriId", source = "dotDieuTriId")
    @Mapping(target = "bakbId", source = "bakbId")
    @Mapping(target = "benhNhanId", source = "benhNhanId")
    @Mapping(target = "donViId", source = "donViId")
    public abstract ThongTinKhoa toEntity(ThongTinKhoaDTO thongTinKhoaDTO);

    public ThongTinKhoa fromId(ThongTinKhoaId id) {
        if (id == null) {
            return null;
        }
        ThongTinKhoa thongTinKhoa = new ThongTinKhoa();
        thongTinKhoa.setId(id.getId());
        thongTinKhoa.setDotDieuTriId(id.getDotDieuTriId());
        thongTinKhoa.setBakbId(id.getBakbId());
        thongTinKhoa.setBenhNhanId(id.getBenhNhanId());
        thongTinKhoa.setDonViId(id.getDonViId());
        return thongTinKhoa;
    }
}
