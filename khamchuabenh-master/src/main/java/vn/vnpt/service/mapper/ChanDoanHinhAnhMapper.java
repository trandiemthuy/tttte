package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.ChanDoanHinhAnhDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ChanDoanHinhAnh} and its DTO {@link ChanDoanHinhAnhDTO}.
 */
@Mapper(componentModel = "spring", uses = {DonViMapper.class, DotThayDoiMaDichVuMapper.class, LoaiChanDoanHinhAnhMapper.class})
public interface ChanDoanHinhAnhMapper extends EntityMapper<ChanDoanHinhAnhDTO, ChanDoanHinhAnh> {

    @Mapping(source = "donVi.id", target = "donViId")
    @Mapping(source = "dotMa.id", target = "dotMaId")
    @Mapping(source = "loaicdha.id", target = "loaicdhaId")
    ChanDoanHinhAnhDTO toDto(ChanDoanHinhAnh chanDoanHinhAnh);

    @Mapping(source = "donViId", target = "donVi")
    @Mapping(source = "dotMaId", target = "dotMa")
    @Mapping(source = "loaicdhaId", target = "loaicdha")
    ChanDoanHinhAnh toEntity(ChanDoanHinhAnhDTO chanDoanHinhAnhDTO);

    default ChanDoanHinhAnh fromId(Long id) {
        if (id == null) {
            return null;
        }
        ChanDoanHinhAnh chanDoanHinhAnh = new ChanDoanHinhAnh();
        chanDoanHinhAnh.setId(id);
        return chanDoanHinhAnh;
    }
}
