package vn.vnpt.service.mapper;

import vn.vnpt.domain.*;
import vn.vnpt.service.dto.DichVuKhamDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DichVuKham} and its DTO {@link DichVuKhamDTO}.
 */
@Mapper(componentModel = "spring", uses = {DotThayDoiMaDichVuMapper.class})
public interface DichVuKhamMapper extends EntityMapper<DichVuKhamDTO, DichVuKham> {

    @Mapping(source = "dotThayDoiMaDichVu.id", target = "dotThayDoiMaDichVuId")
    DichVuKhamDTO toDto(DichVuKham dichVuKham);

    @Mapping(source = "dotThayDoiMaDichVuId", target = "dotThayDoiMaDichVu")
    DichVuKham toEntity(DichVuKhamDTO dichVuKhamDTO);

    default DichVuKham fromId(Long id) {
        if (id == null) {
            return null;
        }
        DichVuKham dichVuKham = new DichVuKham();
        dichVuKham.setId(id);
        return dichVuKham;
    }
}
