package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.LoaiThuThuatPhauThuatDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link LoaiThuThuatPhauThuat} and its DTO {@link LoaiThuThuatPhauThuatDTO}.
 */
@Mapper(componentModel = "spring", uses = {DonViMapper.class})
public interface LoaiThuThuatPhauThuatMapper extends EntityMapper<LoaiThuThuatPhauThuatDTO, LoaiThuThuatPhauThuat> {

    @Mapping(source = "donVi.id", target = "donViId")
    LoaiThuThuatPhauThuatDTO toDto(LoaiThuThuatPhauThuat loaiThuThuatPhauThuat);

    @Mapping(source = "donViId", target = "donVi")
    LoaiThuThuatPhauThuat toEntity(LoaiThuThuatPhauThuatDTO loaiThuThuatPhauThuatDTO);

    default LoaiThuThuatPhauThuat fromId(Long id) {
        if (id == null) {
            return null;
        }
        LoaiThuThuatPhauThuat loaiThuThuatPhauThuat = new LoaiThuThuatPhauThuat();
        loaiThuThuatPhauThuat.setId(id);
        return loaiThuThuatPhauThuat;
    }
}
