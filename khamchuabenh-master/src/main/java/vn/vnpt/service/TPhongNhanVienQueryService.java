package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.TPhongNhanVien;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.TPhongNhanVienRepository;
import vn.vnpt.service.dto.TPhongNhanVienCriteria;
import vn.vnpt.service.dto.TPhongNhanVienDTO;
import vn.vnpt.service.mapper.TPhongNhanVienMapper;

/**
 * Service for executing complex queries for {@link TPhongNhanVien} entities in the database.
 * The main input is a {@link TPhongNhanVienCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TPhongNhanVienDTO} or a {@link Page} of {@link TPhongNhanVienDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TPhongNhanVienQueryService extends QueryService<TPhongNhanVien> {

    private final Logger log = LoggerFactory.getLogger(TPhongNhanVienQueryService.class);

    private final TPhongNhanVienRepository tPhongNhanVienRepository;

    private final TPhongNhanVienMapper tPhongNhanVienMapper;

    public TPhongNhanVienQueryService(TPhongNhanVienRepository tPhongNhanVienRepository, TPhongNhanVienMapper tPhongNhanVienMapper) {
        this.tPhongNhanVienRepository = tPhongNhanVienRepository;
        this.tPhongNhanVienMapper = tPhongNhanVienMapper;
    }

    /**
     * Return a {@link List} of {@link TPhongNhanVienDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TPhongNhanVienDTO> findByCriteria(TPhongNhanVienCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TPhongNhanVien> specification = createSpecification(criteria);
        return tPhongNhanVienMapper.toDto(tPhongNhanVienRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TPhongNhanVienDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TPhongNhanVienDTO> findByCriteria(TPhongNhanVienCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TPhongNhanVien> specification = createSpecification(criteria);
        return tPhongNhanVienRepository.findAll(specification, page)
            .map(tPhongNhanVienMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TPhongNhanVienCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TPhongNhanVien> specification = createSpecification(criteria);
        return tPhongNhanVienRepository.count(specification);
    }

    /**
     * Function to convert {@link TPhongNhanVienCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TPhongNhanVien> createSpecification(TPhongNhanVienCriteria criteria) {
        Specification<TPhongNhanVien> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TPhongNhanVien_.id));
            }
            if (criteria.getEnabled() != null) {
                specification = specification.and(buildSpecification(criteria.getEnabled(), TPhongNhanVien_.enabled));
            }
            if (criteria.getPhongId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhongId(),
                    root -> root.join(TPhongNhanVien_.phong, JoinType.LEFT).get(Phong_.id)));
            }
            if (criteria.getNhanVienId() != null) {
                specification = specification.and(buildSpecification(criteria.getNhanVienId(),
                    root -> root.join(TPhongNhanVien_.nhanVien, JoinType.LEFT).get(NhanVien_.id)));
            }
            if (criteria.getTenNV() != null) {
                specification = specification.and(buildSpecification(criteria.getTenNV(),
                    root -> root.join(TPhongNhanVien_.nhanVien, JoinType.LEFT).get(NhanVien_.ten)));
            }
            if (criteria.getTenPhong() != null) {
                specification = specification.and(buildSpecification(criteria.getTenPhong(),
                    root -> root.join(TPhongNhanVien_.phong, JoinType.LEFT).get(Phong_.ten)));
            }
        }
        return specification;
    }
}
