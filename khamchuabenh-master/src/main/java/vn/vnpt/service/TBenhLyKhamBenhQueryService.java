package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.TBenhLyKhamBenh;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.TBenhLyKhamBenhRepository;
import vn.vnpt.service.dto.TBenhLyKhamBenhCriteria;
import vn.vnpt.service.dto.TBenhLyKhamBenhDTO;
import vn.vnpt.service.mapper.TBenhLyKhamBenhMapper;

/**
 * Service for executing complex queries for {@link TBenhLyKhamBenh} entities in the database.
 * The main input is a {@link TBenhLyKhamBenhCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TBenhLyKhamBenhDTO} or a {@link Page} of {@link TBenhLyKhamBenhDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TBenhLyKhamBenhQueryService extends QueryService<TBenhLyKhamBenh> {

    private final Logger log = LoggerFactory.getLogger(TBenhLyKhamBenhQueryService.class);

    private final TBenhLyKhamBenhRepository tBenhLyKhamBenhRepository;

    private final TBenhLyKhamBenhMapper tBenhLyKhamBenhMapper;

    public TBenhLyKhamBenhQueryService(TBenhLyKhamBenhRepository tBenhLyKhamBenhRepository, TBenhLyKhamBenhMapper tBenhLyKhamBenhMapper) {
        this.tBenhLyKhamBenhRepository = tBenhLyKhamBenhRepository;
        this.tBenhLyKhamBenhMapper = tBenhLyKhamBenhMapper;
    }

    /**
     * Return a {@link List} of {@link TBenhLyKhamBenhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TBenhLyKhamBenhDTO> findByCriteria(TBenhLyKhamBenhCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TBenhLyKhamBenh> specification = createSpecification(criteria);
        return tBenhLyKhamBenhMapper.toDto(tBenhLyKhamBenhRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TBenhLyKhamBenhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TBenhLyKhamBenhDTO> findByCriteria(TBenhLyKhamBenhCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TBenhLyKhamBenh> specification = createSpecification(criteria);
        return tBenhLyKhamBenhRepository.findAll(specification, page)
            .map(tBenhLyKhamBenhMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TBenhLyKhamBenhCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TBenhLyKhamBenh> specification = createSpecification(criteria);
        return tBenhLyKhamBenhRepository.count(specification);
    }

    /**
     * Function to convert {@link TBenhLyKhamBenhCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TBenhLyKhamBenh> createSpecification(TBenhLyKhamBenhCriteria criteria) {
        Specification<TBenhLyKhamBenh> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TBenhLyKhamBenh_.id));
            }
            if (criteria.getLoaiBenh() != null) {
                specification = specification.and(buildSpecification(criteria.getLoaiBenh(), TBenhLyKhamBenh_.loaiBenh));
            }
//            if (criteria.getBakbId() != null) {
//                specification = specification.and(buildSpecification(criteria.getBakbId(),
//                    root -> root.join(TBenhLyKhamBenh_.THONG_TIN_KHAM_BENH, JoinType.LEFT).join(ThongTinKhamBenh_.BAKB_ID,JoinType.LEFT).get(ThongTinKhamBenh_)));
//            }
//            if (criteria.getDonViId() != null) {
//                specification = specification.and(buildSpecification(criteria.getDonViId(),
//                    root -> root.join(TBenhLyKhamBenh_.donVi, JoinType.LEFT).get(ThongTinKhamBenh_.id)));
//            }
//            if (criteria.getBenhNhanId() != null) {
//                specification = specification.and(buildSpecification(criteria.getBenhNhanId(),
//                    root -> root.join(TBenhLyKhamBenh_.benhNhan, JoinType.LEFT).get(ThongTinKhamBenh_.id)));
//            }
//            if (criteria.getDotDieuTriId() != null) {
//                specification = specification.and(buildSpecification(criteria.getDotDieuTriId(),
//                    root -> root.join(TBenhLyKhamBenh_.dotDieuTri, JoinType.LEFT).get(ThongTinKhamBenh_.id)));
//            }
//            if (criteria.getThongTinKhoaId() != null) {
//                specification = specification.and(buildSpecification(criteria.getThongTinKhoaId(),
//                    root -> root.join(TBenhLyKhamBenh_.thongTinKhoa, JoinType.LEFT).get(ThongTinKhamBenh_.id)));
//            }
            if (criteria.getThongTinKhamBenhId() != null) {
                specification = specification.and(buildSpecification(criteria.getThongTinKhamBenhId(),
                    root -> root.join(TBenhLyKhamBenh_.thongTinKhamBenh, JoinType.LEFT).get(ThongTinKhamBenh_.id)));
            }
            if (criteria.getBenhLyId() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhLyId(),
                    root -> root.join(TBenhLyKhamBenh_.benhLy, JoinType.LEFT).get(BenhLy_.id)));
            }
        }
        return specification;
    }
}
