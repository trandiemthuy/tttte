package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.Khoa;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.KhoaRepository;
import vn.vnpt.service.dto.KhoaCriteria;
import vn.vnpt.service.dto.KhoaDTO;
import vn.vnpt.service.mapper.KhoaMapper;

/**
 * Service for executing complex queries for {@link Khoa} entities in the database.
 * The main input is a {@link KhoaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link KhoaDTO} or a {@link Page} of {@link KhoaDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class KhoaQueryService extends QueryService<Khoa> {

    private final Logger log = LoggerFactory.getLogger(KhoaQueryService.class);

    private final KhoaRepository khoaRepository;

    private final KhoaMapper khoaMapper;

    public KhoaQueryService(KhoaRepository khoaRepository, KhoaMapper khoaMapper) {
        this.khoaRepository = khoaRepository;
        this.khoaMapper = khoaMapper;
    }

    /**
     * Return a {@link List} of {@link KhoaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<KhoaDTO> findByCriteria(KhoaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Khoa> specification = createSpecification(criteria);
        return khoaMapper.toDto(khoaRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link KhoaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<KhoaDTO> findByCriteria(KhoaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Khoa> specification = createSpecification(criteria);
        return khoaRepository.findAll(specification, page)
            .map(khoaMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(KhoaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Khoa> specification = createSpecification(criteria);
        return khoaRepository.count(specification);
    }

    /**
     * Function to convert {@link KhoaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Khoa> createSpecification(KhoaCriteria criteria) {
        Specification<Khoa> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Khoa_.id));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCap(), Khoa_.cap));
            }
            if (criteria.getEnabled() != null) {
                specification = specification.and(buildSpecification(criteria.getEnabled(), Khoa_.enabled));
            }
            if (criteria.getKyHieu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKyHieu(), Khoa_.kyHieu));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), Khoa_.phone));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), Khoa_.ten));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(Khoa_.donVi, JoinType.LEFT).get(DonVi_.id)));
            }
        }
        return specification;
    }
}
