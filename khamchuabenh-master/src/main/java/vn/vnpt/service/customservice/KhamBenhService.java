package vn.vnpt.service.customservice;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.vnpt.domain.BenhAnKhamBenhId;
import vn.vnpt.domain.ThongTinKhamBenhId;
import vn.vnpt.service.dto.BenhAnKhamBenhCriteria;
import vn.vnpt.service.dto.ChiDinhDichVuKhamDTO;
import vn.vnpt.service.dto.customdto.KhamBenhDTO;
import vn.vnpt.service.dto.customdto.TiepNhanDTO;
import vn.vnpt.service.dto.customdto.TiepNhanResponseDTO;

import java.util.Optional;

public interface KhamBenhService {
    Optional<KhamBenhDTO> save(KhamBenhDTO khamBenhDTO);
    Optional<KhamBenhDTO> findOne(BenhAnKhamBenhId bakbCId);
    Page<KhamBenhDTO> findByCriteria(BenhAnKhamBenhCriteria criteria, Pageable pageable);
    void delete(BenhAnKhamBenhId id);
    void delete(ThongTinKhamBenhId id);
    Optional<KhamBenhDTO> initKhamBenh(TiepNhanDTO tiepNhanDTO);

    ThongTinKhamBenhId getThongTinKhamBenhIdByTiepNhanNgoaiTru(TiepNhanDTO tiepNhanDTO);
}
