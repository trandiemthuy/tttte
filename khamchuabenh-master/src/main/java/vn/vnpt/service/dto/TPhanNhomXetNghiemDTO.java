package vn.vnpt.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.TPhanNhomXetNghiem} entity.
 */
public class TPhanNhomXetNghiemDTO implements Serializable {
    
    private Long id;


    private Long nhomXNId;

    private Long xNId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNhomXNId() {
        return nhomXNId;
    }

    public void setNhomXNId(Long nhomXetNghiemId) {
        this.nhomXNId = nhomXetNghiemId;
    }

    public Long getXNId() {
        return xNId;
    }

    public void setXNId(Long xetNghiemId) {
        this.xNId = xetNghiemId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TPhanNhomXetNghiemDTO tPhanNhomXetNghiemDTO = (TPhanNhomXetNghiemDTO) o;
        if (tPhanNhomXetNghiemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tPhanNhomXetNghiemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TPhanNhomXetNghiemDTO{" +
            "id=" + getId() +
            ", nhomXNId=" + getNhomXNId() +
            ", xNId=" + getXNId() +
            "}";
    }
}
