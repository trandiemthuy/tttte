package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.TheBhyt} entity.
 */
public class TheBhytDTO implements Serializable {

    private Long id;

    /**
     * Trạng thái có hiệu lực của dịch vụ:\n1: Còn hiệu lực.\n0: Đã ẩn.
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái có hiệu lực của dịch vụ:\n1: Còn hiệu lực.\n0: Đã ẩn.", required = true)
    private Boolean enabled;

    /**
     * Mã khu vực
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Mã khu vực")
    private String maKhuVuc;

    /**
     * Mã số BHXH
     */
    @Size(max = 30)
    @ApiModelProperty(value = "Mã số BHXH")
    private String maSoBhxh;

    /**
     * Mã thẻ cũ
     */
    @Size(max = 30)
    @ApiModelProperty(value = "Mã thẻ cũ")
    private String maTheCu;

    /**
     * Ngày bắt đầu có hiệu lực của thẻ
     */
    @NotNull
    @ApiModelProperty(value = "Ngày bắt đầu có hiệu lực của thẻ", required = true)
    private LocalDate ngayBatDau;

    /**
     * Ngày hưởng quyền lợi đủ 5 năm
     */
    @NotNull
    @ApiModelProperty(value = "Ngày hưởng quyền lợi đủ 5 năm", required = true)
    private LocalDate ngayDuNamNam;

    /**
     * Ngày thẻ hết hạn
     */
    @NotNull
    @ApiModelProperty(value = "Ngày thẻ hết hạn", required = true)
    private LocalDate ngayHetHan;

    /**
     * Mã QR
     */
    @Size(max = 1000)
    @ApiModelProperty(value = "Mã QR")
    private String qr;

    /**
     * Mã số thẻ
     */
    @NotNull
    @Size(max = 30)
    @ApiModelProperty(value = "Mã số thẻ", required = true)
    private String soThe;

    /**
     * Ngày miễn cũng chi trả
     */
    @ApiModelProperty(value = "Ngày miễn cũng chi trả")
    private LocalDate ngayMienCungChiTra;

    /**
     * Mã bệnh nhân
     */
    @ApiModelProperty(value = "Mã bệnh nhân")

    private Long benhNhanId;
    /**
     * Mã đối tượng BHYT
     */
    @ApiModelProperty(value = "Mã đối tượng BHYT")

    private Long doiTuongBhytId;
    /**
     * Mã thông tin BHXH
     */
    @ApiModelProperty(value = "Mã thông tin BHXH")

    private Long thongTinBhxhId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getMaKhuVuc() {
        return maKhuVuc;
    }

    public void setMaKhuVuc(String maKhuVuc) {
        this.maKhuVuc = maKhuVuc;
    }

    public String getMaSoBhxh() {
        return maSoBhxh;
    }

    public void setMaSoBhxh(String maSoBhxh) {
        this.maSoBhxh = maSoBhxh;
    }

    public String getMaTheCu() {
        return maTheCu;
    }

    public void setMaTheCu(String maTheCu) {
        this.maTheCu = maTheCu;
    }

    public LocalDate getNgayBatDau() {
        return ngayBatDau;
    }

    public void setNgayBatDau(LocalDate ngayBatDau) {
        this.ngayBatDau = ngayBatDau;
    }

    public LocalDate getNgayDuNamNam() {
        return ngayDuNamNam;
    }

    public void setNgayDuNamNam(LocalDate ngayDuNamNam) {
        this.ngayDuNamNam = ngayDuNamNam;
    }

    public LocalDate getNgayHetHan() {
        return ngayHetHan;
    }

    public void setNgayHetHan(LocalDate ngayHetHan) {
        this.ngayHetHan = ngayHetHan;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public String getSoThe() {
        return soThe;
    }

    public void setSoThe(String soThe) {
        this.soThe = soThe;
    }

    public LocalDate getNgayMienCungChiTra() {
        return ngayMienCungChiTra;
    }

    public void setNgayMienCungChiTra(LocalDate ngayMienCungChiTra) {
        this.ngayMienCungChiTra = ngayMienCungChiTra;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDoiTuongBhytId() {
        return doiTuongBhytId;
    }

    public void setDoiTuongBhytId(Long doiTuongBhytId) {
        this.doiTuongBhytId = doiTuongBhytId;
    }

    public Long getThongTinBhxhId() {
        return thongTinBhxhId;
    }

    public void setThongTinBhxhId(Long thongTinBhxhId) {
        this.thongTinBhxhId = thongTinBhxhId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TheBhytDTO)) return false;
        TheBhytDTO that = (TheBhytDTO) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(getMaKhuVuc(), that.getMaKhuVuc()) &&
            Objects.equals(getMaSoBhxh(), that.getMaSoBhxh()) &&
            Objects.equals(getMaTheCu(), that.getMaTheCu()) &&
            Objects.equals(getNgayBatDau(), that.getNgayBatDau()) &&
            Objects.equals(getNgayDuNamNam(), that.getNgayDuNamNam()) &&
            Objects.equals(getNgayHetHan(), that.getNgayHetHan()) &&
            Objects.equals(getQr(), that.getQr()) &&
            Objects.equals(getSoThe(), that.getSoThe()) &&
            Objects.equals(getNgayMienCungChiTra(), that.getNgayMienCungChiTra()) &&
            Objects.equals(getBenhNhanId(), that.getBenhNhanId()) &&
            Objects.equals(getDoiTuongBhytId(), that.getDoiTuongBhytId()) &&
            Objects.equals(getThongTinBhxhId(), that.getThongTinBhxhId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), enabled, getMaKhuVuc(), getMaSoBhxh(), getMaTheCu(), getNgayBatDau(), getNgayDuNamNam(), getNgayHetHan(), getQr(), getSoThe(), getNgayMienCungChiTra(), getBenhNhanId(), getDoiTuongBhytId(), getThongTinBhxhId());
    }

    @Override
    public String toString() {
        return "TheBhytDTO{" +
            "id=" + getId() +
            ", enabled='" + isEnabled() + "'" +
            ", maKhuVuc='" + getMaKhuVuc() + "'" +
            ", maSoBhxh='" + getMaSoBhxh() + "'" +
            ", maTheCu='" + getMaTheCu() + "'" +
            ", ngayBatDau='" + getNgayBatDau() + "'" +
            ", ngayDuNamNam='" + getNgayDuNamNam() + "'" +
            ", ngayHetHan='" + getNgayHetHan() + "'" +
            ", qr='" + getQr() + "'" +
            ", soThe='" + getSoThe() + "'" +
            ", ngayMienCungChiTra='" + getNgayMienCungChiTra() + "'" +
            ", benhNhanId=" + getBenhNhanId() +
            ", doiTuongBhytId=" + getDoiTuongBhytId() +
            ", thongTinBhxhId=" + getThongTinBhxhId() +
            "}";
    }
}
