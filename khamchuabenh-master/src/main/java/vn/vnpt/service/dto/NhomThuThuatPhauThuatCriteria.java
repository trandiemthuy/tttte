package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.NhomThuThuatPhauThuat} entity. This class is used
 * in {@link vn.vnpt.web.rest.NhomThuThuatPhauThuatResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /nhom-thu-thuat-phau-thuats?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NhomThuThuatPhauThuatCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter enable;

    private IntegerFilter level;

    private BigDecimalFilter parentId;

    private StringFilter ten;

    private LongFilter donViId;

    public NhomThuThuatPhauThuatCriteria() {
    }

    public NhomThuThuatPhauThuatCriteria(NhomThuThuatPhauThuatCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.enable = other.enable == null ? null : other.enable.copy();
        this.level = other.level == null ? null : other.level.copy();
        this.parentId = other.parentId == null ? null : other.parentId.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
    }

    @Override
    public NhomThuThuatPhauThuatCriteria copy() {
        return new NhomThuThuatPhauThuatCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getEnable() {
        return enable;
    }

    public void setEnable(BooleanFilter enable) {
        this.enable = enable;
    }

    public IntegerFilter getLevel() {
        return level;
    }

    public void setLevel(IntegerFilter level) {
        this.level = level;
    }

    public BigDecimalFilter getParentId() {
        return parentId;
    }

    public void setParentId(BigDecimalFilter parentId) {
        this.parentId = parentId;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NhomThuThuatPhauThuatCriteria that = (NhomThuThuatPhauThuatCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(enable, that.enable) &&
            Objects.equals(level, that.level) &&
            Objects.equals(parentId, that.parentId) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(donViId, that.donViId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        enable,
        level,
        parentId,
        ten,
        donViId
        );
    }

    @Override
    public String toString() {
        return "NhomThuThuatPhauThuatCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (enable != null ? "enable=" + enable + ", " : "") +
                (level != null ? "level=" + level + ", " : "") +
                (parentId != null ? "parentId=" + parentId + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
            "}";
    }

}
