package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.ChucVu} entity. This class is used
 * in {@link vn.vnpt.web.rest.ChucVuResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /chuc-vus?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ChucVuCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter moTa;

    private StringFilter ten;

    private LongFilter nhanVienId;

    public ChucVuCriteria() {
    }

    public ChucVuCriteria(ChucVuCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.moTa = other.moTa == null ? null : other.moTa.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.nhanVienId = other.nhanVienId == null ? null : other.nhanVienId.copy();
    }

    @Override
    public ChucVuCriteria copy() {
        return new ChucVuCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getMoTa() {
        return moTa;
    }

    public void setMoTa(StringFilter moTa) {
        this.moTa = moTa;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public LongFilter getNhanVienId() {
        return nhanVienId;
    }

    public void setNhanVienId(LongFilter nhanVienId) {
        this.nhanVienId = nhanVienId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ChucVuCriteria that = (ChucVuCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(moTa, that.moTa) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(nhanVienId, that.nhanVienId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        moTa,
        ten,
        nhanVienId
        );
    }

    @Override
    public String toString() {
        return "ChucVuCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (moTa != null ? "moTa=" + moTa + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (nhanVienId != null ? "nhanVienId=" + nhanVienId + ", " : "") +
            "}";
    }

}
