package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link vn.vnpt.domain.TBenhLyKhamBenh} entity.
 */
public class TBenhLyKhamBenhDTO implements Serializable {
    
    private Long id;

    /**
     * Loại bệnh:\n1: Bệnh Chính.\n0: Bệnh phụ.
     */
    @NotNull
    @ApiModelProperty(value = "Loại bệnh:\n1: Bệnh Chính.\n0: Bệnh phụ.", required = true)
    private Boolean loaiBenh;


    private Long bakbId;

    private Long donViId;

    private Long benhNhanId;

    private Long dotDieuTriId;

    private Long thongTinKhoaId;

    private Long thongTinKhamBenhId;

    private Long benhLyId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isLoaiBenh() {
        return loaiBenh;
    }

    public void setLoaiBenh(Boolean loaiBenh) {
        this.loaiBenh = loaiBenh;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long thongTinKhamBenhId) {
        this.bakbId = thongTinKhamBenhId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long thongTinKhamBenhId) {
        this.donViId = thongTinKhamBenhId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long thongTinKhamBenhId) {
        this.benhNhanId = thongTinKhamBenhId;
    }

    public Long getDotDieuTriId() {
        return dotDieuTriId;
    }

    public void setDotDieuTriId(Long thongTinKhamBenhId) {
        this.dotDieuTriId = thongTinKhamBenhId;
    }

    public Long getThongTinKhoaId() {
        return thongTinKhoaId;
    }

    public void setThongTinKhoaId(Long thongTinKhamBenhId) {
        this.thongTinKhoaId = thongTinKhamBenhId;
    }

    public Long getThongTinKhamBenhId() {
        return thongTinKhamBenhId;
    }

    public void setThongTinKhamBenhId(Long thongTinKhamBenhId) {
        this.thongTinKhamBenhId = thongTinKhamBenhId;
    }

    public Long getBenhLyId() {
        return benhLyId;
    }

    public void setBenhLyId(Long benhLyId) {
        this.benhLyId = benhLyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TBenhLyKhamBenhDTO)) {
            return false;
        }

        return id != null && id.equals(((TBenhLyKhamBenhDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TBenhLyKhamBenhDTO{" +
            "id=" + getId() +
            ", loaiBenh='" + isLoaiBenh() + "'" +
            ", bakbId=" + getBakbId() +
            ", donViId=" + getDonViId() +
            ", benhNhanId=" + getBenhNhanId() +
            ", dotDieuTriId=" + getDotDieuTriId() +
            ", thongTinKhoaId=" + getThongTinKhoaId() +
            ", thongTinKhamBenhId=" + getThongTinKhamBenhId() +
            ", benhLyId=" + getBenhLyId() +
            "}";
    }
}
