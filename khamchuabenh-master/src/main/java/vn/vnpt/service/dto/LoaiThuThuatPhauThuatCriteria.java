package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.LoaiThuThuatPhauThuat} entity. This class is used
 * in {@link vn.vnpt.web.rest.LoaiThuThuatPhauThuatResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /loai-thu-thuat-phau-thuats?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LoaiThuThuatPhauThuatCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter ten;

    private StringFilter moTa;

    private BooleanFilter enable;

    private IntegerFilter uuTien;

    private StringFilter maPhanLoai;

    private LongFilter donViId;

    public LoaiThuThuatPhauThuatCriteria() {
    }

    public LoaiThuThuatPhauThuatCriteria(LoaiThuThuatPhauThuatCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.moTa = other.moTa == null ? null : other.moTa.copy();
        this.enable = other.enable == null ? null : other.enable.copy();
        this.uuTien = other.uuTien == null ? null : other.uuTien.copy();
        this.maPhanLoai = other.maPhanLoai == null ? null : other.maPhanLoai.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
    }

    @Override
    public LoaiThuThuatPhauThuatCriteria copy() {
        return new LoaiThuThuatPhauThuatCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getMoTa() {
        return moTa;
    }

    public void setMoTa(StringFilter moTa) {
        this.moTa = moTa;
    }

    public BooleanFilter getEnable() {
        return enable;
    }

    public void setEnable(BooleanFilter enable) {
        this.enable = enable;
    }

    public IntegerFilter getUuTien() {
        return uuTien;
    }

    public void setUuTien(IntegerFilter uuTien) {
        this.uuTien = uuTien;
    }

    public StringFilter getMaPhanLoai() {
        return maPhanLoai;
    }

    public void setMaPhanLoai(StringFilter maPhanLoai) {
        this.maPhanLoai = maPhanLoai;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LoaiThuThuatPhauThuatCriteria that = (LoaiThuThuatPhauThuatCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(moTa, that.moTa) &&
            Objects.equals(enable, that.enable) &&
            Objects.equals(uuTien, that.uuTien) &&
            Objects.equals(maPhanLoai, that.maPhanLoai) &&
            Objects.equals(donViId, that.donViId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        ten,
        moTa,
        enable,
        uuTien,
        maPhanLoai,
        donViId
        );
    }

    @Override
    public String toString() {
        return "LoaiThuThuatPhauThuatCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (moTa != null ? "moTa=" + moTa + ", " : "") +
                (enable != null ? "enable=" + enable + ", " : "") +
                (uuTien != null ? "uuTien=" + uuTien + ", " : "") +
                (maPhanLoai != null ? "maPhanLoai=" + maPhanLoai + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
            "}";
    }

}
