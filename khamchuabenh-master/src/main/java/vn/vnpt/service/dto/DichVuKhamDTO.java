package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.DichVuKham} entity.
 */
@ApiModel(description = "Thông tinc ông văn quy định mã của các loại dịch vụ mã bệnh viện sử dụng")
public class DichVuKhamDTO implements Serializable {

    private Long id;

    /**
     * Trạng thái xóa: 1: đã xóa. 0 không xóa
     */
    @ApiModelProperty(value = "Trạng thái xóa: 1: đã xóa. 0 không xóa")
    private Integer deleted;

    /**
     * Trạng thái có hiệu lực: 1: Có. 0: Hết hiệu lực
     */
    @ApiModelProperty(value = "Trạng thái có hiệu lực: 1: Có. 0: Hết hiệu lực")
    private Integer enabled;

    /**
     * Giới hạn chỉ định
     */
    @ApiModelProperty(value = "Giới hạn chỉ định")
    private Integer gioiHanChiDinh;

    private Integer phanTheoGioTinh;

    @Size(max = 1000)
    private String tenHienThi;

    @Size(max = 255)
    private String maDungChung;

    @Size(max = 255)
    private String maNoiBo;

    /**
     * 0: Không bảo hiểm. 1: Có bảo hiểm. 2: Cả 2
     */
    @ApiModelProperty(value = "0: Không bảo hiểm. 1: Có bảo hiểm. 2: Cả 2")
    private Integer phamViChiDinh;

    @Size(max = 255)
    private String donViTinh;

    @NotNull
    private BigDecimal donGiaBenhVien;

    private Long dotThayDoiMaDichVuId;

    public Long getId(){
        return this.id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Integer getGioiHanChiDinh() {
        return gioiHanChiDinh;
    }

    public void setGioiHanChiDinh(Integer gioiHanChiDinh) {
        this.gioiHanChiDinh = gioiHanChiDinh;
    }

    public Integer getPhanTheoGioTinh() {
        return phanTheoGioTinh;
    }

    public void setPhanTheoGioTinh(Integer phanTheoGioTinh) {
        this.phanTheoGioTinh = phanTheoGioTinh;
    }

    public String getTenHienThi() {
        return tenHienThi;
    }

    public void setTenHienThi(String tenHienThi) {
        this.tenHienThi = tenHienThi;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public String getMaNoiBo() {
        return maNoiBo;
    }

    public void setMaNoiBo(String maNoiBo) {
        this.maNoiBo = maNoiBo;
    }

    public Integer getPhamViChiDinh() {
        return phamViChiDinh;
    }

    public void setPhamViChiDinh(Integer phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
    }

    public String getDonViTinh() {
        return donViTinh;
    }

    public void setDonViTinh(String donViTinh) {
        this.donViTinh = donViTinh;
    }

    public BigDecimal getDonGiaBenhVien() {
        return donGiaBenhVien;
    }

    public void setDonGiaBenhVien(BigDecimal donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
    }

    public Long getDotThayDoiMaDichVuId() {
        return dotThayDoiMaDichVuId;
    }

    public void setDotThayDoiMaDichVuId(Long dotThayDoiMaDichVuId) {
        this.dotThayDoiMaDichVuId = dotThayDoiMaDichVuId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DichVuKhamDTO dichVuKhamDTO = (DichVuKhamDTO) o;
        if (dichVuKhamDTO.getId() == null && getId() == null){
            return false;
        }
        return Objects.equals(getId(), dichVuKhamDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "DichVuKhamDTO{" +
            ", id=" + getId() +
            ", deleted=" + getDeleted() +
            ", enabled=" + getEnabled() +
            ", gioiHanChiDinh=" + getGioiHanChiDinh() +
            ", phanTheoGioTinh=" + getPhanTheoGioTinh() +
            ", tenHienThi='" + getTenHienThi() + "'" +
            ", maDungChung='" + getMaDungChung() + "'" +
            ", maNoiBo='" + getMaNoiBo() + "'" +
            ", phamViChiDinh=" + getPhamViChiDinh() +
            ", donViTinh='" + getDonViTinh() + "'" +
            ", donGiaBenhVien=" + getDonGiaBenhVien() +
            ", dotThayDoiMaDichVuId='" + getDotThayDoiMaDichVuId() + "'" +
            "}";
    }
}
