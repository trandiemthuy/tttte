package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.ChucDanh} entity.
 */
@ApiModel(description = "NDB_TABLE=READ_BACKUP=1 Thông tin chức danh của user")
public class ChucDanhDTO implements Serializable {
    
    private Long id;

    /**
     * Mô tả chức danh
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Mô tả chức danh")
    private String moTa;

    /**
     * Tên chức danh
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên chức danh", required = true)
    private String ten;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChucDanhDTO chucDanhDTO = (ChucDanhDTO) o;
        if (chucDanhDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chucDanhDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChucDanhDTO{" +
            "id=" + getId() +
            ", moTa='" + getMoTa() + "'" +
            ", ten='" + getTen() + "'" +
            "}";
    }
}
