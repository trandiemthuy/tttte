package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.DotThayDoiMaDichVu} entity.
 */
@ApiModel(description = "Thông tinc ông văn quy định mã của các loại dịch vụ mã bệnh viện sử dụng")
public class DotThayDoiMaDichVuDTO implements Serializable {

    private Long id;

    /**
     * 0: áp dụng ngay lập tức, 1: Áp dụng cho bệnh nhân mới vào viện
     */
    @ApiModelProperty(value = "0: áp dụng ngay lập tức, 1: Áp dụng cho bệnh nhân mới vào viện")
    private Integer dotTuongApDung;

    /**
     * Ghi chú
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Ghi chú")
    private String ghiChu;

    /**
     * Ngày áp dụng của công văn danh mục
     */
    @NotNull
    @ApiModelProperty(value = "Ngày áp dụng của công văn danh mục", required = true)
    private LocalDate ngayApDung;

    /**
     * Tên công văn
     */
    @NotNull
    @ApiModelProperty(value = "Tên công văn", required = true)
    private String ten;


    private Long donViId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDotTuongApDung() {
        return dotTuongApDung;
    }

    public void setDotTuongApDung(Integer dotTuongApDung) {
        this.dotTuongApDung = dotTuongApDung;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public LocalDate getNgayApDung() {
        return ngayApDung;
    }

    public void setNgayApDung(LocalDate ngayApDung) {
        this.ngayApDung = ngayApDung;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO = (DotThayDoiMaDichVuDTO) o;
        if (dotThayDoiMaDichVuDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dotThayDoiMaDichVuDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DotThayDoiMaDichVuDTO{" +
            "id=" + getId() +
            ", dotTuongApDung=" + getDotTuongApDung() +
            ", ghiChu='" + getGhiChu() + "'" +
            ", ngayApDung='" + getNgayApDung() + "'" +
            ", ten='" + getTen() + "'" +
            ", donViId=" + getDonViId() +
            "}";
    }
}
