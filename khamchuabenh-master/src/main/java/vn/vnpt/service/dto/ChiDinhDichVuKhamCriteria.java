package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.ChiDinhDichVuKham} entity. This class is used
 * in {@link vn.vnpt.web.rest.ChiDinhDichVuKhamResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /chi-dinh-dich-vu-khams?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ChiDinhDichVuKhamCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter congKhamBanDau;

    private BooleanFilter daThanhToan;

    private BigDecimalFilter donGia;

    private StringFilter ten;

    private LocalDateFilter thoiGianChiDinh;

    private BigDecimalFilter tyLeThanhToan;

    private BooleanFilter coBaoHiem;

    private BigDecimalFilter giaBhyt;

    private BigDecimalFilter giaKhongBhyt;

    private StringFilter maDungChung;

    private BooleanFilter dichVuYeuCau;

    private IntegerFilter nam;

    private LongFilter ttkbId;

    private LongFilter dichVuKhamId;

    public ChiDinhDichVuKhamCriteria() {
    }

    public ChiDinhDichVuKhamCriteria(ChiDinhDichVuKhamCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.congKhamBanDau = other.congKhamBanDau == null ? null : other.congKhamBanDau.copy();
        this.daThanhToan = other.daThanhToan == null ? null : other.daThanhToan.copy();
        this.donGia = other.donGia == null ? null : other.donGia.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.thoiGianChiDinh = other.thoiGianChiDinh == null ? null : other.thoiGianChiDinh.copy();
        this.tyLeThanhToan = other.tyLeThanhToan == null ? null : other.tyLeThanhToan.copy();
        this.coBaoHiem = other.coBaoHiem == null ? null : other.coBaoHiem.copy();
        this.giaBhyt = other.giaBhyt == null ? null : other.giaBhyt.copy();
        this.giaKhongBhyt = other.giaKhongBhyt == null ? null : other.giaKhongBhyt.copy();
        this.maDungChung = other.maDungChung == null ? null : other.maDungChung.copy();
        this.dichVuYeuCau = other.dichVuYeuCau == null ? null : other.dichVuYeuCau.copy();
        this.nam = other.nam == null ? null : other.nam.copy();
        this.ttkbId = other.ttkbId == null ? null : other.ttkbId.copy();
        this.dichVuKhamId = other.dichVuKhamId == null ? null : other.dichVuKhamId.copy();
    }

    @Override
    public ChiDinhDichVuKhamCriteria copy() {
        return new ChiDinhDichVuKhamCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getCongKhamBanDau() {
        return congKhamBanDau;
    }

    public void setCongKhamBanDau(BooleanFilter congKhamBanDau) {
        this.congKhamBanDau = congKhamBanDau;
    }

    public BooleanFilter getDaThanhToan() {
        return daThanhToan;
    }

    public void setDaThanhToan(BooleanFilter daThanhToan) {
        this.daThanhToan = daThanhToan;
    }

    public BigDecimalFilter getDonGia() {
        return donGia;
    }

    public void setDonGia(BigDecimalFilter donGia) {
        this.donGia = donGia;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public LocalDateFilter getThoiGianChiDinh() {
        return thoiGianChiDinh;
    }

    public void setThoiGianChiDinh(LocalDateFilter thoiGianChiDinh) {
        this.thoiGianChiDinh = thoiGianChiDinh;
    }

    public BigDecimalFilter getTyLeThanhToan() {
        return tyLeThanhToan;
    }

    public void setTyLeThanhToan(BigDecimalFilter tyLeThanhToan) {
        this.tyLeThanhToan = tyLeThanhToan;
    }

    public BooleanFilter getCoBaoHiem() {
        return coBaoHiem;
    }

    public void setCoBaoHiem(BooleanFilter coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public BigDecimalFilter getGiaBhyt() {
        return giaBhyt;
    }

    public void setGiaBhyt(BigDecimalFilter giaBhyt) {
        this.giaBhyt = giaBhyt;
    }

    public BigDecimalFilter getGiaKhongBhyt() {
        return giaKhongBhyt;
    }

    public void setGiaKhongBhyt(BigDecimalFilter giaKhongBhyt) {
        this.giaKhongBhyt = giaKhongBhyt;
    }

    public StringFilter getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(StringFilter maDungChung) {
        this.maDungChung = maDungChung;
    }

    public BooleanFilter getDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public void setDichVuYeuCau(BooleanFilter dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public IntegerFilter getNam() {
        return nam;
    }

    public void setNam(IntegerFilter nam) {
        this.nam = nam;
    }

    public LongFilter getTtkbId() {
        return ttkbId;
    }

    public void setTtkbId(LongFilter ttkbId) {
        this.ttkbId = ttkbId;
    }

    public LongFilter getDichVuKhamId() {
        return dichVuKhamId;
    }

    public void setDichVuKhamId(LongFilter dichVuKhamId) {
        this.dichVuKhamId = dichVuKhamId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ChiDinhDichVuKhamCriteria that = (ChiDinhDichVuKhamCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(congKhamBanDau, that.congKhamBanDau) &&
            Objects.equals(daThanhToan, that.daThanhToan) &&
            Objects.equals(donGia, that.donGia) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(thoiGianChiDinh, that.thoiGianChiDinh) &&
            Objects.equals(tyLeThanhToan, that.tyLeThanhToan) &&
            Objects.equals(coBaoHiem, that.coBaoHiem) &&
            Objects.equals(giaBhyt, that.giaBhyt) &&
            Objects.equals(giaKhongBhyt, that.giaKhongBhyt) &&
            Objects.equals(maDungChung, that.maDungChung) &&
            Objects.equals(dichVuYeuCau, that.dichVuYeuCau) &&
            Objects.equals(nam, that.nam) &&
            Objects.equals(ttkbId, that.ttkbId) &&
            Objects.equals(dichVuKhamId, that.dichVuKhamId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        congKhamBanDau,
        daThanhToan,
        donGia,
        ten,
        thoiGianChiDinh,
        tyLeThanhToan,
        coBaoHiem,
        giaBhyt,
        giaKhongBhyt,
        maDungChung,
        dichVuYeuCau,
        nam,
        ttkbId,
        dichVuKhamId
        );
    }

    @Override
    public String toString() {
        return "ChiDinhDichVuKhamCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (congKhamBanDau != null ? "congKhamBanDau=" + congKhamBanDau + ", " : "") +
                (daThanhToan != null ? "daThanhToan=" + daThanhToan + ", " : "") +
                (donGia != null ? "donGia=" + donGia + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (thoiGianChiDinh != null ? "thoiGianChiDinh=" + thoiGianChiDinh + ", " : "") +
                (tyLeThanhToan != null ? "tyLeThanhToan=" + tyLeThanhToan + ", " : "") +
                (coBaoHiem != null ? "coBaoHiem=" + coBaoHiem + ", " : "") +
                (giaBhyt != null ? "giaBhyt=" + giaBhyt + ", " : "") +
                (giaKhongBhyt != null ? "giaKhongBhyt=" + giaKhongBhyt + ", " : "") +
                (maDungChung != null ? "maDungChung=" + maDungChung + ", " : "") +
                (dichVuYeuCau != null ? "dichVuYeuCau=" + dichVuYeuCau + ", " : "") +
                (nam != null ? "nam=" + nam + ", " : "") +
                (ttkbId != null ? "ttkbId=" + ttkbId + ", " : "") +
                (dichVuKhamId != null ? "dichVuKhamId=" + dichVuKhamId + ", " : "") +
            "}";
    }

}
