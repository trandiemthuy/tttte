package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.ThongTinBhxh} entity.
 */
public class ThongTinBhxhDTO implements Serializable {
    
    private Long id;

    /**
     * Mã đơn vị trực thuộc
     */
    @NotNull
    @Size(max = 10)
    @ApiModelProperty(value = "Mã đơn vị trực thuộc", required = true)
    private String dvtt;

    /**
     * Trạng thái có hiệu lực: 1: Có hiệu lực: 0: Không có hiệu lực
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái có hiệu lực: 1: Có hiệu lực: 0: Không có hiệu lực", required = true)
    private Boolean enabled;

    /**
     * Ngày áp dụng BHXH
     */
    @NotNull
    @ApiModelProperty(value = "Ngày áp dụng BHXH", required = true)
    private LocalDate ngayApDungBhyt;

    /**
     * Tên đơn vị theo BHXH
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên đơn vị theo BHXH", required = true)
    private String ten;

    /**
     * Mã đơn vị
     */
    @ApiModelProperty(value = "Mã đơn vị")

    private Long donViId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDvtt() {
        return dvtt;
    }

    public void setDvtt(String dvtt) {
        this.dvtt = dvtt;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public LocalDate getNgayApDungBhyt() {
        return ngayApDungBhyt;
    }

    public void setNgayApDungBhyt(LocalDate ngayApDungBhyt) {
        this.ngayApDungBhyt = ngayApDungBhyt;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ThongTinBhxhDTO thongTinBhxhDTO = (ThongTinBhxhDTO) o;
        if (thongTinBhxhDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), thongTinBhxhDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ThongTinBhxhDTO{" +
            "id=" + getId() +
            ", dvtt='" + getDvtt() + "'" +
            ", enabled='" + isEnabled() + "'" +
            ", ngayApDungBhyt='" + getNgayApDungBhyt() + "'" +
            ", ten='" + getTen() + "'" +
            ", donViId=" + getDonViId() +
            "}";
    }
}
