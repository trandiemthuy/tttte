package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link vn.vnpt.domain.Menu} entity.
 */
public class MenuDTO implements Serializable {
    
    private Long id;

    /**
     * Trạng thái có hiệu lực của dịch vụ:\n1: Còn hiệu lực.\n0: Đã ẩn.
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái có hiệu lực của dịch vụ:\n1: Còn hiệu lực.\n0: Đã ẩn.", required = true)
    private Boolean enabled;

    /**
     * Cấp của menu
     */
    @NotNull
    @ApiModelProperty(value = "Cấp của menu", required = true)
    private Integer level;

    /**
     * Tên hiển thị của menu
     */
    @NotNull
    @Size(max = 255)
    @ApiModelProperty(value = "Tên hiển thị của menu", required = true)
    private String name;

    /**
     * Đường dẫn của menu
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Đường dẫn của menu")
    private String uri;

    /**
     * Mã menu cha
     */
    @ApiModelProperty(value = "Mã menu cha")
    private Integer parent;

    /**
     * Mã đơn vị
     */
    @NotNull
    @ApiModelProperty(value = "Mã đơn vị", required = true)
    private Long donViId;

    private Set<UserExtraDTO> users = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public Set<UserExtraDTO> getUsers() {
        return users;
    }

    public void setUsers(Set<UserExtraDTO> userExtras) {
        this.users = userExtras;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MenuDTO)) {
            return false;
        }

        return id != null && id.equals(((MenuDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MenuDTO{" +
            "id=" + getId() +
            ", enabled='" + isEnabled() + "'" +
            ", level=" + getLevel() +
            ", name='" + getName() + "'" +
            ", uri='" + getUri() + "'" +
            ", parent=" + getParent() +
            ", donViId=" + getDonViId() +
            ", users='" + getUsers() + "'" +
            "}";
    }
}
