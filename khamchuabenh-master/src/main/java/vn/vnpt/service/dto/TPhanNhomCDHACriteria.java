package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.TPhanNhomCDHA} entity. This class is used
 * in {@link vn.vnpt.web.rest.TPhanNhomCDHAResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /t-phan-nhom-cdhas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TPhanNhomCDHACriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter nhom_cdhaId;

    private LongFilter cdhaId;

    public TPhanNhomCDHACriteria() {
    }

    public TPhanNhomCDHACriteria(TPhanNhomCDHACriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nhom_cdhaId = other.nhom_cdhaId == null ? null : other.nhom_cdhaId.copy();
        this.cdhaId = other.cdhaId == null ? null : other.cdhaId.copy();
    }

    @Override
    public TPhanNhomCDHACriteria copy() {
        return new TPhanNhomCDHACriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getNhom_cdhaId() {
        return nhom_cdhaId;
    }

    public void setNhom_cdhaId(LongFilter nhom_cdhaId) {
        this.nhom_cdhaId = nhom_cdhaId;
    }

    public LongFilter getCdhaId() {
        return cdhaId;
    }

    public void setCdhaId(LongFilter cdhaId) {
        this.cdhaId = cdhaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TPhanNhomCDHACriteria that = (TPhanNhomCDHACriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nhom_cdhaId, that.nhom_cdhaId) &&
            Objects.equals(cdhaId, that.cdhaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nhom_cdhaId,
        cdhaId
        );
    }

    @Override
    public String toString() {
        return "TPhanNhomCDHACriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nhom_cdhaId != null ? "nhom_cdhaId=" + nhom_cdhaId + ", " : "") +
                (cdhaId != null ? "cdhaId=" + cdhaId + ", " : "") +
            "}";
    }

}
