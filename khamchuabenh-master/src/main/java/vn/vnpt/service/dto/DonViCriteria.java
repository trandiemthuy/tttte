package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.DonVi} entity. This class is used
 * in {@link vn.vnpt.web.rest.DonViResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /don-vis?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DonViCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter cap;

    private StringFilter chiNhanhNganHang;

    private StringFilter diaChi;

    private BigDecimalFilter donViQuanLyId;

    private StringFilter dvtt;

    private StringFilter email;

    private BooleanFilter enabled;

    private StringFilter kyHieu;

    private IntegerFilter loai;

    private StringFilter maTinh;

    private StringFilter phone;

    private StringFilter soTaiKhoan;

    private StringFilter ten;

    private StringFilter tenNganHang;

    public DonViCriteria() {
    }

    public DonViCriteria(DonViCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.cap = other.cap == null ? null : other.cap.copy();
        this.chiNhanhNganHang = other.chiNhanhNganHang == null ? null : other.chiNhanhNganHang.copy();
        this.diaChi = other.diaChi == null ? null : other.diaChi.copy();
        this.donViQuanLyId = other.donViQuanLyId == null ? null : other.donViQuanLyId.copy();
        this.dvtt = other.dvtt == null ? null : other.dvtt.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.kyHieu = other.kyHieu == null ? null : other.kyHieu.copy();
        this.loai = other.loai == null ? null : other.loai.copy();
        this.maTinh = other.maTinh == null ? null : other.maTinh.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.soTaiKhoan = other.soTaiKhoan == null ? null : other.soTaiKhoan.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.tenNganHang = other.tenNganHang == null ? null : other.tenNganHang.copy();
    }

    @Override
    public DonViCriteria copy() {
        return new DonViCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getCap() {
        return cap;
    }

    public void setCap(IntegerFilter cap) {
        this.cap = cap;
    }

    public StringFilter getChiNhanhNganHang() {
        return chiNhanhNganHang;
    }

    public void setChiNhanhNganHang(StringFilter chiNhanhNganHang) {
        this.chiNhanhNganHang = chiNhanhNganHang;
    }

    public StringFilter getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(StringFilter diaChi) {
        this.diaChi = diaChi;
    }

    public BigDecimalFilter getDonViQuanLyId() {
        return donViQuanLyId;
    }

    public void setDonViQuanLyId(BigDecimalFilter donViQuanLyId) {
        this.donViQuanLyId = donViQuanLyId;
    }

    public StringFilter getDvtt() {
        return dvtt;
    }

    public void setDvtt(StringFilter dvtt) {
        this.dvtt = dvtt;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public BooleanFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(BooleanFilter enabled) {
        this.enabled = enabled;
    }

    public StringFilter getKyHieu() {
        return kyHieu;
    }

    public void setKyHieu(StringFilter kyHieu) {
        this.kyHieu = kyHieu;
    }

    public IntegerFilter getLoai() {
        return loai;
    }

    public void setLoai(IntegerFilter loai) {
        this.loai = loai;
    }

    public StringFilter getMaTinh() {
        return maTinh;
    }

    public void setMaTinh(StringFilter maTinh) {
        this.maTinh = maTinh;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public StringFilter getSoTaiKhoan() {
        return soTaiKhoan;
    }

    public void setSoTaiKhoan(StringFilter soTaiKhoan) {
        this.soTaiKhoan = soTaiKhoan;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getTenNganHang() {
        return tenNganHang;
    }

    public void setTenNganHang(StringFilter tenNganHang) {
        this.tenNganHang = tenNganHang;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DonViCriteria that = (DonViCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(cap, that.cap) &&
            Objects.equals(chiNhanhNganHang, that.chiNhanhNganHang) &&
            Objects.equals(diaChi, that.diaChi) &&
                Objects.equals(donViQuanLyId, that.donViQuanLyId) &&
                Objects.equals(dvtt, that.dvtt) &&
                Objects.equals(email, that.email) &&
                Objects.equals(enabled, that.enabled) &&
                Objects.equals(kyHieu, that.kyHieu) &&
                Objects.equals(loai, that.loai) &&
                Objects.equals(maTinh, that.maTinh) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(soTaiKhoan, that.soTaiKhoan) &&
                Objects.equals(ten, that.ten) &&
                Objects.equals(tenNganHang, that.tenNganHang);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        cap,
        chiNhanhNganHang,
        diaChi,
        donViQuanLyId,
        dvtt,
        email,
        enabled,
        kyHieu,
        loai,
        maTinh,
        phone,
        soTaiKhoan,
        ten,
            tenNganHang
        );
    }

    @Override
    public String toString() {
        return "DonViCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (cap != null ? "cap=" + cap + ", " : "") +
                (chiNhanhNganHang != null ? "chiNhanhNganHang=" + chiNhanhNganHang + ", " : "") +
                (diaChi != null ? "diaChi=" + diaChi + ", " : "") +
                (donViQuanLyId != null ? "donViQuanLyId=" + donViQuanLyId + ", " : "") +
                (dvtt != null ? "dvtt=" + dvtt + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (enabled != null ? "enabled=" + enabled + ", " : "") +
                (kyHieu != null ? "kyHieu=" + kyHieu + ", " : "") +
                (loai != null ? "loai=" + loai + ", " : "") +
                (maTinh != null ? "maTinh=" + maTinh + ", " : "") +
                (phone != null ? "phone=" + phone + ", " : "") +
                (soTaiKhoan != null ? "soTaiKhoan=" + soTaiKhoan + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (tenNganHang != null ? "tenNganHang=" + tenNganHang + ", " : "") +
            "}";
    }

}
