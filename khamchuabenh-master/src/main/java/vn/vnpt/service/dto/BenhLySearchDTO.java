package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.BenhLy} entity.
 */
@ApiModel(description = "NDB_TABLE=READ_BACKUP=1 Bảng chức thông tin bệnh lý")
public class BenhLySearchDTO implements Serializable {

    private Long id;


    /**
     * Mô tả icd bằng tiếng Việt
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Mô tả icd bằng tiếng Việt", required = true)
    private String moTa;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BenhLyDTO benhLyDTO = (BenhLyDTO) o;
        if (benhLyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), benhLyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BenhLyDTO{" +
            "id=" + getId() +
            ", moTa='" + getMoTa() + "'" +
            "}";
    }
}
