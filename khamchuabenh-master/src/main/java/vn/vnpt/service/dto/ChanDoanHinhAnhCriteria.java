package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.ChanDoanHinhAnh} entity. This class is used
 * in {@link vn.vnpt.web.rest.ChanDoanHinhAnhResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /chan-doan-hinh-anhs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ChanDoanHinhAnhCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter deleted;

    private BooleanFilter dichVuYeuCau;

    private BooleanFilter doppler;

    private BigDecimalFilter donGiaBenhVien;

    private BooleanFilter enabled;

    private BigDecimalFilter goiHanChiDinh;

    private BooleanFilter phamViChiDinh;

    private IntegerFilter phanTheoGioiTinh;

    private IntegerFilter sapXep;

    private IntegerFilter soLanThucHien;

    private BigDecimalFilter soLuongFilm;

    private BigDecimalFilter soLuongQuiDoi;

    private StringFilter ten;

    private StringFilter tenHienThi;

    private StringFilter maNoiBo;

    private StringFilter maDungChung;

    private LongFilter donViId;

    private LongFilter dotMaId;

    private LongFilter loaicdhaId;

    public ChanDoanHinhAnhCriteria() {
    }

    public ChanDoanHinhAnhCriteria(ChanDoanHinhAnhCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.deleted = other.deleted == null ? null : other.deleted.copy();
        this.dichVuYeuCau = other.dichVuYeuCau == null ? null : other.dichVuYeuCau.copy();
        this.doppler = other.doppler == null ? null : other.doppler.copy();
        this.donGiaBenhVien = other.donGiaBenhVien == null ? null : other.donGiaBenhVien.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.goiHanChiDinh = other.goiHanChiDinh == null ? null : other.goiHanChiDinh.copy();
        this.phamViChiDinh = other.phamViChiDinh == null ? null : other.phamViChiDinh.copy();
        this.phanTheoGioiTinh = other.phanTheoGioiTinh == null ? null : other.phanTheoGioiTinh.copy();
        this.sapXep = other.sapXep == null ? null : other.sapXep.copy();
        this.soLanThucHien = other.soLanThucHien == null ? null : other.soLanThucHien.copy();
        this.soLuongFilm = other.soLuongFilm == null ? null : other.soLuongFilm.copy();
        this.soLuongQuiDoi = other.soLuongQuiDoi == null ? null : other.soLuongQuiDoi.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.tenHienThi = other.tenHienThi == null ? null : other.tenHienThi.copy();
        this.maNoiBo = other.maNoiBo == null ? null : other.maNoiBo.copy();
        this.maDungChung = other.maDungChung == null ? null : other.maDungChung.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
        this.dotMaId = other.dotMaId == null ? null : other.dotMaId.copy();
        this.loaicdhaId = other.loaicdhaId == null ? null : other.loaicdhaId.copy();
    }

    @Override
    public ChanDoanHinhAnhCriteria copy() {
        return new ChanDoanHinhAnhCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getDeleted() {
        return deleted;
    }

    public void setDeleted(BooleanFilter deleted) {
        this.deleted = deleted;
    }

    public BooleanFilter getDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public void setDichVuYeuCau(BooleanFilter dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public BooleanFilter getDoppler() {
        return doppler;
    }

    public void setDoppler(BooleanFilter doppler) {
        this.doppler = doppler;
    }

    public BigDecimalFilter getDonGiaBenhVien() {
        return donGiaBenhVien;
    }

    public void setDonGiaBenhVien(BigDecimalFilter donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
    }

    public BooleanFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(BooleanFilter enabled) {
        this.enabled = enabled;
    }

    public BigDecimalFilter getGoiHanChiDinh() {
        return goiHanChiDinh;
    }

    public void setGoiHanChiDinh(BigDecimalFilter goiHanChiDinh) {
        this.goiHanChiDinh = goiHanChiDinh;
    }

    public BooleanFilter getPhamViChiDinh() {
        return phamViChiDinh;
    }

    public void setPhamViChiDinh(BooleanFilter phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
    }

    public IntegerFilter getPhanTheoGioiTinh() {
        return phanTheoGioiTinh;
    }

    public void setPhanTheoGioiTinh(IntegerFilter phanTheoGioiTinh) {
        this.phanTheoGioiTinh = phanTheoGioiTinh;
    }

    public IntegerFilter getSapXep() {
        return sapXep;
    }

    public void setSapXep(IntegerFilter sapXep) {
        this.sapXep = sapXep;
    }

    public IntegerFilter getSoLanThucHien() {
        return soLanThucHien;
    }

    public void setSoLanThucHien(IntegerFilter soLanThucHien) {
        this.soLanThucHien = soLanThucHien;
    }

    public BigDecimalFilter getSoLuongFilm() {
        return soLuongFilm;
    }

    public void setSoLuongFilm(BigDecimalFilter soLuongFilm) {
        this.soLuongFilm = soLuongFilm;
    }

    public BigDecimalFilter getSoLuongQuiDoi() {
        return soLuongQuiDoi;
    }

    public void setSoLuongQuiDoi(BigDecimalFilter soLuongQuiDoi) {
        this.soLuongQuiDoi = soLuongQuiDoi;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getTenHienThi() {
        return tenHienThi;
    }

    public void setTenHienThi(StringFilter tenHienThi) {
        this.tenHienThi = tenHienThi;
    }

    public StringFilter getMaNoiBo() {
        return maNoiBo;
    }

    public void setMaNoiBo(StringFilter maNoiBo) {
        this.maNoiBo = maNoiBo;
    }

    public StringFilter getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(StringFilter maDungChung) {
        this.maDungChung = maDungChung;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }

    public LongFilter getDotMaId() {
        return dotMaId;
    }

    public void setDotMaId(LongFilter dotMaId) {
        this.dotMaId = dotMaId;
    }

    public LongFilter getLoaicdhaId() {
        return loaicdhaId;
    }

    public void setLoaicdhaId(LongFilter loaicdhaId) {
        this.loaicdhaId = loaicdhaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ChanDoanHinhAnhCriteria that = (ChanDoanHinhAnhCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(deleted, that.deleted) &&
            Objects.equals(dichVuYeuCau, that.dichVuYeuCau) &&
            Objects.equals(doppler, that.doppler) &&
            Objects.equals(donGiaBenhVien, that.donGiaBenhVien) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(goiHanChiDinh, that.goiHanChiDinh) &&
            Objects.equals(phamViChiDinh, that.phamViChiDinh) &&
            Objects.equals(phanTheoGioiTinh, that.phanTheoGioiTinh) &&
            Objects.equals(sapXep, that.sapXep) &&
            Objects.equals(soLanThucHien, that.soLanThucHien) &&
            Objects.equals(soLuongFilm, that.soLuongFilm) &&
            Objects.equals(soLuongQuiDoi, that.soLuongQuiDoi) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(tenHienThi, that.tenHienThi) &&
            Objects.equals(maNoiBo, that.maNoiBo) &&
            Objects.equals(maDungChung, that.maDungChung) &&
            Objects.equals(donViId, that.donViId) &&
            Objects.equals(dotMaId, that.dotMaId) &&
            Objects.equals(loaicdhaId, that.loaicdhaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        deleted,
        dichVuYeuCau,
        doppler,
        donGiaBenhVien,
        enabled,
        goiHanChiDinh,
        phamViChiDinh,
        phanTheoGioiTinh,
        sapXep,
        soLanThucHien,
        soLuongFilm,
        soLuongQuiDoi,
        ten,
        tenHienThi,
        maNoiBo,
        maDungChung,
        donViId,
        dotMaId,
        loaicdhaId
        );
    }

    @Override
    public String toString() {
        return "ChanDoanHinhAnhCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (deleted != null ? "deleted=" + deleted + ", " : "") +
                (dichVuYeuCau != null ? "dichVuYeuCau=" + dichVuYeuCau + ", " : "") +
                (doppler != null ? "doppler=" + doppler + ", " : "") +
                (donGiaBenhVien != null ? "donGiaBenhVien=" + donGiaBenhVien + ", " : "") +
                (enabled != null ? "enabled=" + enabled + ", " : "") +
                (goiHanChiDinh != null ? "goiHanChiDinh=" + goiHanChiDinh + ", " : "") +
                (phamViChiDinh != null ? "phamViChiDinh=" + phamViChiDinh + ", " : "") +
                (phanTheoGioiTinh != null ? "phanTheoGioiTinh=" + phanTheoGioiTinh + ", " : "") +
                (sapXep != null ? "sapXep=" + sapXep + ", " : "") +
                (soLanThucHien != null ? "soLanThucHien=" + soLanThucHien + ", " : "") +
                (soLuongFilm != null ? "soLuongFilm=" + soLuongFilm + ", " : "") +
                (soLuongQuiDoi != null ? "soLuongQuiDoi=" + soLuongQuiDoi + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (tenHienThi != null ? "tenHienThi=" + tenHienThi + ", " : "") +
                (maNoiBo != null ? "maNoiBo=" + maNoiBo + ", " : "") +
                (maDungChung != null ? "maDungChung=" + maDungChung + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
                (dotMaId != null ? "dotMaId=" + dotMaId + ", " : "") +
                (loaicdhaId != null ? "loaicdhaId=" + loaicdhaId + ", " : "") +
            "}";
    }

}
