package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.BenhLy} entity. This class is used
 * in {@link vn.vnpt.web.rest.BenhLyResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /benh-lies?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BenhLyCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter enabled;

    private StringFilter icd;

    private StringFilter moTa;

    private StringFilter moTaTiengAnh;

    private StringFilter tenKhongDau;

    private StringFilter vietTat;

    private StringFilter vncode;

    private LongFilter nhomBenhLyId;

    public BenhLyCriteria() {
    }

    public BenhLyCriteria(BenhLyCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.icd = other.icd == null ? null : other.icd.copy();
        this.moTa = other.moTa == null ? null : other.moTa.copy();
        this.moTaTiengAnh = other.moTaTiengAnh == null ? null : other.moTaTiengAnh.copy();
        this.tenKhongDau = other.tenKhongDau == null ? null : other.tenKhongDau.copy();
        this.vietTat = other.vietTat == null ? null : other.vietTat.copy();
        this.vncode = other.vncode == null ? null : other.vncode.copy();
        this.nhomBenhLyId = other.nhomBenhLyId == null ? null : other.nhomBenhLyId.copy();
    }

    @Override
    public BenhLyCriteria copy() {
        return new BenhLyCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(BooleanFilter enabled) {
        this.enabled = enabled;
    }

    public StringFilter getIcd() {
        return icd;
    }

    public void setIcd(StringFilter icd) {
        this.icd = icd;
    }

    public StringFilter getMoTa() {
        return moTa;
    }

    public void setMoTa(StringFilter moTa) {
        this.moTa = moTa;
    }

    public StringFilter getMoTaTiengAnh() {
        return moTaTiengAnh;
    }

    public void setMoTaTiengAnh(StringFilter moTaTiengAnh) {
        this.moTaTiengAnh = moTaTiengAnh;
    }

    public StringFilter getTenKhongDau() {
        return tenKhongDau;
    }

    public void setTenKhongDau(StringFilter tenKhongDau) {
        this.tenKhongDau = tenKhongDau;
    }

    public StringFilter getVietTat() {
        return vietTat;
    }

    public void setVietTat(StringFilter vietTat) {
        this.vietTat = vietTat;
    }

    public StringFilter getVncode() {
        return vncode;
    }

    public void setVncode(StringFilter vncode) {
        this.vncode = vncode;
    }

    public LongFilter getNhomBenhLyId() {
        return nhomBenhLyId;
    }

    public void setNhomBenhLyId(LongFilter nhomBenhLyId) {
        this.nhomBenhLyId = nhomBenhLyId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BenhLyCriteria that = (BenhLyCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(icd, that.icd) &&
            Objects.equals(moTa, that.moTa) &&
            Objects.equals(moTaTiengAnh, that.moTaTiengAnh) &&
            Objects.equals(tenKhongDau, that.tenKhongDau) &&
            Objects.equals(vietTat, that.vietTat) &&
            Objects.equals(vncode, that.vncode) &&
            Objects.equals(nhomBenhLyId, that.nhomBenhLyId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        enabled,
        icd,
        moTa,
        moTaTiengAnh,
        tenKhongDau,
        vietTat,
        vncode,
        nhomBenhLyId
        );
    }

    @Override
    public String toString() {
        return "BenhLyCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (enabled != null ? "enabled=" + enabled + ", " : "") +
                (icd != null ? "icd=" + icd + ", " : "") +
                (moTa != null ? "moTa=" + moTa + ", " : "") +
                (moTaTiengAnh != null ? "moTaTiengAnh=" + moTaTiengAnh + ", " : "") +
                (tenKhongDau != null ? "tenKhongDau=" + tenKhongDau + ", " : "") +
                (vietTat != null ? "vietTat=" + vietTat + ", " : "") +
                (vncode != null ? "vncode=" + vncode + ", " : "") +
                (nhomBenhLyId != null ? "nhomBenhLyId=" + nhomBenhLyId + ", " : "") +
            "}";
    }

}
