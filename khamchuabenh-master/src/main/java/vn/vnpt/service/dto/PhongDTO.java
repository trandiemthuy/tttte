package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.Phong} entity.
 */
public class PhongDTO implements Serializable {
    
    private Long id;

    /**
     * Trạng thái: 1: Còn hiệu lực. 0 Hết hiệu lực
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái: 1: Còn hiệu lực. 0 Hết hiệu lực", required = true)
    private Boolean enabled;

    /**
     * Ký hiệu phòng
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Ký hiệu phòng", required = true)
    private String kyHieu;

    /**
     * Loại phòng
     */
    @NotNull
    @ApiModelProperty(value = "Loại phòng", required = true)
    private BigDecimal loai;

    /**
     * Số điện thoại của phòng
     */
    @Size(max = 15)
    @ApiModelProperty(value = "Số điện thoại của phòng")
    private String phone;

    /**
     * Tên phòng
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên phòng", required = true)
    private String ten;

    /**
     * Vị trí phòng
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Vị trí phòng")
    private String viTri;

    /**
     * Mã khoa
     */
    @ApiModelProperty(value = "Mã khoa")

    private Long khoaId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getKyHieu() {
        return kyHieu;
    }

    public void setKyHieu(String kyHieu) {
        this.kyHieu = kyHieu;
    }

    public BigDecimal getLoai() {
        return loai;
    }

    public void setLoai(BigDecimal loai) {
        this.loai = loai;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getViTri() {
        return viTri;
    }

    public void setViTri(String viTri) {
        this.viTri = viTri;
    }

    public Long getKhoaId() {
        return khoaId;
    }

    public void setKhoaId(Long khoaId) {
        this.khoaId = khoaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PhongDTO phongDTO = (PhongDTO) o;
        if (phongDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), phongDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PhongDTO{" +
            "id=" + getId() +
            ", enabled='" + isEnabled() + "'" +
            ", kyHieu='" + getKyHieu() + "'" +
            ", loai=" + getLoai() +
            ", phone='" + getPhone() + "'" +
            ", ten='" + getTen() + "'" +
            ", viTri='" + getViTri() + "'" +
            ", khoaId=" + getKhoaId() +
            "}";
    }
}
