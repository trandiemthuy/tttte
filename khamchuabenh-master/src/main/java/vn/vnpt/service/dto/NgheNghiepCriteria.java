package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.NgheNghiep} entity. This class is used
 * in {@link vn.vnpt.web.rest.NgheNghiepResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /nghe-nghieps?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NgheNghiepCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter ma4069;

    private StringFilter ten4069;

    public NgheNghiepCriteria() {
    }

    public NgheNghiepCriteria(NgheNghiepCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ma4069 = other.ma4069 == null ? null : other.ma4069.copy();
        this.ten4069 = other.ten4069 == null ? null : other.ten4069.copy();
    }

    @Override
    public NgheNghiepCriteria copy() {
        return new NgheNghiepCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getMa4069() {
        return ma4069;
    }

    public void setMa4069(IntegerFilter ma4069) {
        this.ma4069 = ma4069;
    }

    public StringFilter getTen4069() {
        return ten4069;
    }

    public void setTen4069(StringFilter ten4069) {
        this.ten4069 = ten4069;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NgheNghiepCriteria that = (NgheNghiepCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(ma4069, that.ma4069) &&
            Objects.equals(ten4069, that.ten4069);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        ma4069,
        ten4069
        );
    }

    @Override
    public String toString() {
        return "NgheNghiepCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (ma4069 != null ? "ma4069=" + ma4069 + ", " : "") +
                (ten4069 != null ? "ten4069=" + ten4069 + ", " : "") +
            "}";
    }

}
