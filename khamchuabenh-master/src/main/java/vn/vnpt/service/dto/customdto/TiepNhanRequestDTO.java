package vn.vnpt.service.dto.customdto;

import java.io.Serializable;
import java.util.Objects;

public class TiepNhanRequestDTO implements Serializable {
    private TiepNhanDTO tiepNhanDTO;
    private Long dichVuKhamId;

    public TiepNhanRequestDTO() {
    }

    public TiepNhanRequestDTO(TiepNhanDTO tiepNhanDTO, Long dichVuKhamId) {
        this.tiepNhanDTO = tiepNhanDTO;
        this.dichVuKhamId = dichVuKhamId;
    }

    public TiepNhanDTO getTiepNhanDTO() {
        return tiepNhanDTO;
    }

    public void setTiepNhanDTO(TiepNhanDTO tiepNhanDTO) {
        this.tiepNhanDTO = tiepNhanDTO;
    }

    public Long getDichVuKhamId() {
        return dichVuKhamId;
    }

    public void setDichVuKhamId(Long dichVuKhamId) {
        this.dichVuKhamId = dichVuKhamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TiepNhanRequestDTO)) return false;
        TiepNhanRequestDTO that = (TiepNhanRequestDTO) o;
        return Objects.equals(getTiepNhanDTO(), that.getTiepNhanDTO()) &&
            Objects.equals(getDichVuKhamId(), that.getDichVuKhamId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTiepNhanDTO(), getDichVuKhamId());
    }
}
