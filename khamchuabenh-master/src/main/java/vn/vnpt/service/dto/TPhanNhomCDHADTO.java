package vn.vnpt.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.TPhanNhomCDHA} entity.
 */
public class TPhanNhomCDHADTO implements Serializable {
    
    private Long id;


    private Long nhom_cdhaId;

    private Long cdhaId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNhom_cdhaId() {
        return nhom_cdhaId;
    }

    public void setNhom_cdhaId(Long nhomChanDoanHinhAnhId) {
        this.nhom_cdhaId = nhomChanDoanHinhAnhId;
    }

    public Long getCdhaId() {
        return cdhaId;
    }

    public void setCdhaId(Long chanDoanHinhAnhId) {
        this.cdhaId = chanDoanHinhAnhId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TPhanNhomCDHADTO tPhanNhomCDHADTO = (TPhanNhomCDHADTO) o;
        if (tPhanNhomCDHADTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tPhanNhomCDHADTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TPhanNhomCDHADTO{" +
            "id=" + getId() +
            ", nhom_cdhaId=" + getNhom_cdhaId() +
            ", cdhaId=" + getCdhaId() +
            "}";
    }
}
