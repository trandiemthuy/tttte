package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.BenhLy} entity.
 */
@ApiModel(description = "NDB_TABLE=READ_BACKUP=1 Bảng chức thông tin bệnh lý")
public class BenhLyDTO implements Serializable {
    
    private Long id;

    /**
     * Trạng thái của 1 dòng thông tin bệnh lý\n\n1: Có hiệu lực.\n\n0: Không có hiệu lực.
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái của 1 dòng thông tin bệnh lý\n\n1: Có hiệu lực.\n\n0: Không có hiệu lực.", required = true)
    private Boolean enabled;

    /**
     * Mã icd
     */
    @NotNull
    @Size(max = 200)
    @ApiModelProperty(value = "Mã icd", required = true)
    private String icd;

    /**
     * Mô tả icd bằng tiếng Việt
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Mô tả icd bằng tiếng Việt", required = true)
    private String moTa;

    /**
     * Mô tả icd bằng tiếng Anh
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Mô tả icd bằng tiếng Anh")
    private String moTaTiengAnh;

    /**
     * Tên không dấu của icd
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên không dấu của icd")
    private String tenKhongDau;

    /**
     * Mã viết tắt của icd
     */
    @Size(max = 100)
    @ApiModelProperty(value = "Mã viết tắt của icd")
    private String vietTat;

    /**
     * Mã bệnh theo VNCode
     */
    @NotNull
    @ApiModelProperty(value = "Mã bệnh theo VNCode", required = true)
    private String vncode;

    /**
     * Mã nhóm bệnh lý
     */
    @ApiModelProperty(value = "Mã nhóm bệnh lý")

    private Long nhomBenhLyId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getIcd() {
        return icd;
    }

    public void setIcd(String icd) {
        this.icd = icd;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getMoTaTiengAnh() {
        return moTaTiengAnh;
    }

    public void setMoTaTiengAnh(String moTaTiengAnh) {
        this.moTaTiengAnh = moTaTiengAnh;
    }

    public String getTenKhongDau() {
        return tenKhongDau;
    }

    public void setTenKhongDau(String tenKhongDau) {
        this.tenKhongDau = tenKhongDau;
    }

    public String getVietTat() {
        return vietTat;
    }

    public void setVietTat(String vietTat) {
        this.vietTat = vietTat;
    }

    public String getVncode() {
        return vncode;
    }

    public void setVncode(String vncode) {
        this.vncode = vncode;
    }

    public Long getNhomBenhLyId() {
        return nhomBenhLyId;
    }

    public void setNhomBenhLyId(Long nhomBenhLyId) {
        this.nhomBenhLyId = nhomBenhLyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BenhLyDTO benhLyDTO = (BenhLyDTO) o;
        if (benhLyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), benhLyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BenhLyDTO{" +
            "id=" + getId() +
            ", enabled='" + isEnabled() + "'" +
            ", icd='" + getIcd() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", moTaTiengAnh='" + getMoTaTiengAnh() + "'" +
            ", tenKhongDau='" + getTenKhongDau() + "'" +
            ", vietTat='" + getVietTat() + "'" +
            ", vncode='" + getVncode() + "'" +
            ", nhomBenhLyId=" + getNhomBenhLyId() +
            "}";
    }
}
