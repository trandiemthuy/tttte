package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import vn.vnpt.domain.DotDieuTri;
import vn.vnpt.domain.DotDieuTriId;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.DotDieuTri} entity.
 */
@ApiModel(description = "NDB_TABLE=READ_BACKUP=1 Đợt điều trị theo")
public class DotDieuTriDTO implements Serializable {

    @NotNull
    private Long id;

    @NotNull
    private Long bakbId;

    @NotNull
    private Long benhNhanId;

    @NotNull
    private Long donViId;

    private String soThuTu;

    /**
     * Giá tiền cận trên BHYT
     */
    @ApiModelProperty(value = "Giá tiền cận trên BHYT")
    private Integer bhytCanTren;

    /**
     * Giá tiền cận trên BHYT quy định cho các Dịch vụ kỹ thuật cao
     */
    @ApiModelProperty(value = "Giá tiền cận trên BHYT quy định cho các Dịch vụ kỹ thuật cao")
    private Integer bhytCanTrenKtc;

    /**
     * Thông tin mã khu vực của thẻ BHYT
     */
    @ApiModelProperty(value = "Thông tin mã khu vực của thẻ BHYT")
    private String bhytMaKhuVuc;

    /**
     * Thời gian có hiệu lực của BHYT
     */
    @ApiModelProperty(value = "Thời gian có hiệu lực của BHYT")
    private LocalDate bhytNgayBatDau;

    /**
     * Thời gian được hưởng quyền lợi bảo hiểm 5 năm
     */
    @ApiModelProperty(value = "Thời gian được hưởng quyền lợi bảo hiểm 5 năm")
    private LocalDate bhytNgayDuNamNam;

    /**
     * Thời gian hết hạn của thẻ BHYT
     */
    @ApiModelProperty(value = "Thời gian hết hạn của thẻ BHYT")
    private LocalDate bhytNgayHetHan;

    /**
     * Trạng thái nội tỉnh của thẻ BHYT:\n1: Nội tỉnh\n0: Ngoại tỉnh
     */
    @ApiModelProperty(value = "Trạng thái nội tỉnh của thẻ BHYT:\n1: Nội tỉnh\n0: Ngoại tỉnh")
    private Boolean bhytNoiTinh;

    /**
     * Mã thẻ BHYT
     */
    @Size(max = 30)
    @ApiModelProperty(value = "Mã thẻ BHYT")
    private String bhytSoThe;

    /**
     * Tỷ lệ miễn giảm của thẻ BHYT
     */
    @ApiModelProperty(value = "Tỷ lệ miễn giảm của thẻ BHYT")
    private Integer bhytTyLeMienGiam;

    /**
     * Tỷ lệ miễn giảm cho các dịch vụ kỹ thuật cao của thẻ BHYT
     */
    @ApiModelProperty(value = "Tỷ lệ miễn giảm cho các dịch vụ kỹ thuật cao của thẻ BHYT")
    private Integer bhytTyLeMienGiamKtc;

    /**
     * Trạng thái có bảo hiểm của
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái có bảo hiểm của", required = true)
    private Boolean coBaoHiem;

    /**
     * Thông tin địa chỉ BHYT của bệnh nhân
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Thông tin địa chỉ BHYT của bệnh nhân")
    private String bhytDiaChi;

    /**
     * Tên đối tượng của thẻ BHYT
     */
    @Size(max = 200)
    @ApiModelProperty(value = "Tên đối tượng của thẻ BHYT")
    private String doiTuongBhytTen;

    /**
     * Trạng thái đúng tuyến của thẻ BHYT\n1: Đúng tuyến\n0: Không đúng tuyến
     */
    @ApiModelProperty(value = "Trạng thái đúng tuyến của thẻ BHYT\n1: Đúng tuyến\n0: Không đúng tuyến")
    private Boolean dungTuyen;

    /**
     * Mã giấy chứng sinh, giấy khai sinh....
     */
    @Size(max = 200)
    @ApiModelProperty(value = "Mã giấy chứng sinh, giấy khai sinh....")
    private String giayToTreEm;

    /**
     * Mã loại giấy tờ của trẻ em như giấy khai sinh, giấy chứng sinh
     */
    @ApiModelProperty(value = "Mã loại giấy tờ của trẻ em như giấy khai sinh, giấy chứng sinh")
    private Integer loaiGiayToTreEm;

    /**
     * Thời gian miễn cùng chi trả của thẻ BHYT
     */
    @ApiModelProperty(value = "Thời gian miễn cùng chi trả của thẻ BHYT")
    private LocalDate ngayMienCungChiTra;

    /**
     * Thông tin thẻ TEKT
     */
    @ApiModelProperty(value = "Thông tin thẻ TEKT")
    private Boolean theTreEm;

    /**
     * Trạng thái thông tuyến theo bhxh xml 4210:\n1: Thông tuyến\n0: không thông tuyến
     */
    @ApiModelProperty(value = "Trạng thái thông tuyến theo bhxh xml 4210:\n1: Thông tuyến\n0: không thông tuyến")
    private Boolean thongTuyenBhxhXml4210;

    /**
     * Trạng thái của Bệnh án khám bệnh:\n0: mới vào khoa, chờ khám;\n1: Điều trị, khám có giường;\n2: Điều trị, khám không giường;\n3: Xuất viện, hoàn tất khám;\n4: chuyển tuyến, chuyển viện;\n5: Nhập viện;\n6: tử vong;\n7: trốn viện;\n8: Kết thúc đợt ĐT, thêm đợt ĐT mới;
     */
    @ApiModelProperty(value = "Trạng thái của Bệnh án khám bệnh:\n0: mới vào khoa, chờ khám;\n1: Điều trị, khám có giường;\n2: Điều trị, khám không giường;\n3: Xuất viện, hoàn tất khám;\n4: chuyển tuyến, chuyển viện;\n5: Nhập viện;\n6: tử vong;\n7: trốn viện;\n8: Kết thúc đợt ĐT, thêm đợt ĐT mới;")
    private Integer trangThai;

    /**
     * Loại của bệnh án 1: ngoại trú. 2: Nội trú, 3: điều trị bệnh án ngoại trú
     */
    @ApiModelProperty(value = "Loại của bệnh án 1: ngoại trú. 2: Nội trú, 3: điều trị bệnh án ngoại trú")
    private Integer loai;

    /**
     * Nơi đăng ký khám chữa bệnh ban đầu
     */
    @Size(max = 30)
    @ApiModelProperty(value = "Nơi đăng ký khám chữa bệnh ban đầu")
    private String bhytNoiDkKcbbd;

    @NotNull
    private Integer nam;

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public String getSoThuTu() {
        return soThuTu;
    }

    public void setSoThuTu(String soThuTu) {
        this.soThuTu = soThuTu;
    }

    public Integer getBhytCanTren() {
        return bhytCanTren;
    }

    public void setBhytCanTren(Integer bhytCanTren) {
        this.bhytCanTren = bhytCanTren;
    }

    public Integer getBhytCanTrenKtc() {
        return bhytCanTrenKtc;
    }

    public void setBhytCanTrenKtc(Integer bhytCanTrenKtc) {
        this.bhytCanTrenKtc = bhytCanTrenKtc;
    }

    public String getBhytMaKhuVuc() {
        return bhytMaKhuVuc;
    }

    public void setBhytMaKhuVuc(String bhytMaKhuVuc) {
        this.bhytMaKhuVuc = bhytMaKhuVuc;
    }

    public LocalDate getBhytNgayBatDau() {
        return bhytNgayBatDau;
    }

    public void setBhytNgayBatDau(LocalDate bhytNgayBatDau) {
        this.bhytNgayBatDau = bhytNgayBatDau;
    }

    public LocalDate getBhytNgayDuNamNam() {
        return bhytNgayDuNamNam;
    }

    public void setBhytNgayDuNamNam(LocalDate bhytNgayDuNamNam) {
        this.bhytNgayDuNamNam = bhytNgayDuNamNam;
    }

    public LocalDate getBhytNgayHetHan() {
        return bhytNgayHetHan;
    }

    public void setBhytNgayHetHan(LocalDate bhytNgayHetHan) {
        this.bhytNgayHetHan = bhytNgayHetHan;
    }

    public Boolean isBhytNoiTinh() {
        return bhytNoiTinh;
    }

    public void setBhytNoiTinh(Boolean bhytNoiTinh) {
        this.bhytNoiTinh = bhytNoiTinh;
    }

    public String getBhytSoThe() {
        return bhytSoThe;
    }

    public void setBhytSoThe(String bhytSoThe) {
        this.bhytSoThe = bhytSoThe;
    }

    public Integer getBhytTyLeMienGiam() {
        return bhytTyLeMienGiam;
    }

    public void setBhytTyLeMienGiam(Integer bhytTyLeMienGiam) {
        this.bhytTyLeMienGiam = bhytTyLeMienGiam;
    }

    public Integer getBhytTyLeMienGiamKtc() {
        return bhytTyLeMienGiamKtc;
    }

    public void setBhytTyLeMienGiamKtc(Integer bhytTyLeMienGiamKtc) {
        this.bhytTyLeMienGiamKtc = bhytTyLeMienGiamKtc;
    }

    public Boolean isCoBaoHiem() {
        return coBaoHiem;
    }

    public void setCoBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public String getBhytDiaChi() {
        return bhytDiaChi;
    }

    public void setBhytDiaChi(String bhytDiaChi) {
        this.bhytDiaChi = bhytDiaChi;
    }

    public String getDoiTuongBhytTen() {
        return doiTuongBhytTen;
    }

    public void setDoiTuongBhytTen(String doiTuongBhytTen) {
        this.doiTuongBhytTen = doiTuongBhytTen;
    }

    public Boolean isDungTuyen() {
        return dungTuyen;
    }

    public void setDungTuyen(Boolean dungTuyen) {
        this.dungTuyen = dungTuyen;
    }

    public String getGiayToTreEm() {
        return giayToTreEm;
    }

    public void setGiayToTreEm(String giayToTreEm) {
        this.giayToTreEm = giayToTreEm;
    }

    public Integer getLoaiGiayToTreEm() {
        return loaiGiayToTreEm;
    }

    public void setLoaiGiayToTreEm(Integer loaiGiayToTreEm) {
        this.loaiGiayToTreEm = loaiGiayToTreEm;
    }

    public LocalDate getNgayMienCungChiTra() {
        return ngayMienCungChiTra;
    }

    public void setNgayMienCungChiTra(LocalDate ngayMienCungChiTra) {
        this.ngayMienCungChiTra = ngayMienCungChiTra;
    }

    public Boolean isTheTreEm() {
        return theTreEm;
    }

    public void setTheTreEm(Boolean theTreEm) {
        this.theTreEm = theTreEm;
    }

    public Boolean isThongTuyenBhxhXml4210() {
        return thongTuyenBhxhXml4210;
    }

    public void setThongTuyenBhxhXml4210(Boolean thongTuyenBhxhXml4210) {
        this.thongTuyenBhxhXml4210 = thongTuyenBhxhXml4210;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    public Integer getLoai() {
        return loai;
    }

    public void setLoai(Integer loai) {
        this.loai = loai;
    }

    public String getBhytNoiDkKcbbd() {
        return bhytNoiDkKcbbd;
    }

    public void setBhytNoiDkKcbbd(String bhytNoiDkKcbbd) {
        this.bhytNoiDkKcbbd = bhytNoiDkKcbbd;
    }

    public Integer getNam() {
        return nam;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    public DotDieuTriId getCompositeId(){
        DotDieuTriId result = new DotDieuTriId();
        result.setId(this.getId());
        result.setBakbId(this.getBakbId());
        result.setBenhNhanId(this.getBenhNhanId());
        result.setDonViId(this.getDonViId());
        return result;
    }

    public void setCompositeId(DotDieuTriId id){
        this.id = id.getId();
        this.bakbId = id.getBakbId();
        this.benhNhanId = id.getBenhNhanId();
        this.donViId = id.getDonViId();
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DotDieuTriDTO)) return false;
        DotDieuTriDTO that = (DotDieuTriDTO) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getBakbId(), that.getBakbId()) &&
            Objects.equals(getBenhNhanId(), that.getBenhNhanId()) &&
            Objects.equals(getDonViId(), that.getDonViId()) &&
            Objects.equals(getSoThuTu(), that.getSoThuTu()) &&
            Objects.equals(getBhytCanTren(), that.getBhytCanTren()) &&
            Objects.equals(getBhytCanTrenKtc(), that.getBhytCanTrenKtc()) &&
            Objects.equals(getBhytMaKhuVuc(), that.getBhytMaKhuVuc()) &&
            Objects.equals(getBhytNgayBatDau(), that.getBhytNgayBatDau()) &&
            Objects.equals(getBhytNgayDuNamNam(), that.getBhytNgayDuNamNam()) &&
            Objects.equals(getBhytNgayHetHan(), that.getBhytNgayHetHan()) &&
            Objects.equals(bhytNoiTinh, that.bhytNoiTinh) &&
            Objects.equals(getBhytSoThe(), that.getBhytSoThe()) &&
            Objects.equals(getBhytTyLeMienGiam(), that.getBhytTyLeMienGiam()) &&
            Objects.equals(getBhytTyLeMienGiamKtc(), that.getBhytTyLeMienGiamKtc()) &&
            Objects.equals(coBaoHiem, that.coBaoHiem) &&
            Objects.equals(getBhytDiaChi(), that.getBhytDiaChi()) &&
            Objects.equals(getDoiTuongBhytTen(), that.getDoiTuongBhytTen()) &&
            Objects.equals(dungTuyen, that.dungTuyen) &&
            Objects.equals(getGiayToTreEm(), that.getGiayToTreEm()) &&
            Objects.equals(getLoaiGiayToTreEm(), that.getLoaiGiayToTreEm()) &&
            Objects.equals(getNgayMienCungChiTra(), that.getNgayMienCungChiTra()) &&
            Objects.equals(theTreEm, that.theTreEm) &&
            Objects.equals(thongTuyenBhxhXml4210, that.thongTuyenBhxhXml4210) &&
            Objects.equals(getTrangThai(), that.getTrangThai()) &&
            Objects.equals(getLoai(), that.getLoai()) &&
            Objects.equals(getBhytNoiDkKcbbd(), that.getBhytNoiDkKcbbd()) &&
            Objects.equals(getNam(), that.getNam());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBakbId(), getBenhNhanId(), getDonViId(), getSoThuTu(), getBhytCanTren(), getBhytCanTrenKtc(), getBhytMaKhuVuc(), getBhytNgayBatDau(), getBhytNgayDuNamNam(), getBhytNgayHetHan(), bhytNoiTinh, getBhytSoThe(), getBhytTyLeMienGiam(), getBhytTyLeMienGiamKtc(), coBaoHiem, getBhytDiaChi(), getDoiTuongBhytTen(), dungTuyen, getGiayToTreEm(), getLoaiGiayToTreEm(), getNgayMienCungChiTra(), theTreEm, thongTuyenBhxhXml4210, getTrangThai(), getLoai(), getBhytNoiDkKcbbd(), getNam());
    }

    @Override
    public String toString() {
        return "DotDieuTriDTO{" +
            "id=" + id +
            ", bakbId=" + bakbId +
            ", benhNhanId=" + benhNhanId +
            ", donViId=" + donViId +
            ", soThuTu='" + soThuTu + '\'' +
            ", bhytCanTren=" + bhytCanTren +
            ", bhytCanTrenKtc=" + bhytCanTrenKtc +
            ", bhytMaKhuVuc='" + bhytMaKhuVuc + '\'' +
            ", bhytNgayBatDau=" + bhytNgayBatDau +
            ", bhytNgayDuNamNam=" + bhytNgayDuNamNam +
            ", bhytNgayHetHan=" + bhytNgayHetHan +
            ", bhytNoiTinh=" + bhytNoiTinh +
            ", bhytSoThe='" + bhytSoThe + '\'' +
            ", bhytTyLeMienGiam=" + bhytTyLeMienGiam +
            ", bhytTyLeMienGiamKtc=" + bhytTyLeMienGiamKtc +
            ", coBaoHiem=" + coBaoHiem +
            ", bhytDiaChi='" + bhytDiaChi + '\'' +
            ", doiTuongBhytTen='" + doiTuongBhytTen + '\'' +
            ", dungTuyen=" + dungTuyen +
            ", giayToTreEm='" + giayToTreEm + '\'' +
            ", loaiGiayToTreEm=" + loaiGiayToTreEm +
            ", ngayMienCungChiTra=" + ngayMienCungChiTra +
            ", theTreEm=" + theTreEm +
            ", thongTuyenBhxhXml4210=" + thongTuyenBhxhXml4210 +
            ", trangThai=" + trangThai +
            ", loai=" + loai +
            ", bhytNoiDkKcbbd='" + bhytNoiDkKcbbd + '\'' +
            ", nam=" + nam +
            '}';
    }
}
