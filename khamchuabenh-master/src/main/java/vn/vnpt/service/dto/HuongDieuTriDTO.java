package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link vn.vnpt.domain.HuongDieuTri} entity.
 */
public class HuongDieuTriDTO implements Serializable {
    
    private Long id;

    /**
     * Trạng thái hướng điều trị: 0: Cả nội và ngoại trú. 1: Ngoại trú, 2: Nội trú
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái hướng điều trị: 0: Cả nội và ngoại trú. 1: Ngoại trú, 2: Nội trú", required = true)
    private Integer noiTru;

    /**
     * Tên hướng điều trị
     */
    @Size(max = 200)
    @ApiModelProperty(value = "Tên hướng điều trị")
    private String ten;

    /**
     * Mã hướng điều trị
     */
    @NotNull
    @ApiModelProperty(value = "Mã hướng điều trị", required = true)
    private Integer ma;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNoiTru() {
        return noiTru;
    }

    public void setNoiTru(Integer noiTru) {
        this.noiTru = noiTru;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Integer getMa() {
        return ma;
    }

    public void setMa(Integer ma) {
        this.ma = ma;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HuongDieuTriDTO)) {
            return false;
        }

        return id != null && id.equals(((HuongDieuTriDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HuongDieuTriDTO{" +
            "id=" + getId() +
            ", noiTru=" + getNoiTru() +
            ", ten='" + getTen() + "'" +
            ", ma=" + getMa() +
            "}";
    }
}
