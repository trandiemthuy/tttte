package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.Country} entity. This class is used
 * in {@link vn.vnpt.web.rest.CountryResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /countries?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CountryCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter isoCode;

    private StringFilter isoNumeric;

    private StringFilter name;

    private IntegerFilter maCtk;

    private StringFilter tenCtk;

    public CountryCriteria() {
    }

    public CountryCriteria(CountryCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.isoCode = other.isoCode == null ? null : other.isoCode.copy();
        this.isoNumeric = other.isoNumeric == null ? null : other.isoNumeric.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.maCtk = other.maCtk == null ? null : other.maCtk.copy();
        this.tenCtk = other.tenCtk == null ? null : other.tenCtk.copy();
    }

    @Override
    public CountryCriteria copy() {
        return new CountryCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(StringFilter isoCode) {
        this.isoCode = isoCode;
    }

    public StringFilter getIsoNumeric() {
        return isoNumeric;
    }

    public void setIsoNumeric(StringFilter isoNumeric) {
        this.isoNumeric = isoNumeric;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public IntegerFilter getMaCtk() {
        return maCtk;
    }

    public void setMaCtk(IntegerFilter maCtk) {
        this.maCtk = maCtk;
    }

    public StringFilter getTenCtk() {
        return tenCtk;
    }

    public void setTenCtk(StringFilter tenCtk) {
        this.tenCtk = tenCtk;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CountryCriteria that = (CountryCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(isoCode, that.isoCode) &&
            Objects.equals(isoNumeric, that.isoNumeric) &&
            Objects.equals(name, that.name) &&
            Objects.equals(maCtk, that.maCtk) &&
            Objects.equals(tenCtk, that.tenCtk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        isoCode,
        isoNumeric,
        name,
        maCtk,
        tenCtk
        );
    }

    @Override
    public String toString() {
        return "CountryCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (isoCode != null ? "isoCode=" + isoCode + ", " : "") +
                (isoNumeric != null ? "isoNumeric=" + isoNumeric + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (maCtk != null ? "maCtk=" + maCtk + ", " : "") +
                (tenCtk != null ? "tenCtk=" + tenCtk + ", " : "") +
            "}";
    }

}
