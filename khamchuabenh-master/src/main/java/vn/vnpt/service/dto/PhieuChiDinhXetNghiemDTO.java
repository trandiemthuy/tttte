package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.PhieuChiDinhXetNghiem} entity.
 */
public class PhieuChiDinhXetNghiemDTO implements Serializable {
    
    private Long id;

    /**
     * Chẩn đoán tổng quát
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Chẩn đoán tổng quát")
    private String chanDoanTongQuat;

    /**
     * Ghi chú
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Ghi chú")
    private String ghiChu;

    /**
     * Kết quả tổng quát
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Kết quả tổng quát")
    private String ketQuaTongQuat;

    @NotNull
    private Integer nam;


    private Long benhNhanId;

    private Long donViId;

    private Long bakbId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChanDoanTongQuat() {
        return chanDoanTongQuat;
    }

    public void setChanDoanTongQuat(String chanDoanTongQuat) {
        this.chanDoanTongQuat = chanDoanTongQuat;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getKetQuaTongQuat() {
        return ketQuaTongQuat;
    }

    public void setKetQuaTongQuat(String ketQuaTongQuat) {
        this.ketQuaTongQuat = ketQuaTongQuat;
    }

    public Integer getNam() {
        return nam;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhAnKhamBenhId) {
        this.benhNhanId = benhAnKhamBenhId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long benhAnKhamBenhId) {
        this.donViId = benhAnKhamBenhId;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long benhAnKhamBenhId) {
        this.bakbId = benhAnKhamBenhId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PhieuChiDinhXetNghiemDTO phieuChiDinhXetNghiemDTO = (PhieuChiDinhXetNghiemDTO) o;
        if (phieuChiDinhXetNghiemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), phieuChiDinhXetNghiemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PhieuChiDinhXetNghiemDTO{" +
            "id=" + getId() +
            ", chanDoanTongQuat='" + getChanDoanTongQuat() + "'" +
            ", ghiChu='" + getGhiChu() + "'" +
            ", ketQuaTongQuat='" + getKetQuaTongQuat() + "'" +
            ", nam=" + getNam() +
            ", benhNhanId=" + getBenhNhanId() +
            ", donViId=" + getDonViId() +
            ", bakbId=" + getBakbId() +
            "}";
    }
}
