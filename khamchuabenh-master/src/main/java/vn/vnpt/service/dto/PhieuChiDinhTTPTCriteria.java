package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.PhieuChiDinhTTPT} entity. This class is used
 * in {@link vn.vnpt.web.rest.PhieuChiDinhTTPTResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /phieu-chi-dinh-ttpts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PhieuChiDinhTTPTCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter chanDoanTongQuat;

    private StringFilter ghiChu;

    private StringFilter ketQuaTongQuat;

    private IntegerFilter nam;

    private LongFilter donViId;

    private LongFilter benhNhanId;

    private LongFilter bakbId;

    public PhieuChiDinhTTPTCriteria() {
    }

    public PhieuChiDinhTTPTCriteria(PhieuChiDinhTTPTCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.chanDoanTongQuat = other.chanDoanTongQuat == null ? null : other.chanDoanTongQuat.copy();
        this.ghiChu = other.ghiChu == null ? null : other.ghiChu.copy();
        this.ketQuaTongQuat = other.ketQuaTongQuat == null ? null : other.ketQuaTongQuat.copy();
        this.nam = other.nam == null ? null : other.nam.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
        this.benhNhanId = other.benhNhanId == null ? null : other.benhNhanId.copy();
        this.bakbId = other.bakbId == null ? null : other.bakbId.copy();
    }

    @Override
    public PhieuChiDinhTTPTCriteria copy() {
        return new PhieuChiDinhTTPTCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getChanDoanTongQuat() {
        return chanDoanTongQuat;
    }

    public void setChanDoanTongQuat(StringFilter chanDoanTongQuat) {
        this.chanDoanTongQuat = chanDoanTongQuat;
    }

    public StringFilter getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(StringFilter ghiChu) {
        this.ghiChu = ghiChu;
    }

    public StringFilter getKetQuaTongQuat() {
        return ketQuaTongQuat;
    }

    public void setKetQuaTongQuat(StringFilter ketQuaTongQuat) {
        this.ketQuaTongQuat = ketQuaTongQuat;
    }

    public IntegerFilter getNam() {
        return nam;
    }

    public void setNam(IntegerFilter nam) {
        this.nam = nam;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }

    public LongFilter getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(LongFilter benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public LongFilter getBakbId() {
        return bakbId;
    }

    public void setBakbId(LongFilter bakbId) {
        this.bakbId = bakbId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PhieuChiDinhTTPTCriteria that = (PhieuChiDinhTTPTCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(chanDoanTongQuat, that.chanDoanTongQuat) &&
            Objects.equals(ghiChu, that.ghiChu) &&
            Objects.equals(ketQuaTongQuat, that.ketQuaTongQuat) &&
            Objects.equals(nam, that.nam) &&
            Objects.equals(donViId, that.donViId) &&
            Objects.equals(benhNhanId, that.benhNhanId) &&
            Objects.equals(bakbId, that.bakbId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        chanDoanTongQuat,
        ghiChu,
        ketQuaTongQuat,
        nam,
        donViId,
        benhNhanId,
        bakbId
        );
    }

    @Override
    public String toString() {
        return "PhieuChiDinhTTPTCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (chanDoanTongQuat != null ? "chanDoanTongQuat=" + chanDoanTongQuat + ", " : "") +
                (ghiChu != null ? "ghiChu=" + ghiChu + ", " : "") +
                (ketQuaTongQuat != null ? "ketQuaTongQuat=" + ketQuaTongQuat + ", " : "") +
                (nam != null ? "nam=" + nam + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
                (benhNhanId != null ? "benhNhanId=" + benhNhanId + ", " : "") +
                (bakbId != null ? "bakbId=" + bakbId + ", " : "") +
            "}";
    }

}
