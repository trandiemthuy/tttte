package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.LoaiXetNghiem;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.LoaiXetNghiemRepository;
import vn.vnpt.service.dto.LoaiXetNghiemCriteria;
import vn.vnpt.service.dto.LoaiXetNghiemDTO;
import vn.vnpt.service.mapper.LoaiXetNghiemMapper;

/**
 * Service for executing complex queries for {@link LoaiXetNghiem} entities in the database.
 * The main input is a {@link LoaiXetNghiemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LoaiXetNghiemDTO} or a {@link Page} of {@link LoaiXetNghiemDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LoaiXetNghiemQueryService extends QueryService<LoaiXetNghiem> {

    private final Logger log = LoggerFactory.getLogger(LoaiXetNghiemQueryService.class);

    private final LoaiXetNghiemRepository loaiXetNghiemRepository;

    private final LoaiXetNghiemMapper loaiXetNghiemMapper;

    public LoaiXetNghiemQueryService(LoaiXetNghiemRepository loaiXetNghiemRepository, LoaiXetNghiemMapper loaiXetNghiemMapper) {
        this.loaiXetNghiemRepository = loaiXetNghiemRepository;
        this.loaiXetNghiemMapper = loaiXetNghiemMapper;
    }

    /**
     * Return a {@link List} of {@link LoaiXetNghiemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LoaiXetNghiemDTO> findByCriteria(LoaiXetNghiemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LoaiXetNghiem> specification = createSpecification(criteria);
        return loaiXetNghiemMapper.toDto(loaiXetNghiemRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link LoaiXetNghiemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LoaiXetNghiemDTO> findByCriteria(LoaiXetNghiemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LoaiXetNghiem> specification = createSpecification(criteria);
        return loaiXetNghiemRepository.findAll(specification, page)
            .map(loaiXetNghiemMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LoaiXetNghiemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LoaiXetNghiem> specification = createSpecification(criteria);
        return loaiXetNghiemRepository.count(specification);
    }

    /**
     * Function to convert {@link LoaiXetNghiemCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LoaiXetNghiem> createSpecification(LoaiXetNghiemCriteria criteria) {
        Specification<LoaiXetNghiem> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LoaiXetNghiem_.id));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), LoaiXetNghiem_.ten));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), LoaiXetNghiem_.moTa));
            }
            if (criteria.getEnable() != null) {
                specification = specification.and(buildSpecification(criteria.getEnable(), LoaiXetNghiem_.enable));
            }
            if (criteria.getUuTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUuTien(), LoaiXetNghiem_.uuTien));
            }
            if (criteria.getMaPhanLoai() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaPhanLoai(), LoaiXetNghiem_.maPhanLoai));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(LoaiXetNghiem_.donVi, JoinType.LEFT).get(DonVi_.id)));
            }
        }
        return specification;
    }
}
