package vn.vnpt.service;

import vn.vnpt.service.dto.DotDieuTriDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import vn.vnpt.domain.DotDieuTriId;

/**
 * Service Interface for managing {@link vn.vnpt.domain.DotDieuTri}.
 */
public interface DotDieuTriService {

    /**
     * Save a dotDieuTri.
     *
     * @param dotDieuTriDTO the entity to save.
     * @return the persisted entity.
     */
    DotDieuTriDTO save(DotDieuTriDTO dotDieuTriDTO);

    /**
     * Get all the dotDieuTris.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DotDieuTriDTO> findAll(Pageable pageable);


    /**
     * Get the "id" dotDieuTri.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DotDieuTriDTO> findOne(DotDieuTriId id);

    /**
     * Delete the "id" dotDieuTri.
     *
     * @param id the id of the entity.
     */
    void delete(DotDieuTriId id);

    Optional<DotDieuTriDTO> makeDotDieuTriDefault(Boolean coBaoHiem, Map thongTin);
    DotDieuTriDTO skipThongTinBhyt(DotDieuTriDTO dotDieuTriDTO);

}
