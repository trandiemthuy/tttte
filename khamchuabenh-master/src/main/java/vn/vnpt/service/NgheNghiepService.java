package vn.vnpt.service;

import vn.vnpt.service.dto.NgheNghiepDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.NgheNghiep}.
 */
public interface NgheNghiepService {

    /**
     * Save a ngheNghiep.
     *
     * @param ngheNghiepDTO the entity to save.
     * @return the persisted entity.
     */
    NgheNghiepDTO save(NgheNghiepDTO ngheNghiepDTO);

    /**
     * Get all the ngheNghieps.
     *
     * @return the list of entities.
     */
    List<NgheNghiepDTO> findAll();

    /**
     * Get the "id" ngheNghiep.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<NgheNghiepDTO> findOne(Long id);

    /**
     * Delete the "id" ngheNghiep.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
