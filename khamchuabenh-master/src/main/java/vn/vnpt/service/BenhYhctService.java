package vn.vnpt.service;

import vn.vnpt.service.dto.BenhYhctDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.BenhYhct}.
 */
public interface BenhYhctService {

    /**
     * Save a benhYhct.
     *
     * @param benhYhctDTO the entity to save.
     * @return the persisted entity.
     */
    BenhYhctDTO save(BenhYhctDTO benhYhctDTO);

    /**
     * Get all the benhYhcts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BenhYhctDTO> findAll(Pageable pageable);

    /**
     * Get the "id" benhYhct.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BenhYhctDTO> findOne(Long id);

    /**
     * Delete the "id" benhYhct.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
