package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.PhieuChiDinhCDHA;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.PhieuChiDinhCDHARepository;
import vn.vnpt.service.dto.PhieuChiDinhCDHACriteria;
import vn.vnpt.service.dto.PhieuChiDinhCDHADTO;
import vn.vnpt.service.mapper.PhieuChiDinhCDHAMapper;

/**
 * Service for executing complex queries for {@link PhieuChiDinhCDHA} entities in the database.
 * The main input is a {@link PhieuChiDinhCDHACriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PhieuChiDinhCDHADTO} or a {@link Page} of {@link PhieuChiDinhCDHADTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PhieuChiDinhCDHAQueryService extends QueryService<PhieuChiDinhCDHA> {

    private final Logger log = LoggerFactory.getLogger(PhieuChiDinhCDHAQueryService.class);

    private final PhieuChiDinhCDHARepository phieuChiDinhCDHARepository;

    private final PhieuChiDinhCDHAMapper phieuChiDinhCDHAMapper;

    public PhieuChiDinhCDHAQueryService(PhieuChiDinhCDHARepository phieuChiDinhCDHARepository, PhieuChiDinhCDHAMapper phieuChiDinhCDHAMapper) {
        this.phieuChiDinhCDHARepository = phieuChiDinhCDHARepository;
        this.phieuChiDinhCDHAMapper = phieuChiDinhCDHAMapper;
    }

    /**
     * Return a {@link List} of {@link PhieuChiDinhCDHADTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PhieuChiDinhCDHADTO> findByCriteria(PhieuChiDinhCDHACriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PhieuChiDinhCDHA> specification = createSpecification(criteria);
        return phieuChiDinhCDHAMapper.toDto(phieuChiDinhCDHARepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PhieuChiDinhCDHADTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PhieuChiDinhCDHADTO> findByCriteria(PhieuChiDinhCDHACriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PhieuChiDinhCDHA> specification = createSpecification(criteria);
        return phieuChiDinhCDHARepository.findAll(specification, page)
            .map(phieuChiDinhCDHAMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PhieuChiDinhCDHACriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PhieuChiDinhCDHA> specification = createSpecification(criteria);
        return phieuChiDinhCDHARepository.count(specification);
    }

    /**
     * Function to convert {@link PhieuChiDinhCDHACriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PhieuChiDinhCDHA> createSpecification(PhieuChiDinhCDHACriteria criteria) {
        Specification<PhieuChiDinhCDHA> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PhieuChiDinhCDHA_.id));
            }
            if (criteria.getChanDoanTongQuat() != null) {
                specification = specification.and(buildStringSpecification(criteria.getChanDoanTongQuat(), PhieuChiDinhCDHA_.chanDoanTongQuat));
            }
            if (criteria.getGhiChu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGhiChu(), PhieuChiDinhCDHA_.ghiChu));
            }
            if (criteria.getKetQuaTongQuat() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKetQuaTongQuat(), PhieuChiDinhCDHA_.ketQuaTongQuat));
            }
            if (criteria.getNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNam(), PhieuChiDinhCDHA_.nam));
            }
            if (criteria.getBakbId() != null) {
                specification = specification.and(buildSpecification(criteria.getBakbId(), PhieuChiDinhCDHA_.bakbId));
            }
            if (criteria.getBenhNhanId() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhNhanId(), PhieuChiDinhCDHA_.benhNhanId));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(), PhieuChiDinhCDHA_.donViId));
            }
        }
        return specification;
    }
}
