package vn.vnpt.service;

import vn.vnpt.domain.ThongTinKhamBenhId;
import vn.vnpt.service.dto.ChiDinhDichVuKhamDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.vnpt.service.dto.customdto.TiepNhanDTO;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.ChiDinhDichVuKham}.
 */
public interface ChiDinhDichVuKhamService {

    /**
     * Save a chiDinhDichVuKham.
     *
     * @param chiDinhDichVuKhamDTO the entity to save.
     * @return the persisted entity.
     */
    ChiDinhDichVuKhamDTO save(ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO);

    /**
     * Get all the chiDinhDichVuKhams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ChiDinhDichVuKhamDTO> findAll(Pageable pageable);

    /**
     * Get the "id" chiDinhDichVuKham.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChiDinhDichVuKhamDTO> findOne(Long id);

    /**
     * Delete the "id" chiDinhDichVuKham.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    ChiDinhDichVuKhamDTO chiDinhDichVuKham(TiepNhanDTO tiepNhanDTO, Long dichVuKhamId);

    ChiDinhDichVuKhamDTO findOneByThongTinKhamBenhId(ThongTinKhamBenhId thongTinKhamBenhId);

    ChiDinhDichVuKhamDTO capNhatChiDinhDv(ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO, Long dichVuKhamId);
}
