package vn.vnpt.domain.custom.keys;

import java.io.Serializable;
import java.util.Objects;

public class BenhAnKhamBenhPrimaryKey implements Serializable {
    private Long id;
    private Long benhNhanId;
    private Long donViId;

    public BenhAnKhamBenhPrimaryKey() {
    }

    public BenhAnKhamBenhPrimaryKey(Long id, Long benhNhanId, Long donViId) {
        this.id = id;
        this.benhNhanId = benhNhanId;
        this.donViId = donViId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BenhAnKhamBenhPrimaryKey)) return false;
        BenhAnKhamBenhPrimaryKey that = (BenhAnKhamBenhPrimaryKey) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getBenhNhanId(), that.getBenhNhanId()) &&
            Objects.equals(getDonViId(), that.getDonViId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBenhNhanId(), getDonViId());
    }

    @Override
    public String toString() {
        return "BenhAnKhamBenhPrimaryKey{" +
            "id=" + id +
            ", benhNhanId=" + benhNhanId +
            ", donViId=" + donViId +
            '}';
    }
}
