package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A TPhongNhanVien.
 */
@Entity
@Table(name = "tphong_nv")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TPhongNhanVien implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("tPhongNhanViens")
    private Phong phong;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("tPhongNhanViens")
    private NhanVien nhanVien;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public TPhongNhanVien enabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Phong getPhong() {
        return phong;
    }

    public TPhongNhanVien phong(Phong phong) {
        this.phong = phong;
        return this;
    }

    public void setPhong(Phong phong) {
        this.phong = phong;
    }

    public NhanVien getNhanVien() {
        return nhanVien;
    }

    public TPhongNhanVien nhanVien(NhanVien nhanVien) {
        this.nhanVien = nhanVien;
        return this;
    }

    public void setNhanVien(NhanVien nhanVien) {
        this.nhanVien = nhanVien;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TPhongNhanVien)) {
            return false;
        }
        return id != null && id.equals(((TPhongNhanVien) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TPhongNhanVien{" +
            "id=" + getId() +
            ", enabled='" + isEnabled() + "'" +
            "}";
    }
}
