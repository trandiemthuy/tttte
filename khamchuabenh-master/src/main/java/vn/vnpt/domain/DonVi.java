package vn.vnpt.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.math.BigDecimal;

/**
 * A DonVi.
 */
@Entity
@Table(name = "don_vi")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DonVi implements Serializable {

   // private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Cấp đơn vị
     */
    @NotNull
    @Column(name = "cap", nullable = false)
    private Integer cap;

    /**
     * Thông tin chi nhánh ngân hàng
     */
    @Size(max = 300)
    @Column(name = "chi_nhanh_ngan_hang", length = 300)
    private String chiNhanhNganHang;

    /**
     * Địa chỉ của đơn vị
     */
    @Size(max = 1000)
    @Column(name = "dia_chi", length = 1000)
    private String diaChi;

    /**
     * Mã đơn vị quản lý
     */
    @Column(name = "don_vi_quan_ly_id", precision = 21, scale = 2)
    private BigDecimal donViQuanLyId;

    /**
     * Đơn vị trực thuộc
     */
    @Size(max = 10)
    @Column(name = "dvtt", length = 10)
    private String dvtt;

    /**
     * email
     */
    @Size(max = 50)
    @Column(name = "email", length = 50)
    private String email;

    /**
     * Trạng thái hiệu lực của dòng dữ liệu: 1: Có hiệu lực. 0: Không có hiệu lực
     */
    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    /**
     * Ký hiệu của đơn vị
     */
    @Size(max = 100)
    @Column(name = "ky_hieu", length = 100)
    private String kyHieu;

    /**
     * Loại của đơn vị
     */
    @NotNull
    @Column(name = "loai", nullable = false)
    private Integer loai;

    /**
     * Mã tỉnh
     */
    @NotNull
    @Size(max = 10)
    @Column(name = "ma_tinh", length = 10, nullable = false)
    private String maTinh;

    /**
     * Số điện thoại
     */
    @Size(max = 15)
    @Column(name = "phone", length = 15)
    private String phone;

    /**
     * Số tài khoản của đơn vị
     */
    @Size(max = 30)
    @Column(name = "so_tai_khoan", length = 30)
    private String soTaiKhoan;

    /**
     * Tên của đơn vị
     */
    @NotNull
    @Size(max = 300)
    @Column(name = "ten", length = 300, nullable = false)
    private String ten;

    /**
     * Tên thông tin ngân hàng
     */
    @Size(max = 300)
    @Column(name = "ten_ngan_hang", length = 300)
    private String tenNganHang;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public Long getId() {
        return id;
    }

    public DonVi id(Long id) {
        this.id = id;
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCap() {
        return cap;
    }

    public DonVi cap(Integer cap) {
        this.cap = cap;
        return this;
    }

    public void setCap(Integer cap) {
        this.cap = cap;
    }

    public String getChiNhanhNganHang() {
        return chiNhanhNganHang;
    }

    public DonVi chiNhanhNganHang(String chiNhanhNganHang) {
        this.chiNhanhNganHang = chiNhanhNganHang;
        return this;
    }

    public void setChiNhanhNganHang(String chiNhanhNganHang) {
        this.chiNhanhNganHang = chiNhanhNganHang;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public DonVi diaChi(String diaChi) {
        this.diaChi = diaChi;
        return this;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public BigDecimal getDonViQuanLyId() {
        return donViQuanLyId;
    }

    public DonVi donViQuanLyId(BigDecimal donViQuanLyId) {
        this.donViQuanLyId = donViQuanLyId;
        return this;
    }

    public void setDonViQuanLyId(BigDecimal donViQuanLyId) {
        this.donViQuanLyId = donViQuanLyId;
    }

    public String getDvtt() {
        return dvtt;
    }

    public DonVi dvtt(String dvtt) {
        this.dvtt = dvtt;
        return this;
    }

    public void setDvtt(String dvtt) {
        this.dvtt = dvtt;
    }

    public String getEmail() {
        return email;
    }

    public DonVi email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public DonVi enabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getKyHieu() {
        return kyHieu;
    }

    public DonVi kyHieu(String kyHieu) {
        this.kyHieu = kyHieu;
        return this;
    }

    public void setKyHieu(String kyHieu) {
        this.kyHieu = kyHieu;
    }

    public Integer getLoai() {
        return loai;
    }

    public DonVi loai(Integer loai) {
        this.loai = loai;
        return this;
    }

    public void setLoai(Integer loai) {
        this.loai = loai;
    }

    public String getMaTinh() {
        return maTinh;
    }

    public DonVi maTinh(String maTinh) {
        this.maTinh = maTinh;
        return this;
    }

    public void setMaTinh(String maTinh) {
        this.maTinh = maTinh;
    }

    public String getPhone() {
        return phone;
    }

    public DonVi phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSoTaiKhoan() {
        return soTaiKhoan;
    }

    public DonVi soTaiKhoan(String soTaiKhoan) {
        this.soTaiKhoan = soTaiKhoan;
        return this;
    }

    public void setSoTaiKhoan(String soTaiKhoan) {
        this.soTaiKhoan = soTaiKhoan;
    }

    public String getTen() {
        return ten;
    }

    public DonVi ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenNganHang() {
        return tenNganHang;
    }

    public DonVi tenNganHang(String tenNganHang) {
        this.tenNganHang = tenNganHang;
        return this;
    }

    public void setTenNganHang(String tenNganHang) {
        this.tenNganHang = tenNganHang;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DonVi)) {
            return false;
        }
        return id != null && id.equals(((DonVi) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DonVi{" +
            ", id=" + getId() +
            ", cap=" + getCap() +
            ", chiNhanhNganHang='" + getChiNhanhNganHang() + "'" +
            ", diaChi='" + getDiaChi() + "'" +
            ", donViQuanLyId=" + getDonViQuanLyId() +
            ", dvtt='" + getDvtt() + "'" +
            ", email='" + getEmail() + "'" +
            ", enabled='" + isEnabled() + "'" +
            ", kyHieu='" + getKyHieu() + "'" +
            ", loai=" + getLoai() +
            ", maTinh='" + getMaTinh() + "'" +
            ", phone='" + getPhone() + "'" +
            ", soTaiKhoan='" + getSoTaiKhoan() + "'" +
            ", ten='" + getTen() + "'" +
            ", tenNganHang='" + getTenNganHang() + "'" +
            "}";
    }
}
