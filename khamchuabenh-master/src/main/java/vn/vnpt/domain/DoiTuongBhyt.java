package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DoiTuongBhyt.
 */
@Entity
@Table(name = "doi_tuong_bhyt")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DoiTuongBhyt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Cận trên BHYT
     */
    @NotNull
    @Column(name = "can_tren", nullable = false)
    private Integer canTren;

    /**
     * Cận trên kỹ thuật cao
     */
    @NotNull
    @Column(name = "can_tren_ktc", nullable = false)
    private Integer canTrenKtc;

    /**
     * Mã đối tượng BHYT
     */
    @NotNull
    @Size(max = 10)
    @Column(name = "ma", length = 10, nullable = false)
    private String ma;

    /**
     * Tên đối tượng
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ten", length = 500, nullable = false)
    private String ten;

    /**
     * Tỷ lệ miễn giảm
     */
    @NotNull
    @Column(name = "ty_le_mien_giam", nullable = false)
    private Integer tyLeMienGiam;

    /**
     * Tỷ lệ miển giảm kỹ thuật cao
     */
    @NotNull
    @Column(name = "ty_le_mien_giam_ktc", nullable = false)
    private Integer tyLeMienGiamKtc;

    /**
     * Mã nhóm đối tượng BHYT
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("doiTuongBhyts")
    private NhomDoiTuongBhyt nhomDoiTuongBhyt;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCanTren() {
        return canTren;
    }

    public DoiTuongBhyt canTren(Integer canTren) {
        this.canTren = canTren;
        return this;
    }

    public void setCanTren(Integer canTren) {
        this.canTren = canTren;
    }

    public Integer getCanTrenKtc() {
        return canTrenKtc;
    }

    public DoiTuongBhyt canTrenKtc(Integer canTrenKtc) {
        this.canTrenKtc = canTrenKtc;
        return this;
    }

    public void setCanTrenKtc(Integer canTrenKtc) {
        this.canTrenKtc = canTrenKtc;
    }

    public String getMa() {
        return ma;
    }

    public DoiTuongBhyt ma(String ma) {
        this.ma = ma;
        return this;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public DoiTuongBhyt ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Integer getTyLeMienGiam() {
        return tyLeMienGiam;
    }

    public DoiTuongBhyt tyLeMienGiam(Integer tyLeMienGiam) {
        this.tyLeMienGiam = tyLeMienGiam;
        return this;
    }

    public void setTyLeMienGiam(Integer tyLeMienGiam) {
        this.tyLeMienGiam = tyLeMienGiam;
    }

    public Integer getTyLeMienGiamKtc() {
        return tyLeMienGiamKtc;
    }

    public DoiTuongBhyt tyLeMienGiamKtc(Integer tyLeMienGiamKtc) {
        this.tyLeMienGiamKtc = tyLeMienGiamKtc;
        return this;
    }

    public void setTyLeMienGiamKtc(Integer tyLeMienGiamKtc) {
        this.tyLeMienGiamKtc = tyLeMienGiamKtc;
    }

    public NhomDoiTuongBhyt getNhomDoiTuongBhyt() {
        return nhomDoiTuongBhyt;
    }

    public DoiTuongBhyt nhomDoiTuongBhyt(NhomDoiTuongBhyt nhomDoiTuongBhyt) {
        this.nhomDoiTuongBhyt = nhomDoiTuongBhyt;
        return this;
    }

    public void setNhomDoiTuongBhyt(NhomDoiTuongBhyt nhomDoiTuongBhyt) {
        this.nhomDoiTuongBhyt = nhomDoiTuongBhyt;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DoiTuongBhyt)) {
            return false;
        }
        return id != null && id.equals(((DoiTuongBhyt) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DoiTuongBhyt{" +
            "id=" + getId() +
            ", canTren=" + getCanTren() +
            ", canTrenKtc=" + getCanTrenKtc() +
            ", ma='" + getMa() + "'" +
            ", ten='" + getTen() + "'" +
            ", tyLeMienGiam=" + getTyLeMienGiam() +
            ", tyLeMienGiamKtc=" + getTyLeMienGiamKtc() +
            "}";
    }
}
