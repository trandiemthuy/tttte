package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.math.BigDecimal;

/**
 * A ChanDoanHinhAnh.
 */
@Entity
@Table(name = "chan_doan_hinh_anh")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ChanDoanHinhAnh implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Trạng thái xóa bỏ của dòng dữ liệu. \r\n1: Đã xóa. \r\n0: Còn hiệu lực.
     */
    @NotNull
    @Column(name = "deleted", nullable = false)
    private Boolean deleted;

    /**
     * Trạng thái dịch vụ theo yêu cầu: \r\n1: Dịch vụ theo yêu cầu. \r\n0: Dịch vụ bình thường.
     */
    @NotNull
    @Column(name = "dich_vu_yeu_cau", nullable = false)
    private Boolean dichVuYeuCau;

    /**
     * Trạng thái Doppler màu: 1: Doppler. 0. Chẩn đoán hình ảnh bình thường
     */
    @Column(name = "doppler")
    private Boolean doppler;

    /**
     * Đơn giá bệnh viện
     */
    @NotNull
    @Column(name = "don_gia_benh_vien", precision = 21, scale = 2, nullable = false)
    private BigDecimal donGiaBenhVien;

    /**
     * Trạng thái có hiệu lực của dịch vụ: \r\n1: Còn hiệu lực. \r\n0: Đã ẩn.
     */
    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    /**
     * Số lượng giới hạn của một chỉ định
     */
    @Column(name = "goi_han_chi_dinh", precision = 21, scale = 2)
    private BigDecimal goiHanChiDinh;

    /**
     * Trạng thái phạm vi của chỉ định:\r\n0: Chỉ có thu phí. \r\n1: Có cả BHYT và thu phí
     */
    @NotNull
    @Column(name = "pham_vi_chi_dinh", nullable = false)
    private Boolean phamViChiDinh;

    /**
     * Phân biệt theo giới tính: \r\n0: Nữ. \r\n1: Nam. \r\n2: Không phân biệt giới tính. \r\n
     */
    @NotNull
    @Column(name = "phan_theo_gioi_tinh", nullable = false)
    private Integer phanTheoGioiTinh;

    /**
     * Thứ tự sắp xếp khi in ra giấy báo kết quả
     */
    @NotNull
    @Column(name = "sap_xep", nullable = false)
    private Integer sapXep;

    /**
     * Số lần thực hiện
     */
    @NotNull
    @Column(name = "so_lan_thuc_hien", nullable = false)
    private Integer soLanThucHien;

    /**
     * Số lượng film của chỉ định
     */
    @NotNull
    @Column(name = "so_luong_film", precision = 21, scale = 2, nullable = false)
    private BigDecimal soLuongFilm;

    /**
     * Số lượng qui đổi
     */
    @NotNull
    @Column(name = "so_luong_qui_doi", precision = 21, scale = 2, nullable = false)
    private BigDecimal soLuongQuiDoi;

    /**
     * Tên của chỉ định
     */
    @Size(max = 500)
    @Column(name = "ten", length = 500)
    private String ten;

    /**
     * Tên hiển thị của chỉ định
     */
    @Size(max = 500)
    @Column(name = "ten_hien_thi", length = 500)
    private String tenHienThi;

    /**
     * Mã nội bộ
     */
    @Size(max = 255)
    @Column(name = "ma_noi_bo", length = 255)
    private String maNoiBo;

    /**
     * Mã dùng chung
     */
    @Size(max = 45)
    @Column(name = "ma_dung_chung", length = 45)
    private String maDungChung;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("cdhas")
    private DonVi donVi;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("cdhas")
    private DotThayDoiMaDichVu dotMa;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("cdhas")
    private LoaiChanDoanHinhAnh loaicdha;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public ChanDoanHinhAnh deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean isDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public ChanDoanHinhAnh dichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
        return this;
    }

    public void setDichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public Boolean isDoppler() {
        return doppler;
    }

    public ChanDoanHinhAnh doppler(Boolean doppler) {
        this.doppler = doppler;
        return this;
    }

    public void setDoppler(Boolean doppler) {
        this.doppler = doppler;
    }

    public BigDecimal getDonGiaBenhVien() {
        return donGiaBenhVien;
    }

    public ChanDoanHinhAnh donGiaBenhVien(BigDecimal donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
        return this;
    }

    public void setDonGiaBenhVien(BigDecimal donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public ChanDoanHinhAnh enabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public BigDecimal getGoiHanChiDinh() {
        return goiHanChiDinh;
    }

    public ChanDoanHinhAnh goiHanChiDinh(BigDecimal goiHanChiDinh) {
        this.goiHanChiDinh = goiHanChiDinh;
        return this;
    }

    public void setGoiHanChiDinh(BigDecimal goiHanChiDinh) {
        this.goiHanChiDinh = goiHanChiDinh;
    }

    public Boolean isPhamViChiDinh() {
        return phamViChiDinh;
    }

    public ChanDoanHinhAnh phamViChiDinh(Boolean phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
        return this;
    }

    public void setPhamViChiDinh(Boolean phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
    }

    public Integer getPhanTheoGioiTinh() {
        return phanTheoGioiTinh;
    }

    public ChanDoanHinhAnh phanTheoGioiTinh(Integer phanTheoGioiTinh) {
        this.phanTheoGioiTinh = phanTheoGioiTinh;
        return this;
    }

    public void setPhanTheoGioiTinh(Integer phanTheoGioiTinh) {
        this.phanTheoGioiTinh = phanTheoGioiTinh;
    }

    public Integer getSapXep() {
        return sapXep;
    }

    public ChanDoanHinhAnh sapXep(Integer sapXep) {
        this.sapXep = sapXep;
        return this;
    }

    public void setSapXep(Integer sapXep) {
        this.sapXep = sapXep;
    }

    public Integer getSoLanThucHien() {
        return soLanThucHien;
    }

    public ChanDoanHinhAnh soLanThucHien(Integer soLanThucHien) {
        this.soLanThucHien = soLanThucHien;
        return this;
    }

    public void setSoLanThucHien(Integer soLanThucHien) {
        this.soLanThucHien = soLanThucHien;
    }

    public BigDecimal getSoLuongFilm() {
        return soLuongFilm;
    }

    public ChanDoanHinhAnh soLuongFilm(BigDecimal soLuongFilm) {
        this.soLuongFilm = soLuongFilm;
        return this;
    }

    public void setSoLuongFilm(BigDecimal soLuongFilm) {
        this.soLuongFilm = soLuongFilm;
    }

    public BigDecimal getSoLuongQuiDoi() {
        return soLuongQuiDoi;
    }

    public ChanDoanHinhAnh soLuongQuiDoi(BigDecimal soLuongQuiDoi) {
        this.soLuongQuiDoi = soLuongQuiDoi;
        return this;
    }

    public void setSoLuongQuiDoi(BigDecimal soLuongQuiDoi) {
        this.soLuongQuiDoi = soLuongQuiDoi;
    }

    public String getTen() {
        return ten;
    }

    public ChanDoanHinhAnh ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenHienThi() {
        return tenHienThi;
    }

    public ChanDoanHinhAnh tenHienThi(String tenHienThi) {
        this.tenHienThi = tenHienThi;
        return this;
    }

    public void setTenHienThi(String tenHienThi) {
        this.tenHienThi = tenHienThi;
    }

    public String getMaNoiBo() {
        return maNoiBo;
    }

    public ChanDoanHinhAnh maNoiBo(String maNoiBo) {
        this.maNoiBo = maNoiBo;
        return this;
    }

    public void setMaNoiBo(String maNoiBo) {
        this.maNoiBo = maNoiBo;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public ChanDoanHinhAnh maDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
        return this;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public DonVi getDonVi() {
        return donVi;
    }

    public ChanDoanHinhAnh donVi(DonVi donVi) {
        this.donVi = donVi;
        return this;
    }

    public void setDonVi(DonVi donVi) {
        this.donVi = donVi;
    }

    public DotThayDoiMaDichVu getDotMa() {
        return dotMa;
    }

    public ChanDoanHinhAnh dotMa(DotThayDoiMaDichVu dotThayDoiMaDichVu) {
        this.dotMa = dotThayDoiMaDichVu;
        return this;
    }

    public void setDotMa(DotThayDoiMaDichVu dotThayDoiMaDichVu) {
        this.dotMa = dotThayDoiMaDichVu;
    }

    public LoaiChanDoanHinhAnh getLoaicdha() {
        return loaicdha;
    }

    public ChanDoanHinhAnh loaicdha(LoaiChanDoanHinhAnh loaiChanDoanHinhAnh) {
        this.loaicdha = loaiChanDoanHinhAnh;
        return this;
    }

    public void setLoaicdha(LoaiChanDoanHinhAnh loaiChanDoanHinhAnh) {
        this.loaicdha = loaiChanDoanHinhAnh;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChanDoanHinhAnh)) {
            return false;
        }
        return id != null && id.equals(((ChanDoanHinhAnh) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ChanDoanHinhAnh{" +
            "id=" + getId() +
            ", deleted='" + isDeleted() + "'" +
            ", dichVuYeuCau='" + isDichVuYeuCau() + "'" +
            ", doppler='" + isDoppler() + "'" +
            ", donGiaBenhVien=" + getDonGiaBenhVien() +
            ", enabled='" + isEnabled() + "'" +
            ", goiHanChiDinh=" + getGoiHanChiDinh() +
            ", phamViChiDinh='" + isPhamViChiDinh() + "'" +
            ", phanTheoGioiTinh=" + getPhanTheoGioiTinh() +
            ", sapXep=" + getSapXep() +
            ", soLanThucHien=" + getSoLanThucHien() +
            ", soLuongFilm=" + getSoLuongFilm() +
            ", soLuongQuiDoi=" + getSoLuongQuiDoi() +
            ", ten='" + getTen() + "'" +
            ", tenHienThi='" + getTenHienThi() + "'" +
            ", maNoiBo='" + getMaNoiBo() + "'" +
            ", maDungChung='" + getMaDungChung() + "'" +
            "}";
    }
}
