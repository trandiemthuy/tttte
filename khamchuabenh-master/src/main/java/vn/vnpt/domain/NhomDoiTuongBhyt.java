package vn.vnpt.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A NhomDoiTuongBhyt.
 */
@Entity
@Table(name = "nhom_doi_tuong_bhyt")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NhomDoiTuongBhyt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Tên nhóm đối tượng BHYT
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ten", length = 500, nullable = false)
    private String ten;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public NhomDoiTuongBhyt ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NhomDoiTuongBhyt)) {
            return false;
        }
        return id != null && id.equals(((NhomDoiTuongBhyt) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "NhomDoiTuongBhyt{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            "}";
    }
}
