package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A TPhanNhomXetNghiem.
 */
@Entity
@Table(name = "tpn_xn")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TPhanNhomXetNghiem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("t_pn_xns")
    private NhomXetNghiem nhomXN;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("t_pn_xns")
    private XetNghiem xN;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NhomXetNghiem getNhomXN() {
        return nhomXN;
    }

    public TPhanNhomXetNghiem nhomXN(NhomXetNghiem nhomXetNghiem) {
        this.nhomXN = nhomXetNghiem;
        return this;
    }

    public void setNhomXN(NhomXetNghiem nhomXetNghiem) {
        this.nhomXN = nhomXetNghiem;
    }

    public XetNghiem getXN() {
        return xN;
    }

    public TPhanNhomXetNghiem xN(XetNghiem xetNghiem) {
        this.xN = xetNghiem;
        return this;
    }

    public void setXN(XetNghiem xetNghiem) {
        this.xN = xetNghiem;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TPhanNhomXetNghiem)) {
            return false;
        }
        return id != null && id.equals(((TPhanNhomXetNghiem) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TPhanNhomXetNghiem{" +
            "id=" + getId() +
            "}";
    }
}
