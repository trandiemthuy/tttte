package vn.vnpt.domain;


import java.util.ArrayList;

public class KeyCloakUser {

    private String username = null;
    private String firstname = null;
    private String lastname = null;
    private String email = null;

    private String password = null;

    private Long donViId = null;

    private ArrayList<String> role = new ArrayList<>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<String> getRole() {
        return role;
    }

    public void setRole(ArrayList<String> role) {
        this.role = role;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public String toString() {
        return "UserKeycloak{" +
            "username ='" + getUsername() + "'" +
            ", firstname ='" + getFirstname() + "'" +
            ", lastname ='" + getLastname() + "'" +
            ", email ='" + getEmail() + "'" +
            ", role = " + getRole() +
            ", donViId = " + getDonViId() +
            "}";
    }
}
