package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * A ChiDinhDichVuKham.
 */
@Entity
@Table(name = "chi_dinh_dvk")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ChiDinhDichVuKham implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Trạng thái công khám ban đầu: 1: Công khám ban đầu. 0: Công khám bình thường
     */
    @NotNull
    @Column(name = "cong_kham_ban_dau", nullable = false)
    private Boolean congKhamBanDau;

    /**
     * Trạng thái đã thanh toán: 0: Chưa thanh toán. 1 Đã thanh toán
     */
    @NotNull
    @Column(name = "da_thanh_toan", nullable = false)
    private Boolean daThanhToan;

    /**
     * Đơn giá
     */
    @NotNull
    @Column(name = "don_gia", precision = 21, scale = 2, nullable = false)
    private BigDecimal donGia;

    /**
     * Tên dịch vụ khám
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ten", length = 500, nullable = false)
    private String ten;

    /**
     * Thời gian chỉ định dịch vụ
     */
    @NotNull
    @Column(name = "thoi_gian_chi_dinh", nullable = false)
    private LocalDate thoiGianChiDinh;

    /**
     * Tỷ lệ thanh toán
     */
    @Column(name = "ty_le_thanh_toan", precision = 21, scale = 2)
    private BigDecimal tyLeThanhToan;

    /**
     * Trạng thái có bảo hiểm: 1: Có bảo hiểm. 0: Không có BHYT
     */
    @NotNull
    @Column(name = "co_bao_hiem", nullable = false)
    private Boolean coBaoHiem;

    /**
     * Giá BHYT
     */
    @NotNull
    @Column(name = "gia_bhyt", precision = 21, scale = 2, nullable = false)
    private BigDecimal giaBhyt;

    /**
     * Giá Không có BHYT
     */
    @NotNull
    @Column(name = "gia_khong_bhyt", precision = 21, scale = 2, nullable = false)
    private BigDecimal giaKhongBhyt;

    /**
     * Mã dùng chung cho chỉ định
     */
    @Size(max = 500)
    @Column(name = "ma_dung_chung", length = 500)
    private String maDungChung;

    /**
     * Trạng thái theo yêu cầu của dịch vụ: 0: Bình thường. 1: Theo yêu cầu
     */
    @NotNull
    @Column(name = "dich_vu_yeu_cau", nullable = false)
    private Boolean dichVuYeuCau;

    @NotNull
    @Column(name = "nam", nullable = false)
    private Integer nam;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("chiDinhDichVuKhams")
    @JoinColumns({
        @JoinColumn(name = "ttkb_id", referencedColumnName = "id"),
        @JoinColumn(name = "thong_tin_khoa_id", referencedColumnName = "thong_tin_khoa_id"),
        @JoinColumn(name = "dot_dieu_tri_id", referencedColumnName = "dot_dieu_tri_id"),
        @JoinColumn(name = "bakb_id", referencedColumnName = "bakb_id"),
        @JoinColumn(name = "benh_nhan_id", referencedColumnName = "benh_nhan_id"),
        @JoinColumn(name = "don_vi_id", referencedColumnName = "don_vi_id")

    })
    private ThongTinKhamBenh ttkb;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("chiDinhDichVuKhams")
    private DichVuKham dichVuKham;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isCongKhamBanDau() {
        return congKhamBanDau;
    }

    public ChiDinhDichVuKham congKhamBanDau(Boolean congKhamBanDau) {
        this.congKhamBanDau = congKhamBanDau;
        return this;
    }

    public void setCongKhamBanDau(Boolean congKhamBanDau) {
        this.congKhamBanDau = congKhamBanDau;
    }

    public Boolean isDaThanhToan() {
        return daThanhToan;
    }

    public ChiDinhDichVuKham daThanhToan(Boolean daThanhToan) {
        this.daThanhToan = daThanhToan;
        return this;
    }

    public void setDaThanhToan(Boolean daThanhToan) {
        this.daThanhToan = daThanhToan;
    }

    public BigDecimal getDonGia() {
        return donGia;
    }

    public ChiDinhDichVuKham donGia(BigDecimal donGia) {
        this.donGia = donGia;
        return this;
    }

    public void setDonGia(BigDecimal donGia) {
        this.donGia = donGia;
    }

    public String getTen() {
        return ten;
    }

    public ChiDinhDichVuKham ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public LocalDate getThoiGianChiDinh() {
        return thoiGianChiDinh;
    }

    public ChiDinhDichVuKham thoiGianChiDinh(LocalDate thoiGianChiDinh) {
        this.thoiGianChiDinh = thoiGianChiDinh;
        return this;
    }

    public void setThoiGianChiDinh(LocalDate thoiGianChiDinh) {
        this.thoiGianChiDinh = thoiGianChiDinh;
    }

    public BigDecimal getTyLeThanhToan() {
        return tyLeThanhToan;
    }

    public ChiDinhDichVuKham tyLeThanhToan(BigDecimal tyLeThanhToan) {
        this.tyLeThanhToan = tyLeThanhToan;
        return this;
    }

    public void setTyLeThanhToan(BigDecimal tyLeThanhToan) {
        this.tyLeThanhToan = tyLeThanhToan;
    }

    public Boolean isCoBaoHiem() {
        return coBaoHiem;
    }

    public ChiDinhDichVuKham coBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
        return this;
    }

    public void setCoBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public BigDecimal getGiaBhyt() {
        return giaBhyt;
    }

    public ChiDinhDichVuKham giaBhyt(BigDecimal giaBhyt) {
        this.giaBhyt = giaBhyt;
        return this;
    }

    public void setGiaBhyt(BigDecimal giaBhyt) {
        this.giaBhyt = giaBhyt;
    }

    public BigDecimal getGiaKhongBhyt() {
        return giaKhongBhyt;
    }

    public ChiDinhDichVuKham giaKhongBhyt(BigDecimal giaKhongBhyt) {
        this.giaKhongBhyt = giaKhongBhyt;
        return this;
    }

    public void setGiaKhongBhyt(BigDecimal giaKhongBhyt) {
        this.giaKhongBhyt = giaKhongBhyt;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public ChiDinhDichVuKham maDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
        return this;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public Boolean isDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public ChiDinhDichVuKham dichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
        return this;
    }

    public void setDichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public Integer getNam() {
        return nam;
    }

    public ChiDinhDichVuKham nam(Integer nam) {
        this.nam = nam;
        return this;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public ThongTinKhamBenh getTtkb() {
        return ttkb;
    }

    public ChiDinhDichVuKham ttkb(ThongTinKhamBenh thongTinKhamBenh) {
        this.ttkb = thongTinKhamBenh;
        return this;
    }

    public void setTtkb(ThongTinKhamBenh thongTinKhamBenh) {
        this.ttkb = thongTinKhamBenh;
    }

    public DichVuKham getDichVuKham() {
        return dichVuKham;
    }

    public ChiDinhDichVuKham dichVuKham(DichVuKham dichVuKham) {
        this.dichVuKham = dichVuKham;
        return this;
    }

    public void setDichVuKham(DichVuKham dichVuKham) {
        this.dichVuKham = dichVuKham;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChiDinhDichVuKham)) {
            return false;
        }
        return id != null && id.equals(((ChiDinhDichVuKham) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ChiDinhDichVuKham{" +
            "id=" + getId() +
            ", congKhamBanDau='" + isCongKhamBanDau() + "'" +
            ", daThanhToan='" + isDaThanhToan() + "'" +
            ", donGia=" + getDonGia() +
            ", ten='" + getTen() + "'" +
            ", thoiGianChiDinh='" + getThoiGianChiDinh() + "'" +
            ", tyLeThanhToan=" + getTyLeThanhToan() +
            ", coBaoHiem='" + isCoBaoHiem() + "'" +
            ", giaBhyt=" + getGiaBhyt() +
            ", giaKhongBhyt=" + getGiaKhongBhyt() +
            ", maDungChung='" + getMaDungChung() + "'" +
            ", dichVuYeuCau='" + isDichVuYeuCau() + "'" +
            ", nam=" + getNam() +
            "}";
    }
}
