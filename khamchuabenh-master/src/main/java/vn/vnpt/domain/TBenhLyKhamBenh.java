package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A TBenhLyKhamBenh.
 */
@Entity
@Table(name = "t_benh_ly_kham_benh")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TBenhLyKhamBenh implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Loại bệnh:\n1: Bệnh Chính.\n0: Bệnh phụ.
     */
    @NotNull
    @Column(name = "loai_benh", nullable = false)
    private Boolean loaiBenh;

//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties(value = "tBenhLyKhamBenhs", allowSetters = true)
//    private ThongTinKhamBenh bakb;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties(value = "tBenhLyKhamBenhs", allowSetters = true)
//    private ThongTinKhamBenh donVi;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties(value = "tBenhLyKhamBenhs", allowSetters = true)
//    private ThongTinKhamBenh benhNhan;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties(value = "tBenhLyKhamBenhs", allowSetters = true)
//    private ThongTinKhamBenh dotDieuTri;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties(value = "tBenhLyKhamBenhs", allowSetters = true)
//    private ThongTinKhamBenh thongTinKhoa;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "tBenhLyKhamBenhs", allowSetters = true)
    @JoinColumns({
        @JoinColumn(name = "thong_tin_kham_benh_id",referencedColumnName = "id",insertable = false, updatable = false),
        @JoinColumn(name = "thong_tin_khoa_id",referencedColumnName = "thong_tin_khoa_id",insertable = false, updatable = false),
        @JoinColumn(name = "dot_dieu_tri_id",referencedColumnName = "dot_dieu_tri_id",insertable = false, updatable = false),
        @JoinColumn(name = "bakb_id",referencedColumnName = "bakb_id",insertable = false, updatable = false),
        @JoinColumn(name = "benh_nhan_id",referencedColumnName = "benh_nhan_id",insertable = false, updatable = false),
        @JoinColumn(name = "don_vi_id",referencedColumnName = "don_vi_id",insertable = false, updatable = false)
    })
    private ThongTinKhamBenh thongTinKhamBenh;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "tBenhLyKhamBenhs", allowSetters = true)
    private BenhLy benhLy;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isLoaiBenh() {
        return loaiBenh;
    }

    public TBenhLyKhamBenh loaiBenh(Boolean loaiBenh) {
        this.loaiBenh = loaiBenh;
        return this;
    }

    public void setLoaiBenh(Boolean loaiBenh) {
        this.loaiBenh = loaiBenh;
    }

//    public ThongTinKhamBenh getBakb() {
//        return bakb;
//    }
//
//    public TBenhLyKhamBenh bakb(ThongTinKhamBenh thongTinKhamBenh) {
//        this.bakb = thongTinKhamBenh;
//        return this;
//    }
//
//    public void setBakb(ThongTinKhamBenh thongTinKhamBenh) {
//        this.bakb = thongTinKhamBenh;
//    }
//
//    public ThongTinKhamBenh getDonVi() {
//        return donVi;
//    }
//
//    public TBenhLyKhamBenh donVi(ThongTinKhamBenh thongTinKhamBenh) {
//        this.donVi = thongTinKhamBenh;
//        return this;
//    }
//
//    public void setDonVi(ThongTinKhamBenh thongTinKhamBenh) {
//        this.donVi = thongTinKhamBenh;
//    }
//
//    public ThongTinKhamBenh getBenhNhan() {
//        return benhNhan;
//    }
//
//    public TBenhLyKhamBenh benhNhan(ThongTinKhamBenh thongTinKhamBenh) {
//        this.benhNhan = thongTinKhamBenh;
//        return this;
//    }
//
//    public void setBenhNhan(ThongTinKhamBenh thongTinKhamBenh) {
//        this.benhNhan = thongTinKhamBenh;
//    }
//
//    public ThongTinKhamBenh getDotDieuTri() {
//        return dotDieuTri;
//    }
//
//    public TBenhLyKhamBenh dotDieuTri(ThongTinKhamBenh thongTinKhamBenh) {
//        this.dotDieuTri = thongTinKhamBenh;
//        return this;
//    }
//
//    public void setDotDieuTri(ThongTinKhamBenh thongTinKhamBenh) {
//        this.dotDieuTri = thongTinKhamBenh;
//    }
//
//    public ThongTinKhamBenh getThongTinKhoa() {
//        return thongTinKhoa;
//    }
//
//    public TBenhLyKhamBenh thongTinKhoa(ThongTinKhamBenh thongTinKhamBenh) {
//        this.thongTinKhoa = thongTinKhamBenh;
//        return this;
//    }
//
//    public void setThongTinKhoa(ThongTinKhamBenh thongTinKhamBenh) {
//        this.thongTinKhoa = thongTinKhamBenh;
//    }

    public ThongTinKhamBenh getThongTinKhamBenh() {
        return thongTinKhamBenh;
    }

    public TBenhLyKhamBenh thongTinKhamBenh(ThongTinKhamBenh thongTinKhamBenh) {
        this.thongTinKhamBenh = thongTinKhamBenh;
        return this;
    }

    public void setThongTinKhamBenh(ThongTinKhamBenh thongTinKhamBenh) {
        this.thongTinKhamBenh = thongTinKhamBenh;
    }

    public BenhLy getBenhLy() {
        return benhLy;
    }

    public TBenhLyKhamBenh benhLy(BenhLy benhLy) {
        this.benhLy = benhLy;
        return this;
    }

    public void setBenhLy(BenhLy benhLy) {
        this.benhLy = benhLy;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TBenhLyKhamBenh)) {
            return false;
        }
        return id != null && id.equals(((TBenhLyKhamBenh) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TBenhLyKhamBenh{" +
            "id=" + getId() +
            ", loaiBenh='" + isLoaiBenh() + "'" +
            "}";
    }
}
