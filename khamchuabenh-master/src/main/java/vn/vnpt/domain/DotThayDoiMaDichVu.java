package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * Thông tinc ông văn quy định mã của các loại dịch vụ mã bệnh viện sử dụng
 */
@Entity
@Table(name = "dot_thay_doi_ma_dich_vu")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DotThayDoiMaDichVu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * 0: áp dụng ngay lập tức, 1: Áp dụng cho bệnh nhân mới vào viện
     */
    @Column(name = "dot_tuong_ap_dung")
    private Integer dotTuongApDung;

    /**
     * Ghi chú
     */
    @Size(max = 500)
    @Column(name = "ghi_chu", length = 500)
    private String ghiChu;

    /**
     * Ngày áp dụng của công văn danh mục
     */
    @NotNull
    @Column(name = "ngay_ap_dung", nullable = false)
    private LocalDate ngayApDung;

    /**
     * Tên công văn
     */
    @NotNull
    @Column(name = "ten", nullable = false)
    private String ten;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("dotGias")
    private DonVi donVi;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDotTuongApDung() {
        return dotTuongApDung;
    }

    public void setDotTuongApDung(Integer dotTuongApDung) {
        this.dotTuongApDung = dotTuongApDung;
    }

    public DotThayDoiMaDichVu dotTuongApDung(Integer dotTuongApDung) {
        this.dotTuongApDung = dotTuongApDung;
        return this;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public DotThayDoiMaDichVu ghiChu(String ghiChu) {
        this.ghiChu = ghiChu;
        return this;
    }

    public LocalDate getNgayApDung() {
        return ngayApDung;
    }

    public void setNgayApDung(LocalDate ngayApDung) {
        this.ngayApDung = ngayApDung;
    }

    public DotThayDoiMaDichVu ngayApDung(LocalDate ngayApDung) {
        this.ngayApDung = ngayApDung;
        return this;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public DotThayDoiMaDichVu ten(String ten) {
        this.ten = ten;
        return this;
    }

    public DonVi getDonVi() {
        return donVi;
    }

    public void setDonVi(DonVi donVi) {
        this.donVi = donVi;
    }

    public DotThayDoiMaDichVu donVi(DonVi donVi) {
        this.donVi = donVi;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DotThayDoiMaDichVu)) {
            return false;
        }
        return id != null && id.equals(((DotThayDoiMaDichVu) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DotThayDoiMaDichVu{" +
            "id=" + getId() +
            ", dotTuongApDung=" + getDotTuongApDung() +
            ", ghiChu='" + getGhiChu() + "'" +
            ", ngayApDung='" + getNgayApDung() + "'" +
            ", ten='" + getTen() + "'" +
            "}";
    }
}
