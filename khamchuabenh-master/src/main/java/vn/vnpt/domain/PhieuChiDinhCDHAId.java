package vn.vnpt.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PhieuChiDinhCDHAId implements Serializable {
    private Long id;
    private Long benhNhanId;
    private Long donViId;
    private Long bakbId;

    public PhieuChiDinhCDHAId() {
    }
    public PhieuChiDinhCDHAId(Long id, Long benhNhanId, Long donViId, Long bakbId){
        this.id = id;
        this.benhNhanId = benhNhanId;
        this.donViId = donViId;
        this.bakbId=bakbId;
    }
    public PhieuChiDinhCDHAId(Long benhNhanId, Long donViId, Long bakbId){
        this.benhNhanId = benhNhanId;
        this.donViId = donViId;
        this.bakbId=bakbId;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PhieuChiDinhXetNghiemId)) return false;
        PhieuChiDinhXetNghiemId that = (PhieuChiDinhXetNghiemId) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getBenhNhanId(), that.getBenhNhanId()) &&
            Objects.equals(getDonViId(), that.getDonViId()) &&
            Objects.equals(getBakbId(), that.getBakbId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBenhNhanId(), getDonViId(), getBakbId());
    }

    @Override
    public String toString() {
        return "PhieuChiDinhXetNghiemId{" +
            "id=" + id +
            ", benhNhanId=" + benhNhanId +
            ", donViId=" + donViId +
            ", bakbId=" + bakbId +
            '}';
    }
}

