package vn.vnpt.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * NDB_TABLE=READ_BACKUP=1 Thông tin chức danh của user
 */
@Entity
@Table(name = "chuc_danh")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ChucDanh implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Mô tả chức danh
     */
    @Size(max = 500)
    @Column(name = "mo_ta", length = 500)
    private String moTa;

    /**
     * Tên chức danh
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ten", length = 500, nullable = false)
    private String ten;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMoTa() {
        return moTa;
    }

    public ChucDanh moTa(String moTa) {
        this.moTa = moTa;
        return this;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getTen() {
        return ten;
    }

    public ChucDanh ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChucDanh)) {
            return false;
        }
        return id != null && id.equals(((ChucDanh) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ChucDanh{" +
            "id=" + getId() +
            ", moTa='" + getMoTa() + "'" +
            ", ten='" + getTen() + "'" +
            "}";
    }
}
