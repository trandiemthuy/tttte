package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * NDB_TABLE=READ_BACKUP=1 Bảng chức thông tin bệnh lý
 */

@Document(indexName = "benhly")
@Entity
@Table(name = "benh_ly")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BenhLySearch implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Trạng thái của 1 dòng thông tin bệnh lý\n\n1: Có hiệu lực.\n\n0: Không có hiệu lực.
     */
    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    /**
     * Mã icd
     */
    @NotNull
    @Size(max = 200)
    @Column(name = "icd", length = 200, nullable = false)
    private String icd;

    /**
     * Mô tả icd bằng tiếng Việt
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "mo_ta", length = 500, nullable = false)
    private String moTa;

    /**
     * Mô tả icd bằng tiếng Anh
     */
    @Size(max = 500)
    @Column(name = "mo_ta_tieng_anh", length = 500)
    private String moTaTiengAnh;

    /**
     * Tên không dấu của icd
     */
    @Size(max = 500)
    @Column(name = "ten_khong_dau", length = 500)
    private String tenKhongDau;

    /**
     * Mã viết tắt của icd
     */
    @Size(max = 100)
    @Column(name = "viet_tat", length = 100)
    private String vietTat;

    /**
     * Mã bệnh theo VNCode
     */
    @NotNull
    @Column(name = "vncode", nullable = false)
    private String vncode;

    /**
     * Mã nhóm bệnh lý
     */
    @NotNull
    @Column(name = "nhom_benh_ly_id", nullable = false)
    private Long nhomBenhLyId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public BenhLySearch enabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getIcd() {
        return icd;
    }

    public BenhLySearch icd(String icd) {
        this.icd = icd;
        return this;
    }

    public void setIcd(String icd) {
        this.icd = icd;
    }

    public String getMoTa() {
        return moTa;
    }

    public BenhLySearch moTa(String moTa) {
        this.moTa = moTa;
        return this;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getMoTaTiengAnh() {
        return moTaTiengAnh;
    }

    public BenhLySearch moTaTiengAnh(String moTaTiengAnh) {
        this.moTaTiengAnh = moTaTiengAnh;
        return this;
    }

    public void setMoTaTiengAnh(String moTaTiengAnh) {
        this.moTaTiengAnh = moTaTiengAnh;
    }

    public String getTenKhongDau() {
        return tenKhongDau;
    }

    public BenhLySearch tenKhongDau(String tenKhongDau) {
        this.tenKhongDau = tenKhongDau;
        return this;
    }

    public void setTenKhongDau(String tenKhongDau) {
        this.tenKhongDau = tenKhongDau;
    }

    public String getVietTat() {
        return vietTat;
    }

    public BenhLySearch vietTat(String vietTat) {
        this.vietTat = vietTat;
        return this;
    }

    public void setVietTat(String vietTat) {
        this.vietTat = vietTat;
    }

    public String getVncode() {
        return vncode;
    }

    public BenhLySearch vncode(String vncode) {
        this.vncode = vncode;
        return this;
    }

    public void setVncode(String vncode) {
        this.vncode = vncode;
    }

    public void setNhomBenhLyId(Long nhomBenhLyId) {
        this.nhomBenhLyId = nhomBenhLyId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BenhLySearch)) {
            return false;
        }
        return id != null && id.equals(((BenhLySearch) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BenhLy{" +
            "id=" + getId() +
            ", enabled='" + isEnabled() + "'" +
            ", icd='" + getIcd() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", moTaTiengAnh='" + getMoTaTiengAnh() + "'" +
            ", tenKhongDau='" + getTenKhongDau() + "'" +
            ", vietTat='" + getVietTat() + "'" +
            ", vncode='" + getVncode() + "'" +
            "}";
    }
}
