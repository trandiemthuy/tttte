import React from 'react';
import { Switch } from 'react-router-dom';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import DonVi from './khamchuabenh/don-vi';
import ThongTinBhxh from './khamchuabenh/thong-tin-bhxh';
import BenhNhan from './khamchuabenh/benh-nhan';
import DanToc from './khamchuabenh/dan-toc';
import NgheNghiep from './khamchuabenh/nghe-nghiep';
import Country from './khamchuabenh/country';
import TinhThanhPho from './khamchuabenh/tinh-thanh-pho';
import QuanHuyen from './khamchuabenh/quan-huyen';
import PhuongXa from './khamchuabenh/phuong-xa';
import UserExtra from './khamchuabenh/user-extra';
import Menu from './khamchuabenh/menu';
import UserType from './khamchuabenh/user-type';
import TheBhyt from './khamchuabenh/the-bhyt';
import DoiTuongBhyt from './khamchuabenh/doi-tuong-bhyt';
import NhomDoiTuongBhyt from './khamchuabenh/nhom-doi-tuong-bhyt';
import Khoa from './khamchuabenh/khoa';
import NhanVien from './khamchuabenh/nhan-vien';
import ChucDanh from './khamchuabenh/chuc-danh';
import ChucVu from './khamchuabenh/chuc-vu';
import DichVuKham from './khamchuabenh/dich-vu-kham';
import DotThayDoiMaDichVu from './khamchuabenh/dot-thay-doi-ma-dich-vu';
import Phong from './khamchuabenh/phong';
import BenhAnKhamBenh from './khamchuabenh/benh-an-kham-benh';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}don-vi`} component={DonVi}/>
      <ErrorBoundaryRoute path={`${match.url}thong-tin-bhxh`} component={ThongTinBhxh} />
      <ErrorBoundaryRoute path={`${match.url}benh-nhan`} component={BenhNhan} />
      <ErrorBoundaryRoute path={`${match.url}dan-toc`} component={DanToc} />
      <ErrorBoundaryRoute path={`${match.url}nghe-nghiep`} component={NgheNghiep} />
      <ErrorBoundaryRoute path={`${match.url}country`} component={Country} />
      <ErrorBoundaryRoute path={`${match.url}tinh-thanh-pho`} component={TinhThanhPho} />
      <ErrorBoundaryRoute path={`${match.url}quan-huyen`} component={QuanHuyen} />
      <ErrorBoundaryRoute path={`${match.url}phuong-xa`} component={PhuongXa} />
      <ErrorBoundaryRoute path={`${match.url}user-extra`} component={UserExtra} />
      <ErrorBoundaryRoute path={`${match.url}menu`} component={Menu} />
      <ErrorBoundaryRoute path={`${match.url}user-type`} component={UserType} />
      <ErrorBoundaryRoute path={`${match.url}the-bhyt`} component={TheBhyt} />
      <ErrorBoundaryRoute path={`${match.url}doi-tuong-bhyt`} component={DoiTuongBhyt} />
      <ErrorBoundaryRoute path={`${match.url}nhom-doi-tuong-bhyt`} component={NhomDoiTuongBhyt} />
      <ErrorBoundaryRoute path={`${match.url}khoa`} component={Khoa} />
      <ErrorBoundaryRoute path={`${match.url}nhan-vien`} component={NhanVien} />
      <ErrorBoundaryRoute path={`${match.url}chuc-danh`} component={ChucDanh} />
      <ErrorBoundaryRoute path={`${match.url}chuc-vu`} component={ChucVu} />
      <ErrorBoundaryRoute path={`${match.url}dich-vu-kham`} component={DichVuKham} />
      <ErrorBoundaryRoute path={`${match.url}dot-thay-doi-ma-dich-vu`} component={DotThayDoiMaDichVu} />
      <ErrorBoundaryRoute path={`${match.url}phong`} component={Phong} />
      <ErrorBoundaryRoute path={`${match.url}benh-an-kham-benh`} component={BenhAnKhamBenh} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
