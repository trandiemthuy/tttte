import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Row, Table} from 'reactstrap';
import {getSortState, JhiItemCount, JhiPagination, Translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {IRootState} from 'app/shared/reducers';
import {getEntities} from './don-vi.reducer';
import {ITEMS_PER_PAGE} from 'app/shared/util/pagination.constants';

export interface IDonViProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {
}

export const DonVi = (props: IDonViProps) => {
  const [paginationState, setPaginationState] = useState(getSortState(props.location, ITEMS_PER_PAGE));

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  useEffect(() => {
    getAllEntities();
  }, []);

  const sortEntities = () => {
    getAllEntities();
    props.history.push(
      `${props.location.pathname}?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`
    );
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage
    });

  const {donViList, match, totalItems} = props;
  return (
    <div>
      <h2 id="don-vi-heading">
        <Translate contentKey="gatewayApp.khamchuabenhDonVi.home.title">Don Vis</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus"/>
          &nbsp;
          <Translate contentKey="gatewayApp.khamchuabenhDonVi.home.createLabel">Create new Don Vi</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {donViList && donViList.length > 0 ? (
          <Table responsive>
            <thead>
            <tr>
              <th className="hand" onClick={sort('id')}>
                <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort"/>
              </th>
              <th className="hand" onClick={sort('cap')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.cap">Cap</Translate> <FontAwesomeIcon icon="sort"/>
              </th>
              <th className="hand" onClick={sort('chiNhanhNganHang')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.chiNhanhNganHang">Chi Nhanh Ngan
                  Hang</Translate>{' '}
                <FontAwesomeIcon icon="sort"/>
              </th>
              <th className="hand" onClick={sort('diaChi')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.diaChi">Dia Chi</Translate> <FontAwesomeIcon
                icon="sort"/>
              </th>
              <th className="hand" onClick={sort('donViQuanLyId')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.donViQuanLyId">Don Vi Quan Ly Id</Translate>{' '}
                <FontAwesomeIcon icon="sort"/>
              </th>
              <th className="hand" onClick={sort('dvtt')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.dvtt">Dvtt</Translate> <FontAwesomeIcon
                icon="sort"/>
              </th>
              <th className="hand" onClick={sort('email')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.email">Email</Translate> <FontAwesomeIcon
                icon="sort"/>
              </th>
              <th className="hand" onClick={sort('enabled')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.enabled">Enabled</Translate> <FontAwesomeIcon
                icon="sort"/>
              </th>
              <th className="hand" onClick={sort('kyHieu')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.kyHieu">Ky Hieu</Translate> <FontAwesomeIcon
                icon="sort"/>
              </th>
              <th className="hand" onClick={sort('loai')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.loai">Loai</Translate> <FontAwesomeIcon
                icon="sort"/>
              </th>
              <th className="hand" onClick={sort('maTinh')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.maTinh">Ma Tinh</Translate> <FontAwesomeIcon
                icon="sort"/>
              </th>
              <th className="hand" onClick={sort('phone')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.phone">Phone</Translate> <FontAwesomeIcon
                icon="sort"/>
              </th>
              <th className="hand" onClick={sort('soTaiKhoan')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.soTaiKhoan">So Tai Khoan</Translate>
                <FontAwesomeIcon icon="sort"/>
              </th>
              <th className="hand" onClick={sort('ten')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.ten">Ten</Translate> <FontAwesomeIcon icon="sort"/>
              </th>
              <th className="hand" onClick={sort('tenNganHang')}>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.tenNganHang">Ten Ngan Hang</Translate>
                <FontAwesomeIcon icon="sort"/>
              </th>
              <th>
                <Translate contentKey="gatewayApp.khamchuabenhDonVi.thongTinBhxh">Thong Tin Bhxh</Translate>{' '}
                <FontAwesomeIcon icon="sort"/>
              </th>
              <th/>
            </tr>
            </thead>
            <tbody>
            {donViList.map((donVi, i) => (
              <tr key={`entity-${i}`}>
                <td>
                  <Button tag={Link} to={`${match.url}/${donVi.id}`} color="link" size="sm">
                    {donVi.id}
                  </Button>
                </td>
                <td>{donVi.cap}</td>
                <td>{donVi.chiNhanhNganHang}</td>
                <td>{donVi.diaChi}</td>
                <td>{donVi.donViQuanLyId}</td>
                <td>{donVi.dvtt}</td>
                <td>{donVi.email}</td>
                <td>{donVi.enabled ? 'true' : 'false'}</td>
                <td>{donVi.kyHieu}</td>
                <td>{donVi.loai}</td>
                <td>{donVi.maTinh}</td>
                <td>{donVi.phone}</td>
                <td>{donVi.soTaiKhoan}</td>
                <td>{donVi.ten}</td>
                <td>{donVi.tenNganHang}</td>
                <td>{donVi.thongTinBhxhId ?
                  <Link to={`thong-tin-bhxh/${donVi.thongTinBhxhId}`}>{donVi.thongTinBhxhId}</Link> : ''}</td>
                <td className="text-right">
                  <div className="btn-group flex-btn-group-container">
                    <Button tag={Link} to={`${match.url}/${donVi.id}`} color="info" size="sm">
                      <FontAwesomeIcon icon="eye"/>{' '}
                      <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                    </Button>
                    <Button
                      tag={Link}
                      to={`${match.url}/${donVi.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                      color="primary"
                      size="sm"
                    >
                      <FontAwesomeIcon icon="pencil-alt"/>{' '}
                      <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                    </Button>
                    <Button
                      tag={Link}
                      to={`${match.url}/${donVi.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                      color="danger"
                      size="sm"
                    >
                      <FontAwesomeIcon icon="trash"/>{' '}
                      <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                    </Button>
                  </div>
                </td>
              </tr>
            ))}
            </tbody>
          </Table>
        ) : (
          <div className="alert alert-warning">
            <Translate contentKey="gatewayApp.khamchuabenhDonVi.home.notFound">No Don Vis found</Translate>
          </div>
        )}
      </div>
      <div className={donViList && donViList.length > 0 ? '' : 'd-none'}>
        <Row className="justify-content-center">
          <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage}
                        i18nEnabled/>
        </Row>
        <Row className="justify-content-center">
          <JhiPagination
            activePage={paginationState.activePage}
            onSelect={handlePagination}
            maxButtons={5}
            itemsPerPage={paginationState.itemsPerPage}
            totalItems={props.totalItems}
          />
        </Row>
      </div>
    </div>
  );
};

const mapStateToProps = ({donVi}: IRootState) => ({
  donViList: donVi.entities,
  totalItems: donVi.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DonVi);
