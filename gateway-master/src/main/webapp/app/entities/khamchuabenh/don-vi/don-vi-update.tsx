import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Col, Label, Row, UncontrolledTooltip} from 'reactstrap';
import {AvField, AvForm, AvGroup, AvInput} from 'availity-reactstrap-validation';
import {Translate, translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IRootState} from 'app/shared/reducers';
import {getEntities as getThongTinBhxhs} from 'app/entities/khamchuabenh/thong-tin-bhxh/thong-tin-bhxh.reducer';
import {createEntity, getEntity, reset, updateEntity} from './don-vi.reducer';

export interface IDonViUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const DonViUpdate = (props: IDonViUpdateProps) => {
  const [thongTinBhxhId, setThongTinBhxhId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const {donViEntity, thongTinBhxhs, loading, updating} = props;

  const handleClose = () => {
    props.history.push('/don-vi' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getThongTinBhxhs();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...donViEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhDonVi.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhDonVi.home.createOrEditLabel">Create or edit a
              DonVi</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : donViEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="don-vi-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="don-vi-id" type="text" className="form-control" name="id" required readOnly/>
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="capLabel" for="don-vi-cap">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.cap">Cap</Translate>
                </Label>
                <AvField
                  id="don-vi-cap"
                  type="string"
                  className="form-control"
                  name="cap"
                  validate={{
                    required: {value: true, errorMessage: translate('entity.validation.required')},
                    number: {value: true, errorMessage: translate('entity.validation.number')}
                  }}
                />
                <UncontrolledTooltip target="capLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.cap"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="chiNhanhNganHangLabel" for="don-vi-chiNhanhNganHang">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.chiNhanhNganHang">Chi Nhanh Ngan Hang</Translate>
                </Label>
                <AvField
                  id="don-vi-chiNhanhNganHang"
                  type="text"
                  name="chiNhanhNganHang"
                  validate={{
                    maxLength: {value: 300, errorMessage: translate('entity.validation.maxlength', {max: 300})}
                  }}
                />
                <UncontrolledTooltip target="chiNhanhNganHangLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.chiNhanhNganHang"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="diaChiLabel" for="don-vi-diaChi">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.diaChi">Dia Chi</Translate>
                </Label>
                <AvField
                  id="don-vi-diaChi"
                  type="text"
                  name="diaChi"
                  validate={{
                    maxLength: {value: 1000, errorMessage: translate('entity.validation.maxlength', {max: 1000})}
                  }}
                />
                <UncontrolledTooltip target="diaChiLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.diaChi"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="donViQuanLyIdLabel" for="don-vi-donViQuanLyId">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.donViQuanLyId">Don Vi Quan Ly Id</Translate>
                </Label>
                <AvField id="don-vi-donViQuanLyId" type="text" name="donViQuanLyId"/>
                <UncontrolledTooltip target="donViQuanLyIdLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.donViQuanLyId"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="dvttLabel" for="don-vi-dvtt">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.dvtt">Dvtt</Translate>
                </Label>
                <AvField
                  id="don-vi-dvtt"
                  type="text"
                  name="dvtt"
                  validate={{
                    maxLength: {value: 10, errorMessage: translate('entity.validation.maxlength', {max: 10})}
                  }}
                />
                <UncontrolledTooltip target="dvttLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.dvtt"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="emailLabel" for="don-vi-email">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.email">Email</Translate>
                </Label>
                <AvField
                  id="don-vi-email"
                  type="text"
                  name="email"
                  validate={{
                    maxLength: {value: 50, errorMessage: translate('entity.validation.maxlength', {max: 50})}
                  }}
                />
                <UncontrolledTooltip target="emailLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.email"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup check>
                <Label id="enabledLabel">
                  <AvInput id="don-vi-enabled" type="checkbox" className="form-check-input" name="enabled"/>
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.enabled">Enabled</Translate>
                </Label>
                <UncontrolledTooltip target="enabledLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.enabled"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="kyHieuLabel" for="don-vi-kyHieu">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.kyHieu">Ky Hieu</Translate>
                </Label>
                <AvField
                  id="don-vi-kyHieu"
                  type="text"
                  name="kyHieu"
                  validate={{
                    maxLength: {value: 100, errorMessage: translate('entity.validation.maxlength', {max: 100})}
                  }}
                />
                <UncontrolledTooltip target="kyHieuLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.kyHieu"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="loaiLabel" for="don-vi-loai">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.loai">Loai</Translate>
                </Label>
                <AvField
                  id="don-vi-loai"
                  type="string"
                  className="form-control"
                  name="loai"
                  validate={{
                    required: {value: true, errorMessage: translate('entity.validation.required')},
                    number: {value: true, errorMessage: translate('entity.validation.number')}
                  }}
                />
                <UncontrolledTooltip target="loaiLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.loai"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="maTinhLabel" for="don-vi-maTinh">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.maTinh">Ma Tinh</Translate>
                </Label>
                <AvField
                  id="don-vi-maTinh"
                  type="text"
                  name="maTinh"
                  validate={{
                    required: {value: true, errorMessage: translate('entity.validation.required')},
                    maxLength: {value: 10, errorMessage: translate('entity.validation.maxlength', {max: 10})}
                  }}
                />
                <UncontrolledTooltip target="maTinhLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.maTinh"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="phoneLabel" for="don-vi-phone">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.phone">Phone</Translate>
                </Label>
                <AvField
                  id="don-vi-phone"
                  type="text"
                  name="phone"
                  validate={{
                    maxLength: {value: 15, errorMessage: translate('entity.validation.maxlength', {max: 15})}
                  }}
                />
                <UncontrolledTooltip target="phoneLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.phone"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="soTaiKhoanLabel" for="don-vi-soTaiKhoan">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.soTaiKhoan">So Tai Khoan</Translate>
                </Label>
                <AvField
                  id="don-vi-soTaiKhoan"
                  type="text"
                  name="soTaiKhoan"
                  validate={{
                    maxLength: {value: 30, errorMessage: translate('entity.validation.maxlength', {max: 30})}
                  }}
                />
                <UncontrolledTooltip target="soTaiKhoanLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.soTaiKhoan"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenLabel" for="don-vi-ten">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.ten">Ten</Translate>
                </Label>
                <AvField
                  id="don-vi-ten"
                  type="text"
                  name="ten"
                  validate={{
                    required: {value: true, errorMessage: translate('entity.validation.required')},
                    maxLength: {value: 300, errorMessage: translate('entity.validation.maxlength', {max: 300})}
                  }}
                />
                <UncontrolledTooltip target="tenLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.ten"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenNganHangLabel" for="don-vi-tenNganHang">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.tenNganHang">Ten Ngan Hang</Translate>
                </Label>
                <AvField
                  id="don-vi-tenNganHang"
                  type="text"
                  name="tenNganHang"
                  validate={{
                    maxLength: {value: 300, errorMessage: translate('entity.validation.maxlength', {max: 300})}
                  }}
                />
                <UncontrolledTooltip target="tenNganHangLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.tenNganHang"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="don-vi-thongTinBhxh">
                  <Translate contentKey="gatewayApp.khamchuabenhDonVi.thongTinBhxh">Thong Tin Bhxh</Translate>
                </Label>
                <AvInput id="don-vi-thongTinBhxh" type="select" className="form-control" name="thongTinBhxhId">
                  <option value="" key="0"/>
                  {thongTinBhxhs
                    ? thongTinBhxhs.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/don-vi" replace color="info">
                <FontAwesomeIcon icon="arrow-left"/>
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save"/>
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  thongTinBhxhs: storeState.thongTinBhxh.entities,
  donViEntity: storeState.donVi.entity,
  loading: storeState.donVi.loading,
  updating: storeState.donVi.updating,
  updateSuccess: storeState.donVi.updateSuccess
});

const mapDispatchToProps = {
  getThongTinBhxhs,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DonViUpdate);
