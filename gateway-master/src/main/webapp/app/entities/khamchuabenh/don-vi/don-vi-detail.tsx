import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Col, Row, UncontrolledTooltip} from 'reactstrap';
import {Translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {IRootState} from 'app/shared/reducers';
import {getEntity} from './don-vi.reducer';

export interface IDonViDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const DonViDetail = (props: IDonViDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const {donViEntity} = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhDonVi.detail.title">DonVi</Translate> [<b>{donViEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="cap">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.cap">Cap</Translate>
            </span>
            <UncontrolledTooltip target="cap">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.cap"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.cap}</dd>
          <dt>
            <span id="chiNhanhNganHang">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.chiNhanhNganHang">Chi Nhanh Ngan Hang</Translate>
            </span>
            <UncontrolledTooltip target="chiNhanhNganHang">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.chiNhanhNganHang"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.chiNhanhNganHang}</dd>
          <dt>
            <span id="diaChi">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.diaChi">Dia Chi</Translate>
            </span>
            <UncontrolledTooltip target="diaChi">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.diaChi"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.diaChi}</dd>
          <dt>
            <span id="donViQuanLyId">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.donViQuanLyId">Don Vi Quan Ly Id</Translate>
            </span>
            <UncontrolledTooltip target="donViQuanLyId">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.donViQuanLyId"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.donViQuanLyId}</dd>
          <dt>
            <span id="dvtt">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.dvtt">Dvtt</Translate>
            </span>
            <UncontrolledTooltip target="dvtt">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.dvtt"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.dvtt}</dd>
          <dt>
            <span id="email">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.email">Email</Translate>
            </span>
            <UncontrolledTooltip target="email">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.email"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.email}</dd>
          <dt>
            <span id="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.enabled">Enabled</Translate>
            </span>
            <UncontrolledTooltip target="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.enabled"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.enabled ? 'true' : 'false'}</dd>
          <dt>
            <span id="kyHieu">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.kyHieu">Ky Hieu</Translate>
            </span>
            <UncontrolledTooltip target="kyHieu">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.kyHieu"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.kyHieu}</dd>
          <dt>
            <span id="loai">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.loai">Loai</Translate>
            </span>
            <UncontrolledTooltip target="loai">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.loai"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.loai}</dd>
          <dt>
            <span id="maTinh">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.maTinh">Ma Tinh</Translate>
            </span>
            <UncontrolledTooltip target="maTinh">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.maTinh"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.maTinh}</dd>
          <dt>
            <span id="phone">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.phone">Phone</Translate>
            </span>
            <UncontrolledTooltip target="phone">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.phone"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.phone}</dd>
          <dt>
            <span id="soTaiKhoan">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.soTaiKhoan">So Tai Khoan</Translate>
            </span>
            <UncontrolledTooltip target="soTaiKhoan">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.soTaiKhoan"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.soTaiKhoan}</dd>
          <dt>
            <span id="ten">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.ten">Ten</Translate>
            </span>
            <UncontrolledTooltip target="ten">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.ten"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.ten}</dd>
          <dt>
            <span id="tenNganHang">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.tenNganHang">Ten Ngan Hang</Translate>
            </span>
            <UncontrolledTooltip target="tenNganHang">
              <Translate contentKey="gatewayApp.khamchuabenhDonVi.help.tenNganHang"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{donViEntity.tenNganHang}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhDonVi.thongTinBhxh">Thong Tin Bhxh</Translate>
          </dt>
          <dd>{donViEntity.thongTinBhxhId ? donViEntity.thongTinBhxhId : ''}</dd>
        </dl>
        <Button tag={Link} to="/don-vi" replace color="info">
          <FontAwesomeIcon icon="arrow-left"/>{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/don-vi/${donViEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt"/>{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({donVi}: IRootState) => ({
  donViEntity: donVi.entity
});

const mapDispatchToProps = {getEntity};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DonViDetail);
