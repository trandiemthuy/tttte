import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IPhong, defaultValue } from 'app/shared/model/khamchuabenh/phong.model';

export const ACTION_TYPES = {
  FETCH_PHONG_LIST: 'phong/FETCH_PHONG_LIST',
  FETCH_PHONG: 'phong/FETCH_PHONG',
  CREATE_PHONG: 'phong/CREATE_PHONG',
  UPDATE_PHONG: 'phong/UPDATE_PHONG',
  DELETE_PHONG: 'phong/DELETE_PHONG',
  RESET: 'phong/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPhong>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type PhongState = Readonly<typeof initialState>;

// Reducer

export default (state: PhongState = initialState, action): PhongState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PHONG_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PHONG):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PHONG):
    case REQUEST(ACTION_TYPES.UPDATE_PHONG):
    case REQUEST(ACTION_TYPES.DELETE_PHONG):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PHONG_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PHONG):
    case FAILURE(ACTION_TYPES.CREATE_PHONG):
    case FAILURE(ACTION_TYPES.UPDATE_PHONG):
    case FAILURE(ACTION_TYPES.DELETE_PHONG):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PHONG_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_PHONG):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PHONG):
    case SUCCESS(ACTION_TYPES.UPDATE_PHONG):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PHONG):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/phongs';

// Actions

export const getEntities: ICrudGetAllAction<IPhong> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PHONG_LIST,
    payload: axios.get<IPhong>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IPhong> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PHONG,
    payload: axios.get<IPhong>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IPhong> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PHONG,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPhong> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PHONG,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPhong> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PHONG,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
