import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IKhoa } from 'app/shared/model/khamchuabenh/khoa.model';
import { getEntities as getKhoas } from 'app/entities/khamchuabenh/khoa/khoa.reducer';
import { getEntity, updateEntity, createEntity, reset } from './phong.reducer';
import { IPhong } from 'app/shared/model/khamchuabenh/phong.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPhongUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PhongUpdate = (props: IPhongUpdateProps) => {
  const [khoaId, setKhoaId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { phongEntity, khoas, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/phong' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getKhoas();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...phongEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhPhong.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhPhong.home.createOrEditLabel">Create or edit a Phong</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : phongEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="phong-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="phong-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup check>
                <Label id="enabledLabel">
                  <AvInput id="phong-enabled" type="checkbox" className="form-check-input" name="enabled" />
                  <Translate contentKey="gatewayApp.khamchuabenhPhong.enabled">Enabled</Translate>
                </Label>
                <UncontrolledTooltip target="enabledLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhPhong.help.enabled" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="kyHieuLabel" for="phong-kyHieu">
                  <Translate contentKey="gatewayApp.khamchuabenhPhong.kyHieu">Ky Hieu</Translate>
                </Label>
                <AvField
                  id="phong-kyHieu"
                  type="text"
                  name="kyHieu"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="kyHieuLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhPhong.help.kyHieu" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="loaiLabel" for="phong-loai">
                  <Translate contentKey="gatewayApp.khamchuabenhPhong.loai">Loai</Translate>
                </Label>
                <AvField
                  id="phong-loai"
                  type="text"
                  name="loai"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="loaiLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhPhong.help.loai" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="phoneLabel" for="phong-phone">
                  <Translate contentKey="gatewayApp.khamchuabenhPhong.phone">Phone</Translate>
                </Label>
                <AvField
                  id="phong-phone"
                  type="text"
                  name="phone"
                  validate={{
                    maxLength: { value: 15, errorMessage: translate('entity.validation.maxlength', { max: 15 }) }
                  }}
                />
                <UncontrolledTooltip target="phoneLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhPhong.help.phone" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenLabel" for="phong-ten">
                  <Translate contentKey="gatewayApp.khamchuabenhPhong.ten">Ten</Translate>
                </Label>
                <AvField
                  id="phong-ten"
                  type="text"
                  name="ten"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="tenLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhPhong.help.ten" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="viTriLabel" for="phong-viTri">
                  <Translate contentKey="gatewayApp.khamchuabenhPhong.viTri">Vi Tri</Translate>
                </Label>
                <AvField
                  id="phong-viTri"
                  type="text"
                  name="viTri"
                  validate={{
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="viTriLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhPhong.help.viTri" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="phong-khoa">
                  <Translate contentKey="gatewayApp.khamchuabenhPhong.khoa">Khoa</Translate>
                </Label>
                <AvInput id="phong-khoa" type="select" className="form-control" name="khoaId" required>
                  {khoas
                    ? khoas.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/phong" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  khoas: storeState.khoa.entities,
  phongEntity: storeState.phong.entity,
  loading: storeState.phong.loading,
  updating: storeState.phong.updating,
  updateSuccess: storeState.phong.updateSuccess
});

const mapDispatchToProps = {
  getKhoas,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PhongUpdate);
