import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Phong from './phong';
import PhongDetail from './phong-detail';
import PhongUpdate from './phong-update';
import PhongDeleteDialog from './phong-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PhongDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PhongUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PhongUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PhongDetail} />
      <ErrorBoundaryRoute path={match.url} component={Phong} />
    </Switch>
  </>
);

export default Routes;
