import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './phong.reducer';
import { IPhong } from 'app/shared/model/khamchuabenh/phong.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPhongDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PhongDetail = (props: IPhongDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { phongEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhPhong.detail.title">Phong</Translate> [<b>{phongEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhPhong.enabled">Enabled</Translate>
            </span>
            <UncontrolledTooltip target="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhPhong.help.enabled" />
            </UncontrolledTooltip>
          </dt>
          <dd>{phongEntity.enabled ? 'true' : 'false'}</dd>
          <dt>
            <span id="kyHieu">
              <Translate contentKey="gatewayApp.khamchuabenhPhong.kyHieu">Ky Hieu</Translate>
            </span>
            <UncontrolledTooltip target="kyHieu">
              <Translate contentKey="gatewayApp.khamchuabenhPhong.help.kyHieu" />
            </UncontrolledTooltip>
          </dt>
          <dd>{phongEntity.kyHieu}</dd>
          <dt>
            <span id="loai">
              <Translate contentKey="gatewayApp.khamchuabenhPhong.loai">Loai</Translate>
            </span>
            <UncontrolledTooltip target="loai">
              <Translate contentKey="gatewayApp.khamchuabenhPhong.help.loai" />
            </UncontrolledTooltip>
          </dt>
          <dd>{phongEntity.loai}</dd>
          <dt>
            <span id="phone">
              <Translate contentKey="gatewayApp.khamchuabenhPhong.phone">Phone</Translate>
            </span>
            <UncontrolledTooltip target="phone">
              <Translate contentKey="gatewayApp.khamchuabenhPhong.help.phone" />
            </UncontrolledTooltip>
          </dt>
          <dd>{phongEntity.phone}</dd>
          <dt>
            <span id="ten">
              <Translate contentKey="gatewayApp.khamchuabenhPhong.ten">Ten</Translate>
            </span>
            <UncontrolledTooltip target="ten">
              <Translate contentKey="gatewayApp.khamchuabenhPhong.help.ten" />
            </UncontrolledTooltip>
          </dt>
          <dd>{phongEntity.ten}</dd>
          <dt>
            <span id="viTri">
              <Translate contentKey="gatewayApp.khamchuabenhPhong.viTri">Vi Tri</Translate>
            </span>
            <UncontrolledTooltip target="viTri">
              <Translate contentKey="gatewayApp.khamchuabenhPhong.help.viTri" />
            </UncontrolledTooltip>
          </dt>
          <dd>{phongEntity.viTri}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhPhong.khoa">Khoa</Translate>
          </dt>
          <dd>{phongEntity.khoaId ? phongEntity.khoaId : ''}</dd>
        </dl>
        <Button tag={Link} to="/phong" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/phong/${phongEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ phong }: IRootState) => ({
  phongEntity: phong.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PhongDetail);
