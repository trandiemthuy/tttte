import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './dich-vu-kham.reducer';
import { IDichVuKham } from 'app/shared/model/khamchuabenh/dich-vu-kham.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IDichVuKhamDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DichVuKhamDetail = (props: IDichVuKhamDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { dichVuKhamEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.detail.title">DichVuKham</Translate> [<b>{dichVuKhamEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="deleted">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.deleted">Deleted</Translate>
            </span>
            <UncontrolledTooltip target="deleted">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.help.deleted" />
            </UncontrolledTooltip>
          </dt>
          <dd>{dichVuKhamEntity.deleted}</dd>
          <dt>
            <span id="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.enabled">Enabled</Translate>
            </span>
            <UncontrolledTooltip target="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.help.enabled" />
            </UncontrolledTooltip>
          </dt>
          <dd>{dichVuKhamEntity.enabled}</dd>
          <dt>
            <span id="gioiHanChiDinh">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.gioiHanChiDinh">Gioi Han Chi Dinh</Translate>
            </span>
            <UncontrolledTooltip target="gioiHanChiDinh">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.help.gioiHanChiDinh" />
            </UncontrolledTooltip>
          </dt>
          <dd>{dichVuKhamEntity.gioiHanChiDinh}</dd>
          <dt>
            <span id="phanTheoGioTinh">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.phanTheoGioTinh">Phan Theo Gio Tinh</Translate>
            </span>
          </dt>
          <dd>{dichVuKhamEntity.phanTheoGioTinh}</dd>
          <dt>
            <span id="tenHienThi">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.tenHienThi">Ten Hien Thi</Translate>
            </span>
          </dt>
          <dd>{dichVuKhamEntity.tenHienThi}</dd>
          <dt>
            <span id="maDungChung">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.maDungChung">Ma Dung Chung</Translate>
            </span>
          </dt>
          <dd>{dichVuKhamEntity.maDungChung}</dd>
          <dt>
            <span id="maNoiBo">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.maNoiBo">Ma Noi Bo</Translate>
            </span>
          </dt>
          <dd>{dichVuKhamEntity.maNoiBo}</dd>
          <dt>
            <span id="phamViChiDinh">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.phamViChiDinh">Pham Vi Chi Dinh</Translate>
            </span>
            <UncontrolledTooltip target="phamViChiDinh">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.help.phamViChiDinh" />
            </UncontrolledTooltip>
          </dt>
          <dd>{dichVuKhamEntity.phamViChiDinh}</dd>
          <dt>
            <span id="donViTinh">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.donViTinh">Don Vi Tinh</Translate>
            </span>
          </dt>
          <dd>{dichVuKhamEntity.donViTinh}</dd>
          <dt>
            <span id="donGiaBenhVien">
              <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.donGiaBenhVien">Don Gia Benh Vien</Translate>
            </span>
          </dt>
          <dd>{dichVuKhamEntity.donGiaBenhVien}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.dotThayDoiMaDichVu">Dot Thay Doi Ma Dich Vu</Translate>
          </dt>
          <dd>{dichVuKhamEntity.dotThayDoiMaDichVuId ? dichVuKhamEntity.dotThayDoiMaDichVuId : ''}</dd>
        </dl>
        <Button tag={Link} to="/dich-vu-kham" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/dich-vu-kham/${dichVuKhamEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ dichVuKham }: IRootState) => ({
  dichVuKhamEntity: dichVuKham.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DichVuKhamDetail);
