import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import DichVuKham from './dich-vu-kham';
import DichVuKhamDetail from './dich-vu-kham-detail';
import DichVuKhamUpdate from './dich-vu-kham-update';
import DichVuKhamDeleteDialog from './dich-vu-kham-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={DichVuKhamDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={DichVuKhamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={DichVuKhamUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={DichVuKhamDetail} />
      <ErrorBoundaryRoute path={match.url} component={DichVuKham} />
    </Switch>
  </>
);

export default Routes;
