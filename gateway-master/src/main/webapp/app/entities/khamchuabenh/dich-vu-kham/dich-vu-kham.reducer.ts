import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IDichVuKham, defaultValue } from 'app/shared/model/khamchuabenh/dich-vu-kham.model';

export const ACTION_TYPES = {
  FETCH_DICHVUKHAM_LIST: 'dichVuKham/FETCH_DICHVUKHAM_LIST',
  FETCH_DICHVUKHAM: 'dichVuKham/FETCH_DICHVUKHAM',
  CREATE_DICHVUKHAM: 'dichVuKham/CREATE_DICHVUKHAM',
  UPDATE_DICHVUKHAM: 'dichVuKham/UPDATE_DICHVUKHAM',
  DELETE_DICHVUKHAM: 'dichVuKham/DELETE_DICHVUKHAM',
  RESET: 'dichVuKham/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IDichVuKham>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type DichVuKhamState = Readonly<typeof initialState>;

// Reducer

export default (state: DichVuKhamState = initialState, action): DichVuKhamState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_DICHVUKHAM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_DICHVUKHAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_DICHVUKHAM):
    case REQUEST(ACTION_TYPES.UPDATE_DICHVUKHAM):
    case REQUEST(ACTION_TYPES.DELETE_DICHVUKHAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_DICHVUKHAM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_DICHVUKHAM):
    case FAILURE(ACTION_TYPES.CREATE_DICHVUKHAM):
    case FAILURE(ACTION_TYPES.UPDATE_DICHVUKHAM):
    case FAILURE(ACTION_TYPES.DELETE_DICHVUKHAM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_DICHVUKHAM_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_DICHVUKHAM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_DICHVUKHAM):
    case SUCCESS(ACTION_TYPES.UPDATE_DICHVUKHAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_DICHVUKHAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/dich-vu-khams';

// Actions

export const getEntities: ICrudGetAllAction<IDichVuKham> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_DICHVUKHAM_LIST,
    payload: axios.get<IDichVuKham>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IDichVuKham> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_DICHVUKHAM,
    payload: axios.get<IDichVuKham>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IDichVuKham> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_DICHVUKHAM,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IDichVuKham> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_DICHVUKHAM,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IDichVuKham> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_DICHVUKHAM,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
