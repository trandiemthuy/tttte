import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IDotThayDoiMaDichVu } from 'app/shared/model/khamchuabenh/dot-thay-doi-ma-dich-vu.model';
import { getEntities as getDotThayDoiMaDichVus } from 'app/entities/khamchuabenh/dot-thay-doi-ma-dich-vu/dot-thay-doi-ma-dich-vu.reducer';
import { getEntity, updateEntity, createEntity, reset } from './dich-vu-kham.reducer';
import { IDichVuKham } from 'app/shared/model/khamchuabenh/dich-vu-kham.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IDichVuKhamUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DichVuKhamUpdate = (props: IDichVuKhamUpdateProps) => {
  const [dotThayDoiMaDichVuId, setDotThayDoiMaDichVuId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { dichVuKhamEntity, dotThayDoiMaDichVus, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/dich-vu-kham' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getDotThayDoiMaDichVus();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...dichVuKhamEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhDichVuKham.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.home.createOrEditLabel">Create or edit a DichVuKham</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : dichVuKhamEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="dich-vu-kham-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="dich-vu-kham-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="deletedLabel" for="dich-vu-kham-deleted">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.deleted">Deleted</Translate>
                </Label>
                <AvField id="dich-vu-kham-deleted" type="string" className="form-control" name="deleted" />
                <UncontrolledTooltip target="deletedLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.help.deleted" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="enabledLabel" for="dich-vu-kham-enabled">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.enabled">Enabled</Translate>
                </Label>
                <AvField id="dich-vu-kham-enabled" type="string" className="form-control" name="enabled" />
                <UncontrolledTooltip target="enabledLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.help.enabled" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="gioiHanChiDinhLabel" for="dich-vu-kham-gioiHanChiDinh">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.gioiHanChiDinh">Gioi Han Chi Dinh</Translate>
                </Label>
                <AvField id="dich-vu-kham-gioiHanChiDinh" type="string" className="form-control" name="gioiHanChiDinh" />
                <UncontrolledTooltip target="gioiHanChiDinhLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.help.gioiHanChiDinh" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="phanTheoGioTinhLabel" for="dich-vu-kham-phanTheoGioTinh">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.phanTheoGioTinh">Phan Theo Gio Tinh</Translate>
                </Label>
                <AvField id="dich-vu-kham-phanTheoGioTinh" type="string" className="form-control" name="phanTheoGioTinh" />
              </AvGroup>
              <AvGroup>
                <Label id="tenHienThiLabel" for="dich-vu-kham-tenHienThi">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.tenHienThi">Ten Hien Thi</Translate>
                </Label>
                <AvField
                  id="dich-vu-kham-tenHienThi"
                  type="text"
                  name="tenHienThi"
                  validate={{
                    maxLength: { value: 1000, errorMessage: translate('entity.validation.maxlength', { max: 1000 }) }
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="maDungChungLabel" for="dich-vu-kham-maDungChung">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.maDungChung">Ma Dung Chung</Translate>
                </Label>
                <AvField
                  id="dich-vu-kham-maDungChung"
                  type="text"
                  name="maDungChung"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="maNoiBoLabel" for="dich-vu-kham-maNoiBo">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.maNoiBo">Ma Noi Bo</Translate>
                </Label>
                <AvField
                  id="dich-vu-kham-maNoiBo"
                  type="text"
                  name="maNoiBo"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="phamViChiDinhLabel" for="dich-vu-kham-phamViChiDinh">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.phamViChiDinh">Pham Vi Chi Dinh</Translate>
                </Label>
                <AvField id="dich-vu-kham-phamViChiDinh" type="string" className="form-control" name="phamViChiDinh" />
                <UncontrolledTooltip target="phamViChiDinhLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.help.phamViChiDinh" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="donViTinhLabel" for="dich-vu-kham-donViTinh">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.donViTinh">Don Vi Tinh</Translate>
                </Label>
                <AvField
                  id="dich-vu-kham-donViTinh"
                  type="text"
                  name="donViTinh"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="donGiaBenhVienLabel" for="dich-vu-kham-donGiaBenhVien">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.donGiaBenhVien">Don Gia Benh Vien</Translate>
                </Label>
                <AvField
                  id="dich-vu-kham-donGiaBenhVien"
                  type="text"
                  name="donGiaBenhVien"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="dich-vu-kham-dotThayDoiMaDichVu">
                  <Translate contentKey="gatewayApp.khamchuabenhDichVuKham.dotThayDoiMaDichVu">Dot Thay Doi Ma Dich Vu</Translate>
                </Label>
                <AvInput id="dich-vu-kham-dotThayDoiMaDichVu" type="select" className="form-control" name="dotThayDoiMaDichVuId" required>
                  {dotThayDoiMaDichVus
                    ? dotThayDoiMaDichVus.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/dich-vu-kham" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  dotThayDoiMaDichVus: storeState.dotThayDoiMaDichVu.entities,
  dichVuKhamEntity: storeState.dichVuKham.entity,
  loading: storeState.dichVuKham.loading,
  updating: storeState.dichVuKham.updating,
  updateSuccess: storeState.dichVuKham.updateSuccess
});

const mapDispatchToProps = {
  getDotThayDoiMaDichVus,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DichVuKhamUpdate);
