import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './tinh-thanh-pho.reducer';
import { ITinhThanhPho } from 'app/shared/model/khamchuabenh/tinh-thanh-pho.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ITinhThanhPhoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TinhThanhPhoDetail = (props: ITinhThanhPhoDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { tinhThanhPhoEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.detail.title">TinhThanhPho</Translate> [<b>{tinhThanhPhoEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="cap">
              <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.cap">Cap</Translate>
            </span>
            <UncontrolledTooltip target="cap">
              <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.help.cap" />
            </UncontrolledTooltip>
          </dt>
          <dd>{tinhThanhPhoEntity.cap}</dd>
          <dt>
            <span id="guessPhrase">
              <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.guessPhrase">Guess Phrase</Translate>
            </span>
            <UncontrolledTooltip target="guessPhrase">
              <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.help.guessPhrase" />
            </UncontrolledTooltip>
          </dt>
          <dd>{tinhThanhPhoEntity.guessPhrase}</dd>
          <dt>
            <span id="ten">
              <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.ten">Ten</Translate>
            </span>
            <UncontrolledTooltip target="ten">
              <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.help.ten" />
            </UncontrolledTooltip>
          </dt>
          <dd>{tinhThanhPhoEntity.ten}</dd>
          <dt>
            <span id="tenKhongDau">
              <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.tenKhongDau">Ten Khong Dau</Translate>
            </span>
            <UncontrolledTooltip target="tenKhongDau">
              <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.help.tenKhongDau" />
            </UncontrolledTooltip>
          </dt>
          <dd>{tinhThanhPhoEntity.tenKhongDau}</dd>
        </dl>
        <Button tag={Link} to="/tinh-thanh-pho" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/tinh-thanh-pho/${tinhThanhPhoEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ tinhThanhPho }: IRootState) => ({
  tinhThanhPhoEntity: tinhThanhPho.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TinhThanhPhoDetail);
