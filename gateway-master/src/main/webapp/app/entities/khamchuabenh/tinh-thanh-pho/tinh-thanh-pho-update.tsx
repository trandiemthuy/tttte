import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './tinh-thanh-pho.reducer';
import { ITinhThanhPho } from 'app/shared/model/khamchuabenh/tinh-thanh-pho.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ITinhThanhPhoUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TinhThanhPhoUpdate = (props: ITinhThanhPhoUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { tinhThanhPhoEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/tinh-thanh-pho' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...tinhThanhPhoEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhTinhThanhPho.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.home.createOrEditLabel">Create or edit a TinhThanhPho</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : tinhThanhPhoEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="tinh-thanh-pho-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="tinh-thanh-pho-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="capLabel" for="tinh-thanh-pho-cap">
                  <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.cap">Cap</Translate>
                </Label>
                <AvField
                  id="tinh-thanh-pho-cap"
                  type="text"
                  name="cap"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="capLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.help.cap" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="guessPhraseLabel" for="tinh-thanh-pho-guessPhrase">
                  <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.guessPhrase">Guess Phrase</Translate>
                </Label>
                <AvField
                  id="tinh-thanh-pho-guessPhrase"
                  type="text"
                  name="guessPhrase"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="guessPhraseLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.help.guessPhrase" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenLabel" for="tinh-thanh-pho-ten">
                  <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.ten">Ten</Translate>
                </Label>
                <AvField
                  id="tinh-thanh-pho-ten"
                  type="text"
                  name="ten"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="tenLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.help.ten" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenKhongDauLabel" for="tinh-thanh-pho-tenKhongDau">
                  <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.tenKhongDau">Ten Khong Dau</Translate>
                </Label>
                <AvField
                  id="tinh-thanh-pho-tenKhongDau"
                  type="text"
                  name="tenKhongDau"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="tenKhongDauLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTinhThanhPho.help.tenKhongDau" />
                </UncontrolledTooltip>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/tinh-thanh-pho" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  tinhThanhPhoEntity: storeState.tinhThanhPho.entity,
  loading: storeState.tinhThanhPho.loading,
  updating: storeState.tinhThanhPho.updating,
  updateSuccess: storeState.tinhThanhPho.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TinhThanhPhoUpdate);
