import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { ITinhThanhPho, defaultValue } from 'app/shared/model/khamchuabenh/tinh-thanh-pho.model';

export const ACTION_TYPES = {
  FETCH_TINHTHANHPHO_LIST: 'tinhThanhPho/FETCH_TINHTHANHPHO_LIST',
  FETCH_TINHTHANHPHO: 'tinhThanhPho/FETCH_TINHTHANHPHO',
  CREATE_TINHTHANHPHO: 'tinhThanhPho/CREATE_TINHTHANHPHO',
  UPDATE_TINHTHANHPHO: 'tinhThanhPho/UPDATE_TINHTHANHPHO',
  DELETE_TINHTHANHPHO: 'tinhThanhPho/DELETE_TINHTHANHPHO',
  RESET: 'tinhThanhPho/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ITinhThanhPho>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type TinhThanhPhoState = Readonly<typeof initialState>;

// Reducer

export default (state: TinhThanhPhoState = initialState, action): TinhThanhPhoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_TINHTHANHPHO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_TINHTHANHPHO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_TINHTHANHPHO):
    case REQUEST(ACTION_TYPES.UPDATE_TINHTHANHPHO):
    case REQUEST(ACTION_TYPES.DELETE_TINHTHANHPHO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_TINHTHANHPHO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_TINHTHANHPHO):
    case FAILURE(ACTION_TYPES.CREATE_TINHTHANHPHO):
    case FAILURE(ACTION_TYPES.UPDATE_TINHTHANHPHO):
    case FAILURE(ACTION_TYPES.DELETE_TINHTHANHPHO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_TINHTHANHPHO_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_TINHTHANHPHO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_TINHTHANHPHO):
    case SUCCESS(ACTION_TYPES.UPDATE_TINHTHANHPHO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_TINHTHANHPHO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/tinh-thanh-phos';

// Actions

export const getEntities: ICrudGetAllAction<ITinhThanhPho> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_TINHTHANHPHO_LIST,
    payload: axios.get<ITinhThanhPho>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ITinhThanhPho> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_TINHTHANHPHO,
    payload: axios.get<ITinhThanhPho>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ITinhThanhPho> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_TINHTHANHPHO,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ITinhThanhPho> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_TINHTHANHPHO,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ITinhThanhPho> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_TINHTHANHPHO,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
