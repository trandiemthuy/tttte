import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import TinhThanhPho from './tinh-thanh-pho';
import TinhThanhPhoDetail from './tinh-thanh-pho-detail';
import TinhThanhPhoUpdate from './tinh-thanh-pho-update';
import TinhThanhPhoDeleteDialog from './tinh-thanh-pho-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={TinhThanhPhoDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={TinhThanhPhoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={TinhThanhPhoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={TinhThanhPhoDetail} />
      <ErrorBoundaryRoute path={match.url} component={TinhThanhPho} />
    </Switch>
  </>
);

export default Routes;
