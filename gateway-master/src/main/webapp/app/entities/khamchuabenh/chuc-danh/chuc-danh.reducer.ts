import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IChucDanh, defaultValue } from 'app/shared/model/khamchuabenh/chuc-danh.model';

export const ACTION_TYPES = {
  FETCH_CHUCDANH_LIST: 'chucDanh/FETCH_CHUCDANH_LIST',
  FETCH_CHUCDANH: 'chucDanh/FETCH_CHUCDANH',
  CREATE_CHUCDANH: 'chucDanh/CREATE_CHUCDANH',
  UPDATE_CHUCDANH: 'chucDanh/UPDATE_CHUCDANH',
  DELETE_CHUCDANH: 'chucDanh/DELETE_CHUCDANH',
  RESET: 'chucDanh/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IChucDanh>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ChucDanhState = Readonly<typeof initialState>;

// Reducer

export default (state: ChucDanhState = initialState, action): ChucDanhState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CHUCDANH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CHUCDANH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_CHUCDANH):
    case REQUEST(ACTION_TYPES.UPDATE_CHUCDANH):
    case REQUEST(ACTION_TYPES.DELETE_CHUCDANH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CHUCDANH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CHUCDANH):
    case FAILURE(ACTION_TYPES.CREATE_CHUCDANH):
    case FAILURE(ACTION_TYPES.UPDATE_CHUCDANH):
    case FAILURE(ACTION_TYPES.DELETE_CHUCDANH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CHUCDANH_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_CHUCDANH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_CHUCDANH):
    case SUCCESS(ACTION_TYPES.UPDATE_CHUCDANH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CHUCDANH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/chuc-danhs';

// Actions

export const getEntities: ICrudGetAllAction<IChucDanh> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CHUCDANH_LIST,
    payload: axios.get<IChucDanh>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IChucDanh> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CHUCDANH,
    payload: axios.get<IChucDanh>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IChucDanh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CHUCDANH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IChucDanh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CHUCDANH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IChucDanh> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CHUCDANH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
