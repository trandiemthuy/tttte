import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ChucDanh from './chuc-danh';
import ChucDanhDetail from './chuc-danh-detail';
import ChucDanhUpdate from './chuc-danh-update';
import ChucDanhDeleteDialog from './chuc-danh-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ChucDanhDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ChucDanhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ChucDanhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ChucDanhDetail} />
      <ErrorBoundaryRoute path={match.url} component={ChucDanh} />
    </Switch>
  </>
);

export default Routes;
