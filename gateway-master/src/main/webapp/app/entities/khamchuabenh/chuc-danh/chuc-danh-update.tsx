import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './chuc-danh.reducer';
import { IChucDanh } from 'app/shared/model/khamchuabenh/chuc-danh.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IChucDanhUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ChucDanhUpdate = (props: IChucDanhUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { chucDanhEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/chuc-danh' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...chucDanhEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhChucDanh.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhChucDanh.home.createOrEditLabel">Create or edit a ChucDanh</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : chucDanhEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="chuc-danh-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="chuc-danh-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="moTaLabel" for="chuc-danh-moTa">
                  <Translate contentKey="gatewayApp.khamchuabenhChucDanh.moTa">Mo Ta</Translate>
                </Label>
                <AvField
                  id="chuc-danh-moTa"
                  type="text"
                  name="moTa"
                  validate={{
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="moTaLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhChucDanh.help.moTa" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenLabel" for="chuc-danh-ten">
                  <Translate contentKey="gatewayApp.khamchuabenhChucDanh.ten">Ten</Translate>
                </Label>
                <AvField
                  id="chuc-danh-ten"
                  type="text"
                  name="ten"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="tenLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhChucDanh.help.ten" />
                </UncontrolledTooltip>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/chuc-danh" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  chucDanhEntity: storeState.chucDanh.entity,
  loading: storeState.chucDanh.loading,
  updating: storeState.chucDanh.updating,
  updateSuccess: storeState.chucDanh.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ChucDanhUpdate);
