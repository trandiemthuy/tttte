import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './chuc-danh.reducer';
import { IChucDanh } from 'app/shared/model/khamchuabenh/chuc-danh.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IChucDanhDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ChucDanhDetail = (props: IChucDanhDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { chucDanhEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhChucDanh.detail.title">ChucDanh</Translate> [<b>{chucDanhEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="moTa">
              <Translate contentKey="gatewayApp.khamchuabenhChucDanh.moTa">Mo Ta</Translate>
            </span>
            <UncontrolledTooltip target="moTa">
              <Translate contentKey="gatewayApp.khamchuabenhChucDanh.help.moTa" />
            </UncontrolledTooltip>
          </dt>
          <dd>{chucDanhEntity.moTa}</dd>
          <dt>
            <span id="ten">
              <Translate contentKey="gatewayApp.khamchuabenhChucDanh.ten">Ten</Translate>
            </span>
            <UncontrolledTooltip target="ten">
              <Translate contentKey="gatewayApp.khamchuabenhChucDanh.help.ten" />
            </UncontrolledTooltip>
          </dt>
          <dd>{chucDanhEntity.ten}</dd>
        </dl>
        <Button tag={Link} to="/chuc-danh" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/chuc-danh/${chucDanhEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ chucDanh }: IRootState) => ({
  chucDanhEntity: chucDanh.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ChucDanhDetail);
