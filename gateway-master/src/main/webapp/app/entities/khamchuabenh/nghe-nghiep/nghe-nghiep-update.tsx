import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './nghe-nghiep.reducer';
import { INgheNghiep } from 'app/shared/model/khamchuabenh/nghe-nghiep.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface INgheNghiepUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const NgheNghiepUpdate = (props: INgheNghiepUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { ngheNghiepEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/nghe-nghiep' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...ngheNghiepEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhNgheNghiep.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhNgheNghiep.home.createOrEditLabel">Create or edit a NgheNghiep</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : ngheNghiepEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="nghe-nghiep-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="nghe-nghiep-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="ma4069Label" for="nghe-nghiep-ma4069">
                  <Translate contentKey="gatewayApp.khamchuabenhNgheNghiep.ma4069">Ma 4069</Translate>
                </Label>
                <AvField
                  id="nghe-nghiep-ma4069"
                  type="string"
                  className="form-control"
                  name="ma4069"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="ma4069Label">
                  <Translate contentKey="gatewayApp.khamchuabenhNgheNghiep.help.ma4069" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ten4069Label" for="nghe-nghiep-ten4069">
                  <Translate contentKey="gatewayApp.khamchuabenhNgheNghiep.ten4069">Ten 4069</Translate>
                </Label>
                <AvField
                  id="nghe-nghiep-ten4069"
                  type="text"
                  name="ten4069"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="ten4069Label">
                  <Translate contentKey="gatewayApp.khamchuabenhNgheNghiep.help.ten4069" />
                </UncontrolledTooltip>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/nghe-nghiep" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  ngheNghiepEntity: storeState.ngheNghiep.entity,
  loading: storeState.ngheNghiep.loading,
  updating: storeState.ngheNghiep.updating,
  updateSuccess: storeState.ngheNghiep.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(NgheNghiepUpdate);
