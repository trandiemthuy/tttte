import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import NgheNghiep from './nghe-nghiep';
import NgheNghiepDetail from './nghe-nghiep-detail';
import NgheNghiepUpdate from './nghe-nghiep-update';
import NgheNghiepDeleteDialog from './nghe-nghiep-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={NgheNghiepDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={NgheNghiepUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={NgheNghiepUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={NgheNghiepDetail} />
      <ErrorBoundaryRoute path={match.url} component={NgheNghiep} />
    </Switch>
  </>
);

export default Routes;
