import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './phuong-xa.reducer';
import { IPhuongXa } from 'app/shared/model/khamchuabenh/phuong-xa.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPhuongXaDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PhuongXaDetail = (props: IPhuongXaDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { phuongXaEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.detail.title">PhuongXa</Translate> [<b>{phuongXaEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="cap">
              <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.cap">Cap</Translate>
            </span>
            <UncontrolledTooltip target="cap">
              <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.help.cap" />
            </UncontrolledTooltip>
          </dt>
          <dd>{phuongXaEntity.cap}</dd>
          <dt>
            <span id="guessPhrase">
              <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.guessPhrase">Guess Phrase</Translate>
            </span>
            <UncontrolledTooltip target="guessPhrase">
              <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.help.guessPhrase" />
            </UncontrolledTooltip>
          </dt>
          <dd>{phuongXaEntity.guessPhrase}</dd>
          <dt>
            <span id="ten">
              <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.ten">Ten</Translate>
            </span>
            <UncontrolledTooltip target="ten">
              <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.help.ten" />
            </UncontrolledTooltip>
          </dt>
          <dd>{phuongXaEntity.ten}</dd>
          <dt>
            <span id="tenKhongDau">
              <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.tenKhongDau">Ten Khong Dau</Translate>
            </span>
            <UncontrolledTooltip target="tenKhongDau">
              <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.help.tenKhongDau" />
            </UncontrolledTooltip>
          </dt>
          <dd>{phuongXaEntity.tenKhongDau}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.quanHuyen">Quan Huyen</Translate>
          </dt>
          <dd>{phuongXaEntity.quanHuyenId ? phuongXaEntity.quanHuyenId : ''}</dd>
        </dl>
        <Button tag={Link} to="/phuong-xa" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/phuong-xa/${phuongXaEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ phuongXa }: IRootState) => ({
  phuongXaEntity: phuongXa.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PhuongXaDetail);
