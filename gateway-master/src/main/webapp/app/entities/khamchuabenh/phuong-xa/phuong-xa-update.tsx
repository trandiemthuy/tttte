import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IQuanHuyen } from 'app/shared/model/khamchuabenh/quan-huyen.model';
import { getEntities as getQuanHuyens } from 'app/entities/khamchuabenh/quan-huyen/quan-huyen.reducer';
import { getEntity, updateEntity, createEntity, reset } from './phuong-xa.reducer';
import { IPhuongXa } from 'app/shared/model/khamchuabenh/phuong-xa.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPhuongXaUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PhuongXaUpdate = (props: IPhuongXaUpdateProps) => {
  const [quanHuyenId, setQuanHuyenId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { phuongXaEntity, quanHuyens, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/phuong-xa' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getQuanHuyens();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...phuongXaEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhPhuongXa.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.home.createOrEditLabel">Create or edit a PhuongXa</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : phuongXaEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="phuong-xa-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="phuong-xa-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="capLabel" for="phuong-xa-cap">
                  <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.cap">Cap</Translate>
                </Label>
                <AvField
                  id="phuong-xa-cap"
                  type="text"
                  name="cap"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="capLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.help.cap" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="guessPhraseLabel" for="phuong-xa-guessPhrase">
                  <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.guessPhrase">Guess Phrase</Translate>
                </Label>
                <AvField
                  id="phuong-xa-guessPhrase"
                  type="text"
                  name="guessPhrase"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="guessPhraseLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.help.guessPhrase" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenLabel" for="phuong-xa-ten">
                  <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.ten">Ten</Translate>
                </Label>
                <AvField
                  id="phuong-xa-ten"
                  type="text"
                  name="ten"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="tenLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.help.ten" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenKhongDauLabel" for="phuong-xa-tenKhongDau">
                  <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.tenKhongDau">Ten Khong Dau</Translate>
                </Label>
                <AvField
                  id="phuong-xa-tenKhongDau"
                  type="text"
                  name="tenKhongDau"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="tenKhongDauLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.help.tenKhongDau" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="phuong-xa-quanHuyen">
                  <Translate contentKey="gatewayApp.khamchuabenhPhuongXa.quanHuyen">Quan Huyen</Translate>
                </Label>
                <AvInput id="phuong-xa-quanHuyen" type="select" className="form-control" name="quanHuyenId" required>
                  {quanHuyens
                    ? quanHuyens.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/phuong-xa" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  quanHuyens: storeState.quanHuyen.entities,
  phuongXaEntity: storeState.phuongXa.entity,
  loading: storeState.phuongXa.loading,
  updating: storeState.phuongXa.updating,
  updateSuccess: storeState.phuongXa.updateSuccess
});

const mapDispatchToProps = {
  getQuanHuyens,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PhuongXaUpdate);
