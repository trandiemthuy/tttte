import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IChucDanh } from 'app/shared/model/khamchuabenh/chuc-danh.model';
import { getEntities as getChucDanhs } from 'app/entities/khamchuabenh/chuc-danh/chuc-danh.reducer';
import { IUserExtra } from 'app/shared/model/khamchuabenh/user-extra.model';
import { getEntities as getUserExtras } from 'app/entities/khamchuabenh/user-extra/user-extra.reducer';
import { IChucVu } from 'app/shared/model/khamchuabenh/chuc-vu.model';
import { getEntities as getChucVus } from 'app/entities/khamchuabenh/chuc-vu/chuc-vu.reducer';
import { getEntity, updateEntity, createEntity, reset } from './nhan-vien.reducer';
import { INhanVien } from 'app/shared/model/khamchuabenh/nhan-vien.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface INhanVienUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const NhanVienUpdate = (props: INhanVienUpdateProps) => {
  const [idschucVu, setIdschucVu] = useState([]);
  const [chucDanhId, setChucDanhId] = useState('0');
  const [userExtraId, setUserExtraId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { nhanVienEntity, chucDanhs, userExtras, chucVus, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/nhan-vien' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getChucDanhs();
    props.getUserExtras();
    props.getChucVus();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...nhanVienEntity,
        ...values,
        chucVus: mapIdList(values.chucVus)
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhNhanVien.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhNhanVien.home.createOrEditLabel">Create or edit a NhanVien</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : nhanVienEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="nhan-vien-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="nhan-vien-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="tenLabel" for="nhan-vien-ten">
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.ten">Ten</Translate>
                </Label>
                <AvField
                  id="nhan-vien-ten"
                  type="text"
                  name="ten"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 300, errorMessage: translate('entity.validation.maxlength', { max: 300 }) }
                  }}
                />
                <UncontrolledTooltip target="tenLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.help.ten" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="chungChiHanhNgheLabel" for="nhan-vien-chungChiHanhNghe">
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.chungChiHanhNghe">Chung Chi Hanh Nghe</Translate>
                </Label>
                <AvField
                  id="nhan-vien-chungChiHanhNghe"
                  type="text"
                  name="chungChiHanhNghe"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="chungChiHanhNgheLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.help.chungChiHanhNghe" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="cmndLabel" for="nhan-vien-cmnd">
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.cmnd">Cmnd</Translate>
                </Label>
                <AvField
                  id="nhan-vien-cmnd"
                  type="text"
                  name="cmnd"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 30, errorMessage: translate('entity.validation.maxlength', { max: 30 }) }
                  }}
                />
                <UncontrolledTooltip target="cmndLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.help.cmnd" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="gioiTinhLabel" for="nhan-vien-gioiTinh">
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.gioiTinh">Gioi Tinh</Translate>
                </Label>
                <AvField
                  id="nhan-vien-gioiTinh"
                  type="string"
                  className="form-control"
                  name="gioiTinh"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="gioiTinhLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.help.gioiTinh" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ngaySinhLabel" for="nhan-vien-ngaySinh">
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.ngaySinh">Ngay Sinh</Translate>
                </Label>
                <AvField
                  id="nhan-vien-ngaySinh"
                  type="date"
                  className="form-control"
                  name="ngaySinh"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') }
                  }}
                />
                <UncontrolledTooltip target="ngaySinhLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.help.ngaySinh" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="nhan-vien-chucDanh">
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.chucDanh">Chuc Danh</Translate>
                </Label>
                <AvInput id="nhan-vien-chucDanh" type="select" className="form-control" name="chucDanhId" required>
                  {chucDanhs
                    ? chucDanhs.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="nhan-vien-userExtra">
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.userExtra">User Extra</Translate>
                </Label>
                <AvInput id="nhan-vien-userExtra" type="select" className="form-control" name="userExtraId">
                  <option value="" key="0" />
                  {userExtras
                    ? userExtras.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="nhan-vien-chucVu">
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.chucVu">Chuc Vu</Translate>
                </Label>
                <AvInput
                  id="nhan-vien-chucVu"
                  type="select"
                  multiple
                  className="form-control"
                  name="chucVus"
                  value={nhanVienEntity.chucVus && nhanVienEntity.chucVus.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {chucVus
                    ? chucVus.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/nhan-vien" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  chucDanhs: storeState.chucDanh.entities,
  userExtras: storeState.userExtra.entities,
  chucVus: storeState.chucVu.entities,
  nhanVienEntity: storeState.nhanVien.entity,
  loading: storeState.nhanVien.loading,
  updating: storeState.nhanVien.updating,
  updateSuccess: storeState.nhanVien.updateSuccess
});

const mapDispatchToProps = {
  getChucDanhs,
  getUserExtras,
  getChucVus,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(NhanVienUpdate);
