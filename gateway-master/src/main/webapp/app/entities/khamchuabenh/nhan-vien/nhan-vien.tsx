import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './nhan-vien.reducer';
import { INhanVien } from 'app/shared/model/khamchuabenh/nhan-vien.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface INhanVienProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const NhanVien = (props: INhanVienProps) => {
  const [paginationState, setPaginationState] = useState(getSortState(props.location, ITEMS_PER_PAGE));

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  useEffect(() => {
    getAllEntities();
  }, []);

  const sortEntities = () => {
    getAllEntities();
    props.history.push(
      `${props.location.pathname}?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`
    );
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage
    });

  const { nhanVienList, match, totalItems } = props;
  return (
    <div>
      <h2 id="nhan-vien-heading">
        <Translate contentKey="gatewayApp.khamchuabenhNhanVien.home.title">Nhan Viens</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="gatewayApp.khamchuabenhNhanVien.home.createLabel">Create new Nhan Vien</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {nhanVienList && nhanVienList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ten')}>
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.ten">Ten</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('chungChiHanhNghe')}>
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.chungChiHanhNghe">Chung Chi Hanh Nghe</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('cmnd')}>
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.cmnd">Cmnd</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('gioiTinh')}>
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.gioiTinh">Gioi Tinh</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ngaySinh')}>
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.ngaySinh">Ngay Sinh</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.chucDanh">Chuc Danh</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhNhanVien.userExtra">User Extra</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {nhanVienList.map((nhanVien, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${nhanVien.id}`} color="link" size="sm">
                      {nhanVien.id}
                    </Button>
                  </td>
                  <td>{nhanVien.ten}</td>
                  <td>{nhanVien.chungChiHanhNghe}</td>
                  <td>{nhanVien.cmnd}</td>
                  <td>{nhanVien.gioiTinh}</td>
                  <td>
                    <TextFormat type="date" value={nhanVien.ngaySinh} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{nhanVien.chucDanhId ? <Link to={`chuc-danh/${nhanVien.chucDanhId}`}>{nhanVien.chucDanhId}</Link> : ''}</td>
                  <td>{nhanVien.userExtraId ? <Link to={`user-extra/${nhanVien.userExtraId}`}>{nhanVien.userExtraId}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${nhanVien.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${nhanVien.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="primary"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${nhanVien.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="danger"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          <div className="alert alert-warning">
            <Translate contentKey="gatewayApp.khamchuabenhNhanVien.home.notFound">No Nhan Viens found</Translate>
          </div>
        )}
      </div>
      <div className={nhanVienList && nhanVienList.length > 0 ? '' : 'd-none'}>
        <Row className="justify-content-center">
          <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} i18nEnabled />
        </Row>
        <Row className="justify-content-center">
          <JhiPagination
            activePage={paginationState.activePage}
            onSelect={handlePagination}
            maxButtons={5}
            itemsPerPage={paginationState.itemsPerPage}
            totalItems={props.totalItems}
          />
        </Row>
      </div>
    </div>
  );
};

const mapStateToProps = ({ nhanVien }: IRootState) => ({
  nhanVienList: nhanVien.entities,
  totalItems: nhanVien.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(NhanVien);
