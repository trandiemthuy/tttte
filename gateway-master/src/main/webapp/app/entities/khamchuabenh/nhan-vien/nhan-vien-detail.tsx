import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './nhan-vien.reducer';
import { INhanVien } from 'app/shared/model/khamchuabenh/nhan-vien.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface INhanVienDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const NhanVienDetail = (props: INhanVienDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { nhanVienEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhNhanVien.detail.title">NhanVien</Translate> [<b>{nhanVienEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="ten">
              <Translate contentKey="gatewayApp.khamchuabenhNhanVien.ten">Ten</Translate>
            </span>
            <UncontrolledTooltip target="ten">
              <Translate contentKey="gatewayApp.khamchuabenhNhanVien.help.ten" />
            </UncontrolledTooltip>
          </dt>
          <dd>{nhanVienEntity.ten}</dd>
          <dt>
            <span id="chungChiHanhNghe">
              <Translate contentKey="gatewayApp.khamchuabenhNhanVien.chungChiHanhNghe">Chung Chi Hanh Nghe</Translate>
            </span>
            <UncontrolledTooltip target="chungChiHanhNghe">
              <Translate contentKey="gatewayApp.khamchuabenhNhanVien.help.chungChiHanhNghe" />
            </UncontrolledTooltip>
          </dt>
          <dd>{nhanVienEntity.chungChiHanhNghe}</dd>
          <dt>
            <span id="cmnd">
              <Translate contentKey="gatewayApp.khamchuabenhNhanVien.cmnd">Cmnd</Translate>
            </span>
            <UncontrolledTooltip target="cmnd">
              <Translate contentKey="gatewayApp.khamchuabenhNhanVien.help.cmnd" />
            </UncontrolledTooltip>
          </dt>
          <dd>{nhanVienEntity.cmnd}</dd>
          <dt>
            <span id="gioiTinh">
              <Translate contentKey="gatewayApp.khamchuabenhNhanVien.gioiTinh">Gioi Tinh</Translate>
            </span>
            <UncontrolledTooltip target="gioiTinh">
              <Translate contentKey="gatewayApp.khamchuabenhNhanVien.help.gioiTinh" />
            </UncontrolledTooltip>
          </dt>
          <dd>{nhanVienEntity.gioiTinh}</dd>
          <dt>
            <span id="ngaySinh">
              <Translate contentKey="gatewayApp.khamchuabenhNhanVien.ngaySinh">Ngay Sinh</Translate>
            </span>
            <UncontrolledTooltip target="ngaySinh">
              <Translate contentKey="gatewayApp.khamchuabenhNhanVien.help.ngaySinh" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            <TextFormat value={nhanVienEntity.ngaySinh} type="date" format={APP_LOCAL_DATE_FORMAT} />
          </dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhNhanVien.chucDanh">Chuc Danh</Translate>
          </dt>
          <dd>{nhanVienEntity.chucDanhId ? nhanVienEntity.chucDanhId : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhNhanVien.userExtra">User Extra</Translate>
          </dt>
          <dd>{nhanVienEntity.userExtraId ? nhanVienEntity.userExtraId : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhNhanVien.chucVu">Chuc Vu</Translate>
          </dt>
          <dd>
            {nhanVienEntity.chucVus
              ? nhanVienEntity.chucVus.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.id}</a>
                    {i === nhanVienEntity.chucVus.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
        </dl>
        <Button tag={Link} to="/nhan-vien" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/nhan-vien/${nhanVienEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ nhanVien }: IRootState) => ({
  nhanVienEntity: nhanVien.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(NhanVienDetail);
