import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUserExtra } from 'app/shared/model/khamchuabenh/user-extra.model';
import { getEntities as getUserExtras } from 'app/entities/khamchuabenh/user-extra/user-extra.reducer';
import { getEntity, updateEntity, createEntity, reset } from './menu.reducer';
import { IMenu } from 'app/shared/model/khamchuabenh/menu.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IMenuUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const MenuUpdate = (props: IMenuUpdateProps) => {
  const [idsuser, setIdsuser] = useState([]);
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { menuEntity, userExtras, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/menu' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getUserExtras();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...menuEntity,
        ...values,
        users: mapIdList(values.users)
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhMenu.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhMenu.home.createOrEditLabel">Create or edit a Menu</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : menuEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="menu-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="menu-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup check>
                <Label id="enabledLabel">
                  <AvInput id="menu-enabled" type="checkbox" className="form-check-input" name="enabled" />
                  <Translate contentKey="gatewayApp.khamchuabenhMenu.enabled">Enabled</Translate>
                </Label>
                <UncontrolledTooltip target="enabledLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhMenu.help.enabled" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="levelLabel" for="menu-level">
                  <Translate contentKey="gatewayApp.khamchuabenhMenu.level">Level</Translate>
                </Label>
                <AvField
                  id="menu-level"
                  type="string"
                  className="form-control"
                  name="level"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="levelLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhMenu.help.level" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="nameLabel" for="menu-name">
                  <Translate contentKey="gatewayApp.khamchuabenhMenu.name">Name</Translate>
                </Label>
                <AvField
                  id="menu-name"
                  type="text"
                  name="name"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="nameLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhMenu.help.name" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="uriLabel" for="menu-uri">
                  <Translate contentKey="gatewayApp.khamchuabenhMenu.uri">Uri</Translate>
                </Label>
                <AvField
                  id="menu-uri"
                  type="text"
                  name="uri"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="uriLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhMenu.help.uri" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="parentLabel" for="menu-parent">
                  <Translate contentKey="gatewayApp.khamchuabenhMenu.parent">Parent</Translate>
                </Label>
                <AvField id="menu-parent" type="string" className="form-control" name="parent" />
                <UncontrolledTooltip target="parentLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhMenu.help.parent" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="donViIdLabel" for="menu-donViId">
                  <Translate contentKey="gatewayApp.khamchuabenhMenu.donViId">Don Vi Id</Translate>
                </Label>
                <AvField
                  id="menu-donViId"
                  type="string"
                  className="form-control"
                  name="donViId"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="donViIdLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhMenu.help.donViId" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="menu-user">
                  <Translate contentKey="gatewayApp.khamchuabenhMenu.user">User</Translate>
                </Label>
                <AvInput
                  id="menu-user"
                  type="select"
                  multiple
                  className="form-control"
                  name="users"
                  value={menuEntity.users && menuEntity.users.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {userExtras
                    ? userExtras.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/menu" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  userExtras: storeState.userExtra.entities,
  menuEntity: storeState.menu.entity,
  loading: storeState.menu.loading,
  updating: storeState.menu.updating,
  updateSuccess: storeState.menu.updateSuccess
});

const mapDispatchToProps = {
  getUserExtras,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(MenuUpdate);
