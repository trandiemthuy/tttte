import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './menu.reducer';
import { IMenu } from 'app/shared/model/khamchuabenh/menu.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IMenuDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const MenuDetail = (props: IMenuDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { menuEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhMenu.detail.title">Menu</Translate> [<b>{menuEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhMenu.enabled">Enabled</Translate>
            </span>
            <UncontrolledTooltip target="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhMenu.help.enabled" />
            </UncontrolledTooltip>
          </dt>
          <dd>{menuEntity.enabled ? 'true' : 'false'}</dd>
          <dt>
            <span id="level">
              <Translate contentKey="gatewayApp.khamchuabenhMenu.level">Level</Translate>
            </span>
            <UncontrolledTooltip target="level">
              <Translate contentKey="gatewayApp.khamchuabenhMenu.help.level" />
            </UncontrolledTooltip>
          </dt>
          <dd>{menuEntity.level}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="gatewayApp.khamchuabenhMenu.name">Name</Translate>
            </span>
            <UncontrolledTooltip target="name">
              <Translate contentKey="gatewayApp.khamchuabenhMenu.help.name" />
            </UncontrolledTooltip>
          </dt>
          <dd>{menuEntity.name}</dd>
          <dt>
            <span id="uri">
              <Translate contentKey="gatewayApp.khamchuabenhMenu.uri">Uri</Translate>
            </span>
            <UncontrolledTooltip target="uri">
              <Translate contentKey="gatewayApp.khamchuabenhMenu.help.uri" />
            </UncontrolledTooltip>
          </dt>
          <dd>{menuEntity.uri}</dd>
          <dt>
            <span id="parent">
              <Translate contentKey="gatewayApp.khamchuabenhMenu.parent">Parent</Translate>
            </span>
            <UncontrolledTooltip target="parent">
              <Translate contentKey="gatewayApp.khamchuabenhMenu.help.parent" />
            </UncontrolledTooltip>
          </dt>
          <dd>{menuEntity.parent}</dd>
          <dt>
            <span id="donViId">
              <Translate contentKey="gatewayApp.khamchuabenhMenu.donViId">Don Vi Id</Translate>
            </span>
            <UncontrolledTooltip target="donViId">
              <Translate contentKey="gatewayApp.khamchuabenhMenu.help.donViId" />
            </UncontrolledTooltip>
          </dt>
          <dd>{menuEntity.donViId}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhMenu.user">User</Translate>
          </dt>
          <dd>
            {menuEntity.users
              ? menuEntity.users.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.id}</a>
                    {i === menuEntity.users.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
        </dl>
        <Button tag={Link} to="/menu" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/menu/${menuEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ menu }: IRootState) => ({
  menuEntity: menu.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(MenuDetail);
