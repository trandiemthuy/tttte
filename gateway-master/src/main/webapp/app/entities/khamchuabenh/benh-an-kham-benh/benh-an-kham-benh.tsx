import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './benh-an-kham-benh.reducer';
import { IBenhAnKhamBenh } from 'app/shared/model/khamchuabenh/benh-an-kham-benh.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IBenhAnKhamBenhProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const BenhAnKhamBenh = (props: IBenhAnKhamBenhProps) => {
  const [paginationState, setPaginationState] = useState(getSortState(props.location, ITEMS_PER_PAGE));

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  useEffect(() => {
    getAllEntities();
  }, []);

  const sortEntities = () => {
    getAllEntities();
    props.history.push(
      `${props.location.pathname}?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`
    );
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage
    });

  const { benhAnKhamBenhList, match, totalItems } = props;
  return (
    <div>
      <h2 id="benh-an-kham-benh-heading">
        <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.home.title">Benh An Kham Benhs</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.home.createLabel">Create new Benh An Kham Benh</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {benhAnKhamBenhList && benhAnKhamBenhList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('benhNhanCu')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.benhNhanCu">Benh Nhan Cu</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('canhBao')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.canhBao">Canh Bao</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('capCuu')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.capCuu">Cap Cuu</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('chiCoNamSinh')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.chiCoNamSinh">Chi Co Nam Sinh</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('cmndBenhNhan')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.cmndBenhNhan">Cmnd Benh Nhan</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('cmndNguoiNha')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.cmndNguoiNha">Cmnd Nguoi Nha</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('coBaoHiem')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.coBaoHiem">Co Bao Hiem</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('diaChi')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.diaChi">Dia Chi</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('email')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.email">Email</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('gioiTinh')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.gioiTinh">Gioi Tinh</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('khangThe')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.khangThe">Khang The</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('lanKhamTrongNgay')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.lanKhamTrongNgay">Lan Kham Trong Ngay</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('loaiGiayToTreEm')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.loaiGiayToTreEm">Loai Giay To Tre Em</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ngayCapCmnd')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.ngayCapCmnd">Ngay Cap Cmnd</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ngayMienCungChiTra')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.ngayMienCungChiTra">Ngay Mien Cung Chi Tra</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ngaySinh')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.ngaySinh">Ngay Sinh</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('nhomMau')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.nhomMau">Nhom Mau</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('noiCapCmnd')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.noiCapCmnd">Noi Cap Cmnd</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('noiLamViec')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.noiLamViec">Noi Lam Viec</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('phone')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.phone">Phone</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('phoneNguoiNha')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.phoneNguoiNha">Phone Nguoi Nha</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('quocTich')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.quocTich">Quoc Tich</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('tenBenhNhan')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tenBenhNhan">Ten Benh Nhan</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('tenNguoiNha')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tenNguoiNha">Ten Nguoi Nha</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('thangTuoi')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.thangTuoi">Thang Tuoi</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('thoiGianTiepNhan')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.thoiGianTiepNhan">Thoi Gian Tiep Nhan</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('tuoi')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tuoi">Tuoi</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('uuTien')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.uuTien">Uu Tien</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('uuid')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.uuid">Uuid</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('trangThai')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.trangThai">Trang Thai</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('maKhoaHoanTatKham')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.maKhoaHoanTatKham">Ma Khoa Hoan Tat Kham</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('maPhongHoanTatKham')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.maPhongHoanTatKham">Ma Phong Hoan Tat Kham</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('tenKhoaHoanTatKham')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tenKhoaHoanTatKham">Ten Khoa Hoan Tat Kham</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('tenPhongHoanTatKham')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tenPhongHoanTatKham">Ten Phong Hoan Tat Kham</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('thoiGianHoanTatKham')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.thoiGianHoanTatKham">Thoi Gian Hoan Tat Kham</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('soBenhAn')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.soBenhAn">So Benh An</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('soBenhAnKhoa')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.soBenhAnKhoa">So Benh An Khoa</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('nam')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.nam">Nam</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.benhNhan">Benh Nhan</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.donVi">Don Vi</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.nhanVienTiepNhan">Nhan Vien Tiep Nhan</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.phongTiepNhan">Phong Tiep Nhan</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {benhAnKhamBenhList.map((benhAnKhamBenh, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${benhAnKhamBenh.id}`} color="link" size="sm">
                      {benhAnKhamBenh.id}
                    </Button>
                  </td>
                  <td>{benhAnKhamBenh.benhNhanCu ? 'true' : 'false'}</td>
                  <td>{benhAnKhamBenh.canhBao ? 'true' : 'false'}</td>
                  <td>{benhAnKhamBenh.capCuu ? 'true' : 'false'}</td>
                  <td>{benhAnKhamBenh.chiCoNamSinh ? 'true' : 'false'}</td>
                  <td>{benhAnKhamBenh.cmndBenhNhan}</td>
                  <td>{benhAnKhamBenh.cmndNguoiNha}</td>
                  <td>{benhAnKhamBenh.coBaoHiem ? 'true' : 'false'}</td>
                  <td>{benhAnKhamBenh.diaChi}</td>
                  <td>{benhAnKhamBenh.email}</td>
                  <td>{benhAnKhamBenh.gioiTinh}</td>
                  <td>{benhAnKhamBenh.khangThe}</td>
                  <td>{benhAnKhamBenh.lanKhamTrongNgay}</td>
                  <td>{benhAnKhamBenh.loaiGiayToTreEm}</td>
                  <td>
                    <TextFormat type="date" value={benhAnKhamBenh.ngayCapCmnd} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>
                    <TextFormat type="date" value={benhAnKhamBenh.ngayMienCungChiTra} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>
                    <TextFormat type="date" value={benhAnKhamBenh.ngaySinh} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{benhAnKhamBenh.nhomMau}</td>
                  <td>{benhAnKhamBenh.noiCapCmnd}</td>
                  <td>{benhAnKhamBenh.noiLamViec}</td>
                  <td>{benhAnKhamBenh.phone}</td>
                  <td>{benhAnKhamBenh.phoneNguoiNha}</td>
                  <td>{benhAnKhamBenh.quocTich}</td>
                  <td>{benhAnKhamBenh.tenBenhNhan}</td>
                  <td>{benhAnKhamBenh.tenNguoiNha}</td>
                  <td>{benhAnKhamBenh.thangTuoi}</td>
                  <td>
                    <TextFormat type="date" value={benhAnKhamBenh.thoiGianTiepNhan} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{benhAnKhamBenh.tuoi}</td>
                  <td>{benhAnKhamBenh.uuTien}</td>
                  <td>{benhAnKhamBenh.uuid}</td>
                  <td>{benhAnKhamBenh.trangThai}</td>
                  <td>{benhAnKhamBenh.maKhoaHoanTatKham}</td>
                  <td>{benhAnKhamBenh.maPhongHoanTatKham}</td>
                  <td>{benhAnKhamBenh.tenKhoaHoanTatKham}</td>
                  <td>{benhAnKhamBenh.tenPhongHoanTatKham}</td>
                  <td>
                    <TextFormat type="date" value={benhAnKhamBenh.thoiGianHoanTatKham} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{benhAnKhamBenh.soBenhAn}</td>
                  <td>{benhAnKhamBenh.soBenhAnKhoa}</td>
                  <td>{benhAnKhamBenh.nam}</td>
                  <td>
                    {benhAnKhamBenh.benhNhanId ? (
                      <Link to={`benh-nhan/${benhAnKhamBenh.benhNhanId}`}>{benhAnKhamBenh.benhNhanId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>{benhAnKhamBenh.donViId ? <Link to={`don-vi/${benhAnKhamBenh.donViId}`}>{benhAnKhamBenh.donViId}</Link> : ''}</td>
                  <td>
                    {benhAnKhamBenh.nhanVienTiepNhanId ? (
                      <Link to={`nhan-vien/${benhAnKhamBenh.nhanVienTiepNhanId}`}>{benhAnKhamBenh.nhanVienTiepNhanId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    {benhAnKhamBenh.phongTiepNhanId ? (
                      <Link to={`phong/${benhAnKhamBenh.phongTiepNhanId}`}>{benhAnKhamBenh.phongTiepNhanId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${benhAnKhamBenh.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${benhAnKhamBenh.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="primary"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${benhAnKhamBenh.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="danger"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          <div className="alert alert-warning">
            <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.home.notFound">No Benh An Kham Benhs found</Translate>
          </div>
        )}
      </div>
      <div className={benhAnKhamBenhList && benhAnKhamBenhList.length > 0 ? '' : 'd-none'}>
        <Row className="justify-content-center">
          <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} i18nEnabled />
        </Row>
        <Row className="justify-content-center">
          <JhiPagination
            activePage={paginationState.activePage}
            onSelect={handlePagination}
            maxButtons={5}
            itemsPerPage={paginationState.itemsPerPage}
            totalItems={props.totalItems}
          />
        </Row>
      </div>
    </div>
  );
};

const mapStateToProps = ({ benhAnKhamBenh }: IRootState) => ({
  benhAnKhamBenhList: benhAnKhamBenh.entities,
  totalItems: benhAnKhamBenh.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(BenhAnKhamBenh);
