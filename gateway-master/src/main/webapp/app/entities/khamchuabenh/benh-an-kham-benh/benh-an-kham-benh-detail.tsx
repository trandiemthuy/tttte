import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './benh-an-kham-benh.reducer';
import { IBenhAnKhamBenh } from 'app/shared/model/khamchuabenh/benh-an-kham-benh.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IBenhAnKhamBenhDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const BenhAnKhamBenhDetail = (props: IBenhAnKhamBenhDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { benhAnKhamBenhEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.detail.title">BenhAnKhamBenh</Translate> [
          <b>{benhAnKhamBenhEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="benhNhanCu">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.benhNhanCu">Benh Nhan Cu</Translate>
            </span>
            <UncontrolledTooltip target="benhNhanCu">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.benhNhanCu" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.benhNhanCu ? 'true' : 'false'}</dd>
          <dt>
            <span id="canhBao">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.canhBao">Canh Bao</Translate>
            </span>
            <UncontrolledTooltip target="canhBao">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.canhBao" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.canhBao ? 'true' : 'false'}</dd>
          <dt>
            <span id="capCuu">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.capCuu">Cap Cuu</Translate>
            </span>
            <UncontrolledTooltip target="capCuu">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.capCuu" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.capCuu ? 'true' : 'false'}</dd>
          <dt>
            <span id="chiCoNamSinh">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.chiCoNamSinh">Chi Co Nam Sinh</Translate>
            </span>
            <UncontrolledTooltip target="chiCoNamSinh">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.chiCoNamSinh" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.chiCoNamSinh ? 'true' : 'false'}</dd>
          <dt>
            <span id="cmndBenhNhan">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.cmndBenhNhan">Cmnd Benh Nhan</Translate>
            </span>
            <UncontrolledTooltip target="cmndBenhNhan">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.cmndBenhNhan" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.cmndBenhNhan}</dd>
          <dt>
            <span id="cmndNguoiNha">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.cmndNguoiNha">Cmnd Nguoi Nha</Translate>
            </span>
            <UncontrolledTooltip target="cmndNguoiNha">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.cmndNguoiNha" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.cmndNguoiNha}</dd>
          <dt>
            <span id="coBaoHiem">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.coBaoHiem">Co Bao Hiem</Translate>
            </span>
            <UncontrolledTooltip target="coBaoHiem">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.coBaoHiem" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.coBaoHiem ? 'true' : 'false'}</dd>
          <dt>
            <span id="diaChi">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.diaChi">Dia Chi</Translate>
            </span>
            <UncontrolledTooltip target="diaChi">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.diaChi" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.diaChi}</dd>
          <dt>
            <span id="email">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.email">Email</Translate>
            </span>
            <UncontrolledTooltip target="email">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.email" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.email}</dd>
          <dt>
            <span id="gioiTinh">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.gioiTinh">Gioi Tinh</Translate>
            </span>
            <UncontrolledTooltip target="gioiTinh">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.gioiTinh" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.gioiTinh}</dd>
          <dt>
            <span id="khangThe">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.khangThe">Khang The</Translate>
            </span>
            <UncontrolledTooltip target="khangThe">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.khangThe" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.khangThe}</dd>
          <dt>
            <span id="lanKhamTrongNgay">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.lanKhamTrongNgay">Lan Kham Trong Ngay</Translate>
            </span>
            <UncontrolledTooltip target="lanKhamTrongNgay">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.lanKhamTrongNgay" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.lanKhamTrongNgay}</dd>
          <dt>
            <span id="loaiGiayToTreEm">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.loaiGiayToTreEm">Loai Giay To Tre Em</Translate>
            </span>
            <UncontrolledTooltip target="loaiGiayToTreEm">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.loaiGiayToTreEm" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.loaiGiayToTreEm}</dd>
          <dt>
            <span id="ngayCapCmnd">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.ngayCapCmnd">Ngay Cap Cmnd</Translate>
            </span>
            <UncontrolledTooltip target="ngayCapCmnd">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.ngayCapCmnd" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            <TextFormat value={benhAnKhamBenhEntity.ngayCapCmnd} type="date" format={APP_LOCAL_DATE_FORMAT} />
          </dd>
          <dt>
            <span id="ngayMienCungChiTra">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.ngayMienCungChiTra">Ngay Mien Cung Chi Tra</Translate>
            </span>
            <UncontrolledTooltip target="ngayMienCungChiTra">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.ngayMienCungChiTra" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            <TextFormat value={benhAnKhamBenhEntity.ngayMienCungChiTra} type="date" format={APP_LOCAL_DATE_FORMAT} />
          </dd>
          <dt>
            <span id="ngaySinh">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.ngaySinh">Ngay Sinh</Translate>
            </span>
            <UncontrolledTooltip target="ngaySinh">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.ngaySinh" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            <TextFormat value={benhAnKhamBenhEntity.ngaySinh} type="date" format={APP_LOCAL_DATE_FORMAT} />
          </dd>
          <dt>
            <span id="nhomMau">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.nhomMau">Nhom Mau</Translate>
            </span>
            <UncontrolledTooltip target="nhomMau">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.nhomMau" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.nhomMau}</dd>
          <dt>
            <span id="noiCapCmnd">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.noiCapCmnd">Noi Cap Cmnd</Translate>
            </span>
            <UncontrolledTooltip target="noiCapCmnd">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.noiCapCmnd" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.noiCapCmnd}</dd>
          <dt>
            <span id="noiLamViec">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.noiLamViec">Noi Lam Viec</Translate>
            </span>
            <UncontrolledTooltip target="noiLamViec">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.noiLamViec" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.noiLamViec}</dd>
          <dt>
            <span id="phone">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.phone">Phone</Translate>
            </span>
            <UncontrolledTooltip target="phone">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.phone" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.phone}</dd>
          <dt>
            <span id="phoneNguoiNha">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.phoneNguoiNha">Phone Nguoi Nha</Translate>
            </span>
            <UncontrolledTooltip target="phoneNguoiNha">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.phoneNguoiNha" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.phoneNguoiNha}</dd>
          <dt>
            <span id="quocTich">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.quocTich">Quoc Tich</Translate>
            </span>
            <UncontrolledTooltip target="quocTich">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.quocTich" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.quocTich}</dd>
          <dt>
            <span id="tenBenhNhan">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tenBenhNhan">Ten Benh Nhan</Translate>
            </span>
            <UncontrolledTooltip target="tenBenhNhan">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.tenBenhNhan" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.tenBenhNhan}</dd>
          <dt>
            <span id="tenNguoiNha">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tenNguoiNha">Ten Nguoi Nha</Translate>
            </span>
            <UncontrolledTooltip target="tenNguoiNha">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.tenNguoiNha" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.tenNguoiNha}</dd>
          <dt>
            <span id="thangTuoi">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.thangTuoi">Thang Tuoi</Translate>
            </span>
            <UncontrolledTooltip target="thangTuoi">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.thangTuoi" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.thangTuoi}</dd>
          <dt>
            <span id="thoiGianTiepNhan">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.thoiGianTiepNhan">Thoi Gian Tiep Nhan</Translate>
            </span>
            <UncontrolledTooltip target="thoiGianTiepNhan">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.thoiGianTiepNhan" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            <TextFormat value={benhAnKhamBenhEntity.thoiGianTiepNhan} type="date" format={APP_LOCAL_DATE_FORMAT} />
          </dd>
          <dt>
            <span id="tuoi">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tuoi">Tuoi</Translate>
            </span>
            <UncontrolledTooltip target="tuoi">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.tuoi" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.tuoi}</dd>
          <dt>
            <span id="uuTien">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.uuTien">Uu Tien</Translate>
            </span>
            <UncontrolledTooltip target="uuTien">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.uuTien" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.uuTien}</dd>
          <dt>
            <span id="uuid">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.uuid">Uuid</Translate>
            </span>
            <UncontrolledTooltip target="uuid">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.uuid" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.uuid}</dd>
          <dt>
            <span id="trangThai">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.trangThai">Trang Thai</Translate>
            </span>
            <UncontrolledTooltip target="trangThai">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.trangThai" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.trangThai}</dd>
          <dt>
            <span id="maKhoaHoanTatKham">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.maKhoaHoanTatKham">Ma Khoa Hoan Tat Kham</Translate>
            </span>
            <UncontrolledTooltip target="maKhoaHoanTatKham">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.maKhoaHoanTatKham" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.maKhoaHoanTatKham}</dd>
          <dt>
            <span id="maPhongHoanTatKham">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.maPhongHoanTatKham">Ma Phong Hoan Tat Kham</Translate>
            </span>
            <UncontrolledTooltip target="maPhongHoanTatKham">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.maPhongHoanTatKham" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.maPhongHoanTatKham}</dd>
          <dt>
            <span id="tenKhoaHoanTatKham">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tenKhoaHoanTatKham">Ten Khoa Hoan Tat Kham</Translate>
            </span>
            <UncontrolledTooltip target="tenKhoaHoanTatKham">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.tenKhoaHoanTatKham" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.tenKhoaHoanTatKham}</dd>
          <dt>
            <span id="tenPhongHoanTatKham">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tenPhongHoanTatKham">Ten Phong Hoan Tat Kham</Translate>
            </span>
            <UncontrolledTooltip target="tenPhongHoanTatKham">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.tenPhongHoanTatKham" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.tenPhongHoanTatKham}</dd>
          <dt>
            <span id="thoiGianHoanTatKham">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.thoiGianHoanTatKham">Thoi Gian Hoan Tat Kham</Translate>
            </span>
            <UncontrolledTooltip target="thoiGianHoanTatKham">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.thoiGianHoanTatKham" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            <TextFormat value={benhAnKhamBenhEntity.thoiGianHoanTatKham} type="date" format={APP_LOCAL_DATE_FORMAT} />
          </dd>
          <dt>
            <span id="soBenhAn">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.soBenhAn">So Benh An</Translate>
            </span>
            <UncontrolledTooltip target="soBenhAn">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.soBenhAn" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.soBenhAn}</dd>
          <dt>
            <span id="soBenhAnKhoa">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.soBenhAnKhoa">So Benh An Khoa</Translate>
            </span>
            <UncontrolledTooltip target="soBenhAnKhoa">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.soBenhAnKhoa" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhAnKhamBenhEntity.soBenhAnKhoa}</dd>
          <dt>
            <span id="nam">
              <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.nam">Nam</Translate>
            </span>
          </dt>
          <dd>{benhAnKhamBenhEntity.nam}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.benhNhan">Benh Nhan</Translate>
          </dt>
          <dd>{benhAnKhamBenhEntity.benhNhanId ? benhAnKhamBenhEntity.benhNhanId : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.donVi">Don Vi</Translate>
          </dt>
          <dd>{benhAnKhamBenhEntity.donViId ? benhAnKhamBenhEntity.donViId : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.nhanVienTiepNhan">Nhan Vien Tiep Nhan</Translate>
          </dt>
          <dd>{benhAnKhamBenhEntity.nhanVienTiepNhanId ? benhAnKhamBenhEntity.nhanVienTiepNhanId : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.phongTiepNhan">Phong Tiep Nhan</Translate>
          </dt>
          <dd>{benhAnKhamBenhEntity.phongTiepNhanId ? benhAnKhamBenhEntity.phongTiepNhanId : ''}</dd>
        </dl>
        <Button tag={Link} to="/benh-an-kham-benh" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/benh-an-kham-benh/${benhAnKhamBenhEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ benhAnKhamBenh }: IRootState) => ({
  benhAnKhamBenhEntity: benhAnKhamBenh.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(BenhAnKhamBenhDetail);
