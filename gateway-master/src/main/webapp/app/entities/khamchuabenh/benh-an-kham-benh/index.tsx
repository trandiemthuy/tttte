import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import BenhAnKhamBenh from './benh-an-kham-benh';
import BenhAnKhamBenhDetail from './benh-an-kham-benh-detail';
import BenhAnKhamBenhUpdate from './benh-an-kham-benh-update';
import BenhAnKhamBenhDeleteDialog from './benh-an-kham-benh-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={BenhAnKhamBenhDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={BenhAnKhamBenhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={BenhAnKhamBenhUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={BenhAnKhamBenhDetail} />
      <ErrorBoundaryRoute path={match.url} component={BenhAnKhamBenh} />
    </Switch>
  </>
);

export default Routes;
