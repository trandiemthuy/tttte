import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IBenhAnKhamBenh, defaultValue } from 'app/shared/model/khamchuabenh/benh-an-kham-benh.model';

export const ACTION_TYPES = {
  FETCH_BENHANKHAMBENH_LIST: 'benhAnKhamBenh/FETCH_BENHANKHAMBENH_LIST',
  FETCH_BENHANKHAMBENH: 'benhAnKhamBenh/FETCH_BENHANKHAMBENH',
  CREATE_BENHANKHAMBENH: 'benhAnKhamBenh/CREATE_BENHANKHAMBENH',
  UPDATE_BENHANKHAMBENH: 'benhAnKhamBenh/UPDATE_BENHANKHAMBENH',
  DELETE_BENHANKHAMBENH: 'benhAnKhamBenh/DELETE_BENHANKHAMBENH',
  RESET: 'benhAnKhamBenh/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IBenhAnKhamBenh>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type BenhAnKhamBenhState = Readonly<typeof initialState>;

// Reducer

export default (state: BenhAnKhamBenhState = initialState, action): BenhAnKhamBenhState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_BENHANKHAMBENH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_BENHANKHAMBENH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_BENHANKHAMBENH):
    case REQUEST(ACTION_TYPES.UPDATE_BENHANKHAMBENH):
    case REQUEST(ACTION_TYPES.DELETE_BENHANKHAMBENH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_BENHANKHAMBENH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_BENHANKHAMBENH):
    case FAILURE(ACTION_TYPES.CREATE_BENHANKHAMBENH):
    case FAILURE(ACTION_TYPES.UPDATE_BENHANKHAMBENH):
    case FAILURE(ACTION_TYPES.DELETE_BENHANKHAMBENH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_BENHANKHAMBENH_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_BENHANKHAMBENH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_BENHANKHAMBENH):
    case SUCCESS(ACTION_TYPES.UPDATE_BENHANKHAMBENH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_BENHANKHAMBENH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/benh-an-kham-benhs';

// Actions

export const getEntities: ICrudGetAllAction<IBenhAnKhamBenh> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_BENHANKHAMBENH_LIST,
    payload: axios.get<IBenhAnKhamBenh>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IBenhAnKhamBenh> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_BENHANKHAMBENH,
    payload: axios.get<IBenhAnKhamBenh>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IBenhAnKhamBenh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_BENHANKHAMBENH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IBenhAnKhamBenh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_BENHANKHAMBENH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IBenhAnKhamBenh> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_BENHANKHAMBENH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
