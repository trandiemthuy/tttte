import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IBenhNhan } from 'app/shared/model/khamchuabenh/benh-nhan.model';
import { getEntities as getBenhNhans } from 'app/entities/khamchuabenh/benh-nhan/benh-nhan.reducer';
import { IDonVi } from 'app/shared/model/khamchuabenh/don-vi.model';
import { getEntities as getDonVis } from 'app/entities/khamchuabenh/don-vi/don-vi.reducer';
import { INhanVien } from 'app/shared/model/khamchuabenh/nhan-vien.model';
import { getEntities as getNhanViens } from 'app/entities/khamchuabenh/nhan-vien/nhan-vien.reducer';
import { IPhong } from 'app/shared/model/khamchuabenh/phong.model';
import { getEntities as getPhongs } from 'app/entities/khamchuabenh/phong/phong.reducer';
import { getEntity, updateEntity, createEntity, reset } from './benh-an-kham-benh.reducer';
import { IBenhAnKhamBenh } from 'app/shared/model/khamchuabenh/benh-an-kham-benh.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IBenhAnKhamBenhUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const BenhAnKhamBenhUpdate = (props: IBenhAnKhamBenhUpdateProps) => {
  const [benhNhanId, setBenhNhanId] = useState('0');
  const [donViId, setDonViId] = useState('0');
  const [nhanVienTiepNhanId, setNhanVienTiepNhanId] = useState('0');
  const [phongTiepNhanId, setPhongTiepNhanId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { benhAnKhamBenhEntity, benhNhans, donVis, nhanViens, phongs, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/benh-an-kham-benh' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getBenhNhans();
    props.getDonVis();
    props.getNhanViens();
    props.getPhongs();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...benhAnKhamBenhEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhBenhAnKhamBenh.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.home.createOrEditLabel">Create or edit a BenhAnKhamBenh</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : benhAnKhamBenhEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="benh-an-kham-benh-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="benh-an-kham-benh-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup check>
                <Label id="benhNhanCuLabel">
                  <AvInput id="benh-an-kham-benh-benhNhanCu" type="checkbox" className="form-check-input" name="benhNhanCu" />
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.benhNhanCu">Benh Nhan Cu</Translate>
                </Label>
                <UncontrolledTooltip target="benhNhanCuLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.benhNhanCu" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup check>
                <Label id="canhBaoLabel">
                  <AvInput id="benh-an-kham-benh-canhBao" type="checkbox" className="form-check-input" name="canhBao" />
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.canhBao">Canh Bao</Translate>
                </Label>
                <UncontrolledTooltip target="canhBaoLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.canhBao" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup check>
                <Label id="capCuuLabel">
                  <AvInput id="benh-an-kham-benh-capCuu" type="checkbox" className="form-check-input" name="capCuu" />
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.capCuu">Cap Cuu</Translate>
                </Label>
                <UncontrolledTooltip target="capCuuLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.capCuu" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup check>
                <Label id="chiCoNamSinhLabel">
                  <AvInput id="benh-an-kham-benh-chiCoNamSinh" type="checkbox" className="form-check-input" name="chiCoNamSinh" />
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.chiCoNamSinh">Chi Co Nam Sinh</Translate>
                </Label>
                <UncontrolledTooltip target="chiCoNamSinhLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.chiCoNamSinh" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="cmndBenhNhanLabel" for="benh-an-kham-benh-cmndBenhNhan">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.cmndBenhNhan">Cmnd Benh Nhan</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-cmndBenhNhan"
                  type="text"
                  name="cmndBenhNhan"
                  validate={{
                    maxLength: { value: 20, errorMessage: translate('entity.validation.maxlength', { max: 20 }) }
                  }}
                />
                <UncontrolledTooltip target="cmndBenhNhanLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.cmndBenhNhan" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="cmndNguoiNhaLabel" for="benh-an-kham-benh-cmndNguoiNha">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.cmndNguoiNha">Cmnd Nguoi Nha</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-cmndNguoiNha"
                  type="text"
                  name="cmndNguoiNha"
                  validate={{
                    maxLength: { value: 20, errorMessage: translate('entity.validation.maxlength', { max: 20 }) }
                  }}
                />
                <UncontrolledTooltip target="cmndNguoiNhaLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.cmndNguoiNha" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup check>
                <Label id="coBaoHiemLabel">
                  <AvInput id="benh-an-kham-benh-coBaoHiem" type="checkbox" className="form-check-input" name="coBaoHiem" />
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.coBaoHiem">Co Bao Hiem</Translate>
                </Label>
                <UncontrolledTooltip target="coBaoHiemLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.coBaoHiem" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="diaChiLabel" for="benh-an-kham-benh-diaChi">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.diaChi">Dia Chi</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-diaChi"
                  type="text"
                  name="diaChi"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="diaChiLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.diaChi" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="emailLabel" for="benh-an-kham-benh-email">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.email">Email</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-email"
                  type="text"
                  name="email"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="emailLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.email" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="gioiTinhLabel" for="benh-an-kham-benh-gioiTinh">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.gioiTinh">Gioi Tinh</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-gioiTinh"
                  type="string"
                  className="form-control"
                  name="gioiTinh"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="gioiTinhLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.gioiTinh" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="khangTheLabel" for="benh-an-kham-benh-khangThe">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.khangThe">Khang The</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-khangThe"
                  type="text"
                  name="khangThe"
                  validate={{
                    maxLength: { value: 5, errorMessage: translate('entity.validation.maxlength', { max: 5 }) }
                  }}
                />
                <UncontrolledTooltip target="khangTheLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.khangThe" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="lanKhamTrongNgayLabel" for="benh-an-kham-benh-lanKhamTrongNgay">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.lanKhamTrongNgay">Lan Kham Trong Ngay</Translate>
                </Label>
                <AvField id="benh-an-kham-benh-lanKhamTrongNgay" type="string" className="form-control" name="lanKhamTrongNgay" />
                <UncontrolledTooltip target="lanKhamTrongNgayLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.lanKhamTrongNgay" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="loaiGiayToTreEmLabel" for="benh-an-kham-benh-loaiGiayToTreEm">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.loaiGiayToTreEm">Loai Giay To Tre Em</Translate>
                </Label>
                <AvField id="benh-an-kham-benh-loaiGiayToTreEm" type="string" className="form-control" name="loaiGiayToTreEm" />
                <UncontrolledTooltip target="loaiGiayToTreEmLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.loaiGiayToTreEm" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ngayCapCmndLabel" for="benh-an-kham-benh-ngayCapCmnd">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.ngayCapCmnd">Ngay Cap Cmnd</Translate>
                </Label>
                <AvField id="benh-an-kham-benh-ngayCapCmnd" type="date" className="form-control" name="ngayCapCmnd" />
                <UncontrolledTooltip target="ngayCapCmndLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.ngayCapCmnd" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ngayMienCungChiTraLabel" for="benh-an-kham-benh-ngayMienCungChiTra">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.ngayMienCungChiTra">Ngay Mien Cung Chi Tra</Translate>
                </Label>
                <AvField id="benh-an-kham-benh-ngayMienCungChiTra" type="date" className="form-control" name="ngayMienCungChiTra" />
                <UncontrolledTooltip target="ngayMienCungChiTraLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.ngayMienCungChiTra" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ngaySinhLabel" for="benh-an-kham-benh-ngaySinh">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.ngaySinh">Ngay Sinh</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-ngaySinh"
                  type="date"
                  className="form-control"
                  name="ngaySinh"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') }
                  }}
                />
                <UncontrolledTooltip target="ngaySinhLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.ngaySinh" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="nhomMauLabel" for="benh-an-kham-benh-nhomMau">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.nhomMau">Nhom Mau</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-nhomMau"
                  type="text"
                  name="nhomMau"
                  validate={{
                    maxLength: { value: 5, errorMessage: translate('entity.validation.maxlength', { max: 5 }) }
                  }}
                />
                <UncontrolledTooltip target="nhomMauLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.nhomMau" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="noiCapCmndLabel" for="benh-an-kham-benh-noiCapCmnd">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.noiCapCmnd">Noi Cap Cmnd</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-noiCapCmnd"
                  type="text"
                  name="noiCapCmnd"
                  validate={{
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="noiCapCmndLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.noiCapCmnd" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="noiLamViecLabel" for="benh-an-kham-benh-noiLamViec">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.noiLamViec">Noi Lam Viec</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-noiLamViec"
                  type="text"
                  name="noiLamViec"
                  validate={{
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="noiLamViecLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.noiLamViec" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="phoneLabel" for="benh-an-kham-benh-phone">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.phone">Phone</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-phone"
                  type="text"
                  name="phone"
                  validate={{
                    maxLength: { value: 15, errorMessage: translate('entity.validation.maxlength', { max: 15 }) }
                  }}
                />
                <UncontrolledTooltip target="phoneLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.phone" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="phoneNguoiNhaLabel" for="benh-an-kham-benh-phoneNguoiNha">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.phoneNguoiNha">Phone Nguoi Nha</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-phoneNguoiNha"
                  type="text"
                  name="phoneNguoiNha"
                  validate={{
                    maxLength: { value: 15, errorMessage: translate('entity.validation.maxlength', { max: 15 }) }
                  }}
                />
                <UncontrolledTooltip target="phoneNguoiNhaLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.phoneNguoiNha" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="quocTichLabel" for="benh-an-kham-benh-quocTich">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.quocTich">Quoc Tich</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-quocTich"
                  type="text"
                  name="quocTich"
                  validate={{
                    maxLength: { value: 200, errorMessage: translate('entity.validation.maxlength', { max: 200 }) }
                  }}
                />
                <UncontrolledTooltip target="quocTichLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.quocTich" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenBenhNhanLabel" for="benh-an-kham-benh-tenBenhNhan">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tenBenhNhan">Ten Benh Nhan</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-tenBenhNhan"
                  type="text"
                  name="tenBenhNhan"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 200, errorMessage: translate('entity.validation.maxlength', { max: 200 }) }
                  }}
                />
                <UncontrolledTooltip target="tenBenhNhanLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.tenBenhNhan" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenNguoiNhaLabel" for="benh-an-kham-benh-tenNguoiNha">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tenNguoiNha">Ten Nguoi Nha</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-tenNguoiNha"
                  type="text"
                  name="tenNguoiNha"
                  validate={{
                    maxLength: { value: 200, errorMessage: translate('entity.validation.maxlength', { max: 200 }) }
                  }}
                />
                <UncontrolledTooltip target="tenNguoiNhaLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.tenNguoiNha" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="thangTuoiLabel" for="benh-an-kham-benh-thangTuoi">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.thangTuoi">Thang Tuoi</Translate>
                </Label>
                <AvField id="benh-an-kham-benh-thangTuoi" type="string" className="form-control" name="thangTuoi" />
                <UncontrolledTooltip target="thangTuoiLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.thangTuoi" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="thoiGianTiepNhanLabel" for="benh-an-kham-benh-thoiGianTiepNhan">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.thoiGianTiepNhan">Thoi Gian Tiep Nhan</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-thoiGianTiepNhan"
                  type="date"
                  className="form-control"
                  name="thoiGianTiepNhan"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') }
                  }}
                />
                <UncontrolledTooltip target="thoiGianTiepNhanLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.thoiGianTiepNhan" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tuoiLabel" for="benh-an-kham-benh-tuoi">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tuoi">Tuoi</Translate>
                </Label>
                <AvField id="benh-an-kham-benh-tuoi" type="string" className="form-control" name="tuoi" />
                <UncontrolledTooltip target="tuoiLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.tuoi" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="uuTienLabel" for="benh-an-kham-benh-uuTien">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.uuTien">Uu Tien</Translate>
                </Label>
                <AvField id="benh-an-kham-benh-uuTien" type="string" className="form-control" name="uuTien" />
                <UncontrolledTooltip target="uuTienLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.uuTien" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="uuidLabel" for="benh-an-kham-benh-uuid">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.uuid">Uuid</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-uuid"
                  type="text"
                  name="uuid"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="uuidLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.uuid" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="trangThaiLabel" for="benh-an-kham-benh-trangThai">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.trangThai">Trang Thai</Translate>
                </Label>
                <AvField id="benh-an-kham-benh-trangThai" type="string" className="form-control" name="trangThai" />
                <UncontrolledTooltip target="trangThaiLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.trangThai" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="maKhoaHoanTatKhamLabel" for="benh-an-kham-benh-maKhoaHoanTatKham">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.maKhoaHoanTatKham">Ma Khoa Hoan Tat Kham</Translate>
                </Label>
                <AvField id="benh-an-kham-benh-maKhoaHoanTatKham" type="string" className="form-control" name="maKhoaHoanTatKham" />
                <UncontrolledTooltip target="maKhoaHoanTatKhamLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.maKhoaHoanTatKham" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="maPhongHoanTatKhamLabel" for="benh-an-kham-benh-maPhongHoanTatKham">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.maPhongHoanTatKham">Ma Phong Hoan Tat Kham</Translate>
                </Label>
                <AvField id="benh-an-kham-benh-maPhongHoanTatKham" type="string" className="form-control" name="maPhongHoanTatKham" />
                <UncontrolledTooltip target="maPhongHoanTatKhamLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.maPhongHoanTatKham" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenKhoaHoanTatKhamLabel" for="benh-an-kham-benh-tenKhoaHoanTatKham">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tenKhoaHoanTatKham">Ten Khoa Hoan Tat Kham</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-tenKhoaHoanTatKham"
                  type="text"
                  name="tenKhoaHoanTatKham"
                  validate={{
                    maxLength: { value: 200, errorMessage: translate('entity.validation.maxlength', { max: 200 }) }
                  }}
                />
                <UncontrolledTooltip target="tenKhoaHoanTatKhamLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.tenKhoaHoanTatKham" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenPhongHoanTatKhamLabel" for="benh-an-kham-benh-tenPhongHoanTatKham">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.tenPhongHoanTatKham">Ten Phong Hoan Tat Kham</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-tenPhongHoanTatKham"
                  type="text"
                  name="tenPhongHoanTatKham"
                  validate={{
                    maxLength: { value: 200, errorMessage: translate('entity.validation.maxlength', { max: 200 }) }
                  }}
                />
                <UncontrolledTooltip target="tenPhongHoanTatKhamLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.tenPhongHoanTatKham" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="thoiGianHoanTatKhamLabel" for="benh-an-kham-benh-thoiGianHoanTatKham">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.thoiGianHoanTatKham">Thoi Gian Hoan Tat Kham</Translate>
                </Label>
                <AvField id="benh-an-kham-benh-thoiGianHoanTatKham" type="date" className="form-control" name="thoiGianHoanTatKham" />
                <UncontrolledTooltip target="thoiGianHoanTatKhamLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.thoiGianHoanTatKham" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="soBenhAnLabel" for="benh-an-kham-benh-soBenhAn">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.soBenhAn">So Benh An</Translate>
                </Label>
                <AvField id="benh-an-kham-benh-soBenhAn" type="text" name="soBenhAn" />
                <UncontrolledTooltip target="soBenhAnLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.soBenhAn" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="soBenhAnKhoaLabel" for="benh-an-kham-benh-soBenhAnKhoa">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.soBenhAnKhoa">So Benh An Khoa</Translate>
                </Label>
                <AvField id="benh-an-kham-benh-soBenhAnKhoa" type="text" name="soBenhAnKhoa" />
                <UncontrolledTooltip target="soBenhAnKhoaLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.help.soBenhAnKhoa" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="namLabel" for="benh-an-kham-benh-nam">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.nam">Nam</Translate>
                </Label>
                <AvField
                  id="benh-an-kham-benh-nam"
                  type="string"
                  className="form-control"
                  name="nam"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="benh-an-kham-benh-benhNhan">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.benhNhan">Benh Nhan</Translate>
                </Label>
                <AvInput id="benh-an-kham-benh-benhNhan" type="select" className="form-control" name="benhNhanId" required>
                  {benhNhans
                    ? benhNhans.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="benh-an-kham-benh-donVi">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.donVi">Don Vi</Translate>
                </Label>
                <AvInput id="benh-an-kham-benh-donVi" type="select" className="form-control" name="donViId" required>
                  {donVis
                    ? donVis.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="benh-an-kham-benh-nhanVienTiepNhan">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.nhanVienTiepNhan">Nhan Vien Tiep Nhan</Translate>
                </Label>
                <AvInput id="benh-an-kham-benh-nhanVienTiepNhan" type="select" className="form-control" name="nhanVienTiepNhanId" required>
                  {nhanViens
                    ? nhanViens.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="benh-an-kham-benh-phongTiepNhan">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhAnKhamBenh.phongTiepNhan">Phong Tiep Nhan</Translate>
                </Label>
                <AvInput id="benh-an-kham-benh-phongTiepNhan" type="select" className="form-control" name="phongTiepNhanId" required>
                  {phongs
                    ? phongs.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/benh-an-kham-benh" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  benhNhans: storeState.benhNhan.entities,
  donVis: storeState.donVi.entities,
  nhanViens: storeState.nhanVien.entities,
  phongs: storeState.phong.entities,
  benhAnKhamBenhEntity: storeState.benhAnKhamBenh.entity,
  loading: storeState.benhAnKhamBenh.loading,
  updating: storeState.benhAnKhamBenh.updating,
  updateSuccess: storeState.benhAnKhamBenh.updateSuccess
});

const mapDispatchToProps = {
  getBenhNhans,
  getDonVis,
  getNhanViens,
  getPhongs,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(BenhAnKhamBenhUpdate);
