import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './quan-huyen.reducer';
import { IQuanHuyen } from 'app/shared/model/khamchuabenh/quan-huyen.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IQuanHuyenDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const QuanHuyenDetail = (props: IQuanHuyenDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { quanHuyenEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.detail.title">QuanHuyen</Translate> [<b>{quanHuyenEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="cap">
              <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.cap">Cap</Translate>
            </span>
            <UncontrolledTooltip target="cap">
              <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.help.cap" />
            </UncontrolledTooltip>
          </dt>
          <dd>{quanHuyenEntity.cap}</dd>
          <dt>
            <span id="guessPhrase">
              <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.guessPhrase">Guess Phrase</Translate>
            </span>
            <UncontrolledTooltip target="guessPhrase">
              <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.help.guessPhrase" />
            </UncontrolledTooltip>
          </dt>
          <dd>{quanHuyenEntity.guessPhrase}</dd>
          <dt>
            <span id="ten">
              <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.ten">Ten</Translate>
            </span>
            <UncontrolledTooltip target="ten">
              <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.help.ten" />
            </UncontrolledTooltip>
          </dt>
          <dd>{quanHuyenEntity.ten}</dd>
          <dt>
            <span id="tenKhongDau">
              <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.tenKhongDau">Ten Khong Dau</Translate>
            </span>
            <UncontrolledTooltip target="tenKhongDau">
              <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.help.tenKhongDau" />
            </UncontrolledTooltip>
          </dt>
          <dd>{quanHuyenEntity.tenKhongDau}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.tinhThanhPho">Tinh Thanh Pho</Translate>
          </dt>
          <dd>{quanHuyenEntity.tinhThanhPhoId ? quanHuyenEntity.tinhThanhPhoId : ''}</dd>
        </dl>
        <Button tag={Link} to="/quan-huyen" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/quan-huyen/${quanHuyenEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ quanHuyen }: IRootState) => ({
  quanHuyenEntity: quanHuyen.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(QuanHuyenDetail);
