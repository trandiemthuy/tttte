import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ITinhThanhPho } from 'app/shared/model/khamchuabenh/tinh-thanh-pho.model';
import { getEntities as getTinhThanhPhos } from 'app/entities/khamchuabenh/tinh-thanh-pho/tinh-thanh-pho.reducer';
import { getEntity, updateEntity, createEntity, reset } from './quan-huyen.reducer';
import { IQuanHuyen } from 'app/shared/model/khamchuabenh/quan-huyen.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IQuanHuyenUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const QuanHuyenUpdate = (props: IQuanHuyenUpdateProps) => {
  const [tinhThanhPhoId, setTinhThanhPhoId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { quanHuyenEntity, tinhThanhPhos, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/quan-huyen' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getTinhThanhPhos();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...quanHuyenEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhQuanHuyen.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.home.createOrEditLabel">Create or edit a QuanHuyen</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : quanHuyenEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="quan-huyen-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="quan-huyen-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="capLabel" for="quan-huyen-cap">
                  <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.cap">Cap</Translate>
                </Label>
                <AvField
                  id="quan-huyen-cap"
                  type="text"
                  name="cap"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="capLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.help.cap" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="guessPhraseLabel" for="quan-huyen-guessPhrase">
                  <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.guessPhrase">Guess Phrase</Translate>
                </Label>
                <AvField
                  id="quan-huyen-guessPhrase"
                  type="text"
                  name="guessPhrase"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="guessPhraseLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.help.guessPhrase" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenLabel" for="quan-huyen-ten">
                  <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.ten">Ten</Translate>
                </Label>
                <AvField
                  id="quan-huyen-ten"
                  type="text"
                  name="ten"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="tenLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.help.ten" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenKhongDauLabel" for="quan-huyen-tenKhongDau">
                  <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.tenKhongDau">Ten Khong Dau</Translate>
                </Label>
                <AvField
                  id="quan-huyen-tenKhongDau"
                  type="text"
                  name="tenKhongDau"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="tenKhongDauLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.help.tenKhongDau" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="quan-huyen-tinhThanhPho">
                  <Translate contentKey="gatewayApp.khamchuabenhQuanHuyen.tinhThanhPho">Tinh Thanh Pho</Translate>
                </Label>
                <AvInput id="quan-huyen-tinhThanhPho" type="select" className="form-control" name="tinhThanhPhoId" required>
                  {tinhThanhPhos
                    ? tinhThanhPhos.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/quan-huyen" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  tinhThanhPhos: storeState.tinhThanhPho.entities,
  quanHuyenEntity: storeState.quanHuyen.entity,
  loading: storeState.quanHuyen.loading,
  updating: storeState.quanHuyen.updating,
  updateSuccess: storeState.quanHuyen.updateSuccess
});

const mapDispatchToProps = {
  getTinhThanhPhos,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(QuanHuyenUpdate);
