import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import BenhNhan from './benh-nhan';
import BenhNhanDetail from './benh-nhan-detail';
import BenhNhanUpdate from './benh-nhan-update';
import BenhNhanDeleteDialog from './benh-nhan-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={BenhNhanDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={BenhNhanUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={BenhNhanUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={BenhNhanDetail} />
      <ErrorBoundaryRoute path={match.url} component={BenhNhan} />
    </Switch>
  </>
);

export default Routes;
