import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IDanToc } from 'app/shared/model/khamchuabenh/dan-toc.model';
import { getEntities as getDanTocs } from 'app/entities/khamchuabenh/dan-toc/dan-toc.reducer';
import { INgheNghiep } from 'app/shared/model/khamchuabenh/nghe-nghiep.model';
import { getEntities as getNgheNghieps } from 'app/entities/khamchuabenh/nghe-nghiep/nghe-nghiep.reducer';
import { ICountry } from 'app/shared/model/khamchuabenh/country.model';
import { getEntities as getCountries } from 'app/entities/khamchuabenh/country/country.reducer';
import { IPhuongXa } from 'app/shared/model/khamchuabenh/phuong-xa.model';
import { getEntities as getPhuongXas } from 'app/entities/khamchuabenh/phuong-xa/phuong-xa.reducer';
import { IQuanHuyen } from 'app/shared/model/khamchuabenh/quan-huyen.model';
import { getEntities as getQuanHuyens } from 'app/entities/khamchuabenh/quan-huyen/quan-huyen.reducer';
import { ITinhThanhPho } from 'app/shared/model/khamchuabenh/tinh-thanh-pho.model';
import { getEntities as getTinhThanhPhos } from 'app/entities/khamchuabenh/tinh-thanh-pho/tinh-thanh-pho.reducer';
import { IUserExtra } from 'app/shared/model/khamchuabenh/user-extra.model';
import { getEntities as getUserExtras } from 'app/entities/khamchuabenh/user-extra/user-extra.reducer';
import { getEntity, updateEntity, createEntity, reset } from './benh-nhan.reducer';
import { IBenhNhan } from 'app/shared/model/khamchuabenh/benh-nhan.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IBenhNhanUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const BenhNhanUpdate = (props: IBenhNhanUpdateProps) => {
  const [danTocId, setDanTocId] = useState('0');
  const [ngheNghiepId, setNgheNghiepId] = useState('0');
  const [quocTichId, setQuocTichId] = useState('0');
  const [phuongXaId, setPhuongXaId] = useState('0');
  const [quanHuyenId, setQuanHuyenId] = useState('0');
  const [tinhThanhPhoId, setTinhThanhPhoId] = useState('0');
  const [userExtraId, setUserExtraId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { benhNhanEntity, danTocs, ngheNghieps, countries, phuongXas, quanHuyens, tinhThanhPhos, userExtras, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/benh-nhan' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getDanTocs();
    props.getNgheNghieps();
    props.getCountries();
    props.getPhuongXas();
    props.getQuanHuyens();
    props.getTinhThanhPhos();
    props.getUserExtras();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...benhNhanEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhBenhNhan.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.home.createOrEditLabel">Create or edit a BenhNhan</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : benhNhanEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="benh-nhan-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="benh-nhan-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup check>
                <Label id="chiCoNamSinhLabel">
                  <AvInput id="benh-nhan-chiCoNamSinh" type="checkbox" className="form-check-input" name="chiCoNamSinh" />
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.chiCoNamSinh">Chi Co Nam Sinh</Translate>
                </Label>
                <UncontrolledTooltip target="chiCoNamSinhLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.chiCoNamSinh" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="cmndLabel" for="benh-nhan-cmnd">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.cmnd">Cmnd</Translate>
                </Label>
                <AvField
                  id="benh-nhan-cmnd"
                  type="text"
                  name="cmnd"
                  validate={{
                    maxLength: { value: 20, errorMessage: translate('entity.validation.maxlength', { max: 20 }) }
                  }}
                />
                <UncontrolledTooltip target="cmndLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.cmnd" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="emailLabel" for="benh-nhan-email">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.email">Email</Translate>
                </Label>
                <AvField
                  id="benh-nhan-email"
                  type="text"
                  name="email"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="emailLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.email" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup check>
                <Label id="enabledLabel">
                  <AvInput id="benh-nhan-enabled" type="checkbox" className="form-check-input" name="enabled" />
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.enabled">Enabled</Translate>
                </Label>
                <UncontrolledTooltip target="enabledLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.enabled" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="gioiTinhLabel" for="benh-nhan-gioiTinh">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.gioiTinh">Gioi Tinh</Translate>
                </Label>
                <AvField
                  id="benh-nhan-gioiTinh"
                  type="string"
                  className="form-control"
                  name="gioiTinh"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="gioiTinhLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.gioiTinh" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="khangTheLabel" for="benh-nhan-khangThe">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.khangThe">Khang The</Translate>
                </Label>
                <AvField
                  id="benh-nhan-khangThe"
                  type="text"
                  name="khangThe"
                  validate={{
                    maxLength: { value: 5, errorMessage: translate('entity.validation.maxlength', { max: 5 }) }
                  }}
                />
                <UncontrolledTooltip target="khangTheLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.khangThe" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="maSoThueLabel" for="benh-nhan-maSoThue">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.maSoThue">Ma So Thue</Translate>
                </Label>
                <AvField
                  id="benh-nhan-maSoThue"
                  type="text"
                  name="maSoThue"
                  validate={{
                    maxLength: { value: 100, errorMessage: translate('entity.validation.maxlength', { max: 100 }) }
                  }}
                />
                <UncontrolledTooltip target="maSoThueLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.maSoThue" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ngayCapCmndLabel" for="benh-nhan-ngayCapCmnd">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.ngayCapCmnd">Ngay Cap Cmnd</Translate>
                </Label>
                <AvField id="benh-nhan-ngayCapCmnd" type="date" className="form-control" name="ngayCapCmnd" />
                <UncontrolledTooltip target="ngayCapCmndLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.ngayCapCmnd" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ngaySinhLabel" for="benh-nhan-ngaySinh">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.ngaySinh">Ngay Sinh</Translate>
                </Label>
                <AvField
                  id="benh-nhan-ngaySinh"
                  type="date"
                  className="form-control"
                  name="ngaySinh"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') }
                  }}
                />
                <UncontrolledTooltip target="ngaySinhLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.ngaySinh" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="nhomMauLabel" for="benh-nhan-nhomMau">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.nhomMau">Nhom Mau</Translate>
                </Label>
                <AvField
                  id="benh-nhan-nhomMau"
                  type="text"
                  name="nhomMau"
                  validate={{
                    maxLength: { value: 5, errorMessage: translate('entity.validation.maxlength', { max: 5 }) }
                  }}
                />
                <UncontrolledTooltip target="nhomMauLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.nhomMau" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="noiCapCmndLabel" for="benh-nhan-noiCapCmnd">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.noiCapCmnd">Noi Cap Cmnd</Translate>
                </Label>
                <AvField
                  id="benh-nhan-noiCapCmnd"
                  type="text"
                  name="noiCapCmnd"
                  validate={{
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="noiCapCmndLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.noiCapCmnd" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="noiLamViecLabel" for="benh-nhan-noiLamViec">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.noiLamViec">Noi Lam Viec</Translate>
                </Label>
                <AvField
                  id="benh-nhan-noiLamViec"
                  type="text"
                  name="noiLamViec"
                  validate={{
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="noiLamViecLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.noiLamViec" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="phoneLabel" for="benh-nhan-phone">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.phone">Phone</Translate>
                </Label>
                <AvField
                  id="benh-nhan-phone"
                  type="text"
                  name="phone"
                  validate={{
                    maxLength: { value: 15, errorMessage: translate('entity.validation.maxlength', { max: 15 }) }
                  }}
                />
                <UncontrolledTooltip target="phoneLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.phone" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenLabel" for="benh-nhan-ten">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.ten">Ten</Translate>
                </Label>
                <AvField
                  id="benh-nhan-ten"
                  type="text"
                  name="ten"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 200, errorMessage: translate('entity.validation.maxlength', { max: 200 }) }
                  }}
                />
                <UncontrolledTooltip target="tenLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.ten" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="soNhaXomLabel" for="benh-nhan-soNhaXom">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.soNhaXom">So Nha Xom</Translate>
                </Label>
                <AvField
                  id="benh-nhan-soNhaXom"
                  type="text"
                  name="soNhaXom"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="soNhaXomLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.soNhaXom" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="apThonIdLabel" for="benh-nhan-apThon">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.apThon">Ap Thon</Translate>
                </Label>
                <AvField
                  id="benh-nhan-apThon"
                  type="text"
                  name="apThon"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="apThonIdLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.apThon" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="diaPhuongIdLabel" for="benh-nhan-diaPhuongId">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.diaPhuongId">Dia Phuong Id</Translate>
                </Label>
                <AvField id="benh-nhan-diaPhuongId" type="string" className="form-control" name="diaPhuongId" />
                <UncontrolledTooltip target="diaPhuongIdLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.diaPhuongId" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="diaChiThuongTruLabel" for="benh-nhan-diaChiThuongTru">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.diaChiThuongTru">Dia Chi Thuong Tru</Translate>
                </Label>
                <AvField
                  id="benh-nhan-diaChiThuongTru"
                  type="text"
                  name="diaChiThuongTru"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="diaChiThuongTruLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.diaChiThuongTru" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="benh-nhan-danToc">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.danToc">Dan Toc</Translate>
                </Label>
                <AvInput id="benh-nhan-danToc" type="select" className="form-control" name="danTocId">
                  <option value="" key="0" />
                  {danTocs
                    ? danTocs.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="benh-nhan-ngheNghiep">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.ngheNghiep">Nghe Nghiep</Translate>
                </Label>
                <AvInput id="benh-nhan-ngheNghiep" type="select" className="form-control" name="ngheNghiepId">
                  <option value="" key="0" />
                  {ngheNghieps
                    ? ngheNghieps.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="benh-nhan-quocTich">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.quocTich">Quoc Tich</Translate>
                </Label>
                <AvInput id="benh-nhan-quocTich" type="select" className="form-control" name="quocTichId" required>
                  {countries
                    ? countries.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="benh-nhan-phuongXa">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.phuongXa">Phuong Xa</Translate>
                </Label>
                <AvInput id="benh-nhan-phuongXa" type="select" className="form-control" name="phuongXaId">
                  <option value="" key="0" />
                  {phuongXas
                    ? phuongXas.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="benh-nhan-quanHuyen">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.quanHuyen">Quan Huyen</Translate>
                </Label>
                <AvInput id="benh-nhan-quanHuyen" type="select" className="form-control" name="quanHuyenId">
                  <option value="" key="0" />
                  {quanHuyens
                    ? quanHuyens.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="benh-nhan-tinhThanhPho">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.tinhThanhPho">Tinh Thanh Pho</Translate>
                </Label>
                <AvInput id="benh-nhan-tinhThanhPho" type="select" className="form-control" name="tinhThanhPhoId">
                  <option value="" key="0" />
                  {tinhThanhPhos
                    ? tinhThanhPhos.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="benh-nhan-userExtra">
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.userExtra">User Extra</Translate>
                </Label>
                <AvInput id="benh-nhan-userExtra" type="select" className="form-control" name="userExtraId">
                  <option value="" key="0" />
                  {userExtras
                    ? userExtras.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/benh-nhan" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  danTocs: storeState.danToc.entities,
  ngheNghieps: storeState.ngheNghiep.entities,
  countries: storeState.country.entities,
  phuongXas: storeState.phuongXa.entities,
  quanHuyens: storeState.quanHuyen.entities,
  tinhThanhPhos: storeState.tinhThanhPho.entities,
  userExtras: storeState.userExtra.entities,
  benhNhanEntity: storeState.benhNhan.entity,
  loading: storeState.benhNhan.loading,
  updating: storeState.benhNhan.updating,
  updateSuccess: storeState.benhNhan.updateSuccess
});

const mapDispatchToProps = {
  getDanTocs,
  getNgheNghieps,
  getCountries,
  getPhuongXas,
  getQuanHuyens,
  getTinhThanhPhos,
  getUserExtras,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(BenhNhanUpdate);
