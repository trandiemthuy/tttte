import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './benh-nhan.reducer';
import { IBenhNhan } from 'app/shared/model/khamchuabenh/benh-nhan.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IBenhNhanDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const BenhNhanDetail = (props: IBenhNhanDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { benhNhanEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.detail.title">BenhNhan</Translate> [<b>{benhNhanEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="chiCoNamSinh">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.chiCoNamSinh">Chi Co Nam Sinh</Translate>
            </span>
            <UncontrolledTooltip target="chiCoNamSinh">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.chiCoNamSinh" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.chiCoNamSinh ? 'true' : 'false'}</dd>
          <dt>
            <span id="cmnd">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.cmnd">Cmnd</Translate>
            </span>
            <UncontrolledTooltip target="cmnd">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.cmnd" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.cmnd}</dd>
          <dt>
            <span id="email">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.email">Email</Translate>
            </span>
            <UncontrolledTooltip target="email">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.email" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.email}</dd>
          <dt>
            <span id="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.enabled">Enabled</Translate>
            </span>
            <UncontrolledTooltip target="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.enabled" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.enabled ? 'true' : 'false'}</dd>
          <dt>
            <span id="gioiTinh">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.gioiTinh">Gioi Tinh</Translate>
            </span>
            <UncontrolledTooltip target="gioiTinh">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.gioiTinh" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.gioiTinh}</dd>
          <dt>
            <span id="khangThe">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.khangThe">Khang The</Translate>
            </span>
            <UncontrolledTooltip target="khangThe">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.khangThe" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.khangThe}</dd>
          <dt>
            <span id="maSoThue">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.maSoThue">Ma So Thue</Translate>
            </span>
            <UncontrolledTooltip target="maSoThue">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.maSoThue" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.maSoThue}</dd>
          <dt>
            <span id="ngayCapCmnd">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.ngayCapCmnd">Ngay Cap Cmnd</Translate>
            </span>
            <UncontrolledTooltip target="ngayCapCmnd">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.ngayCapCmnd" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            <TextFormat value={benhNhanEntity.ngayCapCmnd} type="date" format={APP_LOCAL_DATE_FORMAT} />
          </dd>
          <dt>
            <span id="ngaySinh">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.ngaySinh">Ngay Sinh</Translate>
            </span>
            <UncontrolledTooltip target="ngaySinh">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.ngaySinh" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            <TextFormat value={benhNhanEntity.ngaySinh} type="date" format={APP_LOCAL_DATE_FORMAT} />
          </dd>
          <dt>
            <span id="nhomMau">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.nhomMau">Nhom Mau</Translate>
            </span>
            <UncontrolledTooltip target="nhomMau">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.nhomMau" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.nhomMau}</dd>
          <dt>
            <span id="noiCapCmnd">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.noiCapCmnd">Noi Cap Cmnd</Translate>
            </span>
            <UncontrolledTooltip target="noiCapCmnd">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.noiCapCmnd" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.noiCapCmnd}</dd>
          <dt>
            <span id="noiLamViec">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.noiLamViec">Noi Lam Viec</Translate>
            </span>
            <UncontrolledTooltip target="noiLamViec">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.noiLamViec" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.noiLamViec}</dd>
          <dt>
            <span id="phone">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.phone">Phone</Translate>
            </span>
            <UncontrolledTooltip target="phone">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.phone" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.phone}</dd>
          <dt>
            <span id="ten">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.ten">Ten</Translate>
            </span>
            <UncontrolledTooltip target="ten">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.ten" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.ten}</dd>
          <dt>
            <span id="soNhaXom">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.soNhaXom">So Nha Xom</Translate>
            </span>
            <UncontrolledTooltip target="soNhaXom">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.soNhaXom" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.soNhaXom}</dd>
          <dt>
            <span id="apThon">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.apThon">Ap Thon</Translate>
            </span>
            <UncontrolledTooltip target="apThon">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.apThon" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.apThon}</dd>
          <dt>
            <span id="diaPhuongId">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.diaPhuongId">Dia Phuong Id</Translate>
            </span>
            <UncontrolledTooltip target="diaPhuongId">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.diaPhuongId" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.diaPhuongId}</dd>
          <dt>
            <span id="diaChiThuongTru">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.diaChiThuongTru">Dia Chi Thuong Tru</Translate>
            </span>
            <UncontrolledTooltip target="diaChiThuongTru">
              <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.help.diaChiThuongTru" />
            </UncontrolledTooltip>
          </dt>
          <dd>{benhNhanEntity.diaChiThuongTru}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.danToc">Dan Toc</Translate>
          </dt>
          <dd>{benhNhanEntity.danTocId ? benhNhanEntity.danTocId : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.ngheNghiep">Nghe Nghiep</Translate>
          </dt>
          <dd>{benhNhanEntity.ngheNghiepId ? benhNhanEntity.ngheNghiepId : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.quocTich">Quoc Tich</Translate>
          </dt>
          <dd>{benhNhanEntity.quocTichId ? benhNhanEntity.quocTichId : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.phuongXa">Phuong Xa</Translate>
          </dt>
          <dd>{benhNhanEntity.phuongXaId ? benhNhanEntity.phuongXaId : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.quanHuyen">Quan Huyen</Translate>
          </dt>
          <dd>{benhNhanEntity.quanHuyenId ? benhNhanEntity.quanHuyenId : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.tinhThanhPho">Tinh Thanh Pho</Translate>
          </dt>
          <dd>{benhNhanEntity.tinhThanhPhoId ? benhNhanEntity.tinhThanhPhoId : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.userExtra">User Extra</Translate>
          </dt>
          <dd>{benhNhanEntity.userExtraId ? benhNhanEntity.userExtraId : ''}</dd>
        </dl>
        <Button tag={Link} to="/benh-nhan" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/benh-nhan/${benhNhanEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ benhNhan }: IRootState) => ({
  benhNhanEntity: benhNhan.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(BenhNhanDetail);
