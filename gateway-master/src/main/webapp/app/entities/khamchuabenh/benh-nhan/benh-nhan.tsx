import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './benh-nhan.reducer';
import { IBenhNhan } from 'app/shared/model/khamchuabenh/benh-nhan.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IBenhNhanProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const BenhNhan = (props: IBenhNhanProps) => {
  const [paginationState, setPaginationState] = useState(getSortState(props.location, ITEMS_PER_PAGE));

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  useEffect(() => {
    getAllEntities();
  }, []);

  const sortEntities = () => {
    getAllEntities();
    props.history.push(
      `${props.location.pathname}?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`
    );
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage
    });

  const { benhNhanList, match, totalItems } = props;
  return (
    <div>
      <h2 id="benh-nhan-heading">
        <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.home.title">Benh Nhans</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.home.createLabel">Create new Benh Nhan</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {benhNhanList && benhNhanList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('chiCoNamSinh')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.chiCoNamSinh">Chi Co Nam Sinh</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('cmnd')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.cmnd">Cmnd</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('email')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.email">Email</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('enabled')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.enabled">Enabled</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('gioiTinh')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.gioiTinh">Gioi Tinh</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('khangThe')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.khangThe">Khang The</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('maSoThue')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.maSoThue">Ma So Thue</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ngayCapCmnd')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.ngayCapCmnd">Ngay Cap Cmnd</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ngaySinh')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.ngaySinh">Ngay Sinh</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('nhomMau')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.nhomMau">Nhom Mau</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('noiCapCmnd')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.noiCapCmnd">Noi Cap Cmnd</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('noiLamViec')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.noiLamViec">Noi Lam Viec</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('phone')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.phone">Phone</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ten')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.ten">Ten</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('soNhaXom')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.soNhaXom">So Nha Xom</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('apThon')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.apThon">Ap Thon</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('diaPhuongId')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.diaPhuongId">Dia Phuong Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('diaChiThuongTru')}>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.diaChiThuongTru">Dia Chi Thuong Tru</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.danToc">Dan Toc</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.ngheNghiep">Nghe Nghiep</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.quocTich">Quoc Tich</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.phuongXa">Phuong Xa</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.quanHuyen">Quan Huyen</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.tinhThanhPho">Tinh Thanh Pho</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.userExtra">User Extra</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {benhNhanList.map((benhNhan, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${benhNhan.id}`} color="link" size="sm">
                      {benhNhan.id}
                    </Button>
                  </td>
                  <td>{benhNhan.chiCoNamSinh ? 'true' : 'false'}</td>
                  <td>{benhNhan.cmnd}</td>
                  <td>{benhNhan.email}</td>
                  <td>{benhNhan.enabled ? 'true' : 'false'}</td>
                  <td>{benhNhan.gioiTinh}</td>
                  <td>{benhNhan.khangThe}</td>
                  <td>{benhNhan.maSoThue}</td>
                  <td>
                    <TextFormat type="date" value={benhNhan.ngayCapCmnd} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>
                    <TextFormat type="date" value={benhNhan.ngaySinh} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{benhNhan.nhomMau}</td>
                  <td>{benhNhan.noiCapCmnd}</td>
                  <td>{benhNhan.noiLamViec}</td>
                  <td>{benhNhan.phone}</td>
                  <td>{benhNhan.ten}</td>
                  <td>{benhNhan.soNhaXom}</td>
                  <td>{benhNhan.apThon}</td>
                  <td>{benhNhan.diaPhuongId}</td>
                  <td>{benhNhan.diaChiThuongTru}</td>
                  <td>{benhNhan.danTocId ? <Link to={`dan-toc/${benhNhan.danTocId}`}>{benhNhan.danTocId}</Link> : ''}</td>
                  <td>{benhNhan.ngheNghiepId ? <Link to={`nghe-nghiep/${benhNhan.ngheNghiepId}`}>{benhNhan.ngheNghiepId}</Link> : ''}</td>
                  <td>{benhNhan.quocTichId ? <Link to={`country/${benhNhan.quocTichId}`}>{benhNhan.quocTichId}</Link> : ''}</td>
                  <td>{benhNhan.phuongXaId ? <Link to={`phuong-xa/${benhNhan.phuongXaId}`}>{benhNhan.phuongXaId}</Link> : ''}</td>
                  <td>{benhNhan.quanHuyenId ? <Link to={`quan-huyen/${benhNhan.quanHuyenId}`}>{benhNhan.quanHuyenId}</Link> : ''}</td>
                  <td>
                    {benhNhan.tinhThanhPhoId ? <Link to={`tinh-thanh-pho/${benhNhan.tinhThanhPhoId}`}>{benhNhan.tinhThanhPhoId}</Link> : ''}
                  </td>
                  <td>{benhNhan.userExtraId ? <Link to={`user-extra/${benhNhan.userExtraId}`}>{benhNhan.userExtraId}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${benhNhan.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${benhNhan.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="primary"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${benhNhan.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="danger"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          <div className="alert alert-warning">
            <Translate contentKey="gatewayApp.khamchuabenhBenhNhan.home.notFound">No Benh Nhans found</Translate>
          </div>
        )}
      </div>
      <div className={benhNhanList && benhNhanList.length > 0 ? '' : 'd-none'}>
        <Row className="justify-content-center">
          <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} i18nEnabled />
        </Row>
        <Row className="justify-content-center">
          <JhiPagination
            activePage={paginationState.activePage}
            onSelect={handlePagination}
            maxButtons={5}
            itemsPerPage={paginationState.itemsPerPage}
            totalItems={props.totalItems}
          />
        </Row>
      </div>
    </div>
  );
};

const mapStateToProps = ({ benhNhan }: IRootState) => ({
  benhNhanList: benhNhan.entities,
  totalItems: benhNhan.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(BenhNhan);
