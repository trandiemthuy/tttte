import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './the-bhyt.reducer';
import { ITheBhyt } from 'app/shared/model/khamchuabenh/the-bhyt.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface ITheBhytProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const TheBhyt = (props: ITheBhytProps) => {
  const [paginationState, setPaginationState] = useState(getSortState(props.location, ITEMS_PER_PAGE));

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  useEffect(() => {
    getAllEntities();
  }, []);

  const sortEntities = () => {
    getAllEntities();
    props.history.push(
      `${props.location.pathname}?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`
    );
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage
    });

  const { theBhytList, match, totalItems } = props;
  return (
    <div>
      <h2 id="the-bhyt-heading">
        <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.home.title">The Bhyts</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.home.createLabel">Create new The Bhyt</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {theBhytList && theBhytList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('enabled')}>
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.enabled">Enabled</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('maKhuVuc')}>
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.maKhuVuc">Ma Khu Vuc</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('maSoBhxh')}>
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.maSoBhxh">Ma So Bhxh</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('maTheCu')}>
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.maTheCu">Ma The Cu</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ngayBatDau')}>
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.ngayBatDau">Ngay Bat Dau</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ngayDuNamNam')}>
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.ngayDuNamNam">Ngay Du Nam Nam</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ngayHetHan')}>
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.ngayHetHan">Ngay Het Han</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('qr')}>
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.qr">Qr</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('soThe')}>
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.soThe">So The</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ngayMienCungChiTra')}>
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.ngayMienCungChiTra">Ngay Mien Cung Chi Tra</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.benhNhan">Benh Nhan</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.doiTuongBhyt">Doi Tuong Bhyt</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.thongTinBhxh">Thong Tin Bhxh</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {theBhytList.map((theBhyt, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${theBhyt.id}`} color="link" size="sm">
                      {theBhyt.id}
                    </Button>
                  </td>
                  <td>{theBhyt.enabled ? 'true' : 'false'}</td>
                  <td>{theBhyt.maKhuVuc}</td>
                  <td>{theBhyt.maSoBhxh}</td>
                  <td>{theBhyt.maTheCu}</td>
                  <td>
                    <TextFormat type="date" value={theBhyt.ngayBatDau} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>
                    <TextFormat type="date" value={theBhyt.ngayDuNamNam} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>
                    <TextFormat type="date" value={theBhyt.ngayHetHan} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{theBhyt.qr}</td>
                  <td>{theBhyt.soThe}</td>
                  <td>
                    <TextFormat type="date" value={theBhyt.ngayMienCungChiTra} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{theBhyt.benhNhanId ? <Link to={`benh-nhan/${theBhyt.benhNhanId}`}>{theBhyt.benhNhanId}</Link> : ''}</td>
                  <td>
                    {theBhyt.doiTuongBhytId ? <Link to={`doi-tuong-bhyt/${theBhyt.doiTuongBhytId}`}>{theBhyt.doiTuongBhytId}</Link> : ''}
                  </td>
                  <td>
                    {theBhyt.thongTinBhxhId ? <Link to={`thong-tin-bhxh/${theBhyt.thongTinBhxhId}`}>{theBhyt.thongTinBhxhId}</Link> : ''}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${theBhyt.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${theBhyt.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="primary"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${theBhyt.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="danger"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          <div className="alert alert-warning">
            <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.home.notFound">No The Bhyts found</Translate>
          </div>
        )}
      </div>
      <div className={theBhytList && theBhytList.length > 0 ? '' : 'd-none'}>
        <Row className="justify-content-center">
          <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} i18nEnabled />
        </Row>
        <Row className="justify-content-center">
          <JhiPagination
            activePage={paginationState.activePage}
            onSelect={handlePagination}
            maxButtons={5}
            itemsPerPage={paginationState.itemsPerPage}
            totalItems={props.totalItems}
          />
        </Row>
      </div>
    </div>
  );
};

const mapStateToProps = ({ theBhyt }: IRootState) => ({
  theBhytList: theBhyt.entities,
  totalItems: theBhyt.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TheBhyt);
