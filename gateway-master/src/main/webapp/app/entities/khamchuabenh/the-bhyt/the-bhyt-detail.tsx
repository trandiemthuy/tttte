import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './the-bhyt.reducer';
import { ITheBhyt } from 'app/shared/model/khamchuabenh/the-bhyt.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ITheBhytDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TheBhytDetail = (props: ITheBhytDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { theBhytEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.detail.title">TheBhyt</Translate> [<b>{theBhytEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.enabled">Enabled</Translate>
            </span>
            <UncontrolledTooltip target="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.enabled" />
            </UncontrolledTooltip>
          </dt>
          <dd>{theBhytEntity.enabled ? 'true' : 'false'}</dd>
          <dt>
            <span id="maKhuVuc">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.maKhuVuc">Ma Khu Vuc</Translate>
            </span>
            <UncontrolledTooltip target="maKhuVuc">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.maKhuVuc" />
            </UncontrolledTooltip>
          </dt>
          <dd>{theBhytEntity.maKhuVuc}</dd>
          <dt>
            <span id="maSoBhxh">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.maSoBhxh">Ma So Bhxh</Translate>
            </span>
            <UncontrolledTooltip target="maSoBhxh">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.maSoBhxh" />
            </UncontrolledTooltip>
          </dt>
          <dd>{theBhytEntity.maSoBhxh}</dd>
          <dt>
            <span id="maTheCu">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.maTheCu">Ma The Cu</Translate>
            </span>
            <UncontrolledTooltip target="maTheCu">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.maTheCu" />
            </UncontrolledTooltip>
          </dt>
          <dd>{theBhytEntity.maTheCu}</dd>
          <dt>
            <span id="ngayBatDau">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.ngayBatDau">Ngay Bat Dau</Translate>
            </span>
            <UncontrolledTooltip target="ngayBatDau">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.ngayBatDau" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            <TextFormat value={theBhytEntity.ngayBatDau} type="date" format={APP_LOCAL_DATE_FORMAT} />
          </dd>
          <dt>
            <span id="ngayDuNamNam">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.ngayDuNamNam">Ngay Du Nam Nam</Translate>
            </span>
            <UncontrolledTooltip target="ngayDuNamNam">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.ngayDuNamNam" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            <TextFormat value={theBhytEntity.ngayDuNamNam} type="date" format={APP_LOCAL_DATE_FORMAT} />
          </dd>
          <dt>
            <span id="ngayHetHan">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.ngayHetHan">Ngay Het Han</Translate>
            </span>
            <UncontrolledTooltip target="ngayHetHan">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.ngayHetHan" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            <TextFormat value={theBhytEntity.ngayHetHan} type="date" format={APP_LOCAL_DATE_FORMAT} />
          </dd>
          <dt>
            <span id="qr">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.qr">Qr</Translate>
            </span>
            <UncontrolledTooltip target="qr">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.qr" />
            </UncontrolledTooltip>
          </dt>
          <dd>{theBhytEntity.qr}</dd>
          <dt>
            <span id="soThe">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.soThe">So The</Translate>
            </span>
            <UncontrolledTooltip target="soThe">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.soThe" />
            </UncontrolledTooltip>
          </dt>
          <dd>{theBhytEntity.soThe}</dd>
          <dt>
            <span id="ngayMienCungChiTra">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.ngayMienCungChiTra">Ngay Mien Cung Chi Tra</Translate>
            </span>
            <UncontrolledTooltip target="ngayMienCungChiTra">
              <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.ngayMienCungChiTra" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            <TextFormat value={theBhytEntity.ngayMienCungChiTra} type="date" format={APP_LOCAL_DATE_FORMAT} />
          </dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.benhNhan">Benh Nhan</Translate>
          </dt>
          <dd>{theBhytEntity.benhNhanId ? theBhytEntity.benhNhanId : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.doiTuongBhyt">Doi Tuong Bhyt</Translate>
          </dt>
          <dd>{theBhytEntity.doiTuongBhytId ? theBhytEntity.doiTuongBhytId : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.thongTinBhxh">Thong Tin Bhxh</Translate>
          </dt>
          <dd>{theBhytEntity.thongTinBhxhId ? theBhytEntity.thongTinBhxhId : ''}</dd>
        </dl>
        <Button tag={Link} to="/the-bhyt" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/the-bhyt/${theBhytEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ theBhyt }: IRootState) => ({
  theBhytEntity: theBhyt.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TheBhytDetail);
