import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import TheBhyt from './the-bhyt';
import TheBhytDetail from './the-bhyt-detail';
import TheBhytUpdate from './the-bhyt-update';
import TheBhytDeleteDialog from './the-bhyt-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={TheBhytDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={TheBhytUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={TheBhytUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={TheBhytDetail} />
      <ErrorBoundaryRoute path={match.url} component={TheBhyt} />
    </Switch>
  </>
);

export default Routes;
