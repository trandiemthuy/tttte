import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ITheBhyt, defaultValue } from 'app/shared/model/khamchuabenh/the-bhyt.model';

export const ACTION_TYPES = {
  FETCH_THEBHYT_LIST: 'theBhyt/FETCH_THEBHYT_LIST',
  FETCH_THEBHYT: 'theBhyt/FETCH_THEBHYT',
  CREATE_THEBHYT: 'theBhyt/CREATE_THEBHYT',
  UPDATE_THEBHYT: 'theBhyt/UPDATE_THEBHYT',
  DELETE_THEBHYT: 'theBhyt/DELETE_THEBHYT',
  RESET: 'theBhyt/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ITheBhyt>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type TheBhytState = Readonly<typeof initialState>;

// Reducer

export default (state: TheBhytState = initialState, action): TheBhytState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_THEBHYT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_THEBHYT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_THEBHYT):
    case REQUEST(ACTION_TYPES.UPDATE_THEBHYT):
    case REQUEST(ACTION_TYPES.DELETE_THEBHYT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_THEBHYT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_THEBHYT):
    case FAILURE(ACTION_TYPES.CREATE_THEBHYT):
    case FAILURE(ACTION_TYPES.UPDATE_THEBHYT):
    case FAILURE(ACTION_TYPES.DELETE_THEBHYT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_THEBHYT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_THEBHYT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_THEBHYT):
    case SUCCESS(ACTION_TYPES.UPDATE_THEBHYT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_THEBHYT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/the-bhyts';

// Actions

export const getEntities: ICrudGetAllAction<ITheBhyt> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_THEBHYT_LIST,
    payload: axios.get<ITheBhyt>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ITheBhyt> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_THEBHYT,
    payload: axios.get<ITheBhyt>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ITheBhyt> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_THEBHYT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ITheBhyt> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_THEBHYT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ITheBhyt> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_THEBHYT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
