import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IBenhNhan } from 'app/shared/model/khamchuabenh/benh-nhan.model';
import { getEntities as getBenhNhans } from 'app/entities/khamchuabenh/benh-nhan/benh-nhan.reducer';
import { IDoiTuongBhyt } from 'app/shared/model/khamchuabenh/doi-tuong-bhyt.model';
import { getEntities as getDoiTuongBhyts } from 'app/entities/khamchuabenh/doi-tuong-bhyt/doi-tuong-bhyt.reducer';
import { IThongTinBhxh } from 'app/shared/model/khamchuabenh/thong-tin-bhxh.model';
import { getEntities as getThongTinBhxhs } from 'app/entities/khamchuabenh/thong-tin-bhxh/thong-tin-bhxh.reducer';
import { getEntity, updateEntity, createEntity, reset } from './the-bhyt.reducer';
import { ITheBhyt } from 'app/shared/model/khamchuabenh/the-bhyt.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ITheBhytUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TheBhytUpdate = (props: ITheBhytUpdateProps) => {
  const [benhNhanId, setBenhNhanId] = useState('0');
  const [doiTuongBhytId, setDoiTuongBhytId] = useState('0');
  const [thongTinBhxhId, setThongTinBhxhId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { theBhytEntity, benhNhans, doiTuongBhyts, thongTinBhxhs, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/the-bhyt' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getBenhNhans();
    props.getDoiTuongBhyts();
    props.getThongTinBhxhs();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...theBhytEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhTheBhyt.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.home.createOrEditLabel">Create or edit a TheBhyt</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : theBhytEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="the-bhyt-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="the-bhyt-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup check>
                <Label id="enabledLabel">
                  <AvInput id="the-bhyt-enabled" type="checkbox" className="form-check-input" name="enabled" />
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.enabled">Enabled</Translate>
                </Label>
                <UncontrolledTooltip target="enabledLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.enabled" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="maKhuVucLabel" for="the-bhyt-maKhuVuc">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.maKhuVuc">Ma Khu Vuc</Translate>
                </Label>
                <AvField
                  id="the-bhyt-maKhuVuc"
                  type="text"
                  name="maKhuVuc"
                  validate={{
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="maKhuVucLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.maKhuVuc" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="maSoBhxhLabel" for="the-bhyt-maSoBhxh">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.maSoBhxh">Ma So Bhxh</Translate>
                </Label>
                <AvField
                  id="the-bhyt-maSoBhxh"
                  type="text"
                  name="maSoBhxh"
                  validate={{
                    maxLength: { value: 30, errorMessage: translate('entity.validation.maxlength', { max: 30 }) }
                  }}
                />
                <UncontrolledTooltip target="maSoBhxhLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.maSoBhxh" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="maTheCuLabel" for="the-bhyt-maTheCu">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.maTheCu">Ma The Cu</Translate>
                </Label>
                <AvField
                  id="the-bhyt-maTheCu"
                  type="text"
                  name="maTheCu"
                  validate={{
                    maxLength: { value: 30, errorMessage: translate('entity.validation.maxlength', { max: 30 }) }
                  }}
                />
                <UncontrolledTooltip target="maTheCuLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.maTheCu" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ngayBatDauLabel" for="the-bhyt-ngayBatDau">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.ngayBatDau">Ngay Bat Dau</Translate>
                </Label>
                <AvField
                  id="the-bhyt-ngayBatDau"
                  type="date"
                  className="form-control"
                  name="ngayBatDau"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') }
                  }}
                />
                <UncontrolledTooltip target="ngayBatDauLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.ngayBatDau" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ngayDuNamNamLabel" for="the-bhyt-ngayDuNamNam">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.ngayDuNamNam">Ngay Du Nam Nam</Translate>
                </Label>
                <AvField
                  id="the-bhyt-ngayDuNamNam"
                  type="date"
                  className="form-control"
                  name="ngayDuNamNam"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') }
                  }}
                />
                <UncontrolledTooltip target="ngayDuNamNamLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.ngayDuNamNam" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ngayHetHanLabel" for="the-bhyt-ngayHetHan">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.ngayHetHan">Ngay Het Han</Translate>
                </Label>
                <AvField
                  id="the-bhyt-ngayHetHan"
                  type="date"
                  className="form-control"
                  name="ngayHetHan"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') }
                  }}
                />
                <UncontrolledTooltip target="ngayHetHanLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.ngayHetHan" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="qrLabel" for="the-bhyt-qr">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.qr">Qr</Translate>
                </Label>
                <AvField
                  id="the-bhyt-qr"
                  type="text"
                  name="qr"
                  validate={{
                    maxLength: { value: 1000, errorMessage: translate('entity.validation.maxlength', { max: 1000 }) }
                  }}
                />
                <UncontrolledTooltip target="qrLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.qr" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="soTheLabel" for="the-bhyt-soThe">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.soThe">So The</Translate>
                </Label>
                <AvField
                  id="the-bhyt-soThe"
                  type="text"
                  name="soThe"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 30, errorMessage: translate('entity.validation.maxlength', { max: 30 }) }
                  }}
                />
                <UncontrolledTooltip target="soTheLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.soThe" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ngayMienCungChiTraLabel" for="the-bhyt-ngayMienCungChiTra">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.ngayMienCungChiTra">Ngay Mien Cung Chi Tra</Translate>
                </Label>
                <AvField id="the-bhyt-ngayMienCungChiTra" type="date" className="form-control" name="ngayMienCungChiTra" />
                <UncontrolledTooltip target="ngayMienCungChiTraLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.help.ngayMienCungChiTra" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="the-bhyt-benhNhan">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.benhNhan">Benh Nhan</Translate>
                </Label>
                <AvInput id="the-bhyt-benhNhan" type="select" className="form-control" name="benhNhanId">
                  <option value="" key="0" />
                  {benhNhans
                    ? benhNhans.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="the-bhyt-doiTuongBhyt">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.doiTuongBhyt">Doi Tuong Bhyt</Translate>
                </Label>
                <AvInput id="the-bhyt-doiTuongBhyt" type="select" className="form-control" name="doiTuongBhytId" required>
                  {doiTuongBhyts
                    ? doiTuongBhyts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="the-bhyt-thongTinBhxh">
                  <Translate contentKey="gatewayApp.khamchuabenhTheBhyt.thongTinBhxh">Thong Tin Bhxh</Translate>
                </Label>
                <AvInput id="the-bhyt-thongTinBhxh" type="select" className="form-control" name="thongTinBhxhId" required>
                  {thongTinBhxhs
                    ? thongTinBhxhs.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/the-bhyt" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  benhNhans: storeState.benhNhan.entities,
  doiTuongBhyts: storeState.doiTuongBhyt.entities,
  thongTinBhxhs: storeState.thongTinBhxh.entities,
  theBhytEntity: storeState.theBhyt.entity,
  loading: storeState.theBhyt.loading,
  updating: storeState.theBhyt.updating,
  updateSuccess: storeState.theBhyt.updateSuccess
});

const mapDispatchToProps = {
  getBenhNhans,
  getDoiTuongBhyts,
  getThongTinBhxhs,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TheBhytUpdate);
