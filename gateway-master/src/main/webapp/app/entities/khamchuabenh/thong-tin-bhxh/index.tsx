import React from 'react';
import {Switch} from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ThongTinBhxh from './thong-tin-bhxh';
import ThongTinBhxhDetail from './thong-tin-bhxh-detail';
import ThongTinBhxhUpdate from './thong-tin-bhxh-update';
import ThongTinBhxhDeleteDialog from './thong-tin-bhxh-delete-dialog';

const Routes = ({match}) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ThongTinBhxhDeleteDialog}/>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ThongTinBhxhUpdate}/>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ThongTinBhxhUpdate}/>
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ThongTinBhxhDetail}/>
      <ErrorBoundaryRoute path={match.url} component={ThongTinBhxh}/>
    </Switch>
  </>
);

export default Routes;
