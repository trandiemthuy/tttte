import axios from 'axios';
import { ICrudDeleteAction, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';

import { defaultValue, IThongTinBhxh } from 'app/shared/model/khamchuabenh/thong-tin-bhxh.model';

export const ACTION_TYPES = {
  FETCH_THONGTINBHXH_LIST: 'thongTinBhxh/FETCH_THONGTINBHXH_LIST',
  FETCH_THONGTINBHXH: 'thongTinBhxh/FETCH_THONGTINBHXH',
  CREATE_THONGTINBHXH: 'thongTinBhxh/CREATE_THONGTINBHXH',
  UPDATE_THONGTINBHXH: 'thongTinBhxh/UPDATE_THONGTINBHXH',
  DELETE_THONGTINBHXH: 'thongTinBhxh/DELETE_THONGTINBHXH',
  RESET: 'thongTinBhxh/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IThongTinBhxh>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ThongTinBhxhState = Readonly<typeof initialState>;

// Reducer

export default (state: ThongTinBhxhState = initialState, action): ThongTinBhxhState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_THONGTINBHXH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_THONGTINBHXH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_THONGTINBHXH):
    case REQUEST(ACTION_TYPES.UPDATE_THONGTINBHXH):
    case REQUEST(ACTION_TYPES.DELETE_THONGTINBHXH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_THONGTINBHXH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_THONGTINBHXH):
    case FAILURE(ACTION_TYPES.CREATE_THONGTINBHXH):
    case FAILURE(ACTION_TYPES.UPDATE_THONGTINBHXH):
    case FAILURE(ACTION_TYPES.DELETE_THONGTINBHXH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_THONGTINBHXH_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_THONGTINBHXH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_THONGTINBHXH):
    case SUCCESS(ACTION_TYPES.UPDATE_THONGTINBHXH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_THONGTINBHXH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/thong-tin-bhxhs';

// Actions

export const getEntities: ICrudGetAllAction<IThongTinBhxh> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_THONGTINBHXH_LIST,
    payload: axios.get<IThongTinBhxh>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IThongTinBhxh> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_THONGTINBHXH,
    payload: axios.get<IThongTinBhxh>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IThongTinBhxh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_THONGTINBHXH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IThongTinBhxh> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_THONGTINBHXH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IThongTinBhxh> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_THONGTINBHXH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
