import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Col, Label, Row, UncontrolledTooltip} from 'reactstrap';
import {AvFeedback, AvField, AvForm, AvGroup, AvInput} from 'availity-reactstrap-validation';
import {Translate, translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {IRootState} from 'app/shared/reducers';
import {getEntities as getDonVis} from 'app/entities/khamchuabenh/don-vi/don-vi.reducer';
import {createEntity, getEntity, reset, updateEntity} from './thong-tin-bhxh.reducer';

export interface IThongTinBhxhUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const ThongTinBhxhUpdate = (props: IThongTinBhxhUpdateProps) => {
  const [donViId, setDonViId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const {thongTinBhxhEntity, donVis, loading, updating} = props;

  const handleClose = () => {
    props.history.push('/thong-tin-bhxh' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getDonVis();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...thongTinBhxhEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhThongTinBhxh.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.home.createOrEditLabel">Create or edit a
              ThongTinBhxh</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : thongTinBhxhEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="thong-tin-bhxh-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="thong-tin-bhxh-id" type="text" className="form-control" name="id" required readOnly/>
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="dvttLabel" for="thong-tin-bhxh-dvtt">
                  <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.dvtt">Dvtt</Translate>
                </Label>
                <AvField
                  id="thong-tin-bhxh-dvtt"
                  type="text"
                  name="dvtt"
                  validate={{
                    required: {value: true, errorMessage: translate('entity.validation.required')},
                    maxLength: {value: 10, errorMessage: translate('entity.validation.maxlength', {max: 10})}
                  }}
                />
                <UncontrolledTooltip target="dvttLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.help.dvtt"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup check>
                <Label id="enabledLabel">
                  <AvInput id="thong-tin-bhxh-enabled" type="checkbox" className="form-check-input" name="enabled"/>
                  <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.enabled">Enabled</Translate>
                </Label>
                <UncontrolledTooltip target="enabledLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.help.enabled"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ngayApDungBhytLabel" for="thong-tin-bhxh-ngayApDungBhyt">
                  <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.ngayApDungBhyt">Ngay Ap Dung
                    Bhyt</Translate>
                </Label>
                <AvField
                  id="thong-tin-bhxh-ngayApDungBhyt"
                  type="date"
                  className="form-control"
                  name="ngayApDungBhyt"
                  validate={{
                    required: {value: true, errorMessage: translate('entity.validation.required')}
                  }}
                />
                <UncontrolledTooltip target="ngayApDungBhytLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.help.ngayApDungBhyt"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenLabel" for="thong-tin-bhxh-ten">
                  <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.ten">Ten</Translate>
                </Label>
                <AvField
                  id="thong-tin-bhxh-ten"
                  type="text"
                  name="ten"
                  validate={{
                    required: {value: true, errorMessage: translate('entity.validation.required')},
                    maxLength: {value: 500, errorMessage: translate('entity.validation.maxlength', {max: 500})}
                  }}
                />
                <UncontrolledTooltip target="tenLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.help.ten"/>
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="thong-tin-bhxh-donVi">
                  <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.donVi">Don Vi</Translate>
                </Label>
                <AvInput id="thong-tin-bhxh-donVi" type="select" className="form-control" name="donViId" required>
                  {donVis
                    ? donVis.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/thong-tin-bhxh" replace color="info">
                <FontAwesomeIcon icon="arrow-left"/>
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save"/>
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  donVis: storeState.donVi.entities,
  thongTinBhxhEntity: storeState.thongTinBhxh.entity,
  loading: storeState.thongTinBhxh.loading,
  updating: storeState.thongTinBhxh.updating,
  updateSuccess: storeState.thongTinBhxh.updateSuccess
});

const mapDispatchToProps = {
  getDonVis,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ThongTinBhxhUpdate);
