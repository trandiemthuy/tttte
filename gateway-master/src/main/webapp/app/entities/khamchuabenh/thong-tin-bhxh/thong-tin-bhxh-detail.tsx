import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Col, Row, UncontrolledTooltip} from 'reactstrap';
import {TextFormat, Translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {IRootState} from 'app/shared/reducers';
import {getEntity} from './thong-tin-bhxh.reducer';
import {APP_LOCAL_DATE_FORMAT} from 'app/config/constants';

export interface IThongTinBhxhDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {
}

export const ThongTinBhxhDetail = (props: IThongTinBhxhDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const {thongTinBhxhEntity} = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate
            contentKey="gatewayApp.khamchuabenhThongTinBhxh.detail.title">ThongTinBhxh</Translate> [<b>{thongTinBhxhEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="dvtt">
              <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.dvtt">Dvtt</Translate>
            </span>
            <UncontrolledTooltip target="dvtt">
              <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.help.dvtt"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{thongTinBhxhEntity.dvtt}</dd>
          <dt>
            <span id="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.enabled">Enabled</Translate>
            </span>
            <UncontrolledTooltip target="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.help.enabled"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{thongTinBhxhEntity.enabled ? 'true' : 'false'}</dd>
          <dt>
            <span id="ngayApDungBhyt">
              <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.ngayApDungBhyt">Ngay Ap Dung Bhyt</Translate>
            </span>
            <UncontrolledTooltip target="ngayApDungBhyt">
              <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.help.ngayApDungBhyt"/>
            </UncontrolledTooltip>
          </dt>
          <dd>
            <TextFormat value={thongTinBhxhEntity.ngayApDungBhyt} type="date" format={APP_LOCAL_DATE_FORMAT}/>
          </dd>
          <dt>
            <span id="ten">
              <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.ten">Ten</Translate>
            </span>
            <UncontrolledTooltip target="ten">
              <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.help.ten"/>
            </UncontrolledTooltip>
          </dt>
          <dd>{thongTinBhxhEntity.ten}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.donVi">Don Vi</Translate>
          </dt>
          <dd>{thongTinBhxhEntity.donViId ? thongTinBhxhEntity.donViId : ''}</dd>
        </dl>
        <Button tag={Link} to="/thong-tin-bhxh" replace color="info">
          <FontAwesomeIcon icon="arrow-left"/>{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/thong-tin-bhxh/${thongTinBhxhEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt"/>{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({thongTinBhxh}: IRootState) => ({
  thongTinBhxhEntity: thongTinBhxh.entity
});

const mapDispatchToProps = {getEntity};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ThongTinBhxhDetail);
