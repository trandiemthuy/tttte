import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Row, Table} from 'reactstrap';
import {getSortState, JhiItemCount, JhiPagination, TextFormat, Translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {IRootState} from 'app/shared/reducers';
import {getEntities} from './thong-tin-bhxh.reducer';
import {APP_LOCAL_DATE_FORMAT} from 'app/config/constants';
import {ITEMS_PER_PAGE} from 'app/shared/util/pagination.constants';

export interface IThongTinBhxhProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {
}

export const ThongTinBhxh = (props: IThongTinBhxhProps) => {
  const [paginationState, setPaginationState] = useState(getSortState(props.location, ITEMS_PER_PAGE));

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  useEffect(() => {
    getAllEntities();
  }, []);

  const sortEntities = () => {
    getAllEntities();
    props.history.push(
      `${props.location.pathname}?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`
    );
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage
    });

  const {thongTinBhxhList, match, totalItems} = props;
  return (
    <div>
      <h2 id="thong-tin-bhxh-heading">
        <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.home.title">Thong Tin Bhxhs</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus"/>
          &nbsp;
          <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.home.createLabel">Create new Thong Tin
            Bhxh</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {thongTinBhxhList && thongTinBhxhList.length > 0 ? (
          <Table responsive>
            <thead>
            <tr>
              <th className="hand" onClick={sort('id')}>
                <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort"/>
              </th>
              <th className="hand" onClick={sort('dvtt')}>
                <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.dvtt">Dvtt</Translate> <FontAwesomeIcon
                icon="sort"/>
              </th>
              <th className="hand" onClick={sort('enabled')}>
                <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.enabled">Enabled</Translate> <FontAwesomeIcon
                icon="sort"/>
              </th>
              <th className="hand" onClick={sort('ngayApDungBhyt')}>
                <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.ngayApDungBhyt">Ngay Ap Dung
                  Bhyt</Translate>{' '}
                <FontAwesomeIcon icon="sort"/>
              </th>
              <th className="hand" onClick={sort('ten')}>
                <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.ten">Ten</Translate> <FontAwesomeIcon
                icon="sort"/>
              </th>
              <th>
                <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.donVi">Don Vi</Translate> <FontAwesomeIcon
                icon="sort"/>
              </th>
              <th/>
            </tr>
            </thead>
            <tbody>
            {thongTinBhxhList.map((thongTinBhxh, i) => (
              <tr key={`entity-${i}`}>
                <td>
                  <Button tag={Link} to={`${match.url}/${thongTinBhxh.id}`} color="link" size="sm">
                    {thongTinBhxh.id}
                  </Button>
                </td>
                <td>{thongTinBhxh.dvtt}</td>
                <td>{thongTinBhxh.enabled ? 'true' : 'false'}</td>
                <td>
                  <TextFormat type="date" value={thongTinBhxh.ngayApDungBhyt} format={APP_LOCAL_DATE_FORMAT}/>
                </td>
                <td>{thongTinBhxh.ten}</td>
                <td>{thongTinBhxh.donViId ?
                  <Link to={`don-vi/${thongTinBhxh.donViId}`}>{thongTinBhxh.donViId}</Link> : ''}</td>
                <td className="text-right">
                  <div className="btn-group flex-btn-group-container">
                    <Button tag={Link} to={`${match.url}/${thongTinBhxh.id}`} color="info" size="sm">
                      <FontAwesomeIcon icon="eye"/>{' '}
                      <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                    </Button>
                    <Button
                      tag={Link}
                      to={`${match.url}/${thongTinBhxh.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                      color="primary"
                      size="sm"
                    >
                      <FontAwesomeIcon icon="pencil-alt"/>{' '}
                      <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                    </Button>
                    <Button
                      tag={Link}
                      to={`${match.url}/${thongTinBhxh.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                      color="danger"
                      size="sm"
                    >
                      <FontAwesomeIcon icon="trash"/>{' '}
                      <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                    </Button>
                  </div>
                </td>
              </tr>
            ))}
            </tbody>
          </Table>
        ) : (
          <div className="alert alert-warning">
            <Translate contentKey="gatewayApp.khamchuabenhThongTinBhxh.home.notFound">No Thong Tin Bhxhs
              found</Translate>
          </div>
        )}
      </div>
      <div className={thongTinBhxhList && thongTinBhxhList.length > 0 ? '' : 'd-none'}>
        <Row className="justify-content-center">
          <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage}
                        i18nEnabled/>
        </Row>
        <Row className="justify-content-center">
          <JhiPagination
            activePage={paginationState.activePage}
            onSelect={handlePagination}
            maxButtons={5}
            itemsPerPage={paginationState.itemsPerPage}
            totalItems={props.totalItems}
          />
        </Row>
      </div>
    </div>
  );
};

const mapStateToProps = ({thongTinBhxh}: IRootState) => ({
  thongTinBhxhList: thongTinBhxh.entities,
  totalItems: thongTinBhxh.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ThongTinBhxh);
