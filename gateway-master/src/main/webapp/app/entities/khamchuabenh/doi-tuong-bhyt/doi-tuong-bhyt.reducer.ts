import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IDoiTuongBhyt, defaultValue } from 'app/shared/model/khamchuabenh/doi-tuong-bhyt.model';

export const ACTION_TYPES = {
  FETCH_DOITUONGBHYT_LIST: 'doiTuongBhyt/FETCH_DOITUONGBHYT_LIST',
  FETCH_DOITUONGBHYT: 'doiTuongBhyt/FETCH_DOITUONGBHYT',
  CREATE_DOITUONGBHYT: 'doiTuongBhyt/CREATE_DOITUONGBHYT',
  UPDATE_DOITUONGBHYT: 'doiTuongBhyt/UPDATE_DOITUONGBHYT',
  DELETE_DOITUONGBHYT: 'doiTuongBhyt/DELETE_DOITUONGBHYT',
  RESET: 'doiTuongBhyt/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IDoiTuongBhyt>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type DoiTuongBhytState = Readonly<typeof initialState>;

// Reducer

export default (state: DoiTuongBhytState = initialState, action): DoiTuongBhytState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_DOITUONGBHYT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_DOITUONGBHYT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_DOITUONGBHYT):
    case REQUEST(ACTION_TYPES.UPDATE_DOITUONGBHYT):
    case REQUEST(ACTION_TYPES.DELETE_DOITUONGBHYT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_DOITUONGBHYT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_DOITUONGBHYT):
    case FAILURE(ACTION_TYPES.CREATE_DOITUONGBHYT):
    case FAILURE(ACTION_TYPES.UPDATE_DOITUONGBHYT):
    case FAILURE(ACTION_TYPES.DELETE_DOITUONGBHYT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_DOITUONGBHYT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_DOITUONGBHYT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_DOITUONGBHYT):
    case SUCCESS(ACTION_TYPES.UPDATE_DOITUONGBHYT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_DOITUONGBHYT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/doi-tuong-bhyts';

// Actions

export const getEntities: ICrudGetAllAction<IDoiTuongBhyt> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_DOITUONGBHYT_LIST,
  payload: axios.get<IDoiTuongBhyt>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IDoiTuongBhyt> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_DOITUONGBHYT,
    payload: axios.get<IDoiTuongBhyt>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IDoiTuongBhyt> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_DOITUONGBHYT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IDoiTuongBhyt> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_DOITUONGBHYT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IDoiTuongBhyt> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_DOITUONGBHYT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
