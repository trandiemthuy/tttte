import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Translate, ICrudGetAction, ICrudDeleteAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IDoiTuongBhyt } from 'app/shared/model/khamchuabenh/doi-tuong-bhyt.model';
import { IRootState } from 'app/shared/reducers';
import { getEntity, deleteEntity } from './doi-tuong-bhyt.reducer';

export interface IDoiTuongBhytDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DoiTuongBhytDeleteDialog = (props: IDoiTuongBhytDeleteDialogProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const handleClose = () => {
    props.history.push('/doi-tuong-bhyt');
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const confirmDelete = () => {
    props.deleteEntity(props.doiTuongBhytEntity.id);
  };

  const { doiTuongBhytEntity } = props;
  return (
    <Modal isOpen toggle={handleClose}>
      <ModalHeader toggle={handleClose}>
        <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
      </ModalHeader>
      <ModalBody id="gatewayApp.khamchuabenhDoiTuongBhyt.delete.question">
        <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.delete.question" interpolate={{ id: doiTuongBhytEntity.id }}>
          Are you sure you want to delete this DoiTuongBhyt?
        </Translate>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={handleClose}>
          <FontAwesomeIcon icon="ban" />
          &nbsp;
          <Translate contentKey="entity.action.cancel">Cancel</Translate>
        </Button>
        <Button id="jhi-confirm-delete-doiTuongBhyt" color="danger" onClick={confirmDelete}>
          <FontAwesomeIcon icon="trash" />
          &nbsp;
          <Translate contentKey="entity.action.delete">Delete</Translate>
        </Button>
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = ({ doiTuongBhyt }: IRootState) => ({
  doiTuongBhytEntity: doiTuongBhyt.entity,
  updateSuccess: doiTuongBhyt.updateSuccess
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DoiTuongBhytDeleteDialog);
