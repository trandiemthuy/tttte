import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import DoiTuongBhyt from './doi-tuong-bhyt';
import DoiTuongBhytDetail from './doi-tuong-bhyt-detail';
import DoiTuongBhytUpdate from './doi-tuong-bhyt-update';
import DoiTuongBhytDeleteDialog from './doi-tuong-bhyt-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={DoiTuongBhytDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={DoiTuongBhytUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={DoiTuongBhytUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={DoiTuongBhytDetail} />
      <ErrorBoundaryRoute path={match.url} component={DoiTuongBhyt} />
    </Switch>
  </>
);

export default Routes;
