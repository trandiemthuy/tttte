import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { INhomDoiTuongBhyt } from 'app/shared/model/khamchuabenh/nhom-doi-tuong-bhyt.model';
import { getEntities as getNhomDoiTuongBhyts } from 'app/entities/khamchuabenh/nhom-doi-tuong-bhyt/nhom-doi-tuong-bhyt.reducer';
import { getEntity, updateEntity, createEntity, reset } from './doi-tuong-bhyt.reducer';
import { IDoiTuongBhyt } from 'app/shared/model/khamchuabenh/doi-tuong-bhyt.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IDoiTuongBhytUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DoiTuongBhytUpdate = (props: IDoiTuongBhytUpdateProps) => {
  const [nhomDoiTuongBhytId, setNhomDoiTuongBhytId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { doiTuongBhytEntity, nhomDoiTuongBhyts, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/doi-tuong-bhyt');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getNhomDoiTuongBhyts();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...doiTuongBhytEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhDoiTuongBhyt.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.home.createOrEditLabel">Create or edit a DoiTuongBhyt</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : doiTuongBhytEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="doi-tuong-bhyt-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="doi-tuong-bhyt-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="canTrenLabel" for="doi-tuong-bhyt-canTren">
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.canTren">Can Tren</Translate>
                </Label>
                <AvField
                  id="doi-tuong-bhyt-canTren"
                  type="string"
                  className="form-control"
                  name="canTren"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="canTrenLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.help.canTren" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="canTrenKtcLabel" for="doi-tuong-bhyt-canTrenKtc">
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.canTrenKtc">Can Tren Ktc</Translate>
                </Label>
                <AvField
                  id="doi-tuong-bhyt-canTrenKtc"
                  type="string"
                  className="form-control"
                  name="canTrenKtc"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="canTrenKtcLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.help.canTrenKtc" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="maLabel" for="doi-tuong-bhyt-ma">
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.ma">Ma</Translate>
                </Label>
                <AvField
                  id="doi-tuong-bhyt-ma"
                  type="text"
                  name="ma"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 10, errorMessage: translate('entity.validation.maxlength', { max: 10 }) }
                  }}
                />
                <UncontrolledTooltip target="maLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.help.ma" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenLabel" for="doi-tuong-bhyt-ten">
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.ten">Ten</Translate>
                </Label>
                <AvField
                  id="doi-tuong-bhyt-ten"
                  type="text"
                  name="ten"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="tenLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.help.ten" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tyLeMienGiamLabel" for="doi-tuong-bhyt-tyLeMienGiam">
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.tyLeMienGiam">Ty Le Mien Giam</Translate>
                </Label>
                <AvField
                  id="doi-tuong-bhyt-tyLeMienGiam"
                  type="string"
                  className="form-control"
                  name="tyLeMienGiam"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="tyLeMienGiamLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.help.tyLeMienGiam" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tyLeMienGiamKtcLabel" for="doi-tuong-bhyt-tyLeMienGiamKtc">
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.tyLeMienGiamKtc">Ty Le Mien Giam Ktc</Translate>
                </Label>
                <AvField
                  id="doi-tuong-bhyt-tyLeMienGiamKtc"
                  type="string"
                  className="form-control"
                  name="tyLeMienGiamKtc"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="tyLeMienGiamKtcLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.help.tyLeMienGiamKtc" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="doi-tuong-bhyt-nhomDoiTuongBhyt">
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.nhomDoiTuongBhyt">Nhom Doi Tuong Bhyt</Translate>
                </Label>
                <AvInput id="doi-tuong-bhyt-nhomDoiTuongBhyt" type="select" className="form-control" name="nhomDoiTuongBhytId" required>
                  {nhomDoiTuongBhyts
                    ? nhomDoiTuongBhyts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/doi-tuong-bhyt" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  nhomDoiTuongBhyts: storeState.nhomDoiTuongBhyt.entities,
  doiTuongBhytEntity: storeState.doiTuongBhyt.entity,
  loading: storeState.doiTuongBhyt.loading,
  updating: storeState.doiTuongBhyt.updating,
  updateSuccess: storeState.doiTuongBhyt.updateSuccess
});

const mapDispatchToProps = {
  getNhomDoiTuongBhyts,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DoiTuongBhytUpdate);
