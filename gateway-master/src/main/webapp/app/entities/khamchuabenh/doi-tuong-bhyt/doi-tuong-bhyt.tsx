import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './doi-tuong-bhyt.reducer';
import { IDoiTuongBhyt } from 'app/shared/model/khamchuabenh/doi-tuong-bhyt.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IDoiTuongBhytProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const DoiTuongBhyt = (props: IDoiTuongBhytProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { doiTuongBhytList, match } = props;
  return (
    <div>
      <h2 id="doi-tuong-bhyt-heading">
        <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.home.title">Doi Tuong Bhyts</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.home.createLabel">Create new Doi Tuong Bhyt</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {doiTuongBhytList && doiTuongBhytList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.canTren">Can Tren</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.canTrenKtc">Can Tren Ktc</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.ma">Ma</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.ten">Ten</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.tyLeMienGiam">Ty Le Mien Giam</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.tyLeMienGiamKtc">Ty Le Mien Giam Ktc</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.nhomDoiTuongBhyt">Nhom Doi Tuong Bhyt</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {doiTuongBhytList.map((doiTuongBhyt, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${doiTuongBhyt.id}`} color="link" size="sm">
                      {doiTuongBhyt.id}
                    </Button>
                  </td>
                  <td>{doiTuongBhyt.canTren}</td>
                  <td>{doiTuongBhyt.canTrenKtc}</td>
                  <td>{doiTuongBhyt.ma}</td>
                  <td>{doiTuongBhyt.ten}</td>
                  <td>{doiTuongBhyt.tyLeMienGiam}</td>
                  <td>{doiTuongBhyt.tyLeMienGiamKtc}</td>
                  <td>
                    {doiTuongBhyt.nhomDoiTuongBhytId ? (
                      <Link to={`nhom-doi-tuong-bhyt/${doiTuongBhyt.nhomDoiTuongBhytId}`}>{doiTuongBhyt.nhomDoiTuongBhytId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${doiTuongBhyt.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${doiTuongBhyt.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${doiTuongBhyt.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          <div className="alert alert-warning">
            <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.home.notFound">No Doi Tuong Bhyts found</Translate>
          </div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ doiTuongBhyt }: IRootState) => ({
  doiTuongBhytList: doiTuongBhyt.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DoiTuongBhyt);
