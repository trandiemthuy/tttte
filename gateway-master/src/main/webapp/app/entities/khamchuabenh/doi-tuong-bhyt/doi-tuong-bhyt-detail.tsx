import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './doi-tuong-bhyt.reducer';
import { IDoiTuongBhyt } from 'app/shared/model/khamchuabenh/doi-tuong-bhyt.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IDoiTuongBhytDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DoiTuongBhytDetail = (props: IDoiTuongBhytDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { doiTuongBhytEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.detail.title">DoiTuongBhyt</Translate> [<b>{doiTuongBhytEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="canTren">
              <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.canTren">Can Tren</Translate>
            </span>
            <UncontrolledTooltip target="canTren">
              <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.help.canTren" />
            </UncontrolledTooltip>
          </dt>
          <dd>{doiTuongBhytEntity.canTren}</dd>
          <dt>
            <span id="canTrenKtc">
              <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.canTrenKtc">Can Tren Ktc</Translate>
            </span>
            <UncontrolledTooltip target="canTrenKtc">
              <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.help.canTrenKtc" />
            </UncontrolledTooltip>
          </dt>
          <dd>{doiTuongBhytEntity.canTrenKtc}</dd>
          <dt>
            <span id="ma">
              <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.ma">Ma</Translate>
            </span>
            <UncontrolledTooltip target="ma">
              <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.help.ma" />
            </UncontrolledTooltip>
          </dt>
          <dd>{doiTuongBhytEntity.ma}</dd>
          <dt>
            <span id="ten">
              <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.ten">Ten</Translate>
            </span>
            <UncontrolledTooltip target="ten">
              <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.help.ten" />
            </UncontrolledTooltip>
          </dt>
          <dd>{doiTuongBhytEntity.ten}</dd>
          <dt>
            <span id="tyLeMienGiam">
              <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.tyLeMienGiam">Ty Le Mien Giam</Translate>
            </span>
            <UncontrolledTooltip target="tyLeMienGiam">
              <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.help.tyLeMienGiam" />
            </UncontrolledTooltip>
          </dt>
          <dd>{doiTuongBhytEntity.tyLeMienGiam}</dd>
          <dt>
            <span id="tyLeMienGiamKtc">
              <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.tyLeMienGiamKtc">Ty Le Mien Giam Ktc</Translate>
            </span>
            <UncontrolledTooltip target="tyLeMienGiamKtc">
              <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.help.tyLeMienGiamKtc" />
            </UncontrolledTooltip>
          </dt>
          <dd>{doiTuongBhytEntity.tyLeMienGiamKtc}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhDoiTuongBhyt.nhomDoiTuongBhyt">Nhom Doi Tuong Bhyt</Translate>
          </dt>
          <dd>{doiTuongBhytEntity.nhomDoiTuongBhytId ? doiTuongBhytEntity.nhomDoiTuongBhytId : ''}</dd>
        </dl>
        <Button tag={Link} to="/doi-tuong-bhyt" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/doi-tuong-bhyt/${doiTuongBhytEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ doiTuongBhyt }: IRootState) => ({
  doiTuongBhytEntity: doiTuongBhyt.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DoiTuongBhytDetail);
