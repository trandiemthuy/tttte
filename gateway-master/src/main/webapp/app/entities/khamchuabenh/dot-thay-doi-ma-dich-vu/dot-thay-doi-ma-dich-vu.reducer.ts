import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IDotThayDoiMaDichVu, defaultValue } from 'app/shared/model/khamchuabenh/dot-thay-doi-ma-dich-vu.model';

export const ACTION_TYPES = {
  FETCH_DOTTHAYDOIMADICHVU_LIST: 'dotThayDoiMaDichVu/FETCH_DOTTHAYDOIMADICHVU_LIST',
  FETCH_DOTTHAYDOIMADICHVU: 'dotThayDoiMaDichVu/FETCH_DOTTHAYDOIMADICHVU',
  CREATE_DOTTHAYDOIMADICHVU: 'dotThayDoiMaDichVu/CREATE_DOTTHAYDOIMADICHVU',
  UPDATE_DOTTHAYDOIMADICHVU: 'dotThayDoiMaDichVu/UPDATE_DOTTHAYDOIMADICHVU',
  DELETE_DOTTHAYDOIMADICHVU: 'dotThayDoiMaDichVu/DELETE_DOTTHAYDOIMADICHVU',
  RESET: 'dotThayDoiMaDichVu/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IDotThayDoiMaDichVu>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type DotThayDoiMaDichVuState = Readonly<typeof initialState>;

// Reducer

export default (state: DotThayDoiMaDichVuState = initialState, action): DotThayDoiMaDichVuState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_DOTTHAYDOIMADICHVU_LIST):
    case REQUEST(ACTION_TYPES.FETCH_DOTTHAYDOIMADICHVU):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_DOTTHAYDOIMADICHVU):
    case REQUEST(ACTION_TYPES.UPDATE_DOTTHAYDOIMADICHVU):
    case REQUEST(ACTION_TYPES.DELETE_DOTTHAYDOIMADICHVU):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_DOTTHAYDOIMADICHVU_LIST):
    case FAILURE(ACTION_TYPES.FETCH_DOTTHAYDOIMADICHVU):
    case FAILURE(ACTION_TYPES.CREATE_DOTTHAYDOIMADICHVU):
    case FAILURE(ACTION_TYPES.UPDATE_DOTTHAYDOIMADICHVU):
    case FAILURE(ACTION_TYPES.DELETE_DOTTHAYDOIMADICHVU):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_DOTTHAYDOIMADICHVU_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_DOTTHAYDOIMADICHVU):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_DOTTHAYDOIMADICHVU):
    case SUCCESS(ACTION_TYPES.UPDATE_DOTTHAYDOIMADICHVU):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_DOTTHAYDOIMADICHVU):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/dot-thay-doi-ma-dich-vus';

// Actions

export const getEntities: ICrudGetAllAction<IDotThayDoiMaDichVu> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_DOTTHAYDOIMADICHVU_LIST,
    payload: axios.get<IDotThayDoiMaDichVu>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IDotThayDoiMaDichVu> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_DOTTHAYDOIMADICHVU,
    payload: axios.get<IDotThayDoiMaDichVu>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IDotThayDoiMaDichVu> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_DOTTHAYDOIMADICHVU,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IDotThayDoiMaDichVu> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_DOTTHAYDOIMADICHVU,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IDotThayDoiMaDichVu> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_DOTTHAYDOIMADICHVU,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
