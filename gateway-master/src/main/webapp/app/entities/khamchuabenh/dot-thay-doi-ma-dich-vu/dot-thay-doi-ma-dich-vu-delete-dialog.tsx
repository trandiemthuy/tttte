import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Translate, ICrudGetAction, ICrudDeleteAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IDotThayDoiMaDichVu } from 'app/shared/model/khamchuabenh/dot-thay-doi-ma-dich-vu.model';
import { IRootState } from 'app/shared/reducers';
import { getEntity, deleteEntity } from './dot-thay-doi-ma-dich-vu.reducer';

export interface IDotThayDoiMaDichVuDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DotThayDoiMaDichVuDeleteDialog = (props: IDotThayDoiMaDichVuDeleteDialogProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const handleClose = () => {
    props.history.push('/dot-thay-doi-ma-dich-vu' + props.location.search);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const confirmDelete = () => {
    props.deleteEntity(props.dotThayDoiMaDichVuEntity.id);
  };

  const { dotThayDoiMaDichVuEntity } = props;
  return (
    <Modal isOpen toggle={handleClose}>
      <ModalHeader toggle={handleClose}>
        <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
      </ModalHeader>
      <ModalBody id="gatewayApp.khamchuabenhDotThayDoiMaDichVu.delete.question">
        <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.delete.question" interpolate={{ id: dotThayDoiMaDichVuEntity.id }}>
          Are you sure you want to delete this DotThayDoiMaDichVu?
        </Translate>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={handleClose}>
          <FontAwesomeIcon icon="ban" />
          &nbsp;
          <Translate contentKey="entity.action.cancel">Cancel</Translate>
        </Button>
        <Button id="jhi-confirm-delete-dotThayDoiMaDichVu" color="danger" onClick={confirmDelete}>
          <FontAwesomeIcon icon="trash" />
          &nbsp;
          <Translate contentKey="entity.action.delete">Delete</Translate>
        </Button>
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = ({ dotThayDoiMaDichVu }: IRootState) => ({
  dotThayDoiMaDichVuEntity: dotThayDoiMaDichVu.entity,
  updateSuccess: dotThayDoiMaDichVu.updateSuccess
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DotThayDoiMaDichVuDeleteDialog);
