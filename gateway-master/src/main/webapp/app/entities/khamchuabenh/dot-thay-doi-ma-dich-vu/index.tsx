import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import DotThayDoiMaDichVu from './dot-thay-doi-ma-dich-vu';
import DotThayDoiMaDichVuDetail from './dot-thay-doi-ma-dich-vu-detail';
import DotThayDoiMaDichVuUpdate from './dot-thay-doi-ma-dich-vu-update';
import DotThayDoiMaDichVuDeleteDialog from './dot-thay-doi-ma-dich-vu-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={DotThayDoiMaDichVuDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={DotThayDoiMaDichVuUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={DotThayDoiMaDichVuUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={DotThayDoiMaDichVuDetail} />
      <ErrorBoundaryRoute path={match.url} component={DotThayDoiMaDichVu} />
    </Switch>
  </>
);

export default Routes;
