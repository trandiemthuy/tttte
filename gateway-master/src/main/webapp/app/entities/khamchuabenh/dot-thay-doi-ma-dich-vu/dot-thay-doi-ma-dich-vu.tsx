import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './dot-thay-doi-ma-dich-vu.reducer';
import { IDotThayDoiMaDichVu } from 'app/shared/model/khamchuabenh/dot-thay-doi-ma-dich-vu.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IDotThayDoiMaDichVuProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const DotThayDoiMaDichVu = (props: IDotThayDoiMaDichVuProps) => {
  const [paginationState, setPaginationState] = useState(getSortState(props.location, ITEMS_PER_PAGE));

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  useEffect(() => {
    getAllEntities();
  }, []);

  const sortEntities = () => {
    getAllEntities();
    props.history.push(
      `${props.location.pathname}?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`
    );
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage
    });

  const { dotThayDoiMaDichVuList, match, totalItems } = props;
  return (
    <div>
      <h2 id="dot-thay-doi-ma-dich-vu-heading">
        <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.home.title">Dot Thay Doi Ma Dich Vus</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.home.createLabel">Create new Dot Thay Doi Ma Dich Vu</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {dotThayDoiMaDichVuList && dotThayDoiMaDichVuList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('doiTuongApDung')}>
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.doiTuongApDung">Doi Tuong Ap Dung</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ghiChu')}>
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.ghiChu">Ghi Chu</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ngayApDung')}>
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.ngayApDung">Ngay Ap Dung</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('ten')}>
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.ten">Ten</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.donVi">Don Vi</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {dotThayDoiMaDichVuList.map((dotThayDoiMaDichVu, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${dotThayDoiMaDichVu.id}`} color="link" size="sm">
                      {dotThayDoiMaDichVu.id}
                    </Button>
                  </td>
                  <td>{dotThayDoiMaDichVu.doiTuongApDung}</td>
                  <td>{dotThayDoiMaDichVu.ghiChu}</td>
                  <td>
                    <TextFormat type="date" value={dotThayDoiMaDichVu.ngayApDung} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{dotThayDoiMaDichVu.ten}</td>
                  <td>
                    {dotThayDoiMaDichVu.donViId ? (
                      <Link to={`don-vi/${dotThayDoiMaDichVu.donViId}`}>{dotThayDoiMaDichVu.donViId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${dotThayDoiMaDichVu.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${dotThayDoiMaDichVu.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="primary"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${dotThayDoiMaDichVu.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="danger"
                        size="sm"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          <div className="alert alert-warning">
            <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.home.notFound">No Dot Thay Doi Ma Dich Vus found</Translate>
          </div>
        )}
      </div>
      <div className={dotThayDoiMaDichVuList && dotThayDoiMaDichVuList.length > 0 ? '' : 'd-none'}>
        <Row className="justify-content-center">
          <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} i18nEnabled />
        </Row>
        <Row className="justify-content-center">
          <JhiPagination
            activePage={paginationState.activePage}
            onSelect={handlePagination}
            maxButtons={5}
            itemsPerPage={paginationState.itemsPerPage}
            totalItems={props.totalItems}
          />
        </Row>
      </div>
    </div>
  );
};

const mapStateToProps = ({ dotThayDoiMaDichVu }: IRootState) => ({
  dotThayDoiMaDichVuList: dotThayDoiMaDichVu.entities,
  totalItems: dotThayDoiMaDichVu.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DotThayDoiMaDichVu);
