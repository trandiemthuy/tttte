import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IDonVi } from 'app/shared/model/khamchuabenh/don-vi.model';
import { getEntities as getDonVis } from 'app/entities/khamchuabenh/don-vi/don-vi.reducer';
import { getEntity, updateEntity, createEntity, reset } from './dot-thay-doi-ma-dich-vu.reducer';
import { IDotThayDoiMaDichVu } from 'app/shared/model/khamchuabenh/dot-thay-doi-ma-dich-vu.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IDotThayDoiMaDichVuUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DotThayDoiMaDichVuUpdate = (props: IDotThayDoiMaDichVuUpdateProps) => {
  const [donViId, setDonViId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { dotThayDoiMaDichVuEntity, donVis, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/dot-thay-doi-ma-dich-vu' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getDonVis();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...dotThayDoiMaDichVuEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhDotThayDoiMaDichVu.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.home.createOrEditLabel">
              Create or edit a DotThayDoiMaDichVu
            </Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : dotThayDoiMaDichVuEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="dot-thay-doi-ma-dich-vu-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="dot-thay-doi-ma-dich-vu-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="doiTuongApDungLabel" for="dot-thay-doi-ma-dich-vu-doiTuongApDung">
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.doiTuongApDung">Doi Tuong Ap Dung</Translate>
                </Label>
                <AvField id="dot-thay-doi-ma-dich-vu-doiTuongApDung" type="string" className="form-control" name="doiTuongApDung" />
                <UncontrolledTooltip target="doiTuongApDungLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.help.doiTuongApDung" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ghiChuLabel" for="dot-thay-doi-ma-dich-vu-ghiChu">
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.ghiChu">Ghi Chu</Translate>
                </Label>
                <AvField
                  id="dot-thay-doi-ma-dich-vu-ghiChu"
                  type="text"
                  name="ghiChu"
                  validate={{
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="ghiChuLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.help.ghiChu" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ngayApDungLabel" for="dot-thay-doi-ma-dich-vu-ngayApDung">
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.ngayApDung">Ngay Ap Dung</Translate>
                </Label>
                <AvField
                  id="dot-thay-doi-ma-dich-vu-ngayApDung"
                  type="date"
                  className="form-control"
                  name="ngayApDung"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') }
                  }}
                />
                <UncontrolledTooltip target="ngayApDungLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.help.ngayApDung" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenLabel" for="dot-thay-doi-ma-dich-vu-ten">
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.ten">Ten</Translate>
                </Label>
                <AvField
                  id="dot-thay-doi-ma-dich-vu-ten"
                  type="text"
                  name="ten"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') }
                  }}
                />
                <UncontrolledTooltip target="tenLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.help.ten" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="dot-thay-doi-ma-dich-vu-donVi">
                  <Translate contentKey="gatewayApp.khamchuabenhDotThayDoiMaDichVu.donVi">Don Vi</Translate>
                </Label>
                <AvInput id="dot-thay-doi-ma-dich-vu-donVi" type="select" className="form-control" name="donViId" required>
                  {donVis
                    ? donVis.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/dot-thay-doi-ma-dich-vu" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  donVis: storeState.donVi.entities,
  dotThayDoiMaDichVuEntity: storeState.dotThayDoiMaDichVu.entity,
  loading: storeState.dotThayDoiMaDichVu.loading,
  updating: storeState.dotThayDoiMaDichVu.updating,
  updateSuccess: storeState.dotThayDoiMaDichVu.updateSuccess
});

const mapDispatchToProps = {
  getDonVis,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DotThayDoiMaDichVuUpdate);
