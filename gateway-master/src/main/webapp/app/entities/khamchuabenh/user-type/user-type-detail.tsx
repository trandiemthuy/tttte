import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './user-type.reducer';
import { IUserType } from 'app/shared/model/khamchuabenh/user-type.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IUserTypeDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const UserTypeDetail = (props: IUserTypeDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { userTypeEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhUserType.detail.title">UserType</Translate> [<b>{userTypeEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="moTa">
              <Translate contentKey="gatewayApp.khamchuabenhUserType.moTa">Mo Ta</Translate>
            </span>
            <UncontrolledTooltip target="moTa">
              <Translate contentKey="gatewayApp.khamchuabenhUserType.help.moTa" />
            </UncontrolledTooltip>
          </dt>
          <dd>{userTypeEntity.moTa}</dd>
        </dl>
        <Button tag={Link} to="/user-type" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/user-type/${userTypeEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ userType }: IRootState) => ({
  userTypeEntity: userType.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(UserTypeDetail);
