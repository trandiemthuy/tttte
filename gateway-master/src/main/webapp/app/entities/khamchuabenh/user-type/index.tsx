import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import UserType from './user-type';
import UserTypeDetail from './user-type-detail';
import UserTypeUpdate from './user-type-update';
import UserTypeDeleteDialog from './user-type-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={UserTypeDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={UserTypeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={UserTypeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={UserTypeDetail} />
      <ErrorBoundaryRoute path={match.url} component={UserType} />
    </Switch>
  </>
);

export default Routes;
