import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IUserType, defaultValue } from 'app/shared/model/khamchuabenh/user-type.model';

export const ACTION_TYPES = {
  FETCH_USERTYPE_LIST: 'userType/FETCH_USERTYPE_LIST',
  FETCH_USERTYPE: 'userType/FETCH_USERTYPE',
  CREATE_USERTYPE: 'userType/CREATE_USERTYPE',
  UPDATE_USERTYPE: 'userType/UPDATE_USERTYPE',
  DELETE_USERTYPE: 'userType/DELETE_USERTYPE',
  RESET: 'userType/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IUserType>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type UserTypeState = Readonly<typeof initialState>;

// Reducer

export default (state: UserTypeState = initialState, action): UserTypeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_USERTYPE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_USERTYPE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_USERTYPE):
    case REQUEST(ACTION_TYPES.UPDATE_USERTYPE):
    case REQUEST(ACTION_TYPES.DELETE_USERTYPE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_USERTYPE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_USERTYPE):
    case FAILURE(ACTION_TYPES.CREATE_USERTYPE):
    case FAILURE(ACTION_TYPES.UPDATE_USERTYPE):
    case FAILURE(ACTION_TYPES.DELETE_USERTYPE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_USERTYPE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_USERTYPE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_USERTYPE):
    case SUCCESS(ACTION_TYPES.UPDATE_USERTYPE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_USERTYPE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/user-types';

// Actions

export const getEntities: ICrudGetAllAction<IUserType> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_USERTYPE_LIST,
    payload: axios.get<IUserType>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IUserType> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_USERTYPE,
    payload: axios.get<IUserType>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IUserType> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_USERTYPE,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IUserType> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_USERTYPE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IUserType> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_USERTYPE,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
