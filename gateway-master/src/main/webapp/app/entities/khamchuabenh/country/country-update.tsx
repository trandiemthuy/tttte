import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './country.reducer';
import { ICountry } from 'app/shared/model/khamchuabenh/country.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ICountryUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CountryUpdate = (props: ICountryUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { countryEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/country' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...countryEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhCountry.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhCountry.home.createOrEditLabel">Create or edit a Country</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : countryEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="country-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="country-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="isoCodeLabel" for="country-isoCode">
                  <Translate contentKey="gatewayApp.khamchuabenhCountry.isoCode">Iso Code</Translate>
                </Label>
                <AvField
                  id="country-isoCode"
                  type="text"
                  name="isoCode"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="isoCodeLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhCountry.help.isoCode" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="isoNumericLabel" for="country-isoNumeric">
                  <Translate contentKey="gatewayApp.khamchuabenhCountry.isoNumeric">Iso Numeric</Translate>
                </Label>
                <AvField
                  id="country-isoNumeric"
                  type="text"
                  name="isoNumeric"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="isoNumericLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhCountry.help.isoNumeric" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="nameLabel" for="country-name">
                  <Translate contentKey="gatewayApp.khamchuabenhCountry.name">Name</Translate>
                </Label>
                <AvField
                  id="country-name"
                  type="text"
                  name="name"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="nameLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhCountry.help.name" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="maCtkLabel" for="country-maCtk">
                  <Translate contentKey="gatewayApp.khamchuabenhCountry.maCtk">Ma Ctk</Translate>
                </Label>
                <AvField id="country-maCtk" type="string" className="form-control" name="maCtk" />
                <UncontrolledTooltip target="maCtkLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhCountry.help.maCtk" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenCtkLabel" for="country-tenCtk">
                  <Translate contentKey="gatewayApp.khamchuabenhCountry.tenCtk">Ten Ctk</Translate>
                </Label>
                <AvField
                  id="country-tenCtk"
                  type="text"
                  name="tenCtk"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                  }}
                />
                <UncontrolledTooltip target="tenCtkLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhCountry.help.tenCtk" />
                </UncontrolledTooltip>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/country" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  countryEntity: storeState.country.entity,
  loading: storeState.country.loading,
  updating: storeState.country.updating,
  updateSuccess: storeState.country.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CountryUpdate);
