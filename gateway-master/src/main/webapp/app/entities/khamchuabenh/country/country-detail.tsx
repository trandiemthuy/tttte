import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './country.reducer';
import { ICountry } from 'app/shared/model/khamchuabenh/country.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICountryDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CountryDetail = (props: ICountryDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { countryEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhCountry.detail.title">Country</Translate> [<b>{countryEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="isoCode">
              <Translate contentKey="gatewayApp.khamchuabenhCountry.isoCode">Iso Code</Translate>
            </span>
            <UncontrolledTooltip target="isoCode">
              <Translate contentKey="gatewayApp.khamchuabenhCountry.help.isoCode" />
            </UncontrolledTooltip>
          </dt>
          <dd>{countryEntity.isoCode}</dd>
          <dt>
            <span id="isoNumeric">
              <Translate contentKey="gatewayApp.khamchuabenhCountry.isoNumeric">Iso Numeric</Translate>
            </span>
            <UncontrolledTooltip target="isoNumeric">
              <Translate contentKey="gatewayApp.khamchuabenhCountry.help.isoNumeric" />
            </UncontrolledTooltip>
          </dt>
          <dd>{countryEntity.isoNumeric}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="gatewayApp.khamchuabenhCountry.name">Name</Translate>
            </span>
            <UncontrolledTooltip target="name">
              <Translate contentKey="gatewayApp.khamchuabenhCountry.help.name" />
            </UncontrolledTooltip>
          </dt>
          <dd>{countryEntity.name}</dd>
          <dt>
            <span id="maCtk">
              <Translate contentKey="gatewayApp.khamchuabenhCountry.maCtk">Ma Ctk</Translate>
            </span>
            <UncontrolledTooltip target="maCtk">
              <Translate contentKey="gatewayApp.khamchuabenhCountry.help.maCtk" />
            </UncontrolledTooltip>
          </dt>
          <dd>{countryEntity.maCtk}</dd>
          <dt>
            <span id="tenCtk">
              <Translate contentKey="gatewayApp.khamchuabenhCountry.tenCtk">Ten Ctk</Translate>
            </span>
            <UncontrolledTooltip target="tenCtk">
              <Translate contentKey="gatewayApp.khamchuabenhCountry.help.tenCtk" />
            </UncontrolledTooltip>
          </dt>
          <dd>{countryEntity.tenCtk}</dd>
        </dl>
        <Button tag={Link} to="/country" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/country/${countryEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ country }: IRootState) => ({
  countryEntity: country.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CountryDetail);
