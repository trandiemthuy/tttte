import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { INhanVien } from 'app/shared/model/khamchuabenh/nhan-vien.model';
import { getEntities as getNhanViens } from 'app/entities/khamchuabenh/nhan-vien/nhan-vien.reducer';
import { getEntity, updateEntity, createEntity, reset } from './chuc-vu.reducer';
import { IChucVu } from 'app/shared/model/khamchuabenh/chuc-vu.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IChucVuUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ChucVuUpdate = (props: IChucVuUpdateProps) => {
  const [nhanVienId, setNhanVienId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { chucVuEntity, nhanViens, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/chuc-vu' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getNhanViens();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...chucVuEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhChucVu.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhChucVu.home.createOrEditLabel">Create or edit a ChucVu</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : chucVuEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="chuc-vu-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="chuc-vu-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="moTaLabel" for="chuc-vu-moTa">
                  <Translate contentKey="gatewayApp.khamchuabenhChucVu.moTa">Mo Ta</Translate>
                </Label>
                <AvField
                  id="chuc-vu-moTa"
                  type="text"
                  name="moTa"
                  validate={{
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="tenLabel" for="chuc-vu-ten">
                  <Translate contentKey="gatewayApp.khamchuabenhChucVu.ten">Ten</Translate>
                </Label>
                <AvField
                  id="chuc-vu-ten"
                  type="text"
                  name="ten"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/chuc-vu" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  nhanViens: storeState.nhanVien.entities,
  chucVuEntity: storeState.chucVu.entity,
  loading: storeState.chucVu.loading,
  updating: storeState.chucVu.updating,
  updateSuccess: storeState.chucVu.updateSuccess
});

const mapDispatchToProps = {
  getNhanViens,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ChucVuUpdate);
