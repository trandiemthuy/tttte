import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './chuc-vu.reducer';
import { IChucVu } from 'app/shared/model/khamchuabenh/chuc-vu.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IChucVuDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ChucVuDetail = (props: IChucVuDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { chucVuEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhChucVu.detail.title">ChucVu</Translate> [<b>{chucVuEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="moTa">
              <Translate contentKey="gatewayApp.khamchuabenhChucVu.moTa">Mo Ta</Translate>
            </span>
          </dt>
          <dd>{chucVuEntity.moTa}</dd>
          <dt>
            <span id="ten">
              <Translate contentKey="gatewayApp.khamchuabenhChucVu.ten">Ten</Translate>
            </span>
          </dt>
          <dd>{chucVuEntity.ten}</dd>
        </dl>
        <Button tag={Link} to="/chuc-vu" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/chuc-vu/${chucVuEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ chucVu }: IRootState) => ({
  chucVuEntity: chucVu.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ChucVuDetail);
