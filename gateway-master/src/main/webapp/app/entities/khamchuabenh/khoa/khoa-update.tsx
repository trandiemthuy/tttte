import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IDonVi } from 'app/shared/model/khamchuabenh/don-vi.model';
import { getEntities as getDonVis } from 'app/entities/khamchuabenh/don-vi/don-vi.reducer';
import { getEntity, updateEntity, createEntity, reset } from './khoa.reducer';
import { IKhoa } from 'app/shared/model/khamchuabenh/khoa.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IKhoaUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const KhoaUpdate = (props: IKhoaUpdateProps) => {
  const [donViId, setDonViId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { khoaEntity, donVis, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/khoa' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getDonVis();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...khoaEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhKhoa.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhKhoa.home.createOrEditLabel">Create or edit a Khoa</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : khoaEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="khoa-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="khoa-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="capLabel" for="khoa-cap">
                  <Translate contentKey="gatewayApp.khamchuabenhKhoa.cap">Cap</Translate>
                </Label>
                <AvField
                  id="khoa-cap"
                  type="string"
                  className="form-control"
                  name="cap"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="capLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhKhoa.help.cap" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup check>
                <Label id="enabledLabel">
                  <AvInput id="khoa-enabled" type="checkbox" className="form-check-input" name="enabled" />
                  <Translate contentKey="gatewayApp.khamchuabenhKhoa.enabled">Enabled</Translate>
                </Label>
                <UncontrolledTooltip target="enabledLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhKhoa.help.enabled" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="kyHieuLabel" for="khoa-kyHieu">
                  <Translate contentKey="gatewayApp.khamchuabenhKhoa.kyHieu">Ky Hieu</Translate>
                </Label>
                <AvField
                  id="khoa-kyHieu"
                  type="text"
                  name="kyHieu"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 50, errorMessage: translate('entity.validation.maxlength', { max: 50 }) }
                  }}
                />
                <UncontrolledTooltip target="kyHieuLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhKhoa.help.kyHieu" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="phoneLabel" for="khoa-phone">
                  <Translate contentKey="gatewayApp.khamchuabenhKhoa.phone">Phone</Translate>
                </Label>
                <AvField
                  id="khoa-phone"
                  type="text"
                  name="phone"
                  validate={{
                    maxLength: { value: 15, errorMessage: translate('entity.validation.maxlength', { max: 15 }) }
                  }}
                />
                <UncontrolledTooltip target="phoneLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhKhoa.help.phone" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenLabel" for="khoa-ten">
                  <Translate contentKey="gatewayApp.khamchuabenhKhoa.ten">Ten</Translate>
                </Label>
                <AvField
                  id="khoa-ten"
                  type="text"
                  name="ten"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 200, errorMessage: translate('entity.validation.maxlength', { max: 200 }) }
                  }}
                />
                <UncontrolledTooltip target="tenLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhKhoa.help.ten" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="khoa-donVi">
                  <Translate contentKey="gatewayApp.khamchuabenhKhoa.donVi">Don Vi</Translate>
                </Label>
                <AvInput id="khoa-donVi" type="select" className="form-control" name="donViId" required>
                  {donVis
                    ? donVis.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/khoa" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  donVis: storeState.donVi.entities,
  khoaEntity: storeState.khoa.entity,
  loading: storeState.khoa.loading,
  updating: storeState.khoa.updating,
  updateSuccess: storeState.khoa.updateSuccess
});

const mapDispatchToProps = {
  getDonVis,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(KhoaUpdate);
