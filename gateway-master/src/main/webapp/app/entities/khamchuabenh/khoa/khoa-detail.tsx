import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './khoa.reducer';
import { IKhoa } from 'app/shared/model/khamchuabenh/khoa.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IKhoaDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const KhoaDetail = (props: IKhoaDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { khoaEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhKhoa.detail.title">Khoa</Translate> [<b>{khoaEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="cap">
              <Translate contentKey="gatewayApp.khamchuabenhKhoa.cap">Cap</Translate>
            </span>
            <UncontrolledTooltip target="cap">
              <Translate contentKey="gatewayApp.khamchuabenhKhoa.help.cap" />
            </UncontrolledTooltip>
          </dt>
          <dd>{khoaEntity.cap}</dd>
          <dt>
            <span id="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhKhoa.enabled">Enabled</Translate>
            </span>
            <UncontrolledTooltip target="enabled">
              <Translate contentKey="gatewayApp.khamchuabenhKhoa.help.enabled" />
            </UncontrolledTooltip>
          </dt>
          <dd>{khoaEntity.enabled ? 'true' : 'false'}</dd>
          <dt>
            <span id="kyHieu">
              <Translate contentKey="gatewayApp.khamchuabenhKhoa.kyHieu">Ky Hieu</Translate>
            </span>
            <UncontrolledTooltip target="kyHieu">
              <Translate contentKey="gatewayApp.khamchuabenhKhoa.help.kyHieu" />
            </UncontrolledTooltip>
          </dt>
          <dd>{khoaEntity.kyHieu}</dd>
          <dt>
            <span id="phone">
              <Translate contentKey="gatewayApp.khamchuabenhKhoa.phone">Phone</Translate>
            </span>
            <UncontrolledTooltip target="phone">
              <Translate contentKey="gatewayApp.khamchuabenhKhoa.help.phone" />
            </UncontrolledTooltip>
          </dt>
          <dd>{khoaEntity.phone}</dd>
          <dt>
            <span id="ten">
              <Translate contentKey="gatewayApp.khamchuabenhKhoa.ten">Ten</Translate>
            </span>
            <UncontrolledTooltip target="ten">
              <Translate contentKey="gatewayApp.khamchuabenhKhoa.help.ten" />
            </UncontrolledTooltip>
          </dt>
          <dd>{khoaEntity.ten}</dd>
          <dt>
            <Translate contentKey="gatewayApp.khamchuabenhKhoa.donVi">Don Vi</Translate>
          </dt>
          <dd>{khoaEntity.donViId ? khoaEntity.donViId : ''}</dd>
        </dl>
        <Button tag={Link} to="/khoa" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/khoa/${khoaEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ khoa }: IRootState) => ({
  khoaEntity: khoa.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(KhoaDetail);
