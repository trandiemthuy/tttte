import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IKhoa, defaultValue } from 'app/shared/model/khamchuabenh/khoa.model';

export const ACTION_TYPES = {
  FETCH_KHOA_LIST: 'khoa/FETCH_KHOA_LIST',
  FETCH_KHOA: 'khoa/FETCH_KHOA',
  CREATE_KHOA: 'khoa/CREATE_KHOA',
  UPDATE_KHOA: 'khoa/UPDATE_KHOA',
  DELETE_KHOA: 'khoa/DELETE_KHOA',
  RESET: 'khoa/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IKhoa>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type KhoaState = Readonly<typeof initialState>;

// Reducer

export default (state: KhoaState = initialState, action): KhoaState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_KHOA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_KHOA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_KHOA):
    case REQUEST(ACTION_TYPES.UPDATE_KHOA):
    case REQUEST(ACTION_TYPES.DELETE_KHOA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_KHOA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_KHOA):
    case FAILURE(ACTION_TYPES.CREATE_KHOA):
    case FAILURE(ACTION_TYPES.UPDATE_KHOA):
    case FAILURE(ACTION_TYPES.DELETE_KHOA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_KHOA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_KHOA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_KHOA):
    case SUCCESS(ACTION_TYPES.UPDATE_KHOA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_KHOA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/khoas';

// Actions

export const getEntities: ICrudGetAllAction<IKhoa> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_KHOA_LIST,
    payload: axios.get<IKhoa>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IKhoa> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_KHOA,
    payload: axios.get<IKhoa>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IKhoa> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_KHOA,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IKhoa> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_KHOA,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IKhoa> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_KHOA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
