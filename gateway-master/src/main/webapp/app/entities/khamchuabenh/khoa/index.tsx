import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Khoa from './khoa';
import KhoaDetail from './khoa-detail';
import KhoaUpdate from './khoa-update';
import KhoaDeleteDialog from './khoa-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={KhoaDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={KhoaUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={KhoaUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={KhoaDetail} />
      <ErrorBoundaryRoute path={match.url} component={Khoa} />
    </Switch>
  </>
);

export default Routes;
