import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './dan-toc.reducer';
import { IDanToc } from 'app/shared/model/khamchuabenh/dan-toc.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IDanTocUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DanTocUpdate = (props: IDanTocUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { danTocEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/dan-toc' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...danTocEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.khamchuabenhDanToc.home.createOrEditLabel">
            <Translate contentKey="gatewayApp.khamchuabenhDanToc.home.createOrEditLabel">Create or edit a DanToc</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : danTocEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="dan-toc-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="dan-toc-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="ma4069BytLabel" for="dan-toc-ma4069Byt">
                  <Translate contentKey="gatewayApp.khamchuabenhDanToc.ma4069Byt">Ma 4069 Byt</Translate>
                </Label>
                <AvField
                  id="dan-toc-ma4069Byt"
                  type="string"
                  className="form-control"
                  name="ma4069Byt"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="ma4069BytLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDanToc.help.ma4069Byt" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="maCucThongKeLabel" for="dan-toc-maCucThongKe">
                  <Translate contentKey="gatewayApp.khamchuabenhDanToc.maCucThongKe">Ma Cuc Thong Ke</Translate>
                </Label>
                <AvField
                  id="dan-toc-maCucThongKe"
                  type="string"
                  className="form-control"
                  name="maCucThongKe"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') }
                  }}
                />
                <UncontrolledTooltip target="maCucThongKeLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDanToc.help.maCucThongKe" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="ten4069BytLabel" for="dan-toc-ten4069Byt">
                  <Translate contentKey="gatewayApp.khamchuabenhDanToc.ten4069Byt">Ten 4069 Byt</Translate>
                </Label>
                <AvField
                  id="dan-toc-ten4069Byt"
                  type="text"
                  name="ten4069Byt"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="ten4069BytLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDanToc.help.ten4069Byt" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="tenCucThongKeLabel" for="dan-toc-tenCucThongKe">
                  <Translate contentKey="gatewayApp.khamchuabenhDanToc.tenCucThongKe">Ten Cuc Thong Ke</Translate>
                </Label>
                <AvField
                  id="dan-toc-tenCucThongKe"
                  type="text"
                  name="tenCucThongKe"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 500, errorMessage: translate('entity.validation.maxlength', { max: 500 }) }
                  }}
                />
                <UncontrolledTooltip target="tenCucThongKeLabel">
                  <Translate contentKey="gatewayApp.khamchuabenhDanToc.help.tenCucThongKe" />
                </UncontrolledTooltip>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/dan-toc" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  danTocEntity: storeState.danToc.entity,
  loading: storeState.danToc.loading,
  updating: storeState.danToc.updating,
  updateSuccess: storeState.danToc.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DanTocUpdate);
