import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IDanToc, defaultValue } from 'app/shared/model/khamchuabenh/dan-toc.model';

export const ACTION_TYPES = {
  FETCH_DANTOC_LIST: 'danToc/FETCH_DANTOC_LIST',
  FETCH_DANTOC: 'danToc/FETCH_DANTOC',
  CREATE_DANTOC: 'danToc/CREATE_DANTOC',
  UPDATE_DANTOC: 'danToc/UPDATE_DANTOC',
  DELETE_DANTOC: 'danToc/DELETE_DANTOC',
  RESET: 'danToc/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IDanToc>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type DanTocState = Readonly<typeof initialState>;

// Reducer

export default (state: DanTocState = initialState, action): DanTocState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_DANTOC_LIST):
    case REQUEST(ACTION_TYPES.FETCH_DANTOC):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_DANTOC):
    case REQUEST(ACTION_TYPES.UPDATE_DANTOC):
    case REQUEST(ACTION_TYPES.DELETE_DANTOC):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_DANTOC_LIST):
    case FAILURE(ACTION_TYPES.FETCH_DANTOC):
    case FAILURE(ACTION_TYPES.CREATE_DANTOC):
    case FAILURE(ACTION_TYPES.UPDATE_DANTOC):
    case FAILURE(ACTION_TYPES.DELETE_DANTOC):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_DANTOC_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_DANTOC):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_DANTOC):
    case SUCCESS(ACTION_TYPES.UPDATE_DANTOC):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_DANTOC):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/dan-tocs';

// Actions

export const getEntities: ICrudGetAllAction<IDanToc> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_DANTOC_LIST,
    payload: axios.get<IDanToc>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IDanToc> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_DANTOC,
    payload: axios.get<IDanToc>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IDanToc> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_DANTOC,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IDanToc> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_DANTOC,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IDanToc> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_DANTOC,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
