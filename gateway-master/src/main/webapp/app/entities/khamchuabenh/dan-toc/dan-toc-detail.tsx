import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './dan-toc.reducer';
import { IDanToc } from 'app/shared/model/khamchuabenh/dan-toc.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IDanTocDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const DanTocDetail = (props: IDanTocDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { danTocEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhDanToc.detail.title">DanToc</Translate> [<b>{danTocEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="ma4069Byt">
              <Translate contentKey="gatewayApp.khamchuabenhDanToc.ma4069Byt">Ma 4069 Byt</Translate>
            </span>
            <UncontrolledTooltip target="ma4069Byt">
              <Translate contentKey="gatewayApp.khamchuabenhDanToc.help.ma4069Byt" />
            </UncontrolledTooltip>
          </dt>
          <dd>{danTocEntity.ma4069Byt}</dd>
          <dt>
            <span id="maCucThongKe">
              <Translate contentKey="gatewayApp.khamchuabenhDanToc.maCucThongKe">Ma Cuc Thong Ke</Translate>
            </span>
            <UncontrolledTooltip target="maCucThongKe">
              <Translate contentKey="gatewayApp.khamchuabenhDanToc.help.maCucThongKe" />
            </UncontrolledTooltip>
          </dt>
          <dd>{danTocEntity.maCucThongKe}</dd>
          <dt>
            <span id="ten4069Byt">
              <Translate contentKey="gatewayApp.khamchuabenhDanToc.ten4069Byt">Ten 4069 Byt</Translate>
            </span>
            <UncontrolledTooltip target="ten4069Byt">
              <Translate contentKey="gatewayApp.khamchuabenhDanToc.help.ten4069Byt" />
            </UncontrolledTooltip>
          </dt>
          <dd>{danTocEntity.ten4069Byt}</dd>
          <dt>
            <span id="tenCucThongKe">
              <Translate contentKey="gatewayApp.khamchuabenhDanToc.tenCucThongKe">Ten Cuc Thong Ke</Translate>
            </span>
            <UncontrolledTooltip target="tenCucThongKe">
              <Translate contentKey="gatewayApp.khamchuabenhDanToc.help.tenCucThongKe" />
            </UncontrolledTooltip>
          </dt>
          <dd>{danTocEntity.tenCucThongKe}</dd>
        </dl>
        <Button tag={Link} to="/dan-toc" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/dan-toc/${danTocEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ danToc }: IRootState) => ({
  danTocEntity: danToc.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(DanTocDetail);
