import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import DanToc from './dan-toc';
import DanTocDetail from './dan-toc-detail';
import DanTocUpdate from './dan-toc-update';
import DanTocDeleteDialog from './dan-toc-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={DanTocDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={DanTocUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={DanTocUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={DanTocDetail} />
      <ErrorBoundaryRoute path={match.url} component={DanToc} />
    </Switch>
  </>
);

export default Routes;
