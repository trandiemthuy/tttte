import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './nhom-doi-tuong-bhyt.reducer';
import { INhomDoiTuongBhyt } from 'app/shared/model/khamchuabenh/nhom-doi-tuong-bhyt.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface INhomDoiTuongBhytProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const NhomDoiTuongBhyt = (props: INhomDoiTuongBhytProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { nhomDoiTuongBhytList, match } = props;
  return (
    <div>
      <h2 id="nhom-doi-tuong-bhyt-heading">
        <Translate contentKey="gatewayApp.khamchuabenhNhomDoiTuongBhyt.home.title">Nhom Doi Tuong Bhyts</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="gatewayApp.khamchuabenhNhomDoiTuongBhyt.home.createLabel">Create new Nhom Doi Tuong Bhyt</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {nhomDoiTuongBhytList && nhomDoiTuongBhytList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.khamchuabenhNhomDoiTuongBhyt.ten">Ten</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {nhomDoiTuongBhytList.map((nhomDoiTuongBhyt, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${nhomDoiTuongBhyt.id}`} color="link" size="sm">
                      {nhomDoiTuongBhyt.id}
                    </Button>
                  </td>
                  <td>{nhomDoiTuongBhyt.ten}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${nhomDoiTuongBhyt.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${nhomDoiTuongBhyt.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${nhomDoiTuongBhyt.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          <div className="alert alert-warning">
            <Translate contentKey="gatewayApp.khamchuabenhNhomDoiTuongBhyt.home.notFound">No Nhom Doi Tuong Bhyts found</Translate>
          </div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ nhomDoiTuongBhyt }: IRootState) => ({
  nhomDoiTuongBhytList: nhomDoiTuongBhyt.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(NhomDoiTuongBhyt);
