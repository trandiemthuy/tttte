import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './nhom-doi-tuong-bhyt.reducer';
import { INhomDoiTuongBhyt } from 'app/shared/model/khamchuabenh/nhom-doi-tuong-bhyt.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface INhomDoiTuongBhytDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const NhomDoiTuongBhytDetail = (props: INhomDoiTuongBhytDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { nhomDoiTuongBhytEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="gatewayApp.khamchuabenhNhomDoiTuongBhyt.detail.title">NhomDoiTuongBhyt</Translate> [
          <b>{nhomDoiTuongBhytEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="ten">
              <Translate contentKey="gatewayApp.khamchuabenhNhomDoiTuongBhyt.ten">Ten</Translate>
            </span>
            <UncontrolledTooltip target="ten">
              <Translate contentKey="gatewayApp.khamchuabenhNhomDoiTuongBhyt.help.ten" />
            </UncontrolledTooltip>
          </dt>
          <dd>{nhomDoiTuongBhytEntity.ten}</dd>
        </dl>
        <Button tag={Link} to="/nhom-doi-tuong-bhyt" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/nhom-doi-tuong-bhyt/${nhomDoiTuongBhytEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ nhomDoiTuongBhyt }: IRootState) => ({
  nhomDoiTuongBhytEntity: nhomDoiTuongBhyt.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(NhomDoiTuongBhytDetail);
