import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import NhomDoiTuongBhyt from './nhom-doi-tuong-bhyt';
import NhomDoiTuongBhytDetail from './nhom-doi-tuong-bhyt-detail';
import NhomDoiTuongBhytUpdate from './nhom-doi-tuong-bhyt-update';
import NhomDoiTuongBhytDeleteDialog from './nhom-doi-tuong-bhyt-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={NhomDoiTuongBhytDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={NhomDoiTuongBhytUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={NhomDoiTuongBhytUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={NhomDoiTuongBhytDetail} />
      <ErrorBoundaryRoute path={match.url} component={NhomDoiTuongBhyt} />
    </Switch>
  </>
);

export default Routes;
