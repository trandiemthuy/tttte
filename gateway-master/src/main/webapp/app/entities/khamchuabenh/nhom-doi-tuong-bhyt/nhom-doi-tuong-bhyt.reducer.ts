import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { INhomDoiTuongBhyt, defaultValue } from 'app/shared/model/khamchuabenh/nhom-doi-tuong-bhyt.model';

export const ACTION_TYPES = {
  FETCH_NHOMDOITUONGBHYT_LIST: 'nhomDoiTuongBhyt/FETCH_NHOMDOITUONGBHYT_LIST',
  FETCH_NHOMDOITUONGBHYT: 'nhomDoiTuongBhyt/FETCH_NHOMDOITUONGBHYT',
  CREATE_NHOMDOITUONGBHYT: 'nhomDoiTuongBhyt/CREATE_NHOMDOITUONGBHYT',
  UPDATE_NHOMDOITUONGBHYT: 'nhomDoiTuongBhyt/UPDATE_NHOMDOITUONGBHYT',
  DELETE_NHOMDOITUONGBHYT: 'nhomDoiTuongBhyt/DELETE_NHOMDOITUONGBHYT',
  RESET: 'nhomDoiTuongBhyt/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<INhomDoiTuongBhyt>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type NhomDoiTuongBhytState = Readonly<typeof initialState>;

// Reducer

export default (state: NhomDoiTuongBhytState = initialState, action): NhomDoiTuongBhytState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_NHOMDOITUONGBHYT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_NHOMDOITUONGBHYT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_NHOMDOITUONGBHYT):
    case REQUEST(ACTION_TYPES.UPDATE_NHOMDOITUONGBHYT):
    case REQUEST(ACTION_TYPES.DELETE_NHOMDOITUONGBHYT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_NHOMDOITUONGBHYT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_NHOMDOITUONGBHYT):
    case FAILURE(ACTION_TYPES.CREATE_NHOMDOITUONGBHYT):
    case FAILURE(ACTION_TYPES.UPDATE_NHOMDOITUONGBHYT):
    case FAILURE(ACTION_TYPES.DELETE_NHOMDOITUONGBHYT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_NHOMDOITUONGBHYT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_NHOMDOITUONGBHYT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_NHOMDOITUONGBHYT):
    case SUCCESS(ACTION_TYPES.UPDATE_NHOMDOITUONGBHYT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_NHOMDOITUONGBHYT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/nhom-doi-tuong-bhyts';

// Actions

export const getEntities: ICrudGetAllAction<INhomDoiTuongBhyt> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_NHOMDOITUONGBHYT_LIST,
  payload: axios.get<INhomDoiTuongBhyt>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<INhomDoiTuongBhyt> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_NHOMDOITUONGBHYT,
    payload: axios.get<INhomDoiTuongBhyt>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<INhomDoiTuongBhyt> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_NHOMDOITUONGBHYT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<INhomDoiTuongBhyt> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_NHOMDOITUONGBHYT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<INhomDoiTuongBhyt> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_NHOMDOITUONGBHYT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
