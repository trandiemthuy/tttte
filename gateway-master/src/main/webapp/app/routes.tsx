import React from 'react';
import { Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

import Logout from 'app/modules/login/logout';
import Home from 'app/modules/home/home';
import Entities from 'app/entities';
import VienPhi from 'app/modules/vien-phi';
import PrivateRoute from 'app/shared/auth/private-route';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import PageNotFound from 'app/shared/error/page-not-found';
import { AUTHORITIES } from 'app/config/constants';
import TiepNhan from 'app/modules/TiepNhan';
import DuocModule from 'app/modules/duoc';
import KhamChuaBenh from 'app/modules/KhamChuaBenh/KhamChuaBenh';

const Admin = Loadable({
  loader: () => import(/* webpackChunkName: "administration" */ 'app/modules/administration'),
  loading: () => <div>loading ...</div>
});

const Routes = () => (
  <div className="view-routes">
    <Switch>
      <ErrorBoundaryRoute path="/tiep-nhan" component={TiepNhan} />
      <ErrorBoundaryRoute path="/kham-benh" exact component={KhamChuaBenh} />
      <ErrorBoundaryRoute path="/logout" component={Logout} />
      <PrivateRoute path="/admin" component={Admin} hasAnyAuthorities={[AUTHORITIES.ADMIN]} />
      <ErrorBoundaryRoute path="/" exact component={Home} />
      <PrivateRoute path="/vien-phi" component={VienPhi} hasAnyAuthorities={[AUTHORITIES.USER]} />
      <PrivateRoute path="/duoc" component={DuocModule} />
      <PrivateRoute path="/" component={Entities} hasAnyAuthorities={[AUTHORITIES.USER]} />
      <ErrorBoundaryRoute component={PageNotFound} />
    </Switch>
  </div>
);

export default Routes;
