import React, { useState } from 'react';

import { DateBox, Button as DateBoxButton } from 'devextreme-react/date-box';

export interface IAdvancedDateBox {
    dateValue: any,
    onSetDateValue: Function
}

export const AdvancedDateBox = (props: IAdvancedDateBox) => {
    const millisecondsInDay = 86400000;
    const handleSetDate = (dateValueInput) => {
        props.onSetDateValue(dateValueInput)
    }
    const todayButton = {
        text: 'Hôm nay',
        type: 'default',
        onClick() {
            handleSetDate(new Date().getTime());
        }
    };
    const prevDateButton = {
        icon: 'spinprev',
        stylingMode: 'text',
        onClick() {
            handleSetDate(props.dateValue - millisecondsInDay);
        }
    };

    const nextDateButton = {
        icon: 'spinnext',
        stylingMode: 'text',
        onClick() {
            handleSetDate(props.dateValue + millisecondsInDay);
        }
    };

    const onDateChanged = (e) => {
        props.onSetDateValue(e.value);
    };
    return (
        <React.Fragment>
            <div>
                <div>
                    <div>
                        <DateBox value={props.dateValue}
                            stylingMode="outlined"
                            onValueChanged={onDateChanged}>
                            <DateBoxButton
                                name="today"
                                location="before"
                                options={todayButton}
                            />
                            <DateBoxButton
                                name="prevDate"
                                location="before"
                                options={prevDateButton}
                            />
                            <DateBoxButton
                                name="nextDate"
                                location="after"
                                options={nextDateButton}
                            />
                            <DateBoxButton name="dropDown" />
                        </DateBox>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

AdvancedDateBox.defaultProps = {
    dateValue: new Date().getTime()
}
export default AdvancedDateBox;