import React, { useState } from 'react'
import { Autocomplete } from 'devextreme-react/autocomplete';
import { TextBox } from "devextreme-react";

export interface ISearchInput {
    onTextChange: Function,
    onNameChange: Function,
    text: string,
    name: Array<any>,
    paramInput: string,
    paramOutput: string,
    width: number,
    displayCol: string,
    idCol: string
}
export const SearchInput = (props: ISearchInput) => {
    const [selectedName, setSelectedName] = useState('')
    const [validationStatus, setValidationStatus] = useState(true)
    const handleTextChange = data => {
        props.onTextChange(data.value)
        setSelectedName('');
        let found = 0;
        props.name.forEach(item => {
            if(item[props.idCol] +'' === data.value + ''){
                setSelectedName(item[props.displayCol]);
                found++;
            }
        })
        setValidationStatus(found > 0);
    };
    const handleNameChange = data => {
        props.name.forEach(item => {
            if(item[props.displayCol] + '' === data.value + '') props.onTextChange(item[props.idCol] + '')
        })
        setSelectedName(data.value)
    };
    return (
        <div className="row" style={{ maxWidth: props.width }}>
            <div className="col-3 pr-0">
                <TextBox value={props.text} onValueChanged={handleTextChange} showClearButton={true}></TextBox>
            </div>
            <div className="col-7">
                <Autocomplete
                    dataSource={props.name}
                    value={selectedName}
                    valueExpr={props.displayCol}
                    onValueChanged={handleNameChange}
                    showClearButton={true}
                    placeholder="Nhập..."
                    validationMessageMode="always"
                    validationStatus={validationStatus ? "valid" : "invalid"}
                />
            </div>
        </div>
    );
}
SearchInput.defaultProps = {
    onTextChange: null,
    onNameChange: null,
    text: '',
    name: [],
    paramInput: '',
    paramOutput: '',
    width: 400,
    displayCol: 'string',
    idCol: 'string'
};
export default SearchInput;
