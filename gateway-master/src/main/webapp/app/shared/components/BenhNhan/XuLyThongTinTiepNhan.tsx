import 'devextreme-react/text-area';
import React, {useState, useMemo, useCallback} from 'react';
import Form, {ButtonItem, GroupItem, SimpleItem, Label} from 'devextreme-react/form';
import {IBenhNhan} from "app/shared/model/khamchuabenh/benh-nhan.model";
import {ITheBhyt} from "app/shared/model/khamchuabenh/the-bhyt.model";
import DataSource from 'devextreme/data/data_source';
import {IDoiTuongBhyt} from "app/shared/model/khamchuabenh/doi-tuong-bhyt.model";
import {IPhongBanModel} from "app/modules/duoc/duoc-components/he-thong/phong-ban.model";
import {IDichVuKham} from "app/shared/model/khamchuabenh/dich-vu-kham.model";
import {printPhieuChuyenKho} from "app/modules/duoc/duoc-components/chuyen-kho/chuyen-kho.reducer";
import {IPhong} from "app/shared/model/khamchuabenh/phong.model";

interface IXuLyThongTinTiepNhan {
  onChange?: any;
  thongTinBenhNhan?: IBenhNhan;
  thongTinTheBhyt?: ITheBhyt;
  changeButtonState?: Function;
  phongKham?:ReadonlyArray<IPhong>;
  dichVuKhamList?: ReadonlyArray<IDichVuKham>;
  creatTiepNhanRequest?: Function;
  inputState?: any
}

export const XuLyThongTinTiepNhan = (props: IXuLyThongTinTiepNhan) => {
  const {onChange,phongKham} = props;
  const [dichVuKhamId, setDichVuKhamId] = useState<number>(1);
  const [phongTiepNhanId, setPhongTiepNhanId] =useState<number>(1);
  /*   const buttonOptions = (text: string, type: string, useSubmitBehavior = false, stylingMode = 'contained', width = '125px') => {
    const buttonOption = { text, width, type, stylingMode, useSubmitBehavior };
    return buttonOption;
  }; */
  const [disabled, setDisabled] = useState(true);

  const maPhongKhamOptions = useMemo(() => {
    return {
      placeholder: 'Mã phòng khám'
    };
  }, []);


  const changePhongKham = useCallback((e)=> {
    return setPhongTiepNhanId(e.value)
  },[]);
  const changeDichVu = useCallback((e)=>{
    return setDichVuKhamId(e.value)
  },[])
  const phongKhamDataSource = new DataSource({
    store: {
      data: phongKham,
      type: 'array',
      key: 'id'
    }
  });

  const phongKhamOption = useMemo(()=> {
    return {
      searchEnabled: true,
      dataSource: phongKhamDataSource,
      displayExpr: "ten",
      valueExpr: "id",
      noDataText: "Không có dữ liệu",
      onChange: changePhongKham,
      disabled: props.inputState.phongKham
    }
  },[phongKham]);

  const handleThem = (e) => {
    props.changeButtonState('them');
    setDisabled(!disabled);
  };
  const handleLuu = (e) => {
    props.changeButtonState('luu');
    props.creatTiepNhanRequest({
      dichVuKhamId,
      tiepNhanDTO: {
        coBaoHiem: props.thongTinTheBhyt.soThe ? 1 : 0,
        phongTiepNhanId,
        donViId: 1,
        nhanVienTiepNhanId: 1,
        tenBenhNhan: props.thongTinBenhNhan.ten,
        diaChi: props.thongTinBenhNhan.diaChiThuongTru,
        benhNhanCu: 0,
        nam: (new Date).getFullYear(),
        capCuu: 0,
        thoiGianTiepNhan: new Date(),
        canhBao:0,
        ...props.thongTinBenhNhan,
        ...props.thongTinTheBhyt,

      }
    })
    // console.log(props.thongTinBenhNhan);
  }
  const handleSua = (e) => {
    props.changeButtonState('sua')
   setDisabled(!disabled);
 }

  const handleHuy = (e) => {
    props.changeButtonState('init');
    setDisabled(!disabled);
  }
  
  const handleXoa = (e) => {
     props.changeButtonState('init')
    setDisabled(!disabled);
  }

  const dichVuKhamDatasoure = new DataSource({
    store: {
      data: props.dichVuKhamList,
      type: 'array',
      key: 'id'
    }
  });

  const dichVuKhamOption = useMemo(()=> {
    return {
      searchEnabled: true,
      dataSource: dichVuKhamDatasoure,
      displayExpr: "tenHienThi",
      valueExpr: "id",
      noDataText: "Không có dữ liệu",
      onChange: changeDichVu,
      disabled: props.inputState.phongKham
    }
  },[props.dichVuKhamList]);


  const capCuuOption = useMemo(()=> {
    return {
      disabled: props.inputState.capCuu
    }
  },[]);

  const khamUuTienOption = useMemo(()=> {
    return {
      disabled: props.inputState.khamUuTien
    }
  },[]);

  const dungTuyenOption = useMemo(()=> {
    return {
      disabled: props.inputState.dungTuyen
    }
  },[]);

  const khamOnlineOption = useMemo(()=> {
    return {
      disabled: props.inputState.khamOnline
    }
  },[]);


  return (
    <Form onFieldDataChanged={onChange} colCount={2}>
      <GroupItem cssClass="section-group">
        <GroupItem colCount={4}>
          <SimpleItem
            editorType="dxCheckBox"
            dataField="capCuu"
            cssClass="checkBoxStyle"
            editorOptions={capCuuOption}
          >
            <Label text={'Cấp cứu'}/>
          </SimpleItem>
          <SimpleItem
            editorType="dxCheckBox"
            dataField="khamUuTien"
            cssClass="checkBoxStyle"
            editorOptions={khamUuTienOption}
          >
            <Label text={"Khám ưu tiên"}/>
          </SimpleItem>
          <SimpleItem
            editorType="dxCheckBox"
            dataField="dungTuyen"
            cssClass="checkBoxStyle"
            editorOptions={dungTuyenOption}
          >
            <Label text={"Đúng tuyến"}/>
          </SimpleItem>
          <SimpleItem
            editorType="dxCheckBox"
            dataField="khamOnline"
            cssClass="checkBoxStyle"
            editorOptions={khamOnlineOption}
          >
            <Label text={"Khám Online"}/>
          </SimpleItem>
        </GroupItem>

        <GroupItem>
          <GroupItem colCount={5}>
            <SimpleItem
              colSpan={5}
              dataField="phongTiepNhanId"
              editorType="dxSelectBox"
              editorOptions={phongKhamOption}
              // label={{text: 'Phòng khám'}}
            >
              <Label text={'Phòng khám'}/>
            </SimpleItem>
            {/* <SimpleItem*/}
            {/*  colSpan={3}*/}
            {/*  dataField="tenPhongKham"*/}
            {/*  label={{visible: false}}*/}
            {/*  editorOptions={phongKhamOption}*/}
            {/* >*/}

            {/* </SimpleItem>*/}
          </GroupItem>
          <GroupItem colCount={5}>
            <SimpleItem
              colSpan={5}
              dataField="dichVuKhamId"
              editorType="dxSelectBox"
              editorOptions={dichVuKhamOption}
              // label={{text: 'Dịch vụ'}}
            >
              <Label text={'Dịch vụ'}/>
            </SimpleItem>
            {/* <SimpleItem*/}
            {/*  colSpan={3}*/}
            {/*  dataField="tenDichVu"*/}
            {/* >*/}
            {/*  <Label />*/}
            {/* </SimpleItem>*/}
          </GroupItem>
        </GroupItem>
      </GroupItem>
      {/*       <SpeedDialAction icon="add" label="Thêm" index={1} />
      <SpeedDialAction icon="print" label="In phiếu TN" index={4} />

      <SpeedDialAction
        icon="edit"
        label="Sửa"
        index={2}
        // visible={selectedRowIndex !== undefined && selectedRowIndex !== -1}
        visible={false}
        // onClick={this.editRow}
      />
      <SpeedDialAction
        icon="trash"
        label="Delete row"
        index={3}
        // visible={selectedRowIndex !== undefined && selectedRowIndex !== -1}
        visible={false}
      /> */}
      <GroupItem cssClass="action-button">
        <GroupItem colCount={4}>
          <ButtonItem
            buttonOptions={{
              text: 'Đo sinh hiệu',
              type: 'normal',
              onClick: {},
              stylingMode: 'contained',
              width: '125px'
            }}
          />
          <ButtonItem
            buttonOptions={{
              text: 'Chỉ định CLS',
              type: 'normal',
              onClick: {},
              stylingMode: 'contained',
              width: '125px'
            }}
          />
          <ButtonItem
            buttonOptions={{
              text: 'Thông tin chuyển tuyến',
              type: 'default',
              onClick: {},
              stylingMode: 'contained',
              width: '125px'
            }}
          />
          <ButtonItem
            buttonOptions={{
              disabled,
              text: 'In phiếu TN',
              type: 'default',
              onClick: {},
              stylingMode: 'contained',
              width: '125px'
            }}
          />
        </GroupItem>
        <GroupItem colCount={5}>
          <ButtonItem
            buttonOptions={{
              text: 'Thêm',
              type: 'default',
              onClick: handleThem,
              stylingMode: 'contained',
              width: '125px'
            }}
          />
          <ButtonItem
            buttonOptions={{
              disabled: props.inputState.luuBtn,
              text: 'Lưu',
              type: 'default',
              onClick: handleLuu,
              stylingMode: 'contained',
              width: '125px',
            }}
          />
          <ButtonItem
            buttonOptions={{
              disabled: props.inputState.suaBtn,
              text: 'Sửa',
              type: 'default',
              onClick: handleSua,
              stylingMode: 'contained',
              width: '125px'
            }}
          />
          <ButtonItem
            buttonOptions={{
              disabled: props.inputState.xoaBtn,
              text: 'Xoá',
              type: 'danger',
              onClick: handleXoa,
              stylingMode: 'contained',
              width: '125px'
            }}
          />
          <ButtonItem
            buttonOptions={{
              disabled:  props.inputState.huyBtn,
              text: 'Huỷ',
              type: 'normal',
              onClick: handleHuy,
              stylingMode: 'contained',
              width: '125px'
            }}
          />
        </GroupItem>
      </GroupItem>
    </Form>
  );
};
