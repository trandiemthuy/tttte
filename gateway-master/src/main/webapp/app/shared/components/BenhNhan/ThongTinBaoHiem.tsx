import { GIAY_TO_TE } from 'app/config/constants';
import { IDoiTuongBhyt } from 'app/shared/model/khamchuabenh/doi-tuong-bhyt.model';
import { ITheBhyt } from 'app/shared/model/khamchuabenh/the-bhyt.model';
import Form, { ButtonItem, GroupItem, Label, SimpleItem } from 'devextreme-react/form';
import DataSource from 'devextreme/data/data_source';
import React, { useMemo } from 'react';

interface IThongTinBaoHiemProp {
  values?: ITheBhyt;
  onChange?: any;
  doiTuongBHYTs?: ReadonlyArray<IDoiTuongBhyt>;
  doiTuongBHYT?: any;
  inputState?: any
}

export const ThongTinBaoHiem = (props: IThongTinBaoHiemProp) => {
  const { values, onChange, doiTuongBHYTs, doiTuongBHYT } = props;
  const doiTuongBHYTDataSource = new DataSource({
    store: {
      data: doiTuongBHYTs,
      type: 'array',
      key: 'id'
    }
  });

  const doiTuongBHYTOption = useMemo(() => {
    return {
      searchEnabled: true,
      dataSource: doiTuongBHYTDataSource,
      displayExpr: "ten",
      valueExpr: "id",
      noDataText: "Không có dữ liệu",
      disabled: props.inputState.doiTuongBHYT
    }
  },[doiTuongBHYTs]);

  const giayToTEOption = useMemo(() => {
    return {
      displayExpr: 'text',
      valueExpr: 'value',
      dataSource: GIAY_TO_TE,
      disabled: props.inputState.giayToTE
    };
  }, []);

  const ngayBatDauOptions = useMemo(() => {
    return {
      min: '1900/1/1',
      max: values.ngayBatDau ? values.ngayBatDau : new Date().toISOString(),
      disabled: props.inputState.tuNgay
    };
  }, [values.ngayBatDau]);

  const ngayHetHanOptions = useMemo(() => {
    return {
      min: values.ngayBatDau ? values.ngayBatDau : '1900/01/01',
      max: new Date().toISOString(),
      disabled: props.inputState.denNgay
    };
  }, [values.ngayHetHan]);
  const ngayConLaiBH5NamOptions = useMemo(() => {
    return {
      placeholder: 'Số ngày còn lại của bảo hiểm 5 năm',
      disabled : props.inputState.ngayConLaiBH5Nam
    };
  }, []);
  const tenGiayTE1Options = useMemo(() => {
    return {
      placeholder: 'Tên giấy TE1',
      disabled : props.inputState.tenGiayTE1
    };
  }, []);

  const soTheOptions = useMemo(() => {
    return {
      placeholder: 'Số Bảo Hiểm',
      disabled : props.inputState.soThe
    };
  }, [values]);

  const chuoiNhanDangOptions = useMemo(() => {
    return {
      placeholder: 'Số Chuỗi nhận dạng',
      disabled : props.inputState.chuoiNhanDang
    };
  }, [doiTuongBHYT.ma]);

  const maDangKyOptions = useMemo(() => {
    return {
      placeholder: 'Mã đăng ký',
      disabled : props.inputState.maDangKy
    };
  }, [values]);

  const noiDangKyOptions = useMemo(() => {
    return {
      placeholder: 'Nơi đăng ký',
      disabled : props.inputState.noiDangKy
    };
  }, [values]);

  const maKhuVucOptions = useMemo(() => {
    return {
      placeholder: 'Mã khu vực',
      disabled : props.inputState.maKhuVuc
    };
  }, [values]);

  const tyLeMienGiamOptions = useMemo(() => {
    return {
      placeholder: 'Tỷ lệ miễn giảm ',
      disabled : props.inputState.tyLeMienGiam
    };
  }, [doiTuongBHYT.tyLeMienGiam]);

  const coMCCTOptions = useMemo(() => {
    return {
      text: 'Có MCCT. Ngày MCCT',
      defaultValue: false,
      disabled : props.inputState.coMCCT
    };
  }, [values]);
  
  const ngayMienCungChiTraOptions = useMemo(() => {
    return { placeholder: 'Ngày Miễn cùng chi trả', disabled : props.inputState.ngayMienCungChiTra };
  }, [values]);

  const btnKiemTraTheOptions = useMemo(() => {
    return {
      text: 'Kiểm tra thẻ',
      width: '125px',
      type: 'default',
      stylingMode: 'contained'
    };
  }, []);
  const btnTheTreEmOptions = useMemo(() => {
    return {
      text: 'Thẻ trẻ em',
      width: '125px',
      type: 'default',
      stylingMode: 'contained'
    };
  }, []);
  const btnXoaThongTinTheOptions = useMemo(() => {
    return {
      text: 'Xoá thông tin thẻ',
      width: '125px',
      type: 'normal',
      stylingMode: 'contained'
    };
  }, []);
  return (
    <>
      <Form formData={values} onFieldDataChanged={onChange} showColonAfterLabel>
        <SimpleItem editorType="dxTextBox" dataField="soThe" editorOptions={soTheOptions}>
          <Label text="Số Bảo Hiểm" />
        </SimpleItem>
        <SimpleItem editorType="dxSelectBox" dataField="doiTuongBhytId" editorOptions={doiTuongBHYTOption}>
          <Label text="Đối Tượng thẻ" />
        </SimpleItem>

        <GroupItem colCount={2}>
          <SimpleItem
            editorType="dxTextBox"
            editorOptions={chuoiNhanDangOptions}
            // dataField="chuoiNhanDang"
            // editorOptions={{placeholder: 'Chuỗi nhận dạng'}}>
          >
            <Label text="Số Chuỗi nhận dạng" />
          </SimpleItem>
          <SimpleItem
            editorType="dxTextBox"
            editorOptions={tyLeMienGiamOptions}
            dataField="tyLeMienGiam"
            // editorOptions={{placeholder: 'Từ 1 -> 100'}}
          >
            <Label text="Tỷ lệ miễn giảm (%)" />
            {/* <PatternRule message="Không sử dụng chữ" pattern={/^[0-9]+$/}/>*/}
          </SimpleItem>
        </GroupItem>

        <GroupItem colCount={5}>
          <SimpleItem editorType="dxDateBox" colSpan={3} dataField="ngayBatDau" editorOptions={ngayBatDauOptions}>
            <Label text="Từ ngày" />
          </SimpleItem>
          <SimpleItem editorType="dxDateBox" colSpan={2} dataField="ngayHetHan" editorOptions={ngayHetHanOptions}>
            <Label text="Đến" />
          </SimpleItem>
        </GroupItem>

        <GroupItem colCount={7}>
          <SimpleItem editorType="dxTextBox" dataField="maDK" colSpan={3} editorOptions={maDangKyOptions}>
            <Label text="Mã đăng ký" />
          </SimpleItem>

          <SimpleItem editorType="dxTextBox" dataField="noiDK" colSpan={4} editorOptions={noiDangKyOptions}>
            <Label visible={false} />
          </SimpleItem>
        </GroupItem>

        <SimpleItem editorType="dxTextBox" dataField="maKhuVuc" editorOptions={maKhuVucOptions}>
          <Label text="Mã khu vực" />
        </SimpleItem>
        <GroupItem colCount={5}>
          <SimpleItem dataField="giayTE1" colSpan={3} editorType="dxSelectBox" editorOptions={giayToTEOption}>
            <Label text="Giấy tờ TE1" />
          </SimpleItem>

          <SimpleItem editorType="dxTextBox" dataField="tenGiayTE1" colSpan={2} editorOptions={tenGiayTE1Options}>
            <Label visible={false} />
          </SimpleItem>
        </GroupItem>
        <GroupItem colCount={12}>
          <SimpleItem editorType="dxCheckBox" dataField="coMCCT" colSpan={3} cssClass="checkBoxStyle" editorOptions={coMCCTOptions}>
            <Label visible={false} />
          </SimpleItem>
          <SimpleItem editorType="dxDateBox" colSpan={5} dataField="ngayMienCungChiTra" editorOptions={ngayMienCungChiTraOptions}>
            <Label visible={false} />
          </SimpleItem>

          <SimpleItem editorType="dxTextBox" dataField="ngayDuNamNam" colSpan={4} editorOptions={ngayConLaiBH5NamOptions}>
            <Label visible={false} />
          </SimpleItem>
        </GroupItem>

        {/* <GroupItem colCount={7}>*/}
        {/*  <SimpleItem*/}
        {/*    editorType="dxTextBox"*/}
        {/*    dataField="maNoiChuyenDen"*/}
        {/*    colSpan={3}*/}
        {/*    editorOptions={{placeholder: 'Mã khu vực chuyển đến'}}*/}
        {/*    label={{text: 'Mã khu vực'}}*/}
        {/*  />*/}

        {/*  <SimpleItem*/}
        {/*    editorType="dxTextBox"*/}
        {/*    dataField="noiChuyenDen"*/}
        {/*    colSpan={4}*/}
        {/*    editorOptions={{placeholder: 'Khu vực chuyển đến'}}*/}
        {/*    label={{visible: false}}*/}
        {/*  />*/}
        {/* </GroupItem>*/}

        {/* <GroupItem colCount={7}>*/}
        {/*  <SimpleItem*/}
        {/*    editorType="dxTextBox"*/}
        {/*    dataField="maNoiChuyen"*/}
        {/*    colSpan={3}*/}
        {/*    editorOptions={{placeholder: 'Mã khu vực chuẩn đoán chuyển đến'}}*/}
        {/*    label={{text: 'Chấn đoán nơi chuyển'}}*/}
        {/*  />*/}

        {/*  <SimpleItem*/}
        {/*    editorType="dxTextBox"*/}
        {/*    dataField="noiChuyen"*/}
        {/*    colSpan={4}*/}
        {/*    editorOptions={{placeholder: 'Khu vực chuẩn đoán chuyển đến'}}*/}
        {/*  >*/}
        {/*    <Label visible={false}/>*/}
        {/*  </SimpleItem>*/}
        {/* </GroupItem>*/}
        <GroupItem colCount={3} cssClass="action-bhyt">
          <ButtonItem buttonOptions={btnKiemTraTheOptions} />
          <ButtonItem buttonOptions={btnTheTreEmOptions} />
          <ButtonItem buttonOptions={btnXoaThongTinTheOptions} />
        </GroupItem>

      </Form>
    </>
  );
};
export default React.memo(ThongTinBaoHiem);

const mapStateToProps = storeState => ({});
const mapDispatchToProps = {};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
