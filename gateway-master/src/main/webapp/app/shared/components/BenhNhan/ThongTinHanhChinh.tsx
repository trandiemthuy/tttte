import React, { useMemo } from 'react';
import { PatternRule, StringLengthRule } from 'devextreme-react/data-grid';
import Form, { GroupItem, Item, Label, RequiredRule, SimpleItem } from 'devextreme-react/form';
import { Button } from 'devextreme-react';
import { IBenhNhan } from 'app/shared/model/khamchuabenh/benh-nhan.model';
import { GIOI_TINH } from 'app/config/constants';
import { ICountry } from 'app/shared/model/khamchuabenh/country.model';
import { IDanToc } from 'app/shared/model/khamchuabenh/dan-toc.model';
import { INgheNghiep } from 'app/shared/model/khamchuabenh/nghe-nghiep.model';
import DataSource from 'devextreme/data/data_source';
import { ITinhThanhPho } from 'app/shared/model/khamchuabenh/tinh-thanh-pho.model';
import { IQuanHuyen } from 'app/shared/model/khamchuabenh/quan-huyen.model';
import { IPhuongXa } from 'app/shared/model/khamchuabenh/phuong-xa.model';

interface IThongTinHanhChinhProp {
  values: IBenhNhan;
  tinhTuoi?: Function;
  onChange?: any;
  countries?: ReadonlyArray<ICountry>;
  danTocList?: ReadonlyArray<IDanToc>;
  ngheNghiepList?: ReadonlyArray<INgheNghiep>;
  tinhThanhPhoList?: ReadonlyArray<ITinhThanhPho>;
  quanHuyenList?: ReadonlyArray<IQuanHuyen>;
  phuongXaList?: ReadonlyArray<IPhuongXa>;
  inputState?: any;
}

export const ThongTinHanhChinh = React.memo((props: IThongTinHanhChinhProp) => {
  const { values, onChange, countries, ngheNghiepList, danTocList } = props;
  const { tinhThanhPhoList, quanHuyenList, phuongXaList } = props;
  // eslint-disable-next-line no-console
  // console.log(props.inputState);

  const countriesDataSource = useMemo(() => {
    return new DataSource({
      store: {
        data: countries,
        type: 'array',
        key: 'id'
      }
    });
  }, [countries]);

  const danTocDataSource = useMemo(() => {
    return new DataSource({
      store: {
        data: danTocList,
        type: 'array',
        key: 'id'
      }
    });
  }, [danTocList]);

  const ngheNghiepDataSource = useMemo(() => {
    return new DataSource({
      store: {
        data: ngheNghiepList,
        type: 'array',
        key: 'id'
      }
    });
  }, [ngheNghiepList]);

  const tinhThanhPhoDataSource = useMemo(() => {
    return new DataSource({
      store: {
        data: tinhThanhPhoList,
        type: 'array',
        key: 'id'
      }
    });
  }, [tinhThanhPhoList]);

  const quanHuyenDataSource = useMemo(() => {
    return new DataSource({
      store: {
        data: quanHuyenList,
        type: 'array',
        key: 'id'
      }
    });
  }, [quanHuyenList]);
  const phuongXaDataSource = useMemo(() => {
    return new DataSource({
      store: {
        data: phuongXaList,
        type: 'array',
        key: 'id'
      }
    });
  }, [phuongXaList]);
  const gioiTinhOption = useMemo(() => {
    return {
      displayExpr: 'text',
      valueExpr: 'value',
      dataSource: GIOI_TINH,
      disabled: props.inputState.gioiTinh
    };
  }, []);
  const ngaySinhOption = useMemo(() => {
    return {
      max: new Date(),
      min: '1900/01/01',
      displayFormat: 'dd/MM/yyyy',
      useMaskBehavior: true,
      disabled: props.inputState.ngaySinh
    };
  }, []);
  //
  const chiCoNamSinhOption = useMemo(() => {
    return {
      disabled: props.inputState.chiCoNamSinh
    };
    // return {defaultValue: false}
  }, []);

  const tuoiOption = useMemo(() => {
    return {
      // readOnly: true
      disabled: props.inputState.tuoi
    };
  }, []);
  const ngayOption = useMemo(() => {
    return {
      // readOnly: true
      disabled: props.inputState.ngay
    };
  }, []);
  const thangOption = useMemo(() => {
    return {
      // readOnly: true
      disabled: props.inputState.thang
    };
  }, []);

  const hoChieuOption = useMemo(() => {
    return {
      // readOnly: true
      disabled: props.inputState.hoChieu
    };
  }, []);

  const soDienThoaiOption = useMemo(() => {
    return {
      // readOnly: true
      disabled: props.inputState.soDienThoai
    };
  }, []);

  const diaChiOption = useMemo(() => {
    return {
      // readOnly: true
      disabled: props.inputState.diaChi
    };
  }, []);

  const apThonOption = useMemo(() => {
    return {
      // readOnly: true
      disabled: props.inputState.apThon
    };
  }, []);

  const quocTichOption = useMemo(() => {
    return {
      searchEnabled: true,
      dataSource: countriesDataSource,
      displayExpr: 'tenCtk',
      valueExpr: 'id',
      noDataText: 'Không có dữ liệu',
      disabled: props.inputState.quocTich
    };
  }, [countries]);
  const dantocOption = useMemo(() => {
    return {
      searchEnabled: true,
      dataSource: danTocDataSource,
      displayExpr: 'ten4069Byt',
      valueExpr: 'id',
      noDataText: 'Không có dữ liệu',
      disabled: props.inputState.danToc
    };
  }, [danTocList]);
  const ngheNghiepOption = useMemo(() => {
    return {
      searchEnabled: true,
      dataSource: ngheNghiepDataSource,
      displayExpr: 'ten4069',
      valueExpr: 'id',
      noDataText: 'Không có dữ liệu',
      disabled: props.inputState.ngheNghiep
    };
  }, [ngheNghiepList]);

  const tinhThanhPhoOption = useMemo(() => {
    return {
      searchEnabled: true,
      dataSource: tinhThanhPhoDataSource,
      displayExpr: 'ten',
      valueExpr: 'id',
      noDataText: 'Không có dữ liệu',
      disabled: props.inputState.tinhThanhPho
    };
  }, [tinhThanhPhoList]);

  const quanHuyenOption = useMemo(() => {
    return {
      searchEnabled: true,
      dataSource: quanHuyenDataSource,
      displayExpr: 'ten',
      valueExpr: 'id',
      noDataText: 'Không có dữ liệu',
      disabled: props.inputState.quanHuyen
    };
  }, [values.tinhThanhPhoId]);
  const phuongXaOption = useMemo(() => {
    return {
      searchEnabled: true,
      dataSource: phuongXaDataSource,
      displayExpr: 'ten',
      valueExpr: 'id',
      noDataText: 'Không có dữ liệu',
      disabled: props.inputState.phuongXa
    };
  }, [values.quanHuyenId]);

  const maBenhNhanOption = useMemo(() => {
    return {
      // readOnly: props.inputState ? true : props.inputState.benhNhanId
      disabled: props.inputState ? true : props.inputState.benhNhanId
    };
  }, [props.inputState]);

  const hoTenOption = useMemo(() => {
    return {
      // readOnly: props.inputState.hoTen
      disabled: props.inputState.hoTen
    };
  }, [props.inputState]);
  return (
    <>
      <Form formData={values} onFieldDataChanged={onChange} showColonAfterLabel>
        <GroupItem colCount={3}>
          <SimpleItem colSpan={2} dataField="id" editorType="dxTextBox" editorOptions={maBenhNhanOption}>
            <Label text={'Mã bệnh nhân'} />
          </SimpleItem>
          <Item colSpan={1}>
            <Button text="Tìm bệnh nhân" disabled={props.inputState.timBNBtn} />
          </Item>
        </GroupItem>
        <GroupItem colCount={3}>
          <SimpleItem colSpan={2} isRequired editorType={'dxTextBox'} dataField={'ten'} editorOptions={hoTenOption}>
            <Label text={'Họ tên'} />
            <RequiredRule message={'Không được đễ trống'} />
            <PatternRule pattern={/^[^0-9!@#$%^&*)(+=._-]+$/} message="Tên không chứa ký tự đặc biệt hoặc số" />
          </SimpleItem>
          <SimpleItem
            colSpan={1}
            editorType="dxSelectBox"
            dataField="gioiTinh"
            editorOptions={gioiTinhOption}
            // label={{text: 'Giới tính'}}
          >
            <Label text={'Giới tính'} />
          </SimpleItem>
        </GroupItem>
        <GroupItem colCount={8}>
          <SimpleItem
            colSpan={5}
            isRequired
            dataField={'ngaySinh'}
            editorType={'dxDateBox'}
            editorOptions={ngaySinhOption}
            // label={{text: 'Ngày sinh'}}
          >
            <Label text={'Ngày sinh'} />
            <RequiredRule message={'Không được đễ trống'} />
          </SimpleItem>

          <SimpleItem
            colSpan={3}
            editorType="dxCheckBox"
            dataField="chiCoNamSinh"
            editorOptions={chiCoNamSinhOption}
            // label={{text: "Chỉ có năm sinh"}}
          >
            <Label text={'Chỉ có năm sinh'} />
          </SimpleItem>
        </GroupItem>
        <GroupItem colCount={7}>
          <SimpleItem
            colSpan={3}
            editorType="dxTextBox"
            dataField="tuoi"
            // label={{text: 'Tuổi'}}
            editorOptions={tuoiOption}
          >
            <Label text={'Tuổi'} />
          </SimpleItem>

          <SimpleItem
            colSpan={2}
            editorType="dxTextBox"
            dataField="thang"
            // label={{text: 'Tháng'}}
            editorOptions={thangOption}
          >
            <Label text={'Tháng'} />
          </SimpleItem>

          <SimpleItem
            editorType="dxTextBox"
            colSpan={2}
            dataField="ngay"
            // label={{text: 'Ngày'}}
            editorOptions={ngayOption}
          >
            <Label text={'Ngày'} />
          </SimpleItem>
        </GroupItem>
        <GroupItem colCount={2}>
          <SimpleItem
            colSpan={1}
            editorType="dxSelectBox"
            dataField="quocTichId"
            editorOptions={quocTichOption}
            // label={{text: "Quốc tịch"}}
          >
            <Label text={'Quốc tịch'} />
          </SimpleItem>
          <SimpleItem
            colSpan={1}
            editorType="dxTextBox"
            dataField="cmnd"
            // label={{text: "CMND/ Hộ chiếu"}}
            editorOptions={hoChieuOption}
          >
            <Label text={'CMND/Hộ chiếu'} />
            <PatternRule message="Không sử dụng chữ" pattern={/^[0-9]+$/} />
          </SimpleItem>
        </GroupItem>
        <GroupItem colCount={2}>
          <SimpleItem
            colSpan={1}
            editorType="dxSelectBox"
            dataField="danTocId"
            // label={{text: "Dân tộc"}}
            editorOptions={dantocOption}
          >
            <Label text={'Dân tộc'} />
          </SimpleItem>
          <SimpleItem
            colSpan={1}
            editorType="dxSelectBox"
            dataField="ngheNghiepId"
            // label={{text: "Nghề Nghiệp"}}
            editorOptions={ngheNghiepOption}
          >
            <Label text={'Nghề nghiệp'} />
          </SimpleItem>
        </GroupItem>
        <GroupItem colCount={3}>
          <SimpleItem
            colSpan={2}
            dataField="phone"
            editorType="dxTextBox"
            // label={{text: "Số điện thoại"}}
            editorOptions={soDienThoaiOption}
          >
            <Label text={'Số điện thoại'} />
            <PatternRule message="Không sử dụng chữ" pattern={/^[0-9]+$/} />
            <StringLengthRule message="Tối thiểu 10 số" min={10} />
          </SimpleItem>
          <Item colSpan={1}>
            <Button text="Người liên hệ" disabled={props.inputState.nguoiLienHeBtn}/>
          </Item>
        </GroupItem>

        <GroupItem colCount={2}>
          <SimpleItem
            colSpan={1}
            editorType="dxSelectBox"
            dataField="tinhThanhPhoId"
            // label={{text: 'Tỉnh/thành phố'}}
            editorOptions={tinhThanhPhoOption}
          >
            <Label text={'Tỉnh/thảnh phố'} />
          </SimpleItem>
          <SimpleItem
            colSpan={1}
            editorType="dxSelectBox"
            dataField="quanHuyenId"
            // label={{text: 'Quận/huyện'}}
            editorOptions={quanHuyenOption}
          >
            <Label text={'Quận/huyện'} />
          </SimpleItem>
        </GroupItem>
        <GroupItem colCount={2}>
          <SimpleItem
            colSpan={1}
            editorType="dxSelectBox"
            dataField="phuongXaId"
            // label={{text: 'Phường/xã'}}
            editorOptions={phuongXaOption}
          >
            <Label text={'Phường/xã'} />
          </SimpleItem>

          <SimpleItem
            colSpan={1}
            editorType="dxTextBox"
            dataField="apThon"
            editorOptions={apThonOption}
            // label={{text: 'Ấp/thôn'}}
          >
            <Label text={'Ấp/thôn'} />
          </SimpleItem>
        </GroupItem>
        <GroupItem colCount={1}>
          <SimpleItem
            colSpan={1}
            editorType="dxTextBox"
            dataField="diaChiThuongTru"
            // label={{text: "Địa chỉ"}}
            editorOptions={diaChiOption}
            isRequired
          >
            <Label text={'Địa chỉ'} />
            <RequiredRule message={'Không được để trống'} />
          </SimpleItem>
        </GroupItem>
      </Form>
    </>
  );
});
