import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { Translate, translate } from 'react-jhipster';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown icon="th-list" name={translate('global.menu.entities.main')} id="entity-menu">
    <MenuItem icon="asterisk" to="/don-vi">
      <Translate contentKey="global.menu.entities.khamchuabenhDonVi" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/thong-tin-bhxh">
      <Translate contentKey="global.menu.entities.khamchuabenhThongTinBhxh" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/benh-nhan">
      <Translate contentKey="global.menu.entities.khamchuabenhBenhNhan" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/dan-toc">
      <Translate contentKey="global.menu.entities.khamchuabenhDanToc" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/nghe-nghiep">
      <Translate contentKey="global.menu.entities.khamchuabenhNgheNghiep" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/country">
      <Translate contentKey="global.menu.entities.khamchuabenhCountry" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/tinh-thanh-pho">
      <Translate contentKey="global.menu.entities.khamchuabenhTinhThanhPho" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/quan-huyen">
      <Translate contentKey="global.menu.entities.khamchuabenhQuanHuyen" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/phuong-xa">
      <Translate contentKey="global.menu.entities.khamchuabenhPhuongXa" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/user-extra">
      <Translate contentKey="global.menu.entities.khamchuabenhUserExtra" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/menu">
      <Translate contentKey="global.menu.entities.khamchuabenhMenu" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/user-type">
      <Translate contentKey="global.menu.entities.khamchuabenhUserType" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/the-bhyt">
      <Translate contentKey="global.menu.entities.khamchuabenhTheBhyt" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/doi-tuong-bhyt">
      <Translate contentKey="global.menu.entities.khamchuabenhDoiTuongBhyt" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/nhom-doi-tuong-bhyt">
      <Translate contentKey="global.menu.entities.khamchuabenhNhomDoiTuongBhyt" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/khoa">
      <Translate contentKey="global.menu.entities.khamchuabenhKhoa" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/nhan-vien">
      <Translate contentKey="global.menu.entities.khamchuabenhNhanVien" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/chuc-danh">
      <Translate contentKey="global.menu.entities.khamchuabenhChucDanh" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/chuc-vu">
      <Translate contentKey="global.menu.entities.khamchuabenhChucVu" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/dich-vu-kham">
      <Translate contentKey="global.menu.entities.khamchuabenhDichVuKham" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/dot-thay-doi-ma-dich-vu">
      <Translate contentKey="global.menu.entities.khamchuabenhDotThayDoiMaDichVu" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/phong">
      <Translate contentKey="global.menu.entities.khamchuabenhPhong" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/benh-an-kham-benh">
      <Translate contentKey="global.menu.entities.khamchuabenhBenhAnKhamBenh" />
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
