import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavItem, NavLink, NavbarBrand } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { NavLink as Link } from 'react-router-dom';

export const TiepNhan = () => (
  <NavItem>
    <NavLink tag={Link} to="/tiep-nhan" className="d-flex align-items-center">
      {/* <FontAwesomeIcon icon="home" /> */}
      <span>
        <Translate contentKey="global.menu.outpatient">OutPatient</Translate>
      </span>
    </NavLink>
  </NavItem>
);
