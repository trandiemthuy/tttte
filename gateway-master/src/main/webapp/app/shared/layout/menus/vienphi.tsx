import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { translate, Translate } from 'react-jhipster';
import { NavDropdown } from 'app/shared/layout/menus/menu-components';

export const VienPhiMenu = (props) => (
  <NavDropdown icon="dollar-sign" name={translate('vienphi.menu.title')} style={{ width: 'auto' }} id="vienphi-menu">
    <MenuItem icon="hand-holding-usd" to="/vien-phi/thu-vien-phi">
      <Translate contentKey="vienphi.menu.thutien">Thu viện phí - tạm ứng</Translate>
    </MenuItem>
    <MenuItem icon="money-check" to="/vien-phi/kiem-tra-bang-ke">
      <Translate contentKey="vienphi.menu.kiemtrabangke">Kiểm tra bảng kê</Translate>
    </MenuItem>
    <MenuItem icon="wallet" to="/vien-phi/bao-cao-bhxh">
      <Translate contentKey="vienphi.menu.baocaobhxh">Báo cáo BHXH</Translate>
    </MenuItem>
    <MenuItem icon="book" to="/vien-phi/quan-ly-bien-lai">
      <Translate contentKey="vienphi.menu.quanlybienlai">Quản lý biên lai</Translate>
    </MenuItem>
    <MenuItem icon="calculator" to="/vien-phi/tao-so-lieu-hang-loat">
      <Translate contentKey="vienphi.menu.taosolieuhangloat">Tạo số liệu hàng loạt</Translate>
    </MenuItem>
  </NavDropdown>
);

export default VienPhiMenu;
