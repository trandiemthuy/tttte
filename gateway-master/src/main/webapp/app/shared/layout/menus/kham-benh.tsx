import React from 'react';
import { NavItem, NavLink } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { NavLink as Link } from 'react-router-dom';
import StethoScopeWhite from 'app/shared/assets/icons/stethoscope-white.svg';

export const KhamBenh = () => (
  <NavItem>
    <NavLink tag={Link} to="/kham-benh" className="d-flex align-items-center">
      <span>
      <img alt={'stethoscope icon'} src={StethoScopeWhite} height={16} width={16} style={{marginRight: 3, paddingBottom: 2}}/>
        <Translate contentKey="global.menu.khambenh" >Khám bệnh</Translate>
      </span>
    </NavLink>
  </NavItem>
);
