export interface INhomDoiTuongBhyt {
  id?: number;
  ten?: string;
}

export const defaultValue: Readonly<INhomDoiTuongBhyt> = {};
