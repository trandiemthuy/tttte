export interface IDichVuKham {
  id?: number;
  deleted?: number;
  enabled?: number;
  gioiHanChiDinh?: number;
  phanTheoGioTinh?: number;
  tenHienThi?: string;
  maDungChung?: string;
  maNoiBo?: string;
  phamViChiDinh?: number;
  donViTinh?: string;
  donGiaBenhVien?: number;
  dotThayDoiMaDichVuId?: number;
}

export const defaultValue: Readonly<IDichVuKham> = {};
