export interface IDanToc {
  id?: number;
  ma4069Byt?: number;
  maCucThongKe?: number;
  ten4069Byt?: string;
  tenCucThongKe?: string;
}

export const defaultValue: Readonly<IDanToc> = {};
