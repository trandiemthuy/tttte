export interface IBenhAnKhamBenh {
  id?: number;
  benhNhanCu?: boolean;
  canhBao?: boolean;
  capCuu?: boolean;
  chiCoNamSinh?: boolean;
  cmndBenhNhan?: string;
  cmndNguoiNha?: string;
  coBaoHiem?: boolean;
  diaChi?: string;
  email?: string;
  gioiTinh?: number;
  khangThe?: string;
  lanKhamTrongNgay?: number;
  loaiGiayToTreEm?: number;
  ngayCapCmnd?: Date;
  ngayMienCungChiTra?: Date;
  ngaySinh?: Date;
  nhomMau?: string;
  noiCapCmnd?: string;
  noiLamViec?: string;
  phone?: string;
  phoneNguoiNha?: string;
  quocTich?: string;
  tenBenhNhan?: string;
  tenNguoiNha?: string;
  thangTuoi?: number;
  thoiGianTiepNhan?: Date;
  tuoi?: number;
  uuTien?: number;
  uuid?: string;
  trangThai?: number;
  maKhoaHoanTatKham?: number;
  maPhongHoanTatKham?: number;
  tenKhoaHoanTatKham?: string;
  tenPhongHoanTatKham?: string;
  thoiGianHoanTatKham?: Date;
  soBenhAn?: string;
  soBenhAnKhoa?: string;
  nam?: number;
  benhNhanId?: number;
  donViId?: number;
  nhanVienTiepNhanId?: number;
  phongTiepNhanId?: number;
}

export const defaultValue: Readonly<IBenhAnKhamBenh> = {
  benhNhanCu: false,
  canhBao: false,
  capCuu: false,
  chiCoNamSinh: false,
  coBaoHiem: false
};
