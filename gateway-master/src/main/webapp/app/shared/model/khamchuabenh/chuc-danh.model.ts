export interface IChucDanh {
  id?: number;
  moTa?: string;
  ten?: string;
}

export const defaultValue: Readonly<IChucDanh> = {};
