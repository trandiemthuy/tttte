export interface ICountry {
  id?: number;
  isoCode?: string;
  isoNumeric?: string;
  name?: string;
  maCtk?: number;
  tenCtk?: string;
}

export const defaultValue: Readonly<ICountry> = {};
