import { Moment } from 'moment';

export interface IDotThayDoiMaDichVu {
  id?: number;
  doiTuongApDung?: number;
  ghiChu?: string;
  ngayApDung?: Moment;
  ten?: string;
  donViId?: number;
}

export const defaultValue: Readonly<IDotThayDoiMaDichVu> = {};
