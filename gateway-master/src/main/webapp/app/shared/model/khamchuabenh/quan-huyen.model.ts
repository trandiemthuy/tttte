export interface IQuanHuyen {
  id?: number;
  cap?: string;
  guessPhrase?: string;
  ten?: string;
  tenKhongDau?: string;
  tinhThanhPhoId?: number;
}

export const defaultValue: Readonly<IQuanHuyen> = {};
