import { IMenu } from 'app/shared/model/khamchuabenh/menu.model';

export interface IUserExtra {
  id?: number;
  enabled?: boolean;
  username?: string;
  userId?: number;
  userTypeId?: number;
  menus?: IMenu[];
}

export const defaultValue: Readonly<IUserExtra> = {
  enabled: false
};
