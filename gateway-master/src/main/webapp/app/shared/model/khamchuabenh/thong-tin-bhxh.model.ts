export interface IThongTinBhxh {
  id?: number;
  dvtt?: string;
  enabled?: boolean;
  ngayApDungBhyt?: Date;
  ten?: string;
  donViId?: number;
}

export const defaultValue: Readonly<IThongTinBhxh> = {
  enabled: false
};
