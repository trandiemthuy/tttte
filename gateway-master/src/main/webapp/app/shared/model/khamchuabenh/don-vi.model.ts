export interface IDonVi {
  id?: number;
  cap?: number;
  chiNhanhNganHang?: string;
  diaChi?: string;
  donViQuanLyId?: number;
  dvtt?: string;
  email?: string;
  enabled?: boolean;
  kyHieu?: string;
  loai?: number;
  maTinh?: string;
  phone?: string;
  soTaiKhoan?: string;
  ten?: string;
  tenNganHang?: string;
  thongTinBhxhId?: number;
}

export const defaultValue: Readonly<IDonVi> = {
  enabled: false
};
