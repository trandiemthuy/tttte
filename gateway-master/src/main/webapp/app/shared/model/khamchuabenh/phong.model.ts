export interface IPhong {
  id?: number;
  enabled?: boolean;
  kyHieu?: string;
  loai?: number;
  phone?: string;
  ten?: string;
  viTri?: string;
  khoaId?: number;
}

export const defaultValue: Readonly<IPhong> = {
  enabled: false
};
