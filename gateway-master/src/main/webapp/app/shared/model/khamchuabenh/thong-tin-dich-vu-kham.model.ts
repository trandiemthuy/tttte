export interface IThongTinDichVuKham {
  donGiaBenhVien?: number;
  donViTinh?: string;
  giaBhyt?: number;
  giaKhongBhyt?: number;
  id?: number;
  maDungChung?: string;
  maNoiBo?: string;
  tenDichVuKhongBaoHiem?: string;
  tenHienThi?: string;
  tienBenhNhanChi?: number;
  tienBhxhChi?: number;
  tienNgoaiBhyt?: number;
  tongTienThanhToan?: number;
  tyLeBhytThanhToan?: number;
}

export const defaultValue: Readonly<IThongTinDichVuKham> = {};
