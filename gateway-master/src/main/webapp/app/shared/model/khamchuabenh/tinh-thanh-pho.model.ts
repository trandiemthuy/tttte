export interface ITinhThanhPho {
  id?: number;
  cap?: string;
  guessPhrase?: string;
  ten?: string;
  tenKhongDau?: string;
}

export const defaultValue: Readonly<ITinhThanhPho> = {};
