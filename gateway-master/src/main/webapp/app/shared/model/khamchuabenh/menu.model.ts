import { IUserExtra } from 'app/shared/model/khamchuabenh/user-extra.model';

export interface IMenu {
  id?: number;
  enabled?: boolean;
  level?: number;
  name?: string;
  uri?: string;
  parent?: number;
  donViId?: number;
  users?: IUserExtra[];
}

export const defaultValue: Readonly<IMenu> = {
  enabled: false
};
