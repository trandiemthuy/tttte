export interface IPhuongXa {
  id?: number;
  cap?: string;
  guessPhrase?: string;
  ten?: string;
  tenKhongDau?: string;
  quanHuyenId?: number;
}

export const defaultValue: Readonly<IPhuongXa> = {};
