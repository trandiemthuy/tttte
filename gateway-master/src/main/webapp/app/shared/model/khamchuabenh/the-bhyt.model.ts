export interface ITheBhyt {
  id?: number;
  enabled?: boolean;
  maKhuVuc?: string;
  maSoBhxh?: string;
  maTheCu?: string;
  ngayBatDau?: Date;
  ngayDuNamNam?: Date;
  ngayHetHan?: Date;
  qr?: string;
  soThe?: string;
  ngayMienCungChiTra?: Date;
  benhNhanId?: number;
  doiTuongBhytId?: number;
  thongTinBhxhId?: number;
}

export const defaultValue: Readonly<ITheBhyt> = {
  enabled: false
};
