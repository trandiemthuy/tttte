export interface IKhoa {
  id?: number;
  cap?: number;
  enabled?: boolean;
  kyHieu?: string;
  phone?: string;
  ten?: string;
  donViId?: number;
}

export const defaultValue: Readonly<IKhoa> = {
  enabled: false
};
