export interface IBaoHiem {
  soBHYT?: string;
  dtTheId?: number;
  chuoiNhanDang?: string;
  tyLemienGiam?: number;
  tuNgay?: Date;
  denNgay?: Date;
  maDK?: string;
  noiDK?: string;
  maKV?: string;
  giayTE1?: string;
  tenGiayTE1?: string;
  coMCCT?: boolean;
  ngayMCCT?: Date;
  ngayConLaiBH5Nam?: Date;
  maNoiChuyenDen?: string;
  noiChuyenDen?: string;
  maNoiChuyen?: string;
  noiChuyen?: string;
  capCuu?: boolean;
  khamUuTien?: boolean;
  dungTuyen?: boolean;
  khamOnline?: boolean;
  phongKhamId?: number;
  dichVuId?: number;
}

export const defaultValue: Readonly<IBaoHiem> = {
  capCuu: false,
  khamUuTien: false,
  dungTuyen: false,
  khamOnline: false,
  coMCCT: false,
  tuNgay: new Date(),
  denNgay: new Date()
};
