export interface IDoiTuongBhyt {
  id?: number;
  canTren?: number;
  canTrenKtc?: number;
  ma?: string;
  ten?: string;
  tyLeMienGiam?: number;
  tyLeMienGiamKtc?: number;
  nhomDoiTuongBhytId?: number;
}

export const defaultValue: Readonly<IDoiTuongBhyt> = {};
