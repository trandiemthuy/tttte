export interface INgheNghiep {
  id?: number;
  ma4069?: number;
  ten4069?: string;
}

export const defaultValue: Readonly<INgheNghiep> = {};
