export interface IBenhNhan {
  id?: number;
  chiCoNamSinh?: boolean;
  cmnd?: string;
  email?: string;
  enabled?: boolean;
  gioiTinh?: number;
  khangThe?: string;
  maSoThue?: string;
  ngayCapCmnd?: Date;
  ngaySinh?: Date;
  nhomMau?: string;
  noiCapCmnd?: string;
  noiLamViec?: string;
  phone?: string;
  ten?: string;
  soNhaXom?: string;
  apThon?: string;
  diaPhuongId?: number;
  diaChiThuongTru?: string;
  danTocId?: number;
  ngheNghiepId?: number;
  quocTichId?: number;
  phuongXaId?: number;
  quanHuyenId?: number;
  tinhThanhPhoId?: number;
  userExtraId?: number;
}

export const defaultValue: Readonly<IBenhNhan> = {
  chiCoNamSinh: false,
  enabled: false,
  ngaySinh: new Date(),
  gioiTinh: 2,
  quocTichId: 242,
  danTocId: 25,
  ngheNghiepId: 5,
  tinhThanhPhoId: 82
};
