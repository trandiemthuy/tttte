export interface IUserType {
  id?: number;
  moTa?: string;
}

export const defaultValue: Readonly<IUserType> = {};
