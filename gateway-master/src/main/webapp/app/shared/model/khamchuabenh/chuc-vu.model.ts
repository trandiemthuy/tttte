import { INhanVien } from 'app/shared/model/khamchuabenh/nhan-vien.model';

export interface IChucVu {
  id?: number;
  moTa?: string;
  ten?: string;
  nhanViens?: INhanVien[];
}

export const defaultValue: Readonly<IChucVu> = {};
