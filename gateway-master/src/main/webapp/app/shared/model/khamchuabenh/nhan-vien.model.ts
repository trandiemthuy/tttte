import { Moment } from 'moment';
import { IChucVu } from 'app/shared/model/khamchuabenh/chuc-vu.model';

export interface INhanVien {
  id?: number;
  ten?: string;
  chungChiHanhNghe?: string;
  cmnd?: string;
  gioiTinh?: number;
  ngaySinh?: Moment;
  chucDanhId?: number;
  userExtraId?: number;
  chucVus?: IChucVu[];
}

export const defaultValue: Readonly<INhanVien> = {};
