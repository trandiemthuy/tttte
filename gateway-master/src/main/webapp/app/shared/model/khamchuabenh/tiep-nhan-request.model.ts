import { ITiepNhan } from 'app/shared/model/khamchuabenh/tiep-nhan.model';

export interface ITiepNhanRequest {
  dichVuKhamId?: number;
  tiepNhanDTO?: ITiepNhan;
}

export const defaultValue: Readonly<ITiepNhanRequest> = {
  // enabled: false
};
