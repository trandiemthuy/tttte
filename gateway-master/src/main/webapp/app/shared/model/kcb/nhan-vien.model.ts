export interface INhanVien {
  id: number;
  ten: string;
  chungChiHanhNghe?: string;
  cmnd?: string;
  gioiTinh?: bigint;
  ngaySinh?: Date;
  chucDanhId?: number;
  userExtraId?: number;
  chucVus?: [];
}
