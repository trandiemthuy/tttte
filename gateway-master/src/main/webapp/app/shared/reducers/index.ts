import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from './user-management';
import thongtintiepnhan, { ThongTinTiepNhanState } from 'app/modules/TiepNhan/TiepNhan.reducer';

// prettier-ignore
import donVi, {DonViState} from 'app/entities/khamchuabenh/don-vi/don-vi.reducer';
// prettier-ignore
import thongTinBhxh, {ThongTinBhxhState} from 'app/entities/khamchuabenh/thong-tin-bhxh/thong-tin-bhxh.reducer';

import vienPhiThuTien, { VienPhiThuTienState } from 'app/modules/vien-phi/thu-vien-phi/thu-vien-phi.reducer';
import kiemTraBangKe, { KiemTraBangKeState } from 'app/modules/vien-phi/kiem-tra-bang-ke/kiem-tra-bang-ke.reducer';
import baoCaoBhxh, { BaoCaoBhxhState } from 'app/modules/vien-phi/bao-cao-bhxh/bao-cao-bhxh.reducer';
import taoSoLieuHangLoat, { TaoSoLieuHangLoatState } from 'app/modules/vien-phi/tao-so-lieu-hang-loat/tao-so-lieu-hang-loat.reducer';

/* ------------------------------------------------- Dược - MS ------------------------------------------------------*/
import nhapKho, { nhapKhoState } from 'app/modules/duoc/duoc-components/nhap-kho/nhap-kho.reducer';
import chuyenkho, { chuyenKhoState } from 'app/modules/duoc/duoc-components/chuyen-kho/chuyen-kho.reducer';
import duyetphieu, { duyetPhieuState } from 'app/modules/duoc/duoc-components/duyet-phieu/duyet-phieu.reducer';
import duyetdutruhoantra, {
  duyetDuTruHoanTraState
} from 'app/modules/duoc/duoc-components/duyet-du-tru-hoan-tra/duyet-du-tru-hoan-tra.reducer';
import nhanduoc, { nhanDuocState } from 'app/modules/duoc/duoc-components/nhan-duoc/nhan-duoc.reducer';
import kiemtraduyetnhan, { kiemTraDuyetNhanState } from 'app/modules/duoc/duoc-components/kiem-tra-duyet-nhan/kiem-tra-duyet-nhan.reducer';
import tonghopdutru, { tongHopDuTruState } from 'app/modules/duoc/duoc-components/tong-hop-du-tru/tong-hop-du-tru.reducer';
import danhsachkhambenh, { danhSachKhamBenhState } from 'app/modules/KhamChuaBenh/DanhSachKhamBenh/DanhSachKhamBenh.reducer';
import xuatduoc, { xuatDuocState } from 'app/modules/duoc/duoc-components/xuat-duoc/xuat-duoc.reducer';
import hethong, { heThongState } from 'app/modules/duoc/duoc-components/he-thong/he-thong.reducer';
import xuatduocbanle, { xuatDuocBanLeState } from 'app/modules/duoc/duoc-components/xuat-duoc-ban-le/xuat-duoc-ban-le.reducer';
import kcb, { kcbState } from 'app/modules/duoc/duoc-components/kcb/kcb.reducer';

// prettier-ignore
import benhNhan, {
  BenhNhanState
} from 'app/entities/khamchuabenh/benh-nhan/benh-nhan.reducer';
// prettier-ignore
import danToc, {
  DanTocState
} from 'app/entities/khamchuabenh/dan-toc/dan-toc.reducer';
// prettier-ignore
import ngheNghiep, {
  NgheNghiepState
} from 'app/entities/khamchuabenh/nghe-nghiep/nghe-nghiep.reducer';
// prettier-ignore
import country, {
  CountryState
} from 'app/entities/khamchuabenh/country/country.reducer';
// prettier-ignore
import tinhThanhPho, {
  TinhThanhPhoState
} from 'app/entities/khamchuabenh/tinh-thanh-pho/tinh-thanh-pho.reducer';
// prettier-ignore
import quanHuyen, {
  QuanHuyenState
} from 'app/entities/khamchuabenh/quan-huyen/quan-huyen.reducer';
// prettier-ignore
import phuongXa, {
  PhuongXaState
} from 'app/entities/khamchuabenh/phuong-xa/phuong-xa.reducer';
// prettier-ignore
import userExtra, {
  UserExtraState
} from 'app/entities/khamchuabenh/user-extra/user-extra.reducer';
// prettier-ignore
import menu, {
  MenuState
} from 'app/entities/khamchuabenh/menu/menu.reducer';
// prettier-ignore
import userType, {
  UserTypeState
} from 'app/entities/khamchuabenh/user-type/user-type.reducer';
// prettier-ignore
import theBhyt, {
  TheBhytState
} from 'app/entities/khamchuabenh/the-bhyt/the-bhyt.reducer';
// prettier-ignore
import doiTuongBhyt, {
  DoiTuongBhytState
} from 'app/entities/khamchuabenh/doi-tuong-bhyt/doi-tuong-bhyt.reducer';
// prettier-ignore
import nhomDoiTuongBhyt, {
  NhomDoiTuongBhytState
} from 'app/entities/khamchuabenh/nhom-doi-tuong-bhyt/nhom-doi-tuong-bhyt.reducer';
// prettier-ignore
import khoa, {
  KhoaState
} from 'app/entities/khamchuabenh/khoa/khoa.reducer';
// prettier-ignore
import nhanVien, {
  NhanVienState
} from 'app/entities/khamchuabenh/nhan-vien/nhan-vien.reducer';
// prettier-ignore
import chucDanh, {
  ChucDanhState
} from 'app/entities/khamchuabenh/chuc-danh/chuc-danh.reducer';
// prettier-ignore
import chucVu, {
  ChucVuState
} from 'app/entities/khamchuabenh/chuc-vu/chuc-vu.reducer';
// prettier-ignore
import dichVuKham, {
  DichVuKhamState
} from 'app/entities/khamchuabenh/dich-vu-kham/dich-vu-kham.reducer';
// prettier-ignore
import dotThayDoiMaDichVu, {
  DotThayDoiMaDichVuState
} from 'app/entities/khamchuabenh/dot-thay-doi-ma-dich-vu/dot-thay-doi-ma-dich-vu.reducer';
// prettier-ignore
import phong, {
  PhongState
} from 'app/entities/khamchuabenh/phong/phong.reducer';
// prettier-ignore
import benhAnKhamBenh, {
  BenhAnKhamBenhState
} from 'app/entities/khamchuabenh/benh-an-kham-benh/benh-an-kham-benh.reducer';
import tiepNhan, { TiepNhanState } from 'app/modules/TiepNhan/reducers/tiep-nhan.reducers';
/*  jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly donVi: DonViState;
  readonly thongTinBhxh: ThongTinBhxhState;

  readonly benhNhan: BenhNhanState;
  readonly danToc: DanTocState;
  readonly ngheNghiep: NgheNghiepState;
  readonly country: CountryState;
  readonly tinhThanhPho: TinhThanhPhoState;
  readonly quanHuyen: QuanHuyenState;
  readonly phuongXa: PhuongXaState;
  readonly userExtra: UserExtraState;
  readonly menu: MenuState;
  readonly userType: UserTypeState;
  readonly theBhyt: TheBhytState;
  readonly doiTuongBhyt: DoiTuongBhytState;
  readonly nhomDoiTuongBhyt: NhomDoiTuongBhytState;
  readonly khoa: KhoaState;
  readonly nhanVien: NhanVienState;
  readonly chucDanh: ChucDanhState;
  readonly chucVu: ChucVuState;
  readonly dichVuKham: DichVuKhamState;
  readonly dotThayDoiMaDichVu: DotThayDoiMaDichVuState;
  readonly phong: PhongState;
  readonly benhAnKhamBenh: BenhAnKhamBenhState;
  /*  jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
  readonly vienPhiThuTien: VienPhiThuTienState;
  readonly kiemTraBangKe: KiemTraBangKeState;
  readonly baoCaoBhxh: BaoCaoBhxhState;
  readonly taoSoLieuHangLoat: TaoSoLieuHangLoatState;

  /* ------------------------------------------------- Dược - MS ------------------------------------------------------*/
  readonly nhapKho: nhapKhoState;
  readonly chuyenkho: chuyenKhoState;
  readonly duyetphieu: duyetPhieuState;
  readonly duyetdutruhoantra: duyetDuTruHoanTraState;
  readonly nhanduoc: nhanDuocState;
  readonly kiemtraduyetnhan: kiemTraDuyetNhanState;
  readonly tonghopdutru: tongHopDuTruState;
  readonly danhsachkhambenh: danhSachKhamBenhState;
  readonly xuatduoc: xuatDuocState;
  readonly hethong: heThongState;
  readonly xuatduocbanle: xuatDuocBanLeState;
  readonly kcb: kcbState;
  // Tiep Nhan
  readonly thongtintiepnhan: ThongTinTiepNhanState;
  readonly tiepNhan: TiepNhanState;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  donVi,
  thongTinBhxh,
  benhNhan,
  danToc,
  ngheNghiep,
  country,
  tinhThanhPho,
  quanHuyen,
  phuongXa,
  userExtra,
  menu,
  userType,
  theBhyt,
  doiTuongBhyt,
  nhomDoiTuongBhyt,
  khoa,
  nhanVien,
  chucDanh,
  chucVu,
  dichVuKham,
  dotThayDoiMaDichVu,
  phong,
  benhAnKhamBenh,
  /*  jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
  vienPhiThuTien,
  kiemTraBangKe,
  baoCaoBhxh,
  taoSoLieuHangLoat,

  /* ------------------------------------------------- Dược - MS -----------------------------------------------------*/
  nhapKho,
  chuyenkho,
  duyetphieu,
  duyetdutruhoantra,
  nhanduoc,
  kiemtraduyetnhan,
  danhsachkhambenh,
  tonghopdutru,
  xuatduoc,
  hethong,
  xuatduocbanle,
  // Khám chữa bệnh
  kcb,
  thongtintiepnhan,
  tiepNhan
});

export default rootReducer;
