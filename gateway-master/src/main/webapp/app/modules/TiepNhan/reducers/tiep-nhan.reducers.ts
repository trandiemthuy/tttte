import axios from 'axios';
import { ICrudPutAction } from 'react-jhipster';
import { cleanEntity } from 'app/shared/util/entity-utils';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';
import { defaultValue as defaultTiepNhan, ITiepNhanRequest } from 'app/shared/model/khamchuabenh/tiep-nhan-request.model';

export const ACTION_TYPES = {
  FETCH_BENHANKHAMBENH_LIST: 'benhAnKhamBenh/FETCH_BENHANKHAMBENH_LIST',
  FETCH_BENHANKHAMBENH: 'benhAnKhamBenh/FETCH_BENHANKHAMBENH',
  CREATE_BENHANKHAMBENH: 'benhAnKhamBenh/CREATE_BENHANKHAMBENH',
  UPDATE_BENHANKHAMBENH: 'benhAnKhamBenh/UPDATE_BENHANKHAMBENH',
  DELETE_BENHANKHAMBENH: 'benhAnKhamBenh/DELETE_BENHANKHAMBENH',
  RESET: 'benhAnKhamBenh/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  updating: false,
  updateSuccess: false,
  thongTinTiepNhan: defaultTiepNhan
};

export type TiepNhanState = Readonly<typeof initialState>;

// Reducer

export default (state: TiepNhanState = initialState, action): TiepNhanState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.CREATE_BENHANKHAMBENH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.CREATE_BENHANKHAMBENH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };

    case SUCCESS(ACTION_TYPES.CREATE_BENHANKHAMBENH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        thongTinTiepNhan: action.payload.data
      };

    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/khamchuabenh/api/tiep-nhan-ngoai-trus';

// Actions

export const createTiepNhan: ICrudPutAction<ITiepNhanRequest> = thongTinTiepNhan => async dispatch => {
  return await dispatch({
    type: ACTION_TYPES.CREATE_BENHANKHAMBENH,
    payload: axios.post(apiUrl, cleanEntity(thongTinTiepNhan))
  });
};
