import React, { useState, useEffect, useMemo, useCallback } from 'react';
// import { translate } from 'react-jhipster';
import DataGrid, {
  Column,
  Scrolling,
  Paging,
  Summary,
  TotalItem,
  ValueFormat,
  GroupItem as GroupItemGrid
} from 'devextreme-react/data-grid';
import { Button } from 'devextreme-react';
import Form, { SimpleItem, GroupItem, Label } from 'devextreme-react/form';
import axios from 'axios';
import notify from 'devextreme/ui/notify';
import { KHAMCHUABENH_SERVICE_URL } from 'app/config/constants';
// import { LoadPanel } from 'devextreme-react/load-panel';


const position = { of: '#formSoLuong' };
// eslint-disable-next-line @typescript-eslint/class-name-casing
export interface danhSachProps {
  isOpen?: boolean;
  trigger?: Function;
}

// export const TongSoBenhNhanTheoPhong = React.memo((props: danhSachProps) => {
export const TongSoBenhNhanTheoPhong = (props: danhSachProps) => {
  const [data, setData] = useState([]);
  const selectSource = [
    { id: 0, value: 'Tất cả' },
    { id: 1, value: 'Chờ khám, mới vào khoa' },
    { id: 2, value: 'Đang khám, đang điều trị' },
    { id: 3, value: ' Đã khám, xuất viện' },
    { id: 4, value: 'Chuyển khoa phòng khám' },
    { id: 5, value: 'Chuyển viện, chuyển tuyến' },
    { id: 6, value: 'Nhập viện' },
    { id: 7, value: 'Tử vong' },
    { id: 8, value: 'trốn viện' }
  ];
  const formData = {
    selectedValue: 0
  };
  const [dataSelect, setDataSelect] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      const param = dataSelect === 0 ? '' : '&trangThai.equals=' + dataSelect;
      await axios(
        KHAMCHUABENH_SERVICE_URL + '/api/tiep-nhans/count?thoiGianTiepNhan.equals=' + new Date().toISOString().split('T')[0] + param
      )
        .then(r => {
          setData(r.data);
        })
        .catch(e => {
          notify({ message: 'Lỗi tải danh sách số lượng bệnh nhân theo phòng', width: 300, shading: true }, 'error', 2000);
        });
    };
    fetchData();
  }, [dataSelect]);

  const handleTrigger = useCallback(() => {
    props.trigger();
  },[props.isOpen]);

  // const handleOnSubmit = useCallback((e) => {
  //   console.log(e)
    // setDataSelect(e.value);
  //   // {...thongTinBenhNhanNew, ...{[e.dataField]: e.value}}
  // },[]);

const handleOnSubmit = (e) => {
  setDataSelect(e.value);
};
const selectOption = useMemo(() =>{
   return {
     displayExpr: 'value',
     valueExpr: 'id',
     value: dataSelect,
     dataSource: selectSource
   }
  },[dataSelect])
  return (
    <div>
      {/* {props.isOpen === true ? (*/}
        <div>
          {/* <Button icon="back" onClick={handleTrigger} />*/}
            <Form formData={formData} onFieldDataChanged={handleOnSubmit}>
              <GroupItem>
                <SimpleItem
                  editorType={'dxSelectBox'}
                  dataField={'selectSource'}
                  editorOptions={selectOption}
                >
                  <Label text={'Trạng Thái'}/>
                </SimpleItem>
              </GroupItem>
            </Form>

          {/* <LoadPanel
          shadingColor="rgba(0,0,0,0.4)"
          // position={position}
          visible={false}
          showIndicator={true}
          shading={true}
          showPane={true}
          closeOnOutsideClick={false}
        /> */}
          <DataGrid
            dataSource={data}
            showBorders={true}
            width="100%"
            allowColumnResizing
            wordWrapEnabled
            columnResizingMode={'widget'}
            height={350}
          >
            <Scrolling mode="infinite" />
            <Paging enabled={false} />
            <Column dataField={'tenPhong'} dataType="string" caption="Phòng" alignment="center" width="41%" />
            <Column dataField={'soLuongBN'} dataType="string" caption="Dịch vụ" alignment="center" width="29%" />
            <Column dataField={'soLuongBNCoBhyt'} dataType="string" caption="Bảo hiểm" alignment="center" width="30%" />
            <Summary>
              <TotalItem column="soLuongBN" summaryType="sum">
                <ValueFormat type="decimal" precision={2} />
              </TotalItem>

              <GroupItemGrid column="soLuongBN" summaryType="sum">
                <ValueFormat type="decimal" precision={2} />
              </GroupItemGrid>

              <TotalItem column="soLuongBNCoBhyt" summaryType="sum">
                <ValueFormat type="decimal" precision={2} />
              </TotalItem>

              <GroupItemGrid column="soLuongBNCoBhyt" summaryType="sum">
                <ValueFormat type="decimal" precision={2} />
              </GroupItemGrid>

              <GroupItemGrid summaryType="count" />
            </Summary>
          </DataGrid>
        </div>
      {/* // ) : (*/}
      {/* //   <div>*/}
      {/* //     <Button height="auto" icon="chevronright" onClick={handleTrigger} />*/}
      {/* //   </div>*/}
      {/* // )}*/}
    </div>
  );
// });
};

export default TongSoBenhNhanTheoPhong;
