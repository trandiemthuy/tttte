import { KHAMCHUABENH_SERVICE_URL } from 'app/config/constants';
import axios from 'axios';
import notify from 'devextreme/ui/notify';
import { ITiepNhan } from 'app/shared/model/kcb/thong-tin-tiep-nhan.model';
const ACTION_TYPE = {
  GET_DSTIEPNHAN: 'TIEPNHAN/GET_DANHSACH',
  THEM_DSTIEPNHAN: 'TIEPNHAN/THEMVAO_DANHSACHTIEPNHAN',
  SUA_DSTIEPNHAN: 'TIEPNHAN/SUA_DSTIEPNHAN',
  XOA_DSTIEPNHAN: 'TIEPNHAN/XOA_DSTIEPNHAN',
  CAPNHAT_DSTIEPNHAN: 'TIEPNHAN/CAPNHANT_DSTIEPNHAN'
};

const initialState = {
  danhSach: []
};
export type ThongTinTiepNhanState = Readonly<typeof initialState>;
export default (state: ThongTinTiepNhanState = initialState, action): ThongTinTiepNhanState => {
  switch (action.type) {
    case ACTION_TYPE.GET_DSTIEPNHAN:
      return {
        ...state,
        danhSach: action.payload
      };
    case ACTION_TYPE.XOA_DSTIEPNHAN: {
      const dsTiepNhan = state.danhSach.filter((item: ITiepNhan) => item.bakbId !== action.payload.bakbId);
      return {
        ...state,
        danhSach: dsTiepNhan
      };
    }
    case ACTION_TYPE.THEM_DSTIEPNHAN: {
      const ds = [...state.danhSach];
      ds.push(action.payload);
      return {
        ...state,
        danhSach: ds
      };
    }

    case ACTION_TYPE.SUA_DSTIEPNHAN: {
      const ds = state.danhSach.filter((item: ITiepNhan) => item.bakbId !== action.payload.bakbId);
      ds.push(action.payload);
      return {
        ...state,
        danhSach: ds
      };
    }

    default:
      return state;
  }
};

const getDanhSachTiepNhanAPI = async paramter => {
  try {
    const url = KHAMCHUABENH_SERVICE_URL + '/api/tiep-nhans';
    const response = await axios.get<Array<ITiepNhan>>(url, {
      params: paramter
    });
    return response.data ? response.data : [];
  } catch (e) {
    console.warn('loi ', e);
    notify('Không thể load danh sách tiếp nhận !', 'error');
    return [];
  }
};

export const getDanhSachTiepNhan = paramter => async dispatch => {
  const payload = await getDanhSachTiepNhanAPI(paramter);
  const result = await dispatch({
    type: ACTION_TYPE.GET_DSTIEPNHAN,
    payload
  });
  return result;
};

const luuTiepNhanAPI = async paramter => {
  try {
    const url = KHAMCHUABENH_SERVICE_URL + '/api/tiep-nhans';
    const response = await axios.post<ITiepNhan>(url, paramter);
    return response.data ? response.data : null;
  } catch (e) {
    console.warn('loi ', e);
    notify('Không thể lưu thông tin tiếp nhận !', 'error');
    return null;
  }
};

export const luuTiepNhan = (tiepNhanData: ITiepNhan) => async dispatch => {
  const kqLuu = await luuTiepNhanAPI(tiepNhanData);
  if (kqLuu !== null) {
    const result = await dispatch({
      type: ACTION_TYPE.THEM_DSTIEPNHAN,
      payload: kqLuu
    });
  }
  return kqLuu;
};
const suaTiepNhanAPI = async paramter => {
  try {
    const url = KHAMCHUABENH_SERVICE_URL + '/api/tiep-nhans';
    const response = await axios.put<ITiepNhan>(url, paramter);
    return response.data ? response.data : null;
  } catch (e) {
    console.warn('loi ', e);
    notify('Không thể sửa thông tin tiếp nhận !', 'error');
    return null;
  }
};

export const suaTiepNhan = (tiepNhanData: ITiepNhan) => async dispatch => {
  const kqSua = await suaTiepNhanAPI(tiepNhanData);
  if (kqSua !== null) {
    const result = await dispatch({
      type: ACTION_TYPE.SUA_DSTIEPNHAN,
      payload: kqSua
    });
  }
  return kqSua;
};

const xoaTiepNhanAPI = async (tiepNhanData: ITiepNhan) => {
  try {
    const url = KHAMCHUABENH_SERVICE_URL + '/api/tiep-nhans/' + tiepNhanData.bakbId;
    const response = await axios.delete(url);
    console.warn('sss ', response.status);
    return true;
  } catch (e) {
    console.warn('loi ', e);
    notify('Lỗi xoá thông tin tiếp nhận!', 'error');

    return false;
  }
};

export const xoaTiepNhan = (tiepNhanData: ITiepNhan) => async dispatch => {
  const kqXoa = await xoaTiepNhanAPI(tiepNhanData);

  if (kqXoa) {
    const result = await dispatch({
      type: ACTION_TYPE.XOA_DSTIEPNHAN,
      payload: tiepNhanData
    });
  }
  return kqXoa;
};
