import React from 'react';
import {IRootState} from "app/shared/reducers";
import {connect} from 'react-redux';
import DanhSachTiepNhanForm from "app/modules/TiepNhan/NavigatonTabBar/TabDanhSachTiepNhan/DanhSachTiepNhanForm";


class DanhSachTiepNhan extends React.Component<any, any> {


  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (
      <DanhSachTiepNhanForm onRowDblClick={e => console.warn("click ", e)}/>
    );
  }

}

const mapStateToProps = (storeState: IRootState) => {
  return {}
};

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(
  mapStateToProps,
)(DanhSachTiepNhan);

