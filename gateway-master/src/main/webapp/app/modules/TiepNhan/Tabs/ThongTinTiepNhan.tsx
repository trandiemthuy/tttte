import { IRootState } from 'app/shared/reducers';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { ThongTinBaoHiem, ThongTinHanhChinh } from 'app/shared/components/BenhNhan';
import { getEntities as getCountries } from 'app/entities/khamchuabenh/country/country.reducer';
import { getEntities as getNgheNghiepList } from 'app/entities/khamchuabenh/nghe-nghiep/nghe-nghiep.reducer';
import { getEntities as getDanTocList } from 'app/entities/khamchuabenh/dan-toc/dan-toc.reducer';
import { getEntities as getTinhThanhPhoList } from 'app/entities/khamchuabenh/tinh-thanh-pho/tinh-thanh-pho.reducer';
import { getEntitiesByTinhThanhPhoId as getQuanHuyenByTinhThanhId } from 'app/entities/khamchuabenh/quan-huyen/quan-huyen.reducer';
import { getEntities as getDichVuKhamList } from 'app/entities/khamchuabenh/dich-vu-kham/dich-vu-kham.reducer';
import { createTiepNhan } from 'app/modules/TiepNhan/reducers/tiep-nhan.reducers';
import { getEntities as getPhongList } from 'app/entities/khamchuabenh/phong/phong.reducer';
import {
  getEntities as getDoiTuongBhytList,
  getEntity as getDoiTuongById
} from 'app/entities/khamchuabenh/doi-tuong-bhyt/doi-tuong-bhyt.reducer';
import TongSoBenhNhanTheoPhong from '../Components/TongSoBenhNhanTheoPhong/TongSoBenhNhanTheoPhong';
import { defaultValue as defaultBenhNhan, IBenhNhan } from 'app/shared/model/khamchuabenh/benh-nhan.model';
import { Item } from 'devextreme-react/box';
import { XuLyThongTinTiepNhan } from 'app/shared/components/BenhNhan/XuLyThongTinTiepNhan';
import ResponsiveBox, { Row, Col, Location } from 'devextreme-react/responsive-box';
import value from '*.json';
import { defaultValue as defaultTheBHYT, ITheBhyt } from 'app/shared/model/khamchuabenh/the-bhyt.model';

const HEIGHT = 400;

const BUTTON_STATE = {
  inputStateThongTinHanhChinh: {
    them: {
      benhNhanId: false,
      hoTen: false,
      gioiTinh: false,
      ngaySinh: false,
      chiCoNamSinh: false,
      quocTich: false,

      tuoi: false,
      ngay: false,
      thang: false,
      nam: false,
      danToc: false,
      ngheNghiep: false,
      tinhThanhPho: false,
      quanHuyen: false,
      phuongXa: false,

      hoChieu: false,
      soDienThoai: false,
      diaChi: false,
      apThon: false,
      timBNBtn: false,
      nguoiLienHeBtn: false
    },
    luu: {
      benhNhanId: false,
      hoTen: false,
      gioiTinh: false,
      ngaySinh: false,
      chiCoNamSinh: false,
      quocTich: false,

      tuoi: false,
      ngay: false,
      thang: false,
      nam: false,
      danToc: false,
      ngheNghiep: false,
      tinhThanhPho: false,
      quanHuyen: false,
      phuongXa: false,

      hoChieu: false,
      soDienThoai: false,
      diaChi: false,
      apThon: false,
      timBNBtn: false,
      nguoiLienHeBtn: false
    },

    sua: {
      hoTen: false,
      gioiTinh: false,
      ngaySinh: false,
      chiCoNamSinh: false,
      quocTich: false,

      tuoi: false,
      ngay: false,
      thang: false,
      nam: false,
      danToc: false,
      ngheNghiep: false,
      tinhThanhPho: false,
      quanHuyen: false,
      phuongXa: false,

      hoChieu: false,
      soDienThoai: false,
      diaChi: false,
      apThon: false,
      timBNBtn: false,
      nguoiLienHeBtn: false
    },

    init: {
      benhNhanId: true,
      hoTen: true,
      gioiTinh: true,
      ngaySinh: true,
      chiCoNamSinh: true,
      quocTich: true,

      tuoi: true,
      ngay: true,
      thang: true,
      nam: true,
      danToc: true,
      ngheNghiep: true,
      tinhThanhPho: true,
      quanHuyen: true,
      phuongXa: true,

      hoChieu: true,
      soDienThoai: true,
      diaChi: true,
      apThon: true,
      timBNBtn: true,
      nguoiLienHeBtn: true
    }
  },

  inputStateThongTinBaoHiem: {
    them: {
      soThe: false,
      doiTuongBHYT: false,
      chuoiNhanDang: false,
      tyLeMienGiam: false,
      tuNgay: false,
      denNgay: false,
      maDangKy: false,
      noiDangKy: false,
      maKhuVuc: false,
      giayToTE: false,
      tenGiayTE1: false,
      coMCCT: false,
      ngayConLaiBH5Nam: false,
      ngayMienCungChiTra: false
    },

    luu: {
      soThe: false,
      doiTuongBHYT: false,
      chuoiNhanDang: false,
      tyLeMienGiam: false,
      tuNgay: false,
      denNgay: false,
      noiDangKy: false,
      maKhuVuc: false,
      maDangKy: false,
      giayToTE: false,
      tenGiayTE1: false,
      coMCCT: false,
      ngayConLaiBH5Nam: false,
      ngayMienCungChiTra: false
    },

    sua: {
      hoTen: false,
      gioiTinh: false,
      ngaySinh: false,
      chiCoNamSinh: false,
      quocTich: false,

      tuoi: false,
      ngay: false,
      thang: false,
      nam: false,
      danToc: false,
      ngheNghiep: false,
      tinhThanhPho: false,
      quanHuyen: false,
      phuongXa: false,

      hoChieu: false,
      soDienThoai: false,
      diaChi: false,
      apThon: false,
      timBNBtn: false,
      nguoiLienHeBtn: false
    },
    
    init: {
      soThe: true,
      doiTuongBHYT: true,
      chuoiNhanDang: true,
      tyLeMienGiam: true,
      tuNgay: true,
      denNgay: true,
      noiDangKy: true,
      maKhuVuc: true,
      maDangKy: true,
      giayToTE: true,
      tenGiayTE1: true,
      coMCCT: true,
      ngayConLaiBH5Nam: true,
      ngayMienCungChiTra: true
    }
  },

  inputStateXuLyThongTinTiepNhan: {
    them: {
      capCuu : false,
      khamUuTien : false,
      dungTuyen : false,
      khamOnline : false,
      phongKham : false,
      dichVuKham : false,

      themBtn: true,
      luuBtn: false,
      suaBtn: true,
      xoaBtn: true,
      huyBtn: false
    },

    luu: {
      capCuu : false,
      khamUuTien : false,
      dungTuyen : false,
      khamOnline : false,
      phongKham : false,
      dichVuKham : false,

      themBtn: false,
      luuBtn: true,
      suaBtn: false,
      xoaBtn: false,
      huyBtn: false
    },

    sua: {
      capCuu : false,
      khamUuTien : false,
      dungTuyen : false,
      khamOnline : false,
      phongKham : false,
      dichVuKham : false,

      themBtn: true,
      luuBtn: false,
      suaBtn: true,
      xoaBtn: true,
      huyBtn: false
    },

    init: {
      capCuu : true,
      khamUuTien : true,
      dungTuyen : true,
      khamOnline : true,
      phongKham : true,
      dichVuKham : true,

      themBtn: false,
      luuBtn: true,
      suaBtn: true,
      xoaBtn: true,
      huyBtn: true
    }
  }
};
export interface IThongTinTiepNhanProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const ThongTinTiepNhan = (props: IThongTinTiepNhanProps) => {
  const [buttonState, setButtonState] = useState('init');
  const [thongTinBenhNhanNew, setThongTinBenhNhanNew] = useState<IBenhNhan>(defaultBenhNhan);
  const [thongTinBaoHiem, setThongTinBaoHiem] = useState<ITheBhyt>(defaultTheBHYT);
  useEffect(() => {
    props.getCountries();
    props.getDanTocList();
    props.getNgheNghiepList();
    props.getTinhThanhPhoList();
    props.getDichVuKhamList();
    props.getPhongList();
    props.getDoiTuongBhytList();
  }, []);
  useEffect(() => {
    props.getQuanHuyenByTinhThanhId(thongTinBenhNhanNew.tinhThanhPhoId);
  }, [thongTinBenhNhanNew.tinhThanhPhoId]);

  const onChangeThongTinBenhNhan = useCallback((e): any => {
    setThongTinBenhNhanNew({ ...thongTinBenhNhanNew, ...{ [e.dataField]: e.value } });
  }, []);
  const onChangeThongTinBaoHiem = useCallback((e): any => {
    setThongTinBaoHiem({ ...thongTinBaoHiem, ...{ [e.dataField]: e.value } });
  }, []);
  const { thongTinBenhNhan, countries, danTocList, ngheNghiepList, theBhyt, doiTuongBhytList, doiTuongBhyt } = props;
  const { tinhThanhPhoList, quanHuyenList } = props;

  useEffect(() => {
    props.getDoiTuongById(thongTinBaoHiem.doiTuongBhytId);
  }, [thongTinBaoHiem.doiTuongBhytId]);

  const getSizeQualifier = width => {
    const tiLe = width / window.screen.height;

    if (width < 640) {
      return 'xs';
    }

    if (tiLe < 1.4) {
      return 'sm';
    }

    if (tiLe < 1.8) {
      return 'md';
    }
    return 'lg';
  };

  const getBorderCSS = () => {
    const screenTyle = getSizeQualifier(window.screen.width);
    if (screenTyle === 'sm')
      return {
        borderStyle: 'dashed',
        borderWidth: 0,
        borderTopWidth: 1,
        paddingTop: 10,
        margin: 5,
        borderColor: '#327ab7'
      };
    return { margin: 5, borderWidth: 0 };
  };

  const changeButtonState = useCallback((state: string) => {
    return setButtonState(state);
  }, []);

  const createTiepNhanRequest = useCallback(thongTinTiepNhan => {
    props.createTiepNhan(thongTinTiepNhan);
  }, []);
  const MemoTongSoBenhNhanTheoPhong = React.memo(TongSoBenhNhanTheoPhong);
  return (
    <>
      <ResponsiveBox screenByWidth={getSizeQualifier}>
        <Row ratio={4} />
        <Row ratio={1} />
        <Row ratio={1} />
        <Col ratio={2} />
        <Col ratio={2} />
        <Col ratio={1} screen={'md'} />
        <Item>
          <Location row={0} col={0} />
          <Location screen="md" row={0} col={0} />
          <div style={{ margin: 5, height: HEIGHT }} className={'tran-card'}>
            <ThongTinHanhChinh
              values={thongTinBenhNhan}
              countries={countries}
              danTocList={danTocList}
              ngheNghiepList={ngheNghiepList}
              onChange={onChangeThongTinBenhNhan}
              tinhThanhPhoList={tinhThanhPhoList}
              quanHuyenList={quanHuyenList}
              inputState={BUTTON_STATE.inputStateThongTinHanhChinh[buttonState]}
            />
          </div>
        </Item>
        <Item>
          <Location row={0} col={1} />
          <Location screen="md" row={0} col={1} />
          <div style={{ margin: 5, height: HEIGHT }} className={'tran-card'}>
            <ThongTinBaoHiem
			      inputState={BUTTON_STATE.inputStateThongTinBaoHiem[buttonState]}
              values={theBhyt}
              doiTuongBHYTs={doiTuongBhytList}
              onChange={onChangeThongTinBaoHiem}
              doiTuongBHYT={doiTuongBhyt}
            />
          </div>
        </Item>
        <Item>
          <Location screen="sm" row={2} col={0} colspan={2} />
          <Location screen="md" row={0} col={2} />
          <div style={getBorderCSS()}>
            <TongSoBenhNhanTheoPhong />
          </div>
        </Item>
        <Item>
          <Location row={1} col={0} colspan={2} />
          <Location row={0} col={2} />
          <div style={{ margin: 5 }} className={'tran-card'}>
            <XuLyThongTinTiepNhan
              thongTinBenhNhan={thongTinBenhNhanNew}
              changeButtonState={changeButtonState}
              dichVuKhamList={props.dichVuKhamList}
              creatTiepNhanRequest={createTiepNhanRequest}
              thongTinTheBhyt={theBhyt}
              phongKham={props.phongList}
              inputState={BUTTON_STATE.inputStateXuLyThongTinTiepNhan[buttonState]}
            />
          </div>
        </Item>
      </ResponsiveBox>
    </>
  );
};

const mapStateToProps = ({
  benhNhan,
  country,
  danToc,
  ngheNghiep,
  theBhyt,
  tinhThanhPho,
  quanHuyen,
  dichVuKham,
  phong,
  doiTuongBhyt
}: IRootState) => ({
  thongTinBenhNhan: benhNhan.entity,
  countries: country.entities,
  danTocList: danToc.entities,
  ngheNghiepList: ngheNghiep.entities,
  theBhyt: theBhyt.entity,
  tinhThanhPhoList: tinhThanhPho.entities,
  quanHuyenList: quanHuyen.entities,
  dichVuKhamList: dichVuKham.entities,
  phongList: phong.entities,
  doiTuongBhytList: doiTuongBhyt.entities,
  doiTuongBhyt: doiTuongBhyt.entity
});

const mapDispatchToProps = {
  getCountries,
  getNgheNghiepList,
  getDanTocList,
  getTinhThanhPhoList,
  getQuanHuyenByTinhThanhId,
  getDichVuKhamList,
  createTiepNhan,
  getPhongList,
  getDoiTuongBhytList,
  getDoiTuongById
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ThongTinTiepNhan);
