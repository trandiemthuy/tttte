// import Form, { ButtonItem, GroupItem, Label, SimpleItem } from 'devextreme-react/form';
// import React from 'react';
// import { PatternRule } from 'devextreme-react/data-grid';
// import { XuLyFormTiepNhan } from '../XuLyFormTiepNhan';
//
// /* index.propTypes = {
//
// }; */
// // export interface IThongTinBaoHiemProp extends StateProps, DispatchProps {}
// interface IThongTinBaoHiemProp {
//   initialValues: any;
//   onChange: any;
// }
// const ThongTinBaoHiem = (props: IThongTinBaoHiemProp) => {
//   const { initialValues, onChange } = props;
//   return (
//     <>
//       <Form id="form-thong-tin-bao-hiem" formData={initialValues} onFieldDataChanged={onChange} showColonAfterLabel>
//         <GroupItem cssClass="section-group" caption="Thông tin bảo hiểm y tế">
//           <SimpleItem editorType="dxTextBox" dataField="soBHYT" editorOptions={{ value: initialValues.soBHYT }}>
//             <Label text="Số BHYT" />
//           </SimpleItem>
//           <SimpleItem
//             editorType="dxSelectBox"
//             dataField="dtThe"
//             editorOptions={{
//               placeholder: 'Chọn đối tượng thẻ',
//               displayExpr: 'dataField',
//               valueExpr: 'id',
//               dataSource: ''
//             }}
//           >
//             <Label text="ĐT thẻ" />
//           </SimpleItem>
//
//           <GroupItem colCount={2}>
//             <SimpleItem editorType="dxTextBox" dataField="chuoiNhanDang" editorOptions={{ placeholder: 'Chuỗi nhận dạng' }}>
//               <Label text="Số Chuỗi nhận dạng" />
//             </SimpleItem>
//
//             <SimpleItem editorType="dxTextBox" dataField="tyLemienGiam" editorOptions={{ placeholder: 'Từ 1 -> 100' }}>
//               <PatternRule message="Không sử dụng chữ" pattern={/^[0-9]+$/} />
//
//               <Label text="Tỷ lệ miễn giảm (%)" />
//             </SimpleItem>
//           </GroupItem>
//
//           <GroupItem colCount={5}>
//             <SimpleItem
//               editorType="dxDateBox"
//               colSpan={3}
//               dataField="tuNgay"
//               editorOptions={{
//                 max: initialValues.denNgay ? initialValues.denNgay : initialValues.ngayTN
//               }}
//             >
//               <Label text="Từ ngày" />
//             </SimpleItem>
//             <SimpleItem
//               editorType="dxDateBox"
//               colSpan={2}
//               dataField="denNgay"
//               editorOptions={{
//                 max: initialValues.ngayTN
//               }}
//             >
//               <Label text="Đến" />
//             </SimpleItem>
//           </GroupItem>
//
//           <GroupItem colCount={7}>
//             <SimpleItem
//               editorType="dxTextBox"
//               dataField="maDK"
//               colSpan={3}
//               editorOptions={{ value: initialValues.maDK, placeholder: 'Mã đăng ký' }}
//             >
//               <Label text="Nơi đăng ký" />
//             </SimpleItem>
//
//             <SimpleItem editorType="dxTextBox" dataField="noiDK" colSpan={4} editorOptions={{ value: initialValues.noiDK }}>
//               <Label visible={false} />
//             </SimpleItem>
//           </GroupItem>
//
//           <SimpleItem editorType="dxTextBox" dataField="maKV" editorOptions={{ value: initialValues.maKV }}>
//             <Label text="Mã khu vực" />
//           </SimpleItem>
//           <GroupItem colCount={5}>
//             <SimpleItem dataField="giayTE1" colSpan={3} editorType="dxSelectBox">
//               <Label text="Giấy tờ TE1" />
//             </SimpleItem>
//             <SimpleItem
//               editorType="dxTextBox"
//               dataField="tenGiayTE1"
//               colSpan={2}
//               editorOptions={{ value: initialValues.tenGiayTE1, placeholder: 'Tên giấy TE1' }}
//               label={{ visible: false }}
//             ></SimpleItem>
//           </GroupItem>
//           <GroupItem colCount={12}>
//             <SimpleItem
//               editorType="dxCheckBox"
//               dataField="coMCCT"
//               colSpan={3}
//               cssClass="checkBoxStyle"
//               editorOptions={{ text: 'Có MCCT. Ngày MCCT', defaultValue: false, value: initialValues.coMCCT }}
//               label={{ visible: false }}
//             ></SimpleItem>
//
//             <SimpleItem
//               editorType="dxDateBox"
//               colSpan={5}
//               dataField="ngayMCCT"
//               editorOptions={{
//                 value: initialValues.ngayMCCT,
//                 placeholder: 'Ngày Miễn cùng chi trả',
//                 disabled: initialValues.coMCCT ? false : true
//               }}
//               label={{ visible: false }}
//             ></SimpleItem>
//             <SimpleItem
//               editorType="dxTextBox"
//               dataField="ngayConLaiBH5Nam"
//               colSpan={4}
//               editorOptions={{
//                 value: initialValues.ngayConLaiBH5Nam,
//                 placeholder: 'Số ngày còn lại của bảo hiểm 5 năm'
//               }}
//               label={{ visible: false }}
//             ></SimpleItem>
//           </GroupItem>
//
//           {/*   <GroupItem colCount={7}>
//           <SimpleItem
//             editorType="dxTextBox"
//             dataField="maNoiChuyenDen"
//             colSpan={3}
//             editorOptions={{ value: initialValues.maNoiChuyenDen, placeholder: 'Mã khu vực chuyển đến' }}
//           >
//             <Label text="Mã khu vực" />
//           </SimpleItem>
//
//           <SimpleItem
//             editorType="dxTextBox"
//             dataField="noiChuyenDen"
//             colSpan={4}
//             editorOptions={{ value: initialValues.noiChuyenDen, placeholder: 'Khu vực chuyển đến' }}
//           >
//             <Label visible={false} />
//           </SimpleItem>
//         </GroupItem>
//
//         <GroupItem colCount={7}>
//           <SimpleItem
//             editorType="dxTextBox"
//             dataField="maNoiChuyen"
//             colSpan={3}
//             editorOptions={{
//               value: initialValues.maNoiChuyenDen,
//               placeholder: 'Mã khu vực chuẩn đoán chuyển đến'
//             }}
//           >
//             <Label text="Chấn đoán nơi chuyển" />
//           </SimpleItem>
//
//           <SimpleItem
//             editorType="dxTextBox"
//             dataField="noiChuyen"
//             colSpan={4}
//             editorOptions={{ value: initialValues.noiChuyen, placeholder: 'Khu vực chuẩn đoán chuyển đến' }}
//           >
//             <Label visible={false} />
//           </SimpleItem>
//         </GroupItem> */}
//           <GroupItem colCount={3} cssClass="action-bhyt">
//             <ButtonItem
//               buttonOptions={{
//                 text: 'Kiểm tra thẻ',
//                 width: '125px',
//                 type: 'default',
//                 stylingMode: 'contained'
//               }}
//             />
//             <ButtonItem
//               buttonOptions={{
//                 text: 'Thẻ Trẻ Em',
//                 width: '125px',
//                 type: 'default',
//                 stylingMode: 'contained'
//               }}
//             />
//             <ButtonItem
//               buttonOptions={{
//                 text: 'Xóa Thông Tin',
//                 width: '125px',
//                 type: 'normal',
//                 stylingMode: 'contained'
//               }}
//             />
//           </GroupItem>
//         </GroupItem>
//         <GroupItem>
//           <XuLyFormTiepNhan onChange={onChange} />
//         </GroupItem>
//         {/*   <GroupItem cssClass="section-group" caption="Loại">
//         <GroupItem colCount={4}>
//           <SimpleItem
//             editorType="dxCheckBox"
//             dataField="capCuu"
//             cssClass="checkBoxStyle"
//             editorOptions={{ text: 'Cấp cứu', defaultValue: false, value: initialValues.capCuu }}
//             label={{ visible: false }}
//           ></SimpleItem>
//
//           <SimpleItem
//             editorType="dxCheckBox"
//             dataField="khamUuTien"
//             cssClass="checkBoxStyle"
//             editorOptions={{ text: 'Khám ưu tiên', defaultValue: false, value: initialValues.khamUuTien }}
//             label={{ visible: false }}
//           ></SimpleItem>
//
//           <SimpleItem
//             editorType="dxCheckBox"
//             dataField="dungTuyen"
//             cssClass="checkBoxStyle"
//             editorOptions={{ text: 'Đúng tuyến', defaultValue: false, value: initialValues.dungTuyen }}
//             label={{ visible: false }}
//           ></SimpleItem>
//
//           <SimpleItem
//             editorType="dxCheckBox"
//             dataField="khamOnline"
//             cssClass="checkBoxStyle"
//             editorOptions={{ text: 'Khám Online', defaultValue: false, value: initialValues.khamOnline }}
//             label={{ visible: false }}
//           ></SimpleItem>
//         </GroupItem>
//
//         <GroupItem colCount={10}>
//           <SimpleItem
//             editorType="dxSelectBox"
//             dataField="phongKham"
//             colSpan={6}
//             editorOptions={{
//               placeholder: 'Chọn phòng khám',
//               displayExpr: 'dataField',
//               valueExpr: 'id',
//               dataSource: '',
//               value: initialValues.phongKham
//             }}
//           >
//             <Label text="Phòng khám" />
//           </SimpleItem>
//
//           <SimpleItem
//             editorType="dxSelectBox"
//             dataField="dichVu"
//             colSpan={4}
//             editorOptions={{
//               placeholder: 'Chọn phòng khám',
//               displayExpr: 'dataField',
//               valueExpr: 'id',
//               dataSource: '',
//               value: initialValues.dichVu
//             }}
//           >
//             <Label text="Dịch vụ" />
//           </SimpleItem>
//         </GroupItem>
//       </GroupItem> */}
//       </Form>
//     </>
//   );
// };
// export default React.memo(ThongTinBaoHiem);
// const mapStateToProps = storeState => ({});
// const mapDispatchToProps = {};
// type StateProps = ReturnType<typeof mapStateToProps>;
// type DispatchProps = typeof mapDispatchToProps;
