import Form, { ButtonItem, GroupItem, SimpleItem } from 'devextreme-react/form';
import 'devextreme-react/text-area';
import React, { useState, useRef, useEffect, useMemo } from 'react';
import { connect } from 'react-redux';
import './thong-tin-form.scss';
import ThongTinHanhChinh from './ThongTinHanhChinh/ThongTinHanhChinhForm';
// import ThongTinBaoHiem from './ThongTinBaoHiem/ThongTinBaoHiemForm';
import moment from 'moment';
import { formatDate } from 'devextreme/localization';

export const ThongTinTiepNhanForm = props => {
  const initialValues = {
    ngayTN: new Date(),
    stt: '',
    maYT: '',
    hoTen: '',
    ngaySinh: null,
    namSinhCheckBox: false,
    tuoi: null,
    thang: null,
    ngay: null,
    gioiTinh: null,
    quocGia: null,
    ngoaiKieu: false,
    cmnd: '',
    ngheNghiep: null,
    soDT: null,
    danToc: null,
    tinhThanh: null,
    PhuongXa: null,
    quanHuyen: null,
    soNha: '',
    diaChi: '',
    soBHYT: '',
    dtThe: null,
    chuoiNhanDang: '',
    tyLemienGiam: null,
    tuNgay: null,
    denNgay: null,
    maDK: null,
    noiDK: '',
    maKV: '',
    giayTE1: '',
    tenGiayTE1: '',
    coMCCT: false,
    ngayMCCT: null,
    ngayConLaiBH5Nam: null,
    maNoiChuyenDen: '',
    noiChuyenDen: '',
    maNoiChuyen: '',
    noiChuyen: '',
    capCuu: false,
    khamUuTien: false,
    dungTuyen: false,
    khamOnline: false,
    phongKham: null,
    dichVu: null
  };
  const [value, setValues] = useState(initialValues);
  const [ngaySinhValue, setNgaySinhValue] = useState();

  const toHoTenCase = (str: string) => {
    return str
      .toLowerCase()
      .split(' ')
      .map(word => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  };
  const tinhTuoi = (date: any, checkedNamSinh = false) => {
    if (!date) return;
    let years = null;
    let months = null;
    let days = null;
    if (checkedNamSinh === true) {
      years = moment().diff(date, 'years');
    } else {
      if (moment().diff(date, 'years') <= 6) {
        months = moment().diff(date, 'months');
        days = moment().diff(date, 'days');
      } else {
        years = moment().diff(date, 'years');
      }
    }
    return { years, months, days };
  };
  const handleSubmit = e => {
    e.preventDefault();
  };

  const handleOnChange = e => {
    const formInstance = e.component;
    const newValue = e.value;
    const fields = e.dataField;
    //  const valueNamSinhCheckBox = formInstance.getEditor('namSinhCheckBox').option('value');
    // console.log(formInstance.getEditor('ngaySinh').option('value'));
    switch (fields) {
      case 'namSinhCheckBox': {
        formInstance.updateData('namSinhCheckBox', newValue);
        if (initialValues.namSinhCheckBox === true) {
          formInstance.getEditor('ngaySinh').option({ displayFormat: 'yyyy' });
          formInstance.updateData({ tuoi: tinhTuoi(initialValues.ngaySinh, newValue).years, thang: null, ngay: null });
        } else {
          formInstance.getEditor('ngaySinh').option({ displayFormat: 'dd/MM/yyyy' });
          formInstance.updateData({
            tuoi: tinhTuoi(initialValues.ngaySinh, newValue).years,
            thang: tinhTuoi(initialValues.ngaySinh, newValue).months,
            ngay: tinhTuoi(initialValues.ngaySinh, newValue).days
          });
        }
        break;
      }
      case 'ngaySinh': {
        //   const formatDateValue = formatDate(newValue, valueNamSinhCheckBox === true ? 'yyyy' : 'yyyy-MM-dd');
        formInstance.updateData('ngaySinh', newValue);
        formInstance.updateData({
          tuoi: tinhTuoi(newValue, initialValues.namSinhCheckBox).years,
          thang: tinhTuoi(newValue, initialValues.namSinhCheckBox).months,
          ngay: tinhTuoi(newValue, initialValues.namSinhCheckBox).days
        });
        //  setValues({ ...value, ngaySinh: formatDateValue });
        break;
      }
      case 'tuNgay': {
        formInstance.updateData('tuNgay', newValue);
        formInstance.getEditor('denNgay').option({ min: newValue ? newValue : '1900/1/1', max: initialValues.ngayTN });
        // setValues({ ...value, tuNgay: newValue });
        break;
      }
      case 'denNgay': {
        formInstance.updateData('denNgay', newValue);
        formInstance.getEditor('tuNgay').option({ min: '1900/1/1', max: newValue ? newValue : initialValues.ngayTN });
        // setValues({ ...value, denNgay: newValue });
        break;
      }
      case 'hoTen': {
        formInstance.updateData('hoTen', toHoTenCase(newValue));
        break;
      }
      case 'tyLemienGiam': {
        if (newValue > 100) {
          formInstance.updateData('tyLemienGiam', parseInt('100', 10));
        } else {
          formInstance.updateData('tyLemienGiam', parseInt(newValue, 10));
        }
        break;
      }
      default: {
        break;
      }
    }
  };
  return (
    <form action="" onSubmit={handleSubmit}>
      <div className="form-container">
        <React.Fragment>
          <Form id="form-thong-tin-tiep-nhan" formData={initialValues} onFieldDataChanged={handleOnChange} showColonAfterLabel>
            <GroupItem colCount={2}>
              <GroupItem>
                <ThongTinHanhChinh initialValues={value} onChange={handleOnChange} />
              </GroupItem>
              <GroupItem>
                {/* <ThongTinBaoHiem initialValues={value} onChange={handleOnChange} />*/}
              </GroupItem>
            </GroupItem>
            {/*             <GroupItem>
              <XuLyFormTiepNhan />
            </GroupItem> */}
          </Form>
        </React.Fragment>
      </div>
    </form>
  );
};

const mapStateToProps = storeState => ({});
const mapDispatchToProps = {};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default React.memo(connect(mapStateToProps, mapDispatchToProps)(ThongTinTiepNhanForm));
