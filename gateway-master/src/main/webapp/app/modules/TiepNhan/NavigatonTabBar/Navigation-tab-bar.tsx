
import { Item, TabPanel } from "devextreme-react/tab-panel";
import 'devextreme-react/text-area';
import React from 'react';
import { ThongTinTiepNhanForm } from './TabTiepNhan/ThongTinTiepNhanForm';
import DanhSachTiepNhan from "app/modules/TiepNhan/Tabs/DanhSachTiepNhan";
const NavBar = () => {
    return (
      <TabPanel>
      <Item title={"Thông tin tiếp nhận"} component={ThongTinTiepNhanForm} />
      <Item title={"Danh sách tiếp nhận"} component={DanhSachTiepNhan}/>
      </TabPanel>
    )
};
export default React.memo(NavBar);


