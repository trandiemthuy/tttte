import { GIOI_TINH_OPTIONS } from 'app/modules/TiepNhan/DataSelectOption';
import { PatternRule, StringLengthRule } from 'devextreme-react/data-grid';
import Form, { ButtonItem, GroupItem, Label, RequiredRule, SimpleItem } from 'devextreme-react/form';
import React, { useState, useRef } from 'react';

interface IThongTinHanhChinhProp {
  initialValues: any;
  onChange: any;
}

export const ThongTinHanhChinh = (props: IThongTinHanhChinhProp) => {
  const { initialValues, onChange } = props;
  const [popupVisible, setPopupVisible] = useState(false);
  const handleshowPopup = () => {
    setPopupVisible(true);
  };

  return (
    <Form id="form-thong-tin-tiep-nhan-hanh-chinh" formData={initialValues} onFieldDataChanged={onChange} showColonAfterLabel>
      {/* hông cần caption đâu chừa không gian giao diện */}
      <GroupItem cssClass="section-group" caption="Thông tin hành chính">
        <GroupItem colCount={3}>
          <SimpleItem colSpan={2} dataField="ngayTN" editorType="dxDateBox" editorOptions={{ displayFormat: 'dd/MM/yyyy' }}>
            <Label text="Ngày TN" />
          </SimpleItem>
          <SimpleItem dataField="stt" editorType="dxTextBox" editorOptions={{ placeholder: 'STT' }}>
            <Label visible={false} />
          </SimpleItem>
        </GroupItem>

        <GroupItem colCount={3}>
          <SimpleItem colSpan={2} dataField="maYT" editorType="dxTextBox">
            <Label text="Mã y tế" />
          </SimpleItem>

          <ButtonItem
            colSpan={1}
            buttonOptions={{
              text: 'Tìm bệnh nhân',
              type: 'normal',
              stylingMode: 'contained',
              name: 'timBenhNhan',
              onClick: handleshowPopup
            }}
          />
        </GroupItem>

        <SimpleItem editorType="dxTextBox" dataField="hoTen">
          <Label text="Họ tên" />
          <RequiredRule message="Tên là bắt buộc" />
          <PatternRule pattern={/^[^0-9!@#$%^&*)(+=._-]+$/} message="Tên không chứa ký tự đặc biệt hoặc số" />
        </SimpleItem>
        <GroupItem colCount={2} cssClass="div-input-with-checkBox ">
          <SimpleItem
            dataField="ngaySinh"
            editorType="dxDateBox"
            editorOptions={{
              max: new Date().toISOString(),
              min: '1800/1/1',
              displayFormat: onChange.namSinhCheckBox === true ? 'yyyy' : 'dd/MM/yyyy'
            }}
          >
            <Label text="Ngày sinh" />
            <RequiredRule message="Ngày sinh trống" />
          </SimpleItem>
          <SimpleItem
            editorType="dxCheckBox"
            dataField="namSinhCheckBox"
            cssClass="checkBoxStyle"
            editorOptions={{ text: 'Chỉ có năm sinh', defaultValue: false, value: initialValues.namSinhCheckBox }}
          >
            <Label visible={false} />
          </SimpleItem>
        </GroupItem>

        <GroupItem colCount={7}>
          <SimpleItem colSpan={3} editorType="dxTextBox" dataField="tuoi" editorOptions={{ readOnly: true }}>
            <Label text="Tuổi" />
          </SimpleItem>

          <SimpleItem colSpan={2} editorType="dxTextBox" dataField="thang" editorOptions={{ readOnly: true }}>
            <Label text="Tháng" />
          </SimpleItem>

          <SimpleItem editorType="dxTextBox" colSpan={2} dataField="ngay" editorOptions={{ readOnly: true }}>
            <Label text="Ngày" />
          </SimpleItem>
        </GroupItem>
        <SimpleItem
          editorType="dxSelectBox"
          dataField="gioiTinh"
          editorOptions={{
            placeholder: 'Chọn giới tính',
            displayExpr: 'name',
            valueExpr: 'id',
            dataSource: GIOI_TINH_OPTIONS
          }}
        >
          <Label text="Giới tính" />
        </SimpleItem>
        <GroupItem colCount={2} cssClass="div-input-with-checkBox ">
          <SimpleItem dataField="quocGia" editorOptions={{ searchEnabled: true }}>
            <Label text="Quốc gia" />
          </SimpleItem>

          <SimpleItem
            editorType="dxCheckBox"
            dataField="ngoaiKieu"
            cssClass="checkBoxStyle"
            editorOptions={{ text: 'Ngoại kiều', defaultValue: false }}
            label={{ visible: false }}
          ></SimpleItem>
        </GroupItem>
        <SimpleItem editorType="dxTextBox" dataField="cmnd">
          <Label text="CMT/Hộ chiếu" />
          <PatternRule message="Không sử dụng chữ" pattern={/^[0-9]+$/} />
        </SimpleItem>

        <GroupItem colCount={2}>
          <SimpleItem editorType="dxSelectBox" editorOptions={{ dataSource: '' }} dataField="danToc">
            <Label text="Dân tộc" />
          </SimpleItem>

          <SimpleItem editorType="dxSelectBox" dataField="ngheNghiep" editorOptions={{ searchEnabled: true }}>
            <Label text="Nghề Nghiệp" />
          </SimpleItem>
        </GroupItem>

        <GroupItem colCount={3}>
          <SimpleItem editorType="dxTextBox" colSpan={2} dataField="soDT">
            <Label text="Số ĐT" />
            <PatternRule message="Không sử dụng chữ" pattern={/^[0-9]+$/} />
            <StringLengthRule message="Tối thiểu 10 số" min={10} />
          </SimpleItem>

          <ButtonItem
            buttonOptions={{
              text: 'Người liên hệ',
              type: 'normal',
              stylingMode: 'contained'
            }}
            colSpan={1}
          />
        </GroupItem>

        <GroupItem colCount={2}>
          <SimpleItem editorType="dxSelectBox" dataField="tinhThanh" editorOptions={{ searchEnabled: true }}>
            <Label text="Tỉnh/thành" />
          </SimpleItem>

          <SimpleItem editorType="dxSelectBox" dataField="quanHuyen" editorOptions={{ searchEnabled: true }}>
            <Label text="Quận/huyện" />
          </SimpleItem>
        </GroupItem>

        <GroupItem colCount={2}>
          <SimpleItem editorType="dxSelectBox" dataField="phuongXa" editorOptions={{ searchEnabled: true }}>
            <Label text="Phường/xã" />
          </SimpleItem>

          <SimpleItem editorType="dxTextBox" dataField="soNha">
            <Label text="Số nhà/ấp" />
          </SimpleItem>
        </GroupItem>

        <SimpleItem editorType="dxTextBox" dataField="diaChi">
          <RequiredRule message="Địa chỉ là bắt buộc" />
          <Label text="Địa chỉ" />
        </SimpleItem>
      </GroupItem>
    </Form>
  );
};
export default React.memo(ThongTinHanhChinh);
const mapStateToProps = storeState => ({});
const mapDispatchToProps = {};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
