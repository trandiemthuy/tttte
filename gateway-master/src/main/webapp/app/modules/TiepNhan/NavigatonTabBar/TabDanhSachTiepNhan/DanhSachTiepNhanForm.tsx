import React from 'react';
import {Button, CheckBox, DateBox, SelectBox} from "devextreme-react";
import DataGrid, {Column, FilterRow, LoadPanel, Pager, Paging} from "devextreme-react/data-grid";
import {IKhoa} from "app/shared/model/khamchuabenh/khoa.model";
import notify from 'devextreme/ui/notify';
import axios from 'axios';
import {KHAMCHUABENH_SERVICE_URL} from "app/config/constants";
import {IRootState} from "app/shared/reducers";
import {connect} from 'react-redux';
import {INhanVien} from "app/shared/model/kcb/nhan-vien.model";
import {IDanhSachTiepNhan} from "app/shared/model/kcb/danh-sach-tiep-nhan.model";
import {translate} from "react-jhipster";
import {getDanhSachTiepNhan} from "app/modules/TiepNhan/TiepNhan.reducer";

const outHeight = 300;


export interface IDanhSachTiepNhanState extends StateProps  {
  dsKhoa: IKhoa[],
  dsNhanVien: INhanVien[],
  dsTiepNhan: IDanhSachTiepNhan[],
  nhanVienId: number,
  trangThai: number,
  tuNgay: Date,
  denNgay: Date,
  coBHYT: boolean
}

export interface IDanhSachTiepNhanProps extends DispatchProps,StateProps {
  donVi: string,
  onRowDblClick: Function

}

const trangThaiOptions = [
  {
    id: -1,
    ten: 'Tất cả'
  },
  {
    id: 1,
    ten: "Chờ Khám",
  },
  {
    id: 2,
    ten: "Đang khám",
  },
  {
    id: 3,
    ten: "Đang điều trị",
  },
  {
    id: 4,
    ten: "Đã Khám",
  },
  {
    id: 5,
    ten: "Xuất Viện",
  },
  {
    id: 6,
    ten: "Chuyển Khoa Phòng",
  },
  {
    id: 7,
    ten: "Chuyển viện/ Tuyến",
  },
  {
    id: 8,
    ten: "Nhập viện",
  }
  ,
  {
    id: 9,
    ten: "Tử Vong"
  }
]

const tatCaNhanVien: INhanVien = {
  id: -1,
  ten: "Tất cả"
}

const defaultDsNhanVien = [
  tatCaNhanVien
]


class DanhSachTiepNhanForm extends React.Component<IDanhSachTiepNhanProps, IDanhSachTiepNhanState> {
  height: number;

  constructor(props) {
    super(props);
    this.height = window.screen.height - outHeight;
    this.state = {
      dsKhoa: [
        {
          ten: "Tất cả",
          id: -1
        }
      ],
      dsNhanVien: defaultDsNhanVien,
      dsTiepNhan: [],
      nhanVienId: -1,
      trangThai: -1,
      tuNgay: new Date(),
      denNgay: new Date(),
      coBHYT: null

    }

  }


  getDanhSachKhoa = async () => {
    try {
      const url = KHAMCHUABENH_SERVICE_URL + "/api/khoas?donViId.equals=1";
      const response = await axios.get<Array<IKhoa>>(url);
      return response.data ? response.data : [];
    } catch (e) {
      console.warn("loi KHoa ", e);
      notify('Không thể load danh sách khoa !', 'error');
      return [];
    }
  }


  getDanhSachNhanVienTheoKhoa = async (maKhoa) => {
    try {
      const url = KHAMCHUABENH_SERVICE_URL + "/api/nhan-viens/khoa/" + maKhoa;
      const response = await axios.get<Array<INhanVien>>(url);
      return response.data ? response.data : [];
    } catch (e) {
      console.warn("loi ", e);
      notify('Không thể load danh sách nhân !', 'error');
      return [];
    }
  }

  getDanhSachTiepNhan = async (paramter) => {


    try {
      const url = KHAMCHUABENH_SERVICE_URL + "/api/tiep-nhans"
      const response = await axios.get<Array<IDanhSachTiepNhan>>(url, {
        params: paramter
      });
      return response.data ? response.data : [];
    } catch (e) {
      console.warn("loi ", e);
      notify('Không thể load danh sách tiếp nhận !', 'error');
      return [];
    }

  }


  async getData() {
    let dsKhoa: IKhoa[] = await this.getDanhSachKhoa();
    console.warn("ds khoa ", dsKhoa)
    dsKhoa = this.state.dsKhoa.concat(dsKhoa)
    this.setState({
      dsKhoa
    })


    this.props.getDanhSachTiepNhan({
      "thoiGianTiepNhan.equals": new Date().toISOString().split('T')[0],
      "trangThai.lessThan": 3
    })


  }


  componentDidMount(): void {

    this.getData()

  }


  onKhoaValueChange = async (e) => {

    if (e.value !== -1) {

      let dsNhanVien: INhanVien[] = await this.getDanhSachNhanVienTheoKhoa(e.value);
      dsNhanVien = defaultDsNhanVien.concat(dsNhanVien)
      this.setState(
        {
          dsNhanVien,
          nhanVienId: e.value
        }
      )

    } else {
      this.setState({
        nhanVienId: null
      })
    }


  }

  onNhanVienChange = (e) => {

    this.setState({
      nhanVienId: e.value
    })
  }

  renderFormFiller() {
    return (
      <div>

        <div style={{display: "flex", alignItems: "center"}}>

          <div style={{paddingRight: 5, paddingLeft: 5, alignItems: "center", display: "flex"}}>
            <span style={{width: 70}}>Khoa</span>
            <div style={{}}>
              <SelectBox
                displayExpr={'ten'}
                searchEnabled={true}
                width={220}
                defaultValue={-1}
                items={this.state.dsKhoa}
                valueExpr="id"
                onValueChanged={this.onKhoaValueChange}

              />
            </div>
          </div>


          <div style={{paddingRight: 5, paddingLeft: 5, alignItems: "center", display: "flex"}}>
            <span style={{width: 70}}>Nhân viên</span>
            <div style={{}}>
              <SelectBox
                searchEnabled={true}
                width={220}
                items={this.state.dsNhanVien}
                displayExpr={'ten'}
                valueExpr={'id'}
                defaultValue={-1}
                onValueChanged={this.onNhanVienChange}
              />
            </div>
          </div>

          <div style={{paddingRight: 5, paddingLeft: 5, alignItems: "center", display: "flex"}}>
            <span style={{width: 70}}>Có BHYT</span>
            <div style={{width: 160}}>
              <CheckBox onValueChanged={e => this.setState({coBHYT: e.value})}/>
            </div>
          </div>

          <div style={{}}>
            <Button

              width={120}
              text="Xuất excel"
              onClick={() => {
              }}
            />
          </div>

        </div>


        <div style={{display: "flex", marginTop: 5}}>
          <div style={{paddingRight: 5, paddingLeft: 5, alignItems: "center", display: "flex"}}>
            <span style={{width: 70}}>Từ ngày</span>
            <div style={{}}>
              <DateBox
                width={220}
                displayFormat="dd-MM-yyyy"

                type="date"
                value={this.state.tuNgay}
                onValueChanged={e => {
                  this.setState({
                    tuNgay: e.value
                  })
                }}
              />
            </div>
          </div>

          <div style={{paddingRight: 5, paddingLeft: 5, alignItems: "center", display: "flex"}}>
            <span style={{width: 70}}>Đến ngày</span>
            <div style={{}}>
              <DateBox
                width={220}
                displayFormat="dd-MM-yyyy"

                type="date"

                value={this.state.denNgay}
                onValueChanged={e => {
                  this.setState({
                    denNgay: e.value
                  })
                }}
              />
            </div>
          </div>


          <div style={{paddingRight: 5, paddingLeft: 5, alignItems: "center", display: "flex"}}>
            <span style={{width: 70}}>Trạng thái</span>
            <div style={{}}>
              <SelectBox
                searchEnabled={true}
                width={160}
                items={trangThaiOptions}
                value={this.state.trangThai}
                displayExpr={'ten'}
                valueExpr={'id'}

                onValueChanged={e => {
                  this.setState({
                    trangThai: e.value
                  })
                }}

              />
            </div>
          </div>

          <div>
            <Button
              type={"default"}

              width={120}
              text="Làm mới"
              onClick={e => this.onLamMoiButtonClick()}
            />
          </div>


        </div>

      </div>

    )
  }


  getParamter() {
    const paramter = {
      "thoiGianTiepNhan.greaterThanOrEqual": this.state.tuNgay.toISOString().split('T')[0],
      "rhoiGianTiepNhan.lessThanOrEqual": this.state.denNgay.toISOString().split('T')[0],
    }

    if (this.state.nhanVienId !== -1) {
      paramter["nhanVienTiepNhanId.equals"] = this.state.nhanVienId
    }

    if (this.state.trangThai !== -1) {
      paramter["trangThai.equals"] = this.state.trangThai
    }
    if (this.state.coBHYT === true) {
      paramter["coBaoHiem.equals"] = 1

    }
    return paramter
  }


   onLamMoiButtonClick() {

    const paramter = this.getParamter();
    console.warn("paramter ", paramter)
    this.props.getDanhSachTiepNhan(paramter)

  }


  renderGioiTinhCol(data) {
    return (
      <CheckBox value={data.value}/>
    )

  }


  renderTrangThaiCol(data) {
    const trangThai = data.value
    switch (trangThai) {
      case 1:
        return (
          <div>{translate('khamChuaBenh.trangThaiTiepNhan.choKham')}</div>
        )
      case 2:
        return (
          <div>{translate('khamChuaBenh.trangThaiTiepNhan.dangKham')}</div>
        )
      case 3:
        return (
          <div>{translate('khamChuaBenh.trangThaiTiepNhan.dangDieuTri')}</div>
        )
      case 4:
        return (
          <div>{translate('khamChuaBenh.trangThaiTiepNhan.daKham')}</div>
        )
      case 5:
        return (
          <div>{translate('khamChuaBenh.trangThaiTiepNhan.xuatVien')}</div>
        )
      case 6:
        return (
          <div>{translate('khamChuaBenh.trangThaiTiepNhan.chuyenKhoaPhong')}</div>
        )
      case 7:
        return (
          <div>{translate('khamChuaBenh.trangThaiTiepNhan.chuyenVienTuyen')}</div>
        )
      case 8:
        return (
          <div>{translate('khamChuaBenh.trangThaiTiepNhan.nhapVien')}</div>
        )
      case 9:
        return (
          <div>{translate('khamChuaBenh.trangThaiTiepNhan.tuVong')}</div>
        )
      default:
        return (
          <div style={{backgroundColor: '#ededed'}}>{data.value}</div>
        )
    }

  }

  renderDataGrid() {
    return (
      <div style={{width: "100%"}}>
        <DataGrid
          className={'dataGrip'}
          dataSource={this.props.dsTiepNhan}
          showBorders={true}
          focusedRowEnabled={true}
          showRowLines={true}
          keyExpr={'bakbId'}
          width={"100%"}
          onRowDblClick={e => this.props.onRowDblClick(e)}

        >
          <Paging
            defaultPageIndex={0}
            enabled={true}
            pageSize={15}
          />
          <Pager
            // visible={true}
            showPageSizeSelector={false}
            allowedPageSizes={[5, 10, 20]}
            showInfo={true}/>
          <LoadPanel
            enabled={true}
            showPane={true}
          />
          <FilterRow visible={true}/>
          <Column dataField="soThuTu" caption={'STT'} minWidth={40}/>
          <Column dataField="tenKhoaHoanTatKham" caption={'Phòng Khám'} minWidth={200}/>
          <Column dataField="phieu" caption={'Phiếu'} minWidth={60}/>
          <Column dataField="tenBenhNhan" caption={'Họ tên'} minWidth={200}/>
          <Column dataField="gioiTinh" caption={'NAM'} minWidth={50} cellRender={this.renderGioiTinhCol}/>
          <Column dataField="tuoi" caption={'tuổi'} minWidth={50}/>
          <Column dataField="thangTuoi" caption={'Tháng tuổi'} minWidth={50}/>
          <Column dataField="bhytSoThe" caption={'BHYT'} minWidth={150}/>
          <Column dataField="trangThai" caption={'Trạng thái'} minWidth={90} cellRender={this.renderTrangThaiCol}/>
          <Column dataField="nvTiepNhan" caption={'NV Tiếp nhận'} minWidth={100}/>

        </DataGrid>


      </div>
    )
  }

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (
      <div style={{height: this.height}}>
        <div style={{padding: 10}}>
          {this.renderFormFiller()}
        </div>

        <div style={{padding: 10}}>
          {
            this.renderDataGrid()
          }
        </div>

      </div>
    );
  }


}

const mapDispatchToProps = {
  getDanhSachTiepNhan
};
const mapStateToProps = (storeState: IRootState) => {
  return {
    dsTiepNhan:storeState.thongtintiepnhan.danhSach
  }
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DanhSachTiepNhanForm);

