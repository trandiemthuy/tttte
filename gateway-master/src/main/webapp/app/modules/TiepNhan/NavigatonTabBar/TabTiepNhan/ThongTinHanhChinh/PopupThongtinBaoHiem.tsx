import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Form, {
  ButtonItem,
  GroupItem,
  SimpleItem,
  Label,
  CompareRule,
  EmailRule,
  PatternRule,
  RangeRule,
  RequiredRule,
  StringLengthRule,
  AsyncRule
} from 'devextreme-react/form';
import { SelectBox } from 'devextreme-react/select-box';
import { Popup } from 'devextreme-react/data-grid';
// import service from 'app/modules/tiep-nhan/global';

export const PopupThongTinBenhNhan = props => {
  const { visible } = props;
  // console.log(props);
  return (
    <div id="container">
      <Popup
        visible={visible}
        onHiding={this.hideInfo}
        dragEnabled={false}
        closeOnOutsideClick={true}
        showTitle={true}
        title="Thông tin bệnh nhân"
      >
        <p>
          Full Name:&nbsp;
          <span>{this.state.currentEmployee.FirstName}</span>&nbsp;
          <span>{this.state.currentEmployee.LastName}</span>
        </p>
        <p>
          Birth Date: <span>{this.state.currentEmployee.BirthDate}</span>
        </p>
        <p>
          Address: <span>{this.state.currentEmployee.Address}</span>
        </p>
        <p>
          Hire Date: <span>{this.state.currentEmployee.HireDate}</span>
        </p>
        <p>
          Position: <span>{this.state.currentEmployee.Position}</span>
        </p>
      </Popup>
    </div>
  );
};
