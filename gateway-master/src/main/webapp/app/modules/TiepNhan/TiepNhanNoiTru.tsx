import {IRootState} from "app/shared/reducers";
import {Link, RouteComponentProps} from 'react-router-dom';
import {connect} from 'react-redux';
import React from 'react';
import { TabPanel } from 'devextreme-react';
import { Item } from 'devextreme-react/form';
import ThongTinTiepNhan from "app/modules/TiepNhan/Tabs/ThongTinTiepNhan";
import DanhSachTiepNhan from "app/modules/TiepNhan/Tabs/DanhSachTiepNhan";

export interface ITiepNhanNgoaiTruProps extends DispatchProps, RouteComponentProps<{ url: string }> {
}

export const TiepNhanNgoaiTru = (props: ITiepNhanNgoaiTruProps) => {
  return (
    <>
      <div>
        <TabPanel>
          <Item title={"Thông tin tiếp nhận"} component={ThongTinTiepNhan}/>
          <Item title={"Danh sách tiếp nhận"} component={DanhSachTiepNhan}/>
        </TabPanel>
      </div>
    </>
  );
};

const mapStateToProps = ({benhNhan}: IRootState) => ({
    thongTinBenhNhan: benhNhan.entity
  });

const mapDispatchToProps = {

};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TiepNhanNgoaiTru);

