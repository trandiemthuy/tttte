import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import 'devextreme-react/text-area';
import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import NavBar from './NavigatonTabBar/Navigation-tab-bar';
import TiepNhanNgoaiTru from "app/modules/TiepNhan/TiepNhanNgoaiTru";
import TiepNhanNoiTru from "app/modules/TiepNhan/TiepNhanNoiTru";
const Routes = ({ match }) => (
  <>
    <Switch>

      <ErrorBoundaryRoute path={`${match.url}/ngoai-tru`} component={TiepNhanNgoaiTru} exact/>
      <ErrorBoundaryRoute path={`${match.url}/noi-tru`} component={TiepNhanNoiTru} exact/>
      <ErrorBoundaryRoute path={`${match.url}`} component={NavBar} />
    </Switch>
    </>
);
export default Routes;
