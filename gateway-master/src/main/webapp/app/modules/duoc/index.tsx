import React from 'react';
import { Switch } from 'react-router-dom';
import NhapKhoComponent from './duoc-components/nhap-kho';
import ChuyenKhoComponent from './duoc-components/chuyen-kho';
import DuyetPhieuComponent from './duoc-components/duyet-phieu';
import DuyetDuTruHoanTraComponent from './duoc-components/duyet-du-tru-hoan-tra';
import DuocNhanDuocComponent from './duoc-components/nhan-duoc';
import DuocKiemTraDuyetNhanComponent from './duoc-components/kiem-tra-duyet-nhan';
import DashboardComponent from './duoc-components/dashboard';
import DuocTongHopDuTruComponent from './duoc-components/tong-hop-du-tru';
import XuatDuocComponent from './duoc-components/xuat-duoc';
import XuatDuocBanLeComponent from './duoc-components/xuat-duoc-ban-le';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
// import 'devextreme/dist/css/dx.common.css';
// import 'devextreme/dist/css/dx.light.css';
// import 'devextreme/dist/css/dx.light.compact.css';
import './duoc.scss';
// import 'devextreme/dist/css/dx.material.teal.dark.compact.css';
// import 'devextreme/dist/css/dx.material.blue.light.compact.css';
// import 'devextreme/dist/css/dx.material.orange.dark.compact.css';
// import 'devextreme/dist/css/dx.dark.compact.css';
import {getDvtt} from './duoc-components/utils/thong-tin';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      <ErrorBoundaryRoute path={`${match.url}/nhap-kho`} component={NhapKhoComponent}/>
      <ErrorBoundaryRoute path={`${match.url}/chuyen-kho`} component={ChuyenKhoComponent}/>
      <ErrorBoundaryRoute path={`${match.url}/duyet-phieu`} component={DuyetPhieuComponent}/>
      <ErrorBoundaryRoute path={`${match.url}/duyet-dutru-hoantra`} component={DuyetDuTruHoanTraComponent}/>
      <ErrorBoundaryRoute path={`${match.url}/nhan-duoc`} component={DuocNhanDuocComponent}/>
      <ErrorBoundaryRoute path={`${match.url}/kiem-tra-duyet-nhan`} component={DuocKiemTraDuyetNhanComponent}/>
      <ErrorBoundaryRoute path={`${match.url}/duoc-dashboard`} component={DashboardComponent}/>
      <ErrorBoundaryRoute path={`${match.url}/tong-hop-du-tru`} component={DuocTongHopDuTruComponent}/>
      <ErrorBoundaryRoute path={`${match.url}/xuat-duoc`} component={XuatDuocComponent}/>
      <ErrorBoundaryRoute path={`${match.url}/xuat-duoc-ban-le`} component={XuatDuocBanLeComponent}/>
    </Switch>
  </div>
);

export default Routes;
