import { Column } from 'devextreme-react/data-grid';
import React from 'react';

export const apiURL = 'services/duocapp/';

// Notify
export const NOTIFY_WIDTH = 'auto'; // Chiều rộng của 1 notify
export const NOTIFY_DISPLAYTIME = 3500; // Thời gian tồn tại của 1 notify
export const SHADING = false; //

// loadPanel
export const configLoadPanel = {
  enabled: true,
  height: 90,
  indicatorSrc: '',
  shading: true,
  shadingColor: '',
  showIndicator: true,
  showPane: true,
  text: 'Đang tải. Vui lòng đợi...',
  width: 200
};

export const pageSizes = [5, 10, 20];

export const numberFormat = '#,##0.00';
