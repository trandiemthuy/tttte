import React from 'react';
import { DropDownBox } from 'devextreme-react';
import {DataGrid, Column, FilterRow, HeaderFilter, Selection, IDataGridOptions, Scrolling, Paging} from "devextreme-react/data-grid";
import {configLoadPanel} from '../../../configs';

export interface IDropDownVatTuProp extends IDataGridOptions{
  listDanhSachVatTu: any;
  dropWidth: any;
  dropHeight: any;
  handleSelectTenVatTu: Function;
  className?: any;
}
export interface IDropDownVatTuState {
  isLoaded: boolean;
  selectedValue: any;
  indexRow: number;
  isOpened: boolean;
  focusRow: any;
  selectedVatTuObj: any;
}

export class DropDownVatTu extends React.Component<IDropDownVatTuProp, IDropDownVatTuState> {
  private tenVatTuRef = React.createRef<DropDownBox>();
  private filterRowTenVatTu: any;
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      selectedValue: [],
      indexRow: 0,
      isOpened: false,
      focusRow: 0,
      selectedVatTuObj: {}
    }
  }
  componentDidMount(): void {
  }

  dataGridVatTuRender = () => {
    return <DataGrid
      dataSource={this.props.listDanhSachVatTu}
      showBorders={true}
      showColumnLines={true}
      rowAlternationEnabled={true}
      allowColumnResizing={true}
      columnResizingMode={'nextColumn'}
      columnMinWidth={50}
      height={this.props.dropHeight}
      selectedRowKeys={this.state.selectedValue}
      onSelectionChanged={this.handleOnSelectDatagridVatTu}
      onKeyDown={this.handleOnKeyDownDatagridVatTu}
      loadPanel={configLoadPanel}
      onCellPrepared={this.handleOnCellRepared}
      onContentReady={this.handleOnContentReady}
      {...this.props}
    >
      <Selection mode="single" />
      <Scrolling mode="infinite" />
      <Paging enabled={true} pageSize={10} />
      <FilterRow visible={true}
                 applyFilter={'auto'}

      />
      <HeaderFilter visible={true} key={'l2kd'} />
      <Column
        dataField="MAVATTU"
        caption="Mã vật tư"
        dataType="string"
        alignment="left"
        width={'7%'}
      />
      <Column
        dataField="TENVATTU"
        caption="Tên vật tư"
        dataType="string"
        alignment="left"
        width={'15%'}
      />
      <Column
        dataField="HOATCHAT"
        caption="Hoạt chất"
        dataType="string"
        alignment="left"
        width={'10%'}
      />
      <Column
        dataField="DVT"
        caption="ĐVT"
        dataType="number"
        alignment="left"
        width={'5%'}
      />
      <Column
        dataField="MABAOCAO"
        caption="Số thầu"
        dataType="string"
        alignment="left"
        width={'10%'}
      />
      <Column
        dataField="DONGIA_BV"
        caption="Đơn giá"
        dataType="string"
        alignment="left"
        width={'10%'}
      />
      <Column
        dataField="DONGIA_DV"
        caption="Giá bán"
        dataType="string"
        alignment="left"
        width={'10%'}
      />
      <Column
        dataField="SOGPDK"
        caption="Số GPĐK"
        dataType="string"
        alignment="left"
        width={'10%'}
      />
      <Column
        dataField="GHICHUVATTU"
        caption="Ghi chú"
        dataType="string"
        alignment="left"
        width={'5%'}
        visible={false}
      />
      <Column
        dataField="TEN_QUOC_GIA"
        caption="Nước SX"
        dataType="string"
        alignment="left"
        width={'8%'}
      />
      <Column
        dataField="TEN_HANG_SX"
        caption="Nhà SX"
        dataType="string"
        alignment="left"
        width={'5%'}
        visible={false}
      />
      <Column
        dataField="QUYETDINH"
        caption="Quyết định"
        dataType="string"
        alignment="left"
        width={'5%'}
      />
      <Column
        dataField="MAGOI_THAU_VT"
        caption="Gói thầu"
        dataType="string"
        alignment="left"
        width={'5%'}
        visible={false}
      />
      <Column
        dataField="MA_NHOM_THAU_VT"
        caption="Nhóm thầu"
        dataType="string"
        alignment="left"
        width={'5%'}
        visible={false}
      />
    </DataGrid>
  };

  handleOnSelectDatagridVatTu = (e) => {
    this.setState({
      selectedValue: e.selectedRowKeys[0] && e.selectedRowKeys[0].MAVATTU,
      selectedVatTuObj: e.selectedRowKeys[0]
    });
    this.tenVatTuRef.current.instance.close();
    this.props.handleSelectTenVatTu();
  };
  handleOnKeyDownDatagridVatTu = () => {
    // if (e.keyCode === 13) {
    //   this.props.handleSelectTenVatTu();
    // }
  };

  syncDataGridSelection(e) {
    this.setState({
      selectedValue: e.value
    });
  }
  displayExpr = (item) => {
    return item && item.TENVATTU;
  };
  handleOnFocus = () => {
    this.tenVatTuRef.current.instance.open();
    this.setState({
      selectedValue: null
    })
  };
  handleOnOpened = () => {
    if (this.filterRowTenVatTu !== undefined) {
      setTimeout(() => {
        this.filterRowTenVatTu.focus();
      }, 100);
    }
  };
  handleOnKeyDownTenVatTu = (e) => {
    const firstVatTu = this.props.listDanhSachVatTu.filter(vattu => vattu.TENVATTU.toLowerCase().includes(e.target.value.toLowerCase()))[0];
    this.setState({
      focusRow: firstVatTu.MAVATTU
    });
    if (e.keyCode === 13) {
      this.setState({
        selectedValue: firstVatTu.MAVATTU,
        selectedVatTuObj: firstVatTu
      });
      this.tenVatTuRef.current.instance.close();
      this.props.handleSelectTenVatTu();
    }
  };
  handleFocus = () => {
    this.tenVatTuRef.current.instance.focus();
  };
  handleOnCellRepared = (e) => {
    if (e.rowType === "filter" && e.columnIndex === 1) {
      const cellElement = e.cellElement;
      this.filterRowTenVatTu = cellElement.querySelector(".dx-texteditor-input");
    }
  };
  handleOnContentReady = () => {
    this.filterRowTenVatTu.focus();
    this.filterRowTenVatTu.addEventListener('keydown', this.handleOnKeyDownTenVatTu);
  };

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return <DropDownBox
      ref={this.tenVatTuRef}
      className={this.props.className === undefined ? '' : this.props.className}
      deferRendering={false}
      displayExpr={this.displayExpr}
      placeholder="Chọn vật tư"
      dataSource={this.props.listDanhSachVatTu}
      contentRender={this.dataGridVatTuRender}
      dropDownOptions={{ width: this.props.dropWidth, height: this.props.dropHeight }}
      value={this.state.selectedValue}
      valueExpr="MAVATTU"
      disabled={this.props.disabled}
      onValueChanged={this.syncDataGridSelection}
      onOpened={this.handleOnOpened}
      onKeyDown={() => this.props.handleSelectTenVatTu}
      onFocusIn={this.handleOnFocus}

    />
  }
}

export default DropDownVatTu;
