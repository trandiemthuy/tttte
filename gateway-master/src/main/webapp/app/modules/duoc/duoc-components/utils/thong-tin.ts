export const getDvtt = () => {
  // Đơn vị trực thuộc
  return '40003';
};
export const getMaPhongBan = () => {
  // Mã phòng ban
  return '4000304';
};
export const getTenPhongBan = () => {
  return 'Khoa YHCT';
};
export const getMaPhongBenh = () => {
  // Mã phòng bệnh
  return '9716634';
};
export const getTenPhongBenh = () => {
  return 'PK YHCT';
};
export const getMaNhanVien = () => {
  return 1332990;
};
export const getTenNhanVien = () => {
  return 'Admin Nghệ An';
};
export const getTenBenhVien = () => {
  return 'Trung tâm y tế huyện Nam Đàn';
};
export const getTenTinh = () => {
  return 'Nghệ An';
};
