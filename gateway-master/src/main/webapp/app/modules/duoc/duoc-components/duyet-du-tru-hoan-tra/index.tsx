import React from 'react';
import { Switch } from 'react-router-dom';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import DuyetDuTruHoanTraComponent from "./duyet-dutru-hoantra";

const Routes = ({ match }) => (

  <Switch>
    <ErrorBoundaryRoute
      path={`${match.url}/:nghiepVu/:isBANT`}
      component={DuyetDuTruHoanTraComponent}>
    </ErrorBoundaryRoute>
  </Switch>
);

export default Routes;
