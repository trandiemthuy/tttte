import React from 'react';
import { Switch } from 'react-router-dom';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import DuocKiemTraDuyetNhanComponent from "./kiem-tra-duyet-nhan";

const Routes = ({ match }) => (

  <Switch>
    <ErrorBoundaryRoute
      path={`${match.url}`}
      component={DuocKiemTraDuyetNhanComponent}>
    </ErrorBoundaryRoute>
  </Switch>
);

export default Routes;
