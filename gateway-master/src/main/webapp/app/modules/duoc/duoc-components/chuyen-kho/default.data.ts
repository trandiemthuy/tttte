export const dataXuatHuy = [
  { maXuatHuy: 0, tenXuatHuy: 'Xuất thực' },
  { maXuatHuy: 1, tenXuatHuy: 'Xuất hủy' },
  { maXuatHuy: 2, tenXuatHuy: 'Xuất nhượng' },
  { maXuatHuy: 3, tenXuatHuy: 'Xuất điều trị' }
];
