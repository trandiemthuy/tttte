import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection
} from 'devextreme-react/data-grid';
import {Popup} from 'devextreme-react/popup';
import {Button, ScrollView} from 'devextreme-react';
import Box, {Item as BoxItem} from "devextreme-react/box";
import AdvanceDateBox from "app/modules/duoc/duoc-components/utils/advance-datebox/advance-datebox";
import GridChiTietPhieuDuyet
  from "app/modules/duoc/duoc-components/duyet-phieu/components/popup-chi-tiet-phieu/grid-chi-tiet-phieu-duyet";
import {DuocDropDownButton} from "app/modules/duoc/duoc-components/utils/button-dropdown/button-dropdown";
import notify from "devextreme/ui/notify";
import {NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING} from "app/modules/duoc/configs";
import {convertToDate} from "app/modules/duoc/duoc-components/utils/funtions";

export interface IPopUpChiTietPhieuDuyetProp extends IDataGridOptions {
  listDanhSachChiTietPhieuDuyet: any;
  showPopUp: boolean;
  title: string;
  loaiDuyet?: number; // 0: duyệt    1: Hủy duyệt
  selectedPhieuChuyen?: any;
  handleChangePopUpChiTietState: Function;
  handleOnClickButtonInPhieuPopUp?: Function;
  loadingConfigs?: {
    visible: boolean;
    position: string;
  }
  duyetPhieuChuyenKho?: Function;
  huyDuyetPhieuChuyenKho?: Function;
  isNhan?: boolean;
}
export interface IPopUpChiTietPhieuDuyetState {
  enableButtonDuyet: boolean;
}

export class PopUpChiTietPhieuDuyet extends React.Component<IPopUpChiTietPhieuDuyetProp, IPopUpChiTietPhieuDuyetState> {
  private refNgayDuyet = React.createRef<AdvanceDateBox>();
  private refGridDanhSach = React.createRef<GridChiTietPhieuDuyet>();
  constructor(props) {
    super(props);
    this.state = {
      enableButtonDuyet: true
    };
  }

  renderTitle = () => {
    return this.props.title + " (Số phiếu: " + this.props.selectedPhieuChuyen.SOPHIEUCHUYEN + ")";
  };
  handleOnClickButtonInPhieuPopUp = (loaiFile: string, mau: number) => {
    this.props.handleOnClickButtonInPhieuPopUp(loaiFile, mau);
  };
  handleOnClickButtonDuyet = (e) => {
    if (this.props.loaiDuyet === 1) {
      this.props.huyDuyetPhieuChuyenKho();
      this.setState({
        enableButtonDuyet: false
      });
      return;
    }
    const checkArr = this.refGridDanhSach.current.getDanhSachChiTietChinhSua().filter(ct => ct.SOLUONG < ct.SOLUONG_DUYET).length;
    if (checkArr > 0) {
      notify({ message: "Số lượng duyệt không được lớn hơn số lương yêu cầu. Kiểm tra lại", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    this.props.duyetPhieuChuyenKho(this.refGridDanhSach.current.getDanhSachChiTietChinhSua(), convertToDate(this.refNgayDuyet.current.getValue()));
    this.setState({
      enableButtonDuyet: false
    });
  };
  handleOnShowingPopUp = () => {
    this.setState({
      enableButtonDuyet: true
    });
  };

  render() {
    return (
      <Popup
        visible={this.props.showPopUp}
        onHiding={() => this.props.handleChangePopUpChiTietState(false)}
        dragEnabled={true}
        closeOnOutsideClick={false}
        showTitle={true}
        title={"Chi tiết phiếu " + this.renderTitle()}
        width={1000}
        height={450}
        onShowing={this.handleOnShowingPopUp}
      >
        <ScrollView
          scrollByContent={true}
          scrollByThumb={true}
        >
          <Box
          direction={'col'}
          width={'100%'}
        >
          {
            !this.props.isNhan ? (
              <BoxItem ratio={0} baseSize={'100%'}>
                <div className={'row-padding-bottom-5'} style={{ display: 'flex'}}>
                  <div className={'row-padding-top-5 row-padding-right-5'} style={{ visibility: this.props.loaiDuyet === 0 ? "visible" : "hidden"}}>
                    Ngày duyệt
                  </div>
                  <div style={{ visibility: this.props.loaiDuyet === 0 ? "visible" : "hidden"}}>
                    <AdvanceDateBox ref={this.refNgayDuyet} width={300}/>
                  </div>
                  <Button
                    icon={this.props.loaiDuyet === 0 ? 'check' : 'close'}
                    type={this.props.loaiDuyet === 0 ? 'success' : 'danger'}
                    text={this.props.loaiDuyet === 0 ? 'Duyệt' : 'Hủy duyệt'}
                    width={120}
                    className={'col-margin-left-5'}
                    onClick={this.handleOnClickButtonDuyet}
                    disabled={!this.state.enableButtonDuyet}
                  />
                  <DuocDropDownButton
                    loadingConfigs={this.props.loadingConfigs && this.props.loadingConfigs}
                    text={'In phiếu'}
                    disabled={false}
                    arrayButton={[
                      { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInPhieuPopUp('pdf', 0)},
                      { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInPhieuPopUp('xls', 0)},
                      { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInPhieuPopUp('rtf', 0)}
                    ]}
                    defaultHandleOnClick={() => this.handleOnClickButtonInPhieuPopUp('pdf', 0)}
                    width={120}
                    className={'dx-button-mode-contained dx-button-default duoc-dropdown-button col-margin-left-5'}
                  />
                </div>
              </BoxItem>
            ) : null
          }
          <BoxItem ratio={0} baseSize={'100%'}>
            <GridChiTietPhieuDuyet
              isNhan={true}
              ref={this.refGridDanhSach}
              listDanhSachChiTietPhieuDuyet={this.props.listDanhSachChiTietPhieuDuyet}
              duyet={this.state.enableButtonDuyet === false ? -1 : this.props.loaiDuyet}
            />
          </BoxItem>
        </Box>
        </ScrollView>
      </Popup>);
  }
}

export default PopUpChiTietPhieuDuyet;
