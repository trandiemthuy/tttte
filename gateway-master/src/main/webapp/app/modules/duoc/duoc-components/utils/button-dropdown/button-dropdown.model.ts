export interface IDuocDropDownButton {
  id: number; // id của item
  title: string; // tiêu đề
  icon: string; // icon
  handleOnClick: Function; // handle
}
