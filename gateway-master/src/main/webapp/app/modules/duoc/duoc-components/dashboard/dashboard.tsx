import React from 'react';
import {RouteComponentProps} from "react-router";
import {IRootState} from "app/shared/reducers";
import {connect} from "react-redux";
import './dashboard.scss';
import { CircularGauge, Scale, RangeContainer, Range, Export, Title, Font } from 'devextreme-react/circular-gauge';

export interface IDashboardComponentProp extends StateProps, DispatchProps, RouteComponentProps<{ nghiepVu: string }> {
  account: any;
}

export interface IDashboardComponentState {
  navIndex: number;
}

export class DashboardComponent extends React.Component<IDashboardComponentProp, IDashboardComponentState> {
  constructor(props) {
    super(props);
    this.state = {
      navIndex: 0
    }
  }

  componentDidMount(): void {
  }

  // Handles
  handleOnClickNavItems = (index) => {
    // console.log(e);
    this.setState({
      navIndex: index
    })
  };
  // Funtion


  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (<div>
      <div className={'dashboard-container'}>
        <div className={'dashboard-nav'}>
          <div className={'dashboard-nav-items'}>
            <div className={'dashboard-nav-items-content'} onClick={() => this.handleOnClickNavItems(this.state.navIndex === 0 ? 1 : 0)}>
              <div className={'dx-icon-datafield dasboard-nav-items-icon'}></div>
              <div className={'dasboard-nav-items-title'}>
                <a>
                  Tên chức năng
                </a>

              </div>
            </div>
          </div>
          <div className={'dashboard-nav-items'}>
            <div className={'dashboard-nav-items-content'} onClick={() => this.handleOnClickNavItems(this.state.navIndex === 0 ? 1 : 0)}>
              <div className={'dx-icon-datafield dasboard-nav-items-icon'}></div>
              <div className={'dasboard-nav-items-title'}>
                <a>
                  Tên chức năng
                </a>

              </div>
            </div>
          </div>
          <div className={'dashboard-nav-items'}>
            <div className={'dashboard-nav-items-content'} onClick={() => this.handleOnClickNavItems(this.state.navIndex === 0 ? 1 : 0)}>
              <div className={'dx-icon-datafield dasboard-nav-items-icon'}></div>
              <div className={'dasboard-nav-items-title'}>
                <a>
                  Tên chức năng
                </a>

              </div>
            </div>
          </div>
          <div className={'dashboard-nav-items'}>
            <div className={'dashboard-nav-items-content'} onClick={() => this.handleOnClickNavItems(this.state.navIndex === 0 ? 1 : 0)}>
              <div className={'dx-icon-datafield dasboard-nav-items-icon'}></div>
              <div className={'dasboard-nav-items-title'}>
                <a>
                  Tên chức năng
                </a>

              </div>
            </div>
          </div>
        </div>
        <div className={'dashboard-content'}>
          {
            this.state.navIndex === 0 ? (
              <CircularGauge
                id="gauge"
                value={0}
                subvalues={[50, 80]}
              >
                <Scale startValue={0} endValue={100} tickInterval={10}>
                  {/* <Label customizeText={() => } />*/}
                </Scale>
                <RangeContainer>
                  <Range startValue={0} endValue={40} color="#00cc99" />
                  <Range startValue={40} endValue={70} color="#ff8000" />
                  <Range startValue={70} endValue={100} color="#ff0000" />
                </RangeContainer>
                <Export enabled={true} />
                <Title text="Battery Charge">
                  <Font size={28} />
                </Title>
              </CircularGauge>
            ) : (
            <CircularGauge
            id="gauge"
            value={100}
            subvalues={[60, 90]}
            >
            <Scale startValue={0} endValue={100} tickInterval={10}>
            </Scale>
            <RangeContainer>
            <Range startValue={0} endValue={40} color="#00cc99" />
            <Range startValue={40} endValue={70} color="#ff8000" />
            <Range startValue={70} endValue={100} color="#ff0000" />
            </RangeContainer>
            <Export enabled={true} />
            <Title text="Battery Charge">
            <Font size={28} />
            </Title>
            </CircularGauge>
            )
          }
        </div>
      </div>
    </div>);
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated
});

const mapDispatchToProps = {
};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardComponent);
