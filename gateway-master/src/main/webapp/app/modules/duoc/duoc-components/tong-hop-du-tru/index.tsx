import React from 'react';
import { Switch } from 'react-router-dom';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import TongHopDuTruComponent from "./tong-hop-du-tru";

const Routes = ({ match }) => (

  <Switch>
    <ErrorBoundaryRoute
      path={`${match.url}/:nghiepVu/:isBANT`}
      component={TongHopDuTruComponent}>
    </ErrorBoundaryRoute>
  </Switch>
);

export default Routes;
