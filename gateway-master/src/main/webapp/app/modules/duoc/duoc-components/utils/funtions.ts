// Chuyển sang định dạng yyyy-mm-dd
import notify from 'devextreme/ui/notify';
import { NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING } from 'app/modules/duoc/configs';

export const convertToSQLDate = (date: string) => {
  const dateTemp = new Date(date);
  const day = dateTemp.getDate();
  const month = dateTemp.getMonth() + 1;
  const year = dateTemp.getFullYear();
  return year + '-' + (month < 10 ? '0' + month : month) + '-' + (day < 10 ? '0' + day : day);
};

// Chuyển dd/mm/yyyy sang định dạng yyyy-mm-dd
export const convertToSQLDate1 = (date: string) => {
  const dateTemp = date.split('/');
  const day = parseInt(dateTemp[0], 0);
  const month = parseInt(dateTemp[1], 0);
  const year = parseInt(dateTemp[2], 0);
  return year + '-' + (month < 10 ? '0' + month : month) + '-' + (day < 10 ? '0' + day : day);
};

// Chuyển từ yyyy-mm-dd sang định dạng dd/mm/yyyy
export const convertToDate = (date: string) => {
  if (date === '' || date === ' ') {
    return '';
  }
  const dateTemp = new Date(date);
  const day = dateTemp.getDate();
  const month = dateTemp.getMonth() + 1;
  const year = dateTemp.getFullYear();
  return (day < 10 ? '0' + day : day) + '/' + (month < 10 ? '0' + month : month) + '/' + year;
};

// Chuyển kiểu date sang định dạng dd/mm/yyyy
export const convertToDate1 = (date: string) => {
  return new Date(date).toLocaleDateString('en-GB');
};

// Lấy mô tả của tham số đơn vị theo mã tham số
export const getMoTaThamSo = (listThamSo: any, maThamSo: number) => {
  const result = listThamSo.filter(thamso => thamso.MA_THAMSO === maThamSo);
  if (result.length <= 0) {
    return 0;
  } else {
    return result[0].MOTA_THAMSO;
  }
};
// Lấy ngày đầu tháng, ví dụ 01/06/2020
export const getFirstDate = () => {
  const today = new Date();
  const day = 1;
  const month = today.getMonth();
  const year = today.getFullYear();
  return new Date(year, month, day);
};
// Lấy mô tả của tham số dược mới
export const getMoTaThamSoDuocMoi = (listThamSo: any, maThamSo: number) => {
  const result = listThamSo.filter(thamso => thamso.MA_THAMSO === maThamSo);
  if (result.length <= 0) {
    return 0;
  } else {
    return result.MOTA_THAMSO;
  }
};

// Lấy ngày này của k năm sau
export const getKYearFromNow = (k: number) => {
  const dateTemp = new Date();
  const day = dateTemp.getDate();
  const month = dateTemp.getMonth();
  const year = dateTemp.getFullYear() + k;
  return new Date(year, month, day);
};

export const convertDateFormat = (date: string) => {
  const pattern1 = '/';
  const pattern2 = '-';
  if (date === null || date === undefined) {
    return '';
  }
  if (date.includes(pattern1)) {
    return date;
  } else if (date.includes(pattern2)) {
    return convertToDate(date);
  }
};

export const simpleIf = (condition: boolean, trueValue: any, falseValue: any) => {
  if (condition === null || condition === undefined) {
    return falseValue;
  }
  if (condition) {
    return trueValue;
  } else {
    return falseValue;
  }
};

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
export const simpleIfFunction = (condition?: boolean, trueFunction: Function, falseFunction: Function) => {
  if (condition === null || condition === undefined) {
    falseFunction();
  }
  if (condition) {
    trueFunction();
  } else {
    falseFunction();
  }
};

export const simpleCheckOccurArray = (array: any, element: any, trueFunction: Function, falseFunction: Function) => {
  if (array === null || array === undefined) {
    falseFunction();
  }
  if (element === null || element === undefined) {
    falseFunction();
  }
  if (array.includes(element)) {
    trueFunction();
  } else {
    falseFunction();
  }
};

export const catchStatusCodeFromResponse = (statusCode: number, defaultMessage?: string) => {
  let message = '';
  switch (statusCode) {
    case 400: {
      message = 'Thông tin không hợp lệ. Vui lòng kiểm tra lại!'; // Thiếu params hoặc truyền sai params
      break;
    }
    case 401: {
      message = 'Bạn không có quyền truy cập hoặc phiên đăng nhập đã hết hạn. Vui lòng đăng nhập lại!'; // Mất xác thực
      break;
    }
    case 404: {
      message = 'Đường dẫn không tồn tại. Vui lòng kiểm tra lại!'; // Gọi sai đường dẫn
      break;
    }
    case 405: {
      message = 'Phương thức truy cập không hợp lệ. Vui lòng kiểm tra lại!'; // Gọi sai request method
      break;
    }
    case 500: {
      message = 'Máy chủ không phản hồi. Vui lòng thử lại sau!'; // Máy chủ xảy ra lỗi
      break;
    }
    case 504: {
      message = 'Hết thời gian truy cập máy chủ. Vui lòng thử lại sau!'; // Gateway timeout
      break;
    }
    default: {
      message = defaultMessage;
      break;
    }
  }
  notify(
    {
      message: defaultMessage === undefined ? 'Có lỗi xảy ra' : message,
      width: NOTIFY_WIDTH,
      shading: SHADING
    },
    'error',
    NOTIFY_DISPLAYTIME
  );
};
