import React from "react";
import {Button} from "devextreme-react";
import Form, { SimpleItem, Label, RequiredRule, GroupItem } from 'devextreme-react/form';
import {dataXuatHuy} from "app/modules/duoc/duoc-components/chuyen-kho/default.data";
import {DuocDropDownButton} from "app/modules/duoc/duoc-components/utils/button-dropdown/button-dropdown";
import {defaultPhieuChuyenKho, IPhieuChuyenKho} from "../../chuyen-kho.model";
import {ISetEnable, defaultSetEnable} from "../../enable.model";
import notify from "devextreme/ui/notify";
import {NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING} from "app/modules/duoc/configs";
import dialog from "devextreme/ui/dialog";

export interface IFormPhieuChuyenKhoProp {
  maNghiepVu: number; // Mã nghiệp vụ mặc định ban đầu
  maDonViQuanLy: string;
  maDonVi: string;
  maPhongBan: string;
  listDanhSachNghiepVu: any;
  listDanhSachKhoChuyen: any;
  listDanhSachKhoNhan: any;
  enableForm: boolean;
  loadingConfigs: {
    visible: boolean;
    position: string;
  }
  addPhieuChuyenKho: Function;
  updatePhieuChuyenKho: Function;
  deletePhieuChuyenKho: Function;
  getDanhSachPhieuChuyen: Function;
  getDanhSachKhoNhan: Function;
  getDanhSachKhoChuyen: Function;
  handleOnClickButtonInPhieuChuyenKho: Function;
  handleOnClickButtonInPhieuXuatKho: Function;
  handleOnClickButtonInBienBanThanhLy: Function;
  handleOnClickButtonInPhieuDuTruDinhMuc: Function;
  handleChangeEnableInput: Function;
  handleChangePopUpState: Function;
  sendPhieuChuyenKho: {
    phieuChuyenKho: IPhieuChuyenKho;
    isUpdated: number;
  }
}
export interface IFormPhieuChuyenKhoState {
  phieuChuyenKhoObj: IPhieuChuyenKho;
  enable: ISetEnable;
}

export class FormPhieuChuyenKho extends React.Component<IFormPhieuChuyenKhoProp, IFormPhieuChuyenKhoState> {
  constructor(props) {
    super(props);
    this.state = {
      phieuChuyenKhoObj: {...defaultPhieuChuyenKho, maNghiepVuChuyen: props.maNghiepVu},
      enable: defaultSetEnable
    };
  }
  componentDidMount(): void {
  }

  componentDidUpdate(prevProps: Readonly<IFormPhieuChuyenKhoProp>, prevState: Readonly<IFormPhieuChuyenKhoState>, snapshot?: any): void {
    if (prevProps.maNghiepVu !== this.props.maNghiepVu) {
      this.setState({
        phieuChuyenKhoObj: {
          ...defaultPhieuChuyenKho,
          maNghiepVuChuyen: this.props.maNghiepVu
        }
      });
    }

    if (prevProps.sendPhieuChuyenKho.isUpdated !== this.props.sendPhieuChuyenKho.isUpdated && this.props.sendPhieuChuyenKho.isUpdated !== -1) {
      this.setState({
        phieuChuyenKhoObj: this.props.sendPhieuChuyenKho.phieuChuyenKho
      });
      if (this.props.sendPhieuChuyenKho.isUpdated === 0) { // Nhấn double click vào grid
        this.setEnabled(false, true, false, true, true, false);
      } else if (this.props.sendPhieuChuyenKho.isUpdated === 1) { // Sau khi thêm mới phiếu
        this.setEnabled(false, false, true, true, false, false);
      } else if (this.props.sendPhieuChuyenKho.isUpdated === 2) { // Sau khi cập nhật (lưu, xóa) phiếu
        this.setEnabled(true, false, false, false, false, true);
      }
    }
  }

  // Handles
  handleOnChangeFormPhieuChuyen = (e) => {
    if (e.dataField === 'maNghiepVuChuyen') {
      this.setState({
        phieuChuyenKhoObj: {...this.state.phieuChuyenKhoObj,
          nghiepVu: this.props.listDanhSachNghiepVu.filter(nv => nv.MA_NGHIEP_VU === this.state.phieuChuyenKhoObj.maNghiepVuChuyen)[0].NGHIEP_VU,
        }
      });
      this.props.getDanhSachPhieuChuyen(this.state.phieuChuyenKhoObj.maNghiepVuChuyen);
      this.props.getDanhSachKhoChuyen(this.state.phieuChuyenKhoObj.maNghiepVuChuyen);
      this.props.getDanhSachKhoNhan(this.state.phieuChuyenKhoObj.maNghiepVuChuyen);
    }
  };
  handleOnClickButtonThem = (e) => {
    e.preventDefault();
    const nghiepVu = this.props.maNghiepVu;
    const donViQuanLy = this.props.maDonViQuanLy;
    const donVi = this.props.maDonVi;
    const phongBan = this.props.maPhongBan;
    let maDonViGiao = null;
    let maDonViNhan = null;
    if (nghiepVu === 37) {
      maDonViGiao = donViQuanLy;
      maDonViNhan = donVi;
    } else if (nghiepVu === 50) {
      maDonViGiao = donVi;
      maDonViNhan = donViQuanLy;
    } else {
      maDonViGiao = donVi;
      maDonViNhan = donVi;
    }
    this.setState({
      phieuChuyenKhoObj: {...this.state.phieuChuyenKhoObj,
        maDonViTao: donVi,
        maDonViGiao,
        maDonViNhan,
        maNguoiTao: 1,
        duTruCSTT: 0,
        nghiepVu: this.state.phieuChuyenKhoObj.nghiepVu === null ? this.props.listDanhSachNghiepVu.filter(nv => nv.MA_NGHIEP_VU === this.state.phieuChuyenKhoObj.maNghiepVuChuyen)[0].NGHIEP_VU : this.state.phieuChuyenKhoObj.nghiepVu,
        maPhongBanTao: phongBan,
        xuatHuy: this.state.phieuChuyenKhoObj.maNghiepVuChuyen !== 55 ? -1 : this.state.phieuChuyenKhoObj.xuatHuy
      }
    }, () => {
      this.props.addPhieuChuyenKho(this.state.phieuChuyenKhoObj, this.setEnabled);
    });
    this.props.handleChangeEnableInput(true);
  };
  handleOnClickButtonSua = (e) => {
    if (this.state.phieuChuyenKhoObj.idChuyenKhoVatTu === null) {
      notify({ message: "Chưa chọn phiếu chuyển kho", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    this.setEnabled(false, false, true, true, false, true);
    this.props.handleChangeEnableInput(true);
  };
  handleOnClickButtonLuu = (e) => {
    if (this.state.phieuChuyenKhoObj.idChuyenKhoVatTu === null) {
      notify({ message: "Chưa chọn phiếu chuyển kho", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    if (this.props.sendPhieuChuyenKho.isUpdated === 1) {
      this.setEnabled(true, false, false, false, false, true);
      return;
    }
    dialog.confirm("Cập nhật thông tin phiếu " + "<span style='color: red'>" + this.state.phieuChuyenKhoObj.soPhieuChuyen + "</span>" + " ?", "Thông báo hệ thống")
      .then(dialogResult => {
        if (dialogResult) {
          this.props.updatePhieuChuyenKho(this.state.phieuChuyenKhoObj);
        }
      });
  };
  handleOnClickButtonHuy = (e) => {
    this.setEnabled(true, false, false, false, false, true);
    this.setState({
      phieuChuyenKhoObj: {...defaultPhieuChuyenKho,
      maNghiepVuChuyen: this.props.maNghiepVu}
    });
    this.props.handleChangeEnableInput(false);
  };
  handleOnClickButtonXoa = (e) => {
    if (this.state.phieuChuyenKhoObj.idChuyenKhoVatTu === null) {
      notify({ message: "Chưa chọn phiếu chuyển kho", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    dialog.confirm("Xóa phiếu " + "<span style='color: red'>" + this.state.phieuChuyenKhoObj.soPhieuChuyen + "</span>" + " ?", "Thông báo hệ thống")
      .then(dialogResult => {
        if (dialogResult) {
          this.props.deletePhieuChuyenKho(this.state.phieuChuyenKhoObj.idChuyenKhoVatTu, this.state.phieuChuyenKhoObj.maNghiepVuChuyen);
        }
      });
  };
  handleOnClickButtonInPhieuChuyenKho = (loaiFile: string) => {
    this.props.handleOnClickButtonInPhieuChuyenKho(loaiFile);
  };
  handleOnClickButtonInPhieuXuatKho = (loaiFile: string, mau: number) => {
    this.props.handleOnClickButtonInPhieuXuatKho(loaiFile, mau);
  };
  handleOnClickButtonInBienBanThanhLy = (loaiFile:string, mau: number) => {
    this.props.handleOnClickButtonInBienBanThanhLy(loaiFile, mau);
  };
  handleChangePopUpState = (loaiIn: number, visible: boolean, loaiFile: string) => {
    this.props.handleChangePopUpState(loaiIn, visible, loaiFile);
  };
  handleOnClickButtonInPhieuDuTruDinhMuc = (loaiFile: string) => {
    this.props.handleOnClickButtonInPhieuDuTruDinhMuc(loaiFile);
  };

  // Functions
  changeTitleByNghiepVu = () => {
    const nghiepVu = this.props.maNghiepVu;
    const formPhieuChuyenObj = {
      titleForm: '',
      titleNoiNhan: '',
      dataFieldNoiNhan: '',
      titleNoiChuyen: '',
      dataFieldNoiChuyen: ''
    };
    switch (nghiepVu) {
      case 37: {
        formPhieuChuyenObj.titleForm = "Dự trù tuyến trên";
        formPhieuChuyenObj.titleNoiNhan = "Nơi yêu cầu";
        formPhieuChuyenObj.titleNoiChuyen = "Nơi chuyển";
        formPhieuChuyenObj.dataFieldNoiChuyen = "maKhoGiao";
        formPhieuChuyenObj.dataFieldNoiNhan = "maKhoNhan";
        break;
      }
      case 50: {
        formPhieuChuyenObj.titleForm = "Hoàn trả tuyến trên";
        formPhieuChuyenObj.titleNoiNhan = "Nơi hoàn trả";
        formPhieuChuyenObj.titleNoiChuyen = "Nơi nhận";
        formPhieuChuyenObj.dataFieldNoiChuyen = "maKhoNhan";
        formPhieuChuyenObj.dataFieldNoiNhan = "maKhoGiao";
        break;
      }
      case 35: {
        formPhieuChuyenObj.titleForm = "Hoàn trả khoa phòng";
        formPhieuChuyenObj.titleNoiNhan = "Nơi hoàn trả";
        formPhieuChuyenObj.titleNoiChuyen = "Nơi nhận";
        formPhieuChuyenObj.dataFieldNoiChuyen = "maKhoNhan";
        formPhieuChuyenObj.dataFieldNoiNhan = "maKhoGiao";
        break;
      }
      case 34: {
        formPhieuChuyenObj.titleForm = "Dự trù khoa phòng";
        formPhieuChuyenObj.titleNoiNhan = "Nơi yêu cầu";
        formPhieuChuyenObj.titleNoiChuyen = "Nơi chuyển";
        formPhieuChuyenObj.dataFieldNoiChuyen = "maKhoGiao";
        formPhieuChuyenObj.dataFieldNoiNhan = "maKhoNhan";
        break;
      }
      case 55: {
        formPhieuChuyenObj.titleForm = "Chuyển hủy sử dụng vật tư tiêu hao khoa phòng, CLS";
        formPhieuChuyenObj.titleNoiNhan = "Nơi yêu cầu";
        formPhieuChuyenObj.titleNoiChuyen = "Nơi chuyển";
        formPhieuChuyenObj.dataFieldNoiChuyen = "maKhoGiao";
        formPhieuChuyenObj.dataFieldNoiNhan = "maKhoNhan";
        break;
      }
      default: {
        formPhieuChuyenObj.titleForm = "Chuyển kho";
        formPhieuChuyenObj.titleNoiNhan = "Nơi yêu cầu";
        formPhieuChuyenObj.titleNoiChuyen = "Nơi chuyển";
        formPhieuChuyenObj.dataFieldNoiChuyen = "maKhoGiao";
        formPhieuChuyenObj.dataFieldNoiNhan = "maKhoNhan";
      }
    }
    return formPhieuChuyenObj;
  };
  setEnabled = (btnThem, btnSua, btnLuu, btnHuy, btnXoa, formPhieuChuyen) => {
    this.setState({
      enable: {
        btnThem, btnSua, btnLuu, btnHuy, btnXoa, formPhieuChuyen
      }
    })
  };
  getPhieuChuyenKhoObj = () => {
    return this.state.phieuChuyenKhoObj;
  };
  getState = () => {
    return this.state;
  };

  render() {
    return (
      <div>
        <form action={''} onSubmit={this.handleOnClickButtonThem} className={'row-margin-top-5'}>
          <Form
            formData={this.state.phieuChuyenKhoObj}
            validationGroup={"formPhieuChuyenKho"}
            labelLocation={"left"}
            onFieldDataChanged={this.handleOnChangeFormPhieuChuyen}
          >
            <GroupItem colCount={2}>
              <SimpleItem
                dataField={'soPhieuChuyen'}
                editorType={'dxTextBox'}
                editorOptions={{
                  readOnly: true
                }}
              >
                <Label text="Phiếu yêu cầu" />
              </SimpleItem>
              <SimpleItem
                dataField={'soLuuTru'}
                editorType={'dxTextBox'}
                editorOptions={{
                  readOnly: true
                }}
              >
                <Label text="Số lưu trữ" />
              </SimpleItem>
              <SimpleItem
                dataField={'maNghiepVuChuyen'}
                editorType={'dxSelectBox'}
                editorOptions={{
                  dataSource: this.props.listDanhSachNghiepVu,
                  displayExpr: 'MOTA_NGHIEPVU',
                  valueExpr: 'MA_NGHIEP_VU',
                  placeholder: 'Chọn nghiệp vụ chuyển',
                  readOnly: !this.state.enable.formPhieuChuyen || this.state.phieuChuyenKhoObj.idChuyenKhoVatTu || [34, 35, 37, 50].includes(this.props.maNghiepVu)
                }}
              >
                <Label text="Nghiệp vụ chuyển" />
                <RequiredRule message="Chưa chọn nghiệp vụ chuyển" />
              </SimpleItem>
              <SimpleItem
                dataField={'ngayPhieuChuyen'}
                editorType={'dxDateBox'}
                editorOptions={{
                  defaultValue: new Date(),
                  type: "date",
                  displayFormat: "dd/MM/yyyy",
                  readOnly: !this.state.enable.formPhieuChuyen || this.state.phieuChuyenKhoObj.idChuyenKhoVatTu,
                  openOnFieldClick: true
                }}
              >
                <Label text="Ngày lập phiếu" />
                <RequiredRule message="Chưa chọn ngày lập phiếu" />
              </SimpleItem>
              <SimpleItem
                dataField={this.changeTitleByNghiepVu().dataFieldNoiNhan}
                editorType={'dxSelectBox'}
                editorOptions={{
                  dataSource: this.props.listDanhSachKhoNhan,
                  displayExpr: 'TENKHOVATTU',
                  valueExpr: 'MAKHOVATTU',
                  placeholder: 'Chọn ' + this.changeTitleByNghiepVu().titleNoiNhan.toLowerCase(),
                  readOnly: !this.state.enable.formPhieuChuyen || this.state.phieuChuyenKhoObj.idChuyenKhoVatTu
                }}
              >
                <Label text={this.changeTitleByNghiepVu().titleNoiNhan} />
                <RequiredRule message={"Chưa chọn " + this.changeTitleByNghiepVu().titleNoiNhan.toLowerCase()} />
              </SimpleItem>
              <SimpleItem
                dataField={this.changeTitleByNghiepVu().dataFieldNoiChuyen}
                editorType={'dxSelectBox'}
                editorOptions={{
                  dataSource: this.props.listDanhSachKhoChuyen,
                  displayExpr: 'TENKHOVATTU',
                  valueExpr: 'MAKHOVATTU',
                  placeholder: 'Chọn ' + this.changeTitleByNghiepVu().titleNoiChuyen.toLowerCase(),
                  readOnly: !this.state.enable.formPhieuChuyen || this.state.phieuChuyenKhoObj.idChuyenKhoVatTu
                }}
              >
                <Label text={this.changeTitleByNghiepVu().titleNoiChuyen} />
                <RequiredRule message={"Chưa chọn " + this.changeTitleByNghiepVu().titleNoiChuyen.toLowerCase()} />
              </SimpleItem>
              <SimpleItem
                dataField={'ngayChuyen'}
                editorType={'dxDateBox'}
                editorOptions={{
                  defaultValue: new Date(),
                  type: "date",
                  displayFormat: "dd/MM/yyyy",
                  readOnly: !this.state.enable.formPhieuChuyen || this.props.enableForm,
                  openOnFieldClick: true
                }}
              >
                <Label text="Ngày chuyển" />
                <RequiredRule message="Chưa chọn ngày chuyển" />
              </SimpleItem>
              <SimpleItem
                render={() => {
                  return <div style={{ paddingTop: 10}}>(là ngày hiển thị trong báo cáo chuyển kho)</div>
                }}
              />
              <SimpleItem
                colSpan={2}
                dataField={'ghiChu'}
                editorType={'dxTextBox'}
                editorOptions={{
                  placeholder: "Nhập diễn giải",
                  readOnly: !this.state.enable.formPhieuChuyen
                }}
              >
                <Label text="Diễn giải" />
              </SimpleItem>
              {
                this.state.phieuChuyenKhoObj.maNghiepVuChuyen === 55 ? (
                  <SimpleItem
                    dataField={'xuatHuy'}
                    editorType={'dxSelectBox'}
                    editorOptions={{
                      dataSource: dataXuatHuy,
                      displayExpr: 'tenXuatHuy',
                      valueExpr: 'maXuatHuy',
                      placeholder: 'Chọn loại xuất hủy',
                      readOnly: !this.state.enable.formPhieuChuyen,
                      defaultValue: 0
                    }}
                  >
                    <Label text='Loại xuất' />
                  </SimpleItem>
                ) : (
                  <SimpleItem/>
                )
              }
            </GroupItem>
            <GroupItem
              colCount={5}
              colSpan={2}
            >
              <SimpleItem
                colSpan={5}
                render={() => {
                  return <div className={'div-button'}>
                    <Button
                      icon={'plus'}
                      validationGroup={'formPhieuChuyenKho'}
                      useSubmitBehavior={true}
                      width={120}
                      type={"default"}
                      text={'Thêm'}
                      disabled={!this.state.enable.btnThem}
                      className={'btn-margin-5'}
                    />
                    <Button
                      icon={'edit'}
                      width={120}
                      type={"default"}
                      text={'Sửa'}
                      disabled={!this.state.enable.btnSua}
                      className={'btn-margin-5'}
                      onClick={this.handleOnClickButtonSua}
                    />
                    <Button
                      icon={'save'}
                      width={120}
                      type={"default"}
                      text={'Lưu'}
                      disabled={!this.state.enable.btnLuu}
                      className={'btn-margin-5'}
                      onClick={this.handleOnClickButtonLuu}
                    />
                    <Button
                      width={120}
                      icon={'close'}
                      type={"default"}
                      text={'Hủy'}
                      disabled={!this.state.enable.btnHuy}
                      className={'btn-margin-5'}
                      onClick={this.handleOnClickButtonHuy}
                    />
                    <Button
                      icon={'clear'}
                      width={120}
                      type={"danger"}
                      text={'Xóa'}
                      disabled={!this.state.enable.btnXoa}
                      className={'btn-margin-5'}
                      onClick={this.handleOnClickButtonXoa}
                    />
                    <DuocDropDownButton
                      icon={'print'}
                      loadingConfigs={this.props.loadingConfigs}
                      disabled={this.state.phieuChuyenKhoObj.idChuyenKhoVatTu === null}
                      text={'In phiếu chuyển'}
                      arrayButton={[
                        { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInPhieuChuyenKho('pdf')},
                        { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInPhieuChuyenKho('xls')},
                        { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInPhieuChuyenKho('rtf')}
                      ]}
                      defaultHandleOnClick={() => this.handleOnClickButtonInPhieuChuyenKho('pdf')}
                      width={150}
                      className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
                    />
                    <DuocDropDownButton
                      icon={'print'}
                      loadingConfigs={this.props.loadingConfigs}
                      disabled={this.state.phieuChuyenKhoObj.idChuyenKhoVatTu === null}
                      text={'In phiếu xuất (đứng)'}
                      arrayButton={[
                        { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInPhieuXuatKho('pdf', 0)},
                        { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInPhieuXuatKho('xls', 0)},
                        { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInPhieuXuatKho('rtf', 0)}
                      ]}
                      defaultHandleOnClick={() => this.handleOnClickButtonInPhieuXuatKho('pdf', 0)}
                      width={200}
                      className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
                    />
                    <DuocDropDownButton
                      icon={'print'}
                      loadingConfigs={this.props.loadingConfigs}
                      disabled={this.state.phieuChuyenKhoObj.idChuyenKhoVatTu === null}
                      text={'In phiếu xuất (ngang)'}
                      arrayButton={[
                        { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInPhieuXuatKho('pdf', 1)},
                        { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInPhieuXuatKho('xls', 1)},
                        { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInPhieuXuatKho('rtf', 1)}
                      ]}
                      defaultHandleOnClick={() => this.handleOnClickButtonInPhieuXuatKho('pdf', 1)}
                      width={200}
                      className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
                    />
                    <DuocDropDownButton
                      icon={'print'}
                      loadingConfigs={this.props.loadingConfigs}
                      disabled={this.state.phieuChuyenKhoObj.idChuyenKhoVatTu === null}
                      text={'In BB thanh lý'}
                      arrayButton={[
                        { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInBienBanThanhLy('pdf', 0)},
                        { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInBienBanThanhLy('xls', 0)},
                        { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInBienBanThanhLy('rtf', 0)}
                      ]}
                      defaultHandleOnClick={() => this.handleOnClickButtonInBienBanThanhLy('pdf', 0)}
                      width={200}
                      className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
                    />
                    <DuocDropDownButton
                      icon={'print'}
                      loadingConfigs={this.props.loadingConfigs}
                      disabled={this.state.phieuChuyenKhoObj.idChuyenKhoVatTu === null}
                      text={'In BB mất/hỏng/vỡ'}
                      arrayButton={[
                        { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInBienBanThanhLy('pdf', 1)},
                        { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInBienBanThanhLy('xls', 1)},
                        { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInBienBanThanhLy('rtf', 1)}
                      ]}
                      defaultHandleOnClick={() => this.handleOnClickButtonInBienBanThanhLy('pdf', 1)}
                      width={200}
                      className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
                    />
                    <DuocDropDownButton
                      icon={'print'}
                      loadingConfigs={this.props.loadingConfigs}
                      disabled={this.state.phieuChuyenKhoObj.idChuyenKhoVatTu === null}
                      text={'In phiếu dự trù'}
                      arrayButton={[
                        { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleChangePopUpState(0,true, 'pdf')},
                        { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleChangePopUpState(0,true, 'xls')},
                        { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleChangePopUpState(0,true, 'rtf')}
                      ]}
                      defaultHandleOnClick={() => this.handleChangePopUpState(0,true, 'pdf')}
                      width={200}
                      className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
                      visible={this.props.maNghiepVu === 34}
                    />
                    <DuocDropDownButton
                      icon={'print'}
                      loadingConfigs={this.props.loadingConfigs}
                      disabled={this.state.phieuChuyenKhoObj.idChuyenKhoVatTu === null}
                      text={'In phiếu bổ sung'}
                      arrayButton={[
                        { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleChangePopUpState(1, true, 'pdf')},
                        { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleChangePopUpState(1, true, 'xls')},
                        { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleChangePopUpState(1, true, 'rtf')}
                      ]}
                      defaultHandleOnClick={() => this.handleChangePopUpState(1, true, 'pdf')}
                      width={200}
                      className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
                      visible={this.props.maNghiepVu === 34}
                    />
                    <DuocDropDownButton
                      icon={'print'}
                      loadingConfigs={this.props.loadingConfigs}
                      disabled={this.state.phieuChuyenKhoObj.idChuyenKhoVatTu === null}
                      text={'In phiếu định mức'}
                      arrayButton={[
                        { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInPhieuDuTruDinhMuc('pdf')},
                        { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInPhieuDuTruDinhMuc('xls')},
                        { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInPhieuDuTruDinhMuc('rtf')}
                      ]}
                      defaultHandleOnClick={() => this.handleOnClickButtonInPhieuDuTruDinhMuc('pdf')}
                      width={200}
                      className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
                      visible={this.props.maNghiepVu === 34}
                    />
                    <DuocDropDownButton
                      icon={'print'}
                      loadingConfigs={this.props.loadingConfigs}
                      disabled={this.state.phieuChuyenKhoObj.idChuyenKhoVatTu === null}
                      text={'In phiếu hoàn trả'}
                      arrayButton={[
                        { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleChangePopUpState(2, true, 'pdf')},
                        { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleChangePopUpState(2, true, 'xls')},
                        { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleChangePopUpState(2, true, 'rtf')}
                      ]}
                      defaultHandleOnClick={() => this.handleChangePopUpState(2, true, 'pdf')}
                      width={200}
                      className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
                      visible={this.props.maNghiepVu === 35}
                    />
                  </div>
                }}
              >
              </SimpleItem>
            </GroupItem>
          </Form>
        </form>
    </div>
    );
  }
}

export default FormPhieuChuyenKho;
