import axios from 'axios';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING } from '../../configs';
import notify from 'devextreme/ui/notify';
import { printType1 } from 'app/modules/duoc/duoc-components/utils/print-helper';
import { catchStatusCodeFromResponse } from 'app/modules/duoc/duoc-components/utils/funtions';

export const ACTION_TYPES = {
  GET_DANH_SACH_PHIEU_DU_TRU: 'tonghopdutru/GET_DANH_SACH_PHIEU_DU_TRU',
  GET_DANH_SACH_CHUA_DU_TRU: 'tonghopdutru/GET_DANH_SACH_CHUA_DU_TRU',
  GET_DANH_SACH_VAT_TU_DU_TRU: 'tonghopdutru/GET_DANH_SACH_VAT_TU_DU_TRU',
  GET_DANH_SACH_BENH_NHAN_DU_TRU: 'tonghopdutru/GET_DANH_SACH_BENH_NHAN_DU_TRU',
  TAO_PHIEU_DU_TRU: 'tonghopdutru/TAO_PHIEU_DU_TRU',
  DELETE_PHIEU_DU_TRU: 'tonghopdutru/DELETE_PHIEU_DU_TRU',
  GUI_PHIEU_DU_TRU_LEN_KHOA: 'tonghopdutru/GUI_PHIEU_DU_TRU_LEN_KHOA',
  RESET_DANH_SACH_VAT_TU: 'tonghopdutru/RESET_DANH_SACH_VAT_TU',
  RESET_DANH_SACH_BENH_NHAN: 'tonghopdutru/RESET_DANH_SACH_BENH_NHAN'
};

const initialState = {
  listDanhSachPhieuDuTru: [],
  listDanhSachChuaDuTru: [],
  listDanhSachVatTuDuTru: [],
  listDanhSachBenhNhanDuTru: []
};

export type tongHopDuTruState = Readonly<typeof initialState>;

// Reducer
// eslint-disable-next-line complexity
export default (state: tongHopDuTruState = initialState, action): tongHopDuTruState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_PHIEU_DU_TRU):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_CHUA_DU_TRU):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_VAT_TU_DU_TRU):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_BENH_NHAN_DU_TRU):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.DELETE_PHIEU_DU_TRU):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_PHIEU_DU_TRU):
      return {
        ...state,
        listDanhSachPhieuDuTru: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_CHUA_DU_TRU):
      return {
        ...state,
        listDanhSachChuaDuTru: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_VAT_TU_DU_TRU):
      return {
        ...state,
        listDanhSachVatTuDuTru: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_BENH_NHAN_DU_TRU):
      return {
        ...state,
        listDanhSachBenhNhanDuTru: []
      };
    case FAILURE(ACTION_TYPES.DELETE_PHIEU_DU_TRU):
      return {
        ...state,
        listDanhSachBenhNhanDuTru: []
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_PHIEU_DU_TRU):
      return {
        ...state,
        listDanhSachPhieuDuTru: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_CHUA_DU_TRU):
      return {
        ...state,
        listDanhSachChuaDuTru: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_VAT_TU_DU_TRU):
      return {
        ...state,
        listDanhSachVatTuDuTru: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_BENH_NHAN_DU_TRU):
      return {
        ...state,
        listDanhSachBenhNhanDuTru: action.payload.data
      };
    case ACTION_TYPES.RESET_DANH_SACH_VAT_TU:
      return {
        ...state,
        listDanhSachVatTuDuTru: []
      };
    case ACTION_TYPES.RESET_DANH_SACH_BENH_NHAN:
      return {
        ...state,
        listDanhSachBenhNhanDuTru: []
      };
    default:
      return state;
  }
};
// Actions
export const resetDanhSachVatTu = () => {
  return {
    type: ACTION_TYPES.RESET_DANH_SACH_VAT_TU
  };
};
export const resetDanhSachBenhNhan = () => {
  return {
    type: ACTION_TYPES.RESET_DANH_SACH_BENH_NHAN
  };
};

export const getDanhSachPhieuDuTru = (
  dvtt: string,
  tuNgay: string,
  denNgay: string,
  trangThai: number,
  bant: number,
  maPhongBan: string,
  loaiPhieu: number // 0: Dự trù   1: Hoàn trả
) => async dispatch => {
  const requestURL = `${apiURL}api/tonghopdutru/getDanhSachPhieuDuTru`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_PHIEU_DU_TRU,
    payload: axios
      .get(requestURL, {
        params: {
          dvtt,
          tuNgay,
          denNgay,
          trangThai,
          bant,
          maPhongBan,
          loaiPhieu
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(
          obj.response.status,
          loaiPhieu === 0 ? 'Lỗi tải danh sách phiếu dự trù nội trú' : 'Lỗi tải danh sách phiếu hoàn trả nội trú'
        );
      })
  });
  return result;
};

export const getDanhSachPhieuChuaDuTru = (
  dvtt: string,
  tuNgay: string,
  denNgay: string,
  trangThai: number,
  bant: number,
  maPhongBan: string,
  loaiPhieu: number // 0: Phiếu dự trù     1: Phiếu hoàn trả
) => async dispatch => {
  const requestURL = `${apiURL}api/tonghopdutru/getDanhSachPhieuChuaDuTru`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_CHUA_DU_TRU,
    payload: axios
      .get(requestURL, {
        params: {
          dvtt,
          tuNgay,
          denNgay,
          trangThai,
          bant,
          maPhongBan,
          loaiPhieu
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(
          obj.response.status,
          loaiPhieu === 0 ? 'Lỗi tải danh sách phiếu điều trị chưa dự trù' : 'Lỗi tải danh sách phiếu chưa hoàn trả'
        );
      })
  });
  return result;
};

export const getDanhSachVatTuPhieuDutru = (
  dvtt: string,
  idPhieu: number,
  maPhongBan: string,
  hinhThucXem: number,
  thoiGian: number,
  loaiPhieu: number // 0: Phiếu dự trù     1: Phiếu hoàn trả
) => async dispatch => {
  const requestURL = `${apiURL}api/tonghopdutru/getDanhSachVatTuPhieuDutru`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_VAT_TU_DU_TRU,
    payload: axios
      .get(requestURL, {
        params: {
          dvtt,
          idPhieu,
          maPhongBan,
          hinhThucXem,
          thoiGian,
          loaiPhieu
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(
          obj.response.status,
          loaiPhieu === 0 ? 'Lỗi tải danh sách vật tư dự trù' : 'Lỗi tải danh sách vật tư hoàn trả'
        );
      })
  });
  return result;
};

export const getDanhSachBenhNhanPhieuDuTru = (
  dvtt: string,
  idPhieu: number,
  ngayLap: string,
  maPhongBan: string,
  loaiPhieu: number // 0: Phiếu dự trù     1: Phiếu hoàn trả
) => async dispatch => {
  const requestURL = `${apiURL}api/tonghopdutru/getDanhSachBenhNhanPhieuDuTru`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_BENH_NHAN_DU_TRU,
    payload: axios
      .get(requestURL, {
        params: {
          dvtt,
          idPhieu,
          ngayLap,
          maPhongBan,
          loaiPhieu
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(
          obj.response.status,
          loaiPhieu === 0 ? 'Lỗi tải danh sách bệnh nhân dự trù' : 'Lỗi tải danh sách bệnh nhân hoàn trả'
        );
      })
  });
  return result;
};

export const tongHopDuTruTheoPhieu = (
  phieuDuTru: any,
  loaiPhieu: number, // 0: Phiếu dự trù     1: Phiếu hoàn trả
  handleReload: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/tonghopdutru/tongHopDuTruTheoPhieu`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.TAO_PHIEU_DU_TRU,
    payload: axios
      .post(requestUrl, JSON.stringify(phieuDuTru), {
        params: { loaiPhieu },
        headers: { 'content-type': 'application/json' }
      })
      .then(rs => {
        if (rs.data.SAISOT === 0) {
          notify({ message: 'Tạo phiếu thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else if (rs.data.SAISOT === 1) {
          notify({ message: 'Không có dữ liệu để tạo phiếu', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else if (rs.data.SAISOT === 100) {
          notify({ message: 'Đã chốt số liệu. Không thể tạo phiếu', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, loaiPhieu === 0 ? 'Lỗi tạo phiếu dự trù' : 'Lỗi tạo phiếu hoàn trả');
      })
  });
  handleReload();
  return reuslt;
};

export const tongHopDuTruTheoNgay = (
  phieuDuTru: any,
  loaiPhieu: number, // 0: Phiếu dự trù     1: Phiếu hoàn trả
  handleReload: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/tonghopdutru/tongHopDuTruTheoNgay`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.TAO_PHIEU_DU_TRU,
    payload: axios
      .post(requestUrl, JSON.stringify(phieuDuTru), {
        params: { loaiPhieu },
        headers: { 'content-type': 'application/json' }
      })
      .then(rs => {
        if (rs.data.SAISOT === 0) {
          notify({ message: 'Tạo phiếu thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else if (rs.data.SAISOT === 1) {
          notify({ message: 'Không có dữ liệu để tạo phiếu', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else if (rs.data.SAISOT === 100) {
          notify({ message: 'Đã chốt số liệu. Không thể tạo phiếu', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, loaiPhieu === 0 ? 'Lỗi tạo phiếu dự trù' : 'Lỗi tạo phiếu hoàn trả');
      })
  });
  handleReload();
  return reuslt;
};

export const deletePhieuDuTru = (
  phieuDuTru: any,
  loaiPhieu: number, // 0: Phiếu dự trù     1: Phiếu hoàn trả
  handleReload: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/tonghopdutru/deletePhieuDuTru`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.DELETE_PHIEU_DU_TRU,
    payload: axios
      .post(requestUrl, JSON.stringify(phieuDuTru), {
        params: { loaiPhieu },
        headers: { 'content-type': 'application/json' }
      })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Xóa phiếu thành công', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
          dispatch(resetDanhSachVatTu());
          dispatch(resetDanhSachBenhNhan());
          handleReload();
        } else if (rs.data === 1) {
          notify({ message: 'Phiếu đã được gửi. Không thể xóa', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, loaiPhieu === 0 ? 'Lỗi xóa phiếu dự trù' : 'Lỗi xóa phiếu hoàn trả');
      })
  });
  return reuslt;
};

export const guiPhieuDuTruLenKhoaDuoc = (
  phieuDuTru: any,
  trangThaiPhieu: number,
  loaiPhieu: number, // 0: Phiếu dự trù     1: Phiếu hoàn trả
  handleReload: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/tonghopdutru/guiPhieuDuTruLenKhoaDuoc`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.GUI_PHIEU_DU_TRU_LEN_KHOA,
    payload: axios
      .post(requestUrl, JSON.stringify(phieuDuTru), {
        params: {
          trangThaiPhieu,
          loaiPhieu
        },
        headers: { 'content-type': 'application/json' }
      })
      .then(rs => {
        if (rs.data === 0) {
          notify(
            {
              message: trangThaiPhieu !== 1 ? 'Gửi phiếu lên khoa dược thành công!' : 'Hủy gửi phiếu lên khoa dược thành công!',
              width: NOTIFY_WIDTH,
              shading: SHADING
            },
            'success',
            NOTIFY_DISPLAYTIME
          );
          dispatch(resetDanhSachVatTu());
          dispatch(resetDanhSachBenhNhan());
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi gửi phiếu lên khoa dược');
      })
  });
  handleReload();
  return reuslt;
};

export const printPhieuHoanTraThuoc = (
  dvtt: string,
  isBANT: string,
  idPhieu: string,
  tenTinh: string,
  tenBenhVien: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/tonghopdutru/printPhieuHoanTraThuoc`;
  const params = {
    dvtt,
    isBANT,
    idPhieu,
    tenTinh,
    tenBenhVien,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In phiếu hoàn trả thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};
