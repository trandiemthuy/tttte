import React from "react";
import { DateBox, Button as DateBoxButton } from 'devextreme-react/date-box';
import {
  convertToDate1,
  convertToSQLDate,
  getFirstDate
} from "app/modules/duoc/duoc-components/utils/funtions";
export interface IAdvanceDateBoxProp {
  getValue?: Function;
  readonly?: boolean;
  className?: any;
  width?: string | number;
  maxDay?: any;
  isDauThang?: boolean;
}
export interface IAdvanceDateBoxState {
  dateValue: any;
}

export class AdvanceDateBox extends React.Component<IAdvanceDateBoxProp, IAdvanceDateBoxState> {
  constructor(props) {
    super(props);
    this.state = {
      dateValue: this.props.isDauThang ? getFirstDate() : new Date()
    }
  }

  handleOnClickToday = (e) => {
    this.setState({
      dateValue: new Date()
    });
  };
  handleOnClickPrevday = (e) => {
    const date = new Date(this.state.dateValue);
    const date1 = new Date(this.state.dateValue);
    date1.setDate(date.getDate() - 1);
    this.setState({
      dateValue: date1
    });
  };
  handleOnClickNextday = (e) => {
    const date = new Date(this.state.dateValue);
    const date1 = new Date(this.state.dateValue);
    date1.setDate(date.getDate() + 1);
    this.setState({
      dateValue: date1
    });
  };
  handleOnChangeValue = (e) => {
    this.setState({
      dateValue: e.value
    });
  };
  getValue = () => {
    return convertToSQLDate(this.state.dateValue);
  };
  setDateValue =(value: string) => {
    this.setState({
      dateValue: value
    });
};

  render() {
    return (
      <div>
        <DateBox
          className={this.props.className === undefined ? '' : this.props.className}
          displayFormat={'dd/MM/yyyy'}
          value={this.state.dateValue}
          stylingMode="outlined"
          onValueChanged={this.handleOnChangeValue}
          openOnFieldClick={true}
          readOnly={this.props.readonly !== undefined ? this.props.readonly : false}
          width={this.props.width === undefined ? 'auto' : this.props.width}
          max={this.props.maxDay === undefined ? null : this.props.maxDay}
        >
          <DateBoxButton
            name="today"
            location="before"
            options={{
              text: 'Hôm nay',
              type: 'default',
              onClick: this.handleOnClickToday
            }}
          />
          <DateBoxButton
            name="prevDate"
            location="before"
            options={{
              icon: 'spinprev',
              type: 'default',
              stylingMode: 'text',
              onClick: this.handleOnClickPrevday
            }}
          />
          <DateBoxButton
            name="nextDate"
            location="after"
            options={{
              icon: 'spinnext',
              type: 'default',
              stylingMode: 'text',
              onClick: this.handleOnClickNextday
            }}
          />
          <DateBoxButton name="dropDown" />
        </DateBox>
      </div>
    );
  }
}

export default AdvanceDateBox;
