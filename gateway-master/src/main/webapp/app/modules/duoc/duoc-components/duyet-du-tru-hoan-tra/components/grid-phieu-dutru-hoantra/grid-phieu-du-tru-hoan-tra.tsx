import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection, ColumnFixing, Pager, Paging
} from 'devextreme-react/data-grid';
import {configLoadPanel, pageSizes} from '../../../../configs';
import {defaultPhieuDuTruModel} from "app/modules/duoc/duoc-components/tong-hop-du-tru/phieu-du-tru.model";

export interface IGridDanhSachPhieuDuTruHoanTraProp extends IDataGridOptions {
  listDanhSachPhieuDuTruHoanTra: any;
  handleOnContextMenuGridDanhSach: any;
  loaiPhieu: number; // 0: Dự trù nội trú     1: Hoàn trả nội trú
}
export interface IGridDanhSachPhieuDuTruHoanTraState {
  isSelect: boolean;
}

export class GridDanhSachPhieuDuTruHoanTra extends React.Component<IGridDanhSachPhieuDuTruHoanTraProp, IGridDanhSachPhieuDuTruHoanTraState> {
  constructor(props) {
    super(props);
    this.state = {
      isSelect: false
    }
  }

  handleOnRowClick = (e) => {
    if (!this.state.isSelect) {
      const keys = e.component.getSelectedRowKeys();
      e.component.deselectRows(keys);
    }
    this.setState({
      isSelect: false
    });
  };

  render() {
    return (
      <DataGrid
        loadPanel={configLoadPanel}
        selection={{ mode: 'multiple' }}
        dataSource={this.props.listDanhSachPhieuDuTruHoanTra}
        showBorders={true}
        showColumnLines={true}
        rowAlternationEnabled={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        columnMinWidth={50}
        onContextMenuPreparing={this.props.handleOnContextMenuGridDanhSach}
        noDataText={'Không có dữ liệu'}
        wordWrapEnabled={true}
        onRowClick={this.handleOnRowClick}
        {...this.props}
      >
        <FilterRow visible={true}
                   applyFilter={'auto'} />
        <HeaderFilter visible={true} />
        <Column
          dataField={this.props.loaiPhieu === 0 ? "ID_PHIEU_DU_TRU_TONG" : "ID_PHIEU_HOAN_TRA_TONG"}
          caption="ID phiếu"
          dataType="string"
          alignment="left"
          visible={false}
        />
        <Column
          dataField="NGAY"
          caption={this.props.loaiPhieu === 0 ? "Ngày dự trù" : "Ngày hoàn trả"}
          dataType="string"
          alignment="left"
          customizeText={(data) => {
            return data.value && data.value.split('-').join('/');
          }}
        />
        <Column
          dataField="TEN_PHONGBAN"
          caption={this.props.loaiPhieu === 0 ? "Nơi yêu cầu" : "Nơi hoàn trả"}
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="THOIGIAN"
          caption="Thời gian"
          dataType="string"
          alignment="left"
          visible={this.props.loaiPhieu === 1}
        />
        <Column
          dataField="SOPHIEUTAO_LUUTRU"
          caption="Số phiếu"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="NGUOIDUYET"
          caption="Người duyệt"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="NGAYGIODUYET"
          caption="Ngày duyệt"
          dataType="string"
          alignment="left"
          customizeText={(data) => {
            return data.value && data.value.split('-').join('/');
          }}
        />
        <Column
          dataField="GHICHU"
          caption="Ghi chú"
          dataType="string"
          alignment="left"
        />
        <Summary>
          <TotalItem
            displayFormat={'{0} phiếu'}
            showInColumn={'NGAY'}
            column={this.props.loaiPhieu === 0 ? "ID_PHIEU_DU_TRU_TONG" : "ID_PHIEU_HOAN_TRA_TONG"}
            summaryType="count"
          />
        </Summary>
        <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
        <Paging defaultPageSize={5} />
      </DataGrid>);
  }
}

export default GridDanhSachPhieuDuTruHoanTra;
