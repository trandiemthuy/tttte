export interface IXuatDuocModel {
  dvtt?: string;
  maNghiepVu?: number;
  soPhieuTT?: string;
  tenBenhNhan?: string;
  ngayXuat?: string;
  tuoi?: number;
  namSinh?: number;
  thoiGianXuatThuoc?: string;
  phanTramBaoHiem?: number;
  ngayHoanTatKham?: string;
  chanDoan?: string; // icd
  soVaoVien?: number;
  maToaThuoc?: string;
  maKhamBenh?: string;
  maBenhNhan?: number;
  vuotCan?: number; // 0: Không vượt cận    1: Vượt cận
  daThanhToan?: number; // 0: Chưa thanh toán    1: Đã thanh toán
  doiTuong?: string;
  dungTuyen?: number;
  ngayKhamBenh?: string;
  maKhoa?: string;
  maPhong?: string;
  maNguoiXuat?: number;
}
export const defaultXuatDuocModel: IXuatDuocModel = {
  dvtt: null,
  maNghiepVu: null,
  soPhieuTT: null,
  tenBenhNhan: null,
  ngayXuat: null,
  tuoi: null,
  namSinh: null,
  thoiGianXuatThuoc: null,
  phanTramBaoHiem: null,
  ngayHoanTatKham: null,
  chanDoan: null,
  soVaoVien: null,
  maToaThuoc: null,
  maKhamBenh: null,
  maBenhNhan: null,
  vuotCan: null, // 0: Không vượt cận    1: Vượt cận
  daThanhToan: null, // 0: Chưa thanh toán    1: Đã thanh toán
  doiTuong: null,
  dungTuyen: null,
  ngayKhamBenh: null,
  maKhoa: null,
  maPhong: null,
  maNguoiXuat: null
};
