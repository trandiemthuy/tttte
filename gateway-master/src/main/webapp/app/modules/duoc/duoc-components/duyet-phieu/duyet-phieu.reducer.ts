import axios from 'axios';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING } from '../../configs';
import notify from 'devextreme/ui/notify';
import { exportXLSType1, printType1 } from 'app/modules/duoc/duoc-components/utils/print-helper';
import { catchStatusCodeFromResponse } from 'app/modules/duoc/duoc-components/utils/funtions';

export const ACTION_TYPES = {
  GET_DANH_SACH_KHO_CHUYEN_DUYET: 'duyetphieu/GET_DANH_SACH_KHO_CHUYEN_DUYET',
  GET_DANH_SACH_PHIEU_CHUYEN_DUYET: 'duyetphieu/GET_DANH_SACH_PHIEU_CHUYEN_DUYET',
  DUYET_PHIEU_CHUYEN_KHO: 'duyetphieu/DUYET_PHIEU_CHUYEN_KHO',
  HUY_DUYET_PHIEU_CHUYEN_KHO: 'duyetphieu/HUY_DUYET_PHIEU_CHUYEN_KHO'
};

const initialState = {
  listDanhSachKhoChuyenDuyet: [],
  listDanhSachPhieuChuyenKhoDuyet: []
};

export type duyetPhieuState = Readonly<typeof initialState>;

// Reducer
// eslint-disable-next-line complexity
export default (state: duyetPhieuState = initialState, action): duyetPhieuState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_PHIEU_CHUYEN_DUYET):
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_KHO_CHUYEN_DUYET):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_PHIEU_CHUYEN_DUYET):
      return {
        ...state,
        listDanhSachPhieuChuyenKhoDuyet: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_KHO_CHUYEN_DUYET):
      return {
        ...state,
        listDanhSachKhoChuyenDuyet: []
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_PHIEU_CHUYEN_DUYET):
      return {
        ...state,
        listDanhSachPhieuChuyenKhoDuyet: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_KHO_CHUYEN_DUYET):
      return {
        ...state,
        listDanhSachKhoChuyenDuyet: action.payload.data
      };
    default:
      return state;
  }
};
// Actions
export const getDanhSachPhieuChuyenKho = (
  dvtt: string,
  tuNgay: string,
  denNgay: string,
  maNghiepVu: number,
  maKho: number,
  trangThaiPhieu: number
) => async dispatch => {
  const requestUrl = `${apiURL}api/duyetphieu/getDanhSachPhieuChuyenTheoNghiepVu`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_PHIEU_CHUYEN_DUYET,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt,
          tuNgay,
          denNgay,
          maNghiepVu,
          maKho,
          trangThaiPhieu
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách phiếu chuyển kho');
      })
  });
  return result;
};
export const getDanhSachKhoChuyenDuyet = (dvtt: string, maNhanVien: number) => async dispatch => {
  const requestUrl = `${apiURL}api/duyetphieu/getDanhSachKhoChuyenTheoNhanVien`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_KHO_CHUYEN_DUYET,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt,
          maNhanVien
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách kho chuyển');
      })
  });
  return result;
};

export const duyetPhieuChuyenKho = (
  listDanhSachChinhSua: any,
  dvtt: string,
  nguoiDuyet: number,
  maPhongBan: string,
  maKhoa: string,
  tuNgay: string,
  denNgay: string,
  maKho: number,
  maNghiepVu: number,
  trangThaiPhieu: number
) => async dispatch => {
  const requestUrl = `${apiURL}api/duyetphieu/duyetPhieuChuyenKho`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.DUYET_PHIEU_CHUYEN_KHO,
    payload: axios
      .post(requestUrl, JSON.stringify(listDanhSachChinhSua), {
        headers: { 'content-type': 'application/json' },
        params: { dvtt, nguoiDuyet, maPhongBan, maKhoa }
      })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Duyệt phiếu thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi duyệt phiếu chuyển kho');
      })
  });
  dispatch(getDanhSachPhieuChuyenKho(dvtt, tuNgay, denNgay, maNghiepVu, maKho, trangThaiPhieu));
  return reuslt;
};

export const huyDuyetPhieuChuyenKho = (
  idPhieuChuyen,
  dvtt: string,
  nguoiDuyet: number,
  maPhongBan: string,
  maKhoa: string,
  tuNgay: string,
  denNgay: string,
  maKho: number,
  maNghiepVu: number,
  trangThaiPhieu: number
) => async dispatch => {
  const requestUrl = `${apiURL}api/duyetphieu/huyDuyetChuyenKho`;
  const params = { idPhieuChuyen, dvtt, nguoiDuyet, maPhongBan, maKhoa };
  const reuslt = await dispatch({
    type: ACTION_TYPES.HUY_DUYET_PHIEU_CHUYEN_KHO,
    payload: axios
      .post(requestUrl, JSON.stringify(params), { headers: { 'content-type': 'application/json' }, params })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Hủy phiếu thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi hủy duyệt phiếu chuyển kho');
      })
  });
  dispatch(getDanhSachPhieuChuyenKho(dvtt, tuNgay, denNgay, maNghiepVu, maKho, trangThaiPhieu));
  return reuslt;
};

export const printPhieuXuatKhoDuyet = (
  dvtt: string,
  idPhieuChuyen: number,
  maNghiepVu: number,
  tenBenhVien: string,
  tenTinh: string,
  nguoiLap: string,
  mau: number,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/duyetphieu/printPhieuXuatKhoDuyet`;
  const params = {
    dvtt,
    idPhieuChuyen,
    maNghiepVu,
    tenBenhVien,
    tenTinh,
    nguoiLap,
    mau, // 0: Đứng      1: Ngang
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In phiếu xuất kho thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const exportExcelDuyetPhieu = (
  dvtt: string,
  tuNgay: string,
  denNgay: string,
  maKho: number,
  duyet: number,
  handleLoading: Function
) => {
  const url = `${apiURL}api/duyetphieu/exportExcelPhieuChuyenKho`;
  const params = {
    dvtt,
    tuNgay,
    denNgay,
    maKho,
    duyet
  };
  exportXLSType1(url, params, {
    successMessage: 'Xuất dữ liệu thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};
