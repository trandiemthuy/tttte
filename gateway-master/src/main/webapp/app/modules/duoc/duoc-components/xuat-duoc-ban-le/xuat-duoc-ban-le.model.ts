import { convertToDate, getKYearFromNow } from 'app/modules/duoc/duoc-components/utils/funtions';

export interface IXuatDuocBanLeModel {
  dvtt?: string;
  nghiepVu?: string;
  maToaThuoc?: string;
  tenKhachHang?: string;
  ngayXuat?: string;
  ghiChu?: string;
  noiXuat?: number;
  tenNoiXuat?: string;
  maBacSi?: string;
  tenBacSi?: string;
  maICD?: string;
  tenICD?: string;
  maBenhPhu?: string;
  tenBenhPhu?: string;
  maPhongBan?: string;
  maPhongBenh?: string;
  daThanhToan?: number; // 0: Chưa thanh toán     1: Đã thanh toán
  idGiaoDich?: number;
  diaChi?: string;
  tuoi?: number;
  liDoXuat?: string;
  maNhanVien?: number;
}
export const defaultXuatDuocBanLeModel: Readonly<IXuatDuocBanLeModel> = {
  dvtt: null,
  nghiepVu: null,
  maToaThuoc: null,
  tenKhachHang: null,
  ngayXuat: convertToDate(new Date().toDateString()),
  ghiChu: null,
  noiXuat: null,
  tenNoiXuat: null,
  maBacSi: '-1',
  tenBacSi: null,
  maICD: null,
  tenICD: null,
  maBenhPhu: null,
  tenBenhPhu: null,
  maPhongBan: null,
  maPhongBenh: null,
  daThanhToan: null,
  idGiaoDich: null,
  diaChi: null,
  tuoi: null,
  liDoXuat: null,
  maNhanVien: null
};
