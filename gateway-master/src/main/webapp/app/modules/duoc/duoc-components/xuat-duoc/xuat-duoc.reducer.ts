import axios from 'axios';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING } from '../../configs';
import notify from 'devextreme/ui/notify';
import { IXuatDuocModel } from 'app/modules/duoc/duoc-components/xuat-duoc/xuat-duoc.model';
import { exportXLSType1, printType1 } from 'app/modules/duoc/duoc-components/utils/print-helper';
import { catchStatusCodeFromResponse } from 'app/modules/duoc/duoc-components/utils/funtions';

export const ACTION_TYPES = {
  GET_DANH_SACH_BENH_NHAN_XUAT_DUOC: 'xuatduoc/GET_DANH_SACH_BENH_NHAN_XUAT_DUOC',
  GET_DANH_SACH_VAT_TU_XUAT_DUOC: 'xuatduoc/GET_DANH_SACH_VAT_TU_XUAT_DUOC',
  GET_DANH_SACH_VAT_TU_CHUA_XUAT_DUOC: 'xuatduoc/GET_DANH_SACH_VAT_TU_CHUA_XUAT_DUOC',
  XUAT_DUOC: 'xuatduoc/XUAT_DUOC',
  TRA_THUOC_VE_KHO: 'xuatduoc/TRA_THUOC_VE_KHO',
  RESET_DANH_SACH_VAT_TU: 'xuatduoc/RESET_DANH_SACH_VAT_TU'
};

const initialState = {
  listDanhSachBenhNhanXuatDuoc: [],
  listDanhSachVatTuXuatDuoc: [],
  listDanhSachVatTuChuaXuatDuoc: []
};

export type xuatDuocState = Readonly<typeof initialState>;

// Reducer
// eslint-disable-next-line complexity
export default (state: xuatDuocState = initialState, action): xuatDuocState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_BENH_NHAN_XUAT_DUOC):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_VAT_TU_XUAT_DUOC):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_VAT_TU_CHUA_XUAT_DUOC):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_BENH_NHAN_XUAT_DUOC):
      return {
        ...state,
        listDanhSachBenhNhanXuatDuoc: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_VAT_TU_XUAT_DUOC):
      return {
        ...state,
        listDanhSachVatTuXuatDuoc: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_VAT_TU_CHUA_XUAT_DUOC):
      return {
        ...state,
        listDanhSachVatTuChuaXuatDuoc: []
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_BENH_NHAN_XUAT_DUOC):
      return {
        ...state,
        listDanhSachBenhNhanXuatDuoc: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_VAT_TU_XUAT_DUOC):
      return {
        ...state,
        listDanhSachVatTuXuatDuoc: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_VAT_TU_CHUA_XUAT_DUOC):
      return {
        ...state,
        listDanhSachVatTuChuaXuatDuoc: action.payload.data
      };
    case ACTION_TYPES.RESET_DANH_SACH_VAT_TU:
      return {
        ...state,
        listDanhSachVatTuXuatDuoc: [],
        listDanhSachVatTuChuaXuatDuoc: []
      };
    default:
      return state;
  }
};
// Actions
export const resetDanhSachVatTu = () => {
  return {
    type: ACTION_TYPES.RESET_DANH_SACH_VAT_TU
  };
};

export const getDanhSachBenhNhanXuatDuoc = (
  dvtt: string,
  maNghiepVu: number,
  ngay: string,
  trangThai: number,
  maKhoa: string
) => async dispatch => {
  const requestURL = `${apiURL}api/xuatduoc/getDanhSachBenhNhanXuatDuoc`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_BENH_NHAN_XUAT_DUOC,
    payload: axios
      .get(requestURL, {
        params: {
          dvtt,
          maNghiepVu,
          ngay,
          trangThai,
          maKhoa
        }
      })
      .catch(obj => {
        notify(
          {
            message: 'Lỗi tải danh sách bệnh nhân lãnh dược',
            width: NOTIFY_WIDTH,
            shading: SHADING
          },
          'error',
          NOTIFY_DISPLAYTIME
        );
      })
  });
  return result;
};

export const getDanhSachVatTuXuatDuoc = (
  dvtt: string,
  maNghiepVu: number,
  maToaThuoc: string,
  trangThai: number,
  soVaoVien: number
) => async dispatch => {
  const requestURL = `${apiURL}api/xuatduoc/getDanhSachVatTuXuatDuoc`;
  const result = await dispatch({
    type: trangThai === 0 ? ACTION_TYPES.GET_DANH_SACH_VAT_TU_CHUA_XUAT_DUOC : ACTION_TYPES.GET_DANH_SACH_VAT_TU_XUAT_DUOC,
    payload: axios
      .get(requestURL, {
        params: {
          dvtt,
          maNghiepVu,
          maToaThuoc,
          trangThai,
          soVaoVien
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách vật tư xuất dược');
      })
  });
  return result;
};

export const xuatDuoc = (ttXuatduoc: IXuatDuocModel, trangThai: number, handleReload: Function) => async dispatch => {
  const requestUrl = `${apiURL}api/xuatduoc/xuatDuocVatTu`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.XUAT_DUOC,
    payload: axios
      .post(requestUrl, JSON.stringify(ttXuatduoc), {
        params: { trangThai },
        headers: { 'content-type': 'application/json' }
      })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Xuất dược thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else if (rs.data === 1 || rs.data === 2) {
          notify({ message: 'Bệnh nhân chưa thanh toán viện phí.', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi xuất dược cho bệnh nhân');
      })
  });
  handleReload();
  return reuslt;
};

export const printPhieuHoanTraThuoc = (
  dvtt: string,
  ngay: string,
  nghiepVu: string,
  tenTinh: string,
  tenBenhVien: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/xuatduoc/printDanhSachBenhNhanXuatDuoc`;
  const params = {
    dvtt,
    ngay,
    nghiepVu,
    tenTinh,
    tenBenhVien,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In danh sách bệnh nhân xuất dược thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const exportDanhSachPhieuXuat = (dvtt: string, ngayLap: string, nghiepVu: string, hinhThuc: string, handleLoading: Function) => {
  const url = `${apiURL}api/xuatduoc/exportDanhSachPhieuXuat`;
  const params = {
    dvtt,
    ngayLap,
    nghiepVu,
    hinhThuc
  };
  exportXLSType1(url, params, {
    successMessage: 'Xuất danh sách phiếu xuất thành công!',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const traThuocVeKho = (ttXuatduoc: IXuatDuocModel, nghiepVu: string, handleReload: Function) => async dispatch => {
  const requestUrl = `${apiURL}api/xuatduoc/traThuocVeKho`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.TRA_THUOC_VE_KHO,
    payload: axios
      .post(requestUrl, JSON.stringify(ttXuatduoc), {
        params: { nghiepVu },
        headers: { 'content-type': 'application/json' }
      })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Trả thuốc về kho thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else if (rs.data === 101) {
          notify(
            { message: 'Bệnh nhân đã thanh toán. Không thể trả thuốc', width: NOTIFY_WIDTH, shading: SHADING },
            'error',
            NOTIFY_DISPLAYTIME
          );
        } else if (rs.data === 102) {
          notify(
            { message: 'Bệnh nhân đã xuất dược. Không thể trả thuốc', width: NOTIFY_WIDTH, shading: SHADING },
            'error',
            NOTIFY_DISPLAYTIME
          );
        } else if (rs.data === 103) {
          notify(
            { message: 'Bệnh nhân đã trả thuốc về kho. Không thể trả 2 lần', width: NOTIFY_WIDTH, shading: SHADING },
            'error',
            NOTIFY_DISPLAYTIME
          );
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi trả thuốc về kho');
      })
  });
  handleReload();
  return reuslt;
};
