import React from 'react';
import { Switch } from 'react-router-dom';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import DuyetPhieuComponent from "./duyet-phieu";

const Routes = ({ match }) => (

  <Switch>
    <ErrorBoundaryRoute
      path={`${match.url}/:nghiepVu`}
      component={DuyetPhieuComponent}>
    </ErrorBoundaryRoute>
  </Switch>
);

export default Routes;
