import React from "react";
import Box, {Item as BoxItem} from "devextreme-react/box";
import {Tabs, Button} from "devextreme-react";
import Form, { SimpleItem, Label, RequiredRule, EmptyItem, GroupItem } from 'devextreme-react/form';
import {DuocDropDownButton} from "app/modules/duoc/duoc-components/utils/button-dropdown/button-dropdown";
import {defaultNhanVienKiemNhap, defaultPhieuNhap, INhanVienKiemNhap, IPhieuNhapKho} from "../../nhap-kho.model";
import {ISetEnable, defaultSetEnable} from "../../enable.model";
import notify from "devextreme/ui/notify";
import {NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING} from "app/modules/duoc/configs";
import dialog from "devextreme/ui/dialog";
import {getDvtt, getMaNhanVien, getMaPhongBan} from "app/modules/duoc/duoc-components/utils/thong-tin";
import {GridNhanVienKiemNhap} from '../grid-nhan-vien-kiem-nhap/grid-nhan-vien-kiem-nhap';

export interface IFormPhieuNhapKhoProp {
  maNghiepVu: number; // Mã nghiệp vụ mặc định ban đầu
  listDanhSachNguonDuoc: any;
  listDanhSachNhaCungCap: any;
  listDanhSachKhoNhap: any;
  listDanhSachNguoiNhan: any;
  listDanhSachHoiDongKiemNhap: any;
  listDanhSachNhanVienKiemNhap: any;
  enableForm: boolean;
  loadingConfigs: {
    visible: boolean;
    position: string;
  }
  addPhieuNhapKho: Function;
  updatePhieuNhapKho: Function;
  deletePhieuNhapKho: Function;
  handleOnClickButtonInPhieuNhapKho: Function;
  handleOnClickButtonInBBKN: Function;
  handleOnClickButtonInBBKNNhieuPhieu: Function;
  handleOnClickButtonXuatDM: Function;
  handleChangeEnableInput: Function;
  addNhanVienKiemNhap: Function;
  deleteNhanVienKiemNhap: Function;
  sendPhieuNhapKho: {
    phieuNhapKho: IPhieuNhapKho;
    isUpdated: number;
  }
  selectedPhieuChuyenLength?: boolean;
}
export interface IFormPhieuNhapKhoState {
  phieuNhapKhoObj: IPhieuNhapKho;
  enable: ISetEnable;
  curTab: number;
  nhanVienKiemNhapObj: INhanVienKiemNhap;
}

export class FormPhieuNhapKho extends React.Component<IFormPhieuNhapKhoProp, IFormPhieuNhapKhoState> {
  constructor(props) {
    super(props);
    this.state = {
      phieuNhapKhoObj: {...defaultPhieuNhap, maNghiepVuChuyen: props.maNghiepVu},
      enable: defaultSetEnable,
      curTab: 0,
      nhanVienKiemNhapObj: defaultNhanVienKiemNhap
    };
  }
  componentDidMount(): void {
  }

  componentDidUpdate(prevProps: Readonly<IFormPhieuNhapKhoProp>, prevState: Readonly<IFormPhieuNhapKhoState>, snapshot?: any): void {
    if (prevProps.maNghiepVu !== this.props.maNghiepVu) {
      this.setState({
        phieuNhapKhoObj: {
          ...defaultPhieuNhap,
          maNghiepVuChuyen: this.props.maNghiepVu
        }
      });
    }

    if (prevProps.sendPhieuNhapKho.isUpdated !== this.props.sendPhieuNhapKho.isUpdated && this.props.sendPhieuNhapKho.isUpdated !== -1) {
      this.setState({
        phieuNhapKhoObj: this.props.sendPhieuNhapKho.phieuNhapKho
      });
      if (this.props.sendPhieuNhapKho.isUpdated === 0) { // Nhấn double click vào grid
        this.setEnabled(false, true, false, true, true, false);
      } else if (this.props.sendPhieuNhapKho.isUpdated === 1) { // Sau khi thêm mới phiếu
        this.setEnabled(false, false, true, true, false, false);
      } else if (this.props.sendPhieuNhapKho.isUpdated === 2) { // Sau khi cập nhật (lưu, xóa) phiếu
        this.setEnabled(true, false, false, false, false, true);
      }
    }
  }

  // Handles
  handleChangeTab = (e) => {
    this.setState({
      curTab: e.itemIndex
    });
  };
  handleOnClickButtonThem = (e) => {
    e.preventDefault();
    this.setState({
      phieuNhapKhoObj: {
        ...this.state.phieuNhapKhoObj,
        maNghiepVuChuyen: this.props.maNghiepVu,
        dvtt: getDvtt(),
        maPhongBanNhap: getMaPhongBan(),
        nguoiTao: getMaNhanVien(),
        dongY:this.props.maNghiepVu === 23 ? 1 : 0,
        maDonViTao: getDvtt()
      }
    }, () => {
      this.props.addPhieuNhapKho(this.state.phieuNhapKhoObj, this.setEnabled);
    });
    this.props.handleChangeEnableInput(true);
  };
  handleOnClickButtonSua = (e) => {
    if (this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap === null) {
      notify({ message: "Chưa chọn phiếu chuyển kho", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    this.setEnabled(false, false, true, true, false, true);
    this.props.handleChangeEnableInput(true);
  };
  handleOnClickButtonLuu = (e) => {
    if (this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap === null) {
      notify({ message: "Chưa chọn phiếu chuyển kho", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    if (this.props.sendPhieuNhapKho.isUpdated === 1) {
      this.setEnabled(true, false, false, false, false, true);
      return;
    }
    dialog.confirm("Cập nhật thông tin phiếu " + "<span style='color: red'>" + this.state.phieuNhapKhoObj.soPhieuNhap + "</span>" + " ?", "Thông báo hệ thống")
      .then(dialogResult => {
        if (dialogResult) {
          this.props.updatePhieuNhapKho(this.state.phieuNhapKhoObj);
        }
      });
  };
  handleOnClickButtonHuy = (e) => {
    this.setEnabled(true, false, false, false, false, true);
    this.setState({
      phieuNhapKhoObj: {...defaultPhieuNhap,
        maNghiepVuChuyen: this.props.maNghiepVu}
    });
    this.props.handleChangeEnableInput(false);
  };
  handleOnClickButtonXoa = (e) => {
    if (this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap === null) {
      notify({ message: "Chưa chọn phiếu chuyển kho", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    dialog.confirm("Xóa phiếu " + "<span style='color: red'>" + this.state.phieuNhapKhoObj.soPhieuNhap + "</span>" + " ?", "Thông báo hệ thống")
      .then(dialogResult => {
        if (dialogResult) {
          this.props.deletePhieuNhapKho(this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap, this.state.phieuNhapKhoObj.maNghiepVuChuyen);
        }
      });
  };
  handleOnClickButtonInPhieuNhapKho = (loaiPhieu: number, loaiFile: string) => {
    this.props.handleOnClickButtonInPhieuNhapKho(loaiPhieu, loaiFile);
  };
  handleOnClickButtonInBBKN = (loaiPhieu: number, loaiFile: string) => {
    this.props.handleOnClickButtonInBBKN(loaiPhieu, loaiFile);
  };
  handleOnClickButtonInBBKNNhieuPhieu = (loaiFile: string) => {
    this.props.handleOnClickButtonInBBKNNhieuPhieu(loaiFile);
  };
  handleOnClickButtonXuatDM = () => {
    this.props.handleOnClickButtonXuatDM();
  };
  handleSelectNhanVienKiemNhap = (e) => {
    if (e.dataField === "maNhanVien") {
      this.setState({
        nhanVienKiemNhapObj: {
          ...this.state.nhanVienKiemNhapObj,
          chucDanh: this.props.listDanhSachNguoiNhan.filter(nv => nv.MA_NHANVIEN === e.value)[0].CHUCDANH,
          dvtt: getDvtt(),
          idNhapKhoTuNhaCungCap: this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap,
          tenNhanVien: this.props.listDanhSachNguoiNhan.filter(nv => nv.MA_NHANVIEN === e.value)[0].TEN_NHANVIEN,
          soPhieuNhap: this.state.phieuNhapKhoObj.soPhieuNhap
        }
      });
    }
  };
  handleOnClickButtonThemNV = (e) => {
    e.preventDefault();
    this.props.addNhanVienKiemNhap(this.state.nhanVienKiemNhapObj);
  };
  handleOnKeyDownGridNhanVienKiemNhap = (e) => {
    const isMac = window.navigator.platform.toUpperCase().includes('MAC', 0);
    if (e.event.keyCode === 46 || (e.event.keyCode === 8 && isMac)) {
      dialog.confirm("Xóa nhân viên kiểm nhập " + "<span style='color: red'>" + this.state.nhanVienKiemNhapObj.tenNhanVien + "</span>" + " ?", "Thông báo hệ thống")
        .then(dialogResult => {
          if (dialogResult) {
            this.props.deleteNhanVienKiemNhap(this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap, this.state.nhanVienKiemNhapObj.dvtt, this.state.nhanVienKiemNhapObj.maNhanVien);
          }
        });
    }
  };
  handleOnSelectRowDataGridNhanVienKiemNhap = (e) => {
    if (e.selectedRowsData.length <= 0) {
      this.setState({
        nhanVienKiemNhapObj: defaultNhanVienKiemNhap
      });
      return;
    }
    const selectedNhanVienKiemNhap = e.selectedRowsData[0];
    const mapNhanVienKiemNhapObj = {
      ...defaultNhanVienKiemNhap,
      chucDanh: selectedNhanVienKiemNhap === undefined ? "" : selectedNhanVienKiemNhap.CHUCDANH,
      dvtt: selectedNhanVienKiemNhap === undefined ? "" : selectedNhanVienKiemNhap.DVTT,
      idNhapKhoTuNhaCungCap: selectedNhanVienKiemNhap === undefined ? "" : selectedNhanVienKiemNhap.ID_NHAPKHONHACC,
      maNhanVien: selectedNhanVienKiemNhap === undefined ? "" : selectedNhanVienKiemNhap.MANHANVIEN,
      soPhieuNhap: selectedNhanVienKiemNhap === undefined ? "" : selectedNhanVienKiemNhap.SOPHIEUNHAP,
      tenNhanVien: selectedNhanVienKiemNhap === undefined ? "" : selectedNhanVienKiemNhap.TENNHANVIEN,
    };
    this.setState({
      nhanVienKiemNhapObj: mapNhanVienKiemNhapObj
    });
  };

  // Functions
  setEnabled = (btnThem, btnSua, btnLuu, btnHuy, btnXoa, formPhieuNhap) => {
    this.setState({
      enable: {
        btnThem, btnSua, btnLuu, btnHuy, btnXoa, formPhieuNhap
      }
    })
  };
  getphieuNhapKhoObj = () => {
    return this.state.phieuNhapKhoObj;
  };
  getState = () => {
    return this.state;
  };

  render() {
    return (
      <div>
        <Tabs
          dataSource={[{
            id: 0,
            text: 'Nhập kho từ nhà cung cấp'
          },
            {
              id: 1,
              text: 'Nhân viên kiểm nhập'
            }
          ]}
          selectedIndex={this.state.curTab}
          onItemClick={this.handleChangeTab}
        />
        {
          this.state.curTab === 0 ? (
            <div className={'row-margin-top-5'}>
              <form action={''} onSubmit={this.handleOnClickButtonThem}>
                <Form
                  formData={this.state.phieuNhapKhoObj}
                  validationGroup={"formPhieuNhap"}
                  labelLocation={"left"}
                >
                  <GroupItem colCount={2}>
                    <SimpleItem
                      dataField={'nguonDuoc'}
                      editorType={'dxSelectBox'}
                      editorOptions={{
                        dataSource: this.props.listDanhSachNguonDuoc,
                        displayExpr: 'TEN_NGUONDUOC',
                        valueExpr: 'MA_NGUONDUOC',
                        placeholder: 'Chọn nguồn dược',
                        defaultValue: this.props.listDanhSachNguonDuoc.length > 0 ? this.props.listDanhSachNguonDuoc[0].MA_NGUONDUOC : null,
                        readOnly: !this.state.enable.formPhieuNhap
                      }}
                    >
                      <Label text="Nguồn dược" />
                      <RequiredRule message="Chưa chọn nguồn dược" />
                    </SimpleItem>
                    <EmptyItem />
                    <SimpleItem
                      dataField={'soPhieuNhap'}
                      editorType={'dxTextBox'}
                      editorOptions={{
                        readOnly: true
                      }}
                    >
                      <Label text="Phiếu nhập kho" />
                    </SimpleItem>
                    <SimpleItem
                      dataField={'soLuuTru'}
                      editorType={'dxTextBox'}
                      editorOptions={{
                        readOnly: true
                      }}
                    >
                      <Label text="Số lưu trữ" />
                    </SimpleItem>
                    <SimpleItem
                      dataField={'soHoaDon'}
                      editorType={'dxTextBox'}
                      editorOptions={{
                        placeholder: "Nhập số hóa đơn",
                        readOnly: !this.state.enable.formPhieuNhap
                      }}
                    >
                      <Label text="Số hóa đơn" />
                      <RequiredRule message="Chưa nhập số hóa đơn" />
                    </SimpleItem>
                    <SimpleItem
                      dataField={'vat'}
                      editorType={'dxNumberBox'}
                      editorOptions={{
                        min: 0,
                        max: 99,
                        defaultValue: 0,
                        showSpinButtons: true,
                        width: 150,
                        readOnly: !this.state.enable.formPhieuNhap || this.props.enableForm
                      }}
                    >
                      <Label text="VAT" />
                    </SimpleItem>
                    <SimpleItem
                      dataField={'ngayHoaDon'}
                      editorType={'dxDateBox'}
                      editorOptions={{
                        defaultValue: new Date(),
                        type: "date",
                        displayFormat: "dd/MM/yyyy",
                        readOnly: !this.state.enable.formPhieuNhap,
                        openOnFieldClick: true
                      }}
                    >
                      <Label text="Ngày hóa đơn" />
                      <RequiredRule message="Chưa chọn ngày hóa đơn" />
                    </SimpleItem>
                    <SimpleItem
                      dataField={'ngayNhap'}
                      editorType={'dxDateBox'}
                      editorOptions={{
                        defaultValue: new Date(),
                        type: "date",
                        displayFormat: "dd/MM/yyyy",
                        readOnly: !this.state.enable.formPhieuNhap,
                        openOnFieldClick: true
                      }}
                    >
                      <Label text="Ngày nhập" />
                      <RequiredRule message="Chưa chọn ngày nhập" />
                    </SimpleItem>
                    <SimpleItem
                      dataField={'maNhaCungCap'}
                      editorType={'dxSelectBox'}
                      editorOptions={{
                        dataSource: this.props.listDanhSachNhaCungCap,
                        displayExpr: 'TENNHACC',
                        valueExpr: 'MANHACC',
                        placeholder: 'Chọn nhà cung cấp',
                        searchEnabled: true,
                        searchMode: 'contains',
                        searchExpr: 'TENNHACC',
                        readOnly: !this.state.enable.formPhieuNhap
                      }}
                    >
                      <Label text="Nhà cung cấp" />
                      <RequiredRule message="Chưa chọn nhà cung cấp" />
                    </SimpleItem>
                    <SimpleItem
                      dataField={'maKhoNhan'}
                      editorType={'dxSelectBox'}
                      editorOptions={{
                        dataSource: this.props.listDanhSachKhoNhap,
                        displayExpr: 'TENKHOVATTU',
                        valueExpr: 'MAKHOVATTU',
                        placeholder: 'Chọn kho nhập',
                        readOnly: !this.state.enable.formPhieuNhap || this.props.enableForm
                      }}
                    >
                      <Label text="Nhập kho" />
                      <RequiredRule message="Chưa chọn kho nhập" />
                    </SimpleItem>
                    <SimpleItem
                      dataField={'maNguoiNhan'}
                      editorType={'dxSelectBox'}
                      editorOptions={{
                        dataSource: this.props.listDanhSachNguoiNhan,
                        displayExpr: 'TEN_NHANVIEN',
                        valueExpr: 'MA_NHANVIEN',
                        placeholder: 'Chọn người nhận',
                        readOnly: !this.state.enable.formPhieuNhap
                      }}
                    >
                      <Label text="Người nhận" />
                      <RequiredRule message="Chưa chọn người nhận" />
                    </SimpleItem>
                    <SimpleItem
                      dataField={'nguoiGiaoHang'}
                      editorType={'dxTextBox'}
                      editorOptions={{
                        placeholder: "Nhập người giao",
                        readOnly: !this.state.enable.formPhieuNhap
                      }}
                    >
                      <Label text="Người giao" />
                    </SimpleItem>
                    <SimpleItem
                      colSpan={2}
                      dataField={'ghiChu'}
                      editorType={'dxTextBox'}
                      editorOptions={{
                        placeholder: "Nhập ghi chú",
                        readOnly: !this.state.enable.formPhieuNhap
                      }}
                    >
                      <Label text="Ghi chú" />
                    </SimpleItem>
                    <SimpleItem
                      colSpan={2}
                      dataField={'soHopDong'}
                      editorType={'dxTextBox'}
                      editorOptions={{
                        placeholder: "Nhập số hợp đồng",
                        readOnly: !this.state.enable.formPhieuNhap
                      }}
                    >
                      <Label text="Số hợp đồng" />
                    </SimpleItem>
                    <SimpleItem
                      dataField={'file'}
                      editorType={'dxTextBox'}
                      editorOptions={{
                        placeholder: "Chọn tập tin"
                      }}
                    >
                      <Label text="File import"/>
                    </SimpleItem>
                    <SimpleItem
                      dataField={'maHoiDongKiemNhap'}
                      editorType={'dxSelectBox'}
                      editorOptions={{
                        dataSource: this.props.listDanhSachHoiDongKiemNhap,
                        displayExpr: 'TENHDKN',
                        valueExpr: 'MAHDKN',
                        placeholder: 'Chọn hội đồng kiểm nhập (In BBKN)'
                      }}
                    >
                      <Label text="Hội đồng kiểm nhập" />
                      {/* <RequiredRule message="Chưa chọn hội đồng kiểm nhập" />*/}
                    </SimpleItem>
                  </GroupItem>
                  <GroupItem
                    colCount={5}
                    colSpan={2}
                  >
                    <SimpleItem
                      colSpan={5}
                      render={() => {
                        return <div className={'div-button'}>
                          <Button
                            icon={'plus'}
                            validationGroup={'formPhieuNhap'}
                            useSubmitBehavior={true}
                            type={"default"}
                            text={'Thêm'}
                            disabled={!this.state.enable.btnThem}
                            className={'btn-margin-5 button-duoc'}
                          />
                          <Button
                            icon={'edit'}
                            type={"default"}
                            text={'Sửa'}
                            disabled={!this.state.enable.btnSua}
                            className={'btn-margin-5 button-duoc'}
                            onClick={this.handleOnClickButtonSua}
                          />
                          <Button
                            icon={'save'}
                            type={"default"}
                            text={'Lưu'}
                            disabled={!this.state.enable.btnLuu}
                            className={'btn-margin-5 button-duoc'}
                            onClick={this.handleOnClickButtonLuu}
                          />
                          <Button
                            icon={'close'}
                            type={"default"}
                            text={'Hủy'}
                            disabled={!this.state.enable.btnHuy}
                            className={'btn-margin-5 button-duoc'}
                            onClick={this.handleOnClickButtonHuy}
                          />
                          <Button
                            icon={'clear'}
                            type={"danger"}
                            text={'Xóa'}
                            disabled={!this.state.enable.btnXoa}
                            className={'btn-margin-5 button-duoc'}
                            onClick={this.handleOnClickButtonXoa}
                          />
                          <DuocDropDownButton
                            icon={'print'}
                            loadingConfigs={this.props.loadingConfigs}
                            disabled={!this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap}
                            text={'In phiếu'}
                            arrayButton={[
                              { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInPhieuNhapKho(this.props.maNghiepVu === 24 ? 0 : 1, 'pdf')},
                              { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () =>  this.handleOnClickButtonInPhieuNhapKho(this.props.maNghiepVu === 24 ? 0 : 1, 'xls')},
                              { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () =>  this.handleOnClickButtonInPhieuNhapKho(this.props.maNghiepVu === 24 ? 0 : 1, 'rtf')}
                            ]}
                            defaultHandleOnClick={() => this.handleOnClickButtonInPhieuNhapKho(this.props.maNghiepVu === 24 ? 0 : 1, 'pdf')}
                            className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5 button-duoc'}
                          />
                          <Button
                            icon={'exportxlsx'}
                            type={"default"}
                            text={'Xuất DM mẫu'}
                            disabled={false}
                            className={'btn-margin-5 button-duoc'}
                            onClick={this.handleOnClickButtonXuatDM}
                          />
                          <DuocDropDownButton
                            icon={'print'}
                            loadingConfigs={this.props.loadingConfigs}
                            disabled={!this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap}
                            text={'In BB Kiểm nhập'}
                            arrayButton={[
                              { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () =>  this.handleOnClickButtonInBBKN(this.props.maNghiepVu === 24 ? 0 : 1, 'pdf')},
                              { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () =>  this.handleOnClickButtonInBBKN(this.props.maNghiepVu === 24 ? 0 : 1, 'xls')},
                              { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () =>  this.handleOnClickButtonInBBKN(this.props.maNghiepVu === 24 ? 0 : 1, 'rtf')}
                            ]}
                            defaultHandleOnClick={() =>  this.handleOnClickButtonInBBKN(this.props.maNghiepVu === 24 ? 0 : 1, 'pdf')}
                            className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5 button-duoc'}
                          />
                          <DuocDropDownButton
                            icon={'print'}
                            loadingConfigs={this.props.loadingConfigs}
                            disabled={this.props.selectedPhieuChuyenLength === undefined ? true : this.props.selectedPhieuChuyenLength}
                            text={'In BB Kiểm nhập (nhiều)'}
                            arrayButton={[
                              { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () =>  this.handleOnClickButtonInBBKNNhieuPhieu('pdf')},
                              { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () =>  this.handleOnClickButtonInBBKNNhieuPhieu('xls')},
                              { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () =>  this.handleOnClickButtonInBBKNNhieuPhieu('rtf')}
                            ]}
                            defaultHandleOnClick={() =>  this.handleOnClickButtonInBBKNNhieuPhieu('pdf')}
                            className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5 button-duoc'}
                          />
                        </div>
                      }}
                    >
                    </SimpleItem>
                  </GroupItem>
                </Form>
              </form>

            </div>
          ) : (
            // Tab 2
            <div>
              <Box
                direction="col"
                width="100%"
                height={'auto'}
              >
                <BoxItem ratio={0} baseSize={'100%'}>
                  <form action={''} onSubmit={this.handleOnClickButtonThemNV}>
                    <Form
                      className={'row-margin-top-5'}
                      formData={this.state.nhanVienKiemNhapObj}
                      validationGroup={"formNhanVien"}
                      labelLocation={"left"}
                      colCount={3}
                      onFieldDataChanged={this.handleSelectNhanVienKiemNhap}
                    >
                      <SimpleItem
                        dataField={'maNhanVien'}
                        editorType={'dxSelectBox'}
                        editorOptions={{
                          dataSource: this.props.listDanhSachNguoiNhan,
                          displayExpr: 'TEN_NHANVIEN',
                          valueExpr: 'MA_NHANVIEN',
                          placeholder: 'Chọn nhân viên kiểm nhập',
                          defaultValue: this.props.listDanhSachNguoiNhan.length > 0 ? this.props.listDanhSachNguoiNhan[0].MANHANVIEN : null,
                          readOnly: this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap === null
                        }}
                      >
                        <Label text="Nhân viên" />
                        <RequiredRule message="Chưa chọn nhân viên" />
                      </SimpleItem>
                      <SimpleItem
                        dataField={'chucDanh'}
                        editorType={'dxTextBox'}
                        editorOptions={{
                          readOnly: true
                        }}
                      >
                        <Label text="Chức danh" />
                      </SimpleItem>
                      <SimpleItem
                        render={() => {
                          return <Button
                            validationGroup={'formNhanVien'}
                            useSubmitBehavior={true}
                            text={'Thêm'}
                            type={"default"}
                            disabled={this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap === null}
                          />
                        }}
                      />
                    </Form>
                  </form>
                </BoxItem>
                <br/>
                <BoxItem ratio={0} baseSize={'100%'}>
                  <GridNhanVienKiemNhap
                    className={'row-margin-top-5'}
                    listNhanvienKiemNhap={this.props.listDanhSachNhanVienKiemNhap}
                    onSelectionChanged={this.handleOnSelectRowDataGridNhanVienKiemNhap}
                    onKeyDown={this.handleOnKeyDownGridNhanVienKiemNhap}
                  />
                </BoxItem>
              </Box>
            </div>
          )
        }
      </div>
    );
  }
}

export default FormPhieuNhapKho;
