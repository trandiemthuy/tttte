import React from 'react';
import {RouteComponentProps} from "react-router";
import {IRootState} from "app/shared/reducers";
import {connect} from "react-redux";
import Box, {Item as BoxItem} from "devextreme-react/box";
import {
  getDanhSachPhieuDuTru,
  getDanhSachPhieuChuaDuTru,
  getDanhSachVatTuPhieuDutru,
  getDanhSachBenhNhanPhieuDuTru,
  tongHopDuTruTheoPhieu,
  tongHopDuTruTheoNgay,
  deletePhieuDuTru,
  resetDanhSachVatTu,
  resetDanhSachBenhNhan,
  guiPhieuDuTruLenKhoaDuoc,
  printPhieuHoanTraThuoc
} from './tong-hop-du-tru.reducer';
import {
  getDanhSachBanInDuTruHoanTra,
  printPhieuDuTruNoiTru,
  printSoYLenhDuTruNoiTru,
  printDanhSachToaThuoc,
  printPhieuHoanTraNoiTru
} from '../duyet-du-tru-hoan-tra/duyet-du-tru-hoan-tra.reducer';
import {
  getDvtt,
  getMaNhanVien,
  getMaPhongBan,
  getMaPhongBenh, getTenBenhVien, getTenNhanVien, getTenPhongBan, getTenTinh
} from "app/modules/duoc/duoc-components/utils/thong-tin";
import {convertToSQLDate} from "app/modules/duoc/duoc-components/utils/funtions";
import TuNgayDenNgayDocComponent from "app/modules/duoc/duoc-components/utils/tungay-denngay/tungay-denngay-doc";
import GridDanhSachPhieuDuTru from "./components/grid-danh-sach-phieu-du-tru/grid-danh-sach-phieu-du-tru";
import GridDanhSachPhieuChuaDuTru from "./components/grid-danh-sach-chua-du-tru/grid-danh-sach-chua-du-tru";
import {Button, CheckBox} from "devextreme-react";
import TuNgayDenNgayNgangComponent
  from "app/modules/duoc/duoc-components/utils/tungay-denngay/tungay-denngay-ngang";
import DuocTitleComponent from "app/modules/duoc/duoc-components/utils/duoc-title/duoc-title";
import FormPhieuDuTru
  from "app/modules/duoc/duoc-components/tong-hop-du-tru/components/form-phieu-du-tru/form-phieu-du-tru";
import {DuocDropDownButton} from "app/modules/duoc/duoc-components/utils/button-dropdown/button-dropdown";
import GridDanhSachVatTuDuTru
  from "app/modules/duoc/duoc-components/tong-hop-du-tru/components/grid-danh-sach-vat-tu-du-tru/grid-danh-sach-vat-tu-du-tru";
import DuocLabelComponent from "app/modules/duoc/duoc-components/utils/duoc-label/duoc-label";
import GridDanhSachBenhNhanDuTru
  from "app/modules/duoc/duoc-components/tong-hop-du-tru/components/grid-danh-sach-benh-nhan-du-tru/grid-danh-sach-benh-nhan-du-tru";
import notify from "devextreme/ui/notify";
import {NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING} from "app/modules/duoc/configs";
import PopUpDanhSachBanInChuyenKho
  from "app/modules/duoc/duoc-components/chuyen-kho/components/popup-danh-sach-ban-in/popup-danh-sach-ban-in";
import dialog from "devextreme/ui/dialog";
import {
  defaultPhieuDuTruModel,
  IPhieuDuTruModel
} from "app/modules/duoc/duoc-components/tong-hop-du-tru/phieu-du-tru.model";

export interface ITongHopDuTruComponentProp extends StateProps, DispatchProps, RouteComponentProps<{ nghiepVu: string; isBANT: string; }> {
  account: any;
}

export interface ITongHopDuTruComponentState {
  trangThaiPhieu: number;  // 1: Chưa gửi lên khoa dược     2/3: Đã gửi lên khoa dược
  loadingConfigs: {
    visible: boolean;
    position: string;
  };
  enableButton: boolean;
  printPopUp: {
    loaiIn: number; // 0: In phiếu dự trù nội trú    3: In phiếu hoàn trả
    visible: boolean;
    loaiFile: string;
  };
  sendDataToForm: {
    send: boolean;
    phieuDuTru: IPhieuDuTruModel
  }
}

export class TongHopDuTruComponent extends React.Component<ITongHopDuTruComponentProp, ITongHopDuTruComponentState> {
  private refTuNgayDenNgayPhieuDuTru = React.createRef<TuNgayDenNgayDocComponent>();
  private refTuNgayDenNgayPhieuChuaDuTru = React.createRef<TuNgayDenNgayNgangComponent>();
  private refGridDanhSachPhieuDuTru = React.createRef<GridDanhSachPhieuDuTru>();
  private refGridDanhSachPhieuChuaDuTru = React.createRef<GridDanhSachPhieuChuaDuTru>();
  private refFormPhieuDuTru = React.createRef<FormPhieuDuTru>();
  constructor(props) {
    super(props);
    this.state = {
      trangThaiPhieu: 1,
      loadingConfigs: {
        visible: false,
        position: 'body'
      },
      enableButton: false,
      printPopUp: {
        loaiIn: 0,
        visible: false,
        loaiFile: 'pdf'
      },
      sendDataToForm: {
        send: false,
        phieuDuTru: defaultPhieuDuTruModel
      }
    };
  }

  componentDidMount(): void {
    this.getDanhSachPhieuDuTru(
      getDvtt(),
      convertToSQLDate(new Date().toDateString()),
      convertToSQLDate(new Date().toDateString()),
      this.state.trangThaiPhieu,
      parseInt(this.props.match.params.isBANT, 0),
      getMaPhongBan()
    );
  }

  componentDidUpdate(prevProps: Readonly<ITongHopDuTruComponentProp>, prevState: Readonly<ITongHopDuTruComponentState>, snapshot?: any): void {
    if (prevProps.match.params.nghiepVu !== this.props.match.params.nghiepVu || prevProps.match.params.isBANT !== this.props.match.params.isBANT) {
      this.props.resetDanhSachVatTu();
      this.props.resetDanhSachBenhNhan();
      this.handleOnClickButtonLamMoi();
    }
  }

  // Handles
  handleSendDataToForm = (phieuDuTru: IPhieuDuTruModel) => {
    this.setState({
      sendDataToForm: {
        send: true,
        phieuDuTru
      }
    }, () => {
      this.setState({
        sendDataToForm: {
          send: false,
          phieuDuTru: defaultPhieuDuTruModel
        }
      });
    });
  };
  handleChangeTrangThaiPhieu = (e) => {
    this.setState({
      trangThaiPhieu: e.value ? 23 : 1
    }, () => {
      this.handleOnClickButtonLamMoi();
    });
  };
  handleOnClickButtonLamMoi = () => {
    this.props.resetDanhSachVatTu();
    this.props.resetDanhSachBenhNhan();
    this.getDanhSachPhieuDuTru(
      getDvtt(),
      this.refTuNgayDenNgayPhieuDuTru.current.getTuNgayDenNgay().tuNgay,
      this.refTuNgayDenNgayPhieuDuTru.current.getTuNgayDenNgay().denNgay,
      this.state.trangThaiPhieu,
      parseInt(this.props.match.params.isBANT, 0),
      getMaPhongBan()
    );
  };
  handleOnClickButtonXemPhieuChuaDuTru = () => {
    this.getDanhSachPhieuChuaDuTru();
  };
  handleOnClickButtonTaoPhieuDuTru = (loaiPhieu: number, loaiTongHop: number) => { // loaiPhieu: 0: Dự trù    1: Hoàn trả     // loaiTongHop: 0: Theo phiếu   1: Theo ngày
    if (loaiPhieu === 0) {
      if (loaiTongHop === 0) {
        if (this.refGridDanhSachPhieuChuaDuTru.current.getSelectedPhieuDuTru() === '') {
          notify({ message: 'Chưa chọn phiếu cần dự trù', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
          return;
        }
        this.props.tongHopDuTruTheoPhieu({
          dvtt: getDvtt(),
          idPhieu: this.refGridDanhSachPhieuChuaDuTru.current.getSelectedPhieuDuTru(),
          isBANT: this.props.match.params.isBANT,
          maPhongBan: getMaPhongBan(),
          maPhongBenh: getMaPhongBenh(),
          maNguoiLap: getMaNhanVien(),
          ghiChu: this.refFormPhieuDuTru.current.getPhieuDuTru().ghiChu
        },
          this.props.match.params.nghiepVu === 'dutru' ? 0 : 1,
          () => {
          this.handleOnClickButtonLamMoi();
          this.handleOnClickButtonXemPhieuChuaDuTru();
          this.props.resetDanhSachVatTu();
          this.props.resetDanhSachBenhNhan();
        });
      } else {
        this.props.tongHopDuTruTheoNgay({
          dvtt: getDvtt(),
          tuNgay: this.refTuNgayDenNgayPhieuChuaDuTru.current.getTuNgayDenNgay().tuNgay,
          denNgay: this.refTuNgayDenNgayPhieuChuaDuTru.current.getTuNgayDenNgay().denNgay,
          isBANT: this.props.match.params.isBANT,
          maPhongBan: getMaPhongBan(),
          maPhongBenh: getMaPhongBenh(),
          maNguoiLap: getMaNhanVien(),
          ghiChu: this.refFormPhieuDuTru.current.getPhieuDuTru().ghiChu
        }, this.props.match.params.nghiepVu === 'dutru' ? 0 : 1,
          () => {
          this.handleOnClickButtonLamMoi();
          this.handleOnClickButtonXemPhieuChuaDuTru();
          this.props.resetDanhSachVatTu();
          this.props.resetDanhSachBenhNhan();
        });
      }
    }
  };
  handleOnClickButtonHuy = (e) => {
    this.setEnableButton(false);
  };
  handleOnClickButtonXoa = (e) => {
    const selectedPhieu = this.refGridDanhSachPhieuDuTru.current.getSelectedPhieuDuTru();
    if (selectedPhieu.idPhieu === null) {
      notify({ message: "Chưa chọn phiếu", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    if (this.state.trangThaiPhieu > 1) {
      notify({ message: "Phiếu đã được gửi. Vui lòng hủy gửi trước khi thao tác", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    dialog.confirm("Xóa phiếu " + "<span style='color: red'>" + selectedPhieu.soLuuTru + "</span>" + " ?", "Thông báo hệ thống")
      .then(dialogResult => {
        if (dialogResult) {
          this.props.deletePhieuDuTru({
            dvtt: getDvtt(),
            idPhieu: this.refGridDanhSachPhieuDuTru.current.getSelectedPhieuDuTru().idPhieu,
            maPhongBan: getMaPhongBan(),
            maPhongBenh: getMaPhongBenh(),
            maNguoiLap: getMaNhanVien()
          }, this.props.match.params.nghiepVu === 'dutru' ? 0 : 1,
            () => {
            this.setEnableButton(false);
            this.handleOnClickButtonLamMoi();
            this.handleOnClickButtonXemPhieuChuaDuTru();
            this.handleSendDataToForm(defaultPhieuDuTruModel);
          });
        }
      });
  };
  handleOnClickButtonInPhieuDuTru = (loaiFile: string, loaiIn: number) => {
    this.handleChangePopUpPrintState(loaiIn, true, loaiFile);
  };
  handleLoading = (visible: boolean) => {
    this.setState({
      loadingConfigs: {
        ...this.state.loadingConfigs,
        visible
      }
    });
  };
  handleChangePopUpPrintState = (loaiIn: number, visible: boolean, loaiFile: string) => {
    const selectedPhieu = this.refGridDanhSachPhieuDuTru.current.getSelectedPhieuDuTru();
    this.props.getDanhSachBanInDuTruHoanTra(getDvtt(),
      selectedPhieu.idPhieu,
      0,
      [1, 4].includes(loaiIn) ? 0 : 1,
      this.props.match.params.nghiepVu === 'dutru' ? 0 : 1);
    this.setState({
      printPopUp: {
        loaiIn,
        visible,
        loaiFile
      }
    });
  };
  handleOnSelectRowGridDanhSachBanIn = (e: any) => {
    const selectedLoaiVatTu = e;
    const selectedPhieuDuTru = this.refGridDanhSachPhieuDuTru.current.getSelectedPhieuDuTru();
    if (this.state.printPopUp.loaiIn === 0) {
      this.props.printPhieuDuTruNoiTru(
        getDvtt(),
        selectedPhieuDuTru.idPhieu,
        selectedPhieuDuTru.soLuuTru,
        selectedPhieuDuTru.ngayLap.substr(0, 10),
        selectedLoaiVatTu.THANH_TIEN,
        selectedLoaiVatTu.MAKHOVATTU,
        selectedLoaiVatTu.TENKHOVATTU,
        getTenPhongBan(),
        selectedLoaiVatTu.TEN_PHONGBAN,
        selectedLoaiVatTu.MALOAIVATTU,
        0,
        0,
        1,
        getMaNhanVien(),
        getTenNhanVien(),
        getTenBenhVien(),
        getTenTinh(),
        this.state.printPopUp.loaiFile,
        this.handleLoading
      );
    } else if (this.state.printPopUp.loaiIn === 3) {
      this.props.printPhieuHoanTraNoiTru(
        getDvtt(),
        selectedPhieuDuTru.idPhieu,
        selectedPhieuDuTru.soLuuTru,
        selectedPhieuDuTru.ngayLap.substr(0, 10),
        selectedLoaiVatTu.THANH_TIEN,
        selectedLoaiVatTu.MAKHOVATTU,
        selectedLoaiVatTu.TENKHOVATTU,
        getTenPhongBan(),
        selectedLoaiVatTu.TEN_PHONGBAN,
        selectedLoaiVatTu.MALOAIVATTU,
        0,
        1,
        getMaNhanVien(),
        getTenNhanVien(),
        getTenBenhVien(),
        getTenTinh(),
        this.state.printPopUp.loaiFile,
        this.handleLoading
      );
    }
  };
  handleOnClickMenuGuiPhieuLenKhoaDuoc = (selectedPhieu: any) => {
    this.props.guiPhieuDuTruLenKhoaDuoc({
      dvtt: getDvtt(),
      idPhieu: this.props.match.params.nghiepVu === 'dutru' ? selectedPhieu.ID_PHIEU_DU_TRU_TONG : selectedPhieu.ID_PHIEU_HOAN_TRA_TONG,
      isBANT: this.props.match.params.isBANT,
      maPhongBan: getMaPhongBan(),
      maPhongBenh: getMaPhongBenh(),
      maNguoiLap: getMaNhanVien(),
    }, this.state.trangThaiPhieu === 1 ? 2 : 1,
      this.props.match.params.nghiepVu === 'dutru' ? 0 : 1,
      () => {
      this.handleOnClickButtonLamMoi();
    });
  };
  handleOnClickMenuInSoYLenh = (idPhieuDuTruTong: number, soPhieuTaoLuuTru: string, dvtt: string, tuTuThuoc: number, mauBaoCao: number, maKhoa: string, tenKhoa: string, tenPhongBanDuTru: string, tuNgay: string, denNgay: string, maPhong: string, tenTinh: string, tenBenhVien: string, handleLoading: Function) => {
    this.props.printSoYLenhDuTruNoiTru(
      idPhieuDuTruTong,
      soPhieuTaoLuuTru,
      dvtt,
      tuTuThuoc,
      mauBaoCao,
      maKhoa,
      tenKhoa,
      tenPhongBanDuTru,
      tuNgay,
      denNgay,
      maPhong,
      tenTinh,
      tenBenhVien,
      handleLoading
    );
  };
  handleOnClickMenuInPhieuHoanTra = (selectedPhieu, loaiFile: string) => {
    this.props.printPhieuHoanTraThuoc(getDvtt(),
      this.props.match.params.isBANT,
      selectedPhieu.ID_PHIEU_HOAN_TRA_TONG,
      getTenTinh(),
      getTenBenhVien(),
      loaiFile,
      this.handleLoading);
  };
  handleOnContextMenuGridDanhSach = (e) => {
    if (e.row.data === undefined) {
      return;
    }
    e.component.selectRowsByIndexes(e.rowIndex);
    const selectedPhieu = e.row.data;
    const trangThaiPhieu = this.state.trangThaiPhieu;
    if (e.row.rowType === 'data') {
      if (this.props.match.params.nghiepVu === 'dutru') {
        e.items = [
          {
            icon: 'sorted',
            text: trangThaiPhieu === 1 ? 'Gửi phiếu lên khoa dược' : 'Hủy gửi phiếu lên khoa dược',
            onItemClick: () => {
              this.handleOnClickMenuGuiPhieuLenKhoaDuoc(selectedPhieu);
            }
          },
          {
            icon: 'exportxlsx',
            text: "Chi tiết phiếu lĩnh",
            onItemClick: () => {
              this.handleOnClickMenuInSoYLenh(
                selectedPhieu.ID_PHIEU_DU_TRU_TONG,
                selectedPhieu.SOPHIEUTAO_LUUTRU,
                getDvtt(),
                0,
                0,
                getMaPhongBan(),
                getTenPhongBan(),
                getTenPhongBan(),
                "",
                "",
                getMaPhongBan(),
                getTenTinh(),
                getTenBenhVien(),
                this.handleLoading
              );
            }
          },
        ];
      } else {
        e.items = [
          {
            icon: 'sorted',
            text: trangThaiPhieu === 1 ? 'Gửi phiếu lên khoa dược' : 'Hủy gửi phiếu lên khoa dược',
            onItemClick: () => {
              this.handleOnClickMenuGuiPhieuLenKhoaDuoc(selectedPhieu);
            }
          },
          {
            icon: 'print',
            text: "In phiếu hoàn trả",
            items: [{
              icon: 'exportpdf',
              text: "In phiếu hoàn trả (PDF)",
              onItemClick: () => {
                this.handleOnClickMenuInPhieuHoanTra(selectedPhieu, 'pdf');
              }
            },
              {
                icon: 'exportxlsx',
                text: "In phiếu hoàn trả (XLS)",
                onItemClick: () => {
                  this.handleOnClickMenuInPhieuHoanTra(selectedPhieu, 'pdf');
                }
              },
              {
                icon: 'docfile',
                text: "In phiếu hoàn trả (RTF)",
                onItemClick: () => {
                  this.handleOnClickMenuInPhieuHoanTra(selectedPhieu, 'pdf');
                }
              }]
          },
        ];
      }
    }
  };

  // Funtion
  setEnableButton = (enable: boolean) => {
    this.setState({
      enableButton: enable
    });
  };
  renderTitle = () => {
    return this.props.match.params.nghiepVu === 'dutru' ? 'dự trù' : 'hoàn trả';
  };
  getDanhSachPhieuDuTru = (dvtt: string, tuNgay: string, denNgay: string, trangThai: number, bant: number, maPhongBan: string) => {
    this.props.getDanhSachPhieuDuTru(dvtt,
      tuNgay,
      denNgay,
      trangThai,
      bant,
      maPhongBan,
      this.props.match.params.nghiepVu === 'dutru' ? 0 : 1
      );
  };
  getDanhSachPhieuChuaDuTru = () => {
    this.props.getDanhSachPhieuChuaDuTru(
      getDvtt(),
      this.refTuNgayDenNgayPhieuChuaDuTru.current.getTuNgayDenNgay().tuNgay,
      this.refTuNgayDenNgayPhieuChuaDuTru.current.getTuNgayDenNgay().denNgay,
      1,
      parseInt(this.props.match.params.isBANT, 0),
      getMaPhongBan(),
      this.props.match.params.nghiepVu === 'dutru' ? 0 : 1
    );
  };
  getDanhSachVatTuDuTru = (dvtt: string, idPhieu: number, maPhongBan: string, hinhThucXem: number, thoiGian: number, loaiPhieu: number) => {
    this.props.getDanhSachVatTuPhieuDutru(
      dvtt,
      idPhieu,
      maPhongBan,
      hinhThucXem,
      thoiGian,
      loaiPhieu
    );
  };
  getDanhSachBenhNhanDuTru = (dvtt: string, idPhieu: number, ngayLap: string, maPhongBan: string, loaiPhieu: number) => {
    this.props.getDanhSachBenhNhanPhieuDuTru(
      dvtt,
      idPhieu,
      ngayLap,
      maPhongBan,
      loaiPhieu
    )
  };


  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (<div>
      <DuocTitleComponent text={this.props.match.params.nghiepVu === 'dutru' ? 'Tổng hơp dự trù' : 'Tổng hợp hoàn trả'}/>
      <Box
        direction="row"
        width="100%"
        height={'auto'}
      >
        <BoxItem baseSize={'30%'} ratio={0}>
              <div className={'div-box-wrapper'}>
                <DuocLabelComponent text={this.props.match.params.nghiepVu === 'dutru' ? 'Thông tin lọc phiếu dự trù' : 'Thông tin lọc phiếu hoàn trả'}/>
                <div className={'row-margin-top-5'}/>
                <TuNgayDenNgayDocComponent
                  ref={this.refTuNgayDenNgayPhieuDuTru}
                  handleOnClickButtonLamMoi={this.handleOnClickButtonLamMoi}
                  renderRight={() => <CheckBox
                    className={'row-padding-left-5 row-padding-top-5'}
                    text={'Đã gửi'}
                    value={this.state.trangThaiPhieu === 23}
                    onValueChanged={this.handleChangeTrangThaiPhieu}
                  />}
                />
              </div>
              <div className={'div-box-wrapper row-margin-top-5 div-height-100'}>
                <DuocLabelComponent text={this.props.match.params.nghiepVu === 'dutru' ? 'Danh sách phiếu dự trù' : 'Danh sách phiếu hoàn trả'}/>
                <GridDanhSachPhieuDuTru
                  ref={this.refGridDanhSachPhieuDuTru}
                  setEnableButton={this.setEnableButton}
                  getDanhSachVatTuDuTru={this.getDanhSachVatTuDuTru}
                  getDanhSachBenhNhanDuTru={this.getDanhSachBenhNhanDuTru}
                  resetDanhSachVatTu={this.props.resetDanhSachVatTu}
                  resetDanhSachBenhNhan={this.props.resetDanhSachBenhNhan}
                  className={'row-padding-top-5'}
                  loaiPhieu={this.props.match.params.nghiepVu === 'dutru' ? 0 : 1}
                  listDanhSachPhieuDuTru={this.props.listDanhSachPhieuDuTru}
                  handleOnContextMenuGridDanhSach={this.handleOnContextMenuGridDanhSach}
                  handleSendDataToForm={this.handleSendDataToForm}
                />
                <div
                  style={{
                    display: 'flex',
                    width: '100%'
                  }}
                >
                  <div
                    style={{
                      width: '30px',
                      height: '30px',
                      marginTop: '5px',
                      backgroundImage: 'radial-gradient( circle farthest-corner at 10% 20%,  rgba(0,93,133,1) 0%, rgba(0,181,149,1) 90% )'
                    }}
                  >
                  </div>
                  <div
                    style={{
                      marginTop: '10px'
                    }}
                  >Phiếu đã được duyệt</div>
                </div>
              </div>
        </BoxItem>
        <BoxItem baseSize={'70%'} ratio={0}>
          <div className={'div-box-wrapper row-margin-left-5'}>
            <DuocLabelComponent icon={'bookmark'} text={this.props.match.params.nghiepVu === 'dutru' ? 'Thông tin phiếu dự trù' : 'Thông tin phiếu hoàn trả'}/>
            <FormPhieuDuTru
              ref={this.refFormPhieuDuTru}
              sendDataToForm={this.state.sendDataToForm}
              loaiPhieu={this.props.match.params.nghiepVu === 'dutru' ? 0 : 1}
              handleOnClickButtonXemPhieu={null}
            />
            <TuNgayDenNgayNgangComponent
              ref={this.refTuNgayDenNgayPhieuChuaDuTru}
              dateboxTuNgayWidth={'auto'}
              dateboxDenNgayWidth={'auto'}
              handleOnClickButtonLamMoi={this.handleOnClickButtonXemPhieuChuaDuTru}
              titleButton={this.props.match.params.nghiepVu === 'dutru' ? 'Xem phiếu chưa dự trù' : 'Xem phiếu chưa hoàn trả'}
              isTuNgayDauThang={true}
            />
          </div>
          <div className={'div-box-wrapper row-margin-left-5 row-margin-top-5'}>
            <DuocLabelComponent text={this.props.match.params.nghiepVu === 'dutru' ? 'Danh sách phiếu điều trị cần dự trù' : 'Danh sách phiếu cần hoàn trả'}/>
            <GridDanhSachPhieuChuaDuTru
              ref={this.refGridDanhSachPhieuChuaDuTru}
               listDanhSachPhieuChuaDuTru={this.props.listDanhSachChuaDuTru}
               loaiPhieu={this.props.match.params.nghiepVu === 'dutru' ? 0 : 1}
               handleOnContextMenuGridDanhSach={null}
           />
          </div>
          <div className={'div-button div-box-wrapper row-margin-left-5 row-margin-top-5'}>
            <DuocDropDownButton
              icon={'plus'}
              loadingConfigs={this.state.loadingConfigs}
              // disabled={this.state.phieuChuyenKhoObj.idChuyenKhoVatTu === null}
              text={this.props.match.params.nghiepVu === 'dutru' ? 'Tạo phiếu dự trù theo phiếu' : 'Tạo phiếu hoàn trả theo phiếu'}
              arrayButton={[
                // { id: 1, title: 'Tạo phiếu dự trù theo kho', icon: 'plus', handleOnClick: () => this.handleOnClickButtonTaoPhieuDuTru(0, 0)},
                { id: 2, title: 'Theo ngày', icon: 'plus', handleOnClick: () => this.handleOnClickButtonTaoPhieuDuTru(0, 1)},
              ]}
              defaultHandleOnClick={() => this.handleOnClickButtonTaoPhieuDuTru(0, 0)}
              className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
            />
            <Button
              icon={'clear'}
              type={"default"}
              text={'Hủy thao tác'}
              disabled={!this.state.enableButton}
              className={'btn-margin-5 button-duoc'}
              onClick={this.handleOnClickButtonHuy}
              visible={false}
            />
            <Button
              icon={'close'}
              type={"danger"}
              text={'Xóa'}
              disabled={!this.state.enableButton}
              className={'btn-margin-5 button-duoc'}
              onClick={this.handleOnClickButtonXoa}
            />
            <DuocDropDownButton
              icon={'print'}
              loadingConfigs={this.state.loadingConfigs}
              disabled={!this.state.enableButton}
              text={this.props.match.params.nghiepVu === 'dutru' ? 'In phiếu dự trù' : 'In phiếu hoàn trả'}
              arrayButton={[
                { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInPhieuDuTru('pdf', this.props.match.params.nghiepVu === 'dutru' ? 0 : 3)},
                { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInPhieuDuTru('xls', this.props.match.params.nghiepVu === 'dutru' ? 0 : 3)},
                { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInPhieuDuTru('rtf', this.props.match.params.nghiepVu === 'dutru' ? 0 : 3)}
              ]}
              defaultHandleOnClick={() => this.handleOnClickButtonInPhieuDuTru('pdf', this.props.match.params.nghiepVu === 'dutru' ? 0 : 3)}
              className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
            />
          </div>
        </BoxItem>
      </Box>
      <Box
        direction={"row"}
        width={'100%'}
      >
        <BoxItem
          ratio={1}
          baseSize={'50%'}
        >
          <div className={'div-box-wrapper row-margin-top-5 div-height-100'}>
            <DuocLabelComponent text={this.props.match.params.nghiepVu === 'dutru' ? 'Danh sách dược/vật tư trong phiếu dự trù' : 'Danh sách dược/vật tư trong phiếu hoàn trả'}/>
            <GridDanhSachVatTuDuTru
              listDanhSachVatTuDuTru={this.props.listDanhSachVatTuDuTru}
            />
          </div>
        </BoxItem>
        <BoxItem
          ratio={1}
          baseSize={'50%'}
        >
          <div className={'div-box-wrapper row-margin-left-5 row-margin-top-5 div-height-100'}>
            <DuocLabelComponent text={this.props.match.params.nghiepVu === 'dutru' ? 'Danh sách bệnh nhân trong phiếu dự trù' : 'Danh sách bệnh nhân trong phiếu hoàn trả'}/>
            <GridDanhSachBenhNhanDuTru
              listDanhSachBenhNhanDuTru={this.props.listDanhSachBenhNhanDuTru}
            />
          </div>
        </BoxItem>
      </Box>
      <PopUpDanhSachBanInChuyenKho
        listDanhSachBanIn={this.props.listDanhSachBanInPhieuDuTruHoanTraDuyet}
        handleChangePopUpState={this.handleChangePopUpPrintState}
        handleOnSelectRowGridDanhSachBanIn={this.handleOnSelectRowGridDanhSachBanIn}
        printPopUp={this.state.printPopUp}
      />
    </div>);
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  listDanhSachPhieuDuTru: storeState.tonghopdutru.listDanhSachPhieuDuTru,
  listDanhSachChuaDuTru: storeState.tonghopdutru.listDanhSachChuaDuTru,
  listDanhSachVatTuDuTru: storeState.tonghopdutru.listDanhSachVatTuDuTru,
  listDanhSachBenhNhanDuTru: storeState.tonghopdutru.listDanhSachBenhNhanDuTru,
  listDanhSachBanInPhieuDuTruHoanTraDuyet: storeState.duyetdutruhoantra.listDanhSachBanInPhieuDuTruHoanTraDuyet
});

const mapDispatchToProps = {
  getDanhSachPhieuDuTru,
  getDanhSachPhieuChuaDuTru,
  getDanhSachVatTuPhieuDutru,
  getDanhSachBenhNhanPhieuDuTru,
  tongHopDuTruTheoPhieu,
  tongHopDuTruTheoNgay,
  deletePhieuDuTru,
  resetDanhSachVatTu,
  resetDanhSachBenhNhan,
  guiPhieuDuTruLenKhoaDuoc,

  getDanhSachBanInDuTruHoanTra,
  printPhieuDuTruNoiTru,
  printSoYLenhDuTruNoiTru,
  printDanhSachToaThuoc,
  printPhieuHoanTraNoiTru,

  printPhieuHoanTraThuoc
};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TongHopDuTruComponent);
