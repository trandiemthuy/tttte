import React from "react";
import { DateBox, Button as DateBoxButton } from 'devextreme-react/date-box';
import {convertToDate1, convertToSQLDate} from "app/modules/duoc/duoc-components/utils/funtions";
import Box, {Item as BoxItem} from "devextreme-react/box";
import {Button} from "devextreme-react";
import AdvanceDateBox from "app/modules/duoc/duoc-components/utils/advance-datebox/advance-datebox";
export interface ITuNgayDenNgayComponentProp {
  handleOnClickButtonLamMoi: Function;
  labelTuNgayWidth?: string | number;
  dateboxTuNgayWidth?: string | number;
  labelDenNgayWidth?: string | number;
  dateboxDenNgayWidth?: string | number;
  lockDateAfter?: boolean;
  renderRight?: Function;
  titleButton?: string;
}
export interface ITuNgayDenNgayComponentState {
  dateValue: any;
}

export class TuNgayDenNgayDocComponent extends React.Component<ITuNgayDenNgayComponentProp, ITuNgayDenNgayComponentState> {
  private refTuNgay = React.createRef<AdvanceDateBox>();
  private refDenNgay = React.createRef<AdvanceDateBox>();
  constructor(props) {
    super(props);
  }

  handleOnClickButtonLamMoi = (e) => {
    this.props.handleOnClickButtonLamMoi();
  };
  getTuNgayDenNgay = () => {
    return {
      tuNgay: this.refTuNgay.current.getValue(),
      denNgay: this.refDenNgay.current.getValue()
    };
  };

  render() {
    return (
      <div style={{ width: '100%'}}>
            <Box>
              <BoxItem baseSize={this.props.labelTuNgayWidth === undefined ? '15%' : this.props.labelTuNgayWidth} ratio={0}>
                <div className={'col-align-right'}>
                  Từ ngày
                </div>
              </BoxItem>
              <BoxItem baseSize={this.props.dateboxTuNgayWidth === undefined ? '60%' : this.props.dateboxTuNgayWidth} ratio={0}>
                <AdvanceDateBox
                  ref={this.refTuNgay}
                  maxDay={this.props.lockDateAfter === undefined ? null : new Date()}
                />
              </BoxItem>
              <BoxItem baseSize={'25%'} ratio={0}>
                <div className={'row-padding-left-5'}>
                  <Button
                    icon={'refresh'}
                    type={"default"}
                    text={this.props.titleButton === undefined ? 'Làm mới' : this.props.titleButton}
                    onClick={this.handleOnClickButtonLamMoi}
                  />
                </div>
              </BoxItem>
            </Box>
            <div className={'row-padding-top-5'}/>
          <Box>
            <BoxItem baseSize={this.props.labelDenNgayWidth === undefined ? '15%' : this.props.labelDenNgayWidth} ratio={0}>
              <div className={'col-align-right'}>
                Đến ngày
              </div>
            </BoxItem>
            <BoxItem baseSize={this.props.dateboxDenNgayWidth === undefined ? '60%' : this.props.dateboxDenNgayWidth} ratio={0}>
              <AdvanceDateBox
                ref={this.refDenNgay}
                maxDay={this.props.lockDateAfter === undefined ? null : new Date()}
              />
            </BoxItem>
            <BoxItem baseSize={'25%'} ratio={0}>
              {
                this.props.renderRight !== undefined ? this.props.renderRight() : null
              }
            </BoxItem>
          </Box>
      </div>
    );
  }
}

export default TuNgayDenNgayDocComponent;
