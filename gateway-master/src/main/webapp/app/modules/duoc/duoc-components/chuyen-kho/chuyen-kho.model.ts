import { convertToDate1 } from 'app/modules/duoc/duoc-components/utils/funtions';

export interface IPhieuChuyenKho {
  diaChi?: string;
  duTruCSTT?: number;
  duTruDVQL?: number;
  duyet?: number;
  ghiChu?: string;
  goiKhoa?: string;
  idChuyenCha?: number;
  idChuyenKhoVatTu?: number;
  idGiaoDich?: number;
  maDonViGiao?: string;
  maDonViNhan?: string;
  maDonViTao?: string;
  maKhoGiao?: number;
  maKhoNhan?: number;
  maNghiepVuChuyen?: number;
  maNguoiDuyet?: number;
  maNguoiGiao?: number;
  maNguoiNhan?: number;
  maNguoiTao?: number;
  maPhongBanTao?: string;
  ngayPhieuChuyen?: string;
  ngayChuyen?: string;
  ngayDuyet?: string;
  ngayDuyetChuyenDi?: string;
  ngayGioChuyen?: string;
  ngayGioDuyet?: string;
  ngayGioDuyetChuyenDi?: string;
  ngayGioNhanChuyenDen?: string;
  ngayGioTao?: string;
  ngayNhanChuyenDen?: string;
  ngayTao?: string;
  nghiepVu?: string;
  soLuuTru?: string;
  soPhieuChuyen?: string;
  soPhieuNhap?: string;
  ttYC?: number;
  xuatHuy?: number; // 0: Xuất thực      1: Xuất hủy     2: Xuất nhượng      3: Xuất điều trị
}

export const defaultPhieuChuyenKho: IPhieuChuyenKho = {
  diaChi: '',
  duTruCSTT: 0,
  duTruDVQL: 0,
  duyet: 0,
  ghiChu: '',
  goiKhoa: null,
  idChuyenCha: null,
  idChuyenKhoVatTu: null,
  idGiaoDich: 0,
  maDonViGiao: null,
  maDonViNhan: null,
  maDonViTao: null,
  maKhoGiao: null,
  maKhoNhan: null,
  maNghiepVuChuyen: null,
  maNguoiDuyet: 0,
  maNguoiGiao: 0,
  maNguoiNhan: 0,
  maNguoiTao: 0,
  maPhongBanTao: null,
  ngayPhieuChuyen: new Date().toDateString(),
  ngayChuyen: new Date().toDateString(),
  ngayDuyet: null,
  ngayDuyetChuyenDi: null,
  ngayGioChuyen: null,
  ngayGioDuyet: null,
  ngayGioDuyetChuyenDi: null,
  ngayGioNhanChuyenDen: null,
  ngayGioTao: null,
  ngayNhanChuyenDen: null,
  ngayTao: null,
  nghiepVu: null,
  soLuuTru: null,
  soPhieuChuyen: null,
  soPhieuNhap: null,
  ttYC: 0,
  xuatHuy: 0 // 0: Xuất thực      1: Xuất hủy     2: Xuất nhượng      3: Xuất điều trị
};

export interface IChiTietPhieuChuyenKho {
  donGia?: number;
  donGiaDV?: number;
  duTruDVQL?: number;
  duyet?: number;
  ghiChu?: string;
  idChuyenKhoVT?: number;
  idChuyenKhoVTCT?: number;
  idGiaoDich?: number;
  idSoPhieuChuyen?: number;
  keyQL?: string;
  keyQLID?: number;
  keyQLIDNhan?: number;
  lanChuyenSoNhapKhoCT?: number;
  maChuyenKhoVatTu?: string;
  maDonViGiao?: string;
  maDonViNhan?: string;
  maDonViTao?: string;
  maKhoNhan?: number;
  maKhoGiao?: number;
  maNghiepVuChuyen?: number;
  maVatTu?: number;
  ngayChuyen?: string;
  ngayDuyetChuyenDi?: string;
  ngayHetHan?: string;
  ngayNhanChuyenDen?: string;
  ngaySanXuat?: string;
  ngayXuLy?: string;
  nguoiNhap?: number;
  soLoSanXuat?: string;
  soLuongDuocDuyet?: number;
  soNhapKhoChiTiet?: number;
  soPhieuChuyen?: string;
  soLuong?: number;
  tenVatTu?: string;
  thanhTien?: number;
  hoatChat?: string;
  dvt?: string;
  soThau?: string;
}
export const defaultChiTietPhieuChuyenKho: IChiTietPhieuChuyenKho = {
  donGia: null,
  donGiaDV: null,
  duTruDVQL: null,
  duyet: null,
  ghiChu: '',
  idChuyenKhoVT: null,
  idChuyenKhoVTCT: 0,
  idGiaoDich: 0,
  idSoPhieuChuyen: null,
  keyQL: '',
  keyQLID: null,
  keyQLIDNhan: null,
  lanChuyenSoNhapKhoCT: null,
  maChuyenKhoVatTu: '',
  maDonViGiao: '',
  maDonViNhan: '',
  maDonViTao: '',
  maKhoNhan: null,
  maKhoGiao: null,
  maNghiepVuChuyen: null,
  maVatTu: null,
  ngayChuyen: '',
  ngayDuyetChuyenDi: '',
  ngayHetHan: '',
  ngayNhanChuyenDen: '',
  ngaySanXuat: '',
  ngayXuLy: '',
  nguoiNhap: null,
  soLoSanXuat: '',
  soLuongDuocDuyet: null,
  soNhapKhoChiTiet: null,
  soPhieuChuyen: '',
  soLuong: 0,
  tenVatTu: '',
  thanhTien: 0,
  hoatChat: '',
  dvt: '',
  soThau: ''
};
