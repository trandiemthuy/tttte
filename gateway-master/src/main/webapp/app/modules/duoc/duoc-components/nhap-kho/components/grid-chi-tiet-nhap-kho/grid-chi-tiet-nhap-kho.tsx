import React from "react";
import DataGrid, {
  Column,
  ColumnChooser, ColumnFixing,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection
} from 'devextreme-react/data-grid';
import {convertToDate} from "app/modules/duoc/duoc-components/utils/funtions";
import {columnsWidth} from "app/modules/duoc/duoc-components/nhap-kho/components/grid-chi-tiet-hoa-don/configs";
import {configLoadPanel, numberFormat} from "app/modules/duoc/configs";

export interface IGridChiTietNhapKhoProp extends IDataGridOptions {
  listChiTietNhapKho: any;
  thamsoVAT: number;
  maNghiepVu: number;
}
export interface IGridChiTietNhapKhoState {
  listChiTietNhapKho: any;
}

export class GridChiTietNhapKho extends React.Component<IGridChiTietNhapKhoProp, IGridChiTietNhapKhoState> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <DataGrid
        loadPanel={configLoadPanel}
        showColumnHeaders={false}
        dataSource={this.props.listChiTietNhapKho}
        showBorders={true}
        showColumnLines={true}
        allowColumnReordering={true}
        rowAlternationEnabled={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        height={350}
        noDataText={'Không có dữ liệu'}
        wordWrapEnabled={true}
        {...this.props}
      >
        <Selection mode="single" />
        <FilterRow visible={true}
                   applyFilter={'auto'} />
        <HeaderFilter visible={true} />
        <Column
          dataField="SONHAPKHOCHITIET"
          caption="Số nhập kho chi tiết"
          dataType="string"
          alignment="left"
          visible={false}
        />
        <Column
          dataField="TENVATTU"
          caption="Tên dược/vật tư"
          dataType="string"
          alignment="left"
          width={columnsWidth.cot1}
        />
        <Column
          dataField="DVT"
          caption="ĐVT"
          dataType="string"
          alignment="left"
          width={columnsWidth.cot2}
        />
        <Column
          dataField="QUYCACH"
          caption="Quy cách"
          dataType="string"
          alignment="left"
          width={columnsWidth.cot3}
        />
        <Column
          dataField="MABAOCAO"
          caption="Số thầu"
          dataType="string"
          alignment="left"
          width={columnsWidth.cot4}
        />
        <Column
          dataField="SOLUONG"
          caption="Số lượng"
          dataType="number"
          alignment="left"
          width={columnsWidth.cot5}
          format={numberFormat}
        />
        <Column
          dataField="PHANTRAM_HAOHUT"
          caption="% hao hụt"
          dataType="number"
          alignment="left"
          width={columnsWidth.cot6}
          format={numberFormat}
        />
        <Column
          dataField="SOLUONG"
          caption={this.props.maNghiepVu === 23 ? "SL thực" : "SL quy đổi"}
          dataType="number"
          alignment="left"
          width={columnsWidth.cot7}
          format={numberFormat}
        />
        <Column
          dataField="SOLOSANXUAT"
          caption="Số lô"
          dataType="string"
          alignment="left"
          width={columnsWidth.cot8}
        />
        <Column
          dataField="NGAYHETHAN"
          caption="Hạn dùng"
          dataType="string"
          alignment="left"
          // format="dd/MM/yyyy"
          customizeText={(data) => {
            return data.value && convertToDate(data.value);
          }}
          width={columnsWidth.cot9}
        />
        <Column
          dataField="DONGIA"
          caption="Đơn giá hóa đơn"
          dataType="number"
          alignment="left"
          width={columnsWidth.cot10}
          format={numberFormat}
        />
        <Column
          dataField={this.props.maNghiepVu === 23 ? "DONGIA" : "DONGIATRUOCVAT"}
          caption="Đơn giá thực"
          dataType="number"
          alignment="left"
          width={columnsWidth.cot11}
          format={numberFormat}
        />
        <Column
          dataField={this.props.maNghiepVu === 23 ? "DONGIA_TRUOCHAOHUT" : "DONGIA"}
          caption="Đơn giá(VAT)"
          dataType="number"
          alignment="left"
          width={columnsWidth.cot12}
          format={numberFormat}
        />
        <Column
          dataField="DONGIA"
          caption="Đơn giá nhập kho"
          dataType="number"
          alignment="left"
          width={columnsWidth.cot13}
          format={numberFormat}
        />
        <Column
          dataField="DONGIA"
          caption="Giá bán"
          dataType="number"
          alignment="left"
          width={columnsWidth.cot14}
          format={numberFormat}
        />
        <Column
          dataField="TIENVAT"
          caption="Tiền VAT"
          dataType="number"
          alignment="left"
          width={columnsWidth.cot15}
          format={numberFormat}
        />
        <Column
          dataField={ this.props.thamsoVAT === 1 ? "THANHTIENVAT" : "THANHTIEN"}
          caption="Thành tiền"
          dataType="number"
          alignment="left"
          width={columnsWidth.cot16}
          format={numberFormat}
        />
        <Summary>
          <TotalItem
            displayFormat={'{0} vật tư'}
            column="TENVATTU"
            summaryType="count"
          />
          <TotalItem
            displayFormat={'Tổng: {0}'}
            column={ this.props.thamsoVAT === 1 ? "THANHTIENVAT" : "THANHTIEN"}
            summaryType="sum"
          />
        </Summary>
      </DataGrid>);
  }
}

export default GridChiTietNhapKho;
