export interface IBacSiModel {
  maBacSi?: number;
  tenBacSi?: string;
}

export const defaultBacSiModel: IBacSiModel = {
  maBacSi: null,
  tenBacSi: ''
};
