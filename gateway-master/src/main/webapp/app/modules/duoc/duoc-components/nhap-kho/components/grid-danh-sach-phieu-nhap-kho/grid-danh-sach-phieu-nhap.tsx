import React from "react";

import {configLoadPanel, numberFormat, pageSizes} from '../../../../configs';
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, Summary, TotalItem, Selection, Pager, Paging
} from 'devextreme-react/data-grid';

export interface IGridNhapKhoProp extends IDataGridOptions {
  listDanhSachPhieuNhapKho: any;
}
export interface IGridNhapKhoState {
  listDanhSachPhieuNhapKho: any;
}

export class GridDanhSachPhieuNhapKho extends React.Component<IGridNhapKhoProp, IGridNhapKhoState> {
  constructor(props) {
    super(props);
  }

  render() {
    return (<div>
      <DataGrid
        loadPanel={configLoadPanel}
        dataSource={this.props.listDanhSachPhieuNhapKho}
        showBorders={true}
        showColumnLines={true}
        rowAlternationEnabled={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        columnMinWidth={50}
        noDataText={'Không có dữ liệu'}
        wordWrapEnabled={true}
        {...this.props}
      >
        <Selection mode="multiple" />
        <FilterRow visible={true}
                   applyFilter={'auto'} />
        <HeaderFilter visible={true} />
        <Column
          dataField="ID_NHAPKHOTUNHACC"
          caption="ID_NHAPKHOTUNHACC"
          dataType="string"
          alignment="left"
          visible={false}
        />
        <Column
          dataField="SOPHIEUNHAP"
          caption="Số phiếu"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="SOHOADON"
          caption="Số hóa đơn"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="THANHTIEN"
          caption="Thành tiền"
          dataType="number"
          alignment="left"
          format={numberFormat}
        />
        <Column
          dataField="GHICHU"
          caption="Ghi chú"
          dataType="string"
          alignment="left"
        />
        <Summary>
          <TotalItem
            column="SOPHIEUNHAP"
            summaryType="count"
            displayFormat={'{0} phiếu'}
          />
        </Summary>
        <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
        <Paging defaultPageSize={5} />
      </DataGrid>
      </div>);
  }
}
export default GridDanhSachPhieuNhapKho;
