import React from "react";
import {pageSizes} from '../../../../configs';
import {Form, Button} from "devextreme-react";
import {GroupItem, Label, SimpleItem} from "devextreme-react/form";
import {
  defaultXuatDuocModel,
  IXuatDuocModel
} from "app/modules/duoc/duoc-components/xuat-duoc/xuat-duoc.model";
import {translate} from "react-jhipster";

export interface IFormThongTinXuatDuocProp {
  sendDataToForm: {
    send: boolean;
    ttXuatDuoc: IXuatDuocModel
  }
}
export interface IFormThongTinXuatDuocState {
  ttXuatDuocObj: IXuatDuocModel;
}

export class FormThongTinXuatDuoc extends React.Component<IFormThongTinXuatDuocProp, IFormThongTinXuatDuocState> {
  constructor(props) {
    super(props);
    this.state = {
      ttXuatDuocObj: defaultXuatDuocModel
    }
  }

  componentDidUpdate(prevProps: Readonly<IFormThongTinXuatDuocProp>, prevState: Readonly<IFormThongTinXuatDuocState>, snapshot?: any): void {
    if (this.props.sendDataToForm.send) {
      this.setState({
        ttXuatDuocObj: this.props.sendDataToForm.ttXuatDuoc
      });
    }
  }

  // Handles

  // Functions
  getttXuatDuoc = () => {
    return this.state.ttXuatDuocObj;
  };

  render() {
    return (
      <div>
        <Form
          className={'row-margin-top-5'}
          formData={this.state.ttXuatDuocObj}
          validationGroup={"formPhieuChuyenKho"}
          labelLocation={"left"}
        >
          <GroupItem
            colCount={2}
          >
            <SimpleItem
              dataField={'soPhieuTT'}
              editorType={'dxTextBox'}
              editorOptions={{
                readOnly: true,
                inputAttr: {
                  style: 'color: red'
                }
              }}
            >
              <Label text={translate('duoc.duocXuatDuoc.labelFormThongTin.soPhieu')} />
            </SimpleItem>
            <SimpleItem
              dataField={'tenBenhNhan'}
              editorType={'dxTextBox'}
              editorOptions={{
                readOnly: true,
                inputAttr: {
                  style: 'color: red'
                }
              }}
            >
              <Label text={translate('duoc.duocXuatDuoc.labelFormThongTin.tenBenhNhan')} />
            </SimpleItem>
            <SimpleItem
              dataField={'thoiGianXuatThuoc'}
              editorType={'dxTextBox'}
              editorOptions={{
                readOnly: true
              }}
            >
              <Label text={translate('duoc.duocXuatDuoc.labelFormThongTin.ngayXuat')} />
            </SimpleItem>
              <SimpleItem
                editorType={'dxTextBox'}
                editorOptions={{
                  readOnly: true,
                  value: this.state.ttXuatDuocObj.namSinh && (this.state.ttXuatDuocObj.namSinh + ' (' + this.state.ttXuatDuocObj.tuoi + ' tuổi)')
                }}
              >
                <Label text={translate('duoc.duocXuatDuoc.labelFormThongTin.namSinh')} />
              </SimpleItem>
            <SimpleItem
              dataField={'doiTuong'}
              editorType={'dxTextBox'}
              editorOptions={{
                readOnly: true
              }}
            >
              <Label text={translate('duoc.duocXuatDuoc.labelFormThongTin.doiTuong')} />
            </SimpleItem>
            <SimpleItem
              editorType={'dxTextBox'}
              editorOptions={{
                readOnly: true,
                value: this.state.ttXuatDuocObj.phanTramBaoHiem && (this.state.ttXuatDuocObj.phanTramBaoHiem + ' %')
              }}
            >
              <Label text={translate('duoc.duocXuatDuoc.labelFormThongTin.mucHuong')} />
            </SimpleItem>
            <SimpleItem
              dataField={'ngayHoanTatKham'}
              editorType={'dxTextBox'}
              editorOptions={{
                readOnly: true
              }}
            >
              <Label text={translate('duoc.duocXuatDuoc.labelFormThongTin.ngayHoanTatKham')} />
            </SimpleItem>
            <SimpleItem
              dataField={'chanDoan'}
              editorType={'dxTextBox'}
              editorOptions={{
                readOnly: true
              }}
            >
              <Label text={translate('duoc.duocXuatDuoc.labelFormThongTin.chanDoan')} />
            </SimpleItem>
          </GroupItem>
        </Form>
      </div>
    );
  }
}

export default FormThongTinXuatDuoc;
