import React from 'react';
import { DropdownItem } from 'reactstrap';
import { NavDropdown } from 'app/shared/layout/menus/menu-components';
import {Link} from "react-router-dom";
import './duoc-menu-component.scss';
import {translate} from "react-jhipster";

export const DuocMenuComponent = props => (
  <>
    {/* <NavDropdown icon="th-list" name="Dược- Nhập kho" >*/}
    {/*  <DropdownItem tag={Link} to="/duoc/nhap-kho/52">*/}
    {/*    <div className="duoc-menu-items">*/}
    {/*      Dược - Nhập kho thường*/}
    {/*    </div>*/}
    {/*  </DropdownItem>*/}
    {/*  <DropdownItem tag={Link} to="/duoc/nhap-kho/23">*/}
    {/*    <div className="duoc-menu-items">*/}
    {/*      Dược - Nhập kho đông y*/}
    {/*    </div>*/}
    {/*  </DropdownItem>*/}
    {/*  <DropdownItem tag={Link} to="/duoc/nhap-kho/24">*/}
    {/*    <div className="duoc-menu-items">*/}
    {/*      Dược - Nhập kho quy cách*/}
    {/*    </div>*/}
    {/*  </DropdownItem>*/}
    {/* </NavDropdown>*/}
    {/* <NavDropdown icon="th-list" name="Dược - Chuyển kho" >*/}
    {/*  <DropdownItem tag={Link} to="/duoc/chuyen-kho/8">*/}
    {/*    <div className="duoc-menu-items">*/}
    {/*      Dược - Chuyển kho thường*/}
    {/*    </div>*/}
    {/*  </DropdownItem>*/}
    {/*  <DropdownItem tag={Link} to="/duoc/chuyen-kho/34">*/}
    {/*    <div className="duoc-menu-items">*/}
    {/*      Dược - Dự trù khoa phòng*/}
    {/*    </div>*/}
    {/*  </DropdownItem>*/}
    {/*  <DropdownItem tag={Link} to="/duoc/chuyen-kho/35">*/}
    {/*    <div className="duoc-menu-items">*/}
    {/*      Dược - Hoàn trả khoa phòng*/}
    {/*    </div>*/}
    {/*  </DropdownItem>*/}
    {/*  <DropdownItem tag={Link} to="/duoc/chuyen-kho/37">*/}
    {/*    <div className="duoc-menu-items">*/}
    {/*      Dược - Dự trù tuyến trên*/}
    {/*    </div>*/}
    {/*  </DropdownItem>*/}
    {/*  <DropdownItem tag={Link} to="/duoc/chuyen-kho/50">*/}
    {/*    <div className="duoc-menu-items">*/}
    {/*      Dược - Hoàn trả tuyến trên*/}
    {/*    </div>*/}
    {/*  </DropdownItem>*/}
    {/*  <DropdownItem tag={Link} to="/duoc/chuyen-kho/55">*/}
    {/*    <div className="duoc-menu-items">*/}
    {/*      Dược - Xuất hủy*/}
    {/*    </div>*/}
    {/*  </DropdownItem>*/}
    {/* </NavDropdown>*/}
    {/* <NavDropdown icon="th-list" name="Dược - Duyệt phiếu" >*/}
    {/*   <DropdownItem tag={Link} to="/duoc/duyet-phieu/8">*/}
    {/*     <div className="duoc-menu-items">*/}
    {/*       Dược - Duyệt phiếu chuyển kho*/}
    {/*     </div>*/}
    {/*   </DropdownItem>*/}
    {/*   <DropdownItem tag={Link} to="/duoc/duyet-phieu/34">*/}
    {/*     <div className="duoc-menu-items">*/}
    {/*       Dược - Duyệt dự trù khoa phòng*/}
    {/*     </div>*/}
    {/*   </DropdownItem>*/}
    {/*   <DropdownItem tag={Link} to="/duoc/duyet-phieu/35">*/}
    {/*     <div className="duoc-menu-items">*/}
    {/*       Dược - Duyệt hoàn trả khoa phòng*/}
    {/*     </div>*/}
    {/*   </DropdownItem>*/}
    {/*   <DropdownItem tag={Link} to="/duoc/duyet-phieu/37">*/}
    {/*     <div className="duoc-menu-items">*/}
    {/*       Dược - Duyệt dự trù tuyến trên*/}
    {/*     </div>*/}
    {/*   </DropdownItem>*/}
    {/*   <DropdownItem tag={Link} to="/duoc/duyet-phieu/50">*/}
    {/*     <div className="duoc-menu-items">*/}
    {/*       Dược - Duyệt hoàn trả tuyến trên*/}
    {/*     </div>*/}
    {/*   </DropdownItem>*/}
    {/* </NavDropdown>*/}
    {/* <NavDropdown icon="th-list" name="Dược - Duyệt dự trù/Hoàn trả" >*/}
    {/*   <DropdownItem tag={Link} to="/duoc/duyet-dutru-hoantra/dutru/0">*/}
    {/*     <div className="duoc-menu-items">*/}
    {/*       Dược - Duyệt dự trù nội trú*/}
    {/*     </div>*/}
    {/*   </DropdownItem>*/}
    {/*   <DropdownItem tag={Link} to="/duoc/duyet-dutru-hoantra/dutru/1">*/}
    {/*     <div className="duoc-menu-items">*/}
    {/*       Dược - Duyệt dự trù BANT*/}
    {/*     </div>*/}
    {/*   </DropdownItem>*/}
    {/*   <DropdownItem tag={Link} to="/duoc/duyet-dutru-hoantra/hoantra/0">*/}
    {/*     <div className="duoc-menu-items">*/}
    {/*       Dược - Duyệt hoàn trả nội trú*/}
    {/*     </div>*/}
    {/*   </DropdownItem>*/}
    {/*   <DropdownItem tag={Link} to="/duoc/duyet-dutru-hoantra/hoantra/1">*/}
    {/*     <div className="duoc-menu-items">*/}
    {/*       Dược - Duyệt hoàn trả BANT*/}
    {/*     </div>*/}
    {/*   </DropdownItem>*/}
    {/* </NavDropdown>*/}



     {/* <NavDropdown icon="th-list" name="Dược - tổng hợp" >*/}
     {/* <DropdownItem tag={Link} to="/duoc/tong-hop-du-tru/dutru/0">*/}
     {/*   <div className="duoc-menu-items">*/}
     {/*     Dược - Tổng hợp dự trù nội trú*/}
     {/*   </div>*/}
     {/* </DropdownItem>*/}
     {/*  <DropdownItem tag={Link} to="/duoc/tong-hop-du-tru/dutru/1">*/}
     {/*    <div className="duoc-menu-items">*/}
     {/*      Dược - Tổng hợp dự trù BANT*/}
     {/*    </div>*/}
     {/*  </DropdownItem>*/}
     {/*  <DropdownItem tag={Link} to="/duoc/tong-hop-du-tru/hoantra/0">*/}
     {/*    <div className="duoc-menu-items">*/}
     {/*      Dược - Tổng hợp hoàn trả nội trú*/}
     {/*    </div>*/}
     {/*  </DropdownItem>*/}
     {/*  <DropdownItem tag={Link} to="/duoc/tong-hop-du-tru/hoantra/1">*/}
     {/*    <div className="duoc-menu-items">*/}
     {/*      Dược - Tổng hợp hoàn trả BANT*/}
     {/*    </div>*/}
     {/*  </DropdownItem>*/}
     {/* </NavDropdown>*/}
    <NavDropdown icon="th-list"
                 name={translate('duoc.menu.xuatDuoc.titleXuatDuoc')}
                 // name={'Dược - xuất dược'}
    >
      <DropdownItem tag={Link} to="/duoc/xuat-duoc/20">
        <div className="duoc-menu-items">
           {translate('duoc.menu.xuatDuoc.titleXuatDuocBHYT')}
        </div>
      </DropdownItem>
      <DropdownItem tag={Link} to="/duoc/xuat-duoc/38">
        <div className="duoc-menu-items">
          {translate('duoc.menu.xuatDuoc.titleXuatDuocToaBanLe')}
        </div>
      </DropdownItem>
      <DropdownItem tag={Link} to="/duoc/xuat-duoc/44">
        <div className="duoc-menu-items">
          {translate('duoc.menu.xuatDuoc.titleXuatDuocBANT')}
        </div>
      </DropdownItem>
      <DropdownItem tag={Link} to="/duoc/xuat-duoc-ban-le">
        <div className="duoc-menu-items">
          {translate('duoc.menu.xuatDuoc.titleXuatDuocBANT')}
        </div>
      </DropdownItem>
    </NavDropdown>
  </>
);
