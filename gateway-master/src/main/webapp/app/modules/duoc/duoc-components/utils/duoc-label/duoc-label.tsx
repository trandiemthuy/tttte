import React from "react";

export interface IDuocLabelComponentProp {
  className?: string;
  text: string;
  icon?: string;
}
export interface IDuocLabelComponentState {
  empty: any;
}

export class DuocLabelComponent extends React.Component<IDuocLabelComponentProp, IDuocLabelComponentState> {
  constructor(props) {
    super(props);
  }

  // Handles

  render() {
    return (
      <div className={'duoc-label'}>
        <i className={this.props.icon === undefined ? 'dx-icon-bulletlist row-margin-right-5' : 'dx-icon-' + this.props.icon + ' row-margin-right-5'}></i>
        {this.props.text}
      </div>
    );
  }
}

export default DuocLabelComponent;
