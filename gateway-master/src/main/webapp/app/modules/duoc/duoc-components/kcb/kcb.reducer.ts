import axios from 'axios';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { apiURL } from '../../configs';
import { catchStatusCodeFromResponse } from 'app/modules/duoc/duoc-components/utils/funtions';
import { defaultVatTuModel, IVatTuModel } from 'app/modules/duoc/duoc-components/he-thong/vat-tu.model';
import { defaultToaThuocNgoaiTruModel } from 'app/modules/duoc/duoc-components/kcb/toa-thuoc-ngoai-tru.model';

export const ACTION_TYPES = {
  GET_DANH_SACH_VAT_TU_TON_KHO: 'kcb/GET_DANH_SACH_VAT_TU_TON_KHO'
};

const initialState = {
  listDanhSachVatTuTonKho: [] as Array<IVatTuModel>
};

export type kcbState = Readonly<typeof initialState>;

// Reducer
// eslint-disable-next-line complexity
export default (state: kcbState = initialState, action): kcbState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_VAT_TU_TON_KHO):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_VAT_TU_TON_KHO):
      return {
        ...state,
        listDanhSachVatTuTonKho: []
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_VAT_TU_TON_KHO): {
      const temp = [];
      action.payload.data &&
        action.payload.data.map(vt => {
          temp.push({
            ...defaultVatTuModel,
            maVatTu: vt.MAVATTU,
            tenVatTu: vt.TENVATTU,
            tenHienThi: vt.TEN_HIEN_THI,
            soLuong: vt.SOLUONG,
            donGia: vt.DONGIA,
            hoatChat: vt.HOATCHAT,
            hamLuong: vt.HAMLUONG,
            dvt: vt.DVT,
            cachSuDung: vt.CACHSUDUNG,
            ngayHetHan: vt.NGAYHETHAN,
            soThau: vt.SOTHAU,
            quyetDinh: vt.QUYETDINH,
            ghiChu: vt.GHICHU,
            ngoaiDanhMuc: vt.NGOAIDANHMUCBHYT, // 0: không    1: Ngoài danh mục
            soLoSanXuat: vt.SOLOSANXUAT,
            dangThuoc: vt.DANGTHUOC,
            nguonDuoc: vt.TEN_NGUONDUOC,
            maVatTuNhap: vt.MAVATTU_NHAP,
            soDangKy: vt.SODANGKY
          });
        });
      return {
        ...state,
        listDanhSachVatTuTonKho: temp
      };
    }
    default:
      return state;
  }
};
// Actions
export const getDanhSachVatTuTonKho = (dvtt: string, searchTerm: string, maKhoVatTu: number) => async dispatch => {
  const requestURL = `${apiURL}api/duoc/getDanhSachDuocTonKho`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_VAT_TU_TON_KHO,
    payload: axios
      .get(requestURL, {
        params: {
          dvtt,
          page: 1,
          rows: 100,
          searchTerm,
          maKhoVatTu,
          maLoaiVatTu: 0,
          maNhomVatTu: -1,
          maPhongBan: -1
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách vật tư tồn kho');
      })
  });
  return result;
};
