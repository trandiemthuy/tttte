import axios from 'axios';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING } from '../../configs';
import notify from 'devextreme/ui/notify';
import { catchStatusCodeFromResponse } from 'app/modules/duoc/duoc-components/utils/funtions';

export const ACTION_TYPES = {
  GET_DANH_SACH_KIEM_TRA_DUYET_NHAN: 'kiemtraduyetnhan/GET_DANH_SACH_KIEM_TRA_DUYET_NHAN'
};

const initialState = {
  listDanhSachKiemTraDuyetNhan: []
};

export type kiemTraDuyetNhanState = Readonly<typeof initialState>;

// Reducer
// eslint-disable-next-line complexity
export default (state: kiemTraDuyetNhanState = initialState, action): kiemTraDuyetNhanState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_KIEM_TRA_DUYET_NHAN):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_KIEM_TRA_DUYET_NHAN):
      return {
        ...state,
        listDanhSachKiemTraDuyetNhan: []
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_KIEM_TRA_DUYET_NHAN):
      return {
        ...state,
        listDanhSachKiemTraDuyetNhan: action.payload.data
      };
    default:
      return state;
  }
};
// Actions
export const getDanhSachKiemTraDuyetNhan = (dvtt: string, trangThai: number) => async dispatch => {
  const requestUrl = `${apiURL}api/kiemtraduyetnhan/getDanhSachPhieuChuyenKhoTheoTrangThai`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_KIEM_TRA_DUYET_NHAN,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt,
          trangThai
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách phiếu chuyển kho');
      })
  });
  return result;
};
