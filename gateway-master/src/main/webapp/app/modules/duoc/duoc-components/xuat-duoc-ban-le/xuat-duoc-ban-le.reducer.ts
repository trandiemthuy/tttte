import axios from 'axios';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING } from '../../configs';
import notify from 'devextreme/ui/notify';
import { defaultXuatDuocBanLeModel, IXuatDuocBanLeModel } from 'app/modules/duoc/duoc-components/xuat-duoc-ban-le/xuat-duoc-ban-le.model';
import { defaultBacSiModel } from 'app/modules/duoc/duoc-components/he-thong/bac-si.model';
import { getDvtt, getMaPhongBan, getMaPhongBenh } from 'app/modules/duoc/duoc-components/utils/thong-tin';
import { catchStatusCodeFromResponse, convertToDate } from 'app/modules/duoc/duoc-components/utils/funtions';
import data from 'devextreme';
import { defaultEnableXuatDuocBanLe, IEnableXuatDuocBanLe } from 'app/modules/duoc/duoc-components/xuat-duoc-ban-le/enable.model';
import { IXuatDuocModel } from 'app/modules/duoc/duoc-components/xuat-duoc/xuat-duoc.model';
import { defaultToaThuocNgoaiTruModel, IToaThuocNgoaiTruModel } from 'app/modules/duoc/duoc-components/kcb/toa-thuoc-ngoai-tru.model';
import { IToaThuocBHYT } from 'app/modules/KhamChuaBenh/KeToaThuoc/ToaThuocBHYT';

export const ACTION_TYPES = {
  GET_DANH_SACH_TOA_THUOC_BAN_LE: 'xuatduocbanle/GET_DANH_SACH_TOA_THUOC_BAN_LE',
  GET_DANH_SACH_CT_TOA_THUOC_BAN_LE: 'xuatduocbanle/GET_DANH_SACH_CT_TOA_THUOC_BAN_LE',
  SET_SELECTED_XUAT_DUOC_BAN_LE: 'xuatduocbanle/SET_SELECTED_XUAT_DUOC_BAN_LE',
  SET_ENABLE_FORM_XUAT_DUOC_BAN_LE: 'xuatduocbanle/SET_ENABLE_FORM_XUAT_DUOC_BAN_LE',
  ADD_TOA_THUOC_BAN_LE: 'xuatduocbanle/ADD_TOA_THUOC_BAN_LE',
  ADD_CT_TOA_THUOC_BAN_LE: 'xuatduocbanle/ADD_CT_TOA_THUOC_BAN_LE',
  DELETE_TOA_THUOC_BAN_LE: 'xuatduocbanle/DELETE_TOA_THUOC_BAN_LE',
  DELETE_CT_TOA_THUOC_BAN_LE: 'xuatduocbanle/DELETE_CT_TOA_THUOC_BAN_LE',
  UPDATE_TOA_THUOC_BAN_LE: 'xuatduocbanle/UPDATE_TOA_THUOC_BAN_LE',
  RESET_SELECTED_XUAT_DUOC: 'xuatduocbanle/RESET_SELECTED_XUAT_DUOC',
  RESET_DANH_SACH_VAT_TU: 'xuatduocbanle/RESET_DANH_SACH_VAT_TU'
};

const initialState = {
  selectedXuatThuocBanLe: defaultXuatDuocBanLeModel,
  listDanhSachToaThuocBanLe: [] as Array<IXuatDuocBanLeModel>,
  listDanhSachCTToaThuocBanLe: [] as Array<IToaThuocNgoaiTruModel>,
  enableForm: defaultEnableXuatDuocBanLe
};

export type xuatDuocBanLeState = Readonly<typeof initialState>;

// Reducer
// eslint-disable-next-line complexity
export default (state: xuatDuocBanLeState = initialState, action): xuatDuocBanLeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_TOA_THUOC_BAN_LE):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_CT_TOA_THUOC_BAN_LE):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_TOA_THUOC_BAN_LE):
      return {
        ...state,
        listDanhSachToaThuocBanLe: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_CT_TOA_THUOC_BAN_LE):
      return {
        ...state,
        listDanhSachCTToaThuocBanLe: []
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_TOA_THUOC_BAN_LE): {
      const temp = [];
      action.payload.data &&
        action.payload.data.map(tt => {
          temp.push({
            ...defaultXuatDuocBanLeModel,
            dvtt: null,
            nghiepVu: 'ngoaitru_toabanle',
            maToaThuoc: tt.STT_TOABANLE,
            tenKhachHang: tt.TEN_KHACH_HANG,
            ngayXuat: tt.NGAY_BAN && convertToDate(tt.NGAY_BAN),
            ghiChu: tt.GHI_CHU,
            noiXuat: null,
            tenNoiXuat: null,
            maBacSi: tt.TEN_BSY,
            tenBacSi: null,
            maICD: tt.ICD_CHINH,
            tenICD: tt.TEN_ICD_CHINH,
            maBenhPhu: null,
            tenBenhPhu: tt.TEN_ICD_PHU,
            maPhongBan: null,
            maPhongBenh: null,
            daThanhToan: tt.TT_THANHTOAN
          });
        });
      return {
        ...state,
        listDanhSachToaThuocBanLe: temp
      };
    }
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_CT_TOA_THUOC_BAN_LE): {
      const temp = [];
      action.payload.data &&
        action.payload.data.map(tt => {
          temp.push({
            ...defaultToaThuocNgoaiTruModel,
            sttToaThuoc: tt.STT_TOATHUOC,
            dvtt: tt.DVTT,
            maToaThuoc: tt.STT_TOABANLE,
            maKhoVatTu: tt.MAKHOVATTU,
            maVatTu: tt.MAVATTU,
            tenVatTu: tt.TEN_VAT_TU,
            tenGoc: tt.HOAT_CHAT,
            nghiepVu: tt.NGHIEP_VU,
            soLuong: tt.SO_LUONG,
            soLuongThucLinh: tt.SO_LUONG_THUC_LINH,
            donGiaBV: tt.DONGIA_BAN_BV,
            donGiaBH: tt.DONGIA_BAN_BH,
            thanhTien: tt.THANHTIEN_THUOC,
            soNgay: tt.SO_NGAY_UONG,
            sang: tt.SANG_UONG,
            trua: tt.TRUA_UONG,
            chieu: tt.CHIEU_UONG,
            toi: tt.TOI_UONG,
            ngayRaToa: tt.NGAY_RA_TOA,
            ghiChu: tt.GHI_CHU_CT_TOA_THUOC,
            maBacSi: tt.MA_BAC_SI_THEMTHUOC,
            daThanhToan: tt.DATHANHTOAN,
            cachSuDung: tt.CACH_SU_DUNG,
            dvt: tt.DVT
          });
        });
      return {
        ...state,
        listDanhSachCTToaThuocBanLe: temp
      };
    }
    case ACTION_TYPES.SET_SELECTED_XUAT_DUOC_BAN_LE: {
      return {
        ...state,
        selectedXuatThuocBanLe: action.data
      };
    }
    case ACTION_TYPES.SET_ENABLE_FORM_XUAT_DUOC_BAN_LE: {
      return {
        ...state,
        enableForm: {
          btnThem: action.data.btnThem,
          btnSua: action.data.btnSua,
          btnLuu: action.data.btnLuu,
          btnHuy: action.data.btnHuy,
          btnXoa: action.data.btnXoa,
          btnIn: action.data.btnIn,
          btnXuatDuoc: action.data.btnXuatDuoc,
          btnHuyXuat: action.data.btnHuyXuat,
          btnThanhToan: action.data.btnThanhToan,
          itemForm: action.data.itemForm
        }
      };
    }
    case SUCCESS(ACTION_TYPES.DELETE_TOA_THUOC_BAN_LE): {
      return {
        ...state,
        selectedXuatThuocBanLe: defaultXuatDuocBanLeModel
      };
    }
    case ACTION_TYPES.RESET_DANH_SACH_VAT_TU: {
      return {
        ...state,
        listDanhSachCTToaThuocBanLe: []
      };
    }
    default:
      return state;
  }
};
// Actions
export const resetDanhSachVatTu = () => {
  return {
    type: ACTION_TYPES.RESET_DANH_SACH_VAT_TU
  };
};
export const setSelectedThongTinXuatDuoc = (xuatDuoc: IXuatDuocBanLeModel) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.SET_SELECTED_XUAT_DUOC_BAN_LE,
    data: xuatDuoc
  });
};
export const setEnableXuatDuocBanLe = (
  btnThem: boolean,
  btnSua: boolean,
  btnLuu: boolean,
  btnHuy: boolean,
  btnXoa: boolean,
  btnIn: boolean,
  btnXuatDuoc: boolean,
  btnHuyXuat: boolean,
  btnThanhToan: boolean,
  itemForm: boolean
) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.SET_ENABLE_FORM_XUAT_DUOC_BAN_LE,
    data: {
      btnThem,
      btnSua,
      btnLuu,
      btnHuy,
      btnXoa,
      btnIn,
      btnXuatDuoc,
      btnHuyXuat,
      btnThanhToan,
      itemForm
    }
  });
};
export const getDanhSachToaThuocBanLe = (dvtt: string, ngay: string) => async dispatch => {
  const requestURL = `${apiURL}api/xuatduocbanle/getDanhSachToaThuocBanLe`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_TOA_THUOC_BAN_LE,
    payload: axios
      .get(requestURL, {
        params: {
          dvtt,
          ngay
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách toa thuốc bán lẻ');
      })
  });
  return result;
};

export const addToaThuocBanLe = (ttXuatDuoc: IXuatDuocBanLeModel, setMaToaThuoc: Function, handleReload: Function) => async dispatch => {
  const requestUrl = `${apiURL}api/xuatduocbanle/addToaThuocBanLe`;
  const result = await dispatch({
    type: ACTION_TYPES.ADD_TOA_THUOC_BAN_LE,
    payload: axios
      .post(requestUrl, JSON.stringify(ttXuatDuoc), {
        headers: { 'content-type': 'application/json' }
      })
      .then(rs => {
        if (rs.data !== -1) {
          notify({ message: 'Thêm toa thuốc bán lẻ thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
          dispatch(setEnableXuatDuocBanLe(false, false, true, true, false, false, false, false, false, false));
          setMaToaThuoc(rs.data);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi thêm toa thuốc bán lẻ');
      })
  });
  handleReload();
  return result;
};

export const updateToaThuocBanLe = (ttXuatDuoc: IXuatDuocBanLeModel, handleReload: Function) => async dispatch => {
  const requestUrl = `${apiURL}api/xuatduocbanle/updateToaThuocBanLe`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.UPDATE_TOA_THUOC_BAN_LE,
    payload: axios
      .post(requestUrl, JSON.stringify(ttXuatDuoc), {
        headers: { 'content-type': 'application/json' }
      })
      .then(rs => {
        if (rs.data === 1) {
          notify(
            { message: 'Cập nhật toa thuốc bán lẻ thành công!', width: NOTIFY_WIDTH, shading: SHADING },
            'success',
            NOTIFY_DISPLAYTIME
          );
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi cập nhật toa thuốc bán lẻ');
      })
  });
  handleReload();
  return reuslt;
};

export const deleteToaThuocBanLe = (ctToaThuoc: IToaThuocNgoaiTruModel, handleReload: Function) => async dispatch => {
  const requestUrl = `${apiURL}api/xuatduocbanle/deleteToaThuocBanLe`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_TOA_THUOC_BAN_LE,
    payload: axios
      .post(requestUrl, JSON.stringify(ctToaThuoc), {
        headers: { 'content-type': 'application/json' }
      })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Xóa toa thuốc bán lẻ thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
          dispatch(setEnableXuatDuocBanLe(true, false, false, false, false, false, false, false, false, true));
          dispatch(resetDanhSachVatTu());
        } else if (rs.data === 1) {
          notify({ message: 'Thuốc đã xác nhận. Không thể xóa!', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else if (rs.data === 2) {
          notify({ message: 'Thuốc đã thanh toán. Không thể xóa!', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else if (rs.data === 3) {
          notify({ message: 'Thuốc đã trả về kho. Không thể xóa!', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi Xóa toa thuốc bán lẻ');
      })
  });
  handleReload();
  return result;
};

export const getDanhSachChiTietThuocBanLe = (dvtt: string, maToaThuoc: string, nghiepVu: string) => async dispatch => {
  const requestURL = `${apiURL}api/xuatduocbanle/getDanhSachChiTietThuocBanLe`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_CT_TOA_THUOC_BAN_LE,
    payload: axios
      .get(requestURL, {
        params: {
          dvtt,
          maToaThuoc,
          nghiepVu
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách chi tiết toa thuốc bán lẻ');
      })
  });
  return result;
};

export const addCTToaThuocBanLe = (ctToaThuoc: IToaThuocNgoaiTruModel, handleReload: Function) => async dispatch => {
  const requestUrl = `${apiURL}api/xuatduocbanle/addChiTietToaThuocBanLe`;
  const result = await dispatch({
    type: ACTION_TYPES.ADD_CT_TOA_THUOC_BAN_LE,
    payload: axios
      .post(requestUrl, JSON.stringify(ctToaThuoc), {
        headers: { 'content-type': 'application/json' }
      })
      .then(rs => {
        if (rs.data !== -1) {
          notify(
            { message: 'Thêm chi tiết toa thuốc bán lẻ thành công!', width: NOTIFY_WIDTH, shading: SHADING },
            'success',
            NOTIFY_DISPLAYTIME
          );
          dispatch(getDanhSachChiTietThuocBanLe(ctToaThuoc.dvtt, ctToaThuoc.maToaThuoc, 'ngoaitru_toabanle'));
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi thêm chi tiết toa thuốc bán lẻ');
      })
  });
  handleReload();
  return result;
};

export const deleteCTToaThuocBanLe = (ctToaThuoc: IToaThuocNgoaiTruModel, handleReload: Function) => async dispatch => {
  const requestUrl = `${apiURL}api/xuatduocbanle/deleteChiTietToaThuocBanLe`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CT_TOA_THUOC_BAN_LE,
    payload: axios
      .post(requestUrl, JSON.stringify(ctToaThuoc), {
        headers: { 'content-type': 'application/json' }
      })
      .then(rs => {
        if (rs.data !== -1) {
          notify(
            { message: 'Xóa chi tiết toa thuốc bán lẻ thành công!', width: NOTIFY_WIDTH, shading: SHADING },
            'success',
            NOTIFY_DISPLAYTIME
          );
          dispatch(getDanhSachChiTietThuocBanLe(ctToaThuoc.dvtt, ctToaThuoc.maToaThuoc, 'ngoaitru_toabanle'));
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi Xóa chi tiết toa thuốc bán lẻ');
      })
  });
  handleReload();
  return result;
};
