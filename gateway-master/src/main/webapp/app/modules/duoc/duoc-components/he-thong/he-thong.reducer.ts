import axios from 'axios';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING } from '../../configs';
import notify from 'devextreme/ui/notify';
import { defaultPhongBanModel, IPhongBanModel } from 'app/modules/duoc/duoc-components/he-thong/phong-ban.model';
import { defaultKhoVatTuModel, IKhoVatTuModel } from 'app/modules/duoc/duoc-components/he-thong/kho-vat-tu.model';
import { defaultBacSiModel, IBacSiModel } from 'app/modules/duoc/duoc-components/he-thong/bac-si.model';
import { catchStatusCodeFromResponse } from 'app/modules/duoc/duoc-components/utils/funtions';

export const ACTION_TYPES = {
  GET_DANH_SACH_KHOA_NOI_TRU: 'hethong/GET_DANH_SACH_KHOA_NOI_TRU',
  GET_DANH_SACH_KHO_NOI_TRU: 'hethong/GET_DANH_SACH_KHO_NOI_TRU',
  GET_DANH_SACH_BAC_SI: 'hethong/GET_DANH_SACH_BAC_SI'
};

const initialState = {
  listDanhSachKhoaNoiTru: [] as Array<IPhongBanModel>,
  listDanhSachKhoNoiTru: [] as Array<IKhoVatTuModel>,
  listDanhSachBacSi: [] as Array<IBacSiModel>
};

export type heThongState = Readonly<typeof initialState>;

// Reducer
// eslint-disable-next-line complexity
export default (state: heThongState = initialState, action): heThongState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_KHOA_NOI_TRU):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_KHO_NOI_TRU):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_BAC_SI):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_KHOA_NOI_TRU):
      return {
        ...state,
        listDanhSachKhoaNoiTru: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_KHO_NOI_TRU):
      return {
        ...state,
        listDanhSachKhoNoiTru: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_BAC_SI):
      return {
        ...state,
        listDanhSachBacSi: []
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_KHOA_NOI_TRU): {
      const temp = [];
      action.payload.data &&
        action.payload.data.map(pb => {
          temp.push({
            ...defaultPhongBanModel,
            maPhongBan: pb.MA_PHONGBAN,
            tenPhongBan: pb.TEN_PHONGBAN
          });
        });
      return {
        ...state,
        listDanhSachKhoaNoiTru: temp
      };
    }
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_KHO_NOI_TRU): {
      const temp = [];
      action.payload.data &&
        action.payload.data.map(pb => {
          temp.push({
            ...defaultKhoVatTuModel,
            maKhoVatTu: pb.MAKHO,
            tenKhoVatTu: pb.TENKHO
          });
        });
      return {
        ...state,
        listDanhSachKhoNoiTru: temp
      };
    }
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_BAC_SI): {
      const temp = [];
      temp.push({ maBacSi: '-1', tenBacSi: '--Tự kê đơn--' });
      action.payload.data &&
        action.payload.data.map(pb => {
          temp.push({
            ...defaultBacSiModel,
            maBacSi: pb.MA_NHANVIEN,
            tenBacSi: pb.TEN_IN
          });
        });
      return {
        ...state,
        listDanhSachBacSi: temp
      };
    }
    default:
      return state;
  }
};
// Actions
export const getDanhSachKhoaNoiTru = (dvtt: string) => async dispatch => {
  const requestURL = `${apiURL}api/hethong/getDanhSachKhoaNoiTru`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_KHOA_NOI_TRU,
    payload: axios
      .get(requestURL, {
        params: {
          dvtt
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách khoa nội trú');
      })
  });
  return result;
};

export const getDanhSachKhoNoiTru = (dvtt: string, maPhongBan: string, nghiepVu: string) => async dispatch => {
  const requestURL = `${apiURL}api/hethong/getDanhSachKhoNoiTru`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_KHO_NOI_TRU,
    payload: axios
      .get(requestURL, {
        params: {
          dvtt,
          maPhongBan,
          nghiepVu
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách kho nội trú');
      })
  });
  return result;
};
export const getDanhSachBacSi = (dvtt: string) => async dispatch => {
  const requestURL = `${apiURL}api/hethong/getDanhSachBacSi`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_BAC_SI,
    payload: axios
      .get(requestURL, {
        params: {
          dvtt
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách bác sĩ');
      })
  });
  return result;
};
