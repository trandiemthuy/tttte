import React from 'react';
import {IDuocDropDownButton} from "./button-dropdown.model";
import { DropDownButton } from 'devextreme-react';
import { LoadPanel } from 'devextreme-react/load-panel';

export interface IDuocDropDownButtonProp {
  visible?: boolean;
  text: string;
  arrayButton: IDuocDropDownButton[];
  defaultHandleOnClick: Function;
  dropDownWidth?: string | number;
  width?: string | number;
  disabled?: boolean;
  className?: any;
  loadingConfigs: {
    visible: boolean;
    position: string;
  }
  icon?: string;
}

export interface IDuocDropDownButtonState {
  state: any;
}

export class DuocDropDownButton extends React.Component<IDuocDropDownButtonProp, IDuocDropDownButtonState> {
  constructor(props) {
    super(props);
  }

  handleOnItemClick = (item) => {
    if (item.itemData !== undefined) {
      item.itemData.handleOnClick();
    }
  };
  handleOnButtonClick = (e) => {
    this.props.defaultHandleOnClick();
  };

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return <>
      <DropDownButton
        icon={this.props.icon === undefined ? '' : this.props.icon}
        visible={this.props.visible === undefined ? true : this.props.visible}
        disabled={this.props.disabled === undefined ? false : this.props.disabled}
        width={this.props.width === null ? 'auto' : this.props.width}
        stylingMode={"outlined"}
        splitButton={true}
        useSelectMode={false}
        text={this.props.text}
        items={this.props.arrayButton}
        displayExpr="title"
        keyExpr="id"
        onButtonClick={this.handleOnButtonClick}
        onItemClick={this.handleOnItemClick}
        dropDownOptions={{
          width: this.props.dropDownWidth === null ? 'auto' : this.props.dropDownWidth
        }}
        style={{
          color: 'white'
        }}
        className={this.props.className}
      />
      <LoadPanel
        shadingColor="rgba(0,0,0,0.2)"
        position={{of: this.props.loadingConfigs !== undefined ? this.props.loadingConfigs.position : ''}}
        visible={this.props.loadingConfigs !== undefined ? this.props.loadingConfigs.visible : false}
        showIndicator={true}
        shading={true}
        showPane={false}
        closeOnOutsideClick={false}
      />
    </>
  }
}

export default DuocDropDownButton;
