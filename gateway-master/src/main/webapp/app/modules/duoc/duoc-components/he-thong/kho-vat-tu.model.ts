export interface IKhoVatTuModel {
  maKhoVatTu?: number;
  tenKhoVatTu?: string;
}

export const defaultKhoVatTuModel: IKhoVatTuModel = {
  maKhoVatTu: null,
  tenKhoVatTu: ''
};
