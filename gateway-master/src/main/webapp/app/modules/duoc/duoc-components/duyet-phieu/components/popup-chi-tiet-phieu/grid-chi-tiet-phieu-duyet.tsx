import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection,
  Editing, Pager, Paging
} from 'devextreme-react/data-grid';
import {Button} from 'devextreme-react';
import {configLoadPanel, numberFormat, pageSizes} from "app/modules/duoc/configs";

export interface IGridChiTietPhieuDuyetProp extends IDataGridOptions {
  listDanhSachChiTietPhieuDuyet: any;
  duyet: number;
  isNhan?: boolean;
}
export interface IGridChiTietPhieuDuyetState {
  listDanhSachChiTietChinhSua: any;
}

export class GridChiTietPhieuDuyet extends React.Component<IGridChiTietPhieuDuyetProp, IGridChiTietPhieuDuyetState> {
  private refGridDanhSach = React.createRef<DataGrid>();
  constructor(props) {
    super(props);
  }

  getDanhSachChiTietChinhSua = () => {
    return this.refGridDanhSach.current.instance.getDataSource().items();
  };

  render() {
    return (
        <DataGrid
          loadPanel={configLoadPanel}
          ref={this.refGridDanhSach}
          dataSource={this.props.listDanhSachChiTietPhieuDuyet}
          showBorders={true}
          showColumnLines={true}
          rowAlternationEnabled={true}
          allowColumnReordering={true}
          allowColumnResizing={true}
          columnResizingMode={'nextColumn'}
          columnMinWidth={50}
          {...this.props}
        >
          <Selection mode="single" />
          <FilterRow visible={true}
                     applyFilter={'auto'} />
          <HeaderFilter visible={true} />
          <Editing mode="cell" allowUpdating={this.props.isNhan ? false : true} selectTextOnEditStart={true} />
          <Column
            dataField="ID_CHUYEN_KHO_VAT_TU_CT"
            caption="ID chuyển kho vật tư chi tiết"
            dataType="string"
            alignment="left"
            width={'0%'}
            visible={false}
          />
          <Column
            dataField="MACHUYENKHOVATTU"
            caption="Mã chuyển kho vật tư"
            dataType="string"
            alignment="left"
            width={'0%'}
            visible={false}
          />
          <Column
            dataField="TENVATTU"
            caption="Tên vật tư"
            dataType="string"
            alignment="left"
            width={'15%'}
            allowEditing={false}
          />
          <Column
            dataField="DVT"
            caption="ĐVT"
            dataType="string"
            alignment="left"
            width={'5%'}
            allowEditing={false}
          />
          <Column
            dataField="SOLUONG"
            caption="SL yêu cầu"
            dataType="string"
            alignment="left"
            width={'10%'}
            allowEditing={false}
            format={numberFormat}
          />
          <Column
            dataField="SOLUONG_DUYET"
            caption="SL duyệt"
            dataType="string"
            alignment="left"
            width={'10%'}
            allowEditing={this.props.duyet === 0}
            format={numberFormat}
          />
          <Column
            dataField="SL_TON"
            caption="SL tồn"
            dataType="string"
            alignment="left"
            width={'10%'}
            allowEditing={false}
            format={numberFormat}
          />
          <Column
            dataField="SOLOSANXUAT"
            caption="Số lô"
            dataType="string"
            alignment="left"
            width={'10%'}
            allowEditing={false}
          />
          <Column
            dataField="NGAYHETHAN"
            caption="Hạn dùng"
            dataType="string"
            alignment="left"
            width={'10%'}
            allowEditing={false}
          />
          <Column
            dataField="DON_GIA"
            caption="Đơn giá"
            dataType="number"
            alignment="left"
            width={'10%'}
            allowEditing={false}
            format={numberFormat}
          />
          <Column
            dataField="THANHTIEN"
            caption="Thành tiền"
            dataType="number"
            alignment="left"
            width={'10%'}
            allowEditing={false}
            format={numberFormat}
          />
          <Column
            dataField="NGUONDUOC"
            caption="Nguồn dược"
            dataType="string"
            alignment="left"
            allowEditing={false}
            width={'10%'}
          />
          <Summary>
            <TotalItem
              displayFormat={'{0} loại vật tư'}
              column="MACHUYENKHOVATTU"
              showInColumn="TENVATTU"
              summaryType="count"
            />
            <TotalItem
              displayFormat={'{0}'}
              column="THANHTIEN"
              summaryType="sum"
            />
          </Summary>
          <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
          <Paging defaultPageSize={5} />
        </DataGrid>);
  }
}

export default GridChiTietPhieuDuyet;
