import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, Summary, TotalItem, Selection, Pager, Paging
} from 'devextreme-react/data-grid';
import {configLoadPanel, pageSizes} from '../../../../configs';
import {translate} from "react-jhipster";
import {
  defaultXuatDuocBanLeModel,
  IXuatDuocBanLeModel
} from "app/modules/duoc/duoc-components/xuat-duoc-ban-le/xuat-duoc-ban-le.model";
import {setEnableXuatDuocBanLe} from "app/modules/duoc/duoc-components/xuat-duoc-ban-le/xuat-duoc-ban-le.reducer";
import {getDvtt, getMaNhanVien, getMaPhongBan, getMaPhongBenh} from "app/modules/duoc/duoc-components/utils/thong-tin";

export interface IGridDanhSachKhachHangBanLeProp extends IDataGridOptions {
  listDanhSachToaThuocBanLe: any;
  handleOnContextMenuGridDanhSach: any;
  getDanhSachChitietToaBanLe: Function;
  setSelectedThongTinXuatDuoc: Function;
  setEnableXuatDuocBanLe: Function;
  resetDanhSachVatTu: Function;
}
export interface IGridDanhSachKhachHangBanLeState {
  isSelected: boolean;
}

export class GridDanhSachKhachHangBanLe extends React.Component<IGridDanhSachKhachHangBanLeProp, IGridDanhSachKhachHangBanLeState> {
  constructor(props) {
    super(props);
    this.state = {
      isSelected: false
    };
  }

  // Handles
  handleOnRowRepared = (e) => {
    if (e.rowType === 'data' && e.data.daThanhToan === 1) { // Bệnh nhân đã thanh toán
      e.rowElement.style.backgroundImage = 'radial-gradient( circle 939px at 0.7% 2.4%,  rgba(116,106,255,1) 0%, rgba(221,221,221,1) 100.2% )';
      e.rowElement.style.color = 'white';
      e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
    }
  };
  handleOnSelectKhachHangXuatDuoc = (e) => {
    const selectedKhachHang = e.selectedRowsData[0];
    if (selectedKhachHang === undefined) return;
    this.props.setSelectedThongTinXuatDuoc({
      ...selectedKhachHang,
      dvtt: getDvtt(),
      maPhongBan: getMaPhongBan(),
      maPhongBenh: getMaPhongBenh(),
      maNhanVien: getMaNhanVien()
    });
    this.props.getDanhSachChitietToaBanLe(getDvtt(), e.selectedRowsData[0].maToaThuoc, 'ngoaitru_toabanle');
    this.props.setEnableXuatDuocBanLe(false, true, false, true, true, true, true, false, true, false);
    this.setState({
      isSelected: true
    });
  };
  handleOnRowClick = (e) => {
    if (!this.state.isSelected) { // Hủy chọn row đang chọn
      const keys = e.component.getSelectedRowKeys();
      e.component.deselectRows(keys);
      this.props.setSelectedThongTinXuatDuoc(defaultXuatDuocBanLeModel);
      this.props.setEnableXuatDuocBanLe(true, false, false, false, false, false, false, false, false, true);
      this.props.resetDanhSachVatTu();
    }
    this.setState({
      isSelected: false
    });
  };

  render() {
    return (
      <DataGrid
        loadPanel={configLoadPanel}
        dataSource={this.props.listDanhSachToaThuocBanLe}
        showBorders={true}
        showRowLines={true}
        showColumnLines={true}
        rowAlternationEnabled={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        columnMinWidth={50}
        onContextMenuPreparing={this.props.handleOnContextMenuGridDanhSach}
        noDataText={'Không có dữ liệu'}
        onRowPrepared={this.handleOnRowRepared}
        onSelectionChanged={this.handleOnSelectKhachHangXuatDuoc}
        onRowClick={this.handleOnRowClick}
        wordWrapEnabled={true}
        {...this.props}
      >
        <Selection mode="single" />
        <FilterRow visible={true}
                   applyFilter={'auto'} />
        <HeaderFilter visible={true} />
        <Column
          dataField={'maToaThuoc'}
          caption={translate('duoc.duocXuatDuocBanLe.formLoc.maToaThuoc')}
          dataType="string"
          alignment="left"
          width={'30%'}
        />
        <Column
          dataField="tenKhachHang"
          caption={translate('duoc.duocXuatDuocBanLe.formLoc.tenKhachHang')}
          dataType="string"
          alignment="left"
          width={'70%'}
        />
        <Summary>
          <TotalItem
            displayFormat={'{0} ' + translate('duoc.duocXuatDuocBanLe.formLoc.tong')}
            showInColumn={'tenKhachHang'}
            column={'maToaThuoc'}
            summaryType="count"
          />
        </Summary>
        <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
        <Paging defaultPageSize={5} />
      </DataGrid>);
  }
}

export default GridDanhSachKhachHangBanLe;
