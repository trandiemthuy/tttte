import React from 'react';
import { Switch } from 'react-router-dom';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import XuatDuocComponent from "./xuat-duoc";

const Routes = ({ match }) => (

  <Switch>
    <ErrorBoundaryRoute
      path={`${match.url}/:nghiepVu`}
      component={XuatDuocComponent}>
    </ErrorBoundaryRoute>
  </Switch>
);

export default Routes;
