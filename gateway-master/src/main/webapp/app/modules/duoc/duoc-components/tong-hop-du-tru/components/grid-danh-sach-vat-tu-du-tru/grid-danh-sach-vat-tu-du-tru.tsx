import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection, ColumnFixing, Pager, Paging
} from 'devextreme-react/data-grid';
import {numberFormat, pageSizes} from '../../../../configs';

export interface IGridDanhSachVatTuDuTruProp extends IDataGridOptions {
  listDanhSachVatTuDuTru: any;
  // loaiPhieu: number; // 0: Dự trù nội trú     1: Hoàn trả nội trú
}
export interface IGridDanhSachVatTuDuTruState {
  listDanhSachPhieuDuTruHoanTra: any;
}

export class GridDanhSachVatTuDuTru extends React.Component<IGridDanhSachVatTuDuTruProp, IGridDanhSachVatTuDuTruState> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <DataGrid
        dataSource={this.props.listDanhSachVatTuDuTru}
        showBorders={true}
        showRowLines={true}
        showColumnLines={true}
        rowAlternationEnabled={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        columnMinWidth={50}
        noDataText={'Không có dữ liệu'}
        wordWrapEnabled={true}
        {...this.props}
      >
        <Selection mode="single" />
        <FilterRow visible={true}
                   applyFilter={'auto'} />
        <HeaderFilter visible={true} />
        <Column
          dataField={"MAVATTU"}
          caption="Mã vật tư"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField={"TEN_VAT_TU"}
          caption="Tên vật tư"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="HOAT_CHAT"
          caption="Hoạt chất"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="SO_LUONG"
          caption="Số lượng"
          dataType="number"
          alignment="left"
          format={numberFormat}
        />
        <Column
          dataField="DONGIA_BAN_BV"
          caption="Đơn giá"
          dataType="number"
          alignment="left"
          format={numberFormat}
        />
        <Summary>
          <TotalItem
            displayFormat={'{0} dược/vật tư'}
            showInColumn={'TEN_VAT_TU'}
            column={'MAVATTU'}
            summaryType="count"
          />
        </Summary>
        <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
        <Paging defaultPageSize={5} />
      </DataGrid>);
  }
}

export default GridDanhSachVatTuDuTru;
