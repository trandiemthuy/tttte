import { IPhieuNhapKho } from 'app/modules/duoc/duoc-components/nhap-kho/nhap-kho.model';

export interface INhapKhoTuNhaCungCapState {
  listPhieuNhapKho?: any;
  tuNgay?: string;
  denNgay?: string;
  phieuNhapObj?: IPhieuNhapKho;
  btnLamMoi?: boolean;
  btnThem?: boolean;
  btnSua?: boolean;
  btnLuu?: boolean;
  btnHuy?: boolean;
  btnXoa?: boolean;
  btnIn?: boolean;
  btnInXLS?: boolean;
  btnXuatDM?: boolean;
  btnInBBKN?: boolean;
  btnInBBKNNP?: boolean;
  isGo?: boolean; // Trạng thái được gõ dược
}
