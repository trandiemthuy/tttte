import React from 'react';
import {RouteComponentProps} from "react-router";
import {IRootState} from "app/shared/reducers";
import {connect} from "react-redux";
import Box, {Item as BoxItem} from "devextreme-react/box";
import {
  getDanhSachKhoaNoiTru
} from '../he-thong/he-thong.reducer';
import {
  getDanhSachBenhNhanXuatDuoc,
  getDanhSachVatTuXuatDuoc,
  resetDanhSachVatTu,
  xuatDuoc,
  printPhieuHoanTraThuoc,
  exportDanhSachPhieuXuat,
  traThuocVeKho
} from './xuat-duoc.reducer';
import {
  getDvtt, getMaNhanVien, getMaPhongBan, getMaPhongBenh, getTenBenhVien, getTenTinh
} from "app/modules/duoc/duoc-components/utils/thong-tin";
import {convertToSQLDate, simpleIf, simpleIfFunction} from "app/modules/duoc/duoc-components/utils/funtions";
import {Button, CheckBox, SelectBox} from "devextreme-react";
import DuocTitleComponent from "app/modules/duoc/duoc-components/utils/duoc-title/duoc-title";
import {DuocDropDownButton} from "app/modules/duoc/duoc-components/utils/button-dropdown/button-dropdown";
import DuocLabelComponent from "app/modules/duoc/duoc-components/utils/duoc-label/duoc-label";
import GridDanhSachBenhNhanXuatDuoc
  from "app/modules/duoc/duoc-components/xuat-duoc/components/grid-danh-sach-benh-nhan/grid-danh-sach-benh-nhan";
import {
  defaultXuatDuocModel,
  IXuatDuocModel
} from "app/modules/duoc/duoc-components/xuat-duoc/xuat-duoc.model";
import AdvanceDateBox from "app/modules/duoc/duoc-components/utils/advance-datebox/advance-datebox";
import FormThongTinXuatDuoc
  from "app/modules/duoc/duoc-components/xuat-duoc/components/form-thong-tin-xuat-duoc/form-thong-tin-xuat-duoc";
import GridDanhSachVatTuXuatDuoc
  from "app/modules/duoc/duoc-components/xuat-duoc/components/grid-danh-sach-vat-tu/grid-danh-sach-vat-tu";
import {translate} from "react-jhipster";

export interface IXuatDuocComponentProp extends StateProps, DispatchProps, RouteComponentProps<{ nghiepVu: string; }> {
  account: any;
}

export interface IXuatDuocComponentState {
  trangThaiPhieu: number;  // 0: Chưa xuất dược     1: Đã xuất dược
  maKhoa: string;
  loadingConfigs: {
    visible: boolean;
    position: string;
  };
  enableButton: boolean;
  printPopUp: {
    loaiIn: number; // 0: In phiếu dự trù nội trú    3: In phiếu hoàn trả
    visible: boolean;
    loaiFile: string;
  };
  sendDataToForm: {
    send: boolean;
    ttXuatDuoc: IXuatDuocModel
  }
}

export class XuatDuocComponent extends React.Component<IXuatDuocComponentProp, IXuatDuocComponentState> {
  private refNgayLap = React.createRef<AdvanceDateBox>();
  private refDanhSachBenhNhan = React.createRef<GridDanhSachBenhNhanXuatDuoc>();
  constructor(props) {
    super(props);
    this.state = {
      trangThaiPhieu: 0,
      maKhoa: '-1',
      loadingConfigs: {
        visible: false,
        position: 'body'
      },
      enableButton: false,
      printPopUp: {
        loaiIn: 0,
        visible: false,
        loaiFile: 'pdf'
      },
      sendDataToForm: {
        send: false,
        ttXuatDuoc: defaultXuatDuocModel
      }
    };
  }

  componentDidMount(): void {
    this.getDanhSachKhoaNoiTru();
    this.handleOnClickButtonLamMoi();
  }

  componentDidUpdate(prevProps: Readonly<IXuatDuocComponentProp>, prevState: Readonly<IXuatDuocComponentState>, snapshot?: any): void {
    if (prevProps.match.params.nghiepVu !== this.props.match.params.nghiepVu) {
      this.props.resetDanhSachVatTu();
      this.handleOnClickButtonLamMoi();
    }
  }

  // Handles
  handleSendDataToForm = (ttXuatDuoc: IXuatDuocModel) => {
    this.setState({
      sendDataToForm: {
        send: true,
        ttXuatDuoc
      }
    }, () => {
      this.setState({
        sendDataToForm: {
          send: false,
          ttXuatDuoc: defaultXuatDuocModel
        }
      });
    });
  };
  handleOnChangeKhoa = (e) => {
    this.setState({
      maKhoa: e.selectedItem.MA_PHONGBAN
    });
    this.handleOnClickButtonLamMoi();
  };
  handleChangeTrangThaiPhieu = (e) => {
    this.setState({
      trangThaiPhieu: e.value ? 1 : 0
    }, () => {
      this.handleOnClickButtonLamMoi();
    });
  };
  handleOnClickButtonLamMoi = () => {
    this.props.resetDanhSachVatTu();
    this.getDanhSachBenhNhanXuatDuoc();
  };
  handleOnClickButtonHuy = (e) => {
    this.setEnableButton(false);
  };
  handleLoading = (visible: boolean) => {
    this.setState({
      loadingConfigs: {
        ...this.state.loadingConfigs,
        visible
      }
    });
  };
  handleOnContextMenuGridDanhSach = (e) => {
    if (e.row.data === undefined) {
      return;
    }
    e.component.selectRowsByIndexes(e.rowIndex);
    const selectedPhieu = e.row.data;
    const trangThaiPhieu = this.state.trangThaiPhieu;
    if (e.row.rowType === 'data') {
      e.items = [
        {
          icon: 'print',
          text: "In danh sách phiếu xuất dược",
          items: [{
            icon: 'exportpdf',
            text: "In danh sách phiếu xuất dược (PDF)",
            onItemClick: () => {
              this.handleOnClickMenuInDanhSachXuatDuoc('pdf');
            }
          },
            {
              icon: 'exportxlsx',
              text: "In danh sách phiếu xuất dược (XLS)",
              onItemClick: () => {
                this.handleOnClickMenuInDanhSachXuatDuoc('pdf');
              }
            },
            {
              icon: 'docfile',
              text: "In danh sách phiếu xuất dược (RTF)",
              onItemClick: () => {
                this.handleOnClickMenuInDanhSachXuatDuoc('pdf');
              }
            }]
        },
        {
          icon: 'exportxlsx',
          text: "Xuất danh sách phiếu xuất tổng hợp",
          onItemClick: () => {
            this.handleOnClickMenuDanhSachPhieuXuat('tonghop');
          }
        },
        {
          icon: 'exportxlsx',
          text: "Xuất danh sách phiếu xuất chi tiết",
          onItemClick: () => {
            this.handleOnClickMenuDanhSachPhieuXuat('chitiet');
          }
        },
        {
          icon: 'redo',
          text: "Trả thuốc về kho",
          onItemClick: () => {
            this.handleOnClickMenuTraThuocVeKho();
          }
        }
      ];
    }
  };
  handleOnClickButtonXuatDuoc = () => {
    this.props.xuatDuoc(
      {...this.refDanhSachBenhNhan.current.getSelectedBenhNhanXuatDuoc(),
        dvtt: getDvtt(),
        maNghiepVu: parseInt(this.props.match.params.nghiepVu, 0),
        maKhoa: getMaPhongBan(),
        maPhong: getMaPhongBenh(),
        maNguoiXuat: getMaNhanVien()
      }, this.state.trangThaiPhieu,
      () => {
        this.getDanhSachBenhNhanXuatDuoc();
        this.getDanhSachVatTuXuatDuoc(this.refDanhSachBenhNhan.current.getSelectedBenhNhanXuatDuoc(), this.state.trangThaiPhieu);
      }
    );
  };
  handleOnClickButtonInToaThuoc = (loaiFile: string) => {

  };
  handleOnClickButtonInToaThuocBS = (loaiFile: string) => {

  };
  handleOnClickButtonInToaTPCN = (loaiFile: string) => {

  };
  handleOnClickButtonInToaN = (loaiFile: string) => {

  };
  handleOnClickButtonInToaH = (loaiFile: string) => {

  };
  handleOnClickButtonInBangKe = (loaiFile: string) => {

  };

  handleOnClickMenuInDanhSachXuatDuoc = (loaiFile: string) => {
    this.props.printPhieuHoanTraThuoc(
      getDvtt(),
      this.refNgayLap.current.getValue(),
      this.props.match.params.nghiepVu === '38' ? 'ngoaitru_toabanle' :  this.props.match.params.nghiepVu === '44' ? 'ba_ngoaitru_toathuoc' : 'ngoaitru_toathuoc',
      getTenTinh(),
      getTenBenhVien(),
      loaiFile,
      this.handleLoading);
  };

  handleOnClickMenuDanhSachPhieuXuat = (loaiPhieu: string) => {
    this.props.exportDanhSachPhieuXuat(
      getDvtt(),
      this.refNgayLap.current.getValue(),
      this.props.match.params.nghiepVu === '38' ? 'ngoaitru_toabanle' :  this.props.match.params.nghiepVu === '44' ? 'ba_ngoaitru_toathuoc' : 'ngoaitru_toathuoc',
      loaiPhieu,
      this.handleLoading
    )
  };
  handleOnClickMenuTraThuocVeKho = () => {
    this.props.traThuocVeKho(
      {
        ...this.refDanhSachBenhNhan.current.getSelectedBenhNhanXuatDuoc(),
        dvtt: getDvtt(),
        maNghiepVu: parseInt(this.props.match.params.nghiepVu, 0),
        maKhoa: getMaPhongBan(),
        maPhong: getMaPhongBenh(),
        maNguoiXuat: getMaNhanVien()
      }, this.props.match.params.nghiepVu === '38' ? 'ngoaitru_toabanle' :  this.props.match.params.nghiepVu === '44' ? 'ba_ngoaitru_toathuoc' : 'ngoaitru_toathuoc',
      () => {
        this.getDanhSachBenhNhanXuatDuoc();
        this.getDanhSachVatTuXuatDuoc(this.refDanhSachBenhNhan.current.getSelectedBenhNhanXuatDuoc(), this.state.trangThaiPhieu);
      }
    );
  };

  // Funtion
  setEnableButton = (enable: boolean) => {
    this.setState({
      enableButton: enable
    });
  };
  getDanhSachKhoaNoiTru = () => {
    this.props.getDanhSachKhoaNoiTru(getDvtt());
  };
  getDanhSachBenhNhanXuatDuoc = () => {
    // if (!this.refNgayLap.current) return;
    this.props.getDanhSachBenhNhanXuatDuoc(
      getDvtt(),
      parseInt(this.props.match.params.nghiepVu, 0),
      this.refNgayLap.current === null ? convertToSQLDate(new Date().toDateString()) : this.refNgayLap.current.getValue(),
      this.state.trangThaiPhieu,
      this.state.maKhoa === null ? '-1' : this.state.maKhoa
    );
  };
  getDanhSachVatTuXuatDuoc = (benhNhan: IXuatDuocModel, trangThai: number) => {
    this.props.getDanhSachVatTuXuatDuoc(
      getDvtt(),
      parseInt(this.props.match.params.nghiepVu, 0),
      benhNhan.maToaThuoc,
      trangThai,
      benhNhan.soVaoVien
    );
  };

  getTenNghiepVu = () => {
    const maNghiepVu = parseInt(this.props.match.params.nghiepVu, 0);
    let tenNghiepVu = '';
    switch (maNghiepVu) {
      case 20: {
        tenNghiepVu = translate('duoc.duocXuatDuoc.titleChucNangBHYT');
        break;
      }
      default: {
        break;
      }
    }
    return tenNghiepVu;
  };


  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (<div>
      <DuocTitleComponent text={this.getTenNghiepVu()}/>
      <Box
        direction="row"
        width="100%"
        height={'auto'}
      >
        <BoxItem baseSize={'30%'} ratio={0}>
          <div className={'div-box-wrapper'}>
            <DuocLabelComponent text={translate('duoc.duocXuatDuoc.formLoc.titleLoc')}/>
            <div className={'row-margin-top-5'}/>
            <Box
              direction={'col'}
              width={'100%'}
              height={'auto'}
            >
              <BoxItem
                ratio={0}
                baseSize={'100%'}
              >
                <div className={'row-margin-top-5'} style={{display: 'flex'}}>
                  <div className={'col-align-right'} style={{width: '15%'}}>
                    {translate('duoc.duocXuatDuoc.formLoc.ngayLap')}
                  </div>
                  <div style={{width: '60%'}}>
                    <AdvanceDateBox ref={this.refNgayLap}/>
                  </div>
                  <div className={'row-margin-left-5'} style={{width: '25%'}}>
                    <Button type={'default'} text={translate('duoc.duocXuatDuoc.formLoc.btnLamMoi')} icon={'refresh'} onClick={this.handleOnClickButtonLamMoi}/>
                  </div>
                </div>
              </BoxItem>
              <BoxItem
                ratio={0}
                baseSize={'100%'}
              >
                <div className={'row-margin-top-5'} style={{display: 'flex'}}>
                  <div className={'col-align-right'} style={{width: '15%'}}>
                    {translate('duoc.duocXuatDuoc.formLoc.khoa')}
                  </div>
                  <div style={{width: '60%'}}>
                    <SelectBox
                      dataSource={this.props.listDanhSachKhoaNoiTru}
                      valueExpr={'maPhongBan'}
                      displayExpr={'tenPhongBan'}
                      placeholder={'Chọn khoa xuất dược...'}
                      onSelectionChanged={this.handleOnChangeKhoa}
                      defaultValue={'-1'}
                    />
                  </div>
                  <div className={'row-margin-left-5 row-margin-top-5'} style={{width: '25%'}}>
                    <CheckBox text={translate('duoc.duocXuatDuoc.formLoc.daXuat')} onValueChanged={this.handleChangeTrangThaiPhieu}/>
                  </div>
                </div>
              </BoxItem>
            </Box>
          </div>
          <div className={'div-box-wrapper row-margin-top-5 div-height-100'}>
            <DuocLabelComponent text={translate('duoc.duocXuatDuoc.titleDanhSachBenhNhan')}/>
            <GridDanhSachBenhNhanXuatDuoc
              ref={this.refDanhSachBenhNhan}
              listDanhSachBenhNhanXuatDuoc={this.props.listDanhSachBenhNhanXuatDuoc}
              resetDanhSachVatTu={this.props.resetDanhSachVatTu}
              setEnableButton={this.setEnableButton}
              handleOnContextMenuGridDanhSach={this.handleOnContextMenuGridDanhSach}
              getDanhSachVatTuXuatDuoc={this.getDanhSachVatTuXuatDuoc}
              handleSendDataToForm={this.handleSendDataToForm}
            />
            <div style={{display: 'flex', width: '100%'}}>
              <div style={{width: '30px', height: '30px', marginTop: '5px', backgroundImage: 'radial-gradient( circle 939px at 0.7% 2.4%,  rgba(116,106,255,1) 0%, rgba(221,221,221,1) 100.2% )'}}>
              </div>
              <div style={{marginTop: '10px'}}>Bệnh nhân vượt cận</div>
            </div>
            <div style={{display: 'flex', width: '100%'}}>
              <div style={{width: '30px', height: '30px', marginTop: '5px', backgroundImage: 'radial-gradient( circle 534px at 7.8% 17.6%,  rgba(254,253,112,1) 1.7%, rgba(248,143,111,1) 91.8% )'}}>
              </div>
              <div style={{marginTop: '10px'}}>Bệnh nhân đã trả thuốc về kho</div>
            </div>
          </div>
        </BoxItem>
        <BoxItem baseSize={'70%'} ratio={0}>
          <div className={'div-box-wrapper row-margin-left-5'}>
            <DuocLabelComponent icon={'bookmark'} text={translate('duoc.duocXuatDuoc.titleFormThongTin')}/>
            <FormThongTinXuatDuoc
              sendDataToForm={this.state.sendDataToForm}
            />
          </div>
          <div className={'div-button div-box-wrapper row-margin-left-5 row-margin-top-5'}>
            <Button
              icon={this.state.trangThaiPhieu === 0 ? 'check' : 'close'}
              type={this.state.trangThaiPhieu === 0 ? 'success' : 'danger'}
              text={this.state.trangThaiPhieu === 0 ? translate('duoc.duocXuatDuoc.btnXuatThuoc') : translate('duoc.duocXuatDuoc.btnHuyXuatThuoc')}
              disabled={!this.state.enableButton}
              className={'btn-margin-5 button-duoc'}
              onClick={this.handleOnClickButtonXuatDuoc}
            />
            <DuocDropDownButton
              icon={'print'}
              loadingConfigs={this.state.loadingConfigs}
              disabled={!this.state.enableButton}
              text={translate('duoc.duocXuatDuoc.btnInToaThuoc')}
              arrayButton={[
                { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInToaThuoc('pdf')},
                { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInToaThuoc('xls')},
                { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInToaThuoc('rtf')}
              ]}
              defaultHandleOnClick={() => this.handleOnClickButtonInToaThuoc('pdf')}
              className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
            />
            <DuocDropDownButton
              icon={'print'}
              loadingConfigs={this.state.loadingConfigs}
              disabled={!this.state.enableButton}
              text={translate('duoc.duocXuatDuoc.btnInToaTPCN')}
              arrayButton={[
                { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInToaTPCN('pdf')},
                { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInToaTPCN('xls')},
                { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInToaTPCN('rtf')}
              ]}
              defaultHandleOnClick={() => this.handleOnClickButtonInToaTPCN('pdf')}
              className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
            />
            <DuocDropDownButton
              icon={'print'}
              loadingConfigs={this.state.loadingConfigs}
              disabled={!this.state.enableButton}
              text={translate('duoc.duocXuatDuoc.btnInToaN')}
              arrayButton={[
                { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInToaN('pdf')},
                { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInToaN('xls')},
                { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInToaN('rtf')}
              ]}
              defaultHandleOnClick={() => this.handleOnClickButtonInToaN('pdf')}
              className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
            />
            <DuocDropDownButton
              icon={'print'}
              loadingConfigs={this.state.loadingConfigs}
              disabled={!this.state.enableButton}
              text={translate('duoc.duocXuatDuoc.btnInToaH')}
              arrayButton={[
                { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInToaH('pdf')},
                { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInToaH('xls')},
                { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInToaH('rtf')}
              ]}
              defaultHandleOnClick={() => this.handleOnClickButtonInToaH('pdf')}
              className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
            />
            <DuocDropDownButton
              icon={'print'}
              loadingConfigs={this.state.loadingConfigs}
              disabled={!this.state.enableButton}
              text={translate('duoc.duocXuatDuoc.btnInBangKe')}
              arrayButton={[
                { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInToaH('pdf')},
                { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInToaH('xls')},
                { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInToaH('rtf')}
              ]}
              defaultHandleOnClick={() => this.handleOnClickButtonInToaH('pdf')}
              className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
            />
          </div>
          <div className={'div-box-wrapper row-margin-left-5 row-margin-top-5'}>
            <DuocLabelComponent text={translate('duoc.duocXuatDuoc.titleDanhSachVatTuChuaXuat')}/>
            <GridDanhSachVatTuXuatDuoc listDanhSachVatTu={this.props.listDanhSachVatTuChuaXuatDuoc}/>
          </div>
          <div className={'div-box-wrapper row-margin-left-5 row-margin-top-5'}>
            <DuocLabelComponent text={translate('duoc.duocXuatDuoc.titleDanhSachVatTuDaXuat')}/>
            <GridDanhSachVatTuXuatDuoc listDanhSachVatTu={this.props.listDanhSachVatTuXuatDuoc}/>
          </div>
        </BoxItem>
      </Box>
    </div>);
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  listDanhSachBenhNhanXuatDuoc: storeState.xuatduoc.listDanhSachBenhNhanXuatDuoc,
  listDanhSachVatTuChuaXuatDuoc: storeState.xuatduoc.listDanhSachVatTuChuaXuatDuoc,
  listDanhSachVatTuXuatDuoc: storeState.xuatduoc.listDanhSachVatTuXuatDuoc,
  listDanhSachKhoaNoiTru: storeState.hethong.listDanhSachKhoaNoiTru
});

const mapDispatchToProps = {
  getDanhSachBenhNhanXuatDuoc,
  getDanhSachVatTuXuatDuoc,
  resetDanhSachVatTu,
  getDanhSachKhoaNoiTru,
  xuatDuoc,
  printPhieuHoanTraThuoc,
  exportDanhSachPhieuXuat,
  traThuocVeKho
};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(XuatDuocComponent);
