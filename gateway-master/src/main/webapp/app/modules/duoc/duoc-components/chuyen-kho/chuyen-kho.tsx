import React from 'react';
import {RouteComponentProps} from "react-router";
import {ISetEnable, defaultSetEnable} from "./enable.model";
import {IRootState} from "app/shared/reducers";
import {connect} from "react-redux";
import { Tabs, DateBox, Button, TextBox, DropDownBox, NumberBox, Validator, SelectBox } from 'devextreme-react';
import {DataGrid, Column, Editing } from 'devextreme-react/data-grid';
import Form, { Item, SimpleItem, Label, RequiredRule, EmptyItem, ButtonItem, ButtonOptions, GroupItem, CompareRule } from 'devextreme-react/form';
import Box, {Item as BoxItem} from "devextreme-react/box";
import notify from "devextreme/ui/notify";
import dialog from "devextreme/ui/dialog";
import {printType1, exportXLSType1} from '../utils/print-helper';
import {
  IPhieuChuyenKho, defaultPhieuChuyenKho, IChiTietPhieuChuyenKho, defaultChiTietPhieuChuyenKho
} from "./chuyen-kho.model";
import {
  getDanhSachPhieuchuyenKho,
  getDanhSachNghiepVu,
  getDanhSachKhoChuyen,
  getDanhSachKhoNhan,
  getDanhSachVatTu,
  getDonViQuanLy,
  addPhieuChuyenKho,
  updatePhieuChuyenKho,
  deletePhieuChuyenKho,
  addChiTietPhieuChuyenKho,
  deleteChiTietPhieuChuyenKho,
  getDanhSachChiTietChuyenKho,
  resetPhieuNhapObj,
  resetChiTietChuyenKho,
  getDanhSachBanInPhieuDuTruKhoaPhong,
  printPhieuChuyenKho,
  printPhieuXuatKho,
  printBienBanThanhLy,
  printPhieuDuTruKhoaPhong,
  printPhieuDuTruBoSungKhoaPhong,
  printPhieuHoanTraKhoaPhong,
  printPhieuDuTruDinhMuc
} from './chuyen-kho.reducer';

import {
  convertToDate,
  convertToDate1,
  convertToSQLDate, convertToSQLDate1,
  getMoTaThamSo
} from "app/modules/duoc/duoc-components/utils/funtions";
import {apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING} from '../../configs';
import { GridDanhSachPhieuChuyenKho } from './components/grid-danh-sach-phieu-chuyen-kho/grid-danh-sach-phieu-chuyen-kho';
import {DuocDropDownButton} from "app/modules/duoc/duoc-components/utils/button-dropdown/button-dropdown";
import {dataXuatHuy} from "app/modules/duoc/duoc-components/chuyen-kho/default.data";
import GridDanhSachChiTietPhieuChuyenKho
  from ".//components/grid-danh-sach-chi-tiet-phieu-chuyen/grid-danh-sach-chi-tiet-phieu-chuyen";
import DropDownVatTu from './/components/dropdown-vat-tu/dropdown-vat-tu';
import PopUpDanhSachBanInChuyenKho from './/components/popup-danh-sach-ban-in/popup-danh-sach-ban-in';
import {AdvanceDateBox} from '../utils/advance-datebox/advance-datebox';
import {TuNgayDenNgayDocComponent} from "../utils/tungay-denngay/tungay-denngay-doc";
import FormPhieuChuyenKho
  from "app/modules/duoc/duoc-components/chuyen-kho/components/form-phieu-chuyen-kho/form-phieu-chuyen-kho";
import {
  getDvtt,
  getMaNhanVien,
  getMaPhongBan,
  getMaPhongBenh,
  getTenBenhVien,
  getTenNhanVien,
  getTenPhongBan,
  getTenTinh
} from '../utils/thong-tin';
import DuocTitleComponent from "app/modules/duoc/duoc-components/utils/duoc-title/duoc-title";
import DuocLabelComponent from "app/modules/duoc/duoc-components/utils/duoc-label/duoc-label";
export interface IChuyenKhoProp extends StateProps, DispatchProps, RouteComponentProps<{ nghiepVu: string }> {
  account: any;
}

export interface IChuyenKhoState {
  curTab: number;
  chiTietPhieuChuyenObj: IChiTietPhieuChuyenKho;
  printPopUp: {
    loaiIn: number; // 0: In phiếu dự trừ   1: In phiếu bổ sung
    visible: boolean;
    loaiFile: string;
  }
  loadingConfigs: {
    visible: boolean;
    position: string;
  }
  sendPhieuChuyenKho: {
    phieuChuyenKho: IPhieuChuyenKho;
    isUpdated: number;
  },
  enableInput: boolean;
}

export class ChuyenKhoComponent extends React.Component<IChuyenKhoProp, IChuyenKhoState> {
  private refTuNgayDenNgay = React.createRef<TuNgayDenNgayDocComponent>();
  private refFormPhieuChuyen = React.createRef<FormPhieuChuyenKho>();

  constructor(props) {
    super(props);
    this.state = {
      curTab: 0,
      chiTietPhieuChuyenObj: defaultChiTietPhieuChuyenKho,
      printPopUp: {
        loaiIn: 0,
        visible: false,
        loaiFile: 'pdf'
      },
      loadingConfigs: {
        visible: false,
        position: 'this'
      },
      sendPhieuChuyenKho: {
        isUpdated: -1,
        phieuChuyenKho: defaultPhieuChuyenKho
      },
      enableInput: false
    }
  }

  componentDidMount(): void {
    this.getDanhSachNghiepVu(getDvtt(), getMaNhanVien(), this.props.match.params.nghiepVu);
    this.getDonViQuanLy(getDvtt());
    if (this.props.match.params.nghiepVu) {
      // this.getDanhsachPhieuChuyenKho(this.state.tuNgay, this.state.denNgay, getDvtt(), this.state.phieuChuyenKhoObj.maNghiepVuChuyen, getMaPhongBan());
      this.getDanhSachKhoChuyen(parseInt(this.props.match.params.nghiepVu, 0));
      this.getDanhSachKhoNhan(parseInt(this.props.match.params.nghiepVu,0));
    }
  }

  componentDidUpdate(prevProps: Readonly<IChuyenKhoProp>, prevState: Readonly<IChuyenKhoState>, snapshot?: any): void {
    if (prevProps.match.params.nghiepVu !== this.props.match.params.nghiepVu) {
      this.props.resetChiTietChuyenKho();
      this.getDanhSachNghiepVu(getDvtt(), getMaNhanVien(), this.props.match.params.nghiepVu);
      this.getDanhsachPhieuChuyenKho(parseInt(this.props.match.params.nghiepVu,0));
      this.getDanhSachKhoChuyen(parseInt(this.props.match.params.nghiepVu,0));
      this.getDanhSachKhoNhan(parseInt(this.props.match.params.nghiepVu,0));
    }
  }

  // Handles
  handleOnClickButtonLamMoi = (e) => {
    if (this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj().maNghiepVuChuyen === null) {
      notify({ message: "Chưa chọn nghiệp vụ chuyển", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    this.getDanhsachPhieuChuyenKho(this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj().maNghiepVuChuyen);
  };
  handleOnDoubleClickGridPhieuChuyen = (e) => {
    const nghiepVu = this.props.match.params.nghiepVu;
    let maDonViGiao = null;
    let maDonViNhan = null;
    if (nghiepVu === '37') {
      maDonViGiao = this.props.donViQuanLy;
      maDonViNhan = getDvtt();
    } else if (nghiepVu === '50') {
      maDonViGiao = getDvtt();
      maDonViNhan = this.props.donViQuanLy;
    } else {
      maDonViGiao = getDvtt();
      maDonViNhan = getDvtt();
    }
    const selectedPhieuChuyen = e.data;
    const mapPhieuChuyenObj = {
      ...defaultPhieuChuyenKho,
      duyet: selectedPhieuChuyen.DUYET,
      ghiChu: selectedPhieuChuyen && selectedPhieuChuyen.GHICHU,
      idChuyenKhoVatTu: selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU,
      maDonViTao: selectedPhieuChuyen.MA_DON_VI_TAO,
      maDonViGiao,
      maDonViNhan,
      maKhoNhan: selectedPhieuChuyen.MA_KHO_NHAN,
      ngayChuyen: selectedPhieuChuyen.NGAYCHUYEN,
      ngayPhieuChuyen: selectedPhieuChuyen.NGAYPHIEUCHUYEN,
      nghiepVu: selectedPhieuChuyen.NGHIEP_VU,
      soLuuTru: selectedPhieuChuyen.SOLUUTRU,
      soPhieuChuyen: selectedPhieuChuyen.SOPHIEUCHUYEN,
      xuatHuy: selectedPhieuChuyen.XUATHUY
    };
    this.setState({
      sendPhieuChuyenKho: {
        isUpdated: 0,
        phieuChuyenKho: {...mapPhieuChuyenObj,
          maKhoGiao: selectedPhieuChuyen.MA_KHO_GIAO,
          maNghiepVuChuyen: this.props.listDanhSachNghiepVu.filter(nv => nv.NGHIEP_VU === selectedPhieuChuyen.NGHIEP_VU)[0].MA_NGHIEP_VU
        }
      }
    }, () => {
      this.setState({
        sendPhieuChuyenKho: {
          isUpdated: -1,
          phieuChuyenKho: defaultPhieuChuyenKho
        }
      })
      }
    );
    this.getDanhSachChiTietChuyenKho(this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj().idChuyenKhoVatTu,
      this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj().maDonViTao);
    this.handleReloadDanhSachVatTu(() => {});
  };
  handleLoading = (visible: boolean) => {
    this.setState({
      loadingConfigs: {
        ...this.state.loadingConfigs,
        visible
      }
    });
  };
  handleOnClickButtonInPhieuChuyenKho = (loaiFile: string) => {
    const phieuChuyenKho = this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj();
    this.props.printPhieuChuyenKho(
      getDvtt(),
      phieuChuyenKho.idChuyenKhoVatTu,
      phieuChuyenKho.maNghiepVuChuyen,
      getTenBenhVien(),
      getTenTinh(),
      loaiFile,
      this.handleLoading
    );
  };
  handleOnClickButtonInPhieuXuatKho = (loaiFile: string, mau: number) => {
    const phieuChuyenKho = this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj();
    this.props.printPhieuXuatKho(
      getDvtt(),
      phieuChuyenKho.idChuyenKhoVatTu,
      phieuChuyenKho.maNghiepVuChuyen,
      getTenBenhVien(),
      getTenTinh(),
      getTenNhanVien(),
      loaiFile,
      mau, // 0: mẫu đứng      1: mẫu ngang
      this.handleLoading
    );
  };
  handleOnClickButtonInBienBanThanhLy = (loaiFile: string, mau: number) => {
    const phieuChuyenKho = this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj();
    this.props.printBienBanThanhLy(
      getDvtt(),
      phieuChuyenKho.idChuyenKhoVatTu,
      phieuChuyenKho.soPhieuChuyen,
      getTenBenhVien(),
      getTenTinh(),
      loaiFile,
      mau, // 0: Biên bản thanh lý      1: Biên bản mất hỏng vỡ
      this.handleLoading
    );
  };
  handleReloadDanhSachVatTu = (focus: Function) => {
    this.props.getDanhSachVatTu(getDvtt(),
      this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj().maKhoGiao,
      convertToDate(this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj().ngayChuyen),
      this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj().maNghiepVuChuyen,
      focus);
  };
  handleOnSelectRowGridChiTietPhieuChuyen = (e) => {
    const selectedChiTietChuyenKho = e.selectedRowKeys[0];
    const mapChiTietPhieuChuyenKhoObj = {
      ...defaultChiTietPhieuChuyenKho,
      idChuyenKhoVTCT: selectedChiTietChuyenKho === undefined ? "" : selectedChiTietChuyenKho.ID_CHUYEN_KHO_VAT_TU_CT,
      tenVatTu: selectedChiTietChuyenKho === undefined ? "" : selectedChiTietChuyenKho.TENVATTU
    };
    this.setState({
      chiTietPhieuChuyenObj: mapChiTietPhieuChuyenKhoObj
    });
  };
  handleOnKeyDownGridChiTietPhieuChuyen = (e) => {
    const isMac = window.navigator.platform.toUpperCase().includes('MAC', 0);
    if (e.event.keyCode === 46 || (e.event.keyCode === 8 && isMac)) {
      dialog.confirm("Xóa thuốc khỏi phiếu chuyển ?", "Thông báo hệ thống")
        .then(dialogResult => {
          if (dialogResult) {
            this.props.deleteChiTietPhieuChuyenKho(this.state.chiTietPhieuChuyenObj.idChuyenKhoVTCT,
              getMaNhanVien(),
              getMaPhongBan(),
              getMaPhongBenh(),
              this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj().maDonViTao,
              this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj().idChuyenKhoVatTu);
          }
        });
    }
  };
  handleChangePopUpState = (loaiIn: number, visible: boolean, loaiFile: string) => {
    const phieuChuyenKho = this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj();
    this.props.getDanhSachBanInPhieuDuTruKhoaPhong(phieuChuyenKho.idChuyenKhoVatTu, phieuChuyenKho.maDonViTao);
    this.setState({
      printPopUp: {
        loaiIn,
        visible,
        loaiFile
      }
    });
  };
  handleOnSelectRowGridDanhSachBanIn = (e: any) => { // 0: In phiếu dự trù    1: In phiếu bổ sung     2: In phiếu hoàn trả
    if (this.state.printPopUp.loaiIn === 0) {
      const phieuChuyenKho = this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj();
      const selectedRow = e;
      this.props.printPhieuDuTruKhoaPhong(
        getDvtt(),
        phieuChuyenKho.soPhieuChuyen,
        phieuChuyenKho.idChuyenKhoVatTu,
        selectedRow.NGAY_CHUYEN,
        selectedRow.THANH_TIEN,
        selectedRow.MAKHOVATTU,
        selectedRow.TENKHOVATTU,
        getMaPhongBan(),
        selectedRow.TEN_PHONGBAN,
        selectedRow.MALOAIVATTU,
        getMaNhanVien(),
        getTenBenhVien(),
        getTenTinh(),
        this.state.printPopUp.loaiFile,
        this.handleLoading
      );
    } else if (this.state.printPopUp.loaiIn === 1) { // In phiếu bổ sung
      const phieuChuyenKho = this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj();
      const selectedRow = e;
      this.props.printPhieuDuTruBoSungKhoaPhong(
        getDvtt(),
        phieuChuyenKho.idChuyenKhoVatTu,
        phieuChuyenKho.soLuuTru,
        selectedRow.NGAY_CHUYEN,
        phieuChuyenKho.ngayPhieuChuyen,
        selectedRow.THANH_TIEN,
        selectedRow.MAKHOVATTU,
        selectedRow.TENKHOVATTU,
        selectedRow.MALOAIVATTU,
        selectedRow.TEN_PHONGBAN,
        getMaPhongBan(),
        getMaNhanVien(),
        getTenNhanVien(),
        getTenBenhVien(),
        getTenTinh(),
        this.state.printPopUp.loaiFile,
        this.handleLoading
      );
    } else if (this.state.printPopUp.loaiIn === 2) { // In phiếu hoàn trả khoa phòng
      const phieuChuyenKho = this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj();
      const selectedRow = e;
      this.props.printPhieuHoanTraKhoaPhong(
        getDvtt(),
        phieuChuyenKho.idChuyenKhoVatTu,
        phieuChuyenKho.soPhieuChuyen,
        selectedRow.NGAY_CHUYEN,
        selectedRow.THANH_TIEN,
        selectedRow.MAKHOVATTU,
        selectedRow.TENKHOVATTU,
        selectedRow.MALOAIVATTU,
        selectedRow.TEN_PHONGBAN,
        getTenPhongBan(),
        getMaNhanVien(),
        getTenNhanVien(),
        getTenBenhVien(),
        getTenTinh(),
        this.state.printPopUp.loaiFile,
        this.handleLoading
      );
    }
  };
  handleOnClickButtonInPhieuDuTruDinhMuc = (loaiFile) => {
    const phieuChuyenKho = this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj();
    this.props.printPhieuDuTruDinhMuc(
      getDvtt(),
      phieuChuyenKho.idChuyenKhoVatTu,
      phieuChuyenKho.maNghiepVuChuyen,
      getTenBenhVien(),
      getTenTinh(),
      loaiFile,
      this.handleLoading
    );
  };
  handleChangeEnableInput = (enable: boolean) => {
    this.setState({
      enableInput: enable
    });
  };
  // Funtion
  mapPhieuChuyenKhoObj = (idPhieuChuyen, soLuuTru, soPhieuChuyen) => {
    this.setState({
      sendPhieuChuyenKho: {
        phieuChuyenKho: {...this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj(),
          idChuyenKhoVatTu: idPhieuChuyen,
          soLuuTru,
          soPhieuChuyen
        },
        isUpdated: 1
      }
    })
  };
  getDanhsachPhieuChuyenKho = (maNghiepVu: number) => {
    this.props.getDanhSachPhieuchuyenKho(this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay,
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay, getDvtt(), maNghiepVu, getMaPhongBan());
  };
  getDanhSachNghiepVu = (dvtt, maNV, defaultNghiepVu) => {
    this.props.getDanhSachNghiepVu(dvtt, maNV, defaultNghiepVu);
  };
  getDanhSachKhoChuyen = (maNghiepVu: number) => {
    this.props.getDanhSachKhoChuyen(getDvtt(), maNghiepVu);
  };
  getDanhSachKhoNhan = (maNghiepVu: number) => {
    this.props.getDanhSachKhoNhan(getDvtt(), maNghiepVu, 1332990);
  };
  getDonViQuanLy = (dvtt) => {
    this.props.getDonViQuanLy(dvtt);
  };
  getDanhSachChiTietChuyenKho = (idPhieuChuyen, dvtt) => {
    this.props.getDanhSachChiTietChuyenKho(idPhieuChuyen, dvtt);
  };
  addChiTietPhieuChuyenKho = (chiTietPhieuChuyen: IChiTietPhieuChuyenKho) => {
    const phieuChuyenKho = this.refFormPhieuChuyen.current.getPhieuChuyenKhoObj();
    this.props.addChiTietPhieuChuyenKho(
      {...chiTietPhieuChuyen,
        maNghiepVuChuyen: phieuChuyenKho.maNghiepVuChuyen,
        maKhoGiao: phieuChuyenKho.maKhoGiao,
        maKhoNhan: phieuChuyenKho.maKhoNhan,
        maDonViTao: phieuChuyenKho.maDonViTao,
        maDonViGiao: phieuChuyenKho.maDonViGiao,
        maDonViNhan: phieuChuyenKho.maDonViNhan,
        idChuyenKhoVT: phieuChuyenKho.idChuyenKhoVatTu,
        ngayChuyen: convertToDate(phieuChuyenKho.ngayChuyen),
        nguoiNhap: 1,
        ghiChu: chiTietPhieuChuyen.ghiChu === "" ? 'chuyenkho' : chiTietPhieuChuyen.ghiChu,
        ngayHetHan: convertToSQLDate1(chiTietPhieuChuyen.ngayHetHan)
      },
      getMaPhongBan(),
      getMaPhongBenh(),
      null
    );
  };
  addPhieuChuyenKho = (phieuChuyenKho: IPhieuChuyenKho, setEnabled: Function) => {
    this.props.addPhieuChuyenKho({
        ...phieuChuyenKho,
        ngayPhieuChuyen: convertToDate1(phieuChuyenKho.ngayPhieuChuyen),
        ngayChuyen: convertToDate1(phieuChuyenKho.ngayChuyen)
      }, getDvtt(),
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay,
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay,
      phieuChuyenKho.maNghiepVuChuyen,
      getMaPhongBan(),
      getMaPhongBenh(),
      setEnabled,
      this.mapPhieuChuyenKhoObj,
      () => {
        this.props.getDanhSachVatTu(getDvtt(),
          phieuChuyenKho.maKhoGiao,
          convertToDate(phieuChuyenKho.ngayChuyen),
          phieuChuyenKho.maNghiepVuChuyen,
          () => {});
      }
    );
  };
  updatePhieuChuyenKho = (phieuChuyenKho: IPhieuChuyenKho) => {
    this.props.updatePhieuChuyenKho({
        ...phieuChuyenKho,
        maPhongBanTao: getMaPhongBan(),
        maNguoiTao: 1,
        duTruCSTT: 0,
        ngayChuyen: convertToDate(phieuChuyenKho.ngayChuyen),
        ngayPhieuChuyen: convertToDate(phieuChuyenKho.ngayPhieuChuyen)
      }, getDvtt(),
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay,
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay,
      phieuChuyenKho.maNghiepVuChuyen,
      getMaPhongBan(),
      getMaPhongBenh(),
      () => {
      this.setState({
        sendPhieuChuyenKho: {
          phieuChuyenKho: defaultPhieuChuyenKho,
          isUpdated: 2
        }
      })
      });
  };
  deletePhieuChuyenKho = (idChuyenKhoVatTu: number, maNghiepVuChuyen: number) => {
    this.props.deletePhieuChuyenKho(
      idChuyenKhoVatTu,
      getMaNhanVien(),
      getMaPhongBan(),
      getMaPhongBenh(),
      getDvtt(),
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay,
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay,
      maNghiepVuChuyen,
      () => {
        this.setState({
          sendPhieuChuyenKho: {
            phieuChuyenKho: {...defaultPhieuChuyenKho, maNghiepVuChuyen: parseInt(this.props.match.params.nghiepVu, 0)},
            isUpdated: 2
          }
        })
      });
  };

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (<div>
      <DuocTitleComponent text={'Dược - Nghiệp vụ Chuyển kho'}/>
      <Box
        direction="row"
        width="100%"
        height={'auto'}
      >
        <BoxItem baseSize={'35%'} ratio={0}>
          <div className={'div-box-wrapper'}>
            <DuocLabelComponent text={'Thông tin lọc phiếu chuyển kho'}/>
            <div className={'row-margin-top-5'}/>
            <TuNgayDenNgayDocComponent
              ref={this.refTuNgayDenNgay}
              handleOnClickButtonLamMoi={this.handleOnClickButtonLamMoi}
            />
          </div>
          <div className={'div-box-wrapper row-margin-top-5 div-height-100'}>
            <DuocLabelComponent text={'Danh sách phiếu chuyển kho'}/>
            <GridDanhSachPhieuChuyenKho
              listDanhSachPhieuChuyenKho={this.props.listDanhSachPhieuChuyenKho}
              height={'auto'}
              onCellDblClick={this.handleOnDoubleClickGridPhieuChuyen}
            />
          </div>
        </BoxItem>
        <BoxItem baseSize={'65%'} ratio={0}>
          <div className={'div-box-wrapper col-margin-left-5 div-height-100'}>
            <DuocLabelComponent text={'Thông tin phiếu chuyển kho'}/>
              <FormPhieuChuyenKho
                ref={this.refFormPhieuChuyen}
                maNghiepVu={parseInt(this.props.match.params.nghiepVu,0)}
                maDonViQuanLy={this.props.donViQuanLy}
                maDonVi={getDvtt()}
                maPhongBan={getMaPhongBan()}
                listDanhSachNghiepVu={this.props.listDanhSachNghiepVu}
                listDanhSachKhoChuyen={this.props.listDanhSachKhoChuyen}
                listDanhSachKhoNhan={this.props.listDanhSachKhoNhan}
                enableForm={this.props.listDanhSachChiTietChuyenKho.length > 0}
                loadingConfigs={this.state.loadingConfigs}
                addPhieuChuyenKho={this.addPhieuChuyenKho}
                updatePhieuChuyenKho={this.updatePhieuChuyenKho}
                deletePhieuChuyenKho={this.deletePhieuChuyenKho}
                getDanhSachPhieuChuyen={this.getDanhsachPhieuChuyenKho}
                getDanhSachKhoNhan={this.getDanhSachKhoNhan}
                getDanhSachKhoChuyen={this.getDanhSachKhoChuyen}
                handleOnClickButtonInPhieuChuyenKho={this.handleOnClickButtonInPhieuChuyenKho}
                handleOnClickButtonInPhieuXuatKho={this.handleOnClickButtonInPhieuXuatKho}
                handleOnClickButtonInBienBanThanhLy={this.handleOnClickButtonInBienBanThanhLy}
                handleOnClickButtonInPhieuDuTruDinhMuc={this.handleOnClickButtonInPhieuDuTruDinhMuc}
                handleChangeEnableInput={this.handleChangeEnableInput}
                handleChangePopUpState={this.handleChangePopUpState}
                sendPhieuChuyenKho={this.state.sendPhieuChuyenKho}
              />
          </div>
        </BoxItem>
      </Box>
      {/* // Grid gõ thuốc */}
      <Box
        direction="col"
        width="100%"
        height={'auto'}
      >
        <BoxItem ratio={0} baseSize={'100%'}>
          <div className={'div-box-wrapper row-margin-top-5'}>
            <DuocLabelComponent text={'Danh sách chi tiết chuyển kho'}/>
            <GridDanhSachChiTietPhieuChuyenKho
              // handleReloadDanhSachVatTu={this.handleReloadDanhSachVatTu}
              addChiTietPhieuChuyenKho={this.addChiTietPhieuChuyenKho}
              listDanhSachVatTu={this.props.listDanhSachVatTu}
              listDanhSachChiTietPhieuChuyenKho={this.props.listDanhSachChiTietChuyenKho}
              height={450}
              onSelectionChanged={this.handleOnSelectRowGridChiTietPhieuChuyen}
              onKeyDown={this.handleOnKeyDownGridChiTietPhieuChuyen}
              enableInput={this.state.enableInput}
            />
          </div>
        </BoxItem>
        <BoxItem ratio={0} baseSize={'100%'}>
          <PopUpDanhSachBanInChuyenKho
            listDanhSachBanIn={this.props.listDanhSachBanInPhieuDuTruKhoaPhong}
            handleChangePopUpState={this.handleChangePopUpState}
            handleOnSelectRowGridDanhSachBanIn={this.handleOnSelectRowGridDanhSachBanIn}
            printPopUp={this.state.printPopUp}
          />
        </BoxItem>
      </Box>
    </div>);
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  listDanhSachPhieuChuyenKho: storeState.chuyenkho.listDanhSachPhieuchuyenKho,
  listDanhSachNghiepVu: storeState.chuyenkho.listDanhSachNghiepVu,
  listDanhSachKhoChuyen: storeState.chuyenkho.listDanhSachKhoChuyen,
  listDanhSachKhoNhan: storeState.chuyenkho.listDanhSachKhoNhan,
  donViQuanLy: storeState.chuyenkho.donViQuanLy,
  listDanhSachChiTietChuyenKho: storeState.chuyenkho.listDanhSachChiTietChuyenKho,
  listDanhSachVatTu: storeState.chuyenkho.listDanhSachVatTu,
  listDanhSachBanInPhieuDuTruKhoaPhong: storeState.chuyenkho.listDanhSachBanInPhieuDuTruKhoaPhong
});

const mapDispatchToProps = {
  getDanhSachPhieuchuyenKho,
  getDanhSachNghiepVu,
  getDanhSachKhoChuyen,
  getDanhSachKhoNhan,
  getDanhSachVatTu,
  getDonViQuanLy,
  addPhieuChuyenKho,
  updatePhieuChuyenKho,
  deletePhieuChuyenKho,
  addChiTietPhieuChuyenKho,
  deleteChiTietPhieuChuyenKho,
  getDanhSachChiTietChuyenKho,
  resetChiTietChuyenKho,
  getDanhSachBanInPhieuDuTruKhoaPhong,
  printPhieuChuyenKho,
  printPhieuXuatKho,
  printBienBanThanhLy,
  printPhieuDuTruKhoaPhong,
  printPhieuDuTruBoSungKhoaPhong,
  printPhieuHoanTraKhoaPhong,
  printPhieuDuTruDinhMuc
};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChuyenKhoComponent);
