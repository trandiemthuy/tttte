export interface IPhongBanModel {
  maPhongBan?: string;
  tenPhongBan?: string;
}

export const defaultPhongBanModel: IPhongBanModel = {
  maPhongBan: null,
  tenPhongBan: ''
};
