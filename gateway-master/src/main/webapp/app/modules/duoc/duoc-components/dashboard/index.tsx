import React from 'react';
import { Switch } from 'react-router-dom';
import DashboardComponent from './dashboard';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (

  <Switch>
    <ErrorBoundaryRoute
      path={`${match.url}`}
      component={DashboardComponent}>
    </ErrorBoundaryRoute>
  </Switch>
);

export default Routes;
