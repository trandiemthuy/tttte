import React from 'react';
import { Switch } from 'react-router-dom';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import DuocNhanDuocComponent from "./nhan-duoc-ve-kho";

const Routes = ({ match }) => (

  <Switch>
    <ErrorBoundaryRoute
      path={`${match.url}/:nghiepVu`}
      component={DuocNhanDuocComponent}>
    </ErrorBoundaryRoute>
  </Switch>
);

export default Routes;
