import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection, ColumnFixing, Pager, Paging
} from 'devextreme-react/data-grid';
import {configLoadPanel, pageSizes} from '../../../../configs';

export interface IGridDanhSachPhieuChuaDuTruProp extends IDataGridOptions {
  listDanhSachPhieuChuaDuTru: any;
  handleOnContextMenuGridDanhSach: any;
  loaiPhieu: number; // 0: Dự trù nội trú     1: Hoàn trả nội trú
}
export interface IGridDanhSachPhieuChuaDuTruState {
  listSelectedPhieu: any;
}

export class GridDanhSachPhieuChuaDuTru extends React.Component<IGridDanhSachPhieuChuaDuTruProp, IGridDanhSachPhieuChuaDuTruState> {
  constructor(props) {
    super(props);
    this.state = {
      listSelectedPhieu: ''
    };
  }

  // Handles
  handleOnSelectGridDanhSach = (e) => {
    let idPhieus = '';
    e.selectedRowsData.map(phieu => {
      idPhieus += phieu.ID_DIEUTRI + ',';
    });
    this.setState({
      listSelectedPhieu: idPhieus.substr(0, idPhieus.length - 1)
    });
  };

  // Function
  getSelectedPhieuDuTru = () => {
    return this.state.listSelectedPhieu;
  };

  render() {
    return (
      <DataGrid
        loadPanel={configLoadPanel}
        dataSource={this.props.listDanhSachPhieuChuaDuTru}
        showBorders={true}
        showRowLines={true}
        showColumnLines={true}
        rowAlternationEnabled={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        columnMinWidth={50}
        onContextMenuPreparing={this.props.handleOnContextMenuGridDanhSach}
        noDataText={'Không có dữ liệu'}
        onSelectionChanged={this.handleOnSelectGridDanhSach}
        wordWrapEnabled={true}
        {...this.props}
      >
        <Selection mode="multiple" />
        <FilterRow visible={true}
                   applyFilter={'auto'} />
        <HeaderFilter visible={true} />
        <Column
          dataField="STT_BENHAN"
          caption="STT bệnh án"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="STT_DOTDIEUTRI"
          caption="STT Đợt điều trị"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField={this.props.loaiPhieu === 0 ? "STT_DIEUTRI" : 'STT_DIEUTRI_HT'}
          caption="Số phiếu"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="TEN_BENH_NHAN"
          caption="Họ tên"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="SO_THE_BHYT"
          caption="Số thẻ BHYT"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="NGAY_LAP"
          caption="Ngày lập"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField={this.props.loaiPhieu === 0 ? 'TENKHOVATTU' : 'TENKHOVATTU_HT'}
          caption="Kho"
          dataType="string"
          alignment="left"
        />
        <Summary>
          <TotalItem
            displayFormat={'{0} phiếu'}
            showInColumn={this.props.loaiPhieu === 0 ? 'STT_DIEUTRI' : 'STT_DIEUTRI_HT'}
            column={this.props.loaiPhieu === 0 ? 'STT_DIEUTRI' : 'STT_DIEUTRI_HT'}
            summaryType="count"
          />
        </Summary>
        <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
        <Paging defaultPageSize={5} />
      </DataGrid>);
  }
}

export default GridDanhSachPhieuChuaDuTru;
