import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, Summary, TotalItem, Selection
} from 'devextreme-react/data-grid';
import {configLoadPanel} from "app/modules/duoc/configs";

export interface IGridNhanVienKiemNhapProp extends IDataGridOptions {
  listNhanvienKiemNhap: any;
}
export interface IGridNhanVienKiemNhapState {
  listNhanvienKiemNhap: any;
}

export class GridNhanVienKiemNhap extends React.Component<IGridNhanVienKiemNhapProp, IGridNhanVienKiemNhapState> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <DataGrid
        loadPanel={configLoadPanel}
        dataSource={this.props.listNhanvienKiemNhap}
        showBorders={true}
        showColumnLines={true}
        allowColumnReordering={true}
        rowAlternationEnabled={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        columnMinWidth={50}
        height={400}
        noDataText={'Không có dữ liệu'}
        wordWrapEnabled={true}
        {...this.props}
      >
        <Selection mode="single" />
        <FilterRow visible={true}
                   applyFilter={'auto'} />
        <HeaderFilter visible={true} />
        <Column
          dataField="ID_NHAPKHONHACC"
          caption="ID nhập kho từ nhà cung cấp"
          dataType="string"
          alignment="left"
          width={'0%'}
          visible={false}
        />
        <Column
          dataField="MANHANVIEN"
          caption="Mã nhân viên"
          dataType="string"
          alignment="left"
          width={'0%'}
          visible={false}
        />
        <Column
          dataField="TENNHANVIEN"
          caption="Tên nhân viên"
          dataType="string"
          alignment="left"
          width={'50%'}
        />
        <Column
          dataField="CHUCDANH"
          caption="Chức danh"
          dataType="string"
          alignment="left"
          width={'50%'}
        />
        <Summary>
          <TotalItem
            customizeText={(data) => {
              return data.value.toLocaleString() + " nhân viên";
            }}
            column="TENNHANVIEN"
            summaryType="count"
          />
        </Summary>
      </DataGrid>);
  }
}

export default GridNhanVienKiemNhap;
