import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection, ColumnFixing, Pager, Paging
} from 'devextreme-react/data-grid';
import {configLoadPanel, pageSizes} from '../../../../configs';
import {
  defaultPhieuDuTruModel,
  IPhieuDuTruModel
} from "app/modules/duoc/duoc-components/tong-hop-du-tru/phieu-du-tru.model";
import {getDvtt} from '../../../utils/thong-tin';

export interface IGridDanhSachPhieuDuTruProp extends IDataGridOptions {
  listDanhSachPhieuDuTru: any;
  handleOnContextMenuGridDanhSach: any;
  loaiPhieu: number; // 0: Dự trù nội trú     1: Hoàn trả nội trú
  getDanhSachVatTuDuTru: Function;
  getDanhSachBenhNhanDuTru: Function;
  setEnableButton: Function;
  resetDanhSachVatTu: Function;
  resetDanhSachBenhNhan: Function;
  handleSendDataToForm: Function;
}
export interface IGridDanhSachPhieuDuTruState {
  selectedPhieu: IPhieuDuTruModel;
  isSelect: boolean;
}

export class GridDanhSachPhieuDuTru extends React.Component<IGridDanhSachPhieuDuTruProp, IGridDanhSachPhieuDuTruState> {
  constructor(props) {
    super(props);
    this.state = {
      selectedPhieu: defaultPhieuDuTruModel,
      isSelect: false
    };
  }

  // Handles
  handleOnRowRepared = (e) => {
    if (e.rowType === 'data' && e.data.TRANG_THAI === 3) {
      e.rowElement.style.backgroundImage = 'radial-gradient( circle farthest-corner at 10% 20%,  rgba(0,93,133,1) 0%, rgba(0,181,149,1) 90% )';
      e.rowElement.style.color = 'white';
      // background-image: radial-gradient( circle 862px at 6% 18%,  rgba(21,219,149,1) 9.4%, rgba(26,35,160,1) 83.6% );
      e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
    }
  };
  handleOnSelectPhieuDuTru = (e) => {
    const selectedPhieu = e.selectedRowsData[0];
    if (selectedPhieu === undefined) return;
    this.setState({
      selectedPhieu: {
        ...defaultPhieuDuTruModel,
        idPhieu: this.props.loaiPhieu === 0 ? selectedPhieu.ID_PHIEU_DU_TRU_TONG : selectedPhieu.ID_PHIEU_HOAN_TRA_TONG,
        soLuuTru: selectedPhieu.SOPHIEUTAO_LUUTRU,
        maPhongBan: this.props.loaiPhieu === 0 ? selectedPhieu.MAPHONGBAN : selectedPhieu.MA_PHONG_BAN_HOAN_TRA,
        tenPhongBan: selectedPhieu.TEN_PHONGBAN,
        ngayLap: this.props.loaiPhieu === 0 ? selectedPhieu.NGAYGIOTAO : selectedPhieu.NGAY_GIO_HOAN_TRA,
        tenKhoVatTu: selectedPhieu.TENKHOVATTU,
        ghiChu: selectedPhieu.GHI_CHU
      },
      isSelect: true
    }, () => {
      this.props.handleSendDataToForm(this.state.selectedPhieu);
      this.props.setEnableButton(true);
      this.props.getDanhSachVatTuDuTru(
        getDvtt(),
        this.state.selectedPhieu.idPhieu,
        this.state.selectedPhieu.maPhongBan,
        0,
        0,
        this.props.loaiPhieu
      );
      this.props.getDanhSachBenhNhanDuTru(
        getDvtt(),
        this.state.selectedPhieu.idPhieu,
        this.state.selectedPhieu.ngayLap.substring(0, 10),
        this.state.selectedPhieu.maPhongBan,
        this.props.loaiPhieu
      );
    });
  };
  handleOnRowClick = (e) => {
    if (!this.state.isSelect) {
      const keys = e.component.getSelectedRowKeys();
      e.component.deselectRows(keys);
      this.props.handleSendDataToForm(defaultPhieuDuTruModel);
      this.props.setEnableButton(false);
      this.props.resetDanhSachVatTu();
      this.props.resetDanhSachBenhNhan();
    }
    this.setState({
      isSelect: false
    });
  };
  getSelectedPhieuDuTru = () => {
    return this.state.selectedPhieu;
  };

  render() {
    return (
      <DataGrid
        loadPanel={configLoadPanel}
        dataSource={this.props.listDanhSachPhieuDuTru}
        showBorders={true}
        showRowLines={true}
        showColumnLines={true}
        rowAlternationEnabled={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        columnMinWidth={50}
        onContextMenuPreparing={this.props.handleOnContextMenuGridDanhSach}
        noDataText={'Không có dữ liệu'}
        onRowPrepared={this.handleOnRowRepared}
        onSelectionChanged={this.handleOnSelectPhieuDuTru}
        onRowClick={this.handleOnRowClick}
        keyExpr={this.props.loaiPhieu === 0 ? 'ID_PHIEU_DU_TRU_TONG' : 'ID_PHIEU_HOAN_TRA_TONG'}
        wordWrapEnabled={true}
        {...this.props}
      >
        <Selection mode="single" />
        <FilterRow visible={true}
                   applyFilter={'auto'} />
        <HeaderFilter visible={true} />
        <Column
          dataField={this.props.loaiPhieu === 0 ? "ID_PHIEU_DU_TRU_TONG" : "ID_PHIEU_HOAN_TRA_TONG"}
          caption="ID phiếu"
          dataType="string"
          alignment="left"
          visible={false}
        />
        <Column
          dataField="SOPHIEUTAO_LUUTRU"
          caption="Số phiếu"
          dataType="string"
          alignment="left"
        />
        <Summary>
          <TotalItem
            displayFormat={'{0} phiếu'}
            showInColumn={'SOPHIEUTAO_LUUTRU'}
            column={this.props.loaiPhieu === 0 ? "ID_PHIEU_DU_TRU_TONG" : "ID_PHIEU_HOAN_TRA_TONG"}
            summaryType="count"
          />
        </Summary>
        <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
        <Paging defaultPageSize={5} />
      </DataGrid>);
  }
}

export default GridDanhSachPhieuDuTru;
