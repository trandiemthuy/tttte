import React from 'react';
import { DropDownBox } from 'devextreme-react';
import {DataGrid, Column, FilterRow, HeaderFilter, Selection, IDataGridOptions, Scrolling, Paging} from "devextreme-react/data-grid";
import {configLoadPanel, numberFormat} from '../../../../configs';

export interface IDropDownVatTuProp extends IDataGridOptions{
  listDanhSachVatTu: any;
  dropWidth: any;
  dropHeight: any;
  handleSelectTenVatTu: Function;
  // handleReloadDanhSachVatTu: Function;
  className?: any;
}
export interface IDropDownVatTuState {
  isLoaded: boolean;
  selectedValue: any;
  indexRow: number;
  isOpened: boolean;
  focusRow: any;
  selectedVatTuObj: any;
  filterRowTenVatTu: any;
}

export class DropDownVatTu extends React.Component<IDropDownVatTuProp, IDropDownVatTuState> {
  private tenVatTuRef = React.createRef<DropDownBox>();
  private filterRowTenVatTu: any;
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      selectedValue: [],
      indexRow: 0,
      isOpened: false,
      focusRow: 0,
      selectedVatTuObj: {},
      filterRowTenVatTu: {}
    }
  }
  componentDidMount(): void {
  }

  dataGridVatTuRender = () => {
    return <DataGrid
      dataSource={this.props.listDanhSachVatTu}
      showBorders={true}
      showColumnLines={true}
      rowAlternationEnabled={true}
      allowColumnResizing={true}
      columnResizingMode={'nextColumn'}
      columnMinWidth={50}
      height={this.props.dropHeight}
      selectedRowKeys={this.state.selectedValue}
      onSelectionChanged={this.handleOnSelectDatagridVatTu}
      onKeyDown={this.handleOnKeyDownDatagridVatTu}
      // loadPanel={configLoadPanel}
      onCellPrepared={this.handleOnCellRepared}
      onContentReady={this.handleOnContentReady}
      autoNavigateToFocusedRow={true}
      {...this.props}
    >
      <Selection mode="single" />
      <Scrolling mode="infinite" />
      <Paging enabled={true} pageSize={10} />
      <FilterRow visible={true}
                 applyFilter={'auto'}
      />
      <HeaderFilter visible={true}/>
      <Column
        dataField="maVatTu"
        caption="Mã vật tư"
        dataType="string"
        alignment="left"
        width={'7%'}
      />
      <Column
        dataField="tenVatTu"
        caption="Tên vật tư"
        dataType="string"
        alignment="left"
        width={'15%'}
      />
      <Column
        dataField="hoatChat"
        caption="Hoạt chất"
        dataType="string"
        alignment="left"
        width={'10%'}
      />
      <Column
        dataField="dvt"
        caption="ĐVT"
        dataType="number"
        alignment="left"
        width={'10%'}
      />
      <Column
        dataField="soThau"
        caption="Số thầu"
        dataType="string"
        alignment="left"
        width={'5%'}
      />
      <Column
        dataField="soLoSanXuat"
        caption="Số lô"
        dataType="string"
        alignment="left"
        width={'5%'}
      />
      <Column
        dataField="nguonDuoc"
        caption="Nguồn dược"
        dataType="string"
        alignment="left"
        width={'10%'}
      />
      <Column
        dataField="ngayHetHan"
        caption="Ngày hết hạn"
        dataType="string"
        alignment="left"
        width={'10%'}
      />
      <Column
        dataField="donGia"
        caption="Đơn giá"
        dataType="number"
        alignment="left"
        width={'10%'}
        format={numberFormat}
      />
      <Column
        dataField="soLuong"
        caption="Số lượng"
        dataType="number"
        alignment="left"
        width={'10%'}
        format={numberFormat}
      />
    </DataGrid>
  };

  handleOnSelectDatagridVatTu = (e) => { // Khi chọn 1 dòng trong bảng thì select
    this.setState({
      selectedValue: e.selectedRowKeys[0] && e.selectedRowKeys[0].maVatTu,
      selectedVatTuObj: e.selectedRowKeys[0]
    });
    this.tenVatTuRef.current.instance.close();
    setTimeout(() => {
      this.props.handleSelectTenVatTu();
    }, 100);
  };
  handleOnKeyDownDatagridVatTu = (e) => {
    // if (e.keyCode === 13) {
    //   this.props.handleSelectTenVatTu();
    // }
  };

  syncDataGridSelection(e) { // Đồng bộ dữ liệu giữa datagrid và dropdownbox
    this.setState({
      selectedValue: e.value
    });
  }
  displayExpr = (item) => { // Render lại giá trị hiển thị trên dropdownbox
    return item && item.tenVatTu;
  };
  handleOnFocus = (e) => { // Khi trỏ vào dropdownbox thì set null cho giá trị đã chọn (Tránh trường hợp ko thể clear)
    this.setState({
      selectedValue: null
    });
    this.tenVatTuRef.current.instance.open();
  };
  handleOnOpened = () => { // Thực hiện focus vào tên vật tư khi mở lên
    if (this.filterRowTenVatTu !== undefined) {
      setTimeout(() => {
        this.filterRowTenVatTu.focus();
      }, 100);
    }
  };
  handleOnKeyDownTenVatTu = (e) => { // Thực hiện Chọn dòng thứ nhất và khi nhấn Enter thì select
    if (e.keyCode === 13) {
      const firstVatTu = this.props.listDanhSachVatTu.filter(vattu => vattu.tenVatTu.toLowerCase().includes(e.target.value.toLowerCase()))[0];
      this.setState({
        focusRow: firstVatTu.maVatTu
      });
      this.setState({
        selectedValue: firstVatTu.maVatTu,
        selectedVatTuObj: firstVatTu
      });
      this.tenVatTuRef.current.instance.close();
      this.props.handleSelectTenVatTu();
    }
  };
  handleFocus = () => { // Focus vào dropdownbox khi thực hiện insert chi tiết xong
    this.tenVatTuRef.current.instance.focus();
  };
  handleClose = () => {
    this.tenVatTuRef.current.instance.focus();
  };
  handleOnCellRepared = (e) => {
    if (e.rowType === "filter" && e.columnIndex === 1) {
      const cellElement = e.cellElement;
      this.filterRowTenVatTu = cellElement.querySelector(".dx-texteditor-input");
    }
  };
  handleOnContentReady = (e) => {
    this.filterRowTenVatTu.focus();
    this.filterRowTenVatTu.addEventListener('keydown', this.handleOnKeyDownTenVatTu);
  };

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return <DropDownBox
      className={this.props.className === undefined ? '' : this.props.className}
      ref={this.tenVatTuRef}
      deferRendering={false}
      displayExpr={this.displayExpr}
      placeholder="Chọn vật tư"
      dataSource={this.props.listDanhSachVatTu}
      contentRender={this.dataGridVatTuRender}
      dropDownOptions={{ width: this.props.dropWidth, height: this.props.dropHeight }}
      value={[this.state.selectedValue]}
      valueExpr="MAVATTU"
      disabled={this.props.disabled}
      onValueChanged={this.syncDataGridSelection}
      onOpened={this.handleOnOpened}
      onKeyDown={() => this.props.handleSelectTenVatTu}
      onFocusIn={this.handleOnFocus}
    />
  }
}

export default DropDownVatTu;
