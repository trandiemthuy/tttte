import React from "react";
import {Form, Button} from "devextreme-react";
import {GroupItem, Label, RequiredRule, SimpleItem} from "devextreme-react/form";
import {
  defaultPhieuDuTruModel,
  IPhieuDuTruModel
} from "app/modules/duoc/duoc-components/tong-hop-du-tru/phieu-du-tru.model";

export interface IFormPhieuDuTruProp {
  loaiPhieu: number; // 0: Dự trù   1: Hoàn trả
  // phieuDuTruObj: IPhieuDuTruModel;
  handleOnClickButtonXemPhieu: Function;
  sendDataToForm: {
    send: boolean;
    phieuDuTru: IPhieuDuTruModel
  }
}
export interface IFormPhieuDuTruState {
  phieuDuTruObj: IPhieuDuTruModel;
}

export class FormPhieuDuTru extends React.Component<IFormPhieuDuTruProp, IFormPhieuDuTruState> {
  constructor(props) {
    super(props);
    this.state = {
      phieuDuTruObj: defaultPhieuDuTruModel
    }
  }

  componentDidUpdate(prevProps: Readonly<IFormPhieuDuTruProp>, prevState: Readonly<IFormPhieuDuTruState>, snapshot?: any): void {
    if (this.props.sendDataToForm.send) {
      this.setState({
        phieuDuTruObj: this.props.sendDataToForm.phieuDuTru
      });
    }
  }

  // Handles

  // Functions
  getPhieuDuTru = () => {
    return this.state.phieuDuTruObj;
  };

  render() {
    return (
        <div>
            <Form
              className={'row-margin-top-5'}
              formData={this.state.phieuDuTruObj}
              validationGroup={"formPhieuChuyenKho"}
              labelLocation={"left"}
            >
              <GroupItem
                colCount={2}
              >
                <SimpleItem
                  dataField={'soLuuTru'}
                  editorType={'dxTextBox'}
                  editorOptions={{
                    readOnly: true
                  }}
                >
                  <Label text="Phiếu yêu cầu" />
                </SimpleItem>
                <SimpleItem
                  dataField={'tenPhongBan'}
                  editorType={'dxTextBox'}
                  editorOptions={{
                    readOnly: true
                  }}
                >
                  <Label text={this.props.loaiPhieu === 0 ? 'Khoa dự trù' : 'Khoa hoàn trả'} />
                </SimpleItem>
                <SimpleItem
                  dataField={'ngayLap'}
                  editorType={'dxTextBox'}
                  editorOptions={{
                    readOnly: true
                  }}
                >
                  <Label text={'Ngày lập'} />
                </SimpleItem>
              <SimpleItem
                dataField={'tenKhoVatTu'}
                editorType={'dxTextBox'}
                editorOptions={{
                  readOnly: true
                }}
              >
                <Label text={this.props.loaiPhieu === 0 ? ' Nơi chuyển' : 'Nơi nhận'} />
              </SimpleItem>
              <SimpleItem
                  colSpan={2}
                  dataField={'ghiChu'}
                  editorType={'dxTextBox'}
                  editorOptions={{
                    placeholder: "Nhập ghi chú"
                  }}
                >
                  <Label text="Ghi chú" />
                </SimpleItem>
              </GroupItem>
            </Form>
        </div>
      );
  }
}

export default FormPhieuDuTru;
