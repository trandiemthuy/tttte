import React from "react";
import {NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, pageSizes, SHADING} from '../../../../configs';
import {Form, Button} from "devextreme-react";
import {GroupItem, Label, RequiredRule, SimpleItem} from "devextreme-react/form";
import {translate} from "react-jhipster";
import {
  defaultXuatDuocBanLeModel,
  IXuatDuocBanLeModel
} from "app/modules/duoc/duoc-components/xuat-duoc-ban-le/xuat-duoc-ban-le.model";
import {IEnableXuatDuocBanLe} from "app/modules/duoc/duoc-components/xuat-duoc-ban-le/enable.model";
import notify from "devextreme/ui/notify";
import {convertToSQLDate1} from "app/modules/duoc/duoc-components/utils/funtions";
import {getDvtt, getMaNhanVien, getMaPhongBan, getMaPhongBenh} from "app/modules/duoc/duoc-components/utils/thong-tin";
import dialog from "devextreme/ui/dialog";

export interface IFormThongTinXuatDuocBanLeProp {
  selectedThongTinXuatDuoc: IXuatDuocBanLeModel;
  enableForm: IEnableXuatDuocBanLe;
  setEnableXuatDuocBanLe: Function;
  listDanhSachKhoNoiTru: any;
  listDanhSachBacSi: any;
  addToaThuocBanLe: Function;
  updateToaThuocBanLe: Function;
  deleteToaThuocBanLe: Function;
  resetDanhSachVatTu: Function;
  getDanhSachDuocTonKho: Function;
}
export interface IFormThongTinXuatDuocBanLeState {
  selectedThongTinXuatDuoc: IXuatDuocBanLeModel
}

export class FormThongTinXuatDuocBanLe extends React.Component<IFormThongTinXuatDuocBanLeProp, IFormThongTinXuatDuocBanLeState> {
  constructor(props) {
    super(props);
    this.state={
      selectedThongTinXuatDuoc: defaultXuatDuocBanLeModel
    }
  }

  componentDidUpdate(prevProps: Readonly<IFormThongTinXuatDuocBanLeProp>, prevState: Readonly<IFormThongTinXuatDuocBanLeState>, snapshot?: any): void {
    if (prevProps.selectedThongTinXuatDuoc.maToaThuoc !== this.props.selectedThongTinXuatDuoc.maToaThuoc) {
      this.setState({
        selectedThongTinXuatDuoc: this.props.selectedThongTinXuatDuoc
      });
    }
  }

  // Handles
  handleOnClickButtonThem = (e) => {
    e.preventDefault();
    this.props.addToaThuocBanLe({...this.state.selectedThongTinXuatDuoc,
      ngayXuat: convertToSQLDate1(this.state.selectedThongTinXuatDuoc.ngayXuat),
      tenBacSi: this.state.selectedThongTinXuatDuoc.maBacSi,
      dvtt: getDvtt(),
      maPhongBan: getMaPhongBan(),
      maPhongBenh: getMaPhongBenh(),
      maNhanVien: getMaNhanVien()
    });
  };
  handleOnClickButtonSua = (e) => {
    if (this.props.selectedThongTinXuatDuoc.maToaThuoc === null) {
      notify({ message: "Chưa chọn khách hàng", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    this.props.setEnableXuatDuocBanLe(false, false, true, true, false, false, false, false, false, true);
  };
  handleOnClickButtonLuu = () => {
    if (this.state.selectedThongTinXuatDuoc.noiXuat === null) {
      notify({ message: 'Chưa chọn nơi xuất', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      return;
    }
    this.props.updateToaThuocBanLe({...this.state.selectedThongTinXuatDuoc,
      ngayXuat: convertToSQLDate1(this.state.selectedThongTinXuatDuoc.ngayXuat),
      tenBacSi: this.state.selectedThongTinXuatDuoc.maBacSi,
      dvtt: getDvtt(),
      maPhongBan: getMaPhongBan(),
      maPhongBenh: getMaPhongBenh(),
      maNhanVien: getMaNhanVien(),
      tuoi: 0
    });
  };
  handleOnClickButtonHuy = () => {
    this.props.setEnableXuatDuocBanLe(true, false, false, false, false, false, false, false, false, true);
    this.props.resetDanhSachVatTu();
    this.setState({
      selectedThongTinXuatDuoc: defaultXuatDuocBanLeModel
    });
  };
  handleOnClickButtonXoa = () => {
    dialog.confirm("Xóa toa thuốc " + "<span style='color: red'>" + this.state.selectedThongTinXuatDuoc.maToaThuoc + "</span>" + " ?", "Thông báo hệ thống")
      .then(dialogResult => {
        if (dialogResult) {
          this.props.deleteToaThuocBanLe({
            ...this.state.selectedThongTinXuatDuoc,
            dvtt: getDvtt(),
            maPhongBan: getMaPhongBan(),
            maPhongBenh: getMaPhongBenh(),
            maNhanVien: getMaNhanVien()
          });
        }
      });

  };
  handleOnChangedFormXuatDuoc = (e) => {
    if (e.dataField === 'noiXuat') {
      this.props.getDanhSachDuocTonKho(getDvtt(), 'a', this.state.selectedThongTinXuatDuoc.noiXuat);
    }
  };
  // Functions

  render() {
    return (
      <div>
        <form action={''} onSubmit={this.handleOnClickButtonThem}>
          <Form
            className={'row-margin-top-5'}
            formData={this.state.selectedThongTinXuatDuoc}
            validationGroup={"formXuatDuocBanLe"}
            labelLocation={"left"}
            onFieldDataChanged={this.handleOnChangedFormXuatDuoc}
          >
            <GroupItem colCount={2}>
              <SimpleItem
                dataField={'maToaThuoc'}
                editorType={'dxTextBox'}
                editorOptions={{
                  readOnly: true,
                  inputAttr: {
                    style: 'color: red'
                  },
                  placeholder: 'Mã toa thuốc'
                }}
              >
                <Label text={translate('duoc.duocXuatDuocBanLe.formThongTinXuatDuoc.maToaThuoc')} />
              </SimpleItem>
              <SimpleItem
                dataField={'tenKhachHang'}
                editorType={'dxTextBox'}
                editorOptions={{
                  readOnly: !this.props.enableForm.itemForm,
                  inputAttr: {
                    style: 'color: red'
                  },
                  placeholder: 'Nhập họ tên khách hàng',
                  showClearButton: true
                }}
              >
                <Label text={translate('duoc.duocXuatDuocBanLe.formThongTinXuatDuoc.tenKhachHang')} />
                <RequiredRule message={"Chưa nhập tên khách hàng"} />
              </SimpleItem>
              <SimpleItem
                dataField={'ngayXuat'}
                editorType={'dxTextBox'}
                editorOptions={{
                  readOnly: true,
                  placeholder: 'Ngày xuất thuốc bán lẻ'
                }}
              >
                <Label text={translate('duoc.duocXuatDuocBanLe.formThongTinXuatDuoc.ngayXuat')} />
              </SimpleItem>
              <SimpleItem
                dataField={'ghiChu'}
                editorType={'dxTextBox'}
                editorOptions={{
                  readOnly: !this.props.enableForm.itemForm,
                  placeholder: 'Nhập ghi chú xuất thuốc bán lẻ',
                  showClearButton: true
                }}
              >
                <Label text={translate('duoc.duocXuatDuocBanLe.formThongTinXuatDuoc.ghiChu')} />
              </SimpleItem>
              <SimpleItem
                dataField={'noiXuat'}
                editorType={'dxSelectBox'}
                editorOptions={{
                  dataSource: this.props.listDanhSachKhoNoiTru,
                  valueExpr: 'maKhoVatTu',
                  displayExpr: 'tenKhoVatTu',
                  placeholder: 'Chọn kho xuất thuốc bán lẻ',
                  showClearButton: true,
                  readOnly: !this.props.enableForm.itemForm
                }}
              >
                <Label text={translate('duoc.duocXuatDuocBanLe.formThongTinXuatDuoc.noiXuat')} />
                <RequiredRule message={"Chưa chọn nơi xuất"} />
              </SimpleItem>
              <SimpleItem
                dataField={'maBacSi'}
                editorType={'dxSelectBox'}
                editorOptions={{
                  dataSource: this.props.listDanhSachBacSi,
                  valueExpr: 'maBacSi',
                  displayExpr: 'tenBacSi',
                  placeholder: 'Chọn bác sĩ xuất thuốc',
                  showClearButton: true,
                  readOnly: !this.props.enableForm.itemForm
                }}
              >
                <Label text={translate('duoc.duocXuatDuocBanLe.formThongTinXuatDuoc.tenBacSi')} />
                <RequiredRule message={"Chưa chọn bác sĩ xuất thuốc"} />
              </SimpleItem>
              <GroupItem
                colCount={2}
              >
                <SimpleItem
                  dataField={'maICD'}
                  editorType={'dxTextBox'}
                  editorOptions={{
                    readOnly: !this.props.enableForm.itemForm,
                    placeholder: 'Nhập ICD bệnh chính',
                    showClearButton: true
                  }}
                >
                  <Label text={translate('duoc.duocXuatDuocBanLe.formThongTinXuatDuoc.chanDoanICD')} />
                </SimpleItem>
                <SimpleItem
                  dataField={'tenICD'}
                  editorType={'dxTextBox'}
                  editorOptions={{
                    readOnly: !this.props.enableForm.itemForm,
                    placeholder: 'Nhập tên bệnh chính',
                    showClearButton: true
                  }}
                >
                  <Label visible={false} text={'Tên ICD'} />
                </SimpleItem>
              </GroupItem>
              <GroupItem
                colCount={2}
              >
                <SimpleItem
                  dataField={'maBenhPhu'}
                  editorType={'dxTextBox'}
                  editorOptions={{
                    readOnly: !this.props.enableForm.itemForm,
                    placeholder: 'Nhập ICD bệnh phụ',
                    showClearButton: true
                  }}
                >
                  <Label text={translate('duoc.duocXuatDuocBanLe.formThongTinXuatDuoc.benhPhu')} />
                </SimpleItem>
                <SimpleItem
                  dataField={'tenBenhPhu'}
                  editorType={'dxTextBox'}
                  editorOptions={{
                    readOnly: !this.props.enableForm.itemForm,
                    placeholder: 'Nhập tên bệnh phụ',
                    showClearButton: true
                  }}
                >
                  <Label visible={false} text={'Tên bệnh phụ'} />
                </SimpleItem>
              </GroupItem>
            </GroupItem>
            <GroupItem
              colCount={5}
              colSpan={2}
            >
              <SimpleItem
                colSpan={5}
                render={() => {
                  return <div className={'div-button'}>
                    <Button
                      icon={'plus'}
                      validationGroup={'formXuatDuocBanLe'}
                      useSubmitBehavior={true}
                      width={120}
                      type={"default"}
                      text={'Thêm'}
                      disabled={!this.props.enableForm.btnThem}
                      className={'btn-margin-5'}
                    />
                    <Button
                      icon={'edit'}
                      width={120}
                      type={"default"}
                      text={'Sửa'}
                      disabled={!this.props.enableForm.btnSua}
                      className={'btn-margin-5'}
                      onClick={this.handleOnClickButtonSua}
                    />
                    <Button
                      icon={'save'}
                      width={120}
                      type={"default"}
                      text={'Lưu'}
                      disabled={!this.props.enableForm.btnLuu}
                      className={'btn-margin-5'}
                      onClick={this.handleOnClickButtonLuu}
                    />
                    <Button
                      width={120}
                      icon={'close'}
                      type={"default"}
                      text={'Hủy'}
                      disabled={!this.props.enableForm.btnHuy}
                      className={'btn-margin-5'}
                      onClick={this.handleOnClickButtonHuy}
                    />
                    <Button
                      icon={'clear'}
                      width={120}
                      type={"danger"}
                      text={'Xóa'}
                      disabled={!this.props.enableForm.btnXoa}
                      className={'btn-margin-5'}
                      onClick={this.handleOnClickButtonXoa}
                    />
                  </div>
                 }
                }
              />
            </GroupItem>
          </Form>
        </form>
      </div>
    );
  }
}

export default FormThongTinXuatDuocBanLe;
