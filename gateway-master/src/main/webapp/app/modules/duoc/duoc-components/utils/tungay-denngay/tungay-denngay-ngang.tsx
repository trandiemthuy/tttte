import React from "react";
import { DateBox, Button as DateBoxButton } from 'devextreme-react/date-box';
import {convertToDate1, convertToSQLDate} from "app/modules/duoc/duoc-components/utils/funtions";
import Box, {Item as BoxItem} from "devextreme-react/box";
import {Button} from "devextreme-react";
import AdvanceDateBox from "app/modules/duoc/duoc-components/utils/advance-datebox/advance-datebox";
export interface ITuNgayDenNgayComponentProp {
  handleOnClickButtonLamMoi: Function;
  labelTuNgayWidth?: string | number;
  dateboxTuNgayWidth?: string | number;
  labelDenNgayWidth?: string | number;
  dateboxDenNgayWidth?: string | number;
  renderRight?: Function;
  titleButton?: string;
  valueTuNgay?: string;
  isTuNgayDauThang?: boolean;
  isDenNgayDauThang?: boolean;
}
export interface ITuNgayDenNgayComponentState {
  dateValue: any;
}

export class TuNgayDenNgayNgangComponent extends React.Component<ITuNgayDenNgayComponentProp, ITuNgayDenNgayComponentState> {
  private refTuNgay = React.createRef<AdvanceDateBox>();
  private refDenNgay = React.createRef<AdvanceDateBox>();
  constructor(props) {
    super(props);
  }

  handleOnClickButtonLamMoi = (e) => {
    this.props.handleOnClickButtonLamMoi();
  };
  getTuNgayDenNgay = () => {
    return {
      tuNgay: this.refTuNgay.current.getValue(),
      denNgay: this.refDenNgay.current.getValue()
    };
  };

  render() {
    return (
      <div style={{ width: '100%'}}>
        <Box direction="row" width="100%" height={'auto'} className={'row-padding-top-5'}>
          <BoxItem baseSize={this.props.labelTuNgayWidth === undefined ? '10%' : this.props.labelTuNgayWidth} ratio={0}>
            <div className={'col-align-right'}>
              Từ ngày
            </div>
          </BoxItem>
          <BoxItem baseSize={this.props.dateboxTuNgayWidth === undefined ? '20%' : this.props.dateboxTuNgayWidth} ratio={0}>
            <AdvanceDateBox
              ref={this.refTuNgay}
              className={'date-border-1'}
              isDauThang={this.props.isTuNgayDauThang === undefined ? false : this.props.isTuNgayDauThang}
            />
          </BoxItem>
          <BoxItem baseSize={this.props.labelDenNgayWidth === undefined ? '10%' : this.props.labelDenNgayWidth} ratio={0}>
            <div className={'col-align-right'}>
              Đến ngày
            </div>
          </BoxItem>
          <BoxItem baseSize={this.props.dateboxDenNgayWidth === undefined ? '20%' : this.props.dateboxDenNgayWidth} ratio={0}>
            <AdvanceDateBox
              ref={this.refDenNgay}
              className={'date-border-1'}
              isDauThang={this.props.isDenNgayDauThang === undefined ? false : this.props.isDenNgayDauThang}
            />
          </BoxItem>
          <BoxItem baseSize={'40%'} ratio={0}>
            <div className={'col-margin-left-5 div-display-flex'}>
              <Button
                icon={'refresh'}
                text={this.props.titleButton === undefined ? 'Làm mới' : this.props.titleButton}
                // width={150}
                type={'default'}
                className={'col-margin-left-5 button-duoc'}
                onClick={this.handleOnClickButtonLamMoi}
              />
              {
                this.props.renderRight !== undefined ? this.props.renderRight() : null
              }
            </div>
          </BoxItem>
        </Box>
      </div>
    );
  }
}

export default TuNgayDenNgayNgangComponent;
