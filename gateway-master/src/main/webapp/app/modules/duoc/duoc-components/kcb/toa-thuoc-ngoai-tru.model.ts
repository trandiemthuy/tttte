export interface IToaThuocNgoaiTruModel {
  maUser?: number; // Mã người dùng
  idGiaoDich?: number; // id giao dịch
  maPhongRaThuoc?: string; // Mã phòng ra thuốc
  capCuu?: number; // Cấp cứu: 0: Không phải      1: Cấp cứu
  dvtt?: string; // Đơn vị trực thuộc
  maToaThuoc?: string; // Mã toa thuốc
  maKhoVatTu?: number; // Mã kho vật tư
  maVatTu?: number; // Mã vật tư
  tenVatTu?: string; // Tên vật tư
  tenGoc?: string; // Tên gốc
  nghiepVu?: string; // Nghiệp vụ
  soLuong?: number; // Số lượng
  soLuongThucLinh?: number; // Số lượng thực lĩnh
  donGiaBV?: number; // Đơn giá bệnh viện
  donGiaBH?: number; // Đơn giá bảo hiểm
  thanhTien?: number; // Thành tiền
  soNgay?: number; // Số ngày sử dụng
  sang?: number; // Số lượng thuốc sử dụng buổi sáng
  trua?: number; // Số lượng thuốc sử dụng buổi trưa
  chieu?: number; // Số lượng thuốc sử dụng buổi chiều
  toi?: number; // Số lượng thuốc sử dụng buổi tối
  ngayRaToa?: string; // Ngày ra toa
  ghiChu?: string; // Ghi chú toa thuốc
  maBacSi?: number; // Mã bác sĩ ra thuốc
  cachSuDung?: string; // Cách sử dụng
  dvt?: string; // Đơn vị tính
  coBHYT?: number; // Có BHYT (tỉ lễ miễn giảm)
  namHienTai?: number; // Năm hiện tại
  maBenhNhan?: string; // Mã bệnh nhân
  soPhieuThanhToan?: string; // Số phiếu thanh toán
  idTiepNhan?: string; // id tiếp nhận
  maKhamBenh?: string; // Mã khám bệnh
  ngayHienTai?: string; // Ngày hiện tại
  daThanhToan?: string; // Đã thanh toán
  soVaoVien?: number; // số vào viện
  maGoiDichVu?: string; // Mã gói dịch vụ
  soPhieu?: string; // Số phiếu
  maCDHA?: string; // Mã chuẩn đoán hình ảnh
  soPhieuTTPT?: string; // Số phiếu tiểu thuật phẫu thuật
  maTTPT?: string; // Mã tiểu thuật phẫu thuật
  soDangKyMN?: string; // Số đăng ký MN
  soPhieuXN?: string; // Số phiếu xét  nghiệm
  maKhoa?: string; // Mã khoa ra thuốc
  maPhong?: string; // Mã phòng ra thuốc
  donGia82112?: number; // Đơn giá theo tham số 82112
  donGiaKhongLamTron?: number; // Đơn giá không làm tròn
  tachToaDichVu?: number; // Tách toa dịch vụ: 0: Không tách   1: Tách
  sttToaThuoc?: number;
}

export const defaultToaThuocNgoaiTruModel: IToaThuocNgoaiTruModel = {
  maUser: null, // Mã người dùng
  idGiaoDich: null, // id giao dịch
  maPhongRaThuoc: null, // Mã phòng ra thuốc
  capCuu: null, // Cấp cứu: 0: Không phải      1: Cấp cứu
  dvtt: null, // Đơn vị trực thuộc
  maToaThuoc: null, // Mã toa thuốc
  maKhoVatTu: null, // Mã kho vật tư
  maVatTu: null, // Mã vật tư
  tenVatTu: null, // Tên vật tư
  tenGoc: null, // Tên gốc
  nghiepVu: null, // Nghiệp vụ
  soLuong: null, // Số lượng
  soLuongThucLinh: null, // Số lượng thực lĩnh
  donGiaBV: null, // Đơn giá bệnh viện
  donGiaBH: null, // Đơn giá bảo hiểm
  thanhTien: null, // Thành tiền
  soNgay: null, // Số ngày sử dụng
  sang: null, // Số lượng thuốc sử dụng buổi sáng
  trua: null, // Số lượng thuốc sử dụng buổi trưa
  chieu: null, // Số lượng thuốc sử dụng buổi chiều
  toi: null, // Số lượng thuốc sử dụng buổi tối
  ngayRaToa: null, // Ngày ra toa
  ghiChu: null, // Ghi chú toa thuốc
  maBacSi: null, // Mã bác sĩ ra thuốc
  cachSuDung: null, // Cách sử dụng
  dvt: null, // Đơn vị tính
  coBHYT: null, // Có BHYT (tỉ lễ miễn giảm)
  namHienTai: null, // Năm hiện tại
  maBenhNhan: null, // Mã bệnh nhân
  soPhieuThanhToan: null, // Số phiếu thanh toán
  idTiepNhan: null, // id tiếp nhận
  maKhamBenh: null, // Mã khám bệnh
  ngayHienTai: null, // Ngày hiện tại
  daThanhToan: null, // Đã thanh toán
  soVaoVien: null, // số vào viện
  maGoiDichVu: null, // Mã gói dịch vụ
  soPhieu: null, // Số phiếu
  maCDHA: null, // Mã chuẩn đoán hình ảnh
  soPhieuTTPT: null, // Số phiếu tiểu thuật phẫu thuật
  maTTPT: null, // Mã tiểu thuật phẫu thuật
  soDangKyMN: null, // Số đăng ký MN
  soPhieuXN: null, // Số phiếu xét  nghiệm
  maKhoa: null, // Mã khoa ra thuốc
  maPhong: null, // Mã phòng ra thuốc
  donGia82112: null, // Đơn giá theo tham số 82112
  donGiaKhongLamTron: null, // Đơn giá không làm tròn
  tachToaDichVu: null, // Tách toa dịch vụ: 0: Không tách   1: Tách
  sttToaThuoc: null
};
