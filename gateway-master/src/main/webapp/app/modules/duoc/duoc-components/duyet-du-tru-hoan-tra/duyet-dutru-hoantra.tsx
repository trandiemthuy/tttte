import React from 'react';
import {RouteComponentProps} from "react-router";
import {IRootState} from "app/shared/reducers";
import {connect} from "react-redux";
import { Tabs, DateBox, Button, TextBox, DropDownBox, DataGrid, NumberBox, Validator, SelectBox, CheckBox } from 'devextreme-react';
import Form, { Item, SimpleItem, Label, RequiredRule, EmptyItem, ButtonItem, ButtonOptions, GroupItem, CompareRule } from 'devextreme-react/form';
import Box, {Item as BoxItem} from "devextreme-react/box";
import notify from "devextreme/ui/notify";
import dialog from "devextreme/ui/dialog";
import {printType1, exportXLSType1} from '../utils/print-helper';
import {
  getDanhSachPhieuDuTruHoanTraDuyet,
  getDanhSachChiTietPhieuDutru,
  getDanhSachBanInDuTruHoanTra,
  printPhieuDuTruNoiTru,
  printPhieuXuatKhoDuTruNoiTru,
  printSoYLenhDuTruNoiTru,
  printDanhSachToaThuoc,
  printPhieuHoanTraNoiTru,
  exportExcelDuTruHoanTraDuyet,
  printPhieuXuatKhoHoanTraNoiTru,
  duyetDuTruHoanTra
} from './duyet-du-tru-hoan-tra.reducer';

import {
  convertToDate,
  convertToDate1,
  convertToSQLDate, convertToSQLDate1,
  getMoTaThamSo
} from "app/modules/duoc/duoc-components/utils/funtions";
import {apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING} from '../../configs';
import PopUpChiTietPhieuDuyet
  from "app/modules/duoc/duoc-components/duyet-phieu/components/popup-chi-tiet-phieu/popup-chi-tiet-phieu";
import {
  getDvtt, getMaNhanVien, getMaPhongBan, getMaPhongBenh,
  getTenBenhVien,
  getTenNhanVien, getTenPhongBan,
  getTenTinh
} from "app/modules/duoc/duoc-components/utils/thong-tin";
import PopUpDanhSachBanInChuyenKho
  from "app/modules/duoc/duoc-components/chuyen-kho/components/popup-danh-sach-ban-in/popup-danh-sach-ban-in";
import TuNgayDenNgayNgangComponent
  from "app/modules/duoc/duoc-components/utils/tungay-denngay/tungay-denngay-ngang";
import AdvanceDateBox from "app/modules/duoc/duoc-components/utils/advance-datebox/advance-datebox";
import GridDanhSachPhieuDuTruHoanTra
  from "app/modules/duoc/duoc-components/duyet-du-tru-hoan-tra/components/grid-phieu-dutru-hoantra/grid-phieu-du-tru-hoan-tra";
import PopUpChiTietPhieuDuTruHoanTra
  from "app/modules/duoc/duoc-components/duyet-du-tru-hoan-tra/components/popup-chitiet-dutru-hoantra/popup-chitiet-dutru-hoantra";
import DuocLabelComponent from "app/modules/duoc/duoc-components/utils/duoc-label/duoc-label";

export interface IDuyetPhieuProp extends StateProps, DispatchProps, RouteComponentProps<{ nghiepVu: string; isBANT: string; }> {
  account: any;
}

export interface IDuyetPhieuState {
  khoChuyen: {
    maKhoVaTTu: number,
    tenKhoVatTu: string
  }
  duyet: number; // 0 : Chưa duyệt    1: Đã duyệt     2: Đã nhận
  selectedPhieuChuyenDuTruHoanTra: any;
  chiTietPopUp: {
    chiTiet: boolean;
    chiTietThoiGian: boolean;
  }
  printPopUp: {
    loaiIn: number; // 0: In phiếu dự trù nội trú   1: In phiếu dự trù nội trú (tất cả)       2: In phiếu xuất kho dự trù    3: In phiếu hoàn trả    4: In phiếu hoàn trả (tất cả)       5: In phiếu xuất kho hoàn trả
    visible: boolean;
    loaiFile: string;
  }
  loadingConfigs: {
    visible: boolean;
    position: string;
  }
}

export class DuyetPhieuComponent extends React.Component<IDuyetPhieuProp, IDuyetPhieuState> {
  private refTuNgayDenNgay = React.createRef<TuNgayDenNgayNgangComponent>();
  private refNgayDuyet = React.createRef<AdvanceDateBox>();
  constructor(props) {
    super(props);
    this.state = {
      khoChuyen: {
        maKhoVaTTu: null,
        tenKhoVatTu: ''
      },
      duyet: 2,
      selectedPhieuChuyenDuTruHoanTra: {},
      chiTietPopUp: {
        chiTiet: false,
        chiTietThoiGian: false
      },
      printPopUp: {
        loaiIn: 0,
        visible: false,
        loaiFile: 'pdf'
      },
      loadingConfigs: {
        visible: false,
        position: 'this'
      }
    }
  }

  componentDidMount(): void {
  }

  componentDidUpdate(prevProps: Readonly<IDuyetPhieuProp>, prevState: Readonly<IDuyetPhieuState>, snapshot?: any): void {
    if (prevProps.match.params.nghiepVu !== this.props.match.params.nghiepVu) {
      this.handleOnClickButtonLamMoi();
    }
  }

  // Handles
  handleOnChangeTrangThai = (e) => {
    this.setState({
      duyet: e.value ? 3 : 2
    }, () => {
      this.handleOnClickButtonLamMoi();
    });
  };
  handleOnClickButtonLamMoi = () => {
    this.getDanhSachPhieuDuTruHoanTra(
      getDvtt(),
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay,
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay,
      this.state.duyet,
      parseInt(this.props.match.params.isBANT, 0),
      getMaPhongBan(),
      getMaNhanVien(),
      this.props.match.params.nghiepVu === 'dutru' ? 0 : 1
    );
  };
  handleOnSelectionChangeGridDanhSach = (e) => {
    this.setState({
      selectedPhieuChuyenDuTruHoanTra: e.selectedRowsData
    });
  };
  handleOnContextMenuGridDanhSach = (e) => {
    if (e.row.data === undefined) {
      return;
    }
    e.component.selectRowsByIndexes(e.rowIndex);
    this.setState({
      selectedPhieuChuyenDuTruHoanTra: [e.row.data]
    });
    if (e.row.rowType === 'data') {
      if (this.props.match.params.nghiepVu === 'dutru') {
        e.items = [
          {
            icon: 'bulletlist',
            text: "Chi tiết phiếu dự trù",
            onItemClick: () => {
              this.handleOnClickMenuXemChiTiet(
                getDvtt(),
                0,
                this.state.selectedPhieuChuyenDuTruHoanTra[0].ID_PHIEU_DU_TRU_TONG,
                0,
                getMaPhongBan(),
                0,
                0
              );
            }
          },
          {
            icon: 'bulletlist',
            text: "Chi tiết phiếu dự trù thời gian",
            onItemClick: () => {
              this.handleOnClickMenuXemChiTiet(
                getDvtt(),
                0,
                this.state.selectedPhieuChuyenDuTruHoanTra[0].ID_PHIEU_DU_TRU_TONG,
                0,
                getMaPhongBan(),
                1,
                0
              );
            }
          },
          {
            icon: 'print',
            text: "In phiếu dự trù",
            items: [{
              icon: 'exportpdf',
              text: "In phiếu dự trù (PDF)",
              onItemClick: () => {
                this.handleChangePopUpPrintState(0, true, 'pdf');
              }
            },
              {
                icon: 'exportxlsx',
                text: "In phiếu dự trù (XLS)",
                onItemClick: () => {
                  this.handleChangePopUpPrintState(0, true, 'xls');
                }
              },
              {
                icon: 'docfile',
                text: "In phiếu dự trù (RTF)",
                onItemClick: () => {
                  this.handleChangePopUpPrintState(0, true, 'rtf');
                }
              }]
          },
          {
            icon: 'print',
            text: "In phiếu dự trù (tất cả)",
            items: [{
              icon: 'exportpdf',
              text: "In phiếu dự trù (tất cả) (PDF)",
              onItemClick: () => {
                this.handleChangePopUpPrintState(1, true, 'pdf');
              }
            },
              {
                icon: 'exportxlsx',
                text: "In phiếu dự trù (tất cả) (XLS)",
                onItemClick: () => {
                  this.handleChangePopUpPrintState(1, true, 'xls');
                }
              },
              {
                icon: 'docfile',
                text: "In phiếu dự trù (tất cả) (RTF)",
                onItemClick: () => {
                  this.handleChangePopUpPrintState(1, true, 'rtf');
                }
              }]
          },
          {
            icon: 'print',
            text: "In phiếu xuất kho",
            items: [{
              icon: 'exportpdf',
              text: "In phiếu xuất kho (PDF)",
              onItemClick: () => {
                this.handleChangePopUpPrintState(2, true, 'pdf');
              }
            },
              {
                icon: 'exportxlsx',
                text: "In phiếu xuất kho (XLS)",
                onItemClick: () => {
                  this.handleChangePopUpPrintState(2, true, 'xls');
                }
              },
              {
                icon: 'docfile',
                text: "In phiếu xuất kho (RTF)",
                onItemClick: () => {
                  this.handleChangePopUpPrintState(2, true, 'rtf');
                }
              }]
          },
          {
            icon: 'exportxlsx',
            text: "Sổ y lệnh",
            onItemClick: () => {
              const nghiepVu = this.props.match.params.nghiepVu;
              const selectedPhieuDuTru = this.state.selectedPhieuChuyenDuTruHoanTra[0];
              this.handleOnClickMenuInSoYLenh(
                nghiepVu === 'dutru' ? selectedPhieuDuTru.ID_PHIEU_DU_TRU_TONG : selectedPhieuDuTru.ID_PHIEU_HOAN_TRA_TONG,
                selectedPhieuDuTru.SOPHIEUTAO_LUUTRU,
                getDvtt(),
                0,
                0,
                getMaPhongBan(),
                getTenPhongBan(),
                getTenPhongBan(),
                "",
                "",
                getMaPhongBan(),
                getTenTinh(),
                getTenBenhVien(),
                this.handleLoading
              );
            }
          },
          {
            icon: 'print',
            text: "In danh sách toa thuốc",
            items: [{
              icon: 'exportpdf',
              text: "In danh sách toa thuốc (PDF)",
              onItemClick: () => {
                this.handleOnClickMenuInDSToaThuoc('pdf');
              }
            },
              {
                icon: 'exportxlsx',
                text: "In danh sách toa thuốc (XLS)",
                onItemClick: () => {
                  this.handleOnClickMenuInDSToaThuoc('xls');
                }
              },
              {
                icon: 'docfile',
                text: "In danh sách toa thuốc (RTF)",
                onItemClick: () => {
                  this.handleOnClickMenuInDSToaThuoc('rtf');
                }
              }]
          }
          ];
      } else {
        e.items = [
          {
            icon: 'bulletlist',
            text: "Chi tiết phiếu hoàn trả",
            onItemClick: () => {
              this.handleOnClickMenuXemChiTiet(
                getDvtt(),
                0,
                0,
                this.state.selectedPhieuChuyenDuTruHoanTra[0].ID_PHIEU_HOAN_TRA_TONG,
                getMaPhongBan(),
                0,
                1
              );
            }
          },
          {
            icon: 'print',
            text: "In phiếu hoàn trả",
            items: [{
              icon: 'exportpdf',
              text: "In phiếu hoàn trả (PDF)",
              onItemClick: () => {
                this.handleChangePopUpPrintState(3, true, 'pdf');
              }
            },
              {
                icon: 'exportxlsx',
                text: "In phiếu hoàn trả (XLS)",
                onItemClick: () => {
                  this.handleChangePopUpPrintState(3, true, 'xls');
                }
              },
              {
                icon: 'docfile',
                text: "In phiếu hoàn trả (RTF)",
                onItemClick: () => {
                  this.handleChangePopUpPrintState(3, true, 'rtf');
                }
              }]
          },
          {
            icon: 'print',
            text: "In phiếu hoàn trả tổng hợp",
            items: [{
              icon: 'exportpdf',
              text: "In phiếu hoàn trả tổng hợp (PDF)",
              onItemClick: () => {
                this.handleChangePopUpPrintState(4, true, 'pdf');
              }
            },
              {
                icon: 'exportxlsx',
                text: "In phiếu hoàn trả tổng hợp (XLS)",
                onItemClick: () => {
                  this.handleChangePopUpPrintState(4, true, 'xls');
                }
              },
              {
                icon: 'docfile',
                text: "In phiếu hoàn trả tổng hợp (RTF)",
                onItemClick: () => {
                  this.handleChangePopUpPrintState(4, true, 'rtf');
                }
              }]
          },
          {
            icon: 'print',
            text: "In phiếu xuất kho hoàn trả",
            items: [{
              icon: 'exportpdf',
              text: "In phiếu xuất kho hoàn trả (PDF)",
              onItemClick: () => {
                this.handleChangePopUpPrintState(5, true, 'pdf');
              }
            },
              {
                icon: 'exportxlsx',
                text: "In phiếu xuất kho hoàn trả (XLS)",
                onItemClick: () => {
                  this.handleChangePopUpPrintState(5, true, 'xls');
                }
              },
              {
                icon: 'docfile',
                text: "In phiếu xuất kho hoàn trả (RTF)",
                onItemClick: () => {
                  this.handleChangePopUpPrintState(5, true, 'rtf');
                }
              }]
          }
        ];
      }
    }
  };
  handleOnClickMenuXemChiTiet = (dvtt: string, hinhThucXem: number, idPhieuDuTruTong: number, idPhieuHoanTraTong: number, maPhongBan: string, thoiGian: number, loaiPhieu: number) => {
    this.props.getDanhSachChiTietPhieuDutru(dvtt,
      hinhThucXem,
      idPhieuDuTruTong,
      idPhieuHoanTraTong,
      maPhongBan,
      thoiGian,
      loaiPhieu);
    this.handleChangePopUpChiTietState(thoiGian === 0, thoiGian !== 0);
  };
  handleChangePopUpChiTietState = (popupChiTiet: boolean, popupChiTietThoiGian: boolean) => {
    this.setState({
      chiTietPopUp: {
        chiTiet: popupChiTiet,
        chiTietThoiGian: popupChiTietThoiGian
      }
    });
  };
  handleChangePopUpPrintState = (loaiIn: number, visible: boolean, loaiFile: string) => {
    const selectedPhieuChuyen = this.state.selectedPhieuChuyenDuTruHoanTra[0];
    this.props.getDanhSachBanInDuTruHoanTra(getDvtt(),
      this.props.match.params.nghiepVu === 'dutru' ? selectedPhieuChuyen.ID_PHIEU_DU_TRU_TONG : selectedPhieuChuyen.ID_PHIEU_HOAN_TRA_TONG,
      0,
      [1, 4].includes(loaiIn) ? 0 : 1,
      this.props.match.params.nghiepVu === 'dutru' ? 0 : 1);
    this.setState({
      printPopUp: {
        loaiIn,
        visible,
        loaiFile
      }
    });
  };
  handleLoading = (visible: boolean) => {
    this.setState({
      loadingConfigs: {
        ...this.state.loadingConfigs,
        visible
      }
    });
  };
  handleOnSelectRowGridDanhSachBanIn = (e: any) => {
    const selectedLoaiVatTu = e;
    const nghiepVu = this.props.match.params.nghiepVu;
    const selectedPhieuDuTru = this.state.selectedPhieuChuyenDuTruHoanTra[0];
    if (this.state.printPopUp.loaiIn === 0 || this.state.printPopUp.loaiIn === 1) {
      this.props.printPhieuDuTruNoiTru(
        getDvtt(),
        nghiepVu === 'dutru' ? selectedPhieuDuTru.ID_PHIEU_DU_TRU_TONG : selectedPhieuDuTru.ID_PHIEU_HOAN_TRA_TONG,
        selectedPhieuDuTru.SOPHIEUTAO_LUUTRU,
        selectedPhieuDuTru.NGAY,
        selectedLoaiVatTu.THANH_TIEN,
        selectedLoaiVatTu.MAKHOVATTU,
        selectedLoaiVatTu.TENKHOVATTU,
        getTenPhongBan(),
        selectedLoaiVatTu.TEN_PHONGBAN,
        selectedLoaiVatTu.MALOAIVATTU,
        0,
        0,
        this.state.printPopUp.loaiIn === 1 ? 0 : 1,
        getMaNhanVien(),
        getTenNhanVien(),
        getTenBenhVien(),
        getTenTinh(),
        this.state.printPopUp.loaiFile,
        this.handleLoading
      );
    } else if (this.state.printPopUp.loaiIn === 2) {
      this.props.printPhieuXuatKhoDuTruNoiTru(
        getDvtt(),
        nghiepVu === 'dutru' ? selectedPhieuDuTru.ID_PHIEU_DU_TRU_TONG : selectedPhieuDuTru.ID_PHIEU_HOAN_TRA_TONG,
        selectedPhieuDuTru.SOPHIEUTAO_LUUTRU,
        selectedPhieuDuTru.NGAY,
        selectedLoaiVatTu.THANH_TIEN,
        selectedLoaiVatTu.MAKHOVATTU,
        selectedLoaiVatTu.TENKHOVATTU,
        getTenPhongBan(),
        selectedLoaiVatTu.TEN_PHONGBAN,
        selectedLoaiVatTu.MALOAIVATTU,
        getTenNhanVien(),
        getTenBenhVien(),
        getTenTinh(),
        this.state.printPopUp.loaiFile,
        this.handleLoading
      )
    } else if (this.state.printPopUp.loaiIn === 3 || this.state.printPopUp.loaiIn === 4) {
      this.props.printPhieuHoanTraNoiTru(
        getDvtt(),
        nghiepVu === 'dutru' ? selectedPhieuDuTru.ID_PHIEU_DU_TRU_TONG : selectedPhieuDuTru.ID_PHIEU_HOAN_TRA_TONG,
        selectedPhieuDuTru.SOPHIEUTAO_LUUTRU,
        selectedPhieuDuTru.NGAY,
        selectedLoaiVatTu.THANH_TIEN,
        selectedLoaiVatTu.MAKHOVATTU,
        selectedLoaiVatTu.TENKHOVATTU,
        getTenPhongBan(),
        selectedLoaiVatTu.TEN_PHONGBAN,
        selectedLoaiVatTu.MALOAIVATTU,
        0,
        this.state.printPopUp.loaiIn === 4 ? 0 : 1,
        getMaNhanVien(),
        getTenNhanVien(),
        getTenBenhVien(),
        getTenTinh(),
        this.state.printPopUp.loaiFile,
        this.handleLoading
      );
    } else if (this.state.printPopUp.loaiIn === 5) {
      this.props.printPhieuXuatKhoHoanTraNoiTru(
        nghiepVu === 'dutru' ? selectedPhieuDuTru.ID_PHIEU_DU_TRU_TONG : selectedPhieuDuTru.ID_PHIEU_HOAN_TRA_TONG,
        getDvtt(),
        selectedPhieuDuTru.SOPHIEUTAO_LUUTRU,
        selectedPhieuDuTru.NGAY,
        selectedLoaiVatTu.THANH_TIEN,
        selectedLoaiVatTu.MAKHOVATTU,
        selectedLoaiVatTu.TENKHOVATTU,
        getTenPhongBan(),
        selectedLoaiVatTu.TEN_PHONGBAN,
        selectedLoaiVatTu.MALOAIVATTU,
        0,
        getTenNhanVien(),
        getTenBenhVien(),
        getTenTinh(),
        this.state.printPopUp.loaiFile,
        this.handleLoading
      );
    }
  };
  handleOnClickButtonExportExcel = () => {
    this.props.exportExcelDuTruHoanTraDuyet(
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay,
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay,
      getDvtt(),
      this.state.duyet,
      this.props.match.params.isBANT,
      this.props.match.params.nghiepVu === 'dutru' ? 0 : 1,
      this.handleLoading
    );
  };
  handleOnClickMenuInSoYLenh = (idPhieuDuTruTong: number, soPhieuTaoLuuTru: string, dvtt: string, tuTuThuoc: number, mauBaoCao: number, maKhoa: string, tenKhoa: string, tenPhongBanDuTru: string, tuNgay: string, denNgay: string, maPhong: string, tenTinh: string, tenBenhVien: string, handleLoading: Function) => {
    this.props.printSoYLenhDuTruNoiTru(
      idPhieuDuTruTong,
      soPhieuTaoLuuTru,
      dvtt,
      tuTuThuoc,
      mauBaoCao,
      maKhoa,
      tenKhoa,
      tenPhongBanDuTru,
      tuNgay,
      denNgay,
      maPhong,
      tenTinh,
      tenBenhVien,
      handleLoading
    );
  };
  handleOnClickMenuInDSToaThuoc = (loaiFile) => {
    const nghiepVu = this.props.match.params.nghiepVu;
    const selectedPhieuDuTru = this.state.selectedPhieuChuyenDuTruHoanTra[0];
    this.props.printDanhSachToaThuoc(
      nghiepVu === 'dutru' ? selectedPhieuDuTru.ID_PHIEU_DU_TRU_TONG : selectedPhieuDuTru.ID_PHIEU_HOAN_TRA_TONG,
      selectedPhieuDuTru.SOPHIEUTAO_LUUTRU,
      getDvtt(),
      getMaPhongBan(),
      "",
      "",
      getTenBenhVien(),
      getTenTinh(),
      loaiFile,
      this.handleLoading
    )
  };
  handleOnClickButtonDuyet = () => {
    const nghiepVu = this.props.match.params.nghiepVu;
    const selectedPhieuDuTru = this.state.selectedPhieuChuyenDuTruHoanTra;
    if (selectedPhieuDuTru.length <= 0) {
      notify({ message: "Chưa chọn phiếu cần duyệt", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    const listPhieuDuyet = [];
    selectedPhieuDuTru.map((phieu, idx) => {
      const obj = {
        id: nghiepVu === 'dutru' ? phieu.ID_PHIEU_DU_TRU_TONG : phieu.ID_PHIEU_HOAN_TRA_TONG,
        soPhieu: phieu.SOPHIEUTAO_LUUTRU,
      };
      listPhieuDuyet.push(obj);
    });
    this.props.duyetDuTruHoanTra(listPhieuDuyet,
      getDvtt(),
      this.state.duyet === 2 ? 1 : 0,
      getMaNhanVien(),
      this.refNgayDuyet.current.getValue(),
      getTenPhongBan(),
      getMaPhongBenh(),
      nghiepVu === 'dutru' ? 0 : 1,
      this.handleOnClickButtonLamMoi
      );
  };

  // Funtion
  getDanhSachPhieuDuTruHoanTra = (dvtt: string, tuNgay: string, denNgay: string, trangThai: number, isBANT: number, maPhongBan: string, maNhanVien: number, loaiPhieu: number) => {
    this.props.getDanhSachPhieuDuTruHoanTraDuyet(
      dvtt,
      tuNgay,
      denNgay,
      trangThai,
      isBANT,
      maPhongBan,
      maNhanVien,
      loaiPhieu
    );
  };

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (<div>
      <div className={'duoc-title'}>
        {
          this.props.match.params.nghiepVu === 'dutru' ? 'Duyệt dự trù nội trú' : 'Duyệt hoàn trả nội trú'
        }
      </div>
      <div className={'div-box-wrapper'}>
        <DuocLabelComponent text={this.props.match.params.nghiepVu === 'dutru' ? 'Thông tin lọc phiếu dự trù' : 'Thông tin lọc phiếu hoàn trả'}/>
        <Box
          direction="col"
          width="100%"
          height={'auto'}
        >
          <BoxItem ratio={0} baseSize={'100%'}>
            <TuNgayDenNgayNgangComponent
              ref={this.refTuNgayDenNgay}
              handleOnClickButtonLamMoi={this.handleOnClickButtonLamMoi}
              labelTuNgayWidth={'10%'}
              dateboxTuNgayWidth={'20%'}
              labelDenNgayWidth={'10%'}
              dateboxDenNgayWidth={'20%'}
              renderRight={() => {
                return <div>
                  <Button
                    icon={'exportxlsx'}
                    text={'Xuất Excel'}
                    // width={150}
                    type={'default'}
                    className={'col-margin-left-5 button-duoc'}
                    onClick={this.handleOnClickButtonExportExcel}
                  />
                  <Button
                    icon={this.state.duyet === 3 ? 'close' : 'check'}
                    text={this.state.duyet === 3 ? 'Hủy duyệt' : 'Duyệt'}
                    // width={150}
                    type={ this.state.duyet === 3 ? 'danger' : 'success'}
                    className={'col-margin-left-5 button-duoc'}
                    onClick={this.handleOnClickButtonDuyet}
                  />
                </div>
              }

              }
            />
          </BoxItem>
          <BoxItem baseSize={'100%'} ratio={0}>
            <Box direction="row" width="100%" height={'auto'} className={'row-padding-top-5'}>
              <BoxItem baseSize={'10%'} ratio={0}>
                <div className={'col-align-right'}>
                  Ngày duyệt
                </div>
              </BoxItem>
              <BoxItem baseSize={'20%'} ratio={0}>
                <AdvanceDateBox
                  ref={this.refNgayDuyet}
                />
              </BoxItem>
              <BoxItem baseSize={'15%'} ratio={0}>
                <div className={'col-align-right'}>
                  <CheckBox
                    text={'Đã duyệt'}
                    onValueChanged={this.handleOnChangeTrangThai}
                    value={this.state.duyet === 3}
                  />
                </div>
              </BoxItem>
            </Box>
          </BoxItem>
        </Box>
      </div>
      <div className={'div-box-wrapper row-margin-top-5'}>
        <DuocLabelComponent text={this.props.match.params.nghiepVu === 'dutru' ? 'Danh sách phiếu dự trù' : 'Danh sách phiếu hoàn trả'}/>
        <Box
          direction={"col"}
          width={'100%'}
        >
          <BoxItem baseSize={'100%'} ratio={0}>
            <div className={'row-padding-top-5'}>
              <GridDanhSachPhieuDuTruHoanTra
                loaiPhieu={this.props.match.params.nghiepVu === 'dutru' ? 0 : 1}
                listDanhSachPhieuDuTruHoanTra={this.props.listDanhSachPhieuDuTruHoanTraDuyet}
                handleOnContextMenuGridDanhSach={this.handleOnContextMenuGridDanhSach}
                // keyExpr={'ID_CHUYEN_KHO_VAT_TU'}
                // selectedRowKeys={[this.state.selectedPhieuChuyen]}
                onSelectionChanged={this.handleOnSelectionChangeGridDanhSach}
              />
            </div>
          </BoxItem>
          <BoxItem ratio={0} baseSize={'100%'}>
            <PopUpChiTietPhieuDuTruHoanTra
              isThoiGian={this.state.chiTietPopUp.chiTietThoiGian}
              loaiPhieu={this.props.match.params.nghiepVu === 'dutru' ? 0 : 1}
              title={'Danh sách chi tiết thuốc'}
              showPopUp={this.state.chiTietPopUp.chiTiet || this.state.chiTietPopUp.chiTietThoiGian}
              handleChangePopUpChiTietState={this.handleChangePopUpChiTietState}
              listDanhSachChiTietPhieuDuTruHoanTra={this.props.listDanhSachChiTietPhieuDuTruHoanTraDuyet}
            />
          </BoxItem>
          <BoxItem ratio={0} baseSize={'100%'}>
            <PopUpDanhSachBanInChuyenKho
              listDanhSachBanIn={this.props.listDanhSachBanInPhieuDuTruHoanTraDuyet}
              handleChangePopUpState={this.handleChangePopUpPrintState}
              handleOnSelectRowGridDanhSachBanIn={this.handleOnSelectRowGridDanhSachBanIn}
              printPopUp={this.state.printPopUp}
            />
          </BoxItem>
        </Box>
      </div>
    </div>);
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  listDanhSachPhieuDuTruHoanTraDuyet: storeState.duyetdutruhoantra.listDanhSachPhieuDuTruHoanTraDuyet,
  listDanhSachChiTietPhieuDuTruHoanTraDuyet: storeState.duyetdutruhoantra.listDanhSachChiTietPhieuDuTruHoanTraDuyet,
  listDanhSachBanInPhieuDuTruHoanTraDuyet: storeState.duyetdutruhoantra.listDanhSachBanInPhieuDuTruHoanTraDuyet
});

const mapDispatchToProps = {
  getDanhSachPhieuDuTruHoanTraDuyet,
  getDanhSachChiTietPhieuDutru,
  getDanhSachBanInDuTruHoanTra,
  printPhieuDuTruNoiTru,
  printPhieuXuatKhoDuTruNoiTru,
  printSoYLenhDuTruNoiTru,
  printDanhSachToaThuoc,
  printPhieuHoanTraNoiTru,
  exportExcelDuTruHoanTraDuyet,
  printPhieuXuatKhoHoanTraNoiTru,
  duyetDuTruHoanTra
};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DuyetPhieuComponent);
