import { getKYearFromNow } from 'app/modules/duoc/duoc-components/utils/funtions';

export interface IPhieuNhapKho {
  dongY?: number; // 1|0
  dvtt?: string;
  ghiChu?: string;
  gioNhap?: string;
  idGiaoDich?: number;
  idNhapKhoTuNhaCungCap?: number;
  kyHieuHD?: string;
  kyHieuHoaDon?: string;
  maKhoHoanTra?: number;
  maNhaCungCap?: number;
  maDonViTao?: string;
  maKhoNhan?: number;
  maNghiepVuChuyen?: number;
  maNguoiNhan?: number;
  maPhongBanNhap?: string;
  ngayHoaDon?: string; // dd/mm/yyyy
  ngayNhap?: string; // dd/mm/yyyy
  nguoiGiaoHang?: string;
  nguoiTao?: number;
  nguonDuoc?: string;
  phanTramGiaBanLe?: number;
  phieuNhapTonBanDau?: number; // 1 | 0
  soHoaDon?: string;
  soHopDong?: string;
  soLuuTru?: string;
  soPhieuNhap?: string;
  ttHoaDon?: number;
  vat?: number; // 0 -> 99
  file?: string;
  maHoiDongKiemNhap?: number;
}

export const defaultPhieuNhap: IPhieuNhapKho = {
  dongY: null, // 1|0
  dvtt: null,
  ghiChu: ' ',
  gioNhap: null,
  idGiaoDich: null,
  idNhapKhoTuNhaCungCap: null,
  kyHieuHD: ' ',
  kyHieuHoaDon: ' ',
  maKhoHoanTra: null,
  maNhaCungCap: null,
  maDonViTao: null,
  maKhoNhan: null,
  maNghiepVuChuyen: null,
  maNguoiNhan: null,
  maPhongBanNhap: null,
  ngayHoaDon: new Date().toDateString(), // dd/mm/yyyy
  ngayNhap: new Date().toDateString(), // dd/mm/yyyy
  nguoiGiaoHang: ' ',
  nguoiTao: null,
  nguonDuoc: 'dichvu',
  phanTramGiaBanLe: 0,
  phieuNhapTonBanDau: null, // 1 | 0
  soHoaDon: null,
  soHopDong: ' ',
  soLuuTru: null,
  soPhieuNhap: null,
  ttHoaDon: 0,
  vat: 0, // 0 -> 99
  file: ' ',
  maHoiDongKiemNhap: -1
};

export const resetPhieuNhap: Readonly<IPhieuNhapKho> = {
  dongY: null, // 1|0
  dvtt: null,
  ghiChu: null,
  gioNhap: null,
  idGiaoDich: null,
  idNhapKhoTuNhaCungCap: null,
  kyHieuHD: null,
  kyHieuHoaDon: null,
  maKhoHoanTra: null,
  maNhaCungCap: null,
  maDonViTao: null,
  maKhoNhan: null,
  maNghiepVuChuyen: null,
  maNguoiNhan: null,
  maPhongBanNhap: null,
  ngayHoaDon: null, // dd/mm/yyyy
  ngayNhap: null, // dd/mm/yyyy
  nguoiGiaoHang: null,
  nguoiTao: null,
  nguonDuoc: null,
  phanTramGiaBanLe: null,
  phieuNhapTonBanDau: null, // 1 | 0
  soHoaDon: null,
  soHopDong: null,
  soLuuTru: null,
  soPhieuNhap: null,
  ttHoaDon: null,
  vat: null, // 0 -> 99,
  file: null
};

export interface INhanVienKiemNhap {
  chucDanh?: string;
  dvtt?: string;
  idNhapKhoTuNhaCungCap?: number;
  maNhanVien?: string;
  soPhieuNhap?: string;
  tenNhanVien?: string;
}

export const defaultNhanVienKiemNhap: INhanVienKiemNhap = {
  chucDanh: '',
  dvtt: null,
  idNhapKhoTuNhaCungCap: null,
  maNhanVien: null,
  soPhieuNhap: null,
  tenNhanVien: ''
};

export interface IChiTietPhieuNhapKho {
  idPhieuNhap?: number;
  dvtt?: string;
  idNhapKhoCT?: number;
  nghiepVu?: number;
  soPhieuNhap?: string;
  soHoaDon?: string;
  quyCach?: string;
  quyCachQuyDoi?: number;
  soLoSanXuat?: string;
  ngaySanXuat?: string; // dd/mm/yyyy
  ngayHetHan?: string; // dd/mm/yyyy
  maVatTu?: number;
  vat?: number;
  soLuong?: number;
  soLuongQuyDoi?: number;
  donGiaHoaDon?: number;
  donGiaQuyDoi?: number;
  donGiaQuyDoiVAT?: number;
  donGia?: number;
  thanhTien?: number;
  tienVAT?: number;
  thanhTienVAT?: number;
  ghiChu?: string;
  maDonViNhan?: number;
  duocConLai?: number;
  hoatChat?: string;
  dvt?: string;
  soThau?: string;
  donGiaTruocVAT?: number;
  maNguoiNhap?: number;
  idGiaoDich?: number;
  maNhapKhoVatTu?: number;
  thanhTienHoaDon?: number;
  ngayNhap?: string;
  phanTramHaoHut?: number;
  donGiaTruocHaoHut?: number;
  donGiaSauHaoHut?: number;
  soLuongTruocHaoHut?: number;
  soLuongSauHaoHut?: number;
  phanTramDonGiaDV?: number;
  donGiaDV?: number;
  donGiaVP?: number;
  soLuongQuyCach?: number;
  donGiaChinhSua?: number;
  soLuongTruocQuyDoi?: number;
}

export const defaultChiTietPhieuNhap: IChiTietPhieuNhapKho = {
  idPhieuNhap: null,
  dvtt: null,
  idNhapKhoCT: 0,
  nghiepVu: null,
  soPhieuNhap: null,
  soHoaDon: null,
  quyCach: '',
  quyCachQuyDoi: 0,
  soLoSanXuat: '',
  ngaySanXuat: null, // dd/mm/yyyy
  ngayHetHan: new Date(getKYearFromNow(2)).toDateString(), // dd/mm/yyyy
  maVatTu: null,
  vat: null,
  soLuong: 0,
  soLuongQuyDoi: 0,
  donGiaHoaDon: 0,
  donGiaQuyDoi: 0,
  donGiaQuyDoiVAT: 0,
  donGia: 0,
  thanhTien: 0,
  tienVAT: 0,
  thanhTienVAT: 0,
  ghiChu: ' ',
  maDonViNhan: null,
  duocConLai: 0,
  hoatChat: ' ',
  dvt: ' ',
  soThau: ' ',
  donGiaTruocVAT: 0,
  maNguoiNhap: null,
  idGiaoDich: null,
  maNhapKhoVatTu: 0,
  thanhTienHoaDon: 0,
  ngayNhap: null,
  phanTramHaoHut: 0,
  donGiaTruocHaoHut: 0,
  donGiaSauHaoHut: 0,
  soLuongTruocHaoHut: 0,
  soLuongSauHaoHut: 0,
  phanTramDonGiaDV: 0,
  donGiaDV: 0,
  donGiaVP: 0,
  soLuongQuyCach: 0,
  donGiaChinhSua: 0,
  soLuongTruocQuyDoi: 0
};
