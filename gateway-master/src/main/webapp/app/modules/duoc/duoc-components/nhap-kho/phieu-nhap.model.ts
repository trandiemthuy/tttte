export interface IPhieuNhapKho {
  dongY?: number; // 1|0
  dvtt?: string;
  ghiChu?: string;
  gioNhap?: string;
  idGiaoDich?: number;
  idNhapKhoTuNhaCungCap?: number;
  kyHieuHD?: string;
  kyHieuHoaDon?: string;
  maKhoHoanTra?: number;
  maNhaCungCap?: number;
  maDonViTao?: string;
  maKhoNhan?: number;
  maNghiepVuChuyen?: number;
  maNguoiNhan?: number;
  maPhongBanNhap?: string;
  ngayHoaDon?: string; // dd/mm/yyyy
  ngayNhap?: string; // dd/mm/yyyy
  nguoiGiaoHang?: string;
  nguoiTao?: number;
  nguonDuoc?: string;
  phanTramGiaBanLe?: number;
  phieuNhapTonBanDau?: number; // 1 | 0
  soHoaDon?: string;
  soHopDong?: string;
  soLuuTru?: string;
  soPhieuNhap?: string;
  ttHoaDon?: number;
  vat?: number; // 0 -> 99
}

export const defaultPhieuNhap: IPhieuNhapKho = {
  dongY: null, // 1|0
  dvtt: null,
  ghiChu: null,
  gioNhap: null,
  idGiaoDich: null,
  idNhapKhoTuNhaCungCap: null,
  kyHieuHD: null,
  kyHieuHoaDon: null,
  maKhoHoanTra: null,
  maNhaCungCap: null,
  maDonViTao: null,
  maKhoNhan: null,
  maNghiepVuChuyen: null,
  maNguoiNhan: null,
  maPhongBanNhap: null,
  ngayHoaDon: null, // dd/mm/yyyy
  ngayNhap: null, // dd/mm/yyyy
  nguoiGiaoHang: null,
  nguoiTao: null,
  nguonDuoc: null,
  phanTramGiaBanLe: null,
  phieuNhapTonBanDau: null, // 1 | 0
  soHoaDon: null,
  soHopDong: null,
  soLuuTru: null,
  soPhieuNhap: null,
  ttHoaDon: null,
  vat: null // 0 -> 99
};

export const resetPhieuNhap: Readonly<IPhieuNhapKho> = {
  dongY: null, // 1|0
  dvtt: null,
  ghiChu: null,
  gioNhap: null,
  idGiaoDich: null,
  idNhapKhoTuNhaCungCap: null,
  kyHieuHD: null,
  kyHieuHoaDon: null,
  maKhoHoanTra: null,
  maNhaCungCap: null,
  maDonViTao: null,
  maKhoNhan: null,
  maNghiepVuChuyen: null,
  maNguoiNhan: null,
  maPhongBanNhap: null,
  ngayHoaDon: null, // dd/mm/yyyy
  ngayNhap: null, // dd/mm/yyyy
  nguoiGiaoHang: null,
  nguoiTao: null,
  nguonDuoc: null,
  phanTramGiaBanLe: null,
  phieuNhapTonBanDau: null, // 1 | 0
  soHoaDon: null,
  soHopDong: null,
  soLuuTru: null,
  soPhieuNhap: null,
  ttHoaDon: null,
  vat: null // 0 -> 99
};

export interface IChiTietPhieuNhap {
  daNhap?: number;
  donGia?: number;
  donGiaHoaDon?: number;
  donGiaQuyDoi?: number;
  donGiaQuyDoiVAT?: number;
  donGiaTruocHaoHut?: number;
  dvtt?: string;
  ghiChu?: string;
  idGiaoDich?: number;
  idNhapKhoTuNhaCungCap?: number;
  idNhapKhoTuNhaCungCapChiTiet?: number;
  maKhoNhan?: number;
  maNhapKhoVatTu?: number;
  maVatTu?: number;
  maNghiepVuChuyen?: number;
  ngayHetHan?: string;
  ngayNhap?: string;
  ngaySanXuat?: string;
  ngayXuLy?: string;
  nguoiNhap?: string;
  phanTramHaoHut?: string;
  quyCach?: string;
  quyCachQuyDoi?: string;
}
