import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection, Pager, Paging
} from 'devextreme-react/data-grid';
import {Popup} from 'devextreme-react/popup';
import {Button, ScrollView} from 'devextreme-react';
import DuocLabelComponent from "app/modules/duoc/duoc-components/utils/duoc-label/duoc-label";
import {configLoadPanel, numberFormat, pageSizes} from "app/modules/duoc/configs";

export interface IPopUpDanhSachBanInChuyenKhoProp extends IDataGridOptions {
  listDanhSachBanIn: any;
  printPopUp: {
    loaiIn: number;
    visible: boolean;
    loaiFile: string;
  };
  handleChangePopUpState: Function;
  handleOnSelectRowGridDanhSachBanIn: Function; // @params: (e: rowdata,
}
export interface IPopUpDanhSachBanInChuyenKhoState {
  selectedRow: any;
}

export class PopUpDanhSachBanInChuyenKho extends React.Component<IPopUpDanhSachBanInChuyenKhoProp, IPopUpDanhSachBanInChuyenKhoState> {
  constructor(props) {
    super(props);
  }
  handleOnClickButtonTaiXuong = (e, data) => {
    this.props.handleOnSelectRowGridDanhSachBanIn(
      data.data
    );
  };
  renderTitle = () => {
    return this.props.printPopUp.loaiFile && this.props.printPopUp.loaiFile.toUpperCase();
  };

  render() {
    return (
      <Popup
        visible={this.props.printPopUp.visible}
        onHiding={() => this.props.handleChangePopUpState(false, this.props.printPopUp.loaiFile)}
        dragEnabled={true}
        closeOnOutsideClick={false}
        showTitle={true}
        title={"Danh sách bản in (" + this.renderTitle() + ")"}
        width={900}
        height={450}
      >
        <ScrollView
          scrollByContent={true}
          scrollByThumb={true}
        >
          <div className={'div-box-wrapper'}>
            <DuocLabelComponent text={"Danh sách bản in (" + this.renderTitle() + ")"}/>
            <DataGrid
              loadPanel={configLoadPanel}
              dataSource={this.props.listDanhSachBanIn}
              showBorders={true}
              showColumnLines={true}
              rowAlternationEnabled={true}
              allowColumnReordering={true}
              allowColumnResizing={true}
              columnResizingMode={'nextColumn'}
              columnMinWidth={50}
              noDataText={'Không có dữ liệu'}
              {...this.props}
            >
              <Selection mode="single" />
              <FilterRow visible={true}
                         applyFilter={'auto'} />
              <HeaderFilter visible={true} />
              <Column
                dataField="TENLOAIVATTU"
                caption="Loại vật tư"
                dataType="string"
                alignment="left"
                width={'20%'}
              />
              <Column
                dataField="TENKHOVATTU"
                caption="Kho"
                dataType="string"
                alignment="left"
                width={'25%'}
              />
              <Column
                dataField="TEN_PHONGBAN"
                caption="Phòng ban"
                dataType="string"
                alignment="left"
                width={'25%'}
              />
              <Column
                dataField="THANH_TIEN"
                caption="Tổng tiền"
                dataType="number"
                alignment="left"
                width={'20%'}
                format={numberFormat}
              />
              <Column
                width={'10%'}
                alignment={"center"}
                cellRender={(data) => {
                  return <Button
                    icon={'download'}
                    type={'default'}
                    onClick={e => this.handleOnClickButtonTaiXuong(e, data)}
                  />
                }}
              />
              <Summary>
                <TotalItem
                  customizeText={(data) => {
                    return data.value + " loại vật tư";
                  }}
                  column="TENLOAIVATTU"
                  summaryType="count"
                />
              </Summary>
              <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
              <Paging defaultPageSize={5} />
            </DataGrid>
          </div>
        </ScrollView>
      </Popup>);
  }
}

export default PopUpDanhSachBanInChuyenKho;
