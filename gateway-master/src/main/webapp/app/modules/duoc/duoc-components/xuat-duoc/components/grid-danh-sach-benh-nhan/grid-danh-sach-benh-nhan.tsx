import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, Summary, TotalItem, Selection, Pager, Paging
} from 'devextreme-react/data-grid';
import {configLoadPanel, pageSizes} from '../../../../configs';
import { IXuatDuocModel, defaultXuatDuocModel } from '../../xuat-duoc.model';
import {translate} from "react-jhipster";

export interface IGridDanhSachBenhNhanXuatDuocProp extends IDataGridOptions {
  listDanhSachBenhNhanXuatDuoc: any;
  handleOnContextMenuGridDanhSach: any;
  getDanhSachVatTuXuatDuoc: Function;
  setEnableButton: Function;
  resetDanhSachVatTu: Function;
  handleSendDataToForm: Function;
}
export interface IGridDanhSachBenhNhanXuatDuocState {
  selectedBenhNhan: IXuatDuocModel;
  isSelect: boolean;
}

export class GridDanhSachBenhNhanXuatDuoc extends React.Component<IGridDanhSachBenhNhanXuatDuocProp, IGridDanhSachBenhNhanXuatDuocState> {
  constructor(props) {
    super(props);
    this.state = {
      selectedBenhNhan: defaultXuatDuocModel,
      isSelect: false
    };
  }

  // Handles
  handleOnRowRepared = (e) => {
    if (e.rowType === 'data' && e.data.VUOTCAN === 1) { // Bệnh nhân vượt cận
      // e.rowElement.style.backgroundImage = 'radial-gradient( circle 939px at 0.7% 2.4%,  rgba(116,106,255,1) 0%, rgba(221,221,221,1) 100.2% )';
      // e.rowElement.style.color = 'white';
      // e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
      e.cells[1].cellElement.style.backgroundImage = 'radial-gradient( circle 939px at 0.7% 2.4%,  rgba(116,106,255,1) 0%, rgba(221,221,221,1) 100.2% )';
      e.cells[1].cellElement.style.color = 'white';
      e.cells[1].cellElement.className = e.cells[1].cellElement.className.replace("dx-row-alt", "");
    }
    if (e.rowType === 'data' && e.data.XACNHAN_TRATHUOCVEKHO === 1) { // Bệnh nhân đã trả tuốc về kho
      e.cells[2].cellElement.style.backgroundImage = 'radial-gradient( circle 534px at 7.8% 17.6%,  rgba(254,253,112,1) 1.7%, rgba(248,143,111,1) 91.8% )';
      e.cells[2].cellElement.style.color = 'black';
      e.cells[2].cellElement.className = e.cells[2].cellElement.className.replace("dx-row-alt", "");
    }
  };
  handleOnSelectBenhNhanXuatDuoc = (e) => {
    const selectedBenhNhan = e.selectedRowsData[0];
    if (selectedBenhNhan === undefined) return;
    this.setState({
      selectedBenhNhan: {
        ...defaultXuatDuocModel,
        soPhieuTT: selectedBenhNhan.SOPHIEUTT,
        tenBenhNhan: selectedBenhNhan.TEN_BENH_NHAN,
        ngayXuat: selectedBenhNhan.NGAYXUAT_THUOC,
        tuoi: selectedBenhNhan.TUOI,
        namSinh: selectedBenhNhan.NAMSINH,
        thoiGianXuatThuoc: selectedBenhNhan.THOIGIANXUATTHUOC,
        phanTramBaoHiem: selectedBenhNhan.PHAN_TRAM_BAOHIEM,
        ngayHoanTatKham: selectedBenhNhan.HOANTATKHAMBENH,
        chanDoan: selectedBenhNhan.ICD,
        soVaoVien: selectedBenhNhan.SOVAOVIEN,
        maToaThuoc: selectedBenhNhan.MA_TOA_THUOC,
        maKhamBenh: selectedBenhNhan.MA_KHAM_BENH,
        maBenhNhan: selectedBenhNhan.MABENHNHAN,
        vuotCan: selectedBenhNhan.VUOTCAN, // 0: Không vượt cận    1: Vượt cận
        daThanhToan: selectedBenhNhan.DATHANHTOAN, // 0: Chưa thanh toán    1: Đã thanh toán
        doiTuong: null,
        dungTuyen: selectedBenhNhan.DUNG_DUYEN,
        ngayKhamBenh: selectedBenhNhan.NGAY_KB
      },
      isSelect: true
    }, () => {
      this.props.getDanhSachVatTuXuatDuoc(this.state.selectedBenhNhan, 0);
      this.props.getDanhSachVatTuXuatDuoc(this.state.selectedBenhNhan, 1);
      this.props.handleSendDataToForm(this.state.selectedBenhNhan);
      this.props.setEnableButton(true);
    });
  };
  handleOnRowClick = (e) => {
    if (!this.state.isSelect) { // Hủy chọn row đang chọn
      const keys = e.component.getSelectedRowKeys();
      e.component.deselectRows(keys);
      this.props.handleSendDataToForm(defaultXuatDuocModel);
      this.props.setEnableButton(false);
      this.props.resetDanhSachVatTu();
    };
    this.setState({
      isSelect: false
    });
  };
  getSelectedBenhNhanXuatDuoc = () => {
    return this.state.selectedBenhNhan;
  };

  render() {
    return (
      <DataGrid
        loadPanel={configLoadPanel}
        dataSource={this.props.listDanhSachBenhNhanXuatDuoc}
        showBorders={true}
        showRowLines={true}
        showColumnLines={true}
        rowAlternationEnabled={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        columnMinWidth={50}
        onContextMenuPreparing={this.props.handleOnContextMenuGridDanhSach}
        noDataText={'Không có dữ liệu'}
        onRowPrepared={this.handleOnRowRepared}
        onSelectionChanged={this.handleOnSelectBenhNhanXuatDuoc}
        onRowClick={this.handleOnRowClick}
        wordWrapEnabled={true}
        {...this.props}
      >
        <Selection mode="single" />
        <FilterRow visible={true}
                   applyFilter={'auto'} />
        <HeaderFilter visible={true} />
        <Column
          dataField={'STT_HANGNGAY'}
          caption={translate('duoc.duocXuatDuoc.headerDanhSachBenhNhan.stt')}
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="SOPHIEUTT"
          caption={translate('duoc.duocXuatDuoc.headerDanhSachBenhNhan.soPhieu')}
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="TEN_BENH_NHAN"
          caption={translate('duoc.duocXuatDuoc.headerDanhSachBenhNhan.tenBenhNhan')}
          dataType="string"
          alignment="left"
        />
        <Column
          dataField="TRANGTHAI_V1"
          caption={translate('duoc.duocXuatDuoc.headerDanhSachBenhNhan.duocMoi')}
          dataType="boolean"
          alignment="center"
        />
        <Summary>
          <TotalItem
            displayFormat={'{0} phiếu'}
            showInColumn={'SOPHIEUTT'}
            column={'SOPHIEUTT'}
            summaryType="count"
          />
        </Summary>
        <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
        <Paging defaultPageSize={5} />
      </DataGrid>);
  }
}

export default GridDanhSachBenhNhanXuatDuoc;
