export const columnsWidth = {
  cot1: '20%',
  cot2: '5%',
  cot3: '5%',
  cot4: '5%',
  cot5: '5%',
  cot6: '5%',
  cot7: '5%',
  cot8: '5%',
  cot9: '5%',
  cot10: '5%',
  cot11: '5%',
  cot12: '5%',
  cot13: '5%',
  cot14: '5%',
  cot15: '5%',
  cot16: '10%'
};
