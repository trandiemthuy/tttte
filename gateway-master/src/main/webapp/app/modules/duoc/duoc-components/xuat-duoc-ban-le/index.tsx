import React from 'react';
import { Switch } from 'react-router-dom';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import XuatDuocBanLeComponent from "./xuat-duoc-ban-le";

const Routes = ({ match }) => (

  <Switch>
    <ErrorBoundaryRoute
      path={`${match.url}`}
      component={XuatDuocBanLeComponent}>
    </ErrorBoundaryRoute>
  </Switch>
);

export default Routes;
