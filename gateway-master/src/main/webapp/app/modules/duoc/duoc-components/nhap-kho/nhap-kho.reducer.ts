import axios from 'axios';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING } from '../../configs';
import notify from 'devextreme/ui/notify';
import { IPhieuNhapKho, defaultPhieuNhap, IChiTietPhieuNhapKho } from './nhap-kho.model';
import { exportXLSType1, printType1 } from 'app/modules/duoc/duoc-components/utils/print-helper';
import { catchStatusCodeFromResponse } from 'app/modules/duoc/duoc-components/utils/funtions';

export const ACTION_TYPES = {
  GET_DANH_SACH_PHIEU_NHAP: 'NhapKho/GET_DANH_SACH_PHIEU_NHAP',
  GET_DANH_SACH_NGUON_DUOC: 'NhapKho/GET_DANH_SACH_NGUON_DUOC',
  GET_DANH_SACH_NHA_CUNG_CAP: 'NhapKho/GET_DANH_SACH_NHA_CUNG_CAP',
  GET_DANH_SACH_KHO_NHAP: 'NhapKho/GET_DANH_SACH_KHO_NHAP',
  GET_DANH_SACH_NGUOI_NHAN: 'NhapKho/GET_DANH_SACH_NGUOI_NHAN',
  GET_DANH_SACH_HOI_DONG_KIEM_NHAP: 'NhapKho/GET_DANH_SACH_HOI_DONG_KIEM_NHAP',
  GET_DANH_SACH_THAM_SO_DON_VI: 'NhapKho/GET_DANH_SACH_THAM_SO_DON_VI',
  GET_DANH_SACH_VAT_TU: 'NhapKho/GET_DANH_SACH_VAT_TU',
  GET_DANH_SACH_CHI_TIET_HOA_DON: 'NhapKho/GET_DANH_SACH_CHI_TIET_HOA_DON',
  GET_DANH_SACH_CHI_TIET_NHAP_KHO: 'NhapKho/GET_DANH_SACH_CHI_TIET_NHAP_KHO',
  GET_DANH_SACH_NHAN_VIEN_KIEM_NHAP: 'NhapKho/GET_DANH_SACH_NHAN_VIEN_KIEM_NHAP',
  ADD_PHIEU_NHAP_KHO: 'NhapKho/ADD_PHIEU_NHAP_KHO',
  ADD_CHI_TIET_HOA_DON: 'NhapKho/ADD_CHI_TIET_HOA_DON', // Nhập kho Quy cách
  ADD_CHI_TIET_NHAP_KHO: 'NhapKho/ADD_CHI_TIET_NHAP_KHO', // Nhập kho quy cách, nhập kho thường, nhập kho đông y
  UPDATE_PHIEU_NHAP_KHO: 'NhapKho/UPDATE_PHIEU_NHAP_KHO',
  DELETE_PHIEU_NHAP_KHO: 'NhapKho/DELETE_PHIEU_NHAP_KHO',
  ADD_NHAN_VIEN_KIEM_NHAP: 'NhapKho/ADD_NHAN_VIEN_KIEM_NHAP',
  DELETE_NHAN_VIEN_KIEM_NHAP: 'NhapKho/DELETE_NHAN_VIEN_KIEM_NHAP',
  DELETE_CHI_TIET_HOA_DON: 'NhapKho/DELETE_CHI_TIET_HOA_DON',
  DELETE_CHI_TIET_NHAP_KHO: 'NhapKho/DELETE_CHI_TIET_NHAP_KHO',
  DELETE_NHAP_KHO: 'NhapKho/DELETE_NHAP_KHO',
  RESET_PHIEU_NHAP: 'NhapKho/RESET_PHIEU_NHAP',
  RESET_CHI_TIET_HOA_DON: 'NhapKho/RESET_CHI_TIET_HOA_DON',
  RESET_CHI_TIET_NHAP_KHO: 'NhapKho/RESET_CHI_TIET_NHAP_KHO'
};

const initialState = {
  loadingDanhSachPhieuNhapKho: false,
  listDanhSachPhieuNhapKho: [],
  listDanhSachNguonDuoc: [],
  listDanhSachNhaCungCap: [],
  listDanhSachKhoNhap: [],
  listDanhSachNguoiNhan: [],
  listDanhSachHoiDongKiemNhap: [],
  listDanhSachThamSoDonVi: [],
  listDanhSachThamSoDuocMoi: [],
  listDanhSachVatTu: [],
  mapPhieuNhapKho: defaultPhieuNhap,
  listDanhSachChiTietHoaDon: [],
  listDanhSachChiTietNhapKho: [],
  listDanhSachNhanVienKiemNhap: []
};

export type nhapKhoState = Readonly<typeof initialState>;

// Reducer
// eslint-disable-next-line complexity
export default (state: nhapKhoState = initialState, action): nhapKhoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_PHIEU_NHAP):
      return {
        ...state,
        loadingDanhSachPhieuNhapKho: true
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_NGUON_DUOC):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_NHA_CUNG_CAP):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_KHO_NHAP):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_NGUOI_NHAN):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_VAT_TU):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_THAM_SO_DON_VI):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_HOI_DONG_KIEM_NHAP):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.ADD_PHIEU_NHAP_KHO):
    case REQUEST(ACTION_TYPES.UPDATE_PHIEU_NHAP_KHO):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.DELETE_PHIEU_NHAP_KHO):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_CHI_TIET_HOA_DON):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_CHI_TIET_NHAP_KHO):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_NHAN_VIEN_KIEM_NHAP):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.ADD_NHAN_VIEN_KIEM_NHAP):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.DELETE_NHAN_VIEN_KIEM_NHAP):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.DELETE_CHI_TIET_HOA_DON):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_PHIEU_NHAP):
      return {
        ...state,
        loadingDanhSachPhieuNhapKho: false,
        listDanhSachPhieuNhapKho: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_NGUON_DUOC):
      return {
        ...state,
        listDanhSachNguonDuoc: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_NHA_CUNG_CAP):
      return {
        ...state,
        listDanhSachNhaCungCap: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_KHO_NHAP):
      return {
        ...state,
        listDanhSachKhoNhap: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_NGUOI_NHAN):
      return {
        ...state,
        listDanhSachNguoiNhan: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_THAM_SO_DON_VI):
      return {
        ...state,
        listDanhSachThamSoDonVi: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_VAT_TU):
      return {
        ...state,
        listDanhSachVatTu: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_HOI_DONG_KIEM_NHAP):
      return {
        ...state,
        listDanhSachHoiDongKiemNhap: []
      };
    case FAILURE(ACTION_TYPES.ADD_PHIEU_NHAP_KHO):
    case FAILURE(ACTION_TYPES.UPDATE_PHIEU_NHAP_KHO):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.DELETE_PHIEU_NHAP_KHO):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_CHI_TIET_HOA_DON):
      return {
        ...state,
        listDanhSachChiTietHoaDon: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_CHI_TIET_NHAP_KHO):
      return {
        ...state,
        listDanhSachChiTietNhapKho: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_NHAN_VIEN_KIEM_NHAP):
      return {
        ...state,
        listDanhSachChiTietNhapKho: []
      };
    case FAILURE(ACTION_TYPES.ADD_NHAN_VIEN_KIEM_NHAP):
    case FAILURE(ACTION_TYPES.DELETE_NHAN_VIEN_KIEM_NHAP):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.DELETE_CHI_TIET_HOA_DON):
      return {
        ...state
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_PHIEU_NHAP):
      return {
        ...state,
        loadingDanhSachPhieuNhapKho: false,
        listDanhSachPhieuNhapKho: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_NGUON_DUOC):
      return {
        ...state,
        listDanhSachNguonDuoc: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_NHA_CUNG_CAP):
      return {
        ...state,
        listDanhSachNhaCungCap: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_KHO_NHAP):
      return {
        ...state,
        listDanhSachKhoNhap: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_NGUOI_NHAN):
      return {
        ...state,
        listDanhSachNguoiNhan: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_THAM_SO_DON_VI):
      return {
        ...state,
        listDanhSachThamSoDonVi: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_VAT_TU):
      return {
        ...state,
        listDanhSachVatTu: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_HOI_DONG_KIEM_NHAP):
      return {
        ...state,
        listDanhSachHoiDongKiemNhap: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.ADD_PHIEU_NHAP_KHO):
      return {
        ...state
      };
    case SUCCESS(ACTION_TYPES.UPDATE_PHIEU_NHAP_KHO):
      return {
        ...state
      };
    case SUCCESS(ACTION_TYPES.DELETE_PHIEU_NHAP_KHO):
      return {
        ...state
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_CHI_TIET_HOA_DON):
      return {
        ...state,
        listDanhSachChiTietHoaDon: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_CHI_TIET_NHAP_KHO):
      return {
        ...state,
        listDanhSachChiTietNhapKho: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_NHAN_VIEN_KIEM_NHAP):
      return {
        ...state,
        listDanhSachNhanVienKiemNhap: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.ADD_NHAN_VIEN_KIEM_NHAP):
      return {
        ...state
      };
    case SUCCESS(ACTION_TYPES.DELETE_NHAN_VIEN_KIEM_NHAP):
      return {
        ...state
      };
    case SUCCESS(ACTION_TYPES.DELETE_CHI_TIET_HOA_DON):
      return {
        ...state
      };
    case ACTION_TYPES.RESET_PHIEU_NHAP:
      return {
        ...state,
        mapPhieuNhapKho: defaultPhieuNhap
      };
    case ACTION_TYPES.RESET_CHI_TIET_HOA_DON:
      return {
        ...state,
        listDanhSachChiTietHoaDon: []
      };
    case ACTION_TYPES.RESET_CHI_TIET_NHAP_KHO:
      return {
        ...state,
        listDanhSachChiTietNhapKho: []
      };
    default:
      return state;
  }
};
// Actions
export const resetPhieuNhapObj = () => {
  return {
    type: ACTION_TYPES.RESET_PHIEU_NHAP
  };
};
export const resetChiTietHoaDon = () => {
  return {
    type: ACTION_TYPES.RESET_CHI_TIET_HOA_DON
  };
};
export const resetChiTietNhapKho = () => {
  return {
    type: ACTION_TYPES.RESET_CHI_TIET_NHAP_KHO
  };
};
export const getDanhSachPhieuNhapKho = (dvtt, tuNgay, denNgay, maNghiepVu) => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/getDanhSachPhieuNhapKho`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_PHIEU_NHAP,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt,
          tuNgay,
          denNgay,
          maNghiepVu
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách phiếu nhập kho');
      })
  });
  return result;
};

export const getDanhSachNguonDuoc = () => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/getDanhSachNguonDuoc`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_NGUON_DUOC,
    payload: axios.get(requestUrl).catch(obj => {
      catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách nguồn dược');
    })
  });
  return result;
};
export const getDanhSachNhaCungCap = () => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/getDanhSachNhaCungCap`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_NHA_CUNG_CAP,
    payload: axios.get(requestUrl).catch(obj => {
      catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách nhà cung cấp');
    })
  });
  return result;
};
export const getDanhSachKhoNhap = (tenNghiepVu, dvtt) => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/getDanhSachKhoNhap`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_KHO_NHAP,
    payload: axios
      .get(requestUrl, {
        params: {
          tenNghiepVu,
          dvtt
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách kho');
      })
  });
  return result;
};
export const getDanhSachNguoiNhan = (dvtt, tenKhoa) => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/getDanhSachNguoiNhan`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_NGUOI_NHAN,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt,
          tenKhoa
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách người nhận');
      })
  });
  return result;
};
export const getDanhSachHoiDongKiemNhap = dvtt => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/getDanhSachHoiDongKiemNhap`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_HOI_DONG_KIEM_NHAP,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách hội đồng kiểm nhập');
      })
  });
  return result;
};
export const getDanhSachThamSoDonVi = dvtt => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/getDanhSachThamSoDonVi`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_THAM_SO_DON_VI,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách tham số đơn vị');
      })
  });
  return result;
};
export const getDanhSachVatTu = dvtt => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/getDanhSachVatTu`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_VAT_TU,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách vật tư');
      })
  });
  return result;
};
export const addPhieuNhapKho = (
  phieuNhapKho: IPhieuNhapKho,
  dvtt: string,
  tuNgay: string,
  denNgay: string,
  maNghiepVu: number,
  setEnabled: Function,
  mapPhieuNhapKhoObj: Function,
  handleFocusTenVatTu: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/addPhieuNhapKho`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.ADD_PHIEU_NHAP_KHO,
    payload: axios
      .post(requestUrl, JSON.stringify(phieuNhapKho), { headers: { 'content-type': 'application/json' } })
      .then(rs => {
        if (rs.data.SAISOT === 0) {
          notify({ message: 'Thêm phiếu nhập kho thành công', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
          setEnabled(false, false, true, true, false, false);
          mapPhieuNhapKhoObj(rs.data.ID, rs.data.SOLUUTRU, rs.data.SOPHIEUNHAP);
          handleFocusTenVatTu();
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi thêm phiếu nhập kho');
      })
  });
  dispatch(getDanhSachPhieuNhapKho(dvtt, tuNgay, denNgay, maNghiepVu));
  return reuslt;
};
export const updatePhieuNhapKho = (
  phieuNhapKho: IPhieuNhapKho,
  dvtt: string,
  tuNgay: string,
  denNgay: string,
  maNghiepVu: number,
  setDefaultStatus: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/updatePhieuNhapKho`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.UPDATE_PHIEU_NHAP_KHO,
    payload: axios
      .post(requestUrl, JSON.stringify(phieuNhapKho), { headers: { 'content-type': 'application/json' } })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Cập nhật phiếu nhập kho thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
          setDefaultStatus();
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi cập nhật phiếu nhập kho');
      })
  });
  dispatch(getDanhSachPhieuNhapKho(dvtt, tuNgay, denNgay, maNghiepVu));
  return reuslt;
};
export const deletePhieuNhapKho = (
  idPhieuNhap: number,
  nguoiXoa: number,
  dvtt: string,
  tuNgay: string,
  denNgay: string,
  maNghiepVu: number,
  setDefaultStatus: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/deletePhieuNhapKho`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.DELETE_PHIEU_NHAP_KHO,
    payload: axios
      .delete(requestUrl, {
        params: {
          idPhieuNhap,
          dvtt,
          nguoiXoa
        }
      })
      .then(rs => {
        if (rs.data === 0) {
          setDefaultStatus();
          notify({ message: 'Xóa phiếu nhập kho thành công', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
          dispatch(resetChiTietNhapKho());
          dispatch(resetChiTietHoaDon());
        } else if (rs.data === 100) {
          notify({ message: 'Đã chốt số liệu dược. Không thể xóa', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi xóa phiếu nhập kho');
      })
  });
  dispatch(getDanhSachPhieuNhapKho(dvtt, tuNgay, denNgay, maNghiepVu));
  return reuslt;
};

export const getDanhSachChiTietHoaDon = idNhapKho => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/getDanhSachChiTietPhieuNhapKho`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_CHI_TIET_HOA_DON,
    payload: axios
      .get(requestUrl, {
        params: {
          idNhapKho,
          loaiDanhSach: 1
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách chi tiết hóa đơn');
      })
  });
  return result;
};

export const getDanhSachChiTietNhapKho = idNhapKho => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/getDanhSachChiTietPhieuNhapKho`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_CHI_TIET_NHAP_KHO,
    payload: axios
      .get(requestUrl, {
        params: {
          idNhapKho,
          loaiDanhSach: 2
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách chi tiết nhập kho');
      })
  });
  return result;
};

export const getDanhSachNhanVienKiemNhap = (idNhapKho, dvtt) => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/getDanhSachNhanVienKiemNhap`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_NHAN_VIEN_KIEM_NHAP,
    payload: axios
      .get(requestUrl, {
        params: {
          idNhapKho,
          dvtt
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách nhân viên kiểm nhập');
      })
  });
  return result;
};

export const addNhanVienKiemNhap = nhanvien => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/addNhanVienKiemNhap`;
  const result = await dispatch({
    type: ACTION_TYPES.ADD_NHAN_VIEN_KIEM_NHAP,
    payload: axios
      .post(requestUrl, nhanvien, { headers: { 'content-type': 'application/json' } })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Thêm nhân viên kiểm nhập thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else if (rs.data === 1) {
          notify({ message: 'Nhân viên đã tồn tại!', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Thử lại sau!', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi thêm nhân viên kiểm nhập');
      })
  });
  dispatch(getDanhSachNhanVienKiemNhap(nhanvien.idNhapKhoTuNhaCungCap, nhanvien.dvtt));
  return result;
};

export const deleteNhanVienKiemNhap = (idPhieuNhap, dvtt, maNhanVien) => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/deleteNhanVienKiemNhap`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_NHAN_VIEN_KIEM_NHAP,
    payload: axios
      .delete(requestUrl, {
        params: {
          idPhieuNhap,
          dvtt,
          maNhanVien
        }
      })
      .then(rs => {
        if (rs.data === 1) {
          notify({ message: 'Xóa nhân viên kiểm nhập thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Thử lại sau!', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi xóa nhân viên kiểm nhập');
      })
  });
  dispatch(getDanhSachNhanVienKiemNhap(idPhieuNhap, dvtt));
  return result;
};

export const deleteChiTietHoaDon = (idChiTiet, idPhieuNhap, dvtt, nguoiXoa, tuNgay, denNgay, maNghiepVu) => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/deleteChiTietHoaDon`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CHI_TIET_HOA_DON,
    payload: axios
      .delete(requestUrl, {
        params: {
          idChiTiet,
          idPhieuNhap,
          dvtt,
          nguoiXoa
        }
      })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Xóa chi tiết hóa đơn thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else if (rs.data === 100) {
          notify({ message: 'Đã chốt số liệu dược. Không thể xóa', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Thử lại sau!', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi xóa chi tiết hóa đơn');
      })
  });
  dispatch(getDanhSachPhieuNhapKho(dvtt, tuNgay, denNgay, maNghiepVu));
  dispatch(getDanhSachChiTietHoaDon(idPhieuNhap));
  dispatch(getDanhSachChiTietNhapKho(idPhieuNhap));
  return result;
};
export const deleteChiTietNhapKho = (idChiTiet, idPhieuNhap, dvtt, nguoiXoa, tuNgay, denNgay, maNghiepVu) => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/deleteChiTietNhapKho`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CHI_TIET_NHAP_KHO,
    payload: axios
      .delete(requestUrl, {
        params: {
          idChiTiet,
          dvtt,
          nguoiXoa
        }
      })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Xóa chi tiết nhập kho thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else if (rs.data === 100) {
          notify({ message: 'Đã chốt số liệu dược. Không thể xóa', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Thử lại sau!', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi xóa chi tiết nhập kho');
      })
  });
  dispatch(getDanhSachPhieuNhapKho(dvtt, tuNgay, denNgay, maNghiepVu));
  dispatch(getDanhSachChiTietHoaDon(idPhieuNhap));
  dispatch(getDanhSachChiTietNhapKho(idPhieuNhap));
  return result;
};
export const deleteNhapKho = (idPhieuNhap, dvtt, nguoiXoa, maPhong, maKhoa, tuNgay, denNgay, maNghiepVu) => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/deleteNhapKho`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_NHAP_KHO,
    payload: axios
      .delete(requestUrl, {
        params: {
          idPhieuNhap,
          dvtt,
          nguoiXoa,
          maPhong,
          maKhoa
        }
      })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Xóa nhập kho thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else if (rs.data === 100) {
          notify({ message: 'Đã chốt số liệu dược. Không thể xóa', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Thử lại sau!', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi xóa nhập kho');
      })
  });
  dispatch(getDanhSachPhieuNhapKho(dvtt, tuNgay, denNgay, maNghiepVu));
  dispatch(getDanhSachChiTietHoaDon(idPhieuNhap));
  dispatch(getDanhSachChiTietNhapKho(idPhieuNhap));
  return result;
};

export const addChiTietHoaDon = (ctHoaDon: IChiTietPhieuNhapKho) => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/addChiTietHoaDon`;
  const result = await dispatch({
    type: ACTION_TYPES.ADD_CHI_TIET_HOA_DON,
    payload: axios
      .post(requestUrl, ctHoaDon, { headers: { 'content-type': 'application/json' } })
      .then(rs => {
        if (rs.data.SAISOT === 0) {
          notify({ message: 'Thêm chi tiết hóa đơn thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else if (rs.data.SAISOT === 3) {
          notify(
            { message: 'Vật tư đã có trong phiếu! Vui lòng kiểm tra lại', width: NOTIFY_WIDTH, shading: SHADING },
            'error',
            NOTIFY_DISPLAYTIME
          );
        } else if (rs.data.SAISOT === 100) {
          notify({ message: 'Đã chốt số liệu dược. Không thể thêm', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Thử lại sau!', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi thêm chi tiết hóa đơn');
      })
  });
  dispatch(getDanhSachChiTietHoaDon(ctHoaDon.idPhieuNhap));
  dispatch(getDanhSachChiTietNhapKho(ctHoaDon.idPhieuNhap));
  return result;
};

export const addChiTietNhapKho = (listCTHoaDon: IChiTietPhieuNhapKho[], tuNgay, denNgay, maNghiepVu) => async dispatch => {
  if (listCTHoaDon.length <= 0) return;
  const requestUrl = `${apiURL}api/nhapkho/addChiTietNhapKho`;
  const result = await dispatch({
    type: ACTION_TYPES.ADD_CHI_TIET_NHAP_KHO,
    payload: axios
      .post(requestUrl, listCTHoaDon, { headers: { 'content-type': 'application/json' } })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Nhập kho thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else if (rs.data === 100) {
          notify(
            { message: 'Đã chốt số liệu dược! Không thể nhập kho', width: NOTIFY_WIDTH, shading: SHADING },
            'error',
            NOTIFY_DISPLAYTIME
          );
        } else {
          notify({ message: 'Có lỗi xảy ra. Thử lại sau!', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi nhập kho! Vui lòng kiểm tra lại!');
      })
  });
  dispatch(getDanhSachPhieuNhapKho(listCTHoaDon[0].dvtt, tuNgay, denNgay, maNghiepVu));
  dispatch(getDanhSachChiTietHoaDon(listCTHoaDon[0].idPhieuNhap));
  dispatch(getDanhSachChiTietNhapKho(listCTHoaDon[0].idPhieuNhap));
  return result;
};

export const printPhieuNhapKhoHoaDon = (
  idPhieuNhap: number,
  soPhieuNhap: string,
  soHoaDon: string,
  dvtt: string,
  nguoiNhan: string,
  ngayHoaDon: string,
  ngayNhap: string,
  daNhapKho: number,
  loaiNhapKho: number,
  tenBenhVien: string,
  loaiFile: string,
  tenTinh: string,
  maKhoa: string,
  tenKhoa: string,
  nguoiIn: number,
  handleLoading: Function
) => {
  const url = `${apiURL}api/nhapkho/printPhieuNhapKhoHoaDon`;
  const params = {
    idPhieuNhap,
    soPhieuNhap,
    soHoaDon,
    dvtt,
    nguoiNhan,
    ngayHoaDon,
    ngayNhap,
    daNhapKho,
    loaiNhapKho,
    tenBenhVien,
    loaiFile,
    tenTinh,
    maKhoa,
    tenKhoa,
    nguoiIn
  };
  printType1(url, params, {
    successMessage: 'In phiếu nhập kho thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const exportDanhMucVatTuMau = (dvtt: string, handleLoading: Function) => {
  const url = `${apiURL}api/nhapkho/exportDanhMucVatTuMau`;
  const params = {
    dvtt
  };
  exportXLSType1(url, params, {
    successMessage: 'Xuất dữ liệu thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const printBienBanKiemNhap = (
  idPhieuNhap: number,
  soPhieuNhap: string,
  soHoaDon: string,
  dvtt: string,
  ngayNhap: string,
  ngayHoaDon: string,
  maKhoNhan: number,
  hoiDongKiemNhap: number,
  tenBenhVien: string,
  tenTinh: string,
  daNhapKho: number,
  loaiNhapKho: number,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/nhapkho/printBienBanKiemNhap`;
  const params = {
    idPhieuNhap,
    soPhieuNhap,
    soHoaDon,
    dvtt,
    ngayNhap,
    ngayHoaDon,
    maKhoNhan,
    hoiDongKiemNhap,
    tenBenhVien,
    tenTinh,
    daNhapKho,
    loaiNhapKho,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In biên bản kiểm nhập thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const printBienBanKiemNhapNhieuPhieu = (
  dvtt: string,
  soPhieuNhap: string,
  soHoaDon: string,
  maKhoNhan: string,
  idHoiDong: number,
  tenBenhVien: string,
  tenTinh: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/nhapkho/printBienBanKiemNhapNhieuPhieu`;
  const params = {
    dvtt,
    soPhieuNhap,
    soHoaDon,
    maKhoNhan,
    idHoiDong,
    tenBenhVien,
    tenTinh,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In biên bản kiểm nhập thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};
