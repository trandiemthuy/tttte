import { convertToDate } from 'app/modules/duoc/duoc-components/utils/funtions';

export interface IPhieuDuTruModel {
  idPhieu?: number; // id phiếu dự trù tổng / id phiếu hoàn trả tổng
  soLuuTru?: string;
  maPhongBan?: string;
  tenPhongBan?: string;
  ngayLap?: string;
  // maKhoVatTu?: number; //  mã kho chuyển (dự trù) / mã kho nhận (hoàn trả)
  tenKhoVatTu?: string;
  ghiChu?: string;
}
export const defaultPhieuDuTruModel: IPhieuDuTruModel = {
  idPhieu: null, // id phiếu dự trù tổng / id phiếu hoàn trả tổng
  soLuuTru: '',
  maPhongBan: '',
  tenPhongBan: '',
  ngayLap: convertToDate(new Date().toDateString()),
  tenKhoVatTu: '',
  ghiChu: ''
};
