import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection, Pager, Paging
} from 'devextreme-react/data-grid';
import DropDownVatTu
  from "app/modules/duoc/duoc-components/chuyen-kho/components/dropdown-vat-tu/dropdown-vat-tu";
import {NumberBox, TextBox, Validator} from "devextreme-react";
import {Box, Item as BoxItem} from "devextreme-react/box";
import {RequiredRule} from "devextreme-react/form";
import {IChiTietPhieuChuyenKho, defaultChiTietPhieuChuyenKho} from "app/modules/duoc/duoc-components/chuyen-kho/chuyen-kho.model";
import notify from "devextreme/ui/notify";
import {
  configLoadPanel,
  NOTIFY_DISPLAYTIME,
  NOTIFY_WIDTH,
  numberFormat,
  pageSizes,
  SHADING
} from "app/modules/duoc/configs";

export interface IGridDanhSachChiTietPhieuChuyenKhoProp extends IDataGridOptions {
  listDanhSachChiTietPhieuChuyenKho: any;
  listDanhSachVatTu: any;
  // handleReloadDanhSachVatTu: Function;
  addChiTietPhieuChuyenKho: Function;
  enableInput: boolean;
}
export interface IGridDanhSachChiTietPhieuChuyenKhoState {
  chiTietPhieuChuyenObj: IChiTietPhieuChuyenKho;
  isClosed: boolean;
}

// Khung gõ chi tiết chuyển kho
const mdctCot1 = 10;
const mdctCot2 = 10;
const mdctCot3 = 10;
const mdctCot4 = 10;
const mdctCot5 = 10;
const mdctCot6 = 10;
const mdctCot7 = 10;
const mdctCot8 = 10;
const mdctCot9 = 10;
const mdctCot10 = 10;

export class GridDanhSachChiTietPhieuChuyenKho extends React.Component<IGridDanhSachChiTietPhieuChuyenKhoProp, IGridDanhSachChiTietPhieuChuyenKhoState> {
  private refTenVatTu = React.createRef<DropDownVatTu>();
  private refHoatChat = React.createRef<TextBox>();
  private refDVT = React.createRef<TextBox>();
  private refSoThau = React.createRef<TextBox>();
  private refSoLo = React.createRef<TextBox>();
  private refNgayHetThan = React.createRef<TextBox>();
  private refSoLuong = React.createRef<NumberBox>();
  private refDonGia = React.createRef<NumberBox>();
  private refThanhTien = React.createRef<NumberBox>();
  private refGhiChu = React.createRef<TextBox>();
  constructor(props) {
    super(props);
    this.state = {
      chiTietPhieuChuyenObj: defaultChiTietPhieuChuyenKho,
      isClosed: false
    };
  }
  componentDidUpdate(prevProps: Readonly<IGridDanhSachChiTietPhieuChuyenKhoProp>, prevState: Readonly<IGridDanhSachChiTietPhieuChuyenKhoState>, snapshot?: any): void {
    if (this.props.enableInput && !this.state.isClosed) {
      setTimeout(() => {
        this.refTenVatTu.current.handleFocus();
      }, 100);
      this.setState({
        isClosed: true
      });
    }
  }

  handleSelectTenVatTu = () => {
    const selectedVatTuObj = this.refTenVatTu.current.state.selectedVatTuObj;
    this.setState({
      chiTietPhieuChuyenObj: {...this.state.chiTietPhieuChuyenObj,
        maVatTu: selectedVatTuObj.maVatTu,
        hoatChat: selectedVatTuObj.hoatChat,
        dvt: selectedVatTuObj.dvt,
        soLoSanXuat: selectedVatTuObj.soLoSanXuat,
        soThau: selectedVatTuObj.soThau,
        donGia: selectedVatTuObj.donGia,
        soLuong: selectedVatTuObj.soLuong,
        thanhTien: selectedVatTuObj.donGia * selectedVatTuObj.soLuong,
        tenVatTu: selectedVatTuObj.tenVatTu,
        ngayHetHan: selectedVatTuObj.ngayHetHan
      }
    });
    setTimeout(() => {
      this.refSoLuong.current.instance.focus();
    }, 100);
  };
  handleEnterNextInput = (e, currentRef, nextRef) => {
    if (currentRef === this.refGhiChu) {
      this.props.addChiTietPhieuChuyenKho(this.state.chiTietPhieuChuyenObj);
      this.refTenVatTu.current.handleFocus();
    } else {
      nextRef.current.instance.focus();
    }
  };
  handleOnChangeChiTietPhieuChuyen = (e, name) => {
    switch (name) {
      case 'soLuong': {
        const soLuong = parseFloat(e.value);
        const donGia = this.state.chiTietPhieuChuyenObj.donGia;
        if (soLuong === 0) {
          notify({ message: "Số lượng phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.refSoLuong.current.instance.focus();
          return;
        }

        this.setState({
          chiTietPhieuChuyenObj: {
            ...this.state.chiTietPhieuChuyenObj,
            soLuong,
            thanhTien: donGia * soLuong
          }
        });
        break;
      }
      default: {
        this.setState({
          chiTietPhieuChuyenObj: {
            ...this.state.chiTietPhieuChuyenObj,
            [name]: e.value
          }
        });
        break;
      }
    }
  };

  render() {
    return (
        <Box
            direction="col"
            width="100%"
            height={'auto'}
        >
            <BoxItem baseSize={'35%'} ratio={0}>
              <table style={{ width: '100%' }}>
                <thead>
                <tr>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot1 + '%' }}>Tên vật tư</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot2 + '%' }}>Hoạt chất</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot3 + '%' }}>ĐVT</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot4 + '%' }}>Số thầu</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot5 + '%' }}>Số lô</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot6 + '%' }}>Ngày HH</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot7 + '%' }}>Số lượng</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot8 + '%' }}>Đơn giá</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot9 + '%' }}>Thành tiền</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot10 + '%' }}>Ghi chú</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>
                    <DropDownVatTu
                      ref={this.refTenVatTu}
                      className={'no-border-radius'}
                      // handleReloadDanhSachVatTu={this.props.handleReloadDanhSachVatTu}
                      listDanhSachVatTu={this.props.listDanhSachVatTu}
                      dropWidth={screen.width-50}
                      dropHeight={500}
                      handleSelectTenVatTu = {this.handleSelectTenVatTu}
                      disabled={!this.props.enableInput}
                    />
                  </td>
                  <td>
                    <TextBox
                      ref={this.refHoatChat}
                      className={'no-border-radius'}
                      width={'100%'}
                      disabled={true}
                      value={this.state.chiTietPhieuChuyenObj.hoatChat}
                    />
                  </td>
                  <td>
                    <TextBox
                      ref={this.refDVT}
                      className={'no-border-radius'}
                      width={'100%'}
                      disabled={true}
                      value={this.state.chiTietPhieuChuyenObj.dvt}
                    />
                  </td>
                  <td>
                    <TextBox
                      ref={this.refSoThau}
                      className={'no-border-radius'}
                      width={'100%'}
                      disabled={true}
                      value={this.state.chiTietPhieuChuyenObj.soThau}
                    />
                  </td>
                  <td>
                    <TextBox
                      ref={this.refSoLo}
                      className={'no-border-radius'}
                      width={'100%'}
                      disabled={true}
                      value={this.state.chiTietPhieuChuyenObj.soLoSanXuat}
                    />
                  </td>
                  <td>
                    <TextBox
                      ref={this.refNgayHetThan}
                      className={'no-border-radius'}
                      width={'100%'}
                      disabled={true}
                      value={this.state.chiTietPhieuChuyenObj.ngayHetHan}
                    />
                  </td>
                  <td>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      ref={this.refSoLuong}
                      width={'100%'}
                      onEnterKey={e => this.handleEnterNextInput(e, this.refSoLuong, this.refGhiChu)}
                      value={this.state.chiTietPhieuChuyenObj.soLuong}
                      onValueChanged={e => this.handleOnChangeChiTietPhieuChuyen(e, 'soLuong')}
                      disabled={!this.props.enableInput}
                      format={numberFormat}
                    >
                      <Validator>
                        <RequiredRule message="Số lượng không được rỗng" />
                      </Validator>
                    </NumberBox>
                  </td>
                  <td>
                    <NumberBox
                      className={'no-border-radius'}
                      ref={this.refDonGia}
                      width={'100%'}
                      disabled={true}
                      value={this.state.chiTietPhieuChuyenObj.donGia}
                      format={numberFormat}
                    />
                  </td>
                  <td>
                    <NumberBox
                      className={'no-border-radius'}
                      ref={this.refThanhTien}
                      width={'100%'}
                      disabled={true}
                      value={this.state.chiTietPhieuChuyenObj.thanhTien}
                      format={numberFormat}
                    />
                  </td>
                  <td>
                    <TextBox
                      className={'no-border-radius'}
                      ref={this.refGhiChu}
                      width={'100%'}
                      value={this.state.chiTietPhieuChuyenObj.ghiChu}
                      onValueChanged={e => this.handleOnChangeChiTietPhieuChuyen(e, 'ghiChu')}
                      onEnterKey={e => this.handleEnterNextInput(e, this.refGhiChu, this.refTenVatTu)}
                      disabled={!this.props.enableInput}
                    />
                  </td>
                </tr>
                </tbody>
              </table>
            </BoxItem>
            <BoxItem baseSize={'35%'} ratio={0}>
              <DataGrid
                loadPanel={configLoadPanel}
                showColumnHeaders={false}
                dataSource={this.props.listDanhSachChiTietPhieuChuyenKho}
                showBorders={true}
                showColumnLines={true}
                rowAlternationEnabled={true}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnResizingMode={'nextColumn'}
                columnMinWidth={50}
                width={'100%'}
                noDataText={'Không có dữ liệu'}
                wordWrapEnabled={true}
                {...this.props}
              >
                <Selection mode="single" />
                <FilterRow visible={true}
                           applyFilter={'auto'} />
                <HeaderFilter visible={true} />
                <Column
                  dataField="ID_CHUYEN_KHO_VAT_TU_CT"
                  caption="ID chuyển kho vật tư chi tiết"
                  dataType="string"
                  alignment="left"
                  width={'0%'}
                  visible={false}
                />
                <Column
                  dataField="TENVATTU"
                  caption="Tên vật tư"
                  dataType="string"
                  alignment="left"
                  width={'10%'}
                />
                <Column
                  dataField="HOATCHAT"
                  caption="Hoạt chất"
                  dataType="string"
                  alignment="left"
                  width={'10%'}
                />
                <Column
                  dataField="DVT"
                  caption="Đơn vị tính"
                  dataType="string"
                  alignment="left"
                  width={'10%'}
                />
                <Column
                  dataField="MABAOCAO"
                  caption="Số thầu"
                  dataType="string"
                  alignment="left"
                  width={'10%'}
                />
                <Column
                  dataField="SOLOSANXUAT"
                  caption="Số lô"
                  dataType="string"
                  alignment="left"
                  width={'10%'}
                />
                <Column
                  dataField="NGAYHETHAN"
                  caption="Ngày hết hạn"
                  dataType="string"
                  alignment="left"
                  width={'10%'}
                />
                <Column
                  dataField="SO_LUONG"
                  caption="Số lượng"
                  dataType="number"
                  alignment="left"
                  width={'10%'}
                  format={numberFormat}
                />
                <Column
                  dataField="DON_GIA"
                  caption="Đơn giá"
                  dataType="number"
                  alignment="left"
                  width={'10%'}
                  format={numberFormat}
                />
                <Column
                  dataField="THANHTIEN"
                  caption="Thành tiền"
                  dataType="number"
                  alignment="left"
                  width={'10%'}
                  format={numberFormat}
                />
                <Column
                  dataField="GHICHU"
                  caption="Ghi chú"
                  dataType="string"
                  alignment="left"
                  width={'10%'}
                />
                <Summary>
                  <TotalItem
                    customizeText={(data) => {
                      return data.value + " vật tư";
                    }}
                    summaryType="count"
                    column="TENVATTU"
                  />
                  <TotalItem
                    displayFormat={'Tổng: {0}'}
                    column="THANHTIEN"
                    summaryType="sum"
                  />
                </Summary>
                <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
                <Paging defaultPageSize={5} />
              </DataGrid>
            </BoxItem>
        </Box>
      );
  }
}

export default GridDanhSachChiTietPhieuChuyenKho;
