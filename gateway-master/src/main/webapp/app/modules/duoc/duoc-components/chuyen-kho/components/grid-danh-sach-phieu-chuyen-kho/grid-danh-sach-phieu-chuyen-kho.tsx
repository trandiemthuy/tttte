import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection,
  Pager,
  Paging
} from 'devextreme-react/data-grid';
import {configLoadPanel, pageSizes} from "app/modules/duoc/configs";

export interface IGridDanhSachPhieuChuyenKhoProp extends IDataGridOptions {
  listDanhSachPhieuChuyenKho: any;
}
export interface IGridDanhSachPhieuChuyenKhoState {
  listDanhSachPhieuChuyenKho: any;
}

export class GridDanhSachPhieuChuyenKho extends React.Component<IGridDanhSachPhieuChuyenKhoProp, IGridDanhSachPhieuChuyenKhoState> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <DataGrid
        loadPanel={configLoadPanel}
        dataSource={this.props.listDanhSachPhieuChuyenKho}
        showBorders={true}
        showColumnLines={true}
        rowAlternationEnabled={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        columnMinWidth={50}
        noDataText={'Không có dữ liệu'}
        wordWrapEnabled={true}
        {...this.props}
      >
        <Selection mode="single" />
        <FilterRow visible={true}
                   applyFilter={'auto'} />
        <HeaderFilter visible={true} />
        <Column
          dataField="ID_CHUYEN_KHO_VAT_TU"
          caption="ID nhập kho từ nhà cung cấp"
          dataType="string"
          alignment="left"
          width={'0%'}
          visible={false}
        />
        <Column
          dataField="SOPHIEUCHUYEN"
          caption="Số phiếu chuyển"
          dataType="string"
          alignment="left"
          width={'60%'}
        />
        <Column
          dataField="GHICHU"
          caption="Ghi chú"
          dataType="string"
          alignment="left"
          width={'40%'}
        />
        <Summary>
          <TotalItem
            displayFormat={'{0} phiếu'}
            column="SOPHIEUCHUYEN"
            summaryType="count"
          />
        </Summary>
        <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
        <Paging defaultPageSize={5} />
      </DataGrid>);
  }
}

export default GridDanhSachPhieuChuyenKho;
