import axios from 'axios';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING } from '../../configs';
import notify from 'devextreme/ui/notify';
import { IPhieuChuyenKho, defaultPhieuChuyenKho, IChiTietPhieuChuyenKho } from './chuyen-kho.model';
import { printType1 } from 'app/modules/duoc/duoc-components/utils/print-helper';
import {
  getDvtt,
  getMaNhanVien,
  getMaPhongBan,
  getTenBenhVien,
  getTenNhanVien,
  getTenTinh
} from 'app/modules/duoc/duoc-components/utils/thong-tin';
import { defaultToaThuocNgoaiTruModel } from 'app/modules/duoc/duoc-components/kcb/toa-thuoc-ngoai-tru.model';
import { defaultVatTuModel, IVatTuModel } from 'app/modules/duoc/duoc-components/he-thong/vat-tu.model';

export const ACTION_TYPES = {
  GET_DANH_SACH_PHIEU_CHUYEN: 'chuyenKho/GET_DANH_SACH_PHIEU_CHUYEN',
  GET_DANH_SACH_NGHIEP_VU: 'chuyenKho/GET_DANH_SACH_NGHIEP_VU',
  GET_DANH_SACH_KHO_CHUYEN: 'chuyenKho/GET_DANH_SACH_KHO_CHUYEN',
  GET_DANH_SACH_KHO_NHAN: 'chuyenKho/GET_DANH_SACH_KHO_NHAN',
  GET_DON_VI_QUAN_LY: 'chuyenKho/GET_DON_VI_QUAN_LY',
  GET_DANH_SACH_THAM_SO_DON_VI: 'chuyenKho/GET_DANH_SACH_THAM_SO_DON_VI',
  GET_DANH_SACH_VAT_TU: 'chuyenKho/GET_DANH_SACH_VAT_TU',
  GET_DANH_SACH_CHI_TIET_CHUYEN_KHO: 'chuyenKho/GET_DANH_SACH_CHI_TIET_CHUYEN_KHO',
  GET_DANH_SACH_BAN_IN_DU_TRU_KHOA_PHONG: 'chuyenKho/GET_DANH_SACH_BAN_IN_DU_TRU_KHOA_PHONG',
  ADD_PHIEU_CHUYEN_KHO: 'chuyenKho/ADD_PHIEU_CHUYEN_KHO',
  ADD_CHI_TIET_CHUYEN_KHO: 'chuyenKho/ADD_CHI_TIET_CHUYEN_KHO',
  UPDATE_PHIEU_CHUYEN_KHO: 'chuyenKho/UPDATE_PHIEU_CHUYEN_KHO',
  DELETE_PHIEU_CHUYEN_KHO: 'chuyenKho/DELETE_PHIEU_CHUYEN_KHO',
  ADD_NHAN_VIEN_KIEM_NHAP: 'chuyenKho/ADD_NHAN_VIEN_KIEM_NHAP',
  DELETE_NHAN_VIEN_KIEM_NHAP: 'chuyenKho/DELETE_NHAN_VIEN_KIEM_NHAP',
  DELETE_CHI_TIET_HOA_DON: 'chuyenKho/DELETE_CHI_TIET_HOA_DON',
  DELETE_CHI_TIET_CHUYEN_KHO: 'chuyenKho/DELETE_CHI_TIET_CHUYEN_KHO',
  DELETE_CHUYEN_KHO: 'chuyenKho/DELETE_CHUYEN_KHO',
  RESET_PHIEU_CHUYEN: 'chuyenKho/RESET_PHIEU_CHUYEN',
  RESET_CHI_TIET_HOA_DON: 'chuyenKho/RESET_CHI_TIET_HOA_DON',
  RESET_CHI_TIET_CHUYEN_KHO: 'chuyenKho/RESET_CHI_TIET_CHUYEN_KHO'
};

const initialState = {
  loadingDanhSachPhieuchuyenKho: false,
  listDanhSachPhieuchuyenKho: [],
  listDanhSachNghiepVu: [],
  listDanhSachKhoChuyen: [],
  listDanhSachKhoNhan: [],
  donViQuanLy: null,
  listDanhSachThamSoDonVi: [],
  listDanhSachThamSoDuocMoi: [],
  listDanhSachVatTu: [] as Array<IVatTuModel>,
  mapPhieuchuyenKho: defaultPhieuChuyenKho,
  listDanhSachChiTietHoaDon: [],
  listDanhSachChiTietChuyenKho: [],
  listDanhSachNhanVienKiemNhap: [],
  listDanhSachBanInPhieuDuTruKhoaPhong: []
};

export type chuyenKhoState = Readonly<typeof initialState>;

// Reducer
// eslint-disable-next-line complexity
export default (state: chuyenKhoState = initialState, action): chuyenKhoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_PHIEU_CHUYEN):
      return {
        ...state,
        loadingDanhSachPhieuchuyenKho: true
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_NGHIEP_VU):
      return {
        ...state
      };
    case ACTION_TYPES.GET_DANH_SACH_NGHIEP_VU:
      return {
        ...state,
        listDanhSachNghiepVu: action.payload && action.payload.data
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_KHO_CHUYEN):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_KHO_NHAN):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DON_VI_QUAN_LY):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_VAT_TU):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_THAM_SO_DON_VI):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.ADD_PHIEU_CHUYEN_KHO):
    case REQUEST(ACTION_TYPES.UPDATE_PHIEU_CHUYEN_KHO):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.DELETE_PHIEU_CHUYEN_KHO):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_CHI_TIET_CHUYEN_KHO):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.ADD_CHI_TIET_CHUYEN_KHO):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.DELETE_CHI_TIET_HOA_DON):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_BAN_IN_DU_TRU_KHOA_PHONG):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_PHIEU_CHUYEN):
      return {
        ...state,
        loadingDanhSachPhieuchuyenKho: false,
        listDanhSachPhieuchuyenKho: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_NGHIEP_VU):
      return {
        ...state,
        listDanhSachNghiepVu: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_KHO_CHUYEN):
      return {
        ...state,
        listDanhSachKhoChuyen: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_KHO_NHAN):
      return {
        ...state,
        listDanhSachKhoNhan: []
      };
    case FAILURE(ACTION_TYPES.GET_DON_VI_QUAN_LY):
      return {
        ...state,
        donViQuanLy: null
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_THAM_SO_DON_VI):
      return {
        ...state,
        listDanhSachThamSoDonVi: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_VAT_TU):
      return {
        ...state,
        listDanhSachVatTu: []
      };
    case FAILURE(ACTION_TYPES.ADD_PHIEU_CHUYEN_KHO):
    case FAILURE(ACTION_TYPES.UPDATE_PHIEU_CHUYEN_KHO):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.DELETE_PHIEU_CHUYEN_KHO):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.ADD_CHI_TIET_CHUYEN_KHO):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_CHI_TIET_CHUYEN_KHO):
      return {
        ...state,
        listDanhSachChiTietChuyenKho: []
      };
    case FAILURE(ACTION_TYPES.DELETE_CHI_TIET_HOA_DON):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_BAN_IN_DU_TRU_KHOA_PHONG):
      return {
        ...state,
        listDanhSachBanInPhieuDuTruKhoaPhong: []
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_PHIEU_CHUYEN):
      return {
        ...state,
        loadingDanhSachPhieuchuyenKho: false,
        listDanhSachPhieuchuyenKho: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_NGHIEP_VU):
      return {
        ...state,
        listDanhSachNghiepVu: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_KHO_CHUYEN):
      return {
        ...state,
        listDanhSachKhoChuyen: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_KHO_NHAN):
      return {
        ...state,
        listDanhSachKhoNhan: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DON_VI_QUAN_LY):
      return {
        ...state,
        donViQuanLy: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_THAM_SO_DON_VI):
      return {
        ...state,
        listDanhSachThamSoDonVi: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_VAT_TU): {
      const temp = [];
      action.payload.data &&
        action.payload.data.map(vt => {
          temp.push({
            ...defaultVatTuModel,
            maVatTu: vt.MAVATTU,
            tenVatTu: vt.TEN_VAT_TU,
            tenHienThi: vt.TEN_VAT_TU,
            soLuong: vt.SOLUONG,
            donGia: vt.DON_GIA,
            hoatChat: vt.HOAT_CHAT,
            hamLuong: vt.HAM_LUONG,
            dvt: vt.DVT,
            cachSuDung: vt.CACHSUDUNG,
            ngayHetHan: vt.NGAYHETHAN,
            soThau: vt.MABAOCAO,
            quyetDinh: vt.QUYETDINH,
            ghiChu: vt.GHICHU,
            ngoaiDanhMuc: vt.NGOAIDANHMUCBHYT, // 0: không    1: Ngoài danh mục
            soLoSanXuat: vt.SOLOSANXUAT,
            dangThuoc: vt.DANGTHUOC,
            nguonDuoc: vt.TEN_NGUONDUOC,
            maVatTuNhap: vt.MAVATTU_NHAP,
            soDangKy: vt.SODANGKY
          });
        });

      return {
        ...state,
        listDanhSachVatTu: temp
      };
    }
    case SUCCESS(ACTION_TYPES.ADD_PHIEU_CHUYEN_KHO):
      return {
        ...state
      };
    case SUCCESS(ACTION_TYPES.UPDATE_PHIEU_CHUYEN_KHO):
      return {
        ...state
      };
    case SUCCESS(ACTION_TYPES.DELETE_PHIEU_CHUYEN_KHO):
      return {
        ...state
      };
    case SUCCESS(ACTION_TYPES.ADD_CHI_TIET_CHUYEN_KHO):
      return {
        ...state
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_CHI_TIET_CHUYEN_KHO):
      return {
        ...state,
        listDanhSachChiTietChuyenKho: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CHI_TIET_HOA_DON):
      return {
        ...state
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_BAN_IN_DU_TRU_KHOA_PHONG):
      return {
        ...state,
        listDanhSachBanInPhieuDuTruKhoaPhong: action.payload.data
      };
    case ACTION_TYPES.RESET_PHIEU_CHUYEN:
      return {
        ...state,
        mapPhieuchuyenKho: defaultPhieuChuyenKho
      };
    case ACTION_TYPES.RESET_CHI_TIET_HOA_DON:
      return {
        ...state,
        listDanhSachChiTietHoaDon: []
      };
    case ACTION_TYPES.RESET_CHI_TIET_CHUYEN_KHO:
      return {
        ...state,
        listDanhSachChiTietChuyenKho: []
      };
    default:
      return state;
  }
};
// Actions
export const getDanhSachPhieuchuyenKho = (tuNgay, denNgay, dvtt, maNghiepVu, maPhongBan) => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/getDanhSachPhieuChuyenKho`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_PHIEU_CHUYEN,
    payload: axios
      .get(requestUrl, {
        params: {
          tuNgay,
          denNgay,
          dvtt,
          maNghiepVu,
          maPhongBan
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi tải danh sách phiếu chuyển kho', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  return result;
};

export const getDanhSachNghiepVu = (dvtt, maNV, defaultNghiepVu: string) => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/getDanhSachNghiepVuNhanVien`;
  let result;
  switch (defaultNghiepVu) {
    case '34': {
      result = dispatch({
        type: ACTION_TYPES.GET_DANH_SACH_NGHIEP_VU,
        payload: {
          data: [
            {
              CAP_NGHIEPVU: 2,
              MA_NGHIEP_VU: 34,
              MOTA_NGHIEPVU: 'Nội trú tủ thuốc dự trù dược',
              NGHIEP_VU: 'noitru_tuthuocdutru_duoc'
            }
          ]
        }
      });
      break;
    }
    case '35': {
      result = dispatch({
        type: ACTION_TYPES.GET_DANH_SACH_NGHIEP_VU,
        payload: {
          data: [
            {
              CAP_NGHIEPVU: 2,
              MA_NGHIEP_VU: 35,
              MOTA_NGHIEPVU: 'Nội trú tủ thuốc hoàn trả dược',
              NGHIEP_VU: 'noitru_duoc_hoantra'
            }
          ]
        }
      });
      break;
    }
    case '50': {
      result = dispatch({
        type: ACTION_TYPES.GET_DANH_SACH_NGHIEP_VU,
        payload: {
          data: [
            {
              CAP_NGHIEPVU: 3,
              MA_NGHIEP_VU: 50,
              MOTA_NGHIEPVU: 'Hoàn trả dược từ tuyến dưới lên tuyến trên',
              NGHIEP_VU: 'hoantraduoc_tuyenduoi_lentuyentren'
            }
          ]
        }
      });
      break;
    }
    case '37': {
      result = dispatch({
        type: ACTION_TYPES.GET_DANH_SACH_NGHIEP_VU,
        payload: {
          data: [
            {
              CAP_NGHIEPVU: 3,
              MA_NGHIEP_VU: 37,
              MOTA_NGHIEPVU: 'Dự trù dược từ đơn vị tuyến trên',
              NGHIEP_VU: 'dutruduoc_tutuyentren'
            }
          ]
        }
      });
      break;
    }
    default: {
      result = await dispatch({
        type: ACTION_TYPES.GET_DANH_SACH_NGHIEP_VU,
        payload: axios
          .get(requestUrl, {
            params: {
              dvtt,
              maNV
            }
          })
          .catch(obj => {
            notify({ message: 'Lỗi tải danh sách nghiệp vụ', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
          })
      });
    }
  }

  return result;
};

export const getDanhSachKhoChuyen = (dvtt, maNghiepVu) => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/getDanhSachKhoChuyen`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_KHO_CHUYEN,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt,
          maNghiepVu
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi tải danh sách nơi chuyển', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  return result;
};

export const getDanhSachKhoNhan = (dvtt, maNghiepVu, maNV) => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/getDanhSachKhoNhan`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_KHO_NHAN,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt,
          maNghiepVu,
          maNV
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi tải danh sách nơi yêu cầu', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  return result;
};
export const getDonViQuanLy = dvtt => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/getDonViQuanLy`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DON_VI_QUAN_LY,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi tải đơn vị quản lý', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  return result;
};

export const getDanhSachThamSoDonVi = dvtt => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/getDanhSachThamSoDonVi`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_THAM_SO_DON_VI,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi tải danh sách tham số đơn vị', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  return result;
};
export const getDanhSachVatTu = (
  dvtt: string,
  maKho: number,
  ngayChuyen: string,
  maNghiepVu: number,
  focus: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/getDanhSachVatTuTonKho`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_VAT_TU,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt,
          maKho,
          ngayChuyen,
          maNghiepVu
        }
      })
      .then(focus())
      .catch(obj => {
        notify({ message: 'Lỗi tải danh sách vật tư', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  return result;
};
export const addPhieuChuyenKho = (
  phieuChuyenKho: IPhieuChuyenKho,
  dvtt: string,
  tuNgay: string,
  denNgay: string,
  maNghiepVu: number,
  maPhongBan: string,
  maKhoa: string,
  setEnabled: Function,
  mapPhieuChuyenKhoObj: Function,
  handleFocusTenVatTu: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/addPhieuChuyenKho`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.ADD_PHIEU_CHUYEN_KHO,
    payload: axios
      .post(requestUrl, JSON.stringify(phieuChuyenKho), { headers: { 'content-type': 'application/json' }, params: { maPhongBan, maKhoa } })
      .then(rs => {
        if (rs.data.SAISOT === 0) {
          notify({ message: 'Thêm phiếu chuyển kho thành công', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
          setEnabled(false, false, true, true, false, false);
          mapPhieuChuyenKhoObj(rs.data.ID_PHIEU, rs.data.SOLUUTRU, rs.data.SOPHIEUCHUYEN);
          handleFocusTenVatTu();
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi thêm phiếu chuyển kho', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  dispatch(getDanhSachPhieuchuyenKho(tuNgay, denNgay, dvtt, maNghiepVu, maPhongBan));
  return reuslt;
};
export const updatePhieuChuyenKho = (
  phieuChuyenKho: IPhieuChuyenKho,
  dvtt: string,
  tuNgay: string,
  denNgay: string,
  maNghiepVu: number,
  maPhongBan: string,
  maKhoa: string,
  setDefaultStatus: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/updatePhieuChuyenKho`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.UPDATE_PHIEU_CHUYEN_KHO,
    payload: axios
      .post(requestUrl, JSON.stringify(phieuChuyenKho), { headers: { 'content-type': 'application/json' }, params: { maPhongBan, maKhoa } })
      .then(rs => {
        if (rs.data === 0) {
          notify(
            { message: 'Cập nhật phiếu chuyển kho thành công!', width: NOTIFY_WIDTH, shading: SHADING },
            'success',
            NOTIFY_DISPLAYTIME
          );
          setDefaultStatus(0);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi cập nhật phiếu chuyển kho', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  dispatch(getDanhSachPhieuchuyenKho(tuNgay, denNgay, dvtt, maNghiepVu, maPhongBan));
  return reuslt;
};
export const deletePhieuChuyenKho = (
  idPhieuChuyen: number,
  nguoiXoa: number,
  maPhongBan: string,
  maKhoa: string,
  dvtt: string,
  tuNgay: string,
  denNgay: string,
  maNghiepVu: number,
  setDefaultStatus: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/deletePhieuChuyenKho`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.DELETE_PHIEU_CHUYEN_KHO,
    payload: axios
      .delete(requestUrl, {
        params: {
          idPhieuChuyen,
          dvtt,
          nguoiXoa,
          maPhongBan,
          maKhoa
        }
      })
      .then(rs => {
        if (rs.data === 0) {
          setDefaultStatus(0);
          notify({ message: 'Xóa phiếu chuyển kho thành công', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else if (rs.data === 1) {
          notify({ message: 'Dữ liệu đã xác nhận. Không thể xóa', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else if (rs.data === 2) {
          notify({ message: 'Dữ liệu đã xác nhận. Không thể xóa', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi xóa phiếu chuyển kho', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  dispatch(getDanhSachPhieuchuyenKho(tuNgay, denNgay, dvtt, maNghiepVu, maPhongBan));
  return reuslt;
};

export const getDanhSachChiTietChuyenKho = (idPhieuChuyen: number, dvtt: string) => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/getDanhSachChiTietPhieuChuyenKho`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_CHI_TIET_CHUYEN_KHO,
    payload: axios
      .get(requestUrl, {
        params: {
          idPhieuChuyen,
          dvtt
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi tải danh sách chi tiết chuyển kho', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  return result;
};
export const addChiTietPhieuChuyenKho = (
  chiTietChuyenKho: IChiTietPhieuChuyenKho,
  maPhongBan: string,
  maKhoa: string,
  setDefaultStatus: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/addChiTietPhieuChuyenKho`;
  const reuslt = await dispatch({
    type: ACTION_TYPES.ADD_CHI_TIET_CHUYEN_KHO,
    payload: axios
      .post(requestUrl, JSON.stringify(chiTietChuyenKho), {
        headers: { 'content-type': 'application/json' },
        params: { maPhongBan, maKhoa }
      })
      .then(rs => {
        if (rs.data.SAISOT === 0) {
          notify(
            { message: 'Thêm chi tiết phiếu chuyển kho thành công', width: NOTIFY_WIDTH, shading: SHADING },
            'success',
            NOTIFY_DISPLAYTIME
          );
          // setDefaultStatus(1);
        } else if (rs.data.SAISOT === 100) {
          notify({ message: 'Đã chốt số liệu dược. Không thể thêm', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else if (rs.data.SAISOT === 1) {
          notify(
            { message: 'Số lượng vật tư không đủ. Kiểm tra lại!', width: NOTIFY_WIDTH, shading: SHADING },
            'error',
            NOTIFY_DISPLAYTIME
          );
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi thêm chi tiết phiếu chuyển kho', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  dispatch(getDanhSachChiTietChuyenKho(chiTietChuyenKho.idChuyenKhoVT, chiTietChuyenKho.maDonViTao));
  return reuslt;
};

export const deleteChiTietPhieuChuyenKho = (
  idChuyenKhoVTCT: number,
  nguoiXoa: number,
  maPhongBan: string,
  maKhoa: string,
  dvtt: string,
  idPhieuChuyen
) => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/deleteChiTietPhieuChuyenKho`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CHI_TIET_CHUYEN_KHO,
    payload: axios
      .delete(requestUrl, {
        params: {
          idChuyenKhoVTCT,
          dvtt,
          nguoiXoa,
          maPhongBan,
          maKhoa
        }
      })
      .then(rs => {
        if (rs.data !== -1) {
          notify({ message: 'Xóa chi tiết chuyển kho thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Thử lại sau!', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi xóa chi tiết chuyển kho', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  dispatch(getDanhSachChiTietChuyenKho(idPhieuChuyen, dvtt));
  return result;
};
export const printPhieuChuyenKho = (
  dvtt: string,
  idPhieuChuyen: number,
  maNghiepVu: number,
  tenBenhVien: string,
  tenTinh: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/chuyenkho/printPhieuChuyenKho`;
  const params = {
    dvtt,
    idPhieuChuyen,
    maNghiepVu,
    tenBenhVien,
    tenTinh,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In phiếu chuyển kho thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const printPhieuXuatKho = (
  dvtt: string,
  idPhieuChuyen: number,
  maNghiepVu: number,
  tenBenhVien: string,
  tenTinh: string,
  nguoiLap: string,
  loaiFile: string,
  mau: number,
  handleLoading: Function
) => {
  const url = `${apiURL}api/chuyenkho/printPhieuXuatKho`;
  const params = {
    dvtt,
    idPhieuChuyen,
    maNghiepVu,
    tenBenhVien,
    tenTinh,
    nguoiLap,
    loaiFile,
    mau
  };
  printType1(url, params, {
    successMessage: 'In phiếu xuất kho thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const printBienBanThanhLy = (
  dvtt: string,
  idPhieuChuyen: number,
  soPhieuChuyen: string,
  tenBenhVien: string,
  tenTinh: string,
  loaiFile: string,
  mau: number,
  handleLoading: Function
) => {
  const url = `${apiURL}api/chuyenkho/printBienBanThanhLyMatHongVo`;
  const params = {
    dvtt,
    idPhieuChuyen,
    soPhieuChuyen,
    tenBenhVien,
    tenTinh,
    loaiFile,
    mau
  };
  const mess = mau === 0 ? 'thanh lý ' : 'mất/hỏng/vỡ ';
  printType1(url, params, {
    successMessage: 'In biên bản ' + mess + 'thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const getDanhSachBanInPhieuDuTruKhoaPhong = (idPhieuChuyen: number, dvtt: string) => async dispatch => {
  const requestUrl = `${apiURL}api/chuyenkho/getDanhSachBanInPhieuDuTruKhoaPhong`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_BAN_IN_DU_TRU_KHOA_PHONG,
    payload: axios
      .get(requestUrl, {
        params: {
          idPhieuChuyen,
          dvtt
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi tải danh sách bản in', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  return result;
};

export const printPhieuDuTruKhoaPhong = (
  dvtt: string,
  soPhieuChuyen: string,
  idPhieuChuyen: number,
  ngayChuyen: string,
  thanhTien: number,
  maKhoVatTu: number,
  tenKhoVatTu: string,
  maPhongBan: string,
  tenKhoYeuCau: string,
  maLoaiVatTu: string,
  nguoiTao: number,
  tenBenhVien: string,
  tenTinh: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/chuyenkho/printPhieuDuTruKhoaPhong`;
  const params = {
    dvtt,
    soPhieuChuyen,
    idPhieuChuyen,
    ngayChuyen,
    thanhTien,
    maKhoVatTu,
    tenKhoVatTu,
    maPhongBan,
    tenKhoYeuCau,
    maLoaiVatTu,
    nguoiTao,
    tenBenhVien,
    tenTinh,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In phiếu dự trù thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const printPhieuDuTruBoSungKhoaPhong = (
  dvtt: string,
  idPhieuChuyen: number,
  soLuuTru: string,
  ngayChuyen: string,
  ngayPhieuChuyen: string,
  tongTien: number,
  maKhoVatTu: number,
  tenKhoVatTu: string,
  maLoaiVatTu: string,
  tenKhoYC: string,
  maPhongBan: string,
  maNguoiIn: number,
  tenNguoiTao: string,
  tenBenhVien: string,
  tenTinh: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/chuyenkho/printPhieuDuTruBoSungKhoaPhong`;
  const params = {
    dvtt,
    idPhieuChuyen,
    soLuuTru,
    ngayChuyen,
    ngayPhieuChuyen,
    tongTien,
    maKhoVatTu,
    tenKhoVatTu,
    maLoaiVatTu,
    tenKhoYC,
    maPhongBan,
    maNguoiIn,
    tenNguoiTao,
    tenBenhVien,
    tenTinh,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In phiếu dự trù bổ sung thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const printPhieuHoanTraKhoaPhong = (
  dvtt: string,
  idPhieuChuyen: number,
  soPhieuChuyen: string,
  ngayChuyen: string,
  thanhTien: number,
  maKhoVatTu: string,
  tenKhoVatTu: string,
  tenKhoYC: string,
  maLoaiVatTu: string,
  nguoiIn: string,
  nguoiTao: number,
  maPhongBan: string,
  tenBenhVien: string,
  tenTinh: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/chuyenkho/printPhieuHoanTraKhoaPhong`;
  const params = {
    dvtt,
    idPhieuChuyen,
    soPhieuChuyen,
    ngayChuyen,
    thanhTien,
    maKhoVatTu,
    tenKhoVatTu,
    tenKhoYC,
    maLoaiVatTu,
    nguoiIn,
    nguoiTao,
    maPhongBan,
    tenBenhVien,
    tenTinh,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In phiếu hoàn trả thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const printPhieuDuTruDinhMuc = (
  dvtt: string,
  idPhieuChuyen: number,
  maNghiepVu: number,
  tenBenhVien: string,
  tenTinh: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/chuyenkho/printPhieuDuTruDinhMuc`;
  const params = {
    dvtt,
    idPhieuChuyen,
    maNghiepVu,
    tenBenhVien,
    tenTinh,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In phiếu hoàn trả thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const resetPhieuNhapObj = () => {
  return {
    type: ACTION_TYPES.RESET_PHIEU_CHUYEN
  };
};
export const resetChiTietChuyenKho = () => {
  return {
    type: ACTION_TYPES.RESET_CHI_TIET_CHUYEN_KHO
  };
};
