import axios from 'axios';
import notify from 'devextreme/ui/notify';
import { NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING } from 'app/modules/duoc/configs';
export const printType1 = (url: string, params: {}, configs: { successMessage: string; errorMessage: string; handleLoading: Function }) => {
  configs.handleLoading(true);
  axios({
    url,
    params,
    method: 'GET',
    responseType: 'blob'
  })
    .then(response => {
      configs.handleLoading(false);
      const fileName = response.headers['content-disposition'].split('filename=')[1];
      url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', fileName);
      document.body.appendChild(link);
      link.click();
      notify({ message: configs.successMessage, width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
    })
    .catch(obj => {
      configs.handleLoading(false);
      notify({ message: configs.errorMessage, width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
    });
};

export const exportXLSType1 = (
  url: string,
  params: {},
  configs: { successMessage: string; errorMessage: string; handleLoading: Function }
) => {
  axios({
    url,
    params,
    method: 'GET',
    responseType: 'blob'
  })
    .then(response => {
      configs.handleLoading(false);
      const fileName = response.headers['content-disposition'].split('filename=')[1];
      url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', fileName.substr(1, fileName.length - 2));
      document.body.appendChild(link);
      link.click();
      notify({ message: configs.successMessage, width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
    })
    .catch(obj => {
      configs.handleLoading(false);
      notify({ message: configs.errorMessage, width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
    });
};
