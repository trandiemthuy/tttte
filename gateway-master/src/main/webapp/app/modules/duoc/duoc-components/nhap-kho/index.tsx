import React from 'react';
import { Switch } from 'react-router-dom';
import NhapKhoComponent from './nhap-kho';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (

    <Switch>
      <ErrorBoundaryRoute
        path={`${match.url}/:nghiepVu`}
        component={NhapKhoComponent}>
      </ErrorBoundaryRoute>
    </Switch>
);

export default Routes;
