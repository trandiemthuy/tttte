import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection, ColumnFixing, Pager, Paging
} from 'devextreme-react/data-grid';
import {configLoadPanel, pageSizes} from '../../../../configs';

export interface IGridDanhSachBenhNhanDuTruProp extends IDataGridOptions {
  listDanhSachBenhNhanDuTru: any;
  // loaiPhieu: number; // 0: Dự trù nội trú     1: Hoàn trả nội trú
}
export interface IGridDanhSachBenhNhanDuTruState {
  listDanhSachBenhNhanDuTru: any;
}

export class GridDanhSachBenhNhanDuTru extends React.Component<IGridDanhSachBenhNhanDuTruProp, IGridDanhSachBenhNhanDuTruState> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <DataGrid
        loadPanel={configLoadPanel}
        dataSource={this.props.listDanhSachBenhNhanDuTru}
        showBorders={true}
        showRowLines={true}
        showColumnLines={true}
        rowAlternationEnabled={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        columnMinWidth={50}
        noDataText={'Không có dữ liệu'}
        wordWrapEnabled={true}
        {...this.props}
      >
        <Selection mode="single" />
        <FilterRow visible={true}
                   applyFilter={'auto'} />
        <HeaderFilter visible={true} />
        <Column
          dataField={"STT_DIEUTRI"}
          caption="Số PĐT"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField={"TEN_BENH_NHAN"}
          caption="Tên bệnh nhân"
          dataType="string"
          alignment="left"
        />
        <Summary>
          <TotalItem
            displayFormat={'{0} phiếu'}
            showInColumn={'STT_DIEUTRI'}
            column={'STT_DIEUTRI'}
            summaryType="count"
          />
        </Summary>
        <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
        <Paging defaultPageSize={5} />
      </DataGrid>);
  }
}

export default GridDanhSachBenhNhanDuTru;
