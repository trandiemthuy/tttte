import axios from 'axios';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
// import { message } from 'antd';
import { apiURL } from '../../../../configs';

export const ACTION_TYPES = {
  GET_DANH_SACH_PHIEU_NHAP: 'NhapKho/GET_DANH_SACH_PHIEU_NHAP',
  RESET_DANH_SACH_PHIEU_NHAP: 'NhapKho/RESET_PHIEU_NHAP'
};

const initialState = {
  loadingDanhSachPhieuNhapKho: false,
  listDanhSachPhieuNhapKho: []
};

export type gridDanhSachPhieuNhapKhoState = Readonly<typeof initialState>;

// Reducer
export default (state: gridDanhSachPhieuNhapKhoState = initialState, action): gridDanhSachPhieuNhapKhoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_PHIEU_NHAP):
      return {
        ...state,
        loadingDanhSachPhieuNhapKho: true
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_PHIEU_NHAP):
      return {
        ...state,
        loadingDanhSachPhieuNhapKho: false,
        listDanhSachPhieuNhapKho: []
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_PHIEU_NHAP):
      return {
        ...state,
        loadingDanhSachPhieuNhapKho: false,
        listDanhSachPhieuNhapKho: action.payload.data
      };
    case ACTION_TYPES.RESET_DANH_SACH_PHIEU_NHAP:
      return {
        ...initialState
      };
    default:
      return state;
  }
};
// Actions
export const getDanhSachPhieuNhapKho = (dvtt, tuNgay, denNgay, maNghiepVu) => async dispatch => {
  const requestUrl = `${apiURL}api/nhapkho/getDanhSachPhieuNhapKho`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_PHIEU_NHAP,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt,
          tuNgay,
          denNgay,
          maNghiepVu
        }
      })
      .catch(obj => {
        // message.error('Lỗi khi tải danh sách phiếu nhập kho! \nMã lỗi: ' + obj.toString());
      })
  });
  return result;
};
