import React from "react";

export interface IDuocTitleComponentProp {
  className?: string;
  text: string;
}
export interface IDuocTitleComponentState {
  empty: any;
}

export class DuocTitleComponent extends React.Component<IDuocTitleComponentProp, IDuocTitleComponentState> {
  constructor(props) {
    super(props);
  }

  // Handles

  render() {
    return (
      <div className={'duoc-title'}>
        {this.props.text}
      </div>
    );
  }
}

export default DuocTitleComponent;
