export interface ISetEnable {
  btnThem: boolean;
  btnSua: boolean;
  btnLuu: boolean;
  btnHuy: boolean;
  btnXoa: boolean;
  formPhieuChuyen: boolean;
}
export const defaultSetEnable: ISetEnable = {
  btnThem: true,
  btnSua: false,
  btnLuu: false,
  btnHuy: false,
  btnXoa: false,
  formPhieuChuyen: true
};
