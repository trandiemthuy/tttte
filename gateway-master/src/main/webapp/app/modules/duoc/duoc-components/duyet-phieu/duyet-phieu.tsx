import React from 'react';
import {RouteComponentProps} from "react-router";
import {IRootState} from "app/shared/reducers";
import {connect} from "react-redux";
import { Button, SelectBox, CheckBox } from 'devextreme-react';
import Box, {Item as BoxItem} from "devextreme-react/box";
import notify from "devextreme/ui/notify";
import {exportXLSType1} from '../utils/print-helper';
import {
  getDanhSachKhoChuyenDuyet,
  getDanhSachPhieuChuyenKho,
  duyetPhieuChuyenKho,
  huyDuyetPhieuChuyenKho,
  printPhieuXuatKhoDuyet,
  exportExcelDuyetPhieu
} from './duyet-phieu.reducer';
import {
  getDanhSachBanInPhieuDuTruKhoaPhong,
  getDanhSachChiTietChuyenKho,
  printPhieuDuTruKhoaPhong,
  printPhieuHoanTraKhoaPhong
} from '../chuyen-kho/chuyen-kho.reducer'
import GridDanhSachPhieuChuyenKhoDuyet from './components/grid-danh-sach-phieu-chuyen-kho-duyet/grid-danh-sach-phieu-chuyen-kho-duyet'

import {
  convertToDate
} from "app/modules/duoc/duoc-components/utils/funtions";
import {apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING} from '../../configs';
import PopUpChiTietPhieuDuyet
  from "app/modules/duoc/duoc-components/duyet-phieu/components/popup-chi-tiet-phieu/popup-chi-tiet-phieu";
import {
  getDvtt, getMaNhanVien, getMaPhongBan, getMaPhongBenh,
  getTenBenhVien,
  getTenNhanVien,
  getTenTinh
} from "app/modules/duoc/duoc-components/utils/thong-tin";
import PopUpDanhSachBanInChuyenKho
  from "app/modules/duoc/duoc-components/chuyen-kho/components/popup-danh-sach-ban-in/popup-danh-sach-ban-in";
import TuNgayDenNgayNgangComponent
  from "app/modules/duoc/duoc-components/utils/tungay-denngay/tungay-denngay-ngang";
import DuocLabelComponent from "app/modules/duoc/duoc-components/utils/duoc-label/duoc-label";

export interface IDuyetPhieuProp extends StateProps, DispatchProps, RouteComponentProps<{ nghiepVu: string }> {
  account: any;
}

export interface IDuyetPhieuState {
  khoChuyen: {
    maKhoVaTTu: number,
    tenKhoVatTu: string
  }
  duyet: number; // 0 : Chưa duyệt    1: Đã duyệt     2: Đã nhận
  selectedPhieuChuyen: any;
  chiTietPopUp: {
    visible: boolean;
  }
  showPopUp: boolean;
  printPopUp: {
    loaiIn: number; // 0: In phiếu dự trù   1: In phiếu bổ sung    2:
    visible: boolean;
    loaiFile: string;
  }
  loadingConfigs: {
    visible: boolean;
    position: string;
  }
}

export class DuyetPhieuComponent extends React.Component<IDuyetPhieuProp, IDuyetPhieuState> {
  private refTuNgayDenNgay = React.createRef<TuNgayDenNgayNgangComponent>();
  constructor(props) {
    super(props);
    this.state = {
      khoChuyen: {
        maKhoVaTTu: null,
        tenKhoVatTu: ''
      },
      duyet: 0,
      selectedPhieuChuyen: {},
      chiTietPopUp: {
        visible: false
      },
      showPopUp: false,
      printPopUp: {
        loaiIn: 0,
        visible: false,
        loaiFile: 'pdf'
      },
      loadingConfigs: {
        visible: false,
        position: ''
      }
    }
  }

  componentDidMount(): void {
    this.getDanhSachKhoChuyen(getDvtt(), getMaNhanVien());
  }

  componentDidUpdate(prevProps: Readonly<IDuyetPhieuProp>, prevState: Readonly<IDuyetPhieuState>, snapshot?: any): void {
    if (prevProps.match.params.nghiepVu !== this.props.match.params.nghiepVu) {
      this.getDanhSachKhoChuyen(getDvtt(), getMaNhanVien());
      this.handleOnClickButtonLamMoi();
    }
  }

  // Handles
  handleOnChangeKhoChuyen = (e) => {
    this.setState({
      khoChuyen: {
        maKhoVaTTu: parseInt(e.selectedItem.MAKHOVATTU, 0),
        tenKhoVatTu: e.selectedItem.TENKHOVATTU
      }
    });
    this.handleOnClickButtonLamMoi();
  };
  handleOnChangeTrangThai = (e, name) => {
    if (name === 'daDuyet' && e.value) {
      this.setState({
        duyet: 1
      });
    } else if (name === 'daNhan' && e.value) {
      this.setState({
        duyet: 2
      });
    } else {
      this.setState({
        duyet: 0
      });
    }
    this.handleOnClickButtonLamMoi();
  };
  handleOnClickButtonLamMoi = () => {
    if (this.state.khoChuyen.maKhoVaTTu === null) {
      notify({ message: "Chưa chọn kho chuyển", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    this.getDanhSachPhieuChuyenKho(
      getDvtt(),
      convertToDate(this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay),
      convertToDate(this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay),
      !['34', '35', '37', '50'].includes(this.props.match.params.nghiepVu) ? 2 : parseInt(this.props.match.params.nghiepVu,0),
      this.state.khoChuyen.maKhoVaTTu,
      this.state.duyet
    );
  };
  handleOnSelectedChangeGridDanhSach = (e) => {
    this.setState({
      selectedPhieuChuyen: e.selectedRowsData
    });
  };
  handleOnContextMenuGridDanhSach = (e) => {
    if (e.row.data === undefined) {
      return;
    }
    this.setState({
      selectedPhieuChuyen: e.row.data
    });
    if (e.row.rowType === 'data') {
      e.items = [{
        icon: 'bulletlist',
        text: "Xem chi tiết phiếu chuyển kho",
        onItemClick: () => {
          this.handleOnClickMenuXemChiTiet(this.state.selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU, getDvtt());
        }
      },
        {
          icon: 'print',
          text: "In phiếu xuất chuyển kho",
          items: [
            {
              icon: 'exportpdf',
              text: 'In phiếu xuất chuyển kho (PDF)',
              onItemClick: () => {
                this.handleOnClickButtonInPhieuPopUp('pdf', 0);
              }
            },
            {
              icon: 'exportxlsx',
              text: 'In phiếu xuất chuyển kho (XLS)',
              onItemClick: () => {
                this.handleOnClickButtonInPhieuPopUp('xls', 0);
              }
            },
            {
              icon: 'docfile',
              text: 'In phiếu xuất chuyển kho (RTF)',
              onItemClick: () => {
                this.handleOnClickButtonInPhieuPopUp('rtf', 0);
              }
            }
          ]
        },
        {
          icon: 'exportxlsx',
          text: "Xuất Excel",
          onItemClick: () => {
            this.handleOnClickButtonExportExcelPopUp();
          }
        }];
      if (this.props.match.params.nghiepVu === '34') {
        e.items.push({
            icon: 'print',
            text: "In phiếu dự trù",
            items: [
              {
                icon: 'exportpdf',
                text: 'In phiếu dự trù (PDF)',
                onItemClick: () => {
                  this.handleChangePopUpPrintState(0,true, 'pdf');
                }
              },
              {
                icon: 'exportxlsx',
                text: 'In phiếu dự trù (XLS)',
                onItemClick: () => {
                  this.handleChangePopUpPrintState(0,true, 'xls');
                }
              },
              {
                icon: 'docfile',
                text: 'In phiếu dự trù (RTF)',
                onItemClick: () => {
                  this.handleChangePopUpPrintState(0,true, 'rtf');
                }
              },
            ]
        },
          {
            icon: 'print',
            text: "In phiếu xuất đứng",
            onItemClick: () => {
              this.handleOnClickButtonInPhieuPopUp('pdf', 0);
            }
          },
          {
            icon: 'print',
            text: "In phiếu xuất ngang",
            onItemClick: () => {
              this.handleOnClickButtonInPhieuPopUp('pdf', 1);
            }
          }
          );
      }
      if (this.props.match.params.nghiepVu === '35') {
        e.items.push({
          icon: 'print',
          text: "In phiếu hoàn trả",
          items: [
            {
              icon: 'exportpdf',
              text: 'In phiếu hoàn trả (PDF)',
              onItemClick: () => {
                this.handleChangePopUpPrintState(2,true, 'pdf');
              }
            },
            {
              icon: 'exportxlsx',
              text: 'In phiếu hoàn trả (XLS)',
              onItemClick: () => {
                this.handleChangePopUpPrintState(2,true, 'xls');
              }
            },
            {
              icon: 'docfile',
              text: 'In phiếu hoàn trả (RTF)',
              onItemClick: () => {
                this.handleChangePopUpPrintState(2,true, 'rtf');
              }
            },
          ]
          }
        );
      }
    }
  };
  handleOnClickMenuXemChiTiet = (idPhieuChuyen: number, dvtt: string) => {
    this.props.getDanhSachChiTietChuyenKho(idPhieuChuyen, dvtt);
    this.handleChangePopUpChiTietState(true);
  };
  handleChangePopUpChiTietState = (showPopUp: boolean) => {
    this.setState({
      showPopUp
    });
  };
  handleChangePopUpPrintState = (loaiIn: number, visible: boolean, loaiFile: string) => {
    const selectedPhieuChuyen = this.state.selectedPhieuChuyen;
    this.props.getDanhSachBanInPhieuDuTruKhoaPhong(selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU, getDvtt());
    this.setState({
      printPopUp: {
        loaiIn,
        visible,
        loaiFile
      }
    });
  };
  handleLoading = (visible: boolean) => {
    this.setState({
      loadingConfigs: {
        ...this.state.loadingConfigs,
        visible
      }
    });
  };
  handleOnClickButtonInPhieuPopUp = (loaiFile: string, mau: number) => {
    const selectedPhieuChuyen = this.state.selectedPhieuChuyen;
    const params = {
      dvtt: getDvtt(),
      idPhieuChuyen: selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU,
      maNghiepVu: selectedPhieuChuyen.MA_NGHIEP_VU_CHUYEN,
      tenBenhVien: getTenBenhVien(),
      tenTinh: getTenTinh(),
      nguoiLap: getTenNhanVien(),
      loaiFile,
      mau // 0: Đứng      1: Ngang
    };
    this.props.printPhieuXuatKhoDuyet(
      getDvtt(),
      selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU,
      selectedPhieuChuyen.MA_NGHIEP_VU_CHUYEN,
      getTenBenhVien(),
      getTenTinh(),
      getTenNhanVien(),
      mau, // 0: Đứng      1: Ngang
      loaiFile,
      this.handleLoading
    );
  };
  handleOnSelectRowGridDanhSachBanIn = (e: any) => { // 0: In phiếu dự trù    1: In phiếu bổ sung     2: In phiếu hoàn trả
    const selectedPhieuChuyen = this.state.selectedPhieuChuyen;
    const selectedRow = e;
    if (this.state.printPopUp.loaiIn === 0) {
      this.props.printPhieuDuTruKhoaPhong(
        getDvtt(),
        selectedPhieuChuyen.SOPHIEUCHUYEN,
        selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU,
        selectedRow.NGAY_CHUYEN,
        selectedRow.THANH_TIEN,
        selectedRow.MAKHOVATTU,
        selectedRow.TENKHOVATTU,
        getMaPhongBan(),
        selectedRow.TEN_PHONGBAN,
        selectedRow.MALOAIVATTU,
        getMaNhanVien(),
        getTenBenhVien(),
        getTenTinh(),
        this.state.printPopUp.loaiFile,
        this.handleLoading
      );
    } else if (this.state.printPopUp.loaiIn === 2) { // In phiếu hoàn trả khoa phòng
      this.props.printPhieuHoanTraKhoaPhong(
        getDvtt(),
        selectedPhieuChuyen.SOPHIEUCHUYEN,
        selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU,
        selectedRow.NGAY_CHUYEN,
        selectedRow.THANH_TIEN,
        selectedRow.MAKHOVATTU,
        selectedRow.TENKHOVATTU,
        selectedPhieuChuyen.MA_KHO_NHAN,
        selectedRow.TEN_PHONGBAN,
        selectedRow.MALOAIVATTU,
        getMaNhanVien(),
        getTenNhanVien(),
        getTenBenhVien(),
        getTenTinh(),
        this.state.printPopUp.loaiFile,
        this.handleLoading
      );
    }
  };
  handleOnClickButtonExportExcel = () => {
    this.props.exportExcelDuyetPhieu(
      getDvtt(),
      convertToDate(this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay),
      convertToDate(this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay),
      this.state.khoChuyen.maKhoVaTTu,
      this.state.duyet,
      this.handleLoading
    );
  };
  handleOnClickButtonExportExcelPopUp = () => {
    const url = `${apiURL}api/duyetphieu/exportExcelChiTietPhieuChuyenKho`;
    const selectedPhieuChuyen = this.state.selectedPhieuChuyen;
    const params = {
      dvtt: getDvtt(),
      idPhieuChuyen: selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU
    };
    exportXLSType1(url, params, {
      successMessage: 'Xuất dữ liệu thành công',
      errorMessage: 'Có lỗi xảy ra. Thử lại sau',
      handleLoading: this.handleLoading
    });
  };

  // Funtion
  changeTitleByNghiepVu = () => {
    const nghiepVu = this.props.match.params.nghiepVu;
    const formPhieuChuyenObj = {
      titleForm: '',
      titleNoiNhan: '',
      dataFieldNoiNhan: '',
      titleNoiChuyen: '',
      dataFieldNoiChuyen: ''
    };
    switch (nghiepVu) {
      case '37': {
        formPhieuChuyenObj.titleForm = "Dự trù tuyến trên";
        formPhieuChuyenObj.titleNoiNhan = "Nơi yêu cầu";
        formPhieuChuyenObj.titleNoiChuyen = "Nơi chuyển";
        formPhieuChuyenObj.dataFieldNoiChuyen = "maKhoGiao";
        formPhieuChuyenObj.dataFieldNoiNhan = "maKhoNhan";
        break;
      }
      case '50': {
        formPhieuChuyenObj.titleForm = "Hoàn trả tuyến trên";
        formPhieuChuyenObj.titleNoiNhan = "Nơi hoàn trả";
        formPhieuChuyenObj.titleNoiChuyen = "Nơi nhận";
        formPhieuChuyenObj.dataFieldNoiChuyen = "maKhoNhan";
        formPhieuChuyenObj.dataFieldNoiNhan = "maKhoGiao";
        break;
      }
      case '35': {
        formPhieuChuyenObj.titleForm = "Hoàn trả khoa phòng";
        formPhieuChuyenObj.titleNoiNhan = "Nơi hoàn trả";
        formPhieuChuyenObj.titleNoiChuyen = "Nơi nhận";
        formPhieuChuyenObj.dataFieldNoiChuyen = "maKhoNhan";
        formPhieuChuyenObj.dataFieldNoiNhan = "maKhoGiao";
        break;
      }
      case '34': {
        formPhieuChuyenObj.titleForm = "Dự trù khoa phòng";
        formPhieuChuyenObj.titleNoiNhan = "Nơi yêu cầu";
        formPhieuChuyenObj.titleNoiChuyen = "Nơi chuyển";
        formPhieuChuyenObj.dataFieldNoiChuyen = "maKhoGiao";
        formPhieuChuyenObj.dataFieldNoiNhan = "maKhoNhan";
        break;
      }
      case '55': {
        formPhieuChuyenObj.titleForm = "Chuyển hủy sử dụng vật tư tiêu hao khoa phòng, CLS";
        formPhieuChuyenObj.titleNoiNhan = "Nơi yêu cầu";
        formPhieuChuyenObj.titleNoiChuyen = "Nơi chuyển";
        formPhieuChuyenObj.dataFieldNoiChuyen = "maKhoGiao";
        formPhieuChuyenObj.dataFieldNoiNhan = "maKhoNhan";
        break;
      }
      default: {
        formPhieuChuyenObj.titleForm = "Chuyển kho";
        formPhieuChuyenObj.titleNoiNhan = "Nơi yêu cầu";
        formPhieuChuyenObj.titleNoiChuyen = "Nơi chuyển";
        formPhieuChuyenObj.dataFieldNoiChuyen = "maKhoGiao";
        formPhieuChuyenObj.dataFieldNoiNhan = "maKhoNhan";
      }
    }
    return formPhieuChuyenObj;
  };
  getDanhSachKhoChuyen = (dvtt: string, maNhanVien: number) => {
    this.props.getDanhSachKhoChuyenDuyet(dvtt, maNhanVien);
  };
  getDanhSachPhieuChuyenKho = (dvtt: string, tuNgay: string, denNgay: string, nghiepVu: number, maKhoVatTu: number, duyet: number) => {
    this.props.getDanhSachPhieuChuyenKho(dvtt, tuNgay, denNgay, nghiepVu, maKhoVatTu, duyet);
  };
  duyetPhieuChuyenKho = (listDanhSachChiTietChinhSua: any, ngayDuyet: string) => {
    const mapPhieuChuyenKho = [];
    listDanhSachChiTietChinhSua.map((ct, idx) => {
      const object = {
        idPhieuChuyen: ct.ID_CHUYEN_KHO_VAT_TU,
        ngayDuyet,
        idChuyenKhoChiTiet: ct.ID_CHUYEN_KHO_VAT_TU_CT,
        soLuongYeuCau: ct.SOLUONG,
        soLuongDuyet: ct.SOLUONG_DUYET
      };
      mapPhieuChuyenKho.push(object);
    });

    this.props.duyetPhieuChuyenKho(mapPhieuChuyenKho,
      getDvtt(),
      getMaNhanVien(),
      getMaPhongBenh(),
      getMaPhongBan(),
      convertToDate(this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay),
      convertToDate(this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay),
      this.state.khoChuyen.maKhoVaTTu,
      parseInt(this.props.match.params.nghiepVu, 0),
      this.state.duyet);
  };
  huyDuyetPhieuChuyenKho = () => {
    this.props.huyDuyetPhieuChuyenKho(this.state.selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU,
      getDvtt(),
      getMaNhanVien(),
      getMaPhongBenh(),
      getMaPhongBan(),
      convertToDate(this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay),
      convertToDate(this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay),
      this.state.khoChuyen.maKhoVaTTu,
      parseInt(this.props.match.params.nghiepVu, 0),
      this.state.duyet
      );
  };

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (<div>
      <div className={'duoc-title'}>
        {
          'Duyệt nghiệp vụ ' + this.changeTitleByNghiepVu().titleForm.toLowerCase()
        }
      </div>
      <Box
        direction="col"
        width="100%"
        height={'auto'}
      >
        <BoxItem baseSize={'100%'} ratio={0}>
        </BoxItem>
        <BoxItem ratio={0} baseSize={'100%'}>

        </BoxItem>
        <BoxItem baseSize={'100%'} ratio={0}>
          <div className={'div-box-wrapper'}>
          <TuNgayDenNgayNgangComponent
            ref={this.refTuNgayDenNgay}
            handleOnClickButtonLamMoi={this.handleOnClickButtonLamMoi}
            labelTuNgayWidth={'10%'}
            dateboxTuNgayWidth={'20%'}
            labelDenNgayWidth={'10%'}
            dateboxDenNgayWidth={'20%'}
            renderRight={() =>
              <Button
                icon={'exportxlsx'}
                text={'Xuất Excel'}
                width={150}
                type={'default'}
                className={'col-margin-left-5'}
                onClick={this.handleOnClickButtonExportExcel}
              />
            }
          />
          <Box direction="row" width="100%" height={'auto'} className={'row-padding-top-5'}>
            <BoxItem baseSize={'10%'} ratio={0}>
              <div className={'col-align-right'}>
                Kho chuyển
              </div>
            </BoxItem>
            <BoxItem baseSize={'20%'} ratio={0}>
              <SelectBox
                dataSource={this.props.listDanhSachKhoChuyenDuyet}
                valueExpr={'MAKHOVATTU'}
                displayExpr={'TENKHOVATTU'}
                placeholder={'Chọn kho chuyển...'}
                onSelectionChanged={this.handleOnChangeKhoChuyen}
              />
            </BoxItem>
            <BoxItem baseSize={'15%'} ratio={0}>
              <div className={'col-align-right'}>
                <CheckBox
                  text={'Đã duyệt'}
                  onValueChanged={e => this.handleOnChangeTrangThai(e, 'daDuyet')}
                  value={this.state.duyet === 1}
                />
              </div>
            </BoxItem>
            <BoxItem baseSize={'15%'} ratio={0}>
              <div className={'col-align-right'}>
                <CheckBox
                  text={'Đã nhận về kho'}
                  onValueChanged={e => this.handleOnChangeTrangThai(e, 'daNhan')}
                  value={this.state.duyet === 2}
                />
              </div>
            </BoxItem>
          </Box>
          </div>
        </BoxItem>
        <BoxItem baseSize={'100%'} ratio={0}>
          <div className={'div-box-wrapper row-margin-top-5'}>
            <DuocLabelComponent icon={'bookmark'} text={'Danh sách phiếu chuyển kho'}/>
            <GridDanhSachPhieuChuyenKhoDuyet
              listDanhSachPhieuChuyenKhoDuyet={this.props.listDanhSachPhieuChuyenKhoDuyet}
              handleOnContextMenuGridDanhSach={this.handleOnContextMenuGridDanhSach}
              // keyExpr={'ID_CHUYEN_KHO_VAT_TU'}
              selectedRowKeys={[this.state.selectedPhieuChuyen]}
              // onSelectionChanged={this.handleOnSelectedChangeGridDanhSach}
              // isNhan={false}
              loaiHienThi={0}
            />
          </div>
        </BoxItem>
        <BoxItem ratio={0} baseSize={'100%'}>
          <PopUpChiTietPhieuDuyet
            listDanhSachChiTietPhieuDuyet = {this.props.listDanhSachChiTietChuyenKho}
            handleChangePopUpChiTietState={this.handleChangePopUpChiTietState}
            showPopUp={this.state.showPopUp}
            title={this.changeTitleByNghiepVu().titleForm}
            loaiDuyet={this.state.duyet}
            selectedPhieuChuyen={this.state.selectedPhieuChuyen}
            loadingConfigs={this.state.loadingConfigs}
            handleOnClickButtonInPhieuPopUp={this.handleOnClickButtonInPhieuPopUp}
            duyetPhieuChuyenKho={this.duyetPhieuChuyenKho}
            huyDuyetPhieuChuyenKho={this.huyDuyetPhieuChuyenKho}
          />
        </BoxItem>
        <BoxItem ratio={0} baseSize={'100%'}>
          <PopUpDanhSachBanInChuyenKho
            listDanhSachBanIn={this.props.listDanhSachBanInPhieuDuTruKhoaPhong}
            handleChangePopUpState={this.handleChangePopUpPrintState}
            handleOnSelectRowGridDanhSachBanIn={this.handleOnSelectRowGridDanhSachBanIn}
            printPopUp={this.state.printPopUp}
          />
        </BoxItem>
      </Box>
    </div>);
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  listDanhSachKhoChuyenDuyet: storeState.duyetphieu.listDanhSachKhoChuyenDuyet,
  listDanhSachPhieuChuyenKhoDuyet: storeState.duyetphieu.listDanhSachPhieuChuyenKhoDuyet,
  listDanhSachChiTietChuyenKho: storeState.chuyenkho.listDanhSachChiTietChuyenKho,
  listDanhSachBanInPhieuDuTruKhoaPhong: storeState.chuyenkho.listDanhSachBanInPhieuDuTruKhoaPhong
});

const mapDispatchToProps = {
  // Duyệt
  getDanhSachKhoChuyenDuyet,
  getDanhSachPhieuChuyenKho,
  duyetPhieuChuyenKho,
  huyDuyetPhieuChuyenKho,
  printPhieuXuatKhoDuyet,
  exportExcelDuyetPhieu,

  // Chuyển kho
  getDanhSachChiTietChuyenKho,
  getDanhSachBanInPhieuDuTruKhoaPhong,
  printPhieuDuTruKhoaPhong,
  printPhieuHoanTraKhoaPhong
};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DuyetPhieuComponent);
