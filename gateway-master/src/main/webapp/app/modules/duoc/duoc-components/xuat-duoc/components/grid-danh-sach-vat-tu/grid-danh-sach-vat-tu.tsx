import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection, ColumnFixing, Pager, Paging
} from 'devextreme-react/data-grid';
import {configLoadPanel, numberFormat, pageSizes} from '../../../../configs';
import { IXuatDuocModel, defaultXuatDuocModel } from '../../xuat-duoc.model'
import {getDvtt} from '../../../utils/thong-tin';
import {translate} from "react-jhipster";

export interface IGridDanhSachVatTuXuatDuocProp extends IDataGridOptions {
  listDanhSachVatTu: any;
}
export interface IGridDanhSachVatTuXuatDuocState {
  litsDanhSachVatTu: any;
}

export class GridDanhSachVatTuXuatDuoc extends React.Component<IGridDanhSachVatTuXuatDuocProp, IGridDanhSachVatTuXuatDuocState> {
  constructor(props) {
    super(props);
  }

  // Handles

  render() {
    return (
      <DataGrid
        loadPanel={configLoadPanel}
        dataSource={this.props.listDanhSachVatTu}
        showBorders={true}
        showRowLines={true}
        showColumnLines={true}
        rowAlternationEnabled={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        columnMinWidth={50}
        noDataText={'Không có dữ liệu'}
        wordWrapEnabled={true}
        {...this.props}
      >
        <Selection mode="single" />
        <FilterRow visible={true}
                   applyFilter={'auto'} />
        <HeaderFilter visible={true} />
        <Column
          dataField={'TEN_VAT_TU'}
          caption={translate('duoc.duocXuatDuoc.headerDanhSachVatTuXuatThuoc.tenThuoc')}
          dataType="string"
          alignment="left"
        />
        <Column
          dataField={'HOAT_CHAT'}
          caption={translate('duoc.duocXuatDuoc.headerDanhSachVatTuXuatThuoc.tenHoatChat')}
          dataType="string"
          alignment="left"
        />
        <Column
          dataField={'DVT'}
          caption={translate('duoc.duocXuatDuoc.headerDanhSachVatTuXuatThuoc.donViTinh')}
          dataType="string"
          alignment="left"
        />
        <Column
          dataField={'SO_LUONG'}
          caption={translate('duoc.duocXuatDuoc.headerDanhSachVatTuXuatThuoc.soLuong')}
          dataType="number"
          alignment="left"
          format={numberFormat}
        />
        <Column
          dataField={'DONGIA_BAN_BV'}
          caption={translate('duoc.duocXuatDuoc.headerDanhSachVatTuXuatThuoc.donGia')}
          dataType="number"
          alignment="left"
          format={numberFormat}
        />
        <Column
          dataField={'THANHTIEN_THUOC'}
          caption={translate('duoc.duocXuatDuoc.headerDanhSachVatTuXuatThuoc.thanhTien')}
          dataType="number"
          alignment="left"
          format={numberFormat}
        />
        <Column
          dataField={'TENKHOVATTU'}
          caption={translate('duoc.duocXuatDuoc.headerDanhSachVatTuXuatThuoc.kho')}
          dataType="string"
          alignment="left"
        />
        <Column
          dataField={'TEN_PHONG_INTOA'}
          caption={translate('duoc.duocXuatDuoc.headerDanhSachVatTuXuatThuoc.phongRatoa')}
          dataType="string"
          alignment="left"
        />
        <Column
          dataField={'BACSI_RATOA'}
          caption={translate('duoc.duocXuatDuoc.headerDanhSachVatTuXuatThuoc.tenBacSi')}
          dataType="string"
          alignment="left"
        />
        <Column
          dataField={'GHI_CHU_CT_TOA_THUOC'}
          caption={translate('duoc.duocXuatDuoc.headerDanhSachVatTuXuatThuoc.ghiChu')}
          dataType="string"
          alignment="left"
        />
        <Summary>
          <TotalItem
            displayFormat={'{0} vật tư'}
            showInColumn={'TEN_VAT_TU'}
            column={'MAVATTU'}
            summaryType="count"
          />
        </Summary>
        <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
        <Paging defaultPageSize={5} />
      </DataGrid>);
  }
}

export default GridDanhSachVatTuXuatDuoc;
