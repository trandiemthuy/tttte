import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection, Pager, Paging
} from 'devextreme-react/data-grid';
import DropDownVatTu
  from "app/modules/duoc/duoc-components/chuyen-kho/components/dropdown-vat-tu/dropdown-vat-tu";
import {NumberBox, TextBox, Validator} from "devextreme-react";
import {Box, Item as BoxItem} from "devextreme-react/box";
import {RequiredRule} from "devextreme-react/form";
import notify from "devextreme/ui/notify";
import {
  configLoadPanel,
  NOTIFY_DISPLAYTIME,
  NOTIFY_WIDTH,
  numberFormat,
  pageSizes,
  SHADING
} from "app/modules/duoc/configs";
import {
  defaultToaThuocNgoaiTruModel,
  IToaThuocNgoaiTruModel
} from "app/modules/duoc/duoc-components/kcb/toa-thuoc-ngoai-tru.model";
import {getDvtt, getMaNhanVien, getMaPhongBan, getMaPhongBenh} from "app/modules/duoc/duoc-components/utils/thong-tin";
import {IXuatDuocBanLeModel} from "app/modules/duoc/duoc-components/xuat-duoc-ban-le/xuat-duoc-ban-le.model";
import {convertToSQLDate, convertToSQLDate1} from "app/modules/duoc/duoc-components/utils/funtions";
import dialog from "devextreme/ui/dialog";

export interface IGridDanhSachChiTietToaThuocBanLeProp extends IDataGridOptions {
  listDanhSachCTToaThuocBanLe: any;
  listDanhSachVatTu: any;
  selectedXuatThuocBanLe: IXuatDuocBanLeModel;
  // handleReloadDanhSachVatTu: Function;
  addCTToaThuocBanLe: Function;
  deleteCTToaThuocBanLe: Function;
  enableInput: boolean;
}
export interface IGridDanhSachChiTietToaThuocBanLeState {
  toaThuocBanLe: IToaThuocNgoaiTruModel;
  isClosed: boolean;
}

// Khung gõ chi tiết chuyển kho
const mdctCot1 = 15;
const mdctCot2 = 15;
const mdctCot3 = 10;
const mdctCot4 = 10;
const mdctCot5 = 10;
const mdctCot6 = 15;
const mdctCot7 = 15;
const mdctCot8 = 10;

export class GridDanhSachChiTietToaThuocBanLe extends React.Component<IGridDanhSachChiTietToaThuocBanLeProp, IGridDanhSachChiTietToaThuocBanLeState> {
  private refTenVatTu = React.createRef<DropDownVatTu>();
  private refHoatChat = React.createRef<TextBox>();
  private refDVT = React.createRef<TextBox>();
  private refCachDung = React.createRef<TextBox>();
  private refSoLuong = React.createRef<NumberBox>();
  private refDonGia = React.createRef<NumberBox>();
  private refThanhTien = React.createRef<NumberBox>();
  private refGhiChu = React.createRef<TextBox>();
  constructor(props) {
    super(props);
    this.state = {
      toaThuocBanLe: defaultToaThuocNgoaiTruModel,
      isClosed: false
    };
  }
  componentDidUpdate(prevProps: Readonly<IGridDanhSachChiTietToaThuocBanLeProp>, prevState: Readonly<IGridDanhSachChiTietToaThuocBanLeState>, snapshot?: any): void {
    if (this.props.enableInput && !this.state.isClosed) {
      setTimeout(() => {
        this.refTenVatTu.current.handleFocus();
      }, 100);
      this.setState({
        isClosed: true
      });
    }
  }

  handleSelectTenVatTu = () => {
    const selectedVatTuObj = this.refTenVatTu.current.state.selectedVatTuObj;
    this.setState({
      toaThuocBanLe: {...this.state.toaThuocBanLe,
        maVatTu: selectedVatTuObj.maVatTu,
        tenGoc: selectedVatTuObj.hoatChat,
        dvt: selectedVatTuObj.dvt,
        cachSuDung: selectedVatTuObj.cachSuDung,
        donGiaBV: selectedVatTuObj.donGia,
        donGiaBH: selectedVatTuObj.donGia,
        soLuong: selectedVatTuObj.soLuong,
        soLuongThucLinh: selectedVatTuObj.soLuong,
        thanhTien: selectedVatTuObj.soLuong * selectedVatTuObj.donGia,
        tenVatTu: selectedVatTuObj.tenVatTu
      }
    });
    setTimeout(() => {
      this.refSoLuong.current.instance.focus();
    }, 100);
  };
  handleEnterNextInput = (e, currentRef, nextRef) => {
    if (currentRef === this.refGhiChu) {
      this.addChiTietToaThuocBanLe();
      this.refTenVatTu.current.handleFocus();
    } else {
      nextRef.current.instance.focus();
    }
  };
  handleOnChangeChiTietPhieuChuyen = (e, name) => {
    switch (name) {
      case 'soLuong': {
        const soLuong = parseFloat(e.value);
        const donGia = this.state.toaThuocBanLe.donGiaBV;
        if (soLuong === 0) {
          notify({ message: "Số lượng phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.refSoLuong.current.instance.focus();
          return;
        }

        this.setState({
          toaThuocBanLe: {
            ...this.state.toaThuocBanLe,
            soLuong,
            thanhTien: donGia * soLuong
          }
        });
        break;
      }
      default: {
        this.setState({
          toaThuocBanLe: {
            ...this.state.toaThuocBanLe,
            [name]: e.value
          }
        });
        break;
      }
    }
  };
  handleOnChangeGridChiTietPhieuChuyen = (e) => {
    this.setState({
      toaThuocBanLe: {...this.state.toaThuocBanLe, ...e.selectedRowsData[0]}
    });
  };
  handleOnKeyDownGridChiTietPhieuChuyen = (e) => {
    const isMac = window.navigator.platform.toUpperCase().includes('MAC', 0);
    if (e.event.keyCode === 46 || (e.event.keyCode === 8 && isMac)) {
      if (!this.props.enableInput) return;
      dialog.confirm("Xóa chi tiết thuốc?", "Thông báo hệ thống")
        .then(dialogResult => {
          if (dialogResult) {
            this.props.deleteCTToaThuocBanLe({...this.state.toaThuocBanLe,
              dvtt: getDvtt(),
              sttToaThuoc: this.state.toaThuocBanLe.sttToaThuoc,
              maToaThuoc: this.props.selectedXuatThuocBanLe.maToaThuoc,
              maKhamBenh: 0,
              soPhieuThanhToan: 0,
              thanhTien: this.state.toaThuocBanLe.thanhTien,
              maKhoVatTu: this.props.selectedXuatThuocBanLe.noiXuat,
              nghiepVu: 'ngoaitru_toabanle',
              soVaoVien: 0,
              maUser: getMaNhanVien(),
              idGiaoDich: 0,
              maKhoa: getMaPhongBan(),
              maPhong: getMaPhongBenh()
            });
          }
        });
    }
  };

  // Functions
  addChiTietToaThuocBanLe = () => {
    this.props.addCTToaThuocBanLe({
      ...this.state.toaThuocBanLe,
      dvtt: getDvtt(),
      maToaThuoc: this.props.selectedXuatThuocBanLe.maToaThuoc,
      maKhoVatTu: this.props.selectedXuatThuocBanLe.noiXuat,
      nghiepVu: 'ngoaitru_toabanle',
      soNgay: 0,
      sang: 0,
      trua: 0,
      chieu: 0,
      toi: 0,
      ngayRaToa: convertToSQLDate1(this.props.selectedXuatThuocBanLe.ngayXuat),
      maBacSi: getMaNhanVien(),
      coBHYT: 0,
      namHienTai: new Date().getFullYear(),
      maBenhNhan: 0,
      soPhieuThanhToan: 0,
      idTiepNhan: 0,
      maKhamBenh: 0,
      ngayHienTai: new Date(),
      daThanhToan: 0,
      soVaoVien: 0,
      maUser: getMaNhanVien(),
      idGiaoDich: 0,
      maKhoa: getMaPhongBan(),
      maPhong: getMaPhongBenh()
    });
  };

  render() {
    return (
      <Box
        direction="col"
        width="100%"
        height={'auto'}
      >
        <BoxItem baseSize={'35%'} ratio={0}>
          <table style={{ width: '100%' }}>
            <thead>
            <tr>
              <td className={'dx-datagrid-headers'} style={{ width: mdctCot1 + '%' }}>Tên vật tư</td>
              <td className={'dx-datagrid-headers'} style={{ width: mdctCot2 + '%' }}>Hoạt chất</td>
              <td className={'dx-datagrid-headers'} style={{ width: mdctCot3 + '%' }}>ĐVT</td>
              <td className={'dx-datagrid-headers'} style={{ width: mdctCot4 + '%' }}>Cách dùng</td>
              <td className={'dx-datagrid-headers'} style={{ width: mdctCot5 + '%' }}>Số lượng</td>
              <td className={'dx-datagrid-headers'} style={{ width: mdctCot6 + '%' }}>Đơn giá</td>
              <td className={'dx-datagrid-headers'} style={{ width: mdctCot7 + '%' }}>Thành tiền</td>
              <td className={'dx-datagrid-headers'} style={{ width: mdctCot8 + '%' }}>Ghi chú</td>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>
                <DropDownVatTu
                  ref={this.refTenVatTu}
                  className={'no-border-radius'}
                  // handleReloadDanhSachVatTu={this.props.handleReloadDanhSachVatTu}
                  listDanhSachVatTu={this.props.listDanhSachVatTu}
                  dropWidth={screen.width-50}
                  dropHeight={500}
                  handleSelectTenVatTu = {this.handleSelectTenVatTu}
                  disabled={!this.props.enableInput}
                />
              </td>
              <td>
                <TextBox
                  ref={this.refHoatChat}
                  className={'no-border-radius'}
                  width={'100%'}
                  disabled={true}
                  value={this.state.toaThuocBanLe.tenGoc}
                />
              </td>
              <td>
                <TextBox
                  ref={this.refDVT}
                  className={'no-border-radius'}
                  width={'100%'}
                  disabled={true}
                  value={this.state.toaThuocBanLe.dvt}
                />
              </td>
              <td>
                <TextBox
                  ref={this.refCachDung}
                  className={'no-border-radius'}
                  width={'100%'}
                  disabled={true}
                  value={this.state.toaThuocBanLe.cachSuDung}
                />
              </td>
              <td>
                <NumberBox
                  className={'no-border-radius'}
                  min={0}
                  ref={this.refSoLuong}
                  width={'100%'}
                  onEnterKey={e => this.handleEnterNextInput(e, this.refSoLuong, this.refGhiChu)}
                  value={this.state.toaThuocBanLe.soLuong}
                  onValueChanged={e => this.handleOnChangeChiTietPhieuChuyen(e, 'soLuong')}
                  disabled={!this.props.enableInput}
                  format={numberFormat}
                >
                  <Validator>
                    <RequiredRule message="Số lượng không được rỗng" />
                  </Validator>
                </NumberBox>
              </td>
              <td>
                <NumberBox
                  className={'no-border-radius'}
                  ref={this.refDonGia}
                  width={'100%'}
                  disabled={true}
                  value={this.state.toaThuocBanLe.donGiaBV}
                  format={numberFormat}
                />
              </td>
              <td>
                <NumberBox
                  className={'no-border-radius'}
                  ref={this.refThanhTien}
                  width={'100%'}
                  disabled={true}
                  value={this.state.toaThuocBanLe.thanhTien}
                  format={numberFormat}
                />
              </td>
              <td>
                <TextBox
                  className={'no-border-radius'}
                  ref={this.refGhiChu}
                  width={'100%'}
                  value={this.state.toaThuocBanLe.ghiChu}
                  onValueChanged={e => this.handleOnChangeChiTietPhieuChuyen(e, 'ghiChu')}
                  onEnterKey={e => this.handleEnterNextInput(e, this.refGhiChu, this.refTenVatTu)}
                  disabled={!this.props.enableInput}
                />
              </td>
            </tr>
            </tbody>
          </table>
        </BoxItem>
        <BoxItem baseSize={'35%'} ratio={0}>
          <DataGrid
            loadPanel={configLoadPanel}
            showColumnHeaders={false}
            dataSource={this.props.listDanhSachCTToaThuocBanLe}
            showBorders={true}
            showColumnLines={true}
            rowAlternationEnabled={true}
            allowColumnReordering={true}
            allowColumnResizing={true}
            columnResizingMode={'nextColumn'}
            columnMinWidth={50}
            width={'100%'}
            noDataText={'Không có dữ liệu'}
            wordWrapEnabled={true}
            onKeyDown={this.handleOnKeyDownGridChiTietPhieuChuyen}
            onSelectionChanged={this.handleOnChangeGridChiTietPhieuChuyen}
            {...this.props}
          >
            <Selection mode="single" />
            <FilterRow visible={true}
                       applyFilter={'auto'} />
            <HeaderFilter visible={true} />
            <Column
              dataField="sttToaThuoc"
              caption="Số thứ tự chi tiết toa thuốc"
              dataType="string"
              alignment="left"
              width={'0%'}
              visible={false}
            />
            <Column
              dataField="tenVatTu"
              caption="Tên vật tư"
              dataType="string"
              alignment="left"
              width={mdctCot1 + '%'}
            />
            <Column
              dataField="tenGoc"
              caption="Hoạt chất"
              dataType="string"
              alignment="left"
              width={mdctCot2 + '%'}
            />
            <Column
              dataField="dvt"
              caption="Đơn vị tính"
              dataType="string"
              alignment="left"
              width={mdctCot3 + '%'}
            />
            <Column
              dataField="cachSuDung"
              caption="Cách dùng"
              dataType="string"
              alignment="left"
              width={mdctCot4 + '%'}
            />
            <Column
              dataField="soLuong"
              caption="Số lượng"
              dataType="number"
              alignment="left"
              width={mdctCot5 + '%'}
              format={numberFormat}
            />
            <Column
              dataField="donGiaBV"
              caption="Đơn giá"
              dataType="number"
              alignment="left"
              width={mdctCot6 + '%'}
              format={numberFormat}
            />
            <Column
              dataField="thanhTien"
              caption="Thành tiền"
              dataType="number"
              alignment="left"
              width={mdctCot7 + '%'}
              format={numberFormat}
            />
            <Column
              dataField="ghiChu"
              caption="Ghi chú"
              dataType="string"
              alignment="left"
              width={mdctCot8 + '%'}
            />
            <Summary>
              <TotalItem
                displayFormat={'{0} vật tư'}
                summaryType="count"
                column="tenVatTu"
              />
              <TotalItem
                displayFormat={'Tổng: {0}'}
                column="thanhTien"
                summaryType="sum"
              />
            </Summary>
            <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
            <Paging defaultPageSize={5} />
          </DataGrid>
        </BoxItem>
      </Box>
    );
  }
}

export default GridDanhSachChiTietToaThuocBanLe;
