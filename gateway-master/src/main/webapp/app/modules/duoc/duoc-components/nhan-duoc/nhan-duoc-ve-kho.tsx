import React from 'react';
import {RouteComponentProps} from "react-router";
import {IRootState} from "app/shared/reducers";
import {connect} from "react-redux";
import { Button, SelectBox, CheckBox } from 'devextreme-react';
import Box, {Item as BoxItem} from "devextreme-react/box";
import notify from "devextreme/ui/notify";
import {
  getDanhSachPhieuNhanDuocVeKho,
  nhanDuocVeKho,
  huyNhanDuocVeKho,
  exportExcelDuyetPhieu,
  printBienBanKiemNhapNhanDuoc
} from './nhan-duoc.reducer';
import {
  getDanhSachKhoChuyenDuyet,
  printPhieuXuatKhoDuyet
} from '../duyet-phieu/duyet-phieu.reducer';
import {
  getDanhSachChiTietChuyenKho,
  getDanhSachBanInPhieuDuTruKhoaPhong,
  printPhieuDuTruKhoaPhong
} from '../chuyen-kho/chuyen-kho.reducer';
import PopUpChiTietPhieuDuyet
  from "app/modules/duoc/duoc-components/duyet-phieu/components/popup-chi-tiet-phieu/popup-chi-tiet-phieu";
import GridDanhSachPhieuChuyenKhoDuyet from '../duyet-phieu/components/grid-danh-sach-phieu-chuyen-kho-duyet/grid-danh-sach-phieu-chuyen-kho-duyet'

import {
  convertToDate
} from "app/modules/duoc/duoc-components/utils/funtions";
import {NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING} from '../../configs';
import {
  getDvtt, getMaNhanVien, getMaPhongBan, getMaPhongBenh,
  getTenBenhVien,
  getTenNhanVien,
  getTenTinh
} from "app/modules/duoc/duoc-components/utils/thong-tin";
import PopUpDanhSachBanInChuyenKho
  from "app/modules/duoc/duoc-components/chuyen-kho/components/popup-danh-sach-ban-in/popup-danh-sach-ban-in";
import TuNgayDenNgayNgangComponent
  from "app/modules/duoc/duoc-components/utils/tungay-denngay/tungay-denngay-ngang";
import AdvanceDateBox from "app/modules/duoc/duoc-components/utils/advance-datebox/advance-datebox";
import DuocLabelComponent from "app/modules/duoc/duoc-components/utils/duoc-label/duoc-label";

export interface INhanDuocProp extends StateProps, DispatchProps, RouteComponentProps<{ nghiepVu: string }> {
  account: any;
}

export interface INhanDuocState {
  khoChuyen: {
    maKhoVaTTu: number,
    tenKhoVatTu: string
  }
  duyet: number; // 0 : Chưa duyệt    1: Đã duyệt     2: Đã nhận
  selectedPhieuChuyen: any;
  chiTietPopUp: {
    visible: boolean;
  }
  showPopUp: boolean;
  printPopUp: {
    loaiIn: number; // 0: In phiếu dự trù   1: In phiếu bổ sung    2:
    visible: boolean;
    loaiFile: string;
  }
  loadingConfigs: {
    visible: boolean;
    position: string;
  }
}

export class DuocNhanDuocComponent extends React.Component<INhanDuocProp, INhanDuocState> {
  private refTuNgayDenNgay = React.createRef<TuNgayDenNgayNgangComponent>();
  private refNgayNhan = React.createRef<AdvanceDateBox>();
  constructor(props) {
    super(props);
    this.state = {
      khoChuyen: {
        maKhoVaTTu: null,
        tenKhoVatTu: ''
      },
      duyet: 1,
      selectedPhieuChuyen: {},
      chiTietPopUp: {
        visible: false
      },
      showPopUp: false,
      printPopUp: {
        loaiIn: 0,
        visible: false,
        loaiFile: 'pdf'
      },
      loadingConfigs: {
        visible: false,
        position: 'this'
      }
    }
  }

  componentDidMount(): void {
    this.getDanhSachKhoChuyen(getDvtt(), getMaNhanVien());
  }

  componentDidUpdate(prevProps: Readonly<INhanDuocProp>, prevState: Readonly<INhanDuocState>, snapshot?: any): void {
    if (prevProps.match.params.nghiepVu !== this.props.match.params.nghiepVu) {
      this.getDanhSachKhoChuyen(getDvtt(), getMaNhanVien());
      this.handleOnClickButtonLamMoi();
    }
  }

  // Handles
  handleOnChangeKhoChuyen = (e) => {
    this.setState({
      khoChuyen: {
        maKhoVaTTu: parseInt(e.selectedItem.MAKHOVATTU, 0),
        tenKhoVatTu: e.selectedItem.TENKHOVATTU
      }
    });
    this.handleOnClickButtonLamMoi();
  };
  handleOnChangeTrangThai = (e) => {
    this.setState({
      duyet: e.value ? 2 : 1
    });
    this.handleOnClickButtonLamMoi();
  };
  handleOnClickButtonLamMoi = () => {
    if (this.state.khoChuyen.maKhoVaTTu === null) {
      notify({ message: "Chưa chọn kho chuyển", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    this.getDanhSachPhieuNhanDuoc();
  };
  handleOnSelectedChangeGridDanhSach = (e) => {
    this.setState({
      selectedPhieuChuyen: e.selectedRowsData
    });
  };
  handleOnContextMenuGridDanhSach = (e) => {
    if (e.row.data === undefined) {
      return;
    }
    this.setState({
      selectedPhieuChuyen: e.row.data
    });
    if (e.row.rowType === 'data') {
      e.items = [{
        icon: 'bulletlist',
        text: "Xem chi tiết phiếu chuyển kho",
        onItemClick: () => {
          this.handleOnClickMenuXemChiTiet(this.state.selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU, getDvtt());
        }
      },
        {
          icon: this.state.duyet === 1 ? 'check' : 'close',
          text: this.state.duyet === 1 ? 'Nhận dược về kho' : 'Hủy nhận dược',
          onItemClick: () => {
            this.handleOnClickMenuNhanDuoc(this.state.duyet);
          }
        },
        {
          icon: 'print',
          text: this.state.duyet === 1 ? 'In phiếu xuất chuyển kho' : 'In phiếu nhận',
          items: [
            {
              icon: 'exportpdf',
              text: this.state.duyet === 1 ? 'In phiếu xuất chuyển kho (PDF)' : 'In phiếu nhận (PDF)',
              onItemClick: () => {
                this.handleOnClickButtonInPhieuXuat('pdf', 0);
              }
            },
            {
              icon: 'exportxlsx',
              text: this.state.duyet === 1 ? 'In phiếu xuất chuyển kho (XLS)' : 'In phiếu nhận (XLS)',
              onItemClick: () => {
                this.handleOnClickButtonInPhieuXuat('xls', 0);
              }
            },
            {
              icon: 'docfile',
              text: this.state.duyet === 1 ? 'In phiếu xuất chuyển kho (RTF)' : 'In phiếu nhận (RTF)',
              onItemClick: () => {
                this.handleOnClickButtonInPhieuXuat('rtf', 0);
              }
            }
          ]
        }
      ];
      if (this.state.duyet === 1) {
        e.items.push();
      } else {
        e.items.push(
          {
            icon: 'print',
            text: "In phiếu nhận dược về kho",
            items: [
              {
                icon: 'exportpdf',
                text: 'In phiếu nhận dược về kho (PDF)',
                onItemClick: () => {
                  this.handleChangePopUpPrintState(0,true, 'pdf');
                }
              },
              {
                icon: 'exportxlsx',
                text: 'In phiếu nhận dược về kho (XLS)',
                onItemClick: () => {
                  this.handleChangePopUpPrintState(0,true, 'xls');
                }
              },
              {
                icon: 'docfile',
                text: 'In phiếu nhận dược về kho (RTF)',
                onItemClick: () => {
                  this.handleChangePopUpPrintState(0,true, 'rtf');
                }
              },
            ]
          },
          {
            icon: 'print',
            text: "In biên bản kiểm nhập",
            items: [
              {
                icon: 'exportpdf',
                text: 'In biên bản kiểm nhập (PDF)',
                onItemClick: () => {
                  this.handleOnClickButtonInBienBanKiemNhap('pdf');
                }
              },
              {
                icon: 'exportxlsx',
                text: 'In biên bản kiểm nhập (XLS)',
                onItemClick: () => {
                  this.handleOnClickButtonInBienBanKiemNhap('xls');
                }
              },
              {
                icon: 'docfile',
                text: 'In biên bản kiểm nhập (RTF)',
                onItemClick: () => {
                  this.handleOnClickButtonInBienBanKiemNhap('rtf');
                }
              },
            ]
          }
        );
      }
    }
  };
  handleOnClickMenuXemChiTiet = (idPhieuChuyen: number, dvtt: string) => {
    this.props.getDanhSachChiTietChuyenKho(idPhieuChuyen, dvtt);
    this.handleChangePopUpChiTietState(true);
  };
  handleOnClickMenuNhanDuoc = (loaiDuyet: number) => { // loaiDuyet: 0: Nhận dược   1: Hủy nhận
    const selectedPhieuChuyen = this.state.selectedPhieuChuyen;
    if (loaiDuyet === 1) {
      this.props.nhanDuocVeKho(
        getDvtt(),
        !['34', '35', '37', '50'].includes(this.props.match.params.nghiepVu) ? 2 : parseInt(this.props.match.params.nghiepVu,0),
        selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU,
        selectedPhieuChuyen.MA_KHO_GIAO,
        selectedPhieuChuyen.MA_KHO_NHAN,
        convertToDate(this.refNgayNhan.current.getValue()),
        getMaNhanVien(),
        getMaPhongBan(),
        getMaPhongBenh(),
        this.handleOnClickButtonLamMoi
      );
    } else {
      this.props.huyNhanDuocVeKho(
        getDvtt(),
        selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU,
        getMaNhanVien(),
        getMaPhongBan(),
        getMaPhongBenh(),
        this.handleOnClickButtonLamMoi
      )
    }
  };
  handleChangePopUpChiTietState = (showPopUp: boolean) => {
    this.setState({
      showPopUp
    });
  };
  handleChangePopUpPrintState = (loaiIn: number, visible: boolean, loaiFile: string) => {
    const selectedPhieuChuyen = this.state.selectedPhieuChuyen;
    this.props.getDanhSachBanInPhieuDuTruKhoaPhong(selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU, getDvtt());
    this.setState({
      printPopUp: {
        loaiIn,
        visible,
        loaiFile
      }
    });
  };
  handleOnSelectRowGridDanhSachBanIn = (e: any) => { // 0: In phiếu dự trù
    const selectedPhieuChuyen = this.state.selectedPhieuChuyen;
    const selectedRow = e;
    if (this.state.printPopUp.loaiIn === 0) {
      this.props.printPhieuDuTruKhoaPhong(
        getDvtt(),
        selectedPhieuChuyen.SOPHIEUCHUYEN,
        selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU,
        selectedRow.NGAY_CHUYEN,
        selectedRow.THANH_TIEN,
        selectedRow.MAKHOVATTU,
        selectedRow.TENKHOVATTU,
        getMaPhongBan(),
        selectedRow.TEN_PHONGBAN,
        selectedRow.MALOAIVATTU,
        getMaNhanVien(),
        getTenBenhVien(),
        getTenTinh(),
        this.state.printPopUp.loaiFile,
        this.handleLoading
      );
    }
  };
  handleLoading = (visible: boolean) => {
    this.setState({
      loadingConfigs: {
        ...this.state.loadingConfigs,
        visible
      }
    });
  };
  handleOnClickButtonInPhieuXuat = (loaiFile: string, mau: number) => {
    const selectedPhieuChuyen = this.state.selectedPhieuChuyen;
    this.props.printPhieuXuatKhoDuyet(
      getDvtt(),
      selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU,
      !['34', '35', '37', '50'].includes(this.props.match.params.nghiepVu) ? 2 : parseInt(this.props.match.params.nghiepVu,0),
      getTenBenhVien(),
      getTenTinh(),
      getTenNhanVien(),
      mau, // 0: Đứng      1: Ngang
      loaiFile,
      this.handleLoading
    )
  };
  handleOnClickButtonInBienBanKiemNhap = (loaiFile: string) => {
    const selectedPhieuChuyen = this.state.selectedPhieuChuyen;
    this.props.printBienBanKiemNhapNhanDuoc(
      getDvtt(),
      selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU,
      getMaPhongBan(),
      getMaNhanVien(),
      getTenBenhVien(),
      getTenTinh(),
      loaiFile,
      this.handleLoading
    );
  };
  handleOnClickButtonExportExcel = () => {
    this.props.exportExcelDuyetPhieu(
      getDvtt(),
      convertToDate(this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay),
      convertToDate(this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay),
      !['34', '35', '37', '50'].includes(this.props.match.params.nghiepVu) ? 2 : parseInt(this.props.match.params.nghiepVu,0),
      this.state.khoChuyen.maKhoVaTTu,
      this.state.duyet,
      getMaPhongBan(),
      this.handleLoading
    );
  };

  // Funtion
  changeTitleByNghiepVu = () => {
    const nghiepVu = this.props.match.params.nghiepVu;
    let title = '';
    switch (nghiepVu) {
      case '37': {
        title = "Dự trù tuyến trên";
        break;
      }
      case '50': {
        title = "Hoàn trả tuyến trên";
        break;
      }
      case '35': {
        title = "Hoàn trả khoa phòng";
        break;
      }
      case '34': {
        title = "Dự trù khoa phòng";
        break;
      }
      case '55': {
        title = "Chuyển hủy sử dụng vật tư tiêu hao khoa phòng, CLS";
        break;
      }
      default: {
        title = "Chuyển kho";
      }
    }
    return title;
  };
  getDanhSachKhoChuyen = (dvtt: string, maNhanVien: number) => {
    this.props.getDanhSachKhoChuyenDuyet(dvtt, maNhanVien);
  };
  getDanhSachPhieuNhanDuoc= () => {
    this.props.getDanhSachPhieuNhanDuocVeKho(
      getDvtt(),
      convertToDate(this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay),
      convertToDate(this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay),
      !['34', '35', '37', '50'].includes(this.props.match.params.nghiepVu) ? 2 : parseInt(this.props.match.params.nghiepVu,0),
      this.state.khoChuyen.maKhoVaTTu,
      this.state.duyet,
      getMaPhongBan()
    );
  };

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (<div>
      <div className={'duoc-title'}>
        {
          'Nhận dược ' + this.changeTitleByNghiepVu().toLowerCase()
        }
      </div>
      <div className={'div-box-wrapper row-margin-top-5'}>
        <DuocLabelComponent icon={'bookmark'} text={'Thông tin lọc phiếu chuyển kho'}/>
        <Box
          direction="col"
          width="100%"
          height={'auto'}
        >
          <BoxItem ratio={0} baseSize={'100%'}>
            <TuNgayDenNgayNgangComponent
              ref={this.refTuNgayDenNgay}
              handleOnClickButtonLamMoi={this.handleOnClickButtonLamMoi}
              labelTuNgayWidth={'10%'}
              dateboxTuNgayWidth={'20%'}
              labelDenNgayWidth={'10%'}
              dateboxDenNgayWidth={'20%'}
              renderRight={() =>
                <Button
                  icon={'exportxlsx'}
                  text={'Xuất Excel'}
                  type={'default'}
                  className={'col-margin-left-5 button-duoc'}
                  onClick={this.handleOnClickButtonExportExcel}
                />
              }
            />
          </BoxItem>
          <BoxItem baseSize={'100%'} ratio={0}>
            <Box direction="row" width="100%" height={'auto'} className={'row-padding-top-5'}>
              <BoxItem baseSize={'10%'} ratio={0}>
                <div className={'col-align-right'}>
                  Kho chuyển
                </div>
              </BoxItem>
              <BoxItem baseSize={'20%'} ratio={0}>
                <SelectBox
                  dataSource={this.props.listDanhSachKhoChuyenDuyet}
                  valueExpr={'MAKHOVATTU'}
                  displayExpr={'TENKHOVATTU'}
                  placeholder={'Chọn kho chuyển...'}
                  onSelectionChanged={this.handleOnChangeKhoChuyen}
                />
              </BoxItem>
              <BoxItem baseSize={'10%'} ratio={0}>
                <div className={'col-align-right'}>
                  Ngày nhận
                </div>
              </BoxItem>
              <BoxItem baseSize={'20%'} ratio={0}>
                <AdvanceDateBox ref={this.refNgayNhan} />
              </BoxItem>
              <BoxItem ratio={0} baseSize={'5%'}>
                <div className={'col-align-right'}>
                  <CheckBox
                    text={'Đã nhận'}
                    onValueChanged={e => this.handleOnChangeTrangThai(e)}
                    value={this.state.duyet === 2}
                  />
                </div>
              </BoxItem>
            </Box>
          </BoxItem>
        </Box>
      </div>
      <Box
        direction={"col"}
        width={'100%'}
      >
        <BoxItem baseSize={'100%'} ratio={0}>
          <div className={'div-box-wrapper row-margin-top-5'}>
            <DuocLabelComponent icon={'bookmark'} text={'Danh sách phiếu chuyển kho'}/>
            <GridDanhSachPhieuChuyenKhoDuyet
              // isNhan={true}
              listDanhSachPhieuChuyenKhoDuyet={this.props.listDanhSachPhieuChuyenKhoNhan}
              handleOnContextMenuGridDanhSach={this.handleOnContextMenuGridDanhSach}
              // keyExpr={'ID_CHUYEN_KHO_VAT_TU'}
              selectedRowKeys={[this.state.selectedPhieuChuyen]}
              // onSelectionChanged={this.handleOnSelectedChangeGridDanhSach}
              loaiHienThi={1}
            />
          </div>
        </BoxItem>
        <BoxItem ratio={0} baseSize={'100%'}>
          <PopUpChiTietPhieuDuyet
            listDanhSachChiTietPhieuDuyet = {this.props.listDanhSachChiTietChuyenKho}
            handleChangePopUpChiTietState={this.handleChangePopUpChiTietState}
            showPopUp={this.state.showPopUp}
            title={this.changeTitleByNghiepVu()}
            selectedPhieuChuyen={this.state.selectedPhieuChuyen}
            isNhan={true}
          />
        </BoxItem>
        <BoxItem ratio={0} baseSize={'100%'}>
          <PopUpDanhSachBanInChuyenKho
            listDanhSachBanIn={this.props.listDanhSachBanInPhieuDuTruKhoaPhong}
            handleChangePopUpState={this.handleChangePopUpPrintState}
            handleOnSelectRowGridDanhSachBanIn={this.handleOnSelectRowGridDanhSachBanIn}
            printPopUp={this.state.printPopUp}
          />
        </BoxItem>
      </Box>
    </div>);
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  listDanhSachKhoChuyenDuyet: storeState.duyetphieu.listDanhSachKhoChuyenDuyet,
  listDanhSachChiTietChuyenKho: storeState.chuyenkho.listDanhSachChiTietChuyenKho,
  listDanhSachBanInPhieuDuTruKhoaPhong: storeState.chuyenkho.listDanhSachBanInPhieuDuTruKhoaPhong,
  listDanhSachPhieuChuyenKhoNhan: storeState.nhanduoc.listDanhSachPhieuChuyenKhoNhan
});

const mapDispatchToProps = {
  // Nhận
  getDanhSachKhoChuyenDuyet,
  getDanhSachPhieuNhanDuocVeKho,
  nhanDuocVeKho,
  huyNhanDuocVeKho,
  printPhieuXuatKhoDuyet,
  exportExcelDuyetPhieu,
  printBienBanKiemNhapNhanDuoc,

  // Chuyển kho
  getDanhSachChiTietChuyenKho,
  getDanhSachBanInPhieuDuTruKhoaPhong,
  printPhieuDuTruKhoaPhong
};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DuocNhanDuocComponent);
