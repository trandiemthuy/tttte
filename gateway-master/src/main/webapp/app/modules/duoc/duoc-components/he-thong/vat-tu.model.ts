export interface IVatTuModel {
  maVatTu?: number;
  tenVatTu?: string;
  tenHienThi?: string;
  soLuong?: number;
  donGia?: number;
  hoatChat?: string;
  hamLuong?: string;
  dvt?: string;
  cachSuDung?: string;
  ngayHetHan?: string;
  soThau?: string;
  quyetDinh?: string;
  ghiChu?: string;
  ngoaiDanhMuc?: number; // 0: không    1: Ngoài danh mục
  soLoSanXuat?: string;
  dangThuoc?: string;
  nguonDuoc?: string;
  maVatTuNhap?: number;
  soDangKy?: string;
}
export const defaultVatTuModel: IVatTuModel = {
  maVatTu: null,
  tenVatTu: null,
  tenHienThi: null,
  soLuong: null,
  donGia: null,
  hoatChat: null,
  hamLuong: null,
  dvt: null,
  cachSuDung: null,
  ngayHetHan: null,
  soThau: null,
  quyetDinh: null,
  ghiChu: null,
  ngoaiDanhMuc: null, // 0: không    1: Ngoài danh mục
  soLoSanXuat: null,
  dangThuoc: null,
  nguonDuoc: null,
  maVatTuNhap: null,
  soDangKy: null
};
