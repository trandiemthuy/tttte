import React, {createRef} from 'react';
import {RouteComponentProps} from "react-router";
import {IRootState} from "app/shared/reducers";
import {connect} from "react-redux";
import Box, {Item as BoxItem} from "devextreme-react/box";
import {
  getDanhSachKhoNoiTru,
  getDanhSachBacSi
} from '../he-thong/he-thong.reducer';
import {
  getDanhSachVatTuTonKho
} from '../kcb/kcb.reducer';
import {
  printPhieuHoanTraThuoc
} from '../xuat-duoc/xuat-duoc.reducer';
import {
  getDanhSachToaThuocBanLe,
  setSelectedThongTinXuatDuoc,
  setEnableXuatDuocBanLe,
  addToaThuocBanLe,
  updateToaThuocBanLe,
  deleteToaThuocBanLe,
  getDanhSachChiTietThuocBanLe,
  addCTToaThuocBanLe,
  deleteCTToaThuocBanLe,
  resetDanhSachVatTu
} from './xuat-duoc-ban-le.reducer';
import {
  getDvtt, getMaNhanVien, getMaPhongBan, getMaPhongBenh, getTenBenhVien, getTenTinh
} from "app/modules/duoc/duoc-components/utils/thong-tin";
import {Button, CheckBox, SelectBox} from "devextreme-react";
import DuocTitleComponent from "app/modules/duoc/duoc-components/utils/duoc-title/duoc-title";
import DuocLabelComponent from "app/modules/duoc/duoc-components/utils/duoc-label/duoc-label";
import {defaultEnableXuatDuocBanLe, IEnableXuatDuocBanLe} from "app/modules/duoc/duoc-components/xuat-duoc-ban-le/enable.model";
import AdvanceDateBox from "app/modules/duoc/duoc-components/utils/advance-datebox/advance-datebox";
import {convertToDate, convertToSQLDate} from "app/modules/duoc/duoc-components/utils/funtions";
import {translate} from "react-jhipster";
import GridDanhSachKhachHangBanLe
  from "app/modules/duoc/duoc-components/xuat-duoc-ban-le/components/grid-danh-sach-khach-hang/grid-danh-sach-khach-hang";
import FormThongTinXuatDuocBanLe
  from "app/modules/duoc/duoc-components/xuat-duoc-ban-le/components/form-thong-tin-xuat-duoc-ban-le/form-thong-tin-xuat-duoc-ban-le";
import {IXuatDuocBanLeModel} from "app/modules/duoc/duoc-components/xuat-duoc-ban-le/xuat-duoc-ban-le.model";
import GridDanhSachChiTietToaThuocBanLe
  from "app/modules/duoc/duoc-components/xuat-duoc-ban-le/components/grid-danh-sach-chi-tiet-xuat-duoc-ban-le/grid-danh-sach-chi-tiet-xuat-duoc-ban-le";
import {IToaThuocNgoaiTruModel} from "app/modules/duoc/duoc-components/kcb/toa-thuoc-ngoai-tru.model";

export interface IXuatDuocBanLeComponentProp extends StateProps, DispatchProps, RouteComponentProps<{ nghiepVu: string; }> {
}

export interface IXuatDuocBanLeComponentState {
  enableButton: IEnableXuatDuocBanLe;
  loadingConfigs: {
    visible: boolean;
    position: string;
  };
  printPopUp: {
    loaiIn: number; // 0: In phiếu dự trù nội trú    3: In phiếu hoàn trả
    visible: boolean;
    loaiFile: string;
  };
}

export class XuatDuocBanLeComponent extends React.Component<IXuatDuocBanLeComponentProp, IXuatDuocBanLeComponentState> {
  private refNgayLap = createRef<AdvanceDateBox>();
  constructor(props) {
    super(props);
    this.state = {
      enableButton: defaultEnableXuatDuocBanLe,
      loadingConfigs: {
        visible: false,
        position: 'body'
      },
      printPopUp: {
        loaiIn: 0,
        visible: false,
        loaiFile: 'pdf'
      }
    };
  }

  componentDidMount(): void {
    this.handleOnClickButtonLamMoi();
    this.getDanhSachKhoNoiTru();
    this.getDanhSachBacSi();
  }

  componentDidUpdate(prevProps: Readonly<IXuatDuocBanLeComponentProp>, prevState: Readonly<IXuatDuocBanLeComponentState>, snapshot?: any): void {
    if (prevProps.match.params.nghiepVu !== this.props.match.params.nghiepVu) {
      this.props.getDanhSachToaThuocBanLe(getDvtt(), convertToSQLDate(new Date().toDateString()));
    }
  }

  // Handles
  handleOnClickButtonLamMoi = () => {
    this.getDanhSachToaThuocBanLe();
  };
  handleLoading = (visible: boolean) => {
    this.setState({
      loadingConfigs: {
        ...this.state.loadingConfigs,
        visible
      }
    });
  };
  handleOnContextMenuGridDanhSach = (e) => {
    if (e.row.data === undefined) {
      return;
    }
    e.component.selectRowsByIndexes(e.rowIndex);
    if (e.row.rowType === 'data') {
      e.items = [
        {
          icon: 'print',
          text: "In danh sách phiếu xuất dược",
          items: [{
            icon: 'exportpdf',
            text: "In danh sách phiếu xuất dược (PDF)",
            onItemClick: () => {
              this.handleOnClickMenuInDanhSachXuatDuoc('pdf');
            }
          },
            {
              icon: 'exportxlsx',
              text: "In danh sách phiếu xuất dược (XLS)",
              onItemClick: () => {
                this.handleOnClickMenuInDanhSachXuatDuoc('pdf');
              }
            },
            {
              icon: 'docfile',
              text: "In danh sách phiếu xuất dược (RTF)",
              onItemClick: () => {
                this.handleOnClickMenuInDanhSachXuatDuoc('pdf');
              }
            }]
        }
      ];
    }
  };
  handleOnClickMenuInDanhSachXuatDuoc = (loaiFile: string) => {
    this.props.printPhieuHoanTraThuoc(
      getDvtt(),
      this.refNgayLap.current.getValue(),
      'ngoaitru_toabanle',
      getTenTinh(),
      getTenBenhVien(),
      loaiFile,
      this.handleLoading);
  };

  // Funtion
  getDanhSachToaThuocBanLe = () => {
    this.props.getDanhSachToaThuocBanLe(getDvtt(),
      this.refNgayLap.current === null ? convertToSQLDate(new Date().toDateString()) : this.refNgayLap.current.getValue()
    );
  };
  getDanhSachKhoNoiTru = () => {
    this.props.getDanhSachKhoNoiTru(getDvtt(), getMaPhongBan(), 'ngoaitru_toabanle');
  };
  getDanhSachBacSi = () => {
    this.props.getDanhSachBacSi(getDvtt());
  } ;
  addToaThuocBanLe = (ttXuatDuoc: IXuatDuocBanLeModel) => {
    this.props.addToaThuocBanLe(ttXuatDuoc, this.setMaToaThuoc, () => {
      this.getDanhSachToaThuocBanLe();
    });
  };
  setMaToaThuoc = (maToaThuoc: string) => {
    this.props.setSelectedThongTinXuatDuoc({...this.props.selectedXuatThuocBanLe, maToaThuoc});
  };
  updateToaThuocBanLe = (ttXuatDuoc: IXuatDuocBanLeModel) => {
    this.props.updateToaThuocBanLe(ttXuatDuoc, () => {
      this.getDanhSachToaThuocBanLe();
    });
  };
  addCTToaThuocBanLe = (ctToaThuoc: IToaThuocNgoaiTruModel) => {
    this.props.addCTToaThuocBanLe(ctToaThuoc, () => {
      this.getDanhSachToaThuocBanLe();
    });
  };
  deleteToaThuocBanLe = (ctToaThuoc: IToaThuocNgoaiTruModel) => {
    this.props.deleteToaThuocBanLe(ctToaThuoc, () => {
      this.getDanhSachToaThuocBanLe();
    })
  };

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    const { selectedXuatThuocBanLe, listDanhSachToaThuocBanLe, enableForm, listDanhSachCTToaThuocBanLe, listDanhSachVatTuTonKho } = this.props;
    return (<div>
      <DuocTitleComponent text={translate('duoc.duocXuatDuocBanLe.titleChucNang')}/>
      <Box
        direction="row"
        width="100%"
        height={'auto'}
      >
        <BoxItem baseSize={'30%'} ratio={0}>
          <div className={'div-box-wrapper'}>
            <DuocLabelComponent text={translate('duoc.duocXuatDuocBanLe.formLoc.labelLoc')}/>
            <div/>
            <Box
              direction={'col'}
              width={'100%'}
              height={'auto'}
            >
              <BoxItem
                ratio={0}
                baseSize={'100%'}
              >
                <div className={'row-margin-top-5'} style={{display: 'flex'}}>
                  <div className={'col-align-right'} style={{width: '15%'}}>
                    Ngày lập
                  </div>
                  <div style={{width: '60%'}}>
                    <AdvanceDateBox ref={this.refNgayLap}/>
                  </div>
                  <div className={'row-margin-left-5'} style={{width: '25%'}}>
                    <Button type={'default'} text={'Làm mới'} icon={'refresh'} onClick={this.handleOnClickButtonLamMoi}/>
                  </div>
                </div>
              </BoxItem>
            </Box>
          </div>
          <div className={'div-box-wrapper row-margin-top-5 div-height-100'}>
            <DuocLabelComponent text={translate('duoc.duocXuatDuocBanLe.formLoc.labelGrid')}/>
            <GridDanhSachKhachHangBanLe
              listDanhSachToaThuocBanLe={listDanhSachToaThuocBanLe}
              handleOnContextMenuGridDanhSach={this.handleOnContextMenuGridDanhSach}
              setSelectedThongTinXuatDuoc={this.props.setSelectedThongTinXuatDuoc}
              resetDanhSachVatTu={this.props.resetDanhSachVatTu}
              getDanhSachChitietToaBanLe={this.props.getDanhSachChiTietThuocBanLe}
              setEnableXuatDuocBanLe={this.props.setEnableXuatDuocBanLe}
            />
            <div style={{display: 'flex', width: '100%'}}>
              <div style={{width: '30px', height: '30px', marginTop: '5px', backgroundImage: 'radial-gradient( circle 939px at 0.7% 2.4%,  rgba(116,106,255,1) 0%, rgba(221,221,221,1) 100.2% )'}}>
              </div>
              <div style={{marginTop: '10px'}}>{translate('duoc.duocXuatDuocBanLe.formLoc.ghiChuDaThanhToan')}</div>
            </div>
          </div>
        </BoxItem>
        <BoxItem baseSize={'70%'} ratio={0}>
          <div className={'div-box-wrapper row-margin-left-5'}>
            <DuocLabelComponent icon={'bookmark'} text={translate('duoc.duocXuatDuocBanLe.formThongTinXuatDuoc.titleForm')}/>
            <FormThongTinXuatDuocBanLe
              setEnableXuatDuocBanLe={this.props.setEnableXuatDuocBanLe}
              selectedThongTinXuatDuoc={selectedXuatThuocBanLe}
              enableForm={enableForm}
              listDanhSachKhoNoiTru={this.props.listDanhSachKhoNoiTru}
              listDanhSachBacSi={this.props.listDanhSachBacSi}
              addToaThuocBanLe={this.addToaThuocBanLe}
              updateToaThuocBanLe={this.updateToaThuocBanLe}
              deleteToaThuocBanLe={this.deleteToaThuocBanLe}
              resetDanhSachVatTu={this.props.resetDanhSachVatTu}
              getDanhSachDuocTonKho={this.props.getDanhSachVatTuTonKho}
            />
          </div>
          <div className={'div-box-wrapper row-margin-left-5 row-margin-top-5'}>
            <DuocLabelComponent icon={'bookmark'} text={translate('duoc.duocXuatDuocBanLe.formThongTinXuatDuoc.titleForm')}/>
            <GridDanhSachChiTietToaThuocBanLe
              listDanhSachCTToaThuocBanLe={listDanhSachCTToaThuocBanLe}
              listDanhSachVatTu={listDanhSachVatTuTonKho}
              addCTToaThuocBanLe={this.addCTToaThuocBanLe}
              deleteCTToaThuocBanLe={this.props.deleteCTToaThuocBanLe}
              enableInput={this.props.enableForm.btnLuu}
              selectedXuatThuocBanLe={this.props.selectedXuatThuocBanLe}
            />
          </div>
        </BoxItem>
      </Box>
    </div>);
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  listDanhSachToaThuocBanLe: storeState.xuatduocbanle.listDanhSachToaThuocBanLe,
  listDanhSachCTToaThuocBanLe: storeState.xuatduocbanle.listDanhSachCTToaThuocBanLe,
  listDanhSachKhoNoiTru: storeState.hethong.listDanhSachKhoNoiTru,
  listDanhSachBacSi: storeState.hethong.listDanhSachBacSi,
  listDanhSachVatTuTonKho: storeState.kcb.listDanhSachVatTuTonKho,
  selectedXuatThuocBanLe: storeState.xuatduocbanle.selectedXuatThuocBanLe,
  enableForm: storeState.xuatduocbanle.enableForm
});

const mapDispatchToProps = {
  getDanhSachToaThuocBanLe,
  setSelectedThongTinXuatDuoc,
  printPhieuHoanTraThuoc,
  setEnableXuatDuocBanLe,
  getDanhSachKhoNoiTru,
  getDanhSachBacSi,
  getDanhSachVatTuTonKho,
  addToaThuocBanLe,
  updateToaThuocBanLe,
  deleteToaThuocBanLe,
  getDanhSachChiTietThuocBanLe,
  addCTToaThuocBanLe,
  deleteCTToaThuocBanLe,
  resetDanhSachVatTu
};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(XuatDuocBanLeComponent);
