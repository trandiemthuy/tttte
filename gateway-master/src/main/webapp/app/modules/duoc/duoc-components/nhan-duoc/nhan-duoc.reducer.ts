import axios from 'axios';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING } from '../../configs';
import notify from 'devextreme/ui/notify';
import { exportXLSType1, printType1 } from 'app/modules/duoc/duoc-components/utils/print-helper';
import { catchStatusCodeFromResponse } from 'app/modules/duoc/duoc-components/utils/funtions';

export const ACTION_TYPES = {
  GET_DANH_SACH_PHIEU_CHUYEN_NHAN: 'nhanduoc/GET_DANH_SACH_PHIEU_CHUYEN_NHAN',
  NHAN_DUOC_VE_KHO: 'nhanduoc/NHAN_DUOC_VE_KHO',
  HUY_NHAN_DUOC_VE_KHO: 'nhanduoc/HUY_NHAN_DUOC_VE_KHO'
};

const initialState = {
  listDanhSachPhieuChuyenKhoNhan: []
};

export type nhanDuocState = Readonly<typeof initialState>;

// Reducer
// eslint-disable-next-line complexity
export default (state: nhanDuocState = initialState, action): nhanDuocState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_PHIEU_CHUYEN_NHAN):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_PHIEU_CHUYEN_NHAN):
      return {
        ...state,
        listDanhSachPhieuChuyenKhoNhan: []
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_PHIEU_CHUYEN_NHAN):
      return {
        ...state,
        listDanhSachPhieuChuyenKhoNhan: action.payload.data
      };
    default:
      return state;
  }
};
// Actions
export const getDanhSachPhieuNhanDuocVeKho = (
  dvtt: string,
  tuNgay: string,
  denNgay: string,
  maNghiepVu: number,
  maKho: number,
  duyet: number,
  maPhongBan: string
) => async dispatch => {
  const requestUrl = `${apiURL}api/nhanduoc/getDanhSachPhieuNhanDuocVeKho`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_PHIEU_CHUYEN_NHAN,
    payload: axios
      .get(requestUrl, {
        params: {
          dvtt,
          tuNgay,
          denNgay,
          maNghiepVu,
          maKho,
          duyet,
          maPhongBan
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi tải danh sách phiếu nhận dược');
      })
  });
  return result;
};

export const nhanDuocVeKho = (
  dvtt: string,
  maNghiepVu: number,
  idChuyenKho: number,
  maKhoGiao: number,
  maKhoNhan: number,
  ngayNhan: string,
  nguoiNhan: number,
  maPhongBan: string,
  maPhongBenh: string,
  handleLamMoi: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/nhanduoc/nhanDuocVeKho`;
  const params = {
    dvtt,
    maNghiepVu,
    idChuyenKho,
    maKhoGiao,
    maKhoNhan,
    ngayNhan,
    nguoiNhan,
    maPhongBan,
    maPhongBenh
  };
  const reuslt = await dispatch({
    type: ACTION_TYPES.NHAN_DUOC_VE_KHO,
    payload: axios
      .post(requestUrl, JSON.stringify(params), { headers: { 'content-type': 'application/json' }, params })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Nhận dược thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi nhận dược về kho');
      })
  });
  handleLamMoi();
  return reuslt;
};

export const huyNhanDuocVeKho = (
  dvtt: string,
  idChuyenKho: number,
  nguoiHuy: number,
  maPhongBan: string,
  maPhongBenh: string,
  handleLamMoi: Function
) => async dispatch => {
  const requestUrl = `${apiURL}api/nhanduoc/huyNhanDuocVeKho`;
  const params = {
    dvtt,
    idChuyenKho,
    nguoiHuy,
    maPhongBan,
    maPhongBenh
  };
  const reuslt = await dispatch({
    type: ACTION_TYPES.HUY_NHAN_DUOC_VE_KHO,
    payload: axios
      .post(requestUrl, JSON.stringify(params), { headers: { 'content-type': 'application/json' }, params })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Hủy nhận dược thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        catchStatusCodeFromResponse(obj.response.status, 'Lỗi hủy nhận dược về kho');
      })
  });
  handleLamMoi();
  return reuslt;
};

export const exportExcelDuyetPhieu = (
  dvtt: string,
  tuNgay: string,
  denNgay: string,
  maNghiepVu: number,
  maKho: number,
  duyet: number,
  maPhongBan: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/nhanduoc/exportExcelNhanDuoc`;
  const params = {
    dvtt,
    tuNgay,
    denNgay,
    maNghiepVu,
    maKho,
    duyet,
    maPhongBan
  };
  exportXLSType1(url, params, {
    successMessage: 'Xuất dữ liệu thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};
export const printBienBanKiemNhapNhanDuoc = (
  dvtt: string,
  idChuyenKho: number,
  maPhongBan: string,
  nguoiLap: number,
  tenBenhVien: string,
  tenTinh: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/nhanduoc/printBienBanKiemNhapNhanDuoc`;
  const params = {
    dvtt,
    idChuyenKho,
    maPhongBan,
    nguoiLap,
    tenBenhVien,
    tenTinh,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In phiếu biên bản kiểm nhập thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};
