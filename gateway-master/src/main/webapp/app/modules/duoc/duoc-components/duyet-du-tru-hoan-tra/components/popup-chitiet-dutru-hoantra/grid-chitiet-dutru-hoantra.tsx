import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection, ColumnFixing, Pager, Paging, Grouping, GroupPanel
} from 'devextreme-react/data-grid';
import {configLoadPanel, numberFormat, pageSizes} from "app/modules/duoc/configs";

export interface IGridDanhSachChiTietPhieuDuTruHoanTraProp extends IDataGridOptions {
  listDanhSachChiTietPhieuDuTruHoanTra: any;
  loaiPhieu: number; // 0: Dự trù       1: Hoàn trả
  isThoiGian: boolean; // true: Xem theo thời gian      false: Không xem theo thời gian
}
export interface IGridDanhSachChiTietPhieuDuTruHoanTraState {
  listDanhSachChiTietPhieuDuTruHoanTra: any;
}

export class GridDanhSachChiTietPhieuDuTruHoanTra extends React.Component<IGridDanhSachChiTietPhieuDuTruHoanTraProp, IGridDanhSachChiTietPhieuDuTruHoanTraState> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <DataGrid
        loadPanel={configLoadPanel}
        dataSource={this.props.listDanhSachChiTietPhieuDuTruHoanTra}
        showBorders={true}
        showColumnLines={true}
        rowAlternationEnabled={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        columnMinWidth={50}
        noDataText={'Không có dữ liệu'}
        wordWrapEnabled={true}
        {...this.props}
      >
        <Selection mode="single" />
        <FilterRow visible={true}
                   applyFilter={'auto'} />
        <HeaderFilter visible={true} />
        <Grouping autoExpandAll={true} />
        <Column
          dataField={"MAVATTU"}
          caption="Mã vật tư"
          dataType="string"
          alignment="left"
          visible={false}
        />
        <Column
          dataField={"NGAY_RA_TOA"}
          caption=""
          dataType="string"
          alignment="left"
          visible={false}
          groupIndex={this.props.isThoiGian ? 0 : undefined}
        />
        <Column
          dataField={"TEN_VAT_TU"}
          caption="Tên vật tư"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField={"HOAT_CHAT"}
          caption="Hoạt chất"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField={"HAM_LUONG"}
          caption="Hàm lượng"
          dataType="string"
          alignment="left"
          visible={this.props.loaiPhieu === 0 ? true : false}
        />
        <Column
          dataField={"DVT"}
          caption="Đơn vị tính"
          dataType="string"
          alignment="left"
        />
        <Column
          dataField={"SO_LUONG"}
          caption="Số lượng"
          dataType="string"
          alignment="left"
          format={numberFormat}
        />
        <Column
          dataField={"DONGIA_BAN_BV"}
          caption="Đơn giá"
          dataType="string"
          alignment="left"
          format={numberFormat}
        />
        <Column
          dataField={"THANHTIEN_THUOC"}
          caption="Thành tiền"
          dataType="string"
          alignment="left"
          format={numberFormat}
        />
        <Summary>
          <TotalItem
            displayFormat={'{0} loại vật tư'}
            showInColumn={'MAVATTU'}
            column={'TEN_VAT_TU'}
            summaryType="count"
          />
        </Summary>
        <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
        <Paging defaultPageSize={5} />
      </DataGrid>);
  }
}

export default GridDanhSachChiTietPhieuDuTruHoanTra;
