import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection, ColumnFixing, Pager, Paging
} from 'devextreme-react/data-grid';
import {
  convertDateFormat,
  convertToDate, simpleIf
} from "app/modules/duoc/duoc-components/utils/funtions";
import {configLoadPanel, pageSizes} from "app/modules/duoc/configs";

export interface IGridDanhSachPhieuChuyenKhoDuyetProp extends IDataGridOptions {
  listDanhSachPhieuChuyenKhoDuyet: any;
  handleOnContextMenuGridDanhSach: any;
  // isNhan?: boolean;
  loaiHienThi?: number; // 0: duyệt phiếu       1: Nhận dược        2: Kiểm tra duyệt nhận
}
export interface IGridDanhSachPhieuChuyenKhoDuyetState {
  listDanhSachPhieuChuyenKhoDuyet: any;
}

export class GridDanhSachPhieuChuyenKhoDuyet extends React.Component<IGridDanhSachPhieuChuyenKhoDuyetProp, IGridDanhSachPhieuChuyenKhoDuyetState> {
  constructor(props) {
    super(props);
  }

  // shouldComponentUpdate(nextProps: Readonly<IGridDanhSachPhieuChuyenKhoDuyetProp>, nextState: Readonly<IGridDanhSachPhieuChuyenKhoDuyetState>, nextContext: any): boolean {
  //   if (this.props.listDanhSachPhieuChuyenKhoDuyet === nextProps.listDanhSachPhieuChuyenKhoDuyet) {
  //     return false;
  //   }
  //   return true;
  // }

  render() {
    return (
      <DataGrid
        loadPanel={configLoadPanel}
        dataSource={this.props.listDanhSachPhieuChuyenKhoDuyet}
        showBorders={true}
        showColumnLines={true}
        rowAlternationEnabled={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnResizingMode={'nextColumn'}
        columnMinWidth={50}
        onContextMenuPreparing={this.props.handleOnContextMenuGridDanhSach}
        noDataText={'Không có dữ liệu'}
        width={'100%'}
        wordWrapEnabled={true}
        {...this.props}
      >
        <Selection mode="single" />
        <FilterRow visible={true}
                   applyFilter={'20%'} />
        <HeaderFilter visible={true} />
        <Column
          dataField="ID_CHUYEN_KHO_VAT_TU"
          caption="ID chuyển kho vật tư"
          dataType="string"
          alignment="left"
          width={'0%'}
          visible={false}
        />
        <Column
          dataField={"NGAYCHUYEN"}
          caption={"Ngày chuyển"}
          dataType="string"
          alignment="left"
          width={simpleIf(this.props.loaiHienThi === 1,'15%',this.props.loaiHienThi === 0 ? '10%' : '20%')}
          customizeText={(data) => {
            return convertDateFormat(data.value);
          }}
          visible={this.props.loaiHienThi !== 1}
        />
        <Column
          dataField={"NGAYNHAN"}
          caption={"Ngày nhận"}
          dataType="string"
          alignment="left"
          width={simpleIf(this.props.loaiHienThi !== 1,'15%',this.props.loaiHienThi === 0 ? '10%' : '20%')}
          // width={this.props.loaiHienThi !== 1 ? '15%' : '10%'}
          customizeText={(data) => {
            return data.value && convertToDate(data.value);
          }}
          visible={this.props.loaiHienThi === 1}
        />
        <Column
          dataField="NGHIEP_VU"
          caption="Nghiệp vụ"
          dataType="string"
          alignment="left"
          width={simpleIf(this.props.loaiHienThi === 1,'15%',this.props.loaiHienThi === 0 ? '15%' : '20%')}
          // width={ this.props.isNhan ? '15%' : '15%'}
        />
        <Column
          dataField="TENKHOYC"
          caption="Kho yêu cầu"
          dataType="string"
          alignment="left"
          width={simpleIf(this.props.loaiHienThi === 1,'20%',this.props.loaiHienThi === 0 ? '12%' : '20%')}
          // width={ this.props.isNhan ? '20%' : '12%'}
        />
        <Column
          dataField="TENKHOCHUYEN"
          caption="Kho chuyển"
          dataType="string"
          alignment="left"
          width={simpleIf(this.props.loaiHienThi === 1,'20%',this.props.loaiHienThi === 0 ? '12%' : '20%')}
          // width={this.props.isNhan ? '20%' : '12%'}
        />
        <Column
          dataField="SOPHIEUCHUYEN"
          caption="Số phiếu"
          dataType="string"
          alignment="left"
          width={simpleIf(this.props.loaiHienThi === 1,'15%',this.props.loaiHienThi === 0 ? '11%' : '20%')}
          // width={this.props.isNhan ? '15%' : '11%'}
        />
        <Column
          dataField="NGAYDUYET"
          caption="Ngày duyệt"
          dataType="string"
          alignment="left"
          // width={ this.props.isNhan ? '15%' : '10%'}
          width={simpleIf(this.props.loaiHienThi === 1,'15%',this.props.loaiHienThi === 0 ? '10%' : '20%')}
          customizeText={(data) => {
            return convertDateFormat(data.value);
          }}
          visible={this.props.loaiHienThi === 0}
        />
        <Column
          dataField="TEN_NGUOI_DUYET"
          caption="Người duyệt"
          dataType="string"
          alignment="left"
          // width={this.props.isNhan ? '15%' : '10%'}
          width={simpleIf(this.props.loaiHienThi === 1,'15%',this.props.loaiHienThi === 0 ? '10%' : '20%')}
          visible={this.props.loaiHienThi === 0}
        />
        <Column
          dataField="SOLUUTRU"
          caption="Số lưu trữ"
          dataType="string"
          alignment="left"
          // width={this.props.isNhan ? '15%' : '10%'}
          width={simpleIf(this.props.loaiHienThi === 1,'15%',this.props.loaiHienThi === 0 ? '10%' : '20%')}
          visible={this.props.loaiHienThi === 0}
        />
        <Column
          dataField="GHICHU"
          caption="Ghi chú"
          dataType="string"
          alignment="left"
          // width={this.props.isNhan ? '15%' : '10%'}
          width={simpleIf(this.props.loaiHienThi === 1,'15%',this.props.loaiHienThi === 0 ? '10%' : '20%')}
          visible={this.props.loaiHienThi === 0}
        />
        <Summary>
          <TotalItem
            displayFormat={'{0} phiếu'}
            showInColumn={'NGAYCHUYEN'}
            column="SOPHIEUCHUYEN"
            summaryType="count"
          />
        </Summary>
        <Pager allowedPageSizes={pageSizes} showPageSizeSelector={true} showNavigationButtons={true}/>
        <Paging defaultPageSize={10} />
      </DataGrid>);
  }
}

export default GridDanhSachPhieuChuyenKhoDuyet;
