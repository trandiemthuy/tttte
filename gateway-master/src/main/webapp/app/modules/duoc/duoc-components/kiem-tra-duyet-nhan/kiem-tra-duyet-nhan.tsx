import React from 'react';
import {RouteComponentProps} from "react-router";
import {IRootState} from "app/shared/reducers";
import {connect} from "react-redux";
import TabPanel from 'devextreme-react/tab-panel';
import {CheckBox} from 'devextreme-react';
import {
  getDanhSachKiemTraDuyetNhan
} from './kiem-tra-duyet-nhan.reducer';
import {
  getDanhSachChiTietChuyenKho,
} from '../chuyen-kho/chuyen-kho.reducer'
import GridDanhSachPhieuChuyenKhoDuyet from '../duyet-phieu/components/grid-danh-sach-phieu-chuyen-kho-duyet/grid-danh-sach-phieu-chuyen-kho-duyet'

import {
  getDvtt
} from "app/modules/duoc/duoc-components/utils/thong-tin";
import PopUpChiTietPhieuDuyet
  from "app/modules/duoc/duoc-components/duyet-phieu/components/popup-chi-tiet-phieu/popup-chi-tiet-phieu";

export interface IDuyetPhieuProp extends StateProps, DispatchProps, RouteComponentProps<{ nghiepVu: string }> {
  account: any;
}

export interface IDuyetPhieuState {
  trangthai: number; // 0: Chưa duyệt     1: Chưa nhận
  selectedPhieuChuyen: any;
  showPopUp: boolean;
}

export class KiemTraDuyetNhanComponent extends React.Component<IDuyetPhieuProp, IDuyetPhieuState> {
  constructor(props) {
    super(props);
    this.state = {
     trangthai: 0,
      selectedPhieuChuyen: {},
      showPopUp: false
    }
  }

  componentDidMount(): void {
    this.getDanhSachKiemTraDuyetNhan();
  }

  // Handles
  handleOnChangeTrangThai = (e, name) => {
    if (name === 'chuaduyet') {
      this.setState({
        trangthai: e.value ? 0 : 1
      });
    } else {
      this.setState({
        trangthai: e.value ? 1 : 0
      });
    }
    this.getDanhSachKiemTraDuyetNhan();
  };
  handleShowPopUp = (showPopUp: boolean) => {
    this.setState({
      showPopUp
    });
  };
  handleOnContextMenuGridDanhSach = (e) => {
    if (e.row.data === undefined) {
      return;
    }
    this.setState({
      selectedPhieuChuyen: e.row.data
    });
    if (e.row.rowType === 'data') {
      e.items = [{
        icon: 'bulletlist',
        text: "Xem chi tiết phiếu chuyển kho",
        onItemClick: () => {
          this.handleOnClickMenuXemChiTiet(this.state.selectedPhieuChuyen.ID_CHUYEN_KHO_VAT_TU, getDvtt());
        }
      }];
    }
  };
  handleOnClickMenuXemChiTiet = (idPhieuChuyen: number, dvtt: string) => {
    this.props.getDanhSachChiTietChuyenKho(idPhieuChuyen, dvtt);
    this.handleShowPopUp(true);
  };

  // Funtion
  getDanhSachKiemTraDuyetNhan = () => {
    this.props.getDanhSachKiemTraDuyetNhan(getDvtt(), this.state.trangthai);
  };

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (<div>
      <div className={'div-button'}>
        <CheckBox
          text={'Chưa duyệt'}
          onValueChanged={e => this.handleOnChangeTrangThai(e, 'chuaduyet')}
          value={this.state.trangthai === 0}
        />
        <CheckBox
          className={'row-padding-left-5'}
          text={'Chưa nhận'}
          onValueChanged={e => this.handleOnChangeTrangThai(e, 'chuanhan')}
          value={this.state.trangthai === 1}
        />
      </div>
      <div className={'row-padding-top-5'}>
        <GridDanhSachPhieuChuyenKhoDuyet
          listDanhSachPhieuChuyenKhoDuyet={this.props.listDanhSachKiemTraDuyetNhan}
          handleOnContextMenuGridDanhSach={this.handleOnContextMenuGridDanhSach}
          // isNhan={false}
          selectedRowKeys={[this.state.selectedPhieuChuyen]}
          loaiHienThi={2}
        />
      </div>
      <div>
        <PopUpChiTietPhieuDuyet
          listDanhSachChiTietPhieuDuyet = {this.props.listDanhSachChiTietChuyenKho}
          handleChangePopUpChiTietState={this.handleShowPopUp}
          showPopUp={this.state.showPopUp}
          title={'Chi tiết phiếu ' + (this.state.trangthai === 0 ? 'chưa duyệt' : 'chưa nhận')}
          selectedPhieuChuyen={this.state.selectedPhieuChuyen}
          isNhan={true}
        />
      </div>
    </div>);
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  listDanhSachKiemTraDuyetNhan: storeState.kiemtraduyetnhan.listDanhSachKiemTraDuyetNhan,
  listDanhSachChiTietChuyenKho: storeState.chuyenkho.listDanhSachChiTietChuyenKho,
});

const mapDispatchToProps = {
  getDanhSachKiemTraDuyetNhan,
  getDanhSachChiTietChuyenKho
};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(KiemTraDuyetNhanComponent);
