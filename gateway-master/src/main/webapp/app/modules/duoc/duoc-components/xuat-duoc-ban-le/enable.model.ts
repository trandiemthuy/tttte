export interface IEnableXuatDuocBanLe {
  btnThem?: boolean;
  btnSua?: boolean;
  btnLuu?: boolean;
  btnHuy?: boolean;
  btnXoa?: boolean;
  btnIn?: boolean;
  btnXuatDuoc?: boolean;
  btnHuyXuat?: boolean;
  btnThanhToan?: boolean;
  itemForm: boolean;
}
export const defaultEnableXuatDuocBanLe = {
  btnThem: true,
  btnSua: false,
  btnLuu: false,
  btnHuy: false,
  btnXoa: false,
  btnIn: false,
  btnXuatDuoc: false,
  btnHuyXuat: false,
  btnThanhToan: false,
  itemForm: true
};
