import axios from 'axios';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING } from '../../configs';
import notify from 'devextreme/ui/notify';
import { IPhieuChuyenKho } from 'app/modules/duoc/duoc-components/chuyen-kho/chuyen-kho.model';
import { getDanhSachPhieuchuyenKho } from 'app/modules/duoc/duoc-components/chuyen-kho/chuyen-kho.reducer';
import { getDvtt, getMaNhanVien, getMaPhongBan, getTenBenhVien, getTenTinh } from 'app/modules/duoc/duoc-components/utils/thong-tin';
import { exportXLSType1, printType1 } from 'app/modules/duoc/duoc-components/utils/print-helper';

export const ACTION_TYPES = {
  GET_DANH_SACH_PHIEU_DU_TRU_HOAN_TRA: 'duyetDuTruHoanTra/GET_DANH_SACH_PHIEU_DU_TRU_HOAN_TRA',
  GET_DANH_SACH_CHI_TIET_DU_TRU_HOAN_TRA: 'duyetDuTruHoanTra/GET_DANH_SACH_CHI_TIET_DU_TRU_HOAN_TRA',
  GET_DANH_SACH_BAN_IN_DUTRU_HOANTRA: 'duyetDuTruHoanTra/GET_DANH_SACH_BAN_IN_DUTRU_HOANTRA',
  DUYET_PHIEU_DU_TRU_HOAN_TRA: 'duyetDuTruHoanTra/DUYET_PHIEU_DU_TRU_HOAN_TRA',
  HUY_DUYET_PHIEU_DU_TRU_HOAN_TRA: 'duyetDuTruHoanTra/HUY_PHIEU_DU_TRU_HOAN_TRA'
};

const initialState = {
  listDanhSachPhieuDuTruHoanTraDuyet: [],
  listDanhSachChiTietPhieuDuTruHoanTraDuyet: [],
  listDanhSachBanInPhieuDuTruHoanTraDuyet: []
};

export type duyetDuTruHoanTraState = Readonly<typeof initialState>;

// Reducer
// eslint-disable-next-line complexity
export default (state: duyetDuTruHoanTraState = initialState, action): duyetDuTruHoanTraState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_PHIEU_DU_TRU_HOAN_TRA):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_CHI_TIET_DU_TRU_HOAN_TRA):
      return {
        ...state
      };
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_BAN_IN_DUTRU_HOANTRA):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_PHIEU_DU_TRU_HOAN_TRA):
      return {
        ...state,
        listDanhSachPhieuDuTruHoanTraDuyet: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_CHI_TIET_DU_TRU_HOAN_TRA):
      return {
        ...state,
        listDanhSachChiTietPhieuDuTruHoanTraDuyet: []
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_BAN_IN_DUTRU_HOANTRA):
      return {
        ...state,
        listDanhSachBanInPhieuDuTruHoanTraDuyet: []
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_PHIEU_DU_TRU_HOAN_TRA):
      return {
        ...state,
        listDanhSachPhieuDuTruHoanTraDuyet: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_CHI_TIET_DU_TRU_HOAN_TRA):
      return {
        ...state,
        listDanhSachChiTietPhieuDuTruHoanTraDuyet: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_BAN_IN_DUTRU_HOANTRA):
      return {
        ...state,
        listDanhSachBanInPhieuDuTruHoanTraDuyet: action.payload.data
      };
    default:
      return state;
  }
};
// Actions
export const getDanhSachPhieuDuTruHoanTraDuyet = (
  dvtt: string,
  tuNgay: string,
  denNgay: string,
  trangThai: number,
  isBANT: number,
  maPhongBan: string,
  maNhanVien: number,
  loaiPhieu: number
) => async dispatch => {
  // loaiPhieu: 0: Dự trù       1: Hoàn trả
  const rawURL = `${apiURL}api/duyetdutruhoantra/`;
  let desURL = '';
  let mess = '';
  if (loaiPhieu === 0) {
    desURL = 'getDanhSachPhieuDuTruNoiTru';
    mess = 'phiếu dự trù nội trú';
  } else {
    desURL = 'getDanhSachPhieuHoanTraNoiTru';
    mess = 'phiếu hoàn trả nội trú';
  }
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_PHIEU_DU_TRU_HOAN_TRA,
    payload: axios
      .get(rawURL + desURL, {
        params: {
          dvtt,
          tuNgay,
          denNgay,
          trangThai,
          isBANT,
          maPhongBan,
          maNhanVien
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi tải danh sách ' + mess, width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  return result;
};

export const getDanhSachChiTietPhieuDutru = (
  dvtt: string,
  hinhThucXem: number,
  idPhieuDuTruTong: number,
  idPhieuHoanTraTong: number,
  maPhongBan: string,
  thoiGian: number,
  loaiPhieu: number
) => async dispatch => {
  const rawURL = `${apiURL}api/duyetdutruhoantra/`;
  let desURL = '';
  let mess = '';
  let params = {};
  if (loaiPhieu === 0) {
    params = {
      dvtt,
      hinhThucXem,
      idPhieuDuTruTong,
      maPhongBan,
      thoiGian
    };
    desURL = 'getDanhSachChiTietPhieuDutru';
    mess = 'phiếu dự trù nội trú';
  } else {
    params = {
      idPhieuHoanTraTong,
      hinhThucXem
    };
    desURL = 'getDanhSachChiTietPhieuHoanTra';
    mess = 'phiếu hoàn trả nội trú';
  }
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_CHI_TIET_DU_TRU_HOAN_TRA,
    payload: axios
      .get(rawURL + desURL, {
        params
      })
      .catch(obj => {
        notify({ message: 'Lỗi tải danh sách chi tiết ' + mess, width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  return result;
};
export const getDanhSachBanInDuTruHoanTra = (
  dvtt: string,
  idPhieuTong: number,
  tuTuThuoc: number,
  tachLoai: number,
  loaiPhieu: number
) => async dispatch => {
  const rawURL = `${apiURL}api/duyetdutruhoantra/`;
  let desURL = '';
  let mess = '';
  let params = {};
  if (loaiPhieu === 0) {
    params = {
      dvtt,
      idPhieuDuTruTong: idPhieuTong,
      tuTuThuoc,
      tachLoai
    };
    desURL = 'getDanhSachBanInPhieuDuTru';
    mess = 'phiếu dự trù nội trú';
  } else {
    params = {
      dvtt,
      idPhieuHoanTraTong: idPhieuTong,
      tuTuThuoc,
      tachLoai
    };
    desURL = 'getDanhSachBanInPhieuHoanTra';
    mess = 'phiếu hoàn trả nội trú';
  }
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_BAN_IN_DUTRU_HOANTRA,
    payload: axios
      .get(rawURL + desURL, {
        params
      })
      .catch(obj => {
        notify({ message: 'Lỗi tải danh sách bản in ' + mess, width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  return result;
};
export const printPhieuDuTruNoiTru = (
  dvtt: string,
  idPhieuDuTruTong: number,
  soPhieuLuuTru: string,
  ngayLap: string,
  thanhTien: string,
  maKhoVatTu: string,
  tenKhoVatTu: string,
  tenKhoa: string,
  tenPhongBanDuTru: string,
  maLoaiVatTu: string,
  tuTuThuoc: number,
  theoNgay: number,
  tachLoai: number,
  maNguoiIn: number,
  nguoiLap: string,
  tenBenhVien: string,
  tenTinh: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/duyetdutruhoantra/printPhieuDuTruNoiTru`;
  const params = {
    dvtt,
    idPhieuDuTruTong,
    soPhieuLuuTru,
    ngayLap,
    thanhTien,
    maKhoVatTu,
    tenKhoVatTu,
    tenKhoa,
    tenPhongBanDuTru,
    maLoaiVatTu,
    tuTuThuoc,
    theoNgay,
    tachLoai,
    maNguoiIn,
    nguoiLap,
    tenBenhVien,
    tenTinh,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In phiếu dự trù thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const printPhieuXuatKhoDuTruNoiTru = (
  dvtt: string,
  idPhieuDuTruTong: string,
  soPhieuLuuTru: string,
  ngayLap: string,
  thanhTien: string,
  maKhoVatTu: string,
  tenKhoVatTu: string,
  tenKhoa: string,
  tenPhongBanDuTru: string,
  maLoaiVatTu: string,
  nguoiLap: string,
  tenBenhVien: string,
  tenTinh: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/duyetdutruhoantra/printPhieuXuatKhoDuTruNoiTru`;
  const params = {
    dvtt,
    idPhieuDuTruTong,
    soPhieuLuuTru,
    ngayLap,
    thanhTien,
    maKhoVatTu,
    tenKhoVatTu,
    tenKhoa,
    tenPhongBanDuTru,
    maLoaiVatTu,
    nguoiLap,
    tenBenhVien,
    tenTinh,
    loaiFile,
    handleLoading
  };
  printType1(url, params, {
    successMessage: 'In phiếu xuất kho dự trù thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const printSoYLenhDuTruNoiTru = (
  idPhieuDuTruTong: number,
  soPhieuTaoLuuTru: string,
  dvtt: string,
  tuTuThuoc: number,
  mauBaoCao: number,
  maKhoa: string,
  tenKhoa: string,
  tenPhongBanDuTru: string,
  tuNgay: string,
  denNgay: string,
  maPhong: string,
  tenTinh: string,
  tenBenhVien: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/duyetdutruhoantra/printSoYLenhDuTruNoiTru`;
  const params = {
    idPhieuDuTruTong,
    soPhieuTaoLuuTru,
    dvtt,
    tuTuThuoc,
    mauBaoCao,
    maKhoa,
    tenKhoa,
    tenPhongBanDuTru,
    tuNgay,
    denNgay,
    maPhong,
    tenTinh,
    tenBenhVien
  };
  exportXLSType1(url, params, {
    successMessage: 'Xuất sổ y lệnh thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const printDanhSachToaThuoc = (
  idPhieuDuTruTong: string,
  soPhieuTaoLuuTru: string,
  dvtt: string,
  maKhoa: string,
  tuNgay: string,
  denNgay: string,
  tenBenhVien: string,
  tenTinh: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/duyetdutruhoantra/printDanhSachToaThuoc`;
  const params = {
    idPhieuDuTruTong,
    soPhieuTaoLuuTru,
    dvtt,
    maKhoa,
    tuNgay,
    denNgay,
    tenBenhVien,
    tenTinh,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In danh sách toa thuốc thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const printPhieuHoanTraNoiTru = (
  dvtt: string,
  idPhieuHoanTraTong: number,
  soPhieuLuuTru: string,
  ngayLap: string,
  thanhTien: string,
  maKhoVatTu: string,
  tenKhoVatTu: string,
  tenKhoa: string,
  tenPhongBanHoanTra: string,
  maLoaiVatTu: string,
  tuTuThuoc: number,
  tachLoai: number,
  maNguoiIn: number,
  nguoiLap: string,
  tenBenhVien: string,
  tenTinh: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/duyetdutruhoantra/printPhieuHoanTraNoiTru`;
  const params = {
    dvtt,
    idPhieuHoanTraTong,
    soPhieuLuuTru,
    ngayLap,
    thanhTien,
    maKhoVatTu,
    tenKhoVatTu,
    tenKhoa,
    tenPhongBanHoanTra,
    maLoaiVatTu,
    tuTuThuoc,
    tachLoai,
    maNguoiIn,
    nguoiLap,
    tenBenhVien,
    tenTinh,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In phiếu hoàn trả thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const printPhieuXuatKhoHoanTraNoiTru = (
  idPhieuHoanTraTong: string,
  dvtt: string,
  soPhieuLuuTru: string,
  ngayLap: string,
  thanhTien: string,
  maKhoVatTu: string,
  tenKhoVatTu: string,
  tenKhoa: string,
  tenPhongBanDuTru: string,
  maLoaiVatTu: string,
  tuTuThuoc: number,
  nguoiLap: string,
  tenBenhVien: string,
  tenTinh: string,
  loaiFile: string,
  handleLoading: Function
) => {
  const url = `${apiURL}api/duyetdutruhoantra/printPhieuXuatKhoHoanTraNoiTru`;
  const params = {
    idPhieuHoanTraTong,
    dvtt,
    soPhieuLuuTru,
    nguoiLap,
    ngayLap,
    thanhTien,
    maKhoVatTu,
    tenKhoVatTu,
    tenKhoa,
    tenPhongBanDuTru,
    maLoaiVatTu,
    tuTuThuoc,
    tenBenhVien,
    tenTinh,
    loaiFile
  };
  printType1(url, params, {
    successMessage: 'In phiếu xuất kho hoàn trả thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const exportExcelDuTruHoanTraDuyet = (
  tuNgay: string,
  denNgay: string,
  dvtt: string,
  duyet: number,
  isBANT: string,
  loaiPhieu: number,
  handleLoading: Function
) => {
  const rawURL = `${apiURL}api/duyetdutruhoantra/`;
  const urlDes = loaiPhieu === 0 ? 'exportExcelDuyetPhieuDuTru' : 'exportExcelDuyetPhieuHoanTra';
  const params = {
    tuNgay,
    denNgay,
    dvtt,
    duyet,
    isBANT
  };
  exportXLSType1(rawURL + urlDes, params, {
    successMessage: 'Xuất Excel thành công',
    errorMessage: 'Có lỗi xảy ra. Thử lại sau',
    handleLoading
  });
};

export const duyetDuTruHoanTra = (
  listDanhSachPhieu: any,
  dvtt: string,
  duyet: number,
  nguoiDuyet: number,
  ngayDuyet,
  maPhongBan: string,
  maPhongBenh: string,
  loaiPhieu: number,
  handleLamMoi: Function
) => async dispatch => {
  const rawURL = `${apiURL}api/duyetdutruhoantra/`;
  const urlDes = loaiPhieu === 0 ? 'duyetPhieuDuTru' : 'duyetPhieuHoanTra';
  const reuslt = await dispatch({
    type: ACTION_TYPES.DUYET_PHIEU_DU_TRU_HOAN_TRA,
    payload: axios
      .post(rawURL + urlDes, JSON.stringify(listDanhSachPhieu), {
        headers: { 'content-type': 'application/json' },
        params: { dvtt, duyet, nguoiDuyet, ngayDuyet, maPhongBan, maPhongBenh }
      })
      .then(rs => {
        if (rs.data === 0) {
          notify(
            { message: (duyet === 1 ? 'Duyệt' : 'Hủy duyệt') + ' phiếu thành công!', width: NOTIFY_WIDTH, shading: SHADING },
            'success',
            NOTIFY_DISPLAYTIME
          );
        } else if (rs.data === 100) {
          notify({ message: 'Số liệu đã chốt. Không để chỉnh sửa', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else if (rs.data === '-1') {
          notify({ message: 'Có lỗi xảy ra. Thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        } else {
          notify(
            {
              message:
                'Phiếu ' +
                rs.data.substring(1, rs.data.length - 1) +
                ' đã cập nhật phiếu sai/hoàn trả hoặc ngày duyệt nhỏ hơn ngày tạo phiếu. Không thể Duyệt/Hủy duyệt!',
              width: NOTIFY_WIDTH,
              shading: SHADING
            },
            'error',
            NOTIFY_DISPLAYTIME
          );
        }
        handleLamMoi();
      })
      .catch(obj => {
        notify({ message: 'Lỗi duyệt phiếu', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  return reuslt;
};

export const huyduyetDuTruHoanTraChuyenKho = (
  idPhieuChuyen,
  dvtt: string,
  nguoiDuyet: number,
  maPhongBan: string,
  maKhoa: string,
  tuNgay: string,
  denNgay: string,
  maKho: number,
  maNghiepVu: number,
  trangThaiPhieu: number
) => async dispatch => {
  const requestUrl = `${apiURL}api/duyetDuTruHoanTra/huyDuyetChuyenKho`;
  const params = { idPhieuChuyen, dvtt, nguoiDuyet, maPhongBan, maKhoa };
  const reuslt = await dispatch({
    type: ACTION_TYPES.HUY_DUYET_PHIEU_DU_TRU_HOAN_TRA,
    payload: axios
      .post(requestUrl, JSON.stringify(params), { headers: { 'content-type': 'application/json' }, params })
      .then(rs => {
        if (rs.data === 0) {
          notify({ message: 'Hủy phiếu thành công!', width: NOTIFY_WIDTH, shading: SHADING }, 'success', NOTIFY_DISPLAYTIME);
        } else {
          notify({ message: 'Có lỗi xảy ra. Vui lòng thử lại sau', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
        }
      })
      .catch(obj => {
        notify({ message: 'Lỗi hủy duyệt phiếu chuyển kho', width: NOTIFY_WIDTH, shading: SHADING }, 'error', NOTIFY_DISPLAYTIME);
      })
  });
  // dispatch(getDanhSachPhieuChuyenKho(dvtt, tuNgay, denNgay, maNghiepVu, maKho, trangThaiPhieu));
  return reuslt;
};
