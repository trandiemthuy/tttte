import React, {ReactDOM} from "react";
import { RouteComponentProps } from 'react-router-dom';
import { GridDanhSachPhieuNhapKho } from './components/grid-danh-sach-phieu-nhap-kho/grid-danh-sach-phieu-nhap';
import { GridChiTietNhapKho } from './components/grid-chi-tiet-nhap-kho/grid-chi-tiet-nhap-kho';
import { GridChiTietHoaDon } from './components/grid-chi-tiet-hoa-don/grid-chi-tiet-hoa-don';
import { GridNhanVienKiemNhap } from './components/grid-nhan-vien-kiem-nhap/grid-nhan-vien-kiem-nhap';
import { DuocDropDownButton } from '../utils/button-dropdown/button-dropdown';
import {defaultPhieuNhap, IPhieuNhapKho, defaultNhanVienKiemNhap, INhanVienKiemNhap, IChiTietPhieuNhapKho, defaultChiTietPhieuNhap} from './nhap-kho.model';
import {IRootState} from "app/shared/reducers";
import {connect} from "react-redux";
import './nhap-kho.scss';
import {apiURL, NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING} from '../../configs';
import {
  getDanhSachPhieuNhapKho,
  getDanhSachNguonDuoc,
  getDanhSachNhaCungCap,
  getDanhSachKhoNhap,
  getDanhSachThamSoDonVi,
  getDanhSachVatTu,
  getDanhSachNguoiNhan,
  getDanhSachHoiDongKiemNhap,
  addPhieuNhapKho,
  updatePhieuNhapKho,
  deletePhieuNhapKho,
  getDanhSachChiTietHoaDon,
  getDanhSachChiTietNhapKho,
  getDanhSachNhanVienKiemNhap,
  addNhanVienKiemNhap,
  deleteNhanVienKiemNhap,
  deleteChiTietHoaDon,
  deleteChiTietNhapKho,
  deleteNhapKho,
  resetChiTietHoaDon,
  resetChiTietNhapKho,
  addChiTietHoaDon,
  addChiTietNhapKho,
  printPhieuNhapKhoHoaDon,
  exportDanhMucVatTuMau,
  printBienBanKiemNhap,
  printBienBanKiemNhapNhieuPhieu
} from './nhap-kho.reducer';
import {
  convertToSQLDate,
  convertToDate,
  convertToDate1,
  getMoTaThamSo,
  getKYearFromNow,
  convertToSQLDate1, getMoTaThamSoDuocMoi
} from "../utils/funtions";
import { Tabs, DateBox, Button, TextBox, DropDownBox, DataGrid, NumberBox, Validator } from 'devextreme-react';
import Form, { Item, SimpleItem, Label, RequiredRule, EmptyItem, ButtonItem, ButtonOptions, GroupItem, CompareRule } from 'devextreme-react/form';
import Box, {Item as BoxItem} from "devextreme-react/box";
import DropDownVatTu from "app/modules/duoc/duoc-components/nhap-kho/components/dropdown-vattu";
import notify from "devextreme/ui/notify";
import dialog from "devextreme/ui/dialog";
import {printType1, exportXLSType1} from '../utils/print-helper';
import {TuNgayDenNgayDocComponent} from "app/modules/duoc/duoc-components/utils/tungay-denngay/tungay-denngay-doc";
import {
  getDvtt,
  getMaNhanVien,
  getMaPhongBan,
  getMaPhongBenh, getTenBenhVien, getTenTinh
} from "app/modules/duoc/duoc-components/utils/thong-tin";
import FormPhieuNhapKho
  from "app/modules/duoc/duoc-components/nhap-kho/components/form-phieu-nhap-kho/form-phieu-nhap-kho";
import {IPhieuChuyenKho} from "app/modules/duoc/duoc-components/chuyen-kho/chuyen-kho.model";
import GridGoQuyCach
  from "app/modules/duoc/duoc-components/nhap-kho/components/grid-chi-tiet-hoa-don/grid-go-quy-cach";
import DuocLabelComponent from "app/modules/duoc/duoc-components/utils/duoc-label/duoc-label";
import DuocTitleComponent from "app/modules/duoc/duoc-components/utils/duoc-title/duoc-title";

export interface INhapKhoTuNhaCungCapProp extends StateProps, DispatchProps, RouteComponentProps<{ nghiepVu: string }> {
  account: any;
}
export interface ISetEnable {
  btnThem: boolean;
  btnSua: boolean;
  btnLuu: boolean;
  btnHuy: boolean;
  btnXoa: boolean;
  formPhieuNhap: boolean;
}
export const defaultSetEnable: ISetEnable = {
  btnThem: true,
  btnSua: false,
  btnLuu: false,
  btnHuy: false,
  btnXoa: false,
  formPhieuNhap: true
};
export interface INhapKhoTuNhaCungCapState {
  tuNgay: string;
  denNgay: string;
  phieuNhapKhoObj: IPhieuNhapKho
  curTab: number;
  enable: ISetEnable;
  nhanVienKiemNhapObj: INhanVienKiemNhap;
  chiTietPhieuNhapObj: IChiTietPhieuNhapKho;
  selectedRowKeysGridDanhSachPhieuNhapKho: any;
  loadingConfigs: {
    visible: boolean;
    position: string;
  };
  sendPhieuNhapKho: {
    phieuNhapKho: IPhieuNhapKho;
    isUpdated: number;
  },
  enableInput: boolean;
}

// Khung gõ chi tiết nhập kho thường (gõ phiếu), đơn vị %
const mdctCot1 = 20;
const mdctCot2 = 10;
const mdctCot3 = 5;
const mdctCot4 = 5;
const mdctCot5 = 10;
const mdctCot6 = 5;
const mdctCot7 = 10;
const mdctCot8 = 10;
const mdctCot9 = 10;
const mdctCot10 = 10;
const mdctCot11 = 5;

// Khung gõ chi tiết nhập kho QC/Đông y (gõ phiếu), đơn vị %
const mdctCot1NV23 = 10;
const mdctCot2NV23 = 3;
const mdctCot3NV23 = 4;
const mdctCot4NV23 = 3;
const mdctCot5NV23 = 5;
const mdctCot6NV23 = 4;
const mdctCot7NV23 = 4;
const mdctCot8NV23 = 5;
const mdctCot9NV23 = 6;
const mdctCot10NV23 = 6;
const mdctCot11NV23 = 7;
const mdctCot12NV23 = 7;
const mdctCot13NV23 = 7;
const mdctCot14NV23 = 4;
const mdctCot15NV23 = 5;
const mdctCot16NV23 = 5;

export class NhapKhoComponent extends React.Component<INhapKhoTuNhaCungCapProp, INhapKhoTuNhaCungCapState> {
  private refTuNgayDenNgay = React.createRef<TuNgayDenNgayDocComponent>();
  // Ref nghiệp vụ nhập kho thường/Đông y
  private tenVatTuDropDownBox = React.createRef<DropDownVatTu>();
  private hoatChatTextBox = React.createRef<TextBox>();
  private dvtTextBox = React.createRef<TextBox>();
  private soThauTextBox = React.createRef<TextBox>();
  private soLuongThucNumberBox = React.createRef<NumberBox>();
  private soLoTextBox = React.createRef<TextBox>();
  private ngayHetHanDateBox = React.createRef<DateBox>();
  private donGiaTruocVATNumberBox = React.createRef<NumberBox>();
  private donGiaVATNumberBox = React.createRef<NumberBox>();
  private thanhTienNumberBox = React.createRef<NumberBox>();
  private ghiChuTextBox = React.createRef<TextBox>();

  // Ref nghiệp vụ nhập kho QC
  private tenVatTuDropDownBoxNV23 = React.createRef<DropDownVatTu>();
  private quyCachTextBoxNV23 = React.createRef<TextBox>();
  private soLuongNhapNumberBoxNV23 = React.createRef<NumberBox>();
  private soLoTextBoxNV23 = React.createRef<TextBox>();
  private ngayHetHanDateBoxNV23 = React.createRef<DateBox>();
  private donGiaNumberBoxNV23 = React.createRef<NumberBox>();
  private donGiaNhapKhoNumberBoxNV23 = React.createRef<NumberBox>();
  private phanTramHaoHutNumberBoxNV23 = React.createRef<NumberBox>();

  private refFormPhieuNhap = React.createRef<FormPhieuNhapKho>();


  constructor(props) {
    super(props);
    this.state = {
      tuNgay: convertToSQLDate(new Date().toDateString()),
      denNgay: convertToSQLDate(new Date().toDateString()),
      // tuNgay: '2020-01-01',
      // denNgay: '2020-05-01',
      phieuNhapKhoObj: defaultPhieuNhap,
      curTab: 0,
      enable: defaultSetEnable,
      nhanVienKiemNhapObj: defaultNhanVienKiemNhap,
      chiTietPhieuNhapObj: defaultChiTietPhieuNhap,
      selectedRowKeysGridDanhSachPhieuNhapKho: [],
      loadingConfigs: {
        visible: false,
        position: 'body'
      },
      sendPhieuNhapKho: {
        phieuNhapKho: defaultPhieuNhap,
        isUpdated: -1
      },
      enableInput: false
    }
  };

  componentDidMount(): void {
    this.getDanhSachThamSoDonVi();
    this.getDanhSachNguonDuoc();
    this.getDanhSachNhaCungCap();
    this.getDanhSachKhoNhap();
    this.getDanhSachVatTu();
    this.getDanhSachNguoiNhan();
    this.getDanhSachHoiDongKiemNhap();
  };

  componentDidUpdate(prevProps: Readonly<INhapKhoTuNhaCungCapProp>, prevState: Readonly<INhapKhoTuNhaCungCapState>, snapshot?: any): void {
    if (prevProps.match.params.nghiepVu !== this.props.match.params.nghiepVu) {
      this.setState({
        phieuNhapKhoObj: defaultPhieuNhap
      });
      this.setEnabled(true, false, false, false, false, true);
      this.getDanhSachChiTietHoaDon(0);
      this.getDanhSachChiTietNhapKho(0);
    }
  }

  // Handles
  handleOnClickLamMoi = () => {
    this.props.getDanhSachPhieuNhapKho(getDvtt(),
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay,
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay,
      this.props.match.params.nghiepVu
    )
  };
  handleOnSelectRowDataGridNhanVienKiemNhap = (e) => {
    if (e.selectedRowsData.length <= 0) {
      this.setState({
        nhanVienKiemNhapObj: defaultNhanVienKiemNhap
      });
      return;
    }
    const selectedNhanVienKiemNhap = e.selectedRowsData[0];
    const mapNhanVienKiemNhapObj = {
      ...defaultNhanVienKiemNhap,
      chucDanh: selectedNhanVienKiemNhap === undefined ? "" : selectedNhanVienKiemNhap.CHUCDANH,
      dvtt: selectedNhanVienKiemNhap === undefined ? "" : selectedNhanVienKiemNhap.DVTT,
      idNhapKhoTuNhaCungCap: selectedNhanVienKiemNhap === undefined ? "" : selectedNhanVienKiemNhap.ID_NHAPKHONHACC,
      maNhanVien: selectedNhanVienKiemNhap === undefined ? "" : selectedNhanVienKiemNhap.MANHANVIEN,
      soPhieuNhap: selectedNhanVienKiemNhap === undefined ? "" : selectedNhanVienKiemNhap.SOPHIEUNHAP,
      tenNhanVien: selectedNhanVienKiemNhap === undefined ? "" : selectedNhanVienKiemNhap.TENNHANVIEN,
    };
    this.setState({
      nhanVienKiemNhapObj: mapNhanVienKiemNhapObj
    });
  };
  addPhieuNhapKho = (phieuNhapKho: IPhieuNhapKho, setEnabaled: Function) => {
    this.props.addPhieuNhapKho({...phieuNhapKho,
        ngayHoaDon: convertToDate1(phieuNhapKho.ngayHoaDon),
        ngayNhap: convertToDate1(phieuNhapKho.ngayNhap)
      },
      getDvtt(),
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay,
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay,
      parseInt(this.props.match.params.nghiepVu, 0),
      setEnabaled,
      this.mapPhieuNhapKhoObj,
      () => {
        if (this.props.match.params.nghiepVu === '52') {
          this.tenVatTuDropDownBox.current.handleFocus();
        } else {
          this.tenVatTuDropDownBoxNV23.current.handleFocus();
        }
      });
  };
  updatePhieuNhapKho = (phieuNhapKho: IPhieuNhapKho) => {
    this.props.updatePhieuNhapKho({
      ...phieuNhapKho,
      ngayHoaDon: convertToDate(phieuNhapKho.ngayHoaDon),
      ngayNhap: convertToDate(phieuNhapKho.ngayNhap),
      maPhongBanNhap: getMaPhongBan()
    }, getDvtt(),
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay,
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay,
      parseInt(this.props.match.params.nghiepVu, 0),
      () => {
      this.setState({
        sendPhieuNhapKho: {
          phieuNhapKho: defaultPhieuNhap,
          isUpdated: 2
        }
      })
      });
  };
  deletePhieuNhapKho = (idPhieuNhap: number) => {
    this.props.deletePhieuNhapKho(idPhieuNhap,
      1,
      getDvtt(),
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay,
      this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay,
      parseInt(this.props.match.params.nghiepVu, 0),
      () => {
        this.setState({
          sendPhieuNhapKho: {
            phieuNhapKho: defaultPhieuNhap,
            isUpdated: 2
          }
        })
      });
  };
  mapPhieuNhapKhoObj = (idPhieuNhap, soLuuTru, soPhieuNhap) => {
    this.setState({
      sendPhieuNhapKho: {
        phieuNhapKho: {...this.refFormPhieuNhap.current.getphieuNhapKhoObj(),
          idNhapKhoTuNhaCungCap: idPhieuNhap,
          soLuuTru,
          soPhieuNhap
        },
        isUpdated: 1
      }
    });
    this.setState({
      phieuNhapKhoObj: {...this.state.phieuNhapKhoObj,
        idNhapKhoTuNhaCungCap: idPhieuNhap,
        soLuuTru,
        soPhieuNhap
      }
    })
  };
  handleEnterNextInput = (e, currentRef, nextRef) => {
    nextRef.current.instance.focus();
    switch (currentRef) {
      case this.soLuongThucNumberBox: {
        if (parseFloat(e.event.target.value) === 0 || e.event.target.value === "" || Number.isNaN(parseFloat(e.event.target.value))) {
          notify({ message: "Số lượng phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.soLuongThucNumberBox.current.instance.focus();
          return;
        }
        break;
      }
      case this.soLoTextBox: {
        if (e.event.target.value === "") {
          notify({ message: "Số lô không được rỗng", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.soLoTextBox.current.instance.focus();
          return;
        }
        break;
      }
      case this.soLoTextBoxNV23: {
        if (e.event.target.value === "") {
          notify({ message: "Số lô không được rỗng", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.soLoTextBoxNV23.current.instance.focus();
          return;
        }
        break;
      }
      case this.ngayHetHanDateBox: {
        if (e.event.target.value === "" || e.event.target.value.length !== 10) {
          notify({ message: "Ngày hết hạn không hợp lệ. Kiểm tra lại", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.ngayHetHanDateBox.current.instance.focus();
          return;
        }
        break;
      }
      case this.ngayHetHanDateBoxNV23: {
        if (e.event.target.value === "" || e.event.target.value.length !== 10) {
          notify({ message: "Ngày hết hạn không hợp lệ. Kiểm tra lại", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.ngayHetHanDateBoxNV23.current.instance.focus();
          return;
        }
        break;
      }
      case this.donGiaTruocVATNumberBox: {
        if (parseFloat(e.event.target.value) === 0 || e.event.target.value === "" || Number.isNaN(parseFloat(e.event.target.value))) {
          notify({ message: "Đơn giá phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.donGiaTruocVATNumberBox.current.instance.focus();
          return;
        }
        const donGiaTruocVAT = this.state.chiTietPhieuNhapObj.donGiaTruocVAT;
        const vat = this.state.phieuNhapKhoObj.vat;
        const soLuong = this.state.chiTietPhieuNhapObj.soLuong;
        let donGia = donGiaTruocVAT * vat / 10 + donGiaTruocVAT;
        if (getMoTaThamSo(this.props.listDanhSachThamSoDonVi, 53) === "1") {
          donGia = Math.round(donGia);
        }

        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            donGia,
            thanhTien: donGia * soLuong
          }
        });
        break;
      }
      case this.quyCachTextBoxNV23: {
        if (this.state.chiTietPhieuNhapObj.quyCach === "") {
          this.quyCachTextBoxNV23.current.instance.focus();
        }
        break;
      }
      case this.soLuongNhapNumberBoxNV23: {
        const soLuong = e.event.target.value;
        if (parseFloat(e.event.target.value) === 0 || e.event.target.value === "" || Number.isNaN(parseFloat(e.event.target.value))) {
          notify({ message: "Số lượng phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.soLuongNhapNumberBoxNV23.current.instance.focus();
          return;
        }
        const quyCach = this.state.chiTietPhieuNhapObj.quyCach;
        let quyCachQuyDoi = 0;
        let soLuongQuyDoi = 0;
        const index = quyCach.indexOf('/');
        if (index === -1) {
          quyCachQuyDoi = 1;
        } else {
          quyCachQuyDoi = parseInt(quyCach.substr(index + 1, quyCach.length),0);
        }
        soLuongQuyDoi = soLuong * quyCachQuyDoi;

        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            soLuong: e.event.target.value,
            quyCachQuyDoi,
            soLuongQuyDoi
          }
        });
        break;
      }
      case this.donGiaNumberBoxNV23: {
        const donGiaNhap = e.event.target.value;
        if (parseFloat(e.event.target.value) === 0 || e.event.target.value === "" || Number.isNaN(parseFloat(e.event.target.value))) {
          notify({ message: "Đơn giá phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          currentRef.current.instance.focus();
          return;
        }
        const soLuongNhap = this.state.chiTietPhieuNhapObj.soLuong;
        const quyCachQuyDoi = this.state.chiTietPhieuNhapObj.quyCachQuyDoi;
        const vat = this.state.phieuNhapKhoObj.vat;
        let donGiaQuyDoi = donGiaNhap / quyCachQuyDoi;
        let donGiaQuyDoiVAT = donGiaQuyDoi + donGiaQuyDoi * vat / 100;
        let thanhTien = donGiaNhap * soLuongNhap;
        const tienVAT = (donGiaNhap * vat / 100) * soLuongNhap;
        const thanhTienVAT = thanhTien + tienVAT;
        const haohut = this.state.chiTietPhieuNhapObj.phanTramHaoHut;
        let soLuongQuyDoi = this.state.chiTietPhieuNhapObj.soLuongQuyDoi;
        let donGiaSauHaoHut = 0;
        if (this.props.match.params.nghiepVu === '23') {
          soLuongQuyDoi = soLuongNhap - soLuongNhap * haohut / 100;
        }
        if (getMoTaThamSo(this.props.listDanhSachThamSoDonVi, 53) === '1') {
          donGiaQuyDoi = Math.round(donGiaQuyDoi);
          donGiaQuyDoiVAT = Math.round(donGiaQuyDoiVAT);
        }
        donGiaSauHaoHut = donGiaQuyDoiVAT * soLuongNhap / soLuongQuyDoi;
        if (this.props.match.params.nghiepVu === '23') {
          thanhTien = donGiaSauHaoHut * soLuongQuyDoi;
        }
        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            donGiaTruocVAT: e.event.target.value,
            quyCachQuyDoi,
            donGiaQuyDoiVAT,
            thanhTien,
            tienVAT,
            thanhTienVAT,
            donGiaSauHaoHut: this.props.match.params.nghiepVu === '24' ? donGiaNhap : donGiaSauHaoHut,
            soLuongQuyDoi,
            donGiaQuyDoi,
            donGiaChinhSua: donGiaQuyDoiVAT
          }
        });
        break;
      }
      default: {
        return;
      }
    }
  };
  handleOnKeyPressDonGiaNhapKho = (e) => {
    this.setState({
      chiTietPhieuNhapObj: {
        ...this.state.chiTietPhieuNhapObj,
        donGiaChinhSua: parseFloat(e.event.target.value)
      }
    });
    if (e.event.charCode === 13) {
      if (this.state.chiTietPhieuNhapObj.donGiaChinhSua === 0 || Number.isNaN(parseFloat(e.event.target.value))) {
        notify({ message: "Đơn giá phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
        this.donGiaNhapKhoNumberBoxNV23.current.instance.focus();
        return;
      }
      this.tenVatTuDropDownBoxNV23.current.handleFocus();
      // this.addChiTietHoaDon();
      this.setState({
        chiTietPhieuNhapObj: defaultChiTietPhieuNhap
      })
    }
  };
  handleOnKeyPressDonGiaTruocVAT = (e) => {
    if (this.props.match.params.nghiepVu === '23') {
      const donGiaNhap = e.event.target.value;
      const soLuongNhap = this.state.chiTietPhieuNhapObj.soLuong;
      const quyCachQuyDoi = this.state.chiTietPhieuNhapObj.quyCachQuyDoi;
      const vat = this.state.phieuNhapKhoObj.vat;
      let donGiaQuyDoi = donGiaNhap / quyCachQuyDoi;
      let donGiaQuyDoiVAT = donGiaQuyDoi + donGiaQuyDoi * vat / 100;
      let thanhTien = donGiaNhap * soLuongNhap;
      const tienVAT = (donGiaNhap * vat / 100) * soLuongNhap;
      const thanhTienVAT = thanhTien + tienVAT;
      const haohut = this.state.chiTietPhieuNhapObj.phanTramHaoHut;
      let soLuongQuyDoi = this.state.chiTietPhieuNhapObj.soLuongQuyDoi;
      let donGiaSauHaoHut = 0;
      if (this.props.match.params.nghiepVu === '23') {
        soLuongQuyDoi = soLuongNhap - soLuongNhap * haohut / 100;
      }
      if (getMoTaThamSo(this.props.listDanhSachThamSoDonVi, 53) === '1') {
        donGiaQuyDoi = Math.round(donGiaQuyDoi);
        donGiaQuyDoiVAT = Math.round(donGiaQuyDoiVAT);
      }
      donGiaSauHaoHut = donGiaQuyDoiVAT * soLuongNhap / soLuongQuyDoi;
      if (this.props.match.params.nghiepVu === '23') {
        thanhTien = donGiaSauHaoHut * soLuongQuyDoi;
      }
      this.setState({
        ...this.state,
        chiTietPhieuNhapObj: {
          ...this.state.chiTietPhieuNhapObj,
          donGiaTruocVAT: e.event.target.value,
          quyCachQuyDoi,
          donGiaQuyDoiVAT,
          thanhTien,
          tienVAT,
          thanhTienVAT,
          donGiaSauHaoHut,
          donGiaTruocHaoHut: this.state.chiTietPhieuNhapObj.donGiaQuyDoiVAT,
          soLuongQuyDoi,
          donGiaQuyDoi,
          donGiaChinhSua: donGiaQuyDoiVAT
        }
      });
      if (e.event.charCode === 13) {
        if (this.state.chiTietPhieuNhapObj.donGiaTruocVAT === 0 || Number.isNaN(parseFloat(e.event.target.value))) {
          notify({ message: "Đơn giá phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.donGiaNumberBoxNV23.current.instance.focus();
          return;
        }
        // this.addChiTietNhapKhoDongY();
      }
    }
  };
  handleOnChangeChiTietPhieuNhap = (e, name) => {
    switch (name) {
      case 'donGiaTruocVAT': {
        const donGiaTruocVAT = e.value;
        if (donGiaTruocVAT === 0) {
          notify({ message: "Đơn giá phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.donGiaVATNumberBox.current.instance.focus();
          return;
        }
        const vat = this.state.phieuNhapKhoObj.vat;
        const soLuong = this.state.chiTietPhieuNhapObj.soLuong;
        let donGia = donGiaTruocVAT * vat / 10 + parseFloat(donGiaTruocVAT);
        if (getMoTaThamSo(this.props.listDanhSachThamSoDonVi, 53) === "1") {
          donGia = Math.round(donGia);
        }

        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            donGiaTruocVAT: e.value,
            donGia,
            thanhTien: donGia * soLuong
          }
        });
        break;
      }
      default: {
        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            [name]: e.value
          }
        });
        break;
      }
    }
};
  handleOnchangeChiTietPhieuNhapNV23 = (e, name) => {
    switch (name) {
      case 'soLuong': {
        const soLuong = parseFloat(e.value);
        const quyCach = this.state.chiTietPhieuNhapObj.quyCach;
        let quyCachQuyDoi = 0;
        let soLuongQuyDoi = 0;
        const index = quyCach.indexOf('/');
        if (index === -1) {
          quyCachQuyDoi = 1;
        } else {
          quyCachQuyDoi = parseInt(quyCach.substr(index + 1, quyCach.length),0);
        }
        soLuongQuyDoi = soLuong * quyCachQuyDoi;

        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            soLuong: parseFloat(e.value),
            quyCachQuyDoi,
            soLuongQuyDoi
          }
        });
        break;
      }
      case 'donGiaTruocVAT': {
        const donGiaNhap = e.value;
        const soLuongNhap = this.state.chiTietPhieuNhapObj.soLuong;
        const quyCachQuyDoi = this.state.chiTietPhieuNhapObj.quyCachQuyDoi;
        const vat = this.state.phieuNhapKhoObj.vat;
        let donGiaQuyDoi = donGiaNhap / quyCachQuyDoi;
        let donGiaQuyDoiVAT = donGiaQuyDoi + donGiaQuyDoi * vat / 100;
        let thanhTien = donGiaNhap * soLuongNhap;
        const tienVAT = (donGiaNhap * vat / 100) * soLuongNhap;
        const thanhTienVAT = thanhTien + tienVAT;
        const haohut = this.state.chiTietPhieuNhapObj.phanTramHaoHut;
        let soLuongQuyDoi = this.state.chiTietPhieuNhapObj.soLuongQuyDoi;
        let donGiaSauHaoHut = 0;
        if (this.props.match.params.nghiepVu === '23') {
          soLuongQuyDoi = soLuongNhap - soLuongNhap * haohut / 100;
        }
        if (getMoTaThamSo(this.props.listDanhSachThamSoDonVi, 53) === '1') {
          donGiaQuyDoi = Math.round(donGiaQuyDoi);
          donGiaQuyDoiVAT = Math.round(donGiaQuyDoiVAT);
        }
        donGiaSauHaoHut = donGiaQuyDoiVAT * soLuongNhap / soLuongQuyDoi;
        if (this.props.match.params.nghiepVu === '23') {
          thanhTien = donGiaSauHaoHut * soLuongQuyDoi;
        }
        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            donGiaTruocVAT: e.value,
            quyCachQuyDoi,
            donGiaQuyDoiVAT,
            thanhTien,
            tienVAT,
            thanhTienVAT,
            donGiaSauHaoHut,
            soLuongQuyDoi,
            donGiaQuyDoi
          }
        });
        break;
      }
      case 'donGiaChinhSua': {
        if (e.value - this.state.chiTietPhieuNhapObj.donGiaQuyDoiVAT > 5 || this.state.chiTietPhieuNhapObj.donGiaQuyDoiVAT - e.value > 5) {
          notify('Đơn giá nhập kho không được chênh lệch 5 đồng so với đơn giá quy đổi VAT', 'error', NOTIFY_DISPLAYTIME);
        }
        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            donGiaChinhSua: e.value
          }
        });
        break;
      }
      case 'phanTramHaoHut': {
        const haohut = e.value;
        let soLuongQuyDoi = 0;
        const vat = this.state.phieuNhapKhoObj.vat;
        const soLuongNhap = this.state.chiTietPhieuNhapObj.soLuong;
        const donGiaNhap = this.state.chiTietPhieuNhapObj.donGiaTruocVAT;
        const donGiaQuyDoiVAT = donGiaNhap + donGiaNhap * vat / 100;
        let donGiaSauHaoHut = 0;
        if (this.props.match.params.nghiepVu === '23') {
          soLuongQuyDoi = soLuongNhap - soLuongNhap * haohut / 100;
          donGiaSauHaoHut = donGiaQuyDoiVAT * soLuongNhap / soLuongQuyDoi;
        }
        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            soLuongQuyDoi,
            donGiaSauHaoHut,
            phanTramHaoHut: e.value,
            donGiaQuyDoi: donGiaNhap,
            donGiaQuyDoiVAT
          }
        });
        break;
      }
      default: {
        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            [name]: e.value
          }
        });
      }
    }
  };
  handleSelectTenVatTu = () => {
    const nghiepVu = this.props.match.params.nghiepVu;
    let selectedVatTuObj;
    if (nghiepVu === '52') {
      this.soLuongThucNumberBox.current.instance.focus();
      selectedVatTuObj = this.tenVatTuDropDownBox.current.state.selectedVatTuObj;
    } else if (nghiepVu === '23') {
      this.soLuongNhapNumberBoxNV23.current.instance.focus();
      selectedVatTuObj = this.tenVatTuDropDownBoxNV23.current.state.selectedVatTuObj;
    } else {
      this.quyCachTextBoxNV23.current.instance.focus();
      selectedVatTuObj = this.tenVatTuDropDownBoxNV23.current.state.selectedVatTuObj;
    }

    this.setState({
      chiTietPhieuNhapObj: {...this.state.chiTietPhieuNhapObj,
        maVatTu: selectedVatTuObj.MAVATTU,
        hoatChat: selectedVatTuObj.HOATCHAT,
        dvt: selectedVatTuObj.DVT,
        soThau: selectedVatTuObj.MABAOCAO,
        donGiaTruocVAT: selectedVatTuObj.DONGIA_BV,
        donGiaSauHaoHut: selectedVatTuObj.DONGIA_BV,
        donGiaChinhSua: selectedVatTuObj.DONGIA_BV
      }
    });
  };

  handleOnDoubleClickGridPhieuNhap = (e) => {
    if (e.data === undefined) {
      return;
    }
    const selectedPhieuNhap = e.data;
    const mapPhieuNhapKhoObj = {
      ...defaultPhieuNhap,
      dongY: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.DONGY,
      dvtt: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.DVTT,
      maDonViTao: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.DVTT,
      ghiChu: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.GHICHU,
      idNhapKhoTuNhaCungCap: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.ID_NHAPKHOTUNHACC,
      maNghiepVuChuyen: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.MANGHIEPVU,
      maNhaCungCap: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.MANHACC,
      maKhoNhan: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.MA_KHO_NHAN,
      maNguoiNhan: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.MA_NGUOI_NHAN,
      ngayHoaDon: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.NGAYHOADON,
      ngayNhap: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.NGAYNHAP,
      nguoiGiaoHang: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.NGUOIGIAOHANG,
      nguoiTao: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.NGUOI_TAO,
      nguonDuoc: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.NGUONDUOC,
      soHoaDon: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.SOHOADON,
      soHopDong: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.SOHOPDONG,
      soLuuTru: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.SOLUUTRU,
      soPhieuNhap: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.SOPHIEUNHAP,
      ttHoaDon: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.THANHTIEN,
      vat: selectedPhieuNhap === undefined ? "" : selectedPhieuNhap.VAT
    };
    this.setState({
      phieuNhapKhoObj: mapPhieuNhapKhoObj,
      chiTietPhieuNhapObj: defaultChiTietPhieuNhap,
      sendPhieuNhapKho: {
        phieuNhapKho: mapPhieuNhapKhoObj,
        isUpdated: 0
      }
    }, () => {
      this.setState({
        sendPhieuNhapKho: {
          phieuNhapKho: defaultPhieuNhap,
          isUpdated: -1
        }
      })
    });
    this.setEnabled(false, true, false, true, true, false);
    this.getDanhSachChiTietHoaDon(mapPhieuNhapKhoObj.idNhapKhoTuNhaCungCap);
    this.getDanhSachChiTietNhapKho(mapPhieuNhapKhoObj.idNhapKhoTuNhaCungCap);
    this.getDanhSachNhanVienKiemNhap(mapPhieuNhapKhoObj.idNhapKhoTuNhaCungCap, getDvtt());
  };
  handleOnChangeGridDanhSachPhieuNhapKho = (e) => {
    this.setState({
      selectedRowKeysGridDanhSachPhieuNhapKho: e.selectedRowsData
    });
  };
  handleOnSelectRowGridChiTietHoaDon = (e) => {
    const selectedChiTietHoaDon = e.selectedRowKeys[0];
    const mapChiTietPhieuNhapKhoObj = {
      ...defaultChiTietPhieuNhap,
      idNhapKhoCT: selectedChiTietHoaDon === undefined ? "" : selectedChiTietHoaDon.ID_NHAPKHOTUNHACC_HD_CT,
    };
    this.setState({
      chiTietPhieuNhapObj: mapChiTietPhieuNhapKhoObj
    });
  };
  handleOnSelectRowGridChiTietNhapKho = (e) => {
    if (this.props.match.params.nghiepVu === '24') {
      return;
    }
    const selectedChiTietNhapKho = e.selectedRowKeys[0];
    const mapChiTietPhieuNhapKhoObj = {
      ...defaultChiTietPhieuNhap,
      idNhapKhoCT: selectedChiTietNhapKho === undefined ? "" : selectedChiTietNhapKho.SONHAPKHOCHITIET,
    };
    this.setState({
      chiTietPhieuNhapObj: mapChiTietPhieuNhapKhoObj
    });
  };
  handleOnKeyDownGridChiTietHoaDon = (e) => {
    if(!this.state.enable.btnLuu) {
      return;
    }
    const isMac = window.navigator.platform.toUpperCase().includes('MAC', 0);
    if (e.event.keyCode === 46 || (e.event.keyCode === 8 && isMac)) {
      dialog.confirm("Xóa thuốc " + "<span style='color: red'>" + this.state.nhanVienKiemNhapObj.tenNhanVien + "</span>" + " ?", "Thông báo hệ thống")
        .then(dialogResult => {
          if (dialogResult) {
            this.props.deleteChiTietHoaDon(this.state.chiTietPhieuNhapObj.idNhapKhoCT,
              this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap,
              this.state.phieuNhapKhoObj.dvtt,
              1,
              this.state.tuNgay,
              this.state.denNgay,
              this.props.match.params.nghiepVu);
          }
        });
    }
  };
  handleOnKeyDownGridChitietNhapKho = (e) => {
    if(!this.state.enable.btnLuu) {
      return;
    }
    if (this.props.match.params.nghiepVu === '24') {
      return;
    }
    if (this.state.phieuNhapKhoObj.maNghiepVuChuyen !== parseInt(this.props.match.params.nghiepVu, 0)) {
      notify({ message: "Không thể điều chỉnh dữ liệu của nghiệp vụ nhập kho khác", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    const isMac = window.navigator.platform.toUpperCase().includes('MAC', 0);
    if (e.event.keyCode === 46 || (e.event.keyCode === 8 && isMac)) {
      dialog.confirm("Xóa thuốc khỏi phiếu nhập ?", "Thông báo hệ thống")
        .then(dialogResult => {
          if (dialogResult) {
            this.props.deleteChiTietNhapKho(this.state.chiTietPhieuNhapObj.idNhapKhoCT,
              this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap,
              this.state.phieuNhapKhoObj.dvtt,
              1,
              this.state.tuNgay,
              this.state.denNgay,
              this.props.match.params.nghiepVu);
          }
        });
    }
  };
  handleOnClickButtonNhapKho = (e) => {
    const arrayChiTietHoaDon = [];
    this.props.listDanhSachChiTietHoaDon.map((element, index) => {
      const chiTietHoaDonObj:IChiTietPhieuNhapKho = {};
      chiTietHoaDonObj.dvtt = this.state.phieuNhapKhoObj.dvtt;
      chiTietHoaDonObj.maNguoiNhap = 1;
      chiTietHoaDonObj.maNhapKhoVatTu = 0;
      chiTietHoaDonObj.soPhieuNhap = this.state.phieuNhapKhoObj.soPhieuNhap;
      chiTietHoaDonObj.maDonViNhan = this.state.phieuNhapKhoObj.maKhoNhan;
      chiTietHoaDonObj.soHoaDon = this.state.phieuNhapKhoObj.soHoaDon;
      chiTietHoaDonObj.soLoSanXuat = element.SOLOSANXUAT;
      chiTietHoaDonObj.ngaySanXuat = element.NGAYSANXUAT;
      chiTietHoaDonObj.ngayHetHan = element.NGAYHETHAN;
      chiTietHoaDonObj.maVatTu = element.MAVATTU;
      chiTietHoaDonObj.vat = this.state.phieuNhapKhoObj.vat;
      chiTietHoaDonObj.ngayNhap = this.state.phieuNhapKhoObj.ngayNhap;
      chiTietHoaDonObj.ghiChu = element.GHICHU;
      chiTietHoaDonObj.thanhTienVAT = element.TIENVAT;
      chiTietHoaDonObj.idNhapKhoCT = element.ID_NHAPKHOTUNHACC_HD_CT;
      chiTietHoaDonObj.idPhieuNhap = element.ID_NHAPKHOTUNHACC;
      chiTietHoaDonObj.donGiaTruocHaoHut = parseFloat(element.DONGIAHOADON);
      chiTietHoaDonObj.soLuongTruocHaoHut = parseFloat(element.SOLUONG);
      chiTietHoaDonObj.nghiepVu = parseInt(this.props.match.params.nghiepVu, 0);
      chiTietHoaDonObj.soLuongTruocQuyDoi = parseFloat(element.SOLUONG);
      chiTietHoaDonObj.soLuongQuyCach = parseFloat(element.QUYCACH_QUYDOI);
      chiTietHoaDonObj.donGiaHoaDon = parseFloat(element.DONGIAHOADON);
      chiTietHoaDonObj.donGiaChinhSua = parseFloat(element.DONGIA);
      chiTietHoaDonObj.soLuongQuyDoi = parseInt(element.SOLUONGQUYDOI, 0);
      chiTietHoaDonObj.donGiaQuyDoi = parseFloat(element.DONGIAQUYDOI);
      chiTietHoaDonObj.donGiaQuyDoiVAT = parseFloat(element.DONGIAQUYDOIVAT);
      chiTietHoaDonObj.thanhTienHoaDon = parseFloat(element.THANHTIEN);
      chiTietHoaDonObj.phanTramHaoHut = parseFloat(element.PHANTRAM_HAOHUT);
      chiTietHoaDonObj.phanTramDonGiaDV = 0;
      chiTietHoaDonObj.donGiaDV = 0;
      arrayChiTietHoaDon.push(chiTietHoaDonObj);
    });
    if (arrayChiTietHoaDon.length <= 0) {
      notify({ message: "Không có vật tư trong hóa đơn. Vui lòng kiểm tra lại", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    if (arrayChiTietHoaDon[0].donGiaChinhSua - arrayChiTietHoaDon[0].donGiaQuyDoiVAT > 5 || arrayChiTietHoaDon[0].donGiaQuyDoiVAT - arrayChiTietHoaDon[0].donGiaChinhSua > 5) {
      notify({ message: "Trong hóa đơn có vật tư có đơn giá hóa đơn chênh lệch quá 5 đồng so với đơn giá quy đổi VAT. Kiểm tra lại", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    this.props.addChiTietNhapKho(arrayChiTietHoaDon, this.state.tuNgay, this.state.denNgay, this.props.match.params.nghiepVu);
  };
  handleOnClickButtonXoaNhapKho = (e) => {
    dialog.confirm("Xóa nhập kho của phiếu " + "<span style='color: red'>" + this.state.phieuNhapKhoObj.soPhieuNhap + "</span>" + " ?", "Thông báo hệ thống")
      .then(dialogResult => {
        if (dialogResult) {
          this.props.deleteNhapKho(this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap,
            this.state.phieuNhapKhoObj.dvtt,
            1,
            getMaPhongBan(),
            getMaPhongBenh(),
            this.state.tuNgay,
            this.state.denNgay,
            this.props.match.params.nghiepVu);
        }
      });
  };

  handleLoading = (visible: boolean) => {
    this.setState({
      loadingConfigs: {
        ...this.state.loadingConfigs,
        visible
      }
    });
  };
  handleOnClickButtonInPhieuNhapKho = (loaiPhieu, loaiFile) => {
    const phieuNhap = this.state.phieuNhapKhoObj;
    this.props.printPhieuNhapKhoHoaDon(
      phieuNhap.idNhapKhoTuNhaCungCap,
      phieuNhap.soPhieuNhap,
      phieuNhap.soHoaDon,
      phieuNhap.dvtt,
      this.props.listDanhSachNguoiNhan.filter(n => n.MA_NHANVIEN === phieuNhap.maNguoiNhan)[0].MA_NHANVIEN,
      phieuNhap.ngayHoaDon,
      phieuNhap.ngayNhap,
      loaiPhieu, // 0: chưa nhập(hóa đơn)     1: đã nhập
      this.props.match.params.nghiepVu === '23' ? 1 : 0,
      getTenBenhVien(),
      loaiFile,
      getTenTinh(),
      getMaPhongBan(),
      'Buồng chuyên khoa',
      getMaNhanVien(),
      this.handleLoading
    );
  };
  handleOnClickButtonXuatDM = () => {
    this.props.exportDanhMucVatTuMau(getDvtt(), this.handleLoading);
  };
  handleOnClickButtonInBBKN = (loaiPhieu, loaiFile) => {
    const phieuNhap = this.state.phieuNhapKhoObj;
    this.props.printBienBanKiemNhap(
      phieuNhap.idNhapKhoTuNhaCungCap,
      phieuNhap.soPhieuNhap,
      phieuNhap.soHoaDon,
      phieuNhap.dvtt,
      phieuNhap.ngayHoaDon,
      phieuNhap.ngayNhap,
      this.state.phieuNhapKhoObj.maKhoNhan,
      this.state.phieuNhapKhoObj.maHoiDongKiemNhap,
      getTenBenhVien(),
      getTenTinh(),
      loaiPhieu,
      this.props.match.params.nghiepVu === '23' ? 1 : 0,
      loaiFile,
      this.handleLoading
    );
  };
  handleOnClickButtonInBBKNNhieuPhieu = (loaiFile) => {
    if (this.state.phieuNhapKhoObj.maHoiDongKiemNhap === null) {
      notify({ message: "Chưa chọn hội đồng kiểm nhập", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
      return;
    }
    const selectedPhieuNhap = this.state.selectedRowKeysGridDanhSachPhieuNhapKho;
    let soPhieuNhap = '';
    let soHoaDon = '';
    let maKhoNhan = '';
    selectedPhieuNhap.map(phieu => {
      soPhieuNhap += phieu.SOPHIEUNHAP + ',';
      soHoaDon += phieu.SOHOADON + ',';
      maKhoNhan += phieu.MA_KHO_NHAN + ',';
    });
    this.props.printBienBanKiemNhapNhieuPhieu(
      soPhieuNhap,
      soHoaDon,
      getDvtt(),
      maKhoNhan,
      this.state.phieuNhapKhoObj.maHoiDongKiemNhap,
      getTenBenhVien(),
      getTenTinh(),
      loaiFile,
      this.handleLoading
    );
  };
  handleChangeEnableInput = (enable: boolean) => {
    this.setState({
      enableInput: enable
    });
  };


  // Functions
  setEnabled = (btnThem, btnSua, btnLuu, btnHuy, btnXoa, formPhieuNhap) => {
    this.setState({
      enable: {
        btnThem, btnSua, btnLuu, btnHuy, btnXoa, formPhieuNhap
      }
    })
  };
  setDefaultStatus = () => {
    this.setEnabled(true, false, false, false, false, true);
    this.props.resetChiTietHoaDon();
    this.props.resetChiTietNhapKho();
    this.setState({
      selectedRowKeysGridDanhSachPhieuNhapKho: [],
      phieuNhapKhoObj: defaultPhieuNhap,
      chiTietPhieuNhapObj: defaultChiTietPhieuNhap
    });
  };
  getDanhSachNguonDuoc = () => {
    this.props.getDanhSachNguonDuoc();
  };
  getDanhSachNhaCungCap = () => {
    this.props.getDanhSachNhaCungCap();
  };
  getDanhSachKhoNhap = () => {
    this.props.getDanhSachKhoNhap('nhaptunhacungcap', getDvtt());
  };
  getDanhSachThamSoDonVi = () => {
    this.props.getDanhSachThamSoDonVi(getDvtt());
  };
  getDanhSachVatTu = () => {
    this.props.getDanhSachVatTu(getDvtt());
  };
  getDanhSachNguoiNhan = () => {
    this.props.getDanhSachNguoiNhan(getDvtt(), 'DUOC');
  };
  getDanhSachHoiDongKiemNhap = () => {
    this.props.getDanhSachHoiDongKiemNhap(getDvtt());
  };
  getDanhSachChiTietHoaDon = (idNhapKho) => {
    this.props.getDanhSachChiTietHoaDon(idNhapKho);
  };
  getDanhSachChiTietNhapKho = (idNhapKho) => {
    this.props.getDanhSachChiTietNhapKho(idNhapKho);
  };
  getDanhSachNhanVienKiemNhap = (idNhapKho, dvtt) => {
    this.props.getDanhSachNhanVienKiemNhap(idNhapKho, dvtt);
  };
  addChiTietHoaDon = (chiTietPhieuNhap: IChiTietPhieuNhapKho) => {
    const phieuNhapKhoObj = this.refFormPhieuNhap.current.getphieuNhapKhoObj();
    const chiTietPhieuNhap1 = {
        ...chiTietPhieuNhap,
        idPhieuNhap: phieuNhapKhoObj.idNhapKhoTuNhaCungCap,
        dvtt: getDvtt(),
        maNguoiNhap: 1,
        nghiepVu: parseInt(this.props.match.params.nghiepVu, 0),
        soHoaDon: phieuNhapKhoObj.soHoaDon,
        soPhieuNhap: phieuNhapKhoObj.soPhieuNhap,
        vat: phieuNhapKhoObj.vat,
        maDonViNhan: phieuNhapKhoObj.maKhoNhan,
        ngayHetHan: convertToSQLDate(chiTietPhieuNhap.ngayHetHan),
        ngaySanXuat: convertToSQLDate(getKYearFromNow(0).toDateString()),
        ngayNhap: phieuNhapKhoObj.ngayNhap,
        idNhapKhoCT: 0,
        phanTramHaoHut: 0,
        donGiaTruocHaoHut: chiTietPhieuNhap.donGiaTruocVAT,
        soLuongTruocHaoHut: chiTietPhieuNhap.soLuong,
        donGiaSauHaoHut: chiTietPhieuNhap.donGiaTruocVAT,
        phanTramDonGiaDV: 0,
        donGia: chiTietPhieuNhap.donGiaChinhSua,
        donGiaDV: 0,
        donGiaVP: 0,
        soLuongTruocQuyDoi: chiTietPhieuNhap.soLuong,
        soLuongQuyCach: 0,
        donGiaHoaDon: chiTietPhieuNhap.donGiaTruocVAT,
        thanhTienHoaDon: chiTietPhieuNhap.thanhTien,
        ghiChu: 'nhaptunhacungcap'
      };
    this.props.addChiTietHoaDon(chiTietPhieuNhap1);
  };
  addChiTietNhapKhoDongY = (chiTietPhieuNhap: IChiTietPhieuNhapKho) => {
    const phieuNhapKhoObj = this.refFormPhieuNhap.current.getphieuNhapKhoObj();
    const chiTietPhieuNhap1 = {
        ...chiTietPhieuNhap,
        idPhieuNhap: phieuNhapKhoObj.idNhapKhoTuNhaCungCap,
        dvtt: getDvtt(),
        maNguoiNhap: 1,
        nghiepVu: parseInt(this.props.match.params.nghiepVu, 0),
        soHoaDon: phieuNhapKhoObj.soHoaDon,
        soPhieuNhap: phieuNhapKhoObj.soPhieuNhap,
        vat: phieuNhapKhoObj.vat,
        maDonViNhan: phieuNhapKhoObj.maKhoNhan,
        ngayHetHan: convertToSQLDate(chiTietPhieuNhap.ngayHetHan),
        ngaySanXuat: convertToSQLDate(getKYearFromNow(0).toDateString()),
        ngayNhap: convertToSQLDate(phieuNhapKhoObj.ngayNhap),
        idNhapKhoCT: 0,
        donGiaChinhSua: chiTietPhieuNhap.donGiaSauHaoHut,
        donGiaTruocHaoHut: chiTietPhieuNhap.donGiaTruocVAT,
        soLuongTruocHaoHut: chiTietPhieuNhap.soLuong,
        phanTramDonGiaDV: 0,
        donGia: chiTietPhieuNhap.donGiaSauHaoHut,
        donGiaDV: 0,
        donGiaVP: 0,
        soLuongTruocQuyDoi: chiTietPhieuNhap.soLuong,
        soLuongQuyCach: 0,
        donGiaHoaDon: chiTietPhieuNhap.donGiaTruocVAT,
        thanhTienHoaDon: chiTietPhieuNhap.thanhTien,
        ghiChu: 'nhapkhodongy'
      };
    this.props.addChiTietNhapKho([chiTietPhieuNhap1], this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay, this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay, this.props.match.params.nghiepVu);
  };
  addChiTietNhapKho = (chiTietNhapKho: IChiTietPhieuNhapKho) => {
    const phieuNhapKhoObj = this.refFormPhieuNhap.current.getphieuNhapKhoObj();
    const chiTietNhapKho1 = {
      ...chiTietNhapKho,
      idPhieuNhap: phieuNhapKhoObj.idNhapKhoTuNhaCungCap,
      dvtt: getDvtt(),
      maNguoiNhap: 1,
      nghiepVu: parseInt(this.props.match.params.nghiepVu, 0),
      soHoaDon: phieuNhapKhoObj.soHoaDon,
      soPhieuNhap: phieuNhapKhoObj.soPhieuNhap,
      vat: phieuNhapKhoObj.vat,
      maDonViNhan: phieuNhapKhoObj.maKhoNhan,
      ngayHetHan: convertToSQLDate(chiTietNhapKho.ngayHetHan),
      ngaySanXuat: convertToSQLDate(getKYearFromNow(0).toDateString()),
      ngayNhap: phieuNhapKhoObj.ngayNhap,
      thanhTienVAT: 0,
      idNhapKhoCT: 0,
      phanTramHaoHut: 0,
      donGiaTruocHaoHut: chiTietNhapKho.donGiaTruocVAT,
      soLuongTruocHaoHut: chiTietNhapKho.soLuong,
      donGiaSauHaoHut: chiTietNhapKho.donGiaTruocVAT,
      phanTramDonGiaDV: 0,
      donGiaDV: 0,
      donGiaVP: 0,
      soLuongTruocQuyDoi: chiTietNhapKho.soLuong,
      soLuongQuyCach: 0,
      donGiaHoaDon: 0,
      donGiaChinhSua: chiTietNhapKho.donGia,
      thanhTienHoaDon: chiTietNhapKho.thanhTien
    };
    this.props.addChiTietNhapKho([chiTietNhapKho1], this.refTuNgayDenNgay.current.getTuNgayDenNgay().tuNgay, this.refTuNgayDenNgay.current.getTuNgayDenNgay().denNgay, this.props.match.params.nghiepVu);
  };
  addNhanVienKiemNhap = (nhanvien: INhanVienKiemNhap) => {
    this.props.addNhanVienKiemNhap(nhanvien);
  };
  deleteNhanVienKiemNhap = (idPhieuNhap: number, dvtt: string, maNhanVien: number) => {
    this.props.deleteNhanVienKiemNhap(idPhieuNhap, dvtt, maNhanVien);
  };
  // addChiTietNhapKho = (e) => {
  //   if (this.props.match.params.nghiepVu === '52') {
  //     this.tenVatTuDropDownBox.current.handleFocus();
  //   } else {
  //     this.tenVatTuDropDownBoxNV23.current.handleFocus();
  //   }
  //   this.setState({
  //     chiTietPhieuNhapObj: {
  //       ...this.state.chiTietPhieuNhapObj,
  //       idPhieuNhap: this.state.phieuNhapKhoObj.idNhapKhoTuNhaCungCap,
  //       dvtt: getDvtt(),
  //       maNguoiNhap: 1,
  //       nghiepVu: parseInt(this.props.match.params.nghiepVu, 0),
  //       soHoaDon: this.state.phieuNhapKhoObj.soHoaDon,
  //       soPhieuNhap: this.state.phieuNhapKhoObj.soPhieuNhap,
  //       vat: this.state.phieuNhapKhoObj.vat,
  //       maDonViNhan: this.state.phieuNhapKhoObj.maKhoNhan,
  //       ngayHetHan: this.state.chiTietPhieuNhapObj.ngayHetHan,
  //       ngaySanXuat: convertToDate1(getKYearFromNow(0).toDateString()),
  //       ngayNhap: this.state.phieuNhapKhoObj.ngayNhap,
  //       thanhTienVAT: 0,
  //       idNhapKhoCT: 0,
  //       phanTramHaoHut: 0,
  //       donGiaTruocHaoHut: this.state.chiTietPhieuNhapObj.donGiaTruocVAT,
  //       soLuongTruocHaoHut: this.state.chiTietPhieuNhapObj.soLuong,
  //       donGiaSauHaoHut: this.state.chiTietPhieuNhapObj.donGiaTruocVAT,
  //       phanTramDonGiaDV: 0,
  //       donGiaDV: 0,
  //       donGiaVP: 0,
  //       soLuongTruocQuyDoi: this.state.chiTietPhieuNhapObj.soLuong,
  //       soLuongQuyCach: 0,
  //       donGiaHoaDon: 0,
  //       donGiaChinhSua: this.state.chiTietPhieuNhapObj.donGia,
  //       thanhTienHoaDon: this.state.chiTietPhieuNhapObj.thanhTien
  //     }
  //   });
  //   this.props.addChiTietNhapKho([{...this.state.chiTietPhieuNhapObj,
  //     ngayNhap: convertToSQLDate(this.state.chiTietPhieuNhapObj.ngayNhap),
  //     ngaySanXuat: convertToSQLDate1(this.state.chiTietPhieuNhapObj.ngaySanXuat),
  //     ngayHetHan: convertToSQLDate(this.state.chiTietPhieuNhapObj.ngayHetHan)
  //   }], this.state.tuNgay, this.state.denNgay, this.props.match.params.nghiepVu);
  // };

  public render() {
    return (<div>
      <DuocTitleComponent text={'Dược - Nghiệp vụ Nhập kho'}/>
        <Box
          direction="row"
          width="100%"
          height={'auto'}
        >
          <BoxItem baseSize={'35%'} ratio={0}>
            <div className={'div-box-wrapper'}>
              <DuocLabelComponent text={'Thông tin lọc phiếu nhập kho'}/>
              <div className={'row-margin-bottom-5'}/>
              <TuNgayDenNgayDocComponent
                ref={this.refTuNgayDenNgay}
                handleOnClickButtonLamMoi={this.handleOnClickLamMoi}
                lockDateAfter={true}
              />
            </div>
            <div className={'div-box-wrapper row-margin-top-5 div-height-100'}>
              <DuocLabelComponent text={'Danh sách phiếu nhập kho'}/>
              <GridDanhSachPhieuNhapKho
                listDanhSachPhieuNhapKho={this.props.listDanhSachPhieuNhapKho}
                height={'100%'}
                selectedRowKeys={this.state.selectedRowKeysGridDanhSachPhieuNhapKho}
                onSelectionChanged={this.handleOnChangeGridDanhSachPhieuNhapKho}
                onCellDblClick={this.handleOnDoubleClickGridPhieuNhap}
              />
            </div>
          </BoxItem>
          <BoxItem baseSize={'65%'} ratio={0}>
            <div className={'div-box-wrapper row-margin-left-5'}>
              <DuocLabelComponent icon={'bookmark'} text={'Thông tin phiếu nhập kho'}/>
              <FormPhieuNhapKho
                ref={this.refFormPhieuNhap}
                selectedPhieuChuyenLength = {this.state.selectedRowKeysGridDanhSachPhieuNhapKho.length <= 1}
                maNghiepVu={parseInt(this.props.match.params.nghiepVu, 0)}
                listDanhSachNguonDuoc={this.props.listDanhSachNguonDuoc}
                listDanhSachNhaCungCap={this.props.listDanhSachNhaCungCap}
                listDanhSachKhoNhap={this.props.listDanhSachKhoNhap}
                listDanhSachNguoiNhan={this.props.listDanhSachNguoiNhan}
                listDanhSachHoiDongKiemNhap={this.props.listDanhSachHoiDongKiemNhap}
                listDanhSachNhanVienKiemNhap={this.props.listDanhSachNhanVienKiemNhap}
                enableForm={this.props.listDanhSachChiTietNhapKho.length > 0}
                loadingConfigs={this.state.loadingConfigs}
                addPhieuNhapKho={this.addPhieuNhapKho}
                updatePhieuNhapKho={this.updatePhieuNhapKho}
                deletePhieuNhapKho={this.deletePhieuNhapKho}
                handleOnClickButtonInPhieuNhapKho={this.handleOnClickButtonInPhieuNhapKho}
                handleOnClickButtonInBBKN={this.handleOnClickButtonInBBKN}
                handleOnClickButtonInBBKNNhieuPhieu={this.handleOnClickButtonInBBKNNhieuPhieu}
                handleOnClickButtonXuatDM={this.handleOnClickButtonXuatDM}
                handleChangeEnableInput={this.handleChangeEnableInput}
                addNhanVienKiemNhap={this.addNhanVienKiemNhap}
                deleteNhanVienKiemNhap={this.deleteNhanVienKiemNhap}
                sendPhieuNhapKho={this.state.sendPhieuNhapKho}
              />
            </div>
          </BoxItem>
        </Box>
      {/* // Grid gõ thuốc*/}
      <Box
        direction="row"
        width="100%"
        height={'auto'}
      >
        <BoxItem ratio={1}>
          <table style={{ width: '100%', display:'none' }}>
            <thead>
            {
              this.props.match.params.nghiepVu === '52' ? (
                <tr>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot1+'%'}}>Tên vật tư</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot2+'%' }}>Hoạt chất</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot3+'%' }}>ĐVT</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot4+'%' }}>Số thầu</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot5+'%' }}>Số lượng thực</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot6+'%' }}>Số lô</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot7+'%' }}>Ngày hết hạn</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot8+'%' }}>Đơn giá</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot9+'%' }}>Đơn giá(VAT)</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot10+'%' }}>Thành tiền</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot11+'%' }}>Ghi chú</td>
                </tr>
              ) : (
                <tr>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot1NV23+'%'}}>Tên vật tư</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot2NV23+'%' }}>ĐVT</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot3NV23+'%' }}>Quy cách</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot4NV23+'%' }}>Số thầu</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot5NV23+'%' }}>SL nhập</td>
                  <td
                    className={'dx-datagrid-headers'}
                    style={{ width: mdctCot6NV23+'%', display: this.props.match.params.nghiepVu === '23' ? '' : 'none' }}
                  >% Hao hụt</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot7NV23+'%' }}>
                    {this.props.match.params.nghiepVu === '23' ? "SL thực" : "SL quy đổi"}
                  </td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot8NV23+'%' }}>Số lô</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot9NV23+'%' }}>Ngày hết hạn</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot10NV23+'%' }}>Đơn giá</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot11NV23+'%' }}>Đơn giá quy đổi</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot12NV23+'%' }}>Đơn giá QĐ(VAT)</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot13NV23+'%' }}>Đơn giá nhập kho</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot14NV23+'%' }}>Giá bán</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot15NV23+'%' }}>Tiền(VAT)</td>
                  <td className={'dx-datagrid-headers'} style={{ width: mdctCot16NV23+'%' }}>Thành tiền</td>
                </tr>
              )
            }
            </thead>
            <tbody>
            {
              this.props.match.params.nghiepVu === '52' ? (
                <tr>
                  <td style={{ width: mdctCot1+'%' }}>
                    <DropDownVatTu
                      ref={this.tenVatTuDropDownBox}
                      className={'no-border-radius'}
                      listDanhSachVatTu={this.props.listDanhSachVatTu}
                      dropWidth={screen.width-50}
                      dropHeight={500}
                      handleSelectTenVatTu = {this.handleSelectTenVatTu}
                      disabled={!this.state.enable.btnLuu}
                    />
                  </td>
                  <td style={{ width: mdctCot2+'%' }}>
                    <TextBox
                      ref={this.hoatChatTextBox}
                      className={'no-border-radius'}
                      width={'100%'}
                      onEnterKey={e => this.handleEnterNextInput(e, this.hoatChatTextBox, this.dvtTextBox)}
                      disabled={true}
                      value={this.state.chiTietPhieuNhapObj.hoatChat}
                    />
                  </td>
                  <td style={{ width: mdctCot3+'%' }}>
                    <TextBox
                      ref={this.dvtTextBox}
                      className={'no-border-radius'}
                      width={'100%'}
                      onEnterKey={e => this.handleEnterNextInput(e, this.dvtTextBox, this.soThauTextBox)}
                      disabled={true}
                      value={this.state.chiTietPhieuNhapObj.dvt}
                    />
                  </td>
                  <td style={{ width: mdctCot4+'%' }}>
                    <TextBox
                      ref={this.soThauTextBox}
                      className={'no-border-radius'}
                      width={'100%'}
                      onEnterKey={e => this.handleEnterNextInput(e, this.soThauTextBox, this.soLuongThucNumberBox)}
                      disabled={true}
                      value={this.state.chiTietPhieuNhapObj.soThau}
                    />
                  </td>
                  <td style={{ width: mdctCot5+'%' }}>
                    <NumberBox
                      min={0}
                      ref={this.soLuongThucNumberBox}
                      className={'no-border-radius'}
                      width={'100%'} onEnterKey={e => this.handleEnterNextInput(e, this.soLuongThucNumberBox, this.soLoTextBox)}
                      disabled={!this.state.enable.btnLuu}
                      value={this.state.chiTietPhieuNhapObj.soLuong}
                      onValueChanged={e => this.handleOnChangeChiTietPhieuNhap(e, 'soLuong')}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Số lượng không được rỗng" />*/}
                      {/* </Validator>*/}
                    </NumberBox>
                  </td>
                  <td style={{ width: mdctCot6+'%' }}>
                    <TextBox
                      ref={this.soLoTextBox}
                      className={'no-border-radius'}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.soLoSanXuat}
                      onEnterKey={e => this.handleEnterNextInput(e, this.soLoTextBox, this.ngayHetHanDateBox)}
                      onValueChanged={e => this.handleOnChangeChiTietPhieuNhap(e, 'soLoSanXuat')}
                      disabled={!this.state.enable.btnLuu}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Số lô không được rỗng" />*/}
                      {/* </Validator>*/}
                    </TextBox>
                  </td>
                  <td style={{ width: mdctCot7+'%' }}>
                    <DateBox
                      ref={this.ngayHetHanDateBox}
                      className={'no-border-radius'}
                      displayFormat={'dd/MM/yyyy'}
                      value={this.state.chiTietPhieuNhapObj.ngayHetHan}
                      width={'100%'} onEnterKey={e => this.handleEnterNextInput(e, this.ngayHetHanDateBox, this.donGiaTruocVATNumberBox)}
                      disabled={!this.state.enable.btnLuu}
                      openOnFieldClick={true}
                      onValueChanged={e => this.handleOnChangeChiTietPhieuNhap(e, 'ngayHetHan')}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Ngày hết hạn không được rỗng" />*/}
                      {/* </Validator>*/}
                    </DateBox>
                  </td>
                  <td style={{ width: mdctCot8+'%' }}>
                    <NumberBox
                      min={0}
                      ref={this.donGiaTruocVATNumberBox}
                      className={'no-border-radius'}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.donGiaTruocVAT}
                      onEnterKey={e => this.handleEnterNextInput(e, this.donGiaTruocVATNumberBox, this.ghiChuTextBox)}
                      onChange={e => this.handleOnChangeChiTietPhieuNhap(e, 'donGiaTruocVAT')}
                      disabled={!this.state.enable.btnLuu}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Đơn giá không được rỗng" />*/}
                      {/* </Validator>*/}
                    </NumberBox>
                  </td>
                  <td style={{ width: mdctCot9+'%' }}>
                    <NumberBox
                      min={0}
                      ref={this.donGiaVATNumberBox}
                      className={'no-border-radius'}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.donGia}
                      onEnterKey={e => this.handleEnterNextInput(e, this.donGiaVATNumberBox, this.thanhTienNumberBox)}
                      disabled={true}
                    />
                  </td>
                  <td style={{ width: mdctCot10+'%' }}>
                    <NumberBox
                      min={0}
                      ref={this.thanhTienNumberBox}
                      className={'no-border-radius'}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.thanhTien}
                      onEnterKey={e => this.handleEnterNextInput(e, this.thanhTienNumberBox, this.ghiChuTextBox)}
                      disabled={true}
                    />
                  </td>
                  <td style={{ width: mdctCot11+'%' }}>
                    <TextBox
                      ref={this.ghiChuTextBox}
                      className={'no-border-radius'}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.ghiChu}
                      // onEnterKey={this.addChiTietNhapKho}
                      onValueChanged={e => this.handleOnChangeChiTietPhieuNhap(e, 'ghiChu')}
                      disabled={!this.state.enable.btnLuu}
                    />
                  </td>
                </tr>
              ) : (
                <tr>
                  <td style={{ width: mdctCot1NV23+'%' }}>
                    <DropDownVatTu
                      ref={this.tenVatTuDropDownBoxNV23}
                      className={'no-border-radius'}
                      listDanhSachVatTu={this.props.match.params.nghiepVu === '23' ? this.props.listDanhSachVatTu.filter(vattu => vattu.LATHUOC === 0) : this.props.listDanhSachVatTu}
                      dropWidth={screen.width-50}
                      dropHeight={500}
                      handleSelectTenVatTu = {this.handleSelectTenVatTu}
                      disabled={!this.state.enable.btnLuu}
                    />
                  </td>
                  <td style={{ width: mdctCot2NV23+'%' }}>
                    <TextBox
                      width={'100%'}
                      disabled={true}
                      value={this.state.chiTietPhieuNhapObj.dvt}
                    />
                  </td>
                  <td style={{ width: mdctCot3NV23+'%' }}>
                    <TextBox
                      ref={this.quyCachTextBoxNV23}
                      className={'no-border-radius'}
                      width={'100%'}
                      disabled={!this.state.enable.btnLuu || this.props.match.params.nghiepVu !== '24'}
                      value={this.state.chiTietPhieuNhapObj.quyCach}
                      onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'quyCach')}
                      onEnterKey={e => this.handleEnterNextInput(e, this.quyCachTextBoxNV23, this.soLuongNhapNumberBoxNV23)}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Quy cách không được rỗng" />*/}
                      {/* </Validator>*/}
                    </TextBox>
                  </td>
                  <td style={{ width: mdctCot4NV23+'%' }}>
                    <TextBox
                      className={'no-border-radius'}
                      width={'100%'}
                      disabled={true}
                      value={this.state.chiTietPhieuNhapObj.soThau}
                    />
                  </td>
                  <td style={{ width: mdctCot5NV23+'%' }}>
                    <NumberBox
                      min={0}
                      ref={this.soLuongNhapNumberBoxNV23}
                      className={'no-border-radius'}
                      width={'100%'}
                      onEnterKey={e => this.handleEnterNextInput(e, this.soLuongNhapNumberBoxNV23, this.props.match.params.nghiepVu === '23' ? this.phanTramHaoHutNumberBoxNV23 : this.soLoTextBoxNV23)}
                      disabled={!this.state.enable.btnLuu}
                      value={this.state.chiTietPhieuNhapObj.soLuong}
                      onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'soLuong')}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Số lượng không được rỗng" />*/}
                      {/* </Validator>*/}
                    </NumberBox>
                  </td>
                  <td style={{ width: mdctCot6NV23+'%', display: this.props.match.params.nghiepVu === '23' ? '' : 'none' }}>
                    <NumberBox
                      min={0}
                      ref={this.phanTramHaoHutNumberBoxNV23}
                      className={'no-border-radius'}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.phanTramHaoHut}
                      disabled={!this.state.enable.btnLuu}
                      onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'phanTramHaoHut')}
                      onEnterKey={e => this.handleEnterNextInput(e, this.phanTramHaoHutNumberBoxNV23, this.soLoTextBoxNV23)}
                    />
                  </td>
                  <td style={{ width: mdctCot7NV23+'%' }}>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      width={'100%'}
                      disabled={true}
                      value={this.state.chiTietPhieuNhapObj.soLuongQuyDoi}
                    />
                  </td>
                  <td style={{ width: mdctCot8NV23+'%' }}>
                    <TextBox
                      ref={this.soLoTextBoxNV23}
                      className={'no-border-radius'}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.soLoSanXuat}
                      onEnterKey={e => this.handleEnterNextInput(e, this.soLoTextBoxNV23, this.ngayHetHanDateBoxNV23)}
                      onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'soLoSanXuat')}
                      disabled={!this.state.enable.btnLuu}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Số lô không được rỗng" />*/}
                      {/* </Validator>*/}
                    </TextBox>
                  </td>
                  <td style={{ width: mdctCot9NV23+'%' }}>
                    <DateBox
                      showDropDownButton={false}
                      ref={this.ngayHetHanDateBoxNV23}
                      className={'no-border-radius'}
                      displayFormat={'dd/MM/yyyy'}
                      value={this.state.chiTietPhieuNhapObj.ngayHetHan}
                      width={'100%'}
                      onEnterKey={e => this.handleEnterNextInput(e, this.ngayHetHanDateBoxNV23, this.donGiaNumberBoxNV23)}
                      disabled={!this.state.enable.btnLuu}
                      openOnFieldClick={true}
                      onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'ngayHetHan')}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Ngày hết hạn không được rỗng" />*/}
                      {/* </Validator>*/}
                    </DateBox>
                  </td>
                  <td style={{ width: mdctCot10NV23+'%' }}>
                    <NumberBox
                      min={0}
                      ref={this.donGiaNumberBoxNV23}
                      className={'no-border-radius'}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.donGiaTruocVAT}
                      onEnterKey={e => this.handleEnterNextInput(e, this.donGiaNumberBoxNV23, this.props.match.params.nghiepVu === '23' ? this.soLuongNhapNumberBoxNV23 : this.donGiaNhapKhoNumberBoxNV23)}
                      onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'donGiaTruocVAT')}
                      onKeyPress={this.handleOnKeyPressDonGiaTruocVAT}
                      disabled={!this.state.enable.btnLuu}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Đơn giá không được rỗng" />*/}
                      {/* </Validator>*/}
                    </NumberBox>
                  </td>
                  <td style={{ width: mdctCot11NV23+'%' }}>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.donGiaQuyDoi}
                      disabled={true}
                    />
                  </td>
                  <td style={{ width: mdctCot12NV23+'%' }}>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.donGiaQuyDoiVAT}
                      disabled={true}
                    />
                  </td>
                  <td style={{ width: mdctCot13NV23+'%' }}>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      ref={this.donGiaNhapKhoNumberBoxNV23}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.donGiaChinhSua}
                      disabled={!this.state.enable.btnLuu || this.props.match.params.nghiepVu !== '24'}
                      // onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'donGiaChinhSua')}
                      // onEnterKey={e => this.handleEnterNextInput(e, this.donGiaNhapKhoNumberBoxNV23, this.quyCachTextBoxNV23)}
                      onKeyPress={this.handleOnKeyPressDonGiaNhapKho}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Đơn giá nhập kho không được rỗng" />*/}
                      {/* </Validator>*/}
                    </NumberBox>
                  </td>
                  <td style={{ width: mdctCot14NV23+'%' }}>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.donGiaSauHaoHut}
                      disabled={true}
                    />
                  </td>
                  <td style={{ width: mdctCot15NV23+'%' }}>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.tienVAT}
                      disabled={true}
                    />
                  </td>
                  <td style={{ width: mdctCot16NV23+'%' }}>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.thanhTien}
                      disabled={true}
                    />
                  </td>
                </tr>
              )
            }
            </tbody>
          </table>
        </BoxItem>
      </Box>
      <div className={'div-box-wrapper row-margin-top-5'}>
        <DuocLabelComponent text={'Danh sách chi tiết phiếu nhập kho'}/>
      <Box
        direction="col"
        width="100%"
        height={'auto'}
      >
        <BoxItem ratio={0} baseSize={'100%'}>
            <GridGoQuyCach
              maNghiepVu={parseInt(this.props.match.params.nghiepVu, 0)}
              listDanhSachVatTu={this.props.listDanhSachVatTu}
              addChiTietNhapKhoHoaDon={this.addChiTietHoaDon}
              addChiTietNhapKhoDongY={this.addChiTietNhapKhoDongY}
              addChiTietNhapKho={this.addChiTietNhapKho}
              enableInput={this.state.enableInput}
              listDanhSachThamSoDonVi={this.props.listDanhSachThamSoDonVi}
              phieuNhapKhoObj={this.refFormPhieuNhap.current && this.refFormPhieuNhap.current.getphieuNhapKhoObj()}
            />
        </BoxItem>
        <BoxItem ratio={0} baseSize={'100%'} visible={this.props.match.params.nghiepVu === '24'}>
              <Box
                direction="col"
                width="100%"
                height={'auto'}
              >
                <BoxItem ratio={0} baseSize={'100%'}>
                  <GridChiTietHoaDon
                    listChiTietHoaDon={this.props.listDanhSachChiTietHoaDon}
                    thamsoVAT={getMoTaThamSo(this.props.listDanhSachThamSoDonVi, 111)}
                    maNghiepVu={23}
                    onSelectionChanged={this.handleOnSelectRowGridChiTietHoaDon}
                    onKeyDown={this.handleOnKeyDownGridChiTietHoaDon}
                  />
                </BoxItem>
                <BoxItem ratio={0} baseSize={'100%'}>
                  <div className={'div-button'} style={{ textAlign: 'left'}}>
                    <Button
                      width={150}
                      type={"default"}
                      text={'Nhập kho'}
                      disabled={this.props.listDanhSachChiTietHoaDon.length <= 0 || !this.state.enable.btnLuu || this.props.listDanhSachChiTietNhapKho.length > 0}
                      className={'btn-margin-5'}
                      onClick={this.handleOnClickButtonNhapKho}
                    />
                    <Button
                      width={150}
                      type={"default"}
                      text={'Xóa nhập kho'}
                      disabled={this.props.listDanhSachChiTietNhapKho.length <= 0 || !this.state.enable.btnLuu}
                      className={'btn-margin-5'}
                      onClick={this.handleOnClickButtonXoaNhapKho}
                    />
                    <DuocDropDownButton
                          loadingConfigs={this.state.loadingConfigs}
                      disabled={this.props.listDanhSachChiTietNhapKho.length <= 0}
                      text={'In nhập kho'}
                      arrayButton={[
                        { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInPhieuNhapKho(1, 'pdf')},
                        { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInPhieuNhapKho(1, 'xls')},
                        { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInPhieuNhapKho(1, 'rtf')}
                      ]}
                      defaultHandleOnClick={() => this.handleOnClickButtonInPhieuNhapKho(1, 'pdf')}
                      width={150}
                      className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
                    />
                    <DuocDropDownButton
                          loadingConfigs={this.state.loadingConfigs}
                      disabled={this.props.listDanhSachChiTietNhapKho.length <= 0}
                      text={'In BB Kiểm nhập'}
                      arrayButton={[
                        { id: 1, title: 'PDF', icon: 'exportpdf', handleOnClick: () => this.handleOnClickButtonInBBKN(1, 'pdf')},
                        { id: 2, title: 'XLS', icon: 'exportxlsx', handleOnClick: () => this.handleOnClickButtonInBBKN(1, 'xls')},
                        { id: 3, title: 'RTF', icon: 'rtffile', handleOnClick: () => this.handleOnClickButtonInBBKN(1, 'rtf')}
                      ]}
                      defaultHandleOnClick={() => this.handleOnClickButtonInBBKN(1, 'pdf')}
                      width={150}
                      className={'dx-button-mode-contained dx-button-default duoc-dropdown-button btn-margin-5'}
                    />
                  </div>
                </BoxItem>
                <br/>
              </Box>
            </BoxItem>
        <BoxItem ratio={0} baseSize={'100%'}>
          <GridChiTietNhapKho
            listChiTietNhapKho={this.props.listDanhSachChiTietNhapKho}
            thamsoVAT={getMoTaThamSo(this.props.listDanhSachThamSoDonVi, 111)}
            maNghiepVu={parseInt(this.props.match.params.nghiepVu, 0)}
            onSelectionChanged={this.handleOnSelectRowGridChiTietNhapKho}
            onKeyDown={this.handleOnKeyDownGridChitietNhapKho}
          />
        </BoxItem>
      </Box>
      </div>
    </div>);
  }
}
const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated,
  listDanhSachPhieuNhapKho: storeState.nhapKho.listDanhSachPhieuNhapKho,
  listDanhSachNguonDuoc: storeState.nhapKho.listDanhSachNguonDuoc,
  listDanhSachNhaCungCap: storeState.nhapKho.listDanhSachNhaCungCap,
  listDanhSachKhoNhap: storeState.nhapKho.listDanhSachKhoNhap,
  listDanhSachNguoiNhan: storeState.nhapKho.listDanhSachNguoiNhan,
  listDanhSachHoiDongKiemNhap: storeState.nhapKho.listDanhSachHoiDongKiemNhap,
  listDanhSachThamSoDonVi: storeState.nhapKho.listDanhSachThamSoDonVi,
  listDanhSachThamSoDuocMoi: storeState.nhapKho.listDanhSachThamSoDuocMoi,
  listDanhSachVatTu: storeState.nhapKho.listDanhSachVatTu,
  mapPhieuNhapKho: storeState.nhapKho.mapPhieuNhapKho,
  listDanhSachChiTietHoaDon: storeState.nhapKho.listDanhSachChiTietHoaDon,
  listDanhSachChiTietNhapKho: storeState.nhapKho.listDanhSachChiTietNhapKho,
  listDanhSachNhanVienKiemNhap: storeState.nhapKho.listDanhSachNhanVienKiemNhap,
});

const mapDispatchToProps = {
  getDanhSachPhieuNhapKho,
  getDanhSachNguonDuoc,
  getDanhSachNhaCungCap,
  getDanhSachKhoNhap,
  getDanhSachThamSoDonVi,
  getDanhSachVatTu,
  getDanhSachNguoiNhan,
  getDanhSachHoiDongKiemNhap,
  addPhieuNhapKho,
  updatePhieuNhapKho,
  deletePhieuNhapKho,
  getDanhSachChiTietHoaDon,
  getDanhSachChiTietNhapKho,
  getDanhSachNhanVienKiemNhap,
  addNhanVienKiemNhap,
  deleteNhanVienKiemNhap,
  deleteChiTietHoaDon,
  deleteChiTietNhapKho,
  deleteNhapKho,
  resetChiTietHoaDon,
  resetChiTietNhapKho,
  addChiTietHoaDon,
  addChiTietNhapKho,
  printPhieuNhapKhoHoaDon,
  exportDanhMucVatTuMau,
  printBienBanKiemNhap,
  printBienBanKiemNhapNhieuPhieu
};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NhapKhoComponent);

