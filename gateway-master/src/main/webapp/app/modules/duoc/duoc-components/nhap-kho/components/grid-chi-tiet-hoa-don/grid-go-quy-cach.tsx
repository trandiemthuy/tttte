import React from "react";
import DataGrid, {
  Column,
  FilterRow,
  HeaderFilter,
  IDataGridOptions, RowDragging,
  SearchPanel, Summary, TotalItem, Selection
} from 'devextreme-react/data-grid';
import { getMoTaThamSo} from "../../../utils/funtions";
import DropDownVatTu from "app/modules/duoc/duoc-components/nhap-kho/components/dropdown-vattu";
import {DateBox, NumberBox, TextBox} from "devextreme-react";
import {columnsWidth} from './configs';
import {
  IChiTietPhieuNhapKho,
  defaultChiTietPhieuNhap,
  IPhieuNhapKho
} from "app/modules/duoc/duoc-components/nhap-kho/nhap-kho.model";
import notify from "devextreme/ui/notify";
import {NOTIFY_DISPLAYTIME, NOTIFY_WIDTH, SHADING} from "app/modules/duoc/configs";

export interface IGridGoQuyCachProp extends IDataGridOptions {
  maNghiepVu: number;
  listDanhSachVatTu: any;
  addChiTietNhapKhoHoaDon: Function;
  addChiTietNhapKhoDongY: Function;
  addChiTietNhapKho: Function;
  enableInput: boolean;
  listDanhSachThamSoDonVi: any;
  phieuNhapKhoObj: IPhieuNhapKho;
}
export interface IGridGoQuyCachState {
  chiTietPhieuNhapObj: IChiTietPhieuNhapKho;
}

export class GridGoQuyCach extends React.Component<IGridGoQuyCachProp, IGridGoQuyCachState> {
  // Ref nghiệp vụ nhập kho QC
  private refTenVatTu = React.createRef<DropDownVatTu>();
  private refQuycach = React.createRef<TextBox>();
  private refSoLuongNhap = React.createRef<NumberBox>();
  private refSoLo = React.createRef<TextBox>();
  private refNgayHetHan = React.createRef<DateBox>();
  private refDonGia = React.createRef<NumberBox>();
  private refDonGiaNhapKho = React.createRef<NumberBox>();
  private refPhanTramHaoHut = React.createRef<NumberBox>();

  constructor(props) {
    super(props);
    this.state = {
      chiTietPhieuNhapObj: defaultChiTietPhieuNhap
    }
  }

  // Handles
  handleOnchangeChiTietPhieuNhapNV23 = (e, name) => {
    switch (name) {
      case 'soLuong': {
        const soLuong = parseFloat(e.value);
        const quyCach = this.state.chiTietPhieuNhapObj.quyCach;
        let quyCachQuyDoi = 0;
        let soLuongQuyDoi = 0;
        const index = quyCach.indexOf('/');
        if (index === -1) {
          quyCachQuyDoi = 1;
        } else {
          quyCachQuyDoi = parseInt(quyCach.substr(index + 1, quyCach.length),0);
        }
        soLuongQuyDoi = soLuong * quyCachQuyDoi;

        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            soLuong: parseFloat(e.value),
            quyCachQuyDoi,
            soLuongQuyDoi
          }
        });
        break;
      }
      case 'donGiaTruocVAT': {
        if (this.props.maNghiepVu === 52) {
          // const donGiaTruocVAT = e.value;
          // const vat = this.props.phieuNhapKhoObj.vat;
          // const soLuong = this.state.chiTietPhieuNhapObj.soLuong;
          // let donGia = donGiaTruocVAT * vat / 10 + parseFloat(donGiaTruocVAT);
          // if (getMoTaThamSo(this.props.listDanhSachThamSoDonVi, 53) === "1") {
          //   donGia = Math.round(donGia);
          // }
          // this.setState({
          //   ...this.state,
          //   chiTietPhieuNhapObj: {
          //     ...this.state.chiTietPhieuNhapObj,
          //     donGiaTruocVAT: e.value,
          //     donGia,
          //     thanhTien: donGia * soLuong
          //   }
          // });
        } else {
          const donGiaNhap = e.value;
          const soLuongNhap = this.state.chiTietPhieuNhapObj.soLuong;
          const quyCachQuyDoi = this.state.chiTietPhieuNhapObj.quyCachQuyDoi;
          const vat = this.props.phieuNhapKhoObj.vat;
          let donGiaQuyDoi = donGiaNhap / quyCachQuyDoi;
          let donGiaQuyDoiVAT = donGiaQuyDoi + donGiaQuyDoi * vat / 100;
          let thanhTien = donGiaNhap * soLuongNhap;
          const tienVAT = (donGiaNhap * vat / 100) * soLuongNhap;
          const thanhTienVAT = thanhTien + tienVAT;
          const haohut = this.state.chiTietPhieuNhapObj.phanTramHaoHut;
          let soLuongQuyDoi = this.state.chiTietPhieuNhapObj.soLuongQuyDoi;
          let donGiaSauHaoHut = 0;
          if (this.props.maNghiepVu === 23) {
            soLuongQuyDoi = soLuongNhap - soLuongNhap * haohut / 100;
          }
          if (getMoTaThamSo(this.props.listDanhSachThamSoDonVi, 53) === '1') {
            donGiaQuyDoi = Math.round(donGiaQuyDoi);
            donGiaQuyDoiVAT = Math.round(donGiaQuyDoiVAT);
          }
          donGiaSauHaoHut = donGiaQuyDoiVAT * soLuongNhap / soLuongQuyDoi;
          if (this.props.maNghiepVu === 23) {
            thanhTien = donGiaSauHaoHut * soLuongQuyDoi;
          }
          this.setState({
            ...this.state,
            chiTietPhieuNhapObj: {
              ...this.state.chiTietPhieuNhapObj,
              donGiaTruocVAT: e.value,
              quyCachQuyDoi,
              donGiaQuyDoiVAT,
              thanhTien,
              tienVAT,
              thanhTienVAT,
              donGiaSauHaoHut,
              soLuongQuyDoi,
              donGiaQuyDoi
            }
          });
        }
        break;
      }
      case 'donGiaChinhSua': {
        if (e.value - this.state.chiTietPhieuNhapObj.donGiaQuyDoiVAT > 5 || this.state.chiTietPhieuNhapObj.donGiaQuyDoiVAT - e.value > 5) {
          notify('Đơn giá nhập kho không được chênh lệch 5 đồng so với đơn giá quy đổi VAT', 'error', NOTIFY_DISPLAYTIME);
        }
        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            donGiaChinhSua: e.value
          }
        });
        break;
      }
      case 'phanTramHaoHut': {
        const haohut = e.value;
        let soLuongQuyDoi = 0;
        const vat = this.props.phieuNhapKhoObj.vat;
        const soLuongNhap = this.state.chiTietPhieuNhapObj.soLuong;
        const donGiaNhap = this.state.chiTietPhieuNhapObj.donGiaTruocVAT;
        const donGiaQuyDoiVAT = donGiaNhap + donGiaNhap * vat / 100;
        let donGiaSauHaoHut = 0;
        if (this.props.maNghiepVu === 23) {
          soLuongQuyDoi = soLuongNhap - soLuongNhap * haohut / 100;
          donGiaSauHaoHut = donGiaQuyDoiVAT * soLuongNhap / soLuongQuyDoi;
        }
        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            soLuongQuyDoi,
            donGiaSauHaoHut,
            phanTramHaoHut: e.value,
            donGiaQuyDoi: donGiaNhap,
            donGiaQuyDoiVAT
          }
        });
        break;
      }
      default: {
        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            [name]: e.value
          }
        });
      }
    }
  };
  handleSelectTenVatTu = () => {
    const selectedVatTuObj = this.refTenVatTu.current.state.selectedVatTuObj;
    this.setState({
      chiTietPhieuNhapObj: {...this.state.chiTietPhieuNhapObj,
        maVatTu: selectedVatTuObj.MAVATTU,
        hoatChat: selectedVatTuObj.HOATCHAT,
        dvt: selectedVatTuObj.DVT,
        soThau: selectedVatTuObj.MABAOCAO,
        donGiaTruocVAT: selectedVatTuObj.DONGIA_BV,
        donGiaSauHaoHut: selectedVatTuObj.DONGIA_BV,
        donGiaChinhSua: selectedVatTuObj.DONGIA_BV
      }
    });
    const nghiepVu = this.props.maNghiepVu;
    setTimeout(() => {
      if (nghiepVu === 23) {
        this.refSoLuongNhap.current.instance.focus();
      } else if (nghiepVu === 24) {
        this.refQuycach.current.instance.focus();
      } else if (nghiepVu === 52) {
        this.refSoLuongNhap.current.instance.focus();
      }
    }, 200);
  };
  handleEnterNextInput = (e, currentRef, nextRef) => {
    nextRef.current.instance.focus();
    switch (currentRef) {
      case this.refSoLo: {
        if (e.event.target.value === "") {
          notify({ message: "Số lô không được rỗng", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.refSoLo.current.instance.focus();
          return;
        }
        break;
      }
      case this.refNgayHetHan: {
        if (e.event.target.value === "" || e.event.target.value.length !== 10) {
          notify({ message: "Ngày hết hạn không hợp lệ. Kiểm tra lại", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.refNgayHetHan.current.instance.focus();
          return;
        }
        break;
      }
      case this.refQuycach: {
        if (this.state.chiTietPhieuNhapObj.quyCach === "") {
          this.refQuycach.current.instance.focus();
        }
        break;
      }
      case this.refSoLuongNhap: {
        const soLuong = e.event.target.value;
        if (parseFloat(e.event.target.value) === 0 || e.event.target.value === "" || Number.isNaN(parseFloat(e.event.target.value))) {
          notify({ message: "Số lượng phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.refSoLuongNhap.current.instance.focus();
          return;
        }
        const quyCach = this.state.chiTietPhieuNhapObj.quyCach;
        let quyCachQuyDoi = 0;
        let soLuongQuyDoi = 0;
        const index = quyCach.indexOf('/');
        if (index === -1) {
          quyCachQuyDoi = 1;
        } else {
          quyCachQuyDoi = parseInt(quyCach.substr(index + 1, quyCach.length),0);
        }
        soLuongQuyDoi = soLuong * quyCachQuyDoi;

        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            soLuong: e.event.target.value,
            quyCachQuyDoi,
            soLuongQuyDoi
          }
        });
        break;
      }
      case this.refDonGia: {
        const donGiaNhap = e.event.target.value;
        if (parseFloat(e.event.target.value) === 0 || e.event.target.value === "" || Number.isNaN(parseFloat(e.event.target.value))) {
          notify({ message: "Đơn giá phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          currentRef.current.instance.focus();
          return;
        }
        const soLuongNhap = this.state.chiTietPhieuNhapObj.soLuong;
        const quyCachQuyDoi = this.state.chiTietPhieuNhapObj.quyCachQuyDoi;
        const vat = this.props.phieuNhapKhoObj.vat;
        let donGiaQuyDoi = donGiaNhap / quyCachQuyDoi;
        let donGiaQuyDoiVAT = donGiaQuyDoi + donGiaQuyDoi * vat / 100;
        let thanhTien = donGiaNhap * soLuongNhap;
        const tienVAT = (donGiaNhap * vat / 100) * soLuongNhap;
        const thanhTienVAT = thanhTien + tienVAT;
        const haohut = this.state.chiTietPhieuNhapObj.phanTramHaoHut;
        let soLuongQuyDoi = this.state.chiTietPhieuNhapObj.soLuongQuyDoi;
        let donGiaSauHaoHut = 0;
        if (this.props.maNghiepVu === 23) {
          soLuongQuyDoi = soLuongNhap - soLuongNhap * haohut / 100;
        }
        if (getMoTaThamSo(this.props.listDanhSachThamSoDonVi, 53) === '1') {
          donGiaQuyDoi = Math.round(donGiaQuyDoi);
          donGiaQuyDoiVAT = Math.round(donGiaQuyDoiVAT);
        }
        donGiaSauHaoHut = donGiaQuyDoiVAT * soLuongNhap / soLuongQuyDoi;
        if (this.props.maNghiepVu === 23) {
          thanhTien = donGiaSauHaoHut * soLuongQuyDoi;
        }
        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            donGiaTruocVAT: e.event.target.value,
            quyCachQuyDoi,
            donGiaQuyDoiVAT,
            thanhTien,
            tienVAT,
            thanhTienVAT,
            donGiaSauHaoHut: this.props.maNghiepVu === 24 ? donGiaNhap : donGiaSauHaoHut,
            soLuongQuyDoi,
            donGiaQuyDoi,
            donGiaChinhSua: donGiaQuyDoiVAT
          }
        });
        break;
      }
      default: {
        return;
      }
    }
  };
  handleOnKeyPressDonGiaTruocVAT = (e) => {
    if (this.props.maNghiepVu === 23) {
      const donGiaNhap = e.event.target.value;
      const soLuongNhap = this.state.chiTietPhieuNhapObj.soLuong;
      const quyCachQuyDoi = this.state.chiTietPhieuNhapObj.quyCachQuyDoi;
      const vat = this.props.phieuNhapKhoObj.vat;
      let donGiaQuyDoi = donGiaNhap / quyCachQuyDoi;
      let donGiaQuyDoiVAT = donGiaQuyDoi + donGiaQuyDoi * vat / 100;
      let thanhTien = donGiaNhap * soLuongNhap;
      const tienVAT = (donGiaNhap * vat / 100) * soLuongNhap;
      const thanhTienVAT = thanhTien + tienVAT;
      const haohut = this.state.chiTietPhieuNhapObj.phanTramHaoHut;
      let soLuongQuyDoi = this.state.chiTietPhieuNhapObj.soLuongQuyDoi;
      let donGiaSauHaoHut = 0;
      if (this.props.maNghiepVu === 23) {
        soLuongQuyDoi = soLuongNhap - soLuongNhap * haohut / 100;
      }
      if (getMoTaThamSo(this.props.listDanhSachThamSoDonVi, 53) === '1') {
        donGiaQuyDoi = Math.round(donGiaQuyDoi);
        donGiaQuyDoiVAT = Math.round(donGiaQuyDoiVAT);
      }
      donGiaSauHaoHut = donGiaQuyDoiVAT * soLuongNhap / soLuongQuyDoi;
      if (this.props.maNghiepVu === 23) {
        thanhTien = donGiaSauHaoHut * soLuongQuyDoi;
      }
      this.setState({
        ...this.state,
        chiTietPhieuNhapObj: {
          ...this.state.chiTietPhieuNhapObj,
          donGiaTruocVAT: e.event.target.value,
          quyCachQuyDoi,
          donGiaQuyDoiVAT,
          thanhTien,
          tienVAT,
          thanhTienVAT,
          donGiaSauHaoHut,
          donGiaTruocHaoHut: this.state.chiTietPhieuNhapObj.donGiaQuyDoiVAT,
          soLuongQuyDoi,
          donGiaQuyDoi,
          donGiaChinhSua: donGiaQuyDoiVAT
        }
      });
      if (e.event.charCode === 13) {
        if (this.state.chiTietPhieuNhapObj.donGiaTruocVAT === 0 || Number.isNaN(parseFloat(e.event.target.value))) {
          notify({ message: "Đơn giá phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.refDonGia.current.instance.focus();
          return;
        }
        this.props.addChiTietNhapKhoDongY(this.state.chiTietPhieuNhapObj);
        setTimeout(() => {
          this.refTenVatTu.current.handleFocus();
        }, 200);
      }
    } else if (this.props.maNghiepVu === 52) {
      // console.log('nhập kho thường nè');
        const donGiaTruocVAT = e.event.target.value;
        const vat = this.props.phieuNhapKhoObj.vat;
        const soLuong = this.state.chiTietPhieuNhapObj.soLuong;
        let donGia = donGiaTruocVAT * vat / 10 + parseFloat(donGiaTruocVAT);
        if (getMoTaThamSo(this.props.listDanhSachThamSoDonVi, 53) === "1") {
          donGia = Math.round(donGia);
        }
        this.setState({
          ...this.state,
          chiTietPhieuNhapObj: {
            ...this.state.chiTietPhieuNhapObj,
            donGiaTruocVAT: e.event.target.value,
            donGia,
            thanhTien: donGia * soLuong
          }
        });
      if (e.event.charCode === 13) {
        if (this.state.chiTietPhieuNhapObj.donGiaTruocVAT === 0 || Number.isNaN(parseFloat(e.event.target.value))) {
          notify({ message: "Đơn giá phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
          this.refDonGia.current.instance.focus();
          return;
        }
        this.props.addChiTietNhapKho(this.state.chiTietPhieuNhapObj);
        setTimeout(() => {
          this.refTenVatTu.current.handleFocus();
        }, 200);
      }
    }
  };
  handleOnKeyPressDonGiaNhapKho = (e) => {
    this.setState({
      chiTietPhieuNhapObj: {
        ...this.state.chiTietPhieuNhapObj,
        donGiaChinhSua: parseFloat(e.event.target.value)
      }
    });
    if (e.event.charCode === 13) {
      if (this.state.chiTietPhieuNhapObj.donGiaChinhSua === 0 || Number.isNaN(parseFloat(e.event.target.value))) {
        notify({ message: "Đơn giá phải > 0", width: NOTIFY_WIDTH, shading: SHADING }, "error", NOTIFY_DISPLAYTIME);
        this.refDonGiaNhapKho.current.instance.focus();
        return;
      }
      this.refTenVatTu.current.handleFocus();
      this.props.addChiTietNhapKhoHoaDon(this.state.chiTietPhieuNhapObj);
      this.setState({
        chiTietPhieuNhapObj: defaultChiTietPhieuNhap
      });
      setTimeout(() => {
        this.refTenVatTu.current.handleFocus();
      }, 200);
    }
  };

  render() {
    return (
        <div>
          <table style={{ width: '100%' }}>
            <thead>
                <tr>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot1}}>Tên vật tư</td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot2}}>ĐVT</td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot3}}>Quy cách</td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot4}}>Số thầu</td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot5}}>SL nhập</td>
                  <td
                    className={'dx-datagrid-headers'}
                    style={{ width: columnsWidth.cot6 }}
                  >% Hao hụt</td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot7 }}>
                    {this.props.maNghiepVu === 23 ? "SL thực" : "SL quy đổi"}
                  </td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot8 }}>Số lô</td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot9 }}>Ngày hết hạn</td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot10 }}>Đơn giá</td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot11 }}>Đơn giá quy đổi</td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot12 }}>Đơn giá QĐ(VAT)</td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot13 }}>Đơn giá nhập kho</td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot14 }}>Giá bán</td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot15 }}>Tiền(VAT)</td>
                  <td className={'dx-datagrid-headers'} style={{ width: columnsWidth.cot16 }}>Thành tiền</td>
                </tr>
            </thead>
            <tbody>
              <tr>
                  <td>
                    <DropDownVatTu
                      ref={this.refTenVatTu}
                      className={'no-border-radius'}
                      listDanhSachVatTu={ this.props.maNghiepVu === 23 ? this.props.listDanhSachVatTu.filter(vattu => vattu.LATHUOC === 0) : this.props.listDanhSachVatTu}
                      dropWidth={screen.width-50}
                      dropHeight={500}
                      handleSelectTenVatTu = {this.handleSelectTenVatTu}
                      disabled={!this.props.enableInput}
                    />
                  </td>
                  <td>
                    <TextBox
                      className={'no-border-radius'}
                      width={'100%'}
                      disabled={true}
                      value={this.state.chiTietPhieuNhapObj.dvt}
                    />
                  </td>
                  <td>
                    <TextBox
                      ref={this.refQuycach}
                      className={'no-border-radius'}
                      width={'100%'}
                      disabled={!this.props.enableInput ||  this.props.maNghiepVu !== 24}
                      value={this.state.chiTietPhieuNhapObj.quyCach}
                      onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'quyCach')}
                      onEnterKey={e => this.handleEnterNextInput(e, this.refQuycach, this.refSoLuongNhap)}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Quy cách không được rỗng" />*/}
                      {/* </Validator>*/}
                    </TextBox>
                  </td>
                  <td>
                    <TextBox
                      className={'no-border-radius'}
                      width={'100%'}
                      disabled={true}
                      value={this.state.chiTietPhieuNhapObj.soThau}
                    />
                  </td>
                  <td>
                    <NumberBox
                      min={0}
                      ref={this.refSoLuongNhap}
                      className={'no-border-radius'}
                      width={'100%'}
                      onEnterKey={e => this.handleEnterNextInput(e, this.refSoLuongNhap,  this.props.maNghiepVu === 23 ? this.refPhanTramHaoHut : this.refSoLo)}
                      disabled={!this.props.enableInput}
                      value={this.state.chiTietPhieuNhapObj.soLuong}
                      onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'soLuong')}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Số lượng không được rỗng" />*/}
                      {/* </Validator>*/}
                    </NumberBox>
                  </td>
                  <td style={{ width: columnsWidth.cot6 }}>
                    <NumberBox
                      min={0}
                      ref={this.refPhanTramHaoHut}
                      className={'no-border-radius'}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.phanTramHaoHut}
                      disabled={!this.props.enableInput || this.props.maNghiepVu !== 23}
                      onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'phanTramHaoHut')}
                      onEnterKey={e => this.handleEnterNextInput(e, this.refPhanTramHaoHut, this.refSoLo)}
                    />
                  </td>
                  <td>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      width={'100%'}
                      disabled={true}
                      value={this.state.chiTietPhieuNhapObj.soLuongQuyDoi}
                    />
                  </td>
                  <td>
                    <TextBox
                      ref={this.refSoLo}
                      className={'no-border-radius'}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.soLoSanXuat}
                      onEnterKey={e => this.handleEnterNextInput(e, this.refSoLo, this.refNgayHetHan)}
                      onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'soLoSanXuat')}
                      disabled={!this.props.enableInput}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Số lô không được rỗng" />*/}
                      {/* </Validator>*/}
                    </TextBox>
                  </td>
                  <td>
                    <DateBox
                      showDropDownButton={false}
                      ref={this.refNgayHetHan}
                      className={'no-border-radius'}
                      displayFormat={'dd/MM/yyyy'}
                      value={this.state.chiTietPhieuNhapObj.ngayHetHan}
                      width={'100%'}
                      onEnterKey={e => this.handleEnterNextInput(e, this.refNgayHetHan, this.refDonGia)}
                      disabled={!this.props.enableInput}
                      openOnFieldClick={true}
                      onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'ngayHetHan')}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Ngày hết hạn không được rỗng" />*/}
                      {/* </Validator>*/}
                    </DateBox>
                  </td>
                  <td>
                    <NumberBox
                      min={0}
                      ref={this.refDonGia}
                      className={'no-border-radius'}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.donGiaTruocVAT}
                      onEnterKey={e => this.handleEnterNextInput(e, this.refDonGia,  this.props.maNghiepVu === 23 ? this.refSoLuongNhap : this.refDonGiaNhapKho)}
                      onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'donGiaTruocVAT')}
                      onKeyPress={this.handleOnKeyPressDonGiaTruocVAT}
                      disabled={!this.props.enableInput}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Đơn giá không được rỗng" />*/}
                      {/* </Validator>*/}
                    </NumberBox>
                  </td>
                  <td>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.donGiaQuyDoi}
                      disabled={true}
                    />
                  </td>
                  <td>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.donGiaQuyDoiVAT}
                      disabled={true}
                    />
                  </td>
                  <td>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      ref={this.refDonGiaNhapKho}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.donGiaChinhSua}
                      disabled={!this.props.enableInput ||  this.props.maNghiepVu !== 24}
                      // onValueChanged={e => this.handleOnchangeChiTietPhieuNhapNV23(e, 'donGiaChinhSua')}
                      // onEnterKey={e => this.handleEnterNextInput(e, this.refDonGiaNhapKho, this.refQuycach)}
                      onKeyPress={this.handleOnKeyPressDonGiaNhapKho}
                    >
                      {/* <Validator>*/}
                      {/*   <RequiredRule message="Đơn giá nhập kho không được rỗng" />*/}
                      {/* </Validator>*/}
                    </NumberBox>
                  </td>
                  <td>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.donGiaSauHaoHut}
                      disabled={true}
                    />
                  </td>
                  <td>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.tienVAT}
                      disabled={true}
                    />
                  </td>
                  <td>
                    <NumberBox
                      className={'no-border-radius'}
                      min={0}
                      width={'100%'}
                      value={this.state.chiTietPhieuNhapObj.thanhTien}
                      disabled={true}
                    />
                  </td>
                </tr>
            </tbody>
          </table>
        </div>
      );
  }
}

export default GridGoQuyCach;
