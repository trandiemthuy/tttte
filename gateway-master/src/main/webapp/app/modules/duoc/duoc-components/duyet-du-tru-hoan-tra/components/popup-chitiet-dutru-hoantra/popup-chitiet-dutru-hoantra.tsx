import React from "react";
import {
  IDataGridOptions
} from 'devextreme-react/data-grid';
import {Popup} from 'devextreme-react/popup';
import Box, {Item as BoxItem} from "devextreme-react/box";
import { ScrollView } from 'devextreme-react/scroll-view';
import GridDanhSachChiTietPhieuDuTruHoanTra
  from "app/modules/duoc/duoc-components/duyet-du-tru-hoan-tra/components/popup-chitiet-dutru-hoantra/grid-chitiet-dutru-hoantra";
import DuocLabelComponent from "app/modules/duoc/duoc-components/utils/duoc-label/duoc-label";

export interface IPopUpChiTietPhieuDuTruHoanTraProp extends IDataGridOptions {
  listDanhSachChiTietPhieuDuTruHoanTra: any;
  showPopUp: boolean;
  title: string;
  loaiPhieu: number; // 0: Dự trù    1: Hoàn trả
  isThoiGian: boolean; //
  handleChangePopUpChiTietState: Function;
}
export interface IPopUpChiTietPhieuDuTruHoanTraState {
  enableButtonDuyet: boolean;
}

export class PopUpChiTietPhieuDuTruHoanTra extends React.Component<IPopUpChiTietPhieuDuTruHoanTraProp, IPopUpChiTietPhieuDuTruHoanTraState> {
  private refGridDanhSach = React.createRef<GridDanhSachChiTietPhieuDuTruHoanTra>();
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Popup
        visible={this.props.showPopUp}
        onHiding={() => this.props.handleChangePopUpChiTietState(false, false)}
        dragEnabled={true}
        closeOnOutsideClick={false}
        showTitle={true}
        title={this.props.title}
        width={1000}
        height={450}
      >
        <ScrollView
          scrollByContent={true}
          scrollByThumb={true}
        >
        <Box
          direction={'col'}
          width={'100%'}
        >
          <BoxItem ratio={0} baseSize={'100%'}>
            <div className={'div-box-wrapper'}>
              <DuocLabelComponent text={'Chi tiết phiếu ' + this.props.loaiPhieu === '0' ? 'dự trù' : 'hoàn trả'}/>
              <GridDanhSachChiTietPhieuDuTruHoanTra
                ref={this.refGridDanhSach}
                loaiPhieu={this.props.loaiPhieu}
                isThoiGian={this.props.isThoiGian}
                listDanhSachChiTietPhieuDuTruHoanTra={this.props.listDanhSachChiTietPhieuDuTruHoanTra}
              />
            </div>
          </BoxItem>
        </Box>
        </ScrollView>
      </Popup>);
  }
}

export default PopUpChiTietPhieuDuTruHoanTra;
