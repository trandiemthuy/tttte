export const VienPhiServicesUrl: Readonly<string> = '/services/vienphi';
export const ToaThuocTheoNghiepVu: Readonly<Array<{}>> = [
  {
    ID: 1,
    tenNghiepVu: 'Toa thuốc BHYT',
    maNghiepVu: 'toaThuocBHYT'
  },
  {
    ID: 2,
    tenNghiepVu: 'Toa vật tư BHYT',
    maNghiepVu: 'toaVattuBHYT'
  },
  {
    ID: 3,
    tenNghiepVu: 'Toa mua ngoài',
    maNghiepVu: ''
  },
  {
    ID: 4,
    tenNghiepVu: 'Toa miễn phí',
    maNghiepVu: ''
  },
  {
    ID: 5,
    tenNghiepVu: 'Toa mua tại quầy BV',
    maNghiepVu: ''
  },
  {
    ID: 6,
    tenNghiepVu: 'Toa dịch vụ',
    maNghiepVu: ''
  },
  {
    ID: 7,
    tenNghiepVu: 'Toa đông y',
    maNghiepVu: ''
  },
  {
    ID: 8,
    tenNghiepVu: 'Toa tổng hợp',
    maNghiepVu: ''
  }
];
