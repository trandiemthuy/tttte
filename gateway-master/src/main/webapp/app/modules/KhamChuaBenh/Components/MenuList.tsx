import React from 'react';
// import {
//   getDanhSachKhamBenh, setSelectedIndexTab, setSelectedPatient
// } from '../DanhSachKhamBenh/DanhSachKhamBenh.reducer';
// import { IRootState } from "app/shared/reducers";
// import { connect } from 'react-redux';
import { Button } from 'devextreme-react';

export interface IMenuList {
  thongTinChanDoanValues: object;
  chiSoSinhTonValues: object;
}

export const MenuList = (props: IMenuList) => {
  const handleKhamOnClick = () => {
    const test = {'text': 'fasdfaf'};
    // eslint-disable-next-line no-console
    console.log(test);
    test.text="fasfsadfasd";
    // eslint-disable-next-line no-console
    console.log(test);
    // eslint-disable-next-line no-console
    console.log(props.chiSoSinhTonValues);
    // eslint-disable-next-line no-console
    console.log(props.thongTinChanDoanValues);
  };
  return (
    <React.Fragment>
      <div className="mt-3 container d-flex justify-content-center">
        <div className={'rounded-left bg-shadow-light p-1'}>
          <Button
            width={120}
            text="Khám"
            onClick={handleKhamOnClick}
            type="default"
            stylingMode="contained"
            className={'mr-1'}
            // onClick={this.onClick}
          />
          <Button
            width={120}
            text="Sửa"
            type="default"
            stylingMode="contained"
            className={'mr-1'}
            // onClick={this.onClick}
          />
          <Button
            width={120}
            text="Lưu"
            type="default"
            stylingMode="contained"
            className={'mr-1'}
            disabled={true}
            // onClick={this.onClick}

          />
          <Button
            width={120}
            text="Hủy"
            type="default"
            stylingMode="contained"
            className={'mr-1'}
            disabled={true}
            // onClick={this.onClick}
          />
        </div>
        <div className={'rounded-right bg-shadow-light p-1 ml-1'}>
        <Button
          width={120}
          text="Chỉ định DV"
          type="success"
          stylingMode="contained"
          className={'mr-1'}
          // onClick={this.onClick}
        />
        <Button
          width={120}
          text="Hoàn tất khám"
          type="danger"
          stylingMode="contained"
          className={'mr-1'}
          // onClick={this.onClick}
        />
        <Button
          width={120}
          text="In bảng kê"
          type="default"
          stylingMode="contained"
          className={'mr-1'}
          // onClick={this.onClick}
        />
        </div>
      </div>
    </React.Fragment>
  );

};

// const mapStateTomrops = (storeState: IRootState) => ({
//   selectedPatient: storeState.danhsachkhambenh.selectedPatient
// });

// const mapDispatchTomrops = {
//   getDanhSachKhamBenh,
//   setSelectedIndexTab,
//   setSelectedPatient
// };
//
//
// type Statemrops = ReturnType<typeof mapStateTomrops>;
// type Dispatchmrops = typeof mapDispatchTomrops;

export default MenuList;
