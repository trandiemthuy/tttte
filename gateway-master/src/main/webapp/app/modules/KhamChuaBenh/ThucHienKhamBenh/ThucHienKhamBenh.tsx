import React from 'react';
import { CheckBox } from 'devextreme-react/check-box';
import { Form, TextBox } from 'devextreme-react';
import { GroupItem } from 'devextreme-react/form';

import {
  getDanhSachKhamBenh,
  setSelectedIndexTab,
  setSelectedPatient
} from '../DanhSachKhamBenh/DanhSachKhamBenh.reducer';

import { ChiSoSinhTon } from 'app/modules/KhamChuaBenh/ChiSoSinhTon/ChiSoSinhTon';
import { ThongTinChanDoan } from 'app/modules/KhamChuaBenh/ThongTinChanDoan/ThongTinChanDoan';
import { MenuList } from 'app/modules/KhamChuaBenh/Components/MenuList';
import { KeToaThuoc } from 'app/modules/KhamChuaBenh/KeToaThuoc/KeToaThuoc';
import { ToaThuocTheoNghiepVu } from 'app/modules/KhamChuaBenh/Utils/KhamChuaBenhConst';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';


export interface IThucHienKhamBenh extends StateProps, DispatchProps {
  url: string
}

export const ThucHienKhamBenh = (props: IThucHienKhamBenh) => {

  // const formatter = function(date) {
  //   const month = date.getMonth() + 1,
  //     day = date.getDate(),
  //     year = date.getFullYear();
  //
  //   return day + '/' + month + '/' + year;
  // };
  const formatDate = new Intl.DateTimeFormat('vi-VN').format;
  const rowTemp = function(rowInfo) {
    return <tbody>
    <tr
      className={`employee dx-row ${rowInfo.data.uuTien === true ? 'bg bg-primary font-weight-bold text-white' : ''}`}>
      <td>{rowInfo.data.stt}</td>
      <td>{formatDate(new Date(rowInfo.data.ngayDangKy))}</td>
      <td>{rowInfo.data.maBn}</td>
      <td>{rowInfo.data.hoTen}</td>
      <td>{formatDate(new Date(rowInfo.data.namSinh))}</td>
      <td>{rowInfo.data.doiTuong}</td>
      <td>{rowInfo.data.tenDichVu}</td>
      <td>{rowInfo.data.soPhieu}</td>
      <td>{<CheckBox defaultValue={rowInfo.data.uuTien === true} disabled={true}/>}</td>
      <td>{rowInfo.data.trangThai}</td>
      <td>{rowInfo.data.thanhToan}</td>
    </tr>
    </tbody>;
  };
  const initialValues = props.selectedPatient;
  const thongTinChanDoanValues = {
    'kqNhanDinh': '',
    'trieuChungLS': '',
    'chanDoanICD': '',
    'benhPhu': '',
    'tntt': '',
    'loiDanKham': '',
    'giaiQuyet': ''
  };
  const chiSoSinhTonValues = {
    'mach': 55,
    'nhipTho': 56,
    'chieuCao': 50,
    'canNang': 50,
    'nhietDo': 50,
    'vongBung': 50,
    'huyetAp1': 10,
    'huyetAp2': 50,
    'creatinin': 50,
    'bmi': 50,
    'doThanhThai': 50,
    'nhomMau': '2',
    'khangThe': '1'
  };
  const handleOnChange = (field, values) => {
    const test1 = {
      [field]: values
    };
    Object.assign(initialValues, test1);
  };
  const test = (e) => {
    e.preventDefault();
    // eslint-disable-next-line no-console
    console.log(chiSoSinhTonValues); console.log(thongTinChanDoanValues);
  };
  const buttonOptions = {
    text: 'OK',
    type: 'success',
    useSubmitBehavior: true
  };
  return (
    <React.Fragment>
      <div className="p-3">
        <div>
          {/* <ThongTinHanhChinh initialValues={props.selectedPatient} onChange={handleOnChange} /> */}
          {/* <TextBox value={props.selectedPatient.stt}/>*/}
          {/* <TextBox value={props.selectedPatient.maYT}/>*/}
          {/* <TextBox value={props.selectedPatient.ngaySinh}/>*/}
          {/* <TextBox value={props.selectedPatient.hoTen}/>*/}

        </div>
        <form onSubmit={test}>
          <Form
            colCount={3}
            id="form"
            formData={{}}>
            <GroupItem colSpan={2} cssClass="bg bg-light mt-3 pt-0 pl-3 pb-4 bg-shadow-light">
              <ThongTinChanDoan thongTinChanDoanValues={thongTinChanDoanValues}/>
            </GroupItem>
            <GroupItem colSpan={1} cssClass="bg bg-light mt-3 ml-2 pt-0 pl-3 pr-3 bg-shadow-light">
              <ChiSoSinhTon chiSoSinhTonValues={chiSoSinhTonValues}/>
            </GroupItem>
            {/* <ButtonItem horizontalAlignment="left"*/}
            {/*    buttonOptions={buttonOptions}*/}
            {/* />*/}
          </Form>
        </form>
        <MenuList thongTinChanDoanValues={thongTinChanDoanValues} chiSoSinhTonValues={chiSoSinhTonValues}/>
        <KeToaThuoc dataSource={ToaThuocTheoNghiepVu}/>
      </div>
    </React.Fragment>
  );

};

const mapStateToProps = (storeState: IRootState) => ({
  selectedPatient: storeState.danhsachkhambenh.selectedPatient
});

const mapDispatchToProps = {
  getDanhSachKhamBenh,
  setSelectedIndexTab,
  setSelectedPatient
};


type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps,
  mapDispatchToProps)(ThucHienKhamBenh);
