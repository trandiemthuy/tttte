import React, { useState } from 'react';
import Form, { GroupItem, Label, SimpleItem } from 'devextreme-react/form';
import 'devextreme-react/autocomplete';
import './ChiSoSinhTon.scss';
import { translate } from 'react-jhipster';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IChiSoSinhTon {
  chiSoSinhTonValues: object
}

const chiSoSinhTonI18N = 'khamChuaBenh.thucHienKhamBenh.chiSoSinhTon.';

export const ChiSoSinhTon = (props: IChiSoSinhTon) => {
  const [BMIValue, setBMIValue] = useState(0);
  const [canNangBMI, setCanNangBMI] = useState(0);
  const [chieuCaoBMI, setChieuCaoBMI] = useState(0);
  const test = (e) => {

  };
  // eslint-disable-next-line no-console
  return (
    <React.Fragment>
      <Form
        formData={props.chiSoSinhTonValues}
        readOnly={false}
        showColonAfterLabel={true}
        showValidationSummary={true}
        validationGroup="customerData"
        onFieldDataChanged={test}
        onOptionChanged={test}
        labelLocation="top"
        id="chi-so-sinh-ton"
      >

        <GroupItem caption={translate(chiSoSinhTonI18N + 'form.caption')} cssClass="h5 pt-4" colCount={12}>
          <SimpleItem colSpan={4} dataField="mach" editorType="dxNumberBox" helpText="(lần/phút)">
            <Label text={translate(chiSoSinhTonI18N + 'form.mach')}/>
            {/* <RequiredRule message="Email is required" />
            <EmailRule message="Email is invalid" />
            <AsyncRule
              message="Email is already registered"
            //   validationCallback={asyncValidation}
            /> */}
          </SimpleItem>
          <SimpleItem colSpan={4} dataField="nhipTho" editorType="dxNumberBox" helpText="(lần/phút)"
            //   editorOptions={this.passwordOptions}
          >
            <Label text="Nhịp thở"/>
            {/* <RequiredRule message="Password is required" /> */}
          </SimpleItem>
          <SimpleItem colSpan={4} dataField="chieuCao" editorType="dxNumberBox" helpText="(cm)"
            // editorOptions={chieuCaoOptions}
            //   editorOptions={this.passwordOptions}
          >
            <Label text={translate(chiSoSinhTonI18N + 'form.chieuCao')}/>
            {/* <RequiredRule message="Confirm Password is required" />
              <CompareRule
                message="Password and Confirm Password do not match"
              //   comparisonTarget={this.passwordComparison}
              /> */}
          </SimpleItem>
        </GroupItem>
        <GroupItem cssClass="h5" colCount={12}>


          <SimpleItem colSpan={4} dataField="canNang" editorType="dxNumberBox" helpText="(kg)"
            //   editorOptions={this.passwordOptions}
          >
            <Label text={translate(chiSoSinhTonI18N + 'form.canNang')}/>
            {/* <RequiredRule message="Confirm Password is required" />
              <CompareRule
                message="Password and Confirm Password do not match"
              //   comparisonTarget={this.passwordComparison}
              /> */}
          </SimpleItem>
          <SimpleItem colSpan={4} dataField="nhietDo" editorType="dxNumberBox" helpText="(độ C)"
            //   editorOptions={this.passwordOptions}
          >
            <Label text={translate(chiSoSinhTonI18N + 'form.nhietDo')}/>
            {/* <RequiredRule message="Confirm Password is required" />
            <CompareRule
              message="Password and Confirm Password do not match"
            //   comparisonTarget={this.passwordComparison}
            /> */}
          </SimpleItem>

          <SimpleItem colSpan={4} dataField="vongBung" editorType="dxNumberBox" helpText="(cm)"
            //   editorOptions={this.passwordOptions}
          >
            <Label text={translate(chiSoSinhTonI18N + 'form.vongBung')}/>
            {/* <RequiredRule message="Confirm Password is required" />
            <CompareRule
              message="Password and Confirm Password do not match"
            //   comparisonTarget={this.passwordComparison}
            /> */}
          </SimpleItem>
        </GroupItem>

        <GroupItem colCount={12}>
          <GroupItem colCount={4} colSpan={4}>
            <Label text={translate(chiSoSinhTonI18N + 'form.huyetAp')}/>
            <SimpleItem colSpan={2} dataField='huyetAp1' editorType="dxNumberBox" cssClass="mr-1 pr-0">
              <Label visible={false}/>
              {/* <RequiredRule message="Confirm Password is required" />
              <CompareRule
                message="Password and Confirm Password do not match"
              //   comparisonTarget={this.passwordComparison}
              /> */}
            </SimpleItem>
            <SimpleItem colSpan={2} dataField='huyetAp2' editorType="dxNumberBox" helpText=" (mmhg)"
                        cssClass='pl-0 ml-0'
              //   editorOptions={this.passwordOptions}
            >
              <Label visible={false}/>
              {/* <RequiredRule message="Confirm Password is required" />
              <CompareRule
                message="Password and Confirm Password do not match"
              //   comparisonTarget={this.passwordComparison}
              /> */}
            </SimpleItem>
          </GroupItem>
          {/* <SimpleItem colSpan={6} dataField={['huyetAp1', 'huyetAp2']} editorType="dxNumberBox" helpText="(mmhg)" component={HuyetAp}
            //   editorOptions={this.passwordOptions}
            >
              <Label text="Huyết áp" />
              <RequiredRule message="Confirm Password is required" />
              <CompareRule
                message="Password and Confirm Password do not match"
              //   comparisonTarget={this.passwordComparison}
              />
            </SimpleItem> */}

          <SimpleItem colSpan={4} dataField="creatinin" editorType="dxNumberBox" helpText="(mg%)"
            //   editorOptions={this.passwordOptions}
          >
            <Label text={translate(chiSoSinhTonI18N + 'form.creatinin')}/>
            {/* <RequiredRule message="Confirm Password is required" />
            <CompareRule
              message="Password and Confirm Password do not match"
            //   comparisonTarget={this.passwordComparison}
            /> */}
          </SimpleItem>
          <SimpleItem colSpan={4} editorType="dxNumberBox"
            //   editorOptions={this.passwordOptions}
          >
            <Label text={translate(chiSoSinhTonI18N + 'form.bmi')}/>
            {/* <RequiredRule message="Confirm Password is required" />
            <CompareRule
              message="Password and Confirm Password do not match"
            //   comparisonTarget={this.passwordComparison}
            /> */}
          </SimpleItem>
        </GroupItem>
        <GroupItem cssClass="h5" colCount={12}>
          <SimpleItem colSpan={4} dataField="doThanhThai" editorType="dxNumberBox" helpText="(ml/min/1.73m2)"
            //   editorOptions={this.passwordOptions}
          >
            <Label text={translate(chiSoSinhTonI18N + 'form.doThanhThai')}/>
            {/* <RequiredRule message="Confirm Password is required" />
            <CompareRule
              message="Password and Confirm Password do not match"
            //   comparisonTarget={this.passwordComparison}
            /> */}
          </SimpleItem>
          <SimpleItem colSpan={4} dataField="nhomMau" isRequired editorType="dxSelectBox"
                      editorOptions={{
                        dataSource: [
                          {
                            name: 'A',
                            value: '0'
                          },
                          {
                            name: 'B',
                            value: '1'
                          },
                          {
                            name: 'O',
                            value: '2'
                          },
                          {
                            name: 'AB',
                            value: '3'
                          }
                        ],
                        displayExpr: 'name',
                        valueExpr: 'value',
                        value: '0'
                      }}
          >
            <Label text={translate(chiSoSinhTonI18N + 'form.nhomMau')}/>
          </SimpleItem>
          <SimpleItem colSpan={4} dataField="khangThe" isRequired editorType="dxSelectBox"
                      editorOptions={{
                        dataSource: [
                          {
                            name: 'RH+',
                            value: '0'
                          },
                          {
                            name: 'RH-',
                            value: '1'
                          }
                        ],
                        displayExpr: 'name',
                        valueExpr: 'value',
                        value: '0'
                      }}
          >
            <Label text={translate(chiSoSinhTonI18N + 'form.khangThe')}/>
          </SimpleItem>
        </GroupItem>

      </Form>
    </React.Fragment>
  );
};

export default ChiSoSinhTon;
