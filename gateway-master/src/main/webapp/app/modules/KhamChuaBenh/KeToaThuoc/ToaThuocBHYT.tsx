import React from 'react';
// import {
//   getDanhSachKhamBenh, setSelectedIndexTab, setSelectedPatient
// } from '../DanhSachKhamBenh/DanhSachKhamBenh.reducer';
// import { IRootState } from "app/shared/reducers";
// import { connect } from 'react-redux';
import { Form } from 'devextreme-react';
import { ButtonItem, GroupItem, Label, SimpleItem } from 'devextreme-react/form';
import ThuocDaThemGrid from 'app/modules/KhamChuaBenh/KeToaThuoc/ThuocDaThemGrid';

export interface IToaThuocBHYT {
  data: any
}

export const ToaThuocBHYT = (props: IToaThuocBHYT) => {
  return (
    <div className={'m-3'}>
      <Form>
        <GroupItem colCount={2}>
          <SimpleItem colSpan={1} editorType={'dxTextBox'} editorOptions={{ 'readOnly': true }}>
            <Label text={'BHYT còn lại'}/>
          </SimpleItem>
          <ButtonItem horizontalAlignment="left" colSpan={1}
                      buttonOptions={{
                        text: 'Xóa toa',
                        type: 'danger',
                        useSubmitBehavior: true,
                        disabled: true
                      }}
          />
        </GroupItem>
        <GroupItem colCount={2}>
          <SimpleItem colSpan={1} editorType={'dxTextBox'}>
            <Label text={'Lời dặn'}/>
          </SimpleItem>
          <SimpleItem colSpan={1} editorType={'dxSelectBox'} editorOptions={{
            // placeholder: 'Hình thức thanh toán',
            dataSource: [{ 'name': 'Kho ngoại trú', 'value': 1 }, { 'name': 'Kho miễn phí', 'value': 2 }],
            displayExpr: 'name',
            valueExpr: 'value',
            value: 1
          }}>
            <Label visible={false}/>
          </SimpleItem>
        </GroupItem>

        <GroupItem colCount={2}>
          <SimpleItem colSpan={1} editorType={'dxTextBox'}>
            <Label text={'Tên thương mại'}/>
          </SimpleItem>
          <SimpleItem colSpan={1} editorType={'dxTextBox'}>
            <Label text={'Tên thuốc/hoạt chất'}/>
          </SimpleItem>
        </GroupItem>

        <GroupItem colCount={6}>
          <SimpleItem colSpan={1} editorType={'dxTextBox'}>
            <Label text={'Số ngày uống'}/>
          </SimpleItem>
          <SimpleItem colSpan={1} editorType={'dxTextBox'}>
            <Label text={'Sáng'}/>
          </SimpleItem>
          <SimpleItem colSpan={1} editorType={'dxTextBox'}>
            <Label text={'Trưa'}/>
          </SimpleItem>
          <SimpleItem colSpan={1} editorType={'dxTextBox'}>
            <Label text={'Chiều'}/>
          </SimpleItem>
          <SimpleItem colSpan={1} editorType={'dxTextBox'}>
            <Label text={'Tối '}/>
          </SimpleItem>
          <SimpleItem colSpan={1} editorType={'dxTextBox'}>
            <Label text={'Số lượng'}/>
          </SimpleItem>
        </GroupItem>
        <GroupItem colCount={3}>
          <SimpleItem colSpan={1} editorType={'dxTextBox'}>
            <Label text={'Cách dùng'}/>
          </SimpleItem>
          <SimpleItem colSpan={1} editorType={'dxTextBox'}>
            <Label text={'Đơn giá'}/>
          </SimpleItem>
          <SimpleItem colSpan={1} editorType={'dxTextBox'}>
            <Label text={'Thành tiền'}/>
          </SimpleItem>
        </GroupItem>
      </Form>
      <div className='mt-3'>
        <ThuocDaThemGrid data={''}/>
      </div>
    </div>
  );

};

// const mapStateTomrops = (storeState: IRootState) => ({
//   selectedPatient: storeState.danhsachkhambenh.selectedPatient
// });

// const mapDispatchTomrops = {
//   getDanhSachKhamBenh,
//   setSelectedIndexTab,
//   setSelectedPatient
// };
//
//
// type Statemrops = ReturnType<typeof mapStateTomrops>;
// type Dispatchmrops = typeof mapDispatchTomrops;

export default ToaThuocBHYT;
