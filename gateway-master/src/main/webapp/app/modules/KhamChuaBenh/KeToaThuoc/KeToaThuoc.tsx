import React, { useState } from 'react';
// import {
//   getDanhSachKhamBenh, setSelectedIndexTab, setSelectedPatient
// } from '../DanhSachKhamBenh/DanhSachKhamBenh.reducer';
// import { IRootState } from "app/shared/reducers";
// import { connect } from 'react-redux';
import { TabPanel } from 'devextreme-react';
import ToaThuocBHYT from 'app/modules/KhamChuaBenh/KeToaThuoc/ToaThuocBHYT';
import ToaVatTuBHYT from 'app/modules/KhamChuaBenh/KeToaThuoc/ToaVatTuBHYT';

export interface IKeToaThuoc {
  dataSource: Readonly<Array<{}>>
}

export interface IListToaThuoc {
  data: any
}

export const KeToaThuoc = (props: IKeToaThuoc) => {
  const [selectedIndex, setSelectedIndex] = useState(0);
  const onSelectionChanged = (args) => {
    if (args.name === 'selectedIndex') {
      setSelectedIndex(args.value);
    }
  };
  const itemTitleRender = (ToaThuoc) =>
    <span>{ToaThuoc.tenNghiepVu}</span>;

  const ListToaThuoc = (e) => {
    switch (e.data.maNghiepVu) {
      case 'toaThuocBHYT':
        return <ToaThuocBHYT data={''}/>;
      case 'toaVattuBHYT':
        return <ToaVatTuBHYT data={''}/>;
      default:
        return <div>Toa khác</div>;
    }
  };
  return (
    <React.Fragment>
      <div className="mt-3 bg-shadow-light">
        <TabPanel
          dataSource={props.dataSource}
          selectedIndex={selectedIndex}
          onOptionChanged={onSelectionChanged}
          itemTitleRender={itemTitleRender}
          itemComponent={ListToaThuoc}
          // animationEnabled={true}
          swipeEnabled={true}
          loop={true}
        />
      </div>
    </React.Fragment>
  );

};

// const mapStateTomrops = (storeState: IRootState) => ({
//   selectedPatient: storeState.danhsachkhambenh.selectedPatient
// });

// const mapDispatchTomrops = {
//   getDanhSachKhamBenh,
//   setSelectedIndexTab,
//   setSelectedPatient
// };
//
//
// type Statemrops = ReturnType<typeof mapStateTomrops>;
// type Dispatchmrops = typeof mapDispatchTomrops;

export default KeToaThuoc;
