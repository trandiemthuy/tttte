import React from 'react';
import { translate } from 'react-jhipster';
import DataGrid, { Column, FilterRow } from 'devextreme-react/data-grid';

export interface IThuocDaThemGrid {
  data: any
}

const ThuocDaThemGridI18N = 'khamChuaBenh.thucHienKhamBenh.keToaThuoc.thuocDaThemGrid.';
export const ThuocDaThemGrid = (props: IThuocDaThemGrid) => {
  const dataSource = [
    {
      'tenThuongMai' : 'Aeroflu 125 HFA 1',
      'tenGoc' : 'Salmeterol  fluticason probionat',
      'donViTinh' : 'lọ',
      'soNgay' : 1,
      'sang' : 1,
      'trua' : 1,
      'chieu' : 0,
      'toi' : 1,
      'soLuong': 3,
      'dangThuoc': 'lọ',
      'cachDung': 'uống',
      'donGia': 30000,
      'thanhTien': 90000
    },{
      'tenThuongMai' : 'Aeroflu 125 HFA 2',
      'tenGoc' : 'Salmeterol  fluticason probionat',
      'donViTinh' : 'lọ',
      'soNgay' : 1,
      'sang' : 1,
      'trua' : 1,
      'chieu' : 0,
      'toi' : 1,
      'soLuong': 3,
      'dangThuoc': 'lọ',
      'cachDung': 'uống',
      'donGia': 30000,
      'thanhTien': 90000
    },{
      'tenThuongMai' : 'Aeroflu 125 HFA 3',
      'tenGoc' : 'Salmeterol  fluticason probionat',
      'donViTinh' : 'lọ',
      'soNgay' : 1,
      'sang' : 1,
      'trua' : 1,
      'chieu' : 0,
      'toi' : 1,
      'soLuong': 3,
      'dangThuoc': 'lọ',
      'cachDung': 'uống',
      'donGia': 30000,
      'thanhTien': 90000
    }
  ];
  return (
    <React.Fragment>
      <DataGrid
        id="gridContainer"
        dataSource={dataSource}
        allowColumnReordering={true}
        columnAutoWidth={true}
        allowColumnResizing={true}
        showBorders={true}
        showColumnLines={true}
        showRowLines={true}
        // eslint-disable-next-line no-console
        // onRowDblClick={handleDblClick}
        // rowAlternationEnabled={true}
        // rowRender={rowTemp}
        // loadPanel={loadPanel}
        selection={{ mode: 'single' }}
        height={'250'}
      >
        <FilterRow
          visible={true}
        />
         <Column
          dataField="tenThuongMai"
          caption={translate(ThuocDaThemGridI18N + 'tenThuongMai')}
          dataType="number"
          // format="currency"
          alignment="center"
         />
         <Column
          dataField="tenGoc"
          caption={translate(ThuocDaThemGridI18N + 'tenGoc')}
          dataType="number"
          // format="currency"
          alignment="center"
         />
         <Column
          dataField="donViTinh"
          caption={translate(ThuocDaThemGridI18N + 'donViTinh')}
          dataType="number"
          // format="currency"
          alignment="center"
         />
         <Column
          dataField="soNgay"
          caption={translate(ThuocDaThemGridI18N + 'soNgay')}
          dataType="number"
          // format="currency"
          alignment="center"
         />
         <Column
          dataField="sang"
          caption={translate(ThuocDaThemGridI18N + 'sang')}
          dataType="number"
          // format="currency"
          alignment="center"
         />
         <Column
          dataField="trua"
          caption={translate(ThuocDaThemGridI18N + 'trua')}
          dataType="number"
          // format="currency"
          alignment="center"
         />
         <Column
          dataField="chieu"
          caption={translate(ThuocDaThemGridI18N + 'chieu')}
          dataType="number"
          // format="currency"
          alignment="center"
         />
         <Column
          dataField="toi"
          caption={translate(ThuocDaThemGridI18N + 'toi')}
          dataType="number"
          // format="currency"
          alignment="center"
         />
         <Column
          dataField="soLuong"
          caption={translate(ThuocDaThemGridI18N + 'soLuong')}
          dataType="number"
          // format="currency"
          alignment="center"
         />
         <Column
          dataField="dangThuoc"
          caption={translate(ThuocDaThemGridI18N + 'dangThuoc')}
          dataType="number"
          // format="currency"
          alignment="center"
         />
         <Column
          dataField="cachDung"
          caption={translate(ThuocDaThemGridI18N + 'cachDung')}
          dataType="number"
          // format="currency"
          alignment="center"
         />
         <Column
          dataField="donGia"
          caption={translate(ThuocDaThemGridI18N + 'donGia')}
          dataType="number"
          // format="currency"
          alignment="center"
         />
         <Column
          dataField="thanhTien"
          caption={translate(ThuocDaThemGridI18N + 'thanhTien')}
          dataType="number"
          // format="currency"
          alignment="center"
         />
      </DataGrid>
    </React.Fragment>
  );

};


export default ThuocDaThemGrid;
