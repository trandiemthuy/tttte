import React from 'react';
import { Item, TabPanel } from 'devextreme-react/tab-panel';
import DSKhamBenh from './DanhSachKhamBenh/DanhSachKhamBenh';
import ThucHienKhamBenh from './ThucHienKhamBenh/ThucHienKhamBenh';
import StethoScope from 'app/shared/assets/icons/stethoscope.svg';
import List from 'app/shared/assets/icons/list.svg';
import { IRootState } from 'app/shared/reducers';
import { setSelectedIndexTab } from './DanhSachKhamBenh/DanhSachKhamBenh.reducer';
import { connect } from 'react-redux';
import { translate } from 'react-jhipster';
export interface IKhamChuaBenh extends StateProps, DispatchProps {
  url: string
}

const khamChuaBenhI18N = 'khamChuaBenh.tabs.';

export const KhamChuaBenh = (props: IKhamChuaBenh) => {
  const onSelectionChanged = (args) => {
    if (args.name === 'selectedIndex') {
      props.setSelectedIndexTab(args.value);
    }
  };
  return (
    <div className="p-3">
      <TabPanel selectedIndex={props.selectedIndex} onOptionChanged={onSelectionChanged}>
        <Item title={translate(khamChuaBenhI18N + 'danhSachBenhNhan')} icon={List} component={DSKhamBenh}/>
        <Item title={translate(khamChuaBenhI18N + 'khamBenh')} icon={StethoScope} component={ThucHienKhamBenh}/>
      </TabPanel>
    </div>
  );

};
const mapStateToProps = (storeState: IRootState) => ({
  selectedIndex: storeState.danhsachkhambenh.selectedIndex
});

const mapDispatchToProps = {
  setSelectedIndexTab
};


type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;


export default connect(mapStateToProps,
  mapDispatchToProps)(KhamChuaBenh);
