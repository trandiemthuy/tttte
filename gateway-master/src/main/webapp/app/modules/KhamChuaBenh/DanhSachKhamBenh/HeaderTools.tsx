import React, { useState } from 'react';

import { translate } from 'react-jhipster';
import AdvancedDateBox from 'app/shared/components/AdvancedDateBox';
import Stethoscope from 'app/shared/assets/icons/stethoscope.svg';
import Done from 'app/shared/assets/icons/done.svg';

import Form, { GroupItem, Label, SimpleItem } from 'devextreme-react/form';
import { Button, Tabs } from 'devextreme-react';

import {
  getDanhSachKhamBenh,
  setSelectedIndexTab,
  setSelectedPatient
} from '../DanhSachKhamBenh/DanhSachKhamBenh.reducer';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';

export interface IHeaderTools extends StateProps, DispatchProps {
  url: string
}

const headerToolsI18N = 'khamChuaBenh.danhSachBenhNhan.headerTools.';

export const HeaderTools = (props: IHeaderTools) => {
  const handleClick = () => {
    props.getDanhSachKhamBenh();
  };
  const [tuNgay, setTuNgay] = useState();
  const [denNgay, setDenNgay] = useState();
  const tuNgayDateBox = () => {
    return <div><AdvancedDateBox dateValue={tuNgay} onSetDateValue={setTuNgay}/></div>;
  };
  const denNgayDateBox = () => {
    return <div><AdvancedDateBox dateValue={denNgay} onSetDateValue={setDenNgay}/></div>;
  };
  return (
    <div>
      <Form>
        <GroupItem colCount={10}>
          <SimpleItem colSpan={3} dataField="tuNgay" isRequired component={tuNgayDateBox}>
            <Label text={translate(headerToolsI18N + 'tuNgay')}/>
          </SimpleItem>
          <SimpleItem colSpan={3} dataField="denNgay" isRequired component={denNgayDateBox}>
            <Label text={translate(headerToolsI18N + 'denNgay')}/>
          </SimpleItem>
          <SimpleItem colSpan={3}>
            <Tabs
              dataSource={[
                {
                  id: 0,
                  text: translate(headerToolsI18N + 'choKham'),
                  icon: 'clock'
                },
                {
                  id: 1,
                  text: translate(headerToolsI18N + 'dangKham'),
                  icon: Stethoscope
                },
                {
                  id: 2,
                  text: translate(headerToolsI18N + 'daKham'),
                  icon: Done
                }
              ]}
              // onOptionChanged={this.onTabsSelectionChanged}
            />
          </SimpleItem>
          <GroupItem colSpan={3} cssClass="p-0">
            <div className="d-flex">
              <Button icon="search" type="default" stylingMode="contained" onClick={handleClick}/>
              <Button icon="clear" className="ml-2" type="default" stylingMode="outlined"/>
            </div>
          </GroupItem>
        </GroupItem>
      </Form>
    </div>
  );
};
HeaderTools.defaultProps = {
  url: ''
};

const mapStateToProps = (storeState: IRootState) => ({
  selectedPatient: storeState.danhsachkhambenh.selectedPatient
});

const mapDispatchToProps = {
  getDanhSachKhamBenh,
  setSelectedIndexTab,
  setSelectedPatient
};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps,
  mapDispatchToProps)(HeaderTools);
