import axios from 'axios';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';
import notify from 'devextreme/ui/notify';

export const ACTION_TYPES = {
  GET_DANH_SACH_KHAM_BENH: 'khambenh/GET_DANH_SACH_KHAMBENH',
  SET_SELETED_INDEX_TAB: 'khambenh/SET_SELETED_INDEX_TAB',
  SET_SELECTED_PATIENT: 'khambenh/SET_SELECTED_PATIENT'
};

const initialState = {
  danhSachKhamBenh: [],
  selectedIndex: 0,
  selectedPatient: {
    stt: '',
    maYT: '',
    hoTen: '',
    ngaySinh: null,
    namSinhCheckBox: false,
    tuoi: null,
    thang: null,
    ngay: null,
    gioiTinh: null,
    quocGia: null,
    ngoaiKieu: false,
    cmnd: '',
    ngheNghiep: null,
    soDT: null,
    danToc: null,
    tinhThanh: null,
    PhuongXa: null,
    quanHuyen: null,
    soNha: '',
    diaChi: ''
  }
};

export type danhSachKhamBenhState = Readonly<typeof initialState>;

export default (state: danhSachKhamBenhState = initialState, action): danhSachKhamBenhState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.GET_DANH_SACH_KHAM_BENH):
      return {
        ...state
      };
    case FAILURE(ACTION_TYPES.GET_DANH_SACH_KHAM_BENH):
      return {
        ...state,
        danhSachKhamBenh: []
      };
    case SUCCESS(ACTION_TYPES.GET_DANH_SACH_KHAM_BENH):
      return {
        ...state,
        danhSachKhamBenh: action.payload.data
      };
    case ACTION_TYPES.SET_SELETED_INDEX_TAB:
      return {
        ...state,
        selectedIndex: action.selectedIndex
      };
    case ACTION_TYPES.SET_SELECTED_PATIENT:
      return {
        ...state,
        selectedPatient: action.selectedPatient
      };
    default:
      return state;
  }
};

export const getDanhSachKhamBenh = () => async dispatch => {
  const requestURL = `https://5d6886388134fd001430be2b.mockapi.io/api/v1/danhsachkhambenh`;
  const result = await dispatch({
    type: ACTION_TYPES.GET_DANH_SACH_KHAM_BENH,
    payload: axios.get(requestURL).catch(obj => {
      notify({ message: 'Lỗi tải danh sách khám bệnh', width: 400, shading: 1 }, 'error', 1000);
    })
  });
  return result;
};

export const setSelectedIndexTab = selectedIndex => async (dispatch, getState) => {
  await dispatch({
    type: ACTION_TYPES.SET_SELETED_INDEX_TAB,
    selectedIndex
  });
};
export const setSelectedPatient = (selectedPatient: object) => (dispatch, getState) => {
  dispatch({
    type: ACTION_TYPES.SET_SELECTED_PATIENT,
    selectedPatient
  });
  dispatch(setSelectedIndexTab(1));
};
