import React from 'react';
import Form, { GroupItem, Label, SimpleItem } from 'devextreme-react/form';
import { translate, Translate } from 'react-jhipster';

export interface IFooterTools {
  url: string
}

const footerToolsI18N = 'khamChuaBenh.danhSachBenhNhan.footerTools.';

function DSTrangThaiBenhNhan() {
  return <div>
    <Translate contentKey={footerToolsI18N + 'chuyen'}/> <span
    style={{ color: 'red' }}><Translate contentKey={footerToolsI18N + 'bant'}/></span>
    <span style={{ color: 'yellow' }}><Translate
      contentKey={footerToolsI18N + 'canhBao'}/> </span>
    <span style={{ color: 'blue' }}>  <Translate
      contentKey={footerToolsI18N + 'uuTienCanhBao'}/></span>
    <span style={{ color: 'violet' }}> <Translate
      contentKey={footerToolsI18N + 'daTuyenNoiTinh'}/> </span>
    <span style={{ color: 'grown' }}>  <Translate
      contentKey={footerToolsI18N + 'daTuyenNgoaiTinh'}/></span>
    <span style={{ color: 'green' }}>  <Translate contentKey={footerToolsI18N + 'henTaiKham'}/></span>
    <span style={{ color: 'grown' }}>  <Translate
      contentKey={footerToolsI18N + 'benhNhanTiepNhan1080'}/> </span>
  </div>;
}

export default function FooterTools() {
  return (
    <div>
      <Form>
        <GroupItem colCount={9}>
          <SimpleItem colSpan={2} dataField="denNgay" editorType="dxTextBox">
            <Label text={translate(footerToolsI18N + 'gioTiepNhan')}/>
          </SimpleItem>
          <SimpleItem colSpan={2} dataField="tuNgay" editorType="dxTextBox">
            <Label text={translate(footerToolsI18N + 'gioKham')}/>
          </SimpleItem>
        </GroupItem>
        <SimpleItem component={DSTrangThaiBenhNhan}>
        </SimpleItem>
      </Form>
    </div>
  );
}
