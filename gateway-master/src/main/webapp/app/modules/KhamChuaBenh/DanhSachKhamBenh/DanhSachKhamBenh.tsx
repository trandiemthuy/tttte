import React, { useEffect } from 'react';
import { translate } from 'react-jhipster';

import DataGrid, { Column, FilterRow } from 'devextreme-react/data-grid';
import { CheckBox } from 'devextreme-react/check-box';

import HeaderTools from './HeaderTools';
import FooterTools from './FooterTools';

import { IRootState } from 'app/shared/reducers';
import { getDanhSachKhamBenh, setSelectedIndexTab, setSelectedPatient } from './DanhSachKhamBenh.reducer';
import { connect } from 'react-redux';

import './DanhSachKhamBenh.scss';

export interface IDSKhamBenh extends StateProps, DispatchProps {
  url: string
}

const danhSachBenhNhanI18N = 'khamChuaBenh.danhSachBenhNhan.';

export const DSKhamBenh = (props: IDSKhamBenh) => {
  useEffect(() => {
    props.getDanhSachKhamBenh();
  }, []);
  const { url } = props;

  const dataSource = props.danhSachKhamBenh;
  const pageSizes = [10, 25, 50, 100];
  const formatter = function(date) {
    const month = date.getMonth() + 1,
      day = date.getDate(),
      year = date.getFullYear();

    return day + '/' + month + '/' + year;
  };
  const formatDate = new Intl.DateTimeFormat('vi-VN').format;
  const rowTemp = function(rowInfo) {
    return <tbody className={`employee dx-row ${rowInfo.data.uuTien === true ? 'bg bg-uutien-canhbao ' : 'bg '}`}>
    <tr className={'border-1'}>
      <td>{rowInfo.data.stt}</td>
      <td>{formatDate(new Date(rowInfo.data.ngayDangKy))}</td>
      <td>{rowInfo.data.maBn}</td>
      <td>{rowInfo.data.hoTen}</td>
      <td>{formatDate(new Date(rowInfo.data.namSinh))}</td>
      <td>{rowInfo.data.doiTuong}</td>
      <td>{rowInfo.data.tenDichVu}</td>
      <td>{rowInfo.data.soPhieu}</td>
      <td>{<CheckBox defaultValue={rowInfo.data.uuTien === true} disabled={true}/>}</td>
      <td>{rowInfo.data.trangThai}</td>
      <td>{rowInfo.data.thanhToan}</td>
    </tr>
    </tbody>;
  };
  const loadPanel = {
    enabled: true,
    loadPanelVisible: false,
    showIndicator: true,
    shading: true,
    shadingColor: 'rgba(0,0,0,0.3)',
    showPane: true,
    text: translate(danhSachBenhNhanI18N + 'loadingData'),
    closeOnOutsideClick: true
  };
  const handleDblClick = (e) => {
    const selectedPatient = {
      stt: e.data.stt,
      maYT: e.data.maBn,
      hoTen: e.data.hoTen,
      ngaySinh: e.data.namSinh,
      namSinhCheckBox: false,
      tuoi: null,
      thang: null,
      ngay: null,
      gioiTinh: null,
      quocGia: null,
      ngoaiKieu: false,
      cmnd: '',
      ngheNghiep: null,
      soDT: null,
      danToc: null,
      tinhThanh: null,
      PhuongXa: null,
      quanHuyen: null,
      soNha: '',
      diaChi: ''
    };
    props.setSelectedPatient(selectedPatient);
    // props.setSelectedIndexTab(1);
  };
  return (
    <div className="p-3">
      <div className="mb-3">
        <HeaderTools url=""/>
      </div>
      <div className="d-flex flex-row-reverse">
        <DataGrid
          id="gridContainer"
          dataSource={dataSource}
          allowColumnReordering={true}
          // columnAutoWidth={true}
          allowColumnResizing={true}
          // showBorders={true}
          // showColumnLines={true}
          // showRowLines={true}
          // eslint-disable-next-line no-console
          onRowDblClick={handleDblClick}
          rowAlternationEnabled={true}
          rowRender={rowTemp}
          loadPanel={loadPanel}
          selection={{ mode: 'single' }}>
          <FilterRow
            visible={true}
          />
          <Column
            dataField="stt"
            caption={translate(danhSachBenhNhanI18N + 'thongTinBenhNhan.stt')}
            dataType="number"
            // format="currency"
            alignment="center"
          />
          <Column
            dataField="ngayDangKy"
            caption={translate(danhSachBenhNhanI18N + 'thongTinBenhNhan.ngayDangKy')}
            dataType="date"
            format={formatter}
            alignment="center"
          />
          <Column
            dataField="maBn"
            caption={translate(danhSachBenhNhanI18N + 'thongTinBenhNhan.maBn')}
            dataType="string"
            // format="currency"
            alignment="center"
          />
          <Column
            dataField="hoTen"
            caption={translate(danhSachBenhNhanI18N + 'thongTinBenhNhan.hoTen')}
            dataType="string"
            alignment="center"
          />
          <Column
            dataField="namSinh"
            caption={translate(danhSachBenhNhanI18N + 'thongTinBenhNhan.namSinh')}
            dataType="date"
            format={formatter}
            alignment="center"
          />
          <Column
            dataField="doiTuong"
            caption={translate(danhSachBenhNhanI18N + 'thongTinBenhNhan.doiTuong')}
            dataType="string"
            alignment="center"
          />
          <Column
            dataField="tenDichVu"
            caption={translate(danhSachBenhNhanI18N + 'thongTinBenhNhan.tenDichVu')}
            dataType="string"
            alignment="center"
          />
          <Column
            dataField="soPhieu"
            caption={translate(danhSachBenhNhanI18N + 'thongTinBenhNhan.soPhieu')}
            dataType="number"
            alignment="center"
          />
          <Column
            dataField="uuTien"
            caption={translate(danhSachBenhNhanI18N + 'thongTinBenhNhan.uuTien')}
            dataType="boolean"
            alignment="center"
          />
          <Column
            dataField="trangThai"
            caption={translate(danhSachBenhNhanI18N + 'thongTinBenhNhan.trangThai')}
            dataType="string"
            alignment="center"
          />
          <Column
            dataField="thanhToan"
            caption={translate(danhSachBenhNhanI18N + 'thongTinBenhNhan.thanhToan')}
            dataType="string"
            alignment="center"
          />
        </DataGrid>
      </div>
      <div className="mt-3">
        <FooterTools/>
      </div>
    </div>
  );

};

const mapStateToProps = (storeState: IRootState) => ({
  danhSachKhamBenh: storeState.danhsachkhambenh.danhSachKhamBenh
});

const mapDispatchToProps = {
  getDanhSachKhamBenh,
  setSelectedIndexTab,
  setSelectedPatient
};


type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps,
  mapDispatchToProps)(DSKhamBenh);

