import React from 'react';
import Form, { GroupItem, Label, SimpleItem } from 'devextreme-react/form';
import 'devextreme-react/autocomplete';
import { translate } from 'react-jhipster';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IThongTinChanDoan {
  thongTinChanDoanValues: object
}

const thongTinChanDoanI18N = 'khamChuaBenh.thucHienKhamBenh.thongTinChanDoan.';
export const ThongTinChanDoan = (props: IThongTinChanDoan) => {
  const buttonOptions = {
    text: 'Xử lý',
    type: 'default',
    useSubmitBehavior: true
  };
  return <React.Fragment>
    <Form
      formData={props.thongTinChanDoanValues}
      readOnly={false}
      showColonAfterLabel={true}
      showValidationSummary={true}
      validationGroup="customerData"
      // eslint-disable-next-line no-console
      onEditorEnterKey={(e) => {
        // console.log(e);
      }}
      id="thong-ton-chan-doan"
    >
      <GroupItem caption={translate(thongTinChanDoanI18N + 'form.caption')} cssClass="h5 pt-4 mb-0 pb-0">
        <SimpleItem dataField="kqNhanDinh" editorType="dxTextBox">
          <Label text={translate(thongTinChanDoanI18N + 'form.kqNhanDinh')}/>
          {/* <RequiredRule message="Email is required" />
                    <EmailRule message="Email is invalid" />
                    <AsyncRule
                        message="Email is already registered"
                    //   validationCallback={asyncValidation}
                    /> */}
        </SimpleItem>
        <SimpleItem dataField="trieuChungLS" editorType="dxTextBox">
          <Label text={translate(thongTinChanDoanI18N + 'form.trieuChungLS')}/>
          {/* <RequiredRule message="Email is required" />
                    <EmailRule message="Email is invalid" />
                    <AsyncRule
                        message="Email is already registered"
                    //   validationCallback={asyncValidation}
                    /> */}
        </SimpleItem>
        <SimpleItem dataField="chanDoanICD" editorType="dxTextBox">
          <Label text={translate(thongTinChanDoanI18N + 'form.chanDoanICD')}/>
          {/* <RequiredRule message="Email is required" />
                    <EmailRule message="Email is invalid" />
                    <AsyncRule
                        message="Email is already registered"
                    //   validationCallback={asyncValidation}
                    /> */}
        </SimpleItem>
        <SimpleItem dataField="benhPhu" editorType="dxTextBox" editorOptions={{ 'height': 60 }}>
          <Label text={translate(thongTinChanDoanI18N + 'form.benhPhu')}/>
          {/* <RequiredRule message="Email is required" />
                    <EmailRule message="Email is invalid" />
                    <AsyncRule
                        message="Email is already registered"
                    //   validationCallback={asyncValidation}
                    /> */}
        </SimpleItem>
        <SimpleItem dataField="tntt" editorType="dxTextBox">
          <Label text={translate(thongTinChanDoanI18N + 'form.tntt')}/>
          {/* <RequiredRule message="Email is required" />
                    <EmailRule message="Email is invalid" />
                    <AsyncRule
                        message="Email is already registered"
                    //   validationCallback={asyncValidation}
                    /> */}
        </SimpleItem>
        <SimpleItem dataField="loiDanKham" editorType="dxTextBox">
          <Label text={translate(thongTinChanDoanI18N + 'form.loiDanKham')}/>
          {/* <RequiredRule message="Email is required" />
                    <EmailRule message="Email is invalid" />
                    <AsyncRule
                        message="Email is already registered"
                    //   validationCallback={asyncValidation}
                    /> */}
        </SimpleItem>
      </GroupItem>
      <GroupItem colCount={3}>
        <SimpleItem dataField="giaiQuyet" editorType="dxTextBox" colSpan={2}>
          <Label text={translate(thongTinChanDoanI18N + 'form.giaiQuyet')}/>
          {/* <RequiredRule message="Email is required" />
                    <EmailRule message="Email is invalid" />
                    <AsyncRule
                        message="Email is already registered"
                    //   validationCallback={asyncValidation}
                    /> */}
        </SimpleItem>
        {/* <ButtonItem horizontalAlignment="left"
                    buttonOptions={buttonOptions}
                /> */}
      </GroupItem>
    </Form>
  </React.Fragment>;
};
export default ThongTinChanDoan;
