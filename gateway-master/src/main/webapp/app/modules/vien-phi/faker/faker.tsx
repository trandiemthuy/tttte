import React from 'react';
import { TextArea, Button } from 'devextreme-react';
import { LoadIndicator } from 'devextreme-react/load-indicator';
import notify from 'devextreme/ui/notify';
import VienPhiTitle from 'app/modules/vien-phi/commons/title';
import { VienPhiServicesUrl } from 'app/modules/vien-phi/utils/vienphi-constant';
import axios from 'axios';

export interface IVienPhiImportFakerState {
  data: string;
  loading: boolean;
}

class VienPhiImportFaker extends React.Component<any, IVienPhiImportFakerState> {

  constructor(props, state) {
    super(props, state);
    this.state = {
      data: '',
      loading: false
    }
  }

  onTextAreaValueChanged = e => {
    this.setState({
      data: e.value
    });
  }

  onClick = e => {
    this.setState({
      loading: true
    });
    const url = `${VienPhiServicesUrl}/api/du-lieu-benh-nhan/`;
    const data = JSON.parse(this.state.data);
    axios.post(url, data)
      .then(res => {
        if (res.status === 201) {
          notify('Import thành công !', 'success');
        }
        else {
          notify('Bộ khóa (soVaoVien, soVaoVienDt, loaiKcb, dvtt) đã tồn tại !', 'error');
        }
        this.setState({
          loading: false
        })
      })
      .catch(error => {
        notify('Có lỗi xảy ra', 'error');
        this.setState({
          loading: false
        })
      });
  }

  render() {
    return (
      <div>
        <VienPhiTitle title={'Import Faker KCB Data'} />
        <div className="d-flex justify-content-center p-3">
          <TextArea
            height={300}
            width={500}
            value={this.state.data}
            onValueChanged={this.onTextAreaValueChanged}
          />
        </div>
        <div className="d-flex justify-content-center">
          <Button width={120} type="normal" stylingMode="outlined" onClick={this.onClick}>
            <span className="dx-button-text m-2">Import</span>
            <LoadIndicator className="button-indicator" visible={this.state.loading} />
          </Button>
        </div>
      </div>
    );
  }

}

export default VienPhiImportFaker;
