import React from 'react';
import { Button } from 'devextreme-react';
import { translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { inBangKe } from 'app/modules/vien-phi/kiem-tra-bang-ke/kiem-tra-bang-ke.reducer';
import { DropDownButton } from 'devextreme-react';
import { getListKieuFile } from 'app/modules/vien-phi/models/kieu-file.model';

export interface IVienPhiKiemTraBangKeActionProps extends StateProps, DispatchProps {
}

class VienPhiKiemTraBangKeAction extends React.Component<IVienPhiKiemTraBangKeActionProps> {

  btnPrintBangKeClick = e => {
    this.props.inBangKe(e.itemData.type);
  }

  render() {
    const { disabled } = this.props;
    return (
      <div className="p-2">
        <DropDownButton
          text={translate('vienphi.kiemtrabangke.kiemtrabangke.inbangke')}
          stylingMode="contained"
          items={[...getListKieuFile(['PDF', 'RTF'])]}
          displayExpr="name"
          keyExpr="type"
          disabled={disabled}
          onItemClick={this.btnPrintBangKeClick}
        />
      </div>
    );
  }

}

const mapStateToProps = ({ kiemTraBangKe }: IRootState) => ({
  disabled: !kiemTraBangKe.benhNhanSelected.maBn
});

const mapDispatchToProps = {
  inBangKe
}

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VienPhiKiemTraBangKeAction);
