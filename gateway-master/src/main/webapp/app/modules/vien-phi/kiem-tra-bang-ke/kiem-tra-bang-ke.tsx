import React, { Component } from 'react';
import { Translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { translate } from 'react-jhipster';
import VienPhiTitle from 'app/modules/vien-phi/commons/title';
import Box, { Item } from 'devextreme-react/box';
import { LoadPanel } from 'devextreme-react/load-panel';
import { NumberBox } from 'devextreme-react/number-box';
import VienPhiFormRequestDsBenhNhan from 'app/modules/vien-phi/commons/form-request-ds-benh-nhan';
import VienPhiGridDanhSachBenhNhan from 'app/modules/vien-phi/commons/grid-danh-sach-benh-nhan';
import {
  getDsBenhNhanHoanTatKham,
  selectBenhNhan
} from 'app/modules/vien-phi/kiem-tra-bang-ke/kiem-tra-bang-ke.reducer';
import { VienPhiBenhNhan } from 'app/modules/vien-phi/commons/benh-nhan';
import VienPhiKiemTraBangKeAction from 'app/modules/vien-phi/kiem-tra-bang-ke/kiem-tra-bang-ke-action';
import VienPhiGridBangKe from 'app/modules/vien-phi/kiem-tra-bang-ke/grid-bang-ke';
import VienPhiThongTinTongHopBangKe from 'app/modules/vien-phi/kiem-tra-bang-ke/thong-tin-tong-hop-bang-ke';

export interface IVienPhiKiemTraBangKeProps extends StateProps, DispatchProps {
}

class VienPhiKiemTraBangKe extends Component<IVienPhiKiemTraBangKeProps> {

  onRowGridDsbnDbClick = e => {
    this.props.selectBenhNhan(e.data);
  }

  render() {
    const { dsBenhNhan, benhNhanSelected, creatingBangKe, dsBangKe, mucHuongBangKe  } = this.props;
    return (
      <React.Fragment>
        <LoadPanel
          shadingColor="rgba(0,0,0,0.4)"
          position={{ of: '#div-kiem-tra-bang-ke' }}
          visible={creatingBangKe}
          showIndicator
          shading={false}
          showPane
          closeOnOutsideClick={false}
        />
        <div id="div-kiem-tra-bang-ke">
          <VienPhiTitle title={translate('vienphi.kiemtrabangke.kiemtrabangke.tieude')} />
          <Box >
            <Item ratio={3}>
                <VienPhiFormRequestDsBenhNhan layout="vertical"
                                              labelLocation="left"
                                              listField={['tuNgay', 'denNgay', 'loaiKcb', 'maKhoa', 'coBhyt', 'daInBangKe']}
                                              action={this.props.getDsBenhNhanHoanTatKham}
                />
                <div className="p-1" style={{height: 415}}>
                  <VienPhiGridDanhSachBenhNhan dataSource={dsBenhNhan}
                                               listColumnsShow={['maBn', 'hoTen', 'trangThaiBn']}
                                               onRowDblClick={this.onRowGridDsbnDbClick}
                                               noPager
                  />
                </div>
            </Item>
            <Item ratio={9}>
                <div className="p-1">
                  <VienPhiBenhNhan data={benhNhanSelected} />
                  <VienPhiThongTinTongHopBangKe />
                  <VienPhiKiemTraBangKeAction />
                  <div className="d-flex justify-content-end font-weight-bold">
                    <div className="p-1 mr-2">
                      <Translate contentKey="vienphi.kiemtrabangke.kiemtrabangke.mucHuongBangKe">Mức hưởng bảng kê</Translate>
                    </div>
                    <NumberBox value={mucHuongBangKe} readOnly={true} width={60} />
                  </div>
                  <VienPhiGridBangKe id="grid-bang-ke"
                                     dataSource={dsBangKe}
                                     height={400}
                  />
                </div>
            </Item>
          </Box>
        </div>
      </React.Fragment>
    );
  }

}

const mapStateToProps = ({ kiemTraBangKe }: IRootState) => ({
  dsBenhNhan: kiemTraBangKe.dsBenhNhan,
  benhNhanSelected: kiemTraBangKe.benhNhanSelected,
  creatingBangKe: kiemTraBangKe.creatingBangKe,
  dsBangKe: kiemTraBangKe.dsBangKe,
  mucHuongBangKe: kiemTraBangKe.mucHuongBangKe
});

const mapDispatchToProps = {
  getDsBenhNhanHoanTatKham,
  selectBenhNhan
}

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VienPhiKiemTraBangKe);
