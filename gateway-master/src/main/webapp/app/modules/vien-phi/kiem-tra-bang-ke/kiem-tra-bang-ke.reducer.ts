import { VienPhiServicesUrl } from 'app/modules/vien-phi/utils/vienphi-constant';
import axios from 'axios';
import { getDvtt, initCustomStore, printReportGet } from 'app/modules/vien-phi/utils/vienphi-function';
import CustomStore from 'devextreme/data/custom_store';
import { defaultVienPhiBenhNhanData, IVienPhiBenhNhanModel } from 'app/modules/vien-phi/models/benh-nhan.model';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';
import notify from 'devextreme/ui/notify';
import { IVienPhiBangKeModel } from 'app/modules/vien-phi/models/bang-ke.model';

export const ACTION_TYPE = {
  GET_DS_BENHNHAN: 'VienPhi/KiemTraBangKe/GetDsBenhNhan',
  SELECT_BENH_NHAN: 'VienPhi/KiemTraBangKeSelectBenhNhan',
  CREATE_BANG_KE: 'VienPhi/KiemTraBangKe/CreateBangKe',
  GET_BANG_KE: 'VienPhi/KiemTraBangKe/GetBangKe',
  CALCULATE_TONG_HOP_BK: 'VienPhi/KiemTraBangKe/CaculateTongHopBangKe',
  IN_BANG_KE: 'VienPhi/KiemTraBangKe/InBangKe'
};

const initialState = {
  dsBenhNhan: initCustomStore(),
  benhNhanSelected: defaultVienPhiBenhNhanData,
  creatingBangKe: false,
  thongTinVpTongHopBangKe: {
    tongChiPhi: 0,
    bhxhChi: 0,
    khac: 0,
    bnTra: 0
  },
  dsBangKe: initCustomStore(),
  mucHuongBangKe: 0
};

export type KiemTraBangKeState = Readonly<typeof initialState>;

// Reducer
export default (state: KiemTraBangKeState = initialState, action): KiemTraBangKeState => {
  switch (action.type) {
    case ACTION_TYPE.GET_DS_BENHNHAN:
      return {
        ...state,
        dsBenhNhan: action.dataSource
      };
    case ACTION_TYPE.SELECT_BENH_NHAN:
      return {
        ...state,
        benhNhanSelected: action.data
      };
    case REQUEST(ACTION_TYPE.CREATE_BANG_KE):
      return {
        ...state,
        creatingBangKe: true
      };
    case FAILURE(ACTION_TYPE.CREATE_BANG_KE):
      notify('Có lỗi xảy ra', 'error');
      return {
        ...state,
        creatingBangKe: false
      };
    case SUCCESS(ACTION_TYPE.CREATE_BANG_KE):
      return {
        ...state,
        creatingBangKe: false
      };
    case ACTION_TYPE.GET_BANG_KE:
      return {
        ...state,
        dsBangKe: action.dataSource
      };
    case ACTION_TYPE.CALCULATE_TONG_HOP_BK:
      return {
        ...state,
        thongTinVpTongHopBangKe: action.data,
        mucHuongBangKe: action.mucHuong
      };
    default:
      return state;
  }
};

export const calculateTongHopBangKe = (mucHuong: number, data: any) => async (dispatch, getState) => {
  await dispatch({
    type: ACTION_TYPE.CALCULATE_TONG_HOP_BK,
    data,
    mucHuong
  });
};

export const getBangKe = () => async (dispatch, getState) => {
  const { benhNhanSelected } = getState().kiemTraBangKe;
  const { soVaoVien, soVaoVienDt, dvtt, loaiKcb } = benhNhanSelected;
  const dataSource = new CustomStore({
    key: 'soPhieuMaDv',
    loadMode: 'raw',
    async load() {
      const url = `${VienPhiServicesUrl}/api/bang-ke`;
      const params = { soVaoVien, soVaoVienDt, dvtt, loaiKcb };
      const response = await axios.get<Array<IVienPhiBangKeModel>>(url, { params });
      return response.data;
    },
    onLoaded(data) {
      dispatch(
        calculateTongHopBangKe(data && data.length > 0 ? data[0].mucHuong : 0, {
          tongChiPhi: data.map(e => e.thanhTienBv).reduce((a, b) => a + b, 0),
          bhxhChi: data.map(e => e.quyBhyt).reduce((a, b) => a + b, 0),
          khac: data.map(e => e.khac).reduce((a, b) => a + b, 0),
          bnTra: data.map(e => e.cungChiTra + e.nguoiBenh).reduce((a, b) => a + b, 0)
        })
      );
    }
  });
  await dispatch({
    type: ACTION_TYPE.GET_BANG_KE,
    dataSource
  });
};

export const createBangKe = () => async (dispatch, getState) => {
  const { benhNhanSelected } = getState().kiemTraBangKe;
  const { soVaoVien, soVaoVienDt, dvtt, loaiKcb } = benhNhanSelected;
  const url = `${VienPhiServicesUrl}/api/bang-ke/`;
  const data = { soVaoVien, soVaoVienDt, dvtt, loaiKcb };
  await dispatch({
    type: ACTION_TYPE.CREATE_BANG_KE,
    payload: axios.post(url, data)
  });
};

export const selectBenhNhan = (data: IVienPhiBenhNhanModel) => async (dispatch, getState) => {
  await dispatch({
    type: ACTION_TYPE.SELECT_BENH_NHAN,
    data
  });
  await dispatch(createBangKe());
  await dispatch(getBangKe());
};

export const getDsBenhNhanHoanTatKham = params => async (dispatch, getState) => {
  const url = `${VienPhiServicesUrl}/api/danh-sach-benh-nhan/hoan-tat-kham`;
  const dataSource = new CustomStore({
    loadMode: 'raw',
    key: ['soVaoVien', 'soVaoVienDt', 'loaiKcb', 'dvtt'],
    async load() {
      const response = await axios.get<Array<IVienPhiBenhNhanModel>>(url, { params });
      return response.data;
    }
  });
  await dispatch({
    type: ACTION_TYPE.GET_DS_BENHNHAN,
    dataSource
  });
};

export const inBangKe = (kieuFile: number) => async (dispatch, getState) => {
  const url = `${VienPhiServicesUrl}/api/bang-ke/print`;
  const { benhNhanSelected } = getState().kiemTraBangKe;
  const params = {
    soVaoVien: benhNhanSelected.soVaoVien,
    soVaoVienDt: benhNhanSelected.soVaoVienDt,
    loaiKcb: benhNhanSelected.loaiKcb,
    dvtt: getDvtt(),
    kieuFile
  };
  await dispatch({
    type: ACTION_TYPE.IN_BANG_KE,
    payload: printReportGet(url, params)
  });
};
