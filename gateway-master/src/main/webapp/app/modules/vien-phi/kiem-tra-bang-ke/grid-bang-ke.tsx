import React from 'react';
import DataGrid, { Column, Grouping, GroupItem, Summary, TotalItem } from 'devextreme-react/data-grid';
import { groupCellRender } from 'app/modules/vien-phi/commons/data-grid-components';
import { nvl } from "app/modules/vien-phi/utils/vienphi-function";

export interface IVienPhiGridBangKeProps {
  id?: string;
  dataSource: any;
  width?: any;
  height?: any;}

class VienPhiGridBangKe extends React.Component<IVienPhiGridBangKeProps> {

  tenNhomSortingMethod = (value1, value2): number => {
    const sttNhom1 = Number(value1.split('.')[0]);
    const sttNhom2 = Number(value2.split('.')[0]);
    return sttNhom1 - sttNhom2;
  }

  cellNoiDungRender = cellData => {
   cellData.row.data.vatTuStent
    return cellData.row.data.vatTuStent ? (
      <span className="font-weight-bold">{cellData.text}</span>
    ) : (
      <span>{cellData.text}</span>
    );
  }

  render() {
    const { dataSource, id, width, height } = this.props;
    return (
      <DataGrid
        id={id}
        dataSource={dataSource}
        allowColumnReordering={true}
        // columnAutoWidth
        allowColumnResizing
        columnResizingMode={'widget'}
        showBorders
        showRowLines
        hoverStateEnabled
        className="m-1"
        width={nvl(width, '100%')}
        height={nvl(height, '100%')}
        wordWrapEnabled
        paging={{ enabled: false }}
      >
        <Grouping autoExpandAll />
        <Column groupCellRender={groupCellRender} sortingMethod={this.tenNhomSortingMethod} dataField="tenNhom" dataType="string" groupIndex={0} />
        <Column dataField="soPhieuMaDv" visible={false} />
        <Column caption="Nội dung" dataField="noiDung" width="15%" dataType="string" cellRender={this.cellNoiDungRender} />
        <Column caption="ĐVT" dataField="dvt" width="45px" dataType="string" />
        <Column caption="Số lượng" dataField="soLuong" dataType="number" />
        <Column caption="Đơn giá BV" dataField="donGiaBv"  dataType="number" format="#,##0.00" />
        <Column caption="Đơn giá BH" dataField="donGiaBh"  dataType="number" format="#,##0.00" />
        <Column caption="Thành tiền BV" dataField="thanhTienBv" width={90}  dataType="number" format="#,##0.00" />
        <Column caption="Thành tiền BH" dataField="thanhTienBh" width={90}  dataType="number" format="#,##0.00" />
        <Column caption="Nguồn thanh toán" alignment="center">
          <Column caption="Quỹ BHYT" dataField="quyBhyt"  dataType="number" format="#,##0.00" />
          <Column caption="Cùng CT" dataField="cungChiTra"  dataType="number" format="#,##0.00" />
          <Column caption="Khác" dataField="khac"  dataType="number" format="#,##0.00" />
          <Column caption="BN tự trả" dataField="nguoiBenh"  dataType="number" format="#,##0.00" />
        </Column>
        <Summary>
          <GroupItem column="thanhTienBv" summaryType="sum" displayFormat="{0}" valueFormat="#,##0.00" showInGroupFooter />
          <GroupItem column="thanhTienBh" summaryType="sum" displayFormat="{0}" valueFormat="#,##0.00" showInGroupFooter />
          <GroupItem column="quyBhyt" summaryType="sum" displayFormat="{0}" valueFormat="#,##0.00" showInGroupFooter />
          <GroupItem column="cungChiTra" summaryType="sum" displayFormat="{0}" valueFormat="#,##0.00" showInGroupFooter />
          <GroupItem column="khac" summaryType="sum" displayFormat="{0}" valueFormat="#,##0.00" showInGroupFooter />
          <GroupItem column="nguoiBenh" summaryType="sum" displayFormat="{0}" valueFormat="#,##0.00" showInGroupFooter />
          <TotalItem name="tongThanhTienBv" displayFormat="{0}" column="thanhTienBv" summaryType="sum" valueFormat="#,##0.00" />
          <TotalItem name="tongThanhTienBh" displayFormat="{0}" column="thanhTienBh" summaryType="sum" valueFormat="#,##0.00" />
          <TotalItem name="tongQuyBhyt"  displayFormat="{0}" column="quyBhyt" summaryType="sum" valueFormat="#,##0.00" />
          <TotalItem name="tongCungChiTra"  displayFormat="{0}" column="cungChiTra" summaryType="sum" valueFormat="#,##0.00" />
          <TotalItem name="tongKhac"  displayFormat="{0}" column="khac" summaryType="sum" valueFormat="#,##0.00" />
          <TotalItem name="tongNguoiBenh"  displayFormat="{0}" column="nguoiBenh" summaryType="sum" valueFormat="#,##0.00" />
        </Summary>
      </DataGrid>
    );
  }

}

export default VienPhiGridBangKe;
