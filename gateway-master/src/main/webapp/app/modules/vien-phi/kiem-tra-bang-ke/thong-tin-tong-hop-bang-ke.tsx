import React from 'react';
import { IRootState } from "app/shared/reducers";
import { connect } from "react-redux";
import Form, { Label, SimpleItem } from 'devextreme-react/form';
import { VienPhiDefaultNumberFormat } from 'app/modules/vien-phi/utils/vienphi-constant';
import { translate } from 'react-jhipster';

export interface IVienPhiThongTinTongHopBangKeProps extends StateProps, DispatchProps{
}

class VienPhiThongTinTongHopBangKe extends React.Component<IVienPhiThongTinTongHopBangKeProps> {

  render() {
    const { data } = this.props;
    return (
        <Form
          id="form-thong-tin-tong-hop-bang-ke"
          formData={{ ...data }}
          readOnly
          showColonAfterLabel
          colCount={4}
        >
          <SimpleItem cssClass="pt-0" dataField="tongChiPhi" editorType="dxNumberBox"
                      editorOptions={{
                        format: VienPhiDefaultNumberFormat,
                        inputAttr: { style: 'text-align: right; color: red; font-weight: bold;' },
                      }}>
            <Label text={translate('vienphi.kiemtrabangke.kiemtrabangke.tongchiphi')}/>
          </SimpleItem>
          <SimpleItem cssClass="pt-0" dataField="bhxhChi" editorType="dxNumberBox" editorOptions={{
            format: VienPhiDefaultNumberFormat,
            inputAttr: { style: 'text-align: right; color: blue; font-weight: bold;' }
          }}>
            <Label text={translate('vienphi.kiemtrabangke.kiemtrabangke.bhxhchi')}/>
          </SimpleItem>
          <SimpleItem cssClass="pt-0" dataField="khac" editorType="dxNumberBox" editorOptions={{
            format: VienPhiDefaultNumberFormat,
            inputAttr: { style: 'text-align: right; color: purple; font-weight: bold;' }
          }}>
            <Label text={translate('vienphi.kiemtrabangke.kiemtrabangke.khac')}/>
          </SimpleItem>
          <SimpleItem cssClass="pt-0" dataField="bnTra" editorType="dxNumberBox" editorOptions={{
            format: VienPhiDefaultNumberFormat,
            inputAttr: { style: 'text-align: right; color: green; font-weight: bold;' }
          }}>
            <Label text={translate('vienphi.kiemtrabangke.kiemtrabangke.bntra')}/>
          </SimpleItem>
        </Form>
    );
  }

}

const mapStateToProps = ({ kiemTraBangKe }: IRootState) => ({
  data: kiemTraBangKe.thongTinVpTongHopBangKe,
});

const mapDispatchToProps = {
}

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VienPhiThongTinTongHopBangKe);
