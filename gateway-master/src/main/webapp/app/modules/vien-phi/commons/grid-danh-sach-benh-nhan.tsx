import React, { Component } from 'react';
import DataGrid, { Column, Summary, TotalItem, Scrolling } from 'devextreme-react/data-grid';
import {
  cellCheckBoxRender,
  cellDateTimeRender,
  cellGioiTinhRender
} from 'app/modules/vien-phi/commons/data-grid-components';

export interface IVienPhiGridDanhSachBenhNhanProps {
  listColumnsShow?: Array<string>;
  dataSource:  any;
  onRowDblClick?: (any) => any;
  focusedRowEnabled?: boolean;
  noPager?: boolean;
}

class VienPhiGridDanhSachBenhNhan extends Component<IVienPhiGridDanhSachBenhNhanProps> {

  isShowColumn = (columnName: string): boolean => {
    return this.props.listColumnsShow === undefined || this.props.listColumnsShow === null
      || this.props.listColumnsShow.findIndex(e => e === columnName) > -1;
  }

  render() {
    return (
      <DataGrid
        id="grid-danh-sach-benh-nhan"
        dataSource={this.props.dataSource}
        allowColumnResizing
        columnResizingMode={'widget'}
        // columnAutoWidth
        showBorders
        showRowLines
        // rowAlternationEnabled
        hoverStateEnabled
        height='100%'
        filterRow={{  visible: true, applyFilter: 'auto' }}
        headerFilter={{ visible: true }}
        paging={{ enabled: !this.props.noPager }}
        pager={this.props.noPager ? undefined : { visible: true, showPageSizeSelector: true, allowedPageSizes: [10, 20, 50], showInfo: true, showNavigationButtons: true }}
        onRowDblClick={this.props.onRowDblClick}
        focusedRowEnabled={this.props.focusedRowEnabled}
      >
        <Scrolling mode="infinite" />
        <Column caption="Mã BN" width={75} dataField="maBn" visible={this.isShowColumn('maBn')} />
        <Column caption="Họ tên" width={150} dataField="hoTen" visible={this.isShowColumn('hoTen')} />
        <Column caption="Ngày sinh" dataField="ngaySinh" alignment="center" dataType="date" format="dd/MM/yyyy" visible={this.isShowColumn('ngaySinh')} />
        <Column caption="Giới tính" dataField="gioiTinh" alignment="center" visible={this.isShowColumn('gioiTinh')} cellRender={cellGioiTinhRender} />
        <Column caption="BHYT" dataField="coBhyt" width={80} alignment="center" visible={this.isShowColumn('coBhyt')} cellRender={cellCheckBoxRender} />
        <Column caption="Đúng tuyến" dataField="dungTuyen" width={110} alignment="center" visible={this.isShowColumn('dungTuyen')} cellRender={cellCheckBoxRender} />
        <Column caption="Ngày vào" dataField="ngayVao" dataType="datetime" format="dd/MM/yyyy HH:mm:ss" alignment="center" visible={this.isShowColumn('ngayVao')} cellRender={cellDateTimeRender} />
        <Column caption="Ngày ra" dataField="ngayRa" dataType="datetime" format="dd/MM/yyyy HH:mm:ss" alignment="center" visible={this.isShowColumn('ngayRa')} cellRender={cellDateTimeRender} />
        <Column caption="Trạng thái" dataField="trangThaiBn" visible={this.isShowColumn('trangThaiBn')} />
        <Column caption="Địa chỉ" dataField="diaChi" visible={this.isShowColumn('diaChi')} />
        <Summary>
          <TotalItem
            column="hoTen"
            summaryType="count"
            displayFormat="Tổng: {0} bệnh nhân"
          />
        </Summary>
      </DataGrid>
    );
  }
}

export default VienPhiGridDanhSachBenhNhan;
