import React from 'react';
import ArrayStore from 'devextreme/data/array_store';
import { VienPhiServicesUrl } from 'app/modules/vien-phi/utils/vienphi-constant';
import { convertDateToStringParam, getDvtt } from 'app/modules/vien-phi/utils/vienphi-function';
import axios from 'axios';
import notify from 'devextreme/ui/notify';
import { listLoaiKcb } from 'app/modules/vien-phi/models/loaikcb.model';
import moment from 'moment';
import Form, { ButtonItem, GroupItem, Label, SimpleItem } from 'devextreme-react/form';
import { translate } from 'react-jhipster';
import VienPhiDateBox from 'app/modules/vien-phi/commons/datebox';

export interface IVienPhiFormRequestDsBenhNhanProps {
  id?: string;
  layout: 'vertical' | 'horizontal';
  labelLocation: 'left' | 'right' | 'top';
  height?: string;
  width?: string;
  listField: Array<string>;
  action: Function;
  hideButtonDate?: boolean;
}

export interface IVienPhiFormRequestDsBenhNhanState {
  dsKhoa: ArrayStore;
  formData: any;
}

class VienPhiFormRequestDsBenhNhan extends React.Component<IVienPhiFormRequestDsBenhNhanProps, IVienPhiFormRequestDsBenhNhanState> {

  private readonly dsLoaiKcb = new ArrayStore({
    data: listLoaiKcb,
    key: 'value'
  });
  private readonly dsBhyt = new ArrayStore({
    data: [{name: 'Tất cả', value:-1}, {name: 'Không BHYT', value: 0}, {name: 'Có BHYT', value: 1}],
    key: 'value'
  })
  private readonly dsDaThanhToan = new ArrayStore({
    data: [{name: 'Tất cả', value:-1}, {name: 'Chưa thanh toán', value: 0}, {name: 'Đã thanh toán', value: 1}],
    key: 'value'
  })
  private readonly dsDaTamUng = new ArrayStore({
    data: [{name: 'Tất cả', value:-1}, {name: 'Chưa tạm ứng', value: 0}, {name: 'Đã tạm ứng', value: 1}],
    key: 'value'
  });
  private readonly dsDaInBangKe = new ArrayStore({
    data: [{name: 'Tất cả', value:-1}, {name: 'Chưa in', value: 0}, {name: 'Đã in', value: 1}],
    key: 'value'
  });

  constructor(props: IVienPhiFormRequestDsBenhNhanProps, state: IVienPhiFormRequestDsBenhNhanState) {
    super(props, state);
    this.state = {
      dsKhoa: new ArrayStore({ data: [], key: 'maKhoa' }),
      formData: {
        tuNgay: moment().toDate(),
        denNgay: moment().toDate(),
        loaiKcb: listLoaiKcb[0].value,
        maKhoa: '',
        coBhyt: -1,
        daThanhToan: -1,
        daTamUng: -1,
        daInBangKe: -1
      }
    }
  }

  dsKhoa = async () => {
    try {
      const url = `${VienPhiServicesUrl}/api/danh-sach-khoa?dvtt=${getDvtt()}`;
      const response = await axios.get<Array<any>>(url);
      return response.data ? response.data : [];
    }
    catch (e) {
      notify('Không thể load danh sách khoa !', 'error');
      return [];
    }
  }

  async componentDidMount() {
    const data = await this.dsKhoa();
    this.setState({
      dsKhoa: new ArrayStore({ data, key: 'maKhoa' }),
      formData: {
        ...this.state.formData,
        maKhoa: data.length > 0 ? data[0]['maKhoa'] : ''
      }
    })
  }

  isShowField = (field: string): boolean => {
    return !this.props.listField ? true :
      this.props.listField.findIndex(e => e === field) > -1;
  }

  btnRefreshClick = e => {
    const { formData } = this.state;
    const temp = {
      tuNgay: convertDateToStringParam(formData.tuNgay),
      denNgay: convertDateToStringParam(formData.denNgay),
      loaiKcb: formData.loaiKcb,
      maKhoa: formData.maKhoa,
      coBhyt: formData.coBhyt,
      daThanhToan: formData.daThanhToan,
      daTamUng: formData.daTamUng,
      daInBangKe: formData.daInBangKe
    };
    let params = { dvtt: getDvtt() };
    this.props.listField.forEach(field => {
      params = { ...params, [field]: temp[field] };
    });
    this.props.action(params);
  }

  render() {
    const { formData, dsKhoa } = this.state;
    const { id, labelLocation, layout, listField, hideButtonDate } = this.props;
    const colCount = layout === 'vertical' ? 1 : listField.length + 1;
    return (
        <Form id={id}
              showColonAfterLabel
              formData={formData}
              className="p-1"
              labelLocation={labelLocation}
              colCount={colCount}
        >
          <SimpleItem cssClass="p-0" visible={this.isShowField('tuNgay')} isRequired render={VienPhiDateBox(formData.tuNgay, hideButtonDate)}>
            <Label text={translate('vienphi.dsbenhnhan.tungay')} />
          </SimpleItem>
          <SimpleItem cssClass="p-0" visible={this.isShowField('denNgay')} isRequired render={VienPhiDateBox(formData.denNgay, hideButtonDate)}>
            <Label text={translate('vienphi.dsbenhnhan.denngay')} />
          </SimpleItem>
          <SimpleItem cssClass="p-0" dataField="loaiKcb" visible={this.isShowField('loaiKcb')} editorType="dxSelectBox"
                      editorOptions={{
                        placeholder: 'Loại KCB',
                        dataSource: this.dsLoaiKcb,
                        displayExpr: 'name',
                        valueExpr: 'value'
                      }}
          >
            <Label text={translate('vienphi.dsbenhnhan.loaikcb')} />
          </SimpleItem>
          <SimpleItem cssClass="p-0" dataField="maKhoa" visible={this.isShowField('maKhoa')} editorType="dxSelectBox"
                      editorOptions={{
                        placeholder: 'Khoa',
                        dataSource: dsKhoa,
                        displayExpr: 'tenKhoa',
                        valueExpr: 'maKhoa',
                        searchEnabled: true
                      }}
          >
            <Label text={translate('vienphi.dsbenhnhan.khoa')} />
          </SimpleItem>
          <SimpleItem cssClass="p-0" dataField="coBhyt" visible={this.isShowField('coBhyt')} editorType="dxSelectBox"
                      editorOptions={{
                        dataSource: this.dsBhyt,
                        displayExpr: 'name',
                        valueExpr: 'value',
                      }}
          >
            <Label text={translate('vienphi.dsbenhnhan.bhyt')} />
          </SimpleItem>
          <SimpleItem cssClass="p-0" dataField="daThanhToan" visible={this.isShowField('daThanhToan')} editorType="dxSelectBox"
                      editorOptions={{
                        dataSource: this.dsDaThanhToan,
                        displayExpr: 'name',
                        valueExpr: 'value',
                      }}
          >
            <Label text={translate('vienphi.dsbenhnhan.thanhtoan')} />
          </SimpleItem>
          <SimpleItem cssClass="p-0" dataField="daTamUng" visible={this.isShowField('daTamUng')} editorType="dxSelectBox"
                      editorOptions={{
                        dataSource: this.dsDaTamUng,
                        displayExpr: 'name',
                        valueExpr: 'value',
                      }}
          >
            <Label text={translate('vienphi.dsbenhnhan.tamung')} />
          </SimpleItem>
          <SimpleItem cssClass="p-0" dataField="daInBangKe" visible={this.isShowField('daInBangKe')} editorType="dxSelectBox"
                      editorOptions={{
                        dataSource: this.dsDaInBangKe,
                        displayExpr: 'name',
                        valueExpr: 'value',
                      }}
          >
            <Label text={translate('vienphi.dsbenhnhan.inbangke')} />
          </SimpleItem>
          <ButtonItem cssClass="p-0" verticalAlignment="bottom" horizontalAlignment={layout === 'horizontal' ? 'left' : 'center'}
                      buttonOptions={{
                        text: translate('vienphi.dsbenhnhan.lammoi'),
                        type: 'default',
                        stylingMode: 'outlined',
                        icon: 'refresh',
                        onClick: this.btnRefreshClick
                      }}
          />
        </Form>
    );
  }

}

export default VienPhiFormRequestDsBenhNhan;
