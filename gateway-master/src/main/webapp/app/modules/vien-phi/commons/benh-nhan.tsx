import { IVienPhiBenhNhanModel } from "app/modules/vien-phi/models/benh-nhan.model";
import React from 'react';
import Form, { GroupItem, Label, SimpleItem } from 'devextreme-react/form';
import { translate } from 'react-jhipster';
import { getTenGioiTinh } from 'app/modules/vien-phi/utils/vienphi-function';

export interface IVienPhiBenhNhanProps {
  data: IVienPhiBenhNhanModel;
}

export class VienPhiBenhNhan extends React.Component<IVienPhiBenhNhanProps> {

  render() {
    const { data } = this.props;
    return (
      <Form
        id="form-benh-nhan"
        formData={data}
        showColonAfterLabel
        readOnly
        colCount={2}
        // className="font-weight-bold"
      >
        {/* <GroupItem caption={translate('vienphi.benhnhan.thongtinbenhnhan')}> */}
        <GroupItem>
          <GroupItem colCount={5}>
            <SimpleItem dataField="maBn" colSpan={2} editorType="dxTextBox" editorOptions={{ placeholder: 'Mã bệnh nhân' }}>
              <Label text={translate('vienphi.benhnhan.mabn')} />
            </SimpleItem>
            <SimpleItem dataField="hoTen" colSpan={3} editorType="dxTextBox" editorOptions={{ placeholder: 'Họ và tên', inputAttr: {style: 'font-weight: bold; color: red'} }}>
              <Label text={translate('vienphi.benhnhan.hoten')} />
            </SimpleItem>
          </GroupItem>
          <GroupItem colCount={2} cssClass="pt-0">
            <SimpleItem editorType="dxTextBox" editorOptions={{ placeholder: 'Giới tính', value: getTenGioiTinh(data.gioiTinh) }}>
              <Label text={translate('vienphi.benhnhan.gioitinh')} />
            </SimpleItem>
            <SimpleItem dataField="ngaySinh" editorType="dxDateBox" editorOptions={{displayFormat: 'dd/MM/yyyy', placeholder: 'Ngày sinh'}} >
              <Label text={translate('vienphi.benhnhan.ngaysinh')} />
            </SimpleItem>
          </GroupItem>
          <SimpleItem dataField="diaChi" cssClass="pt-0" editorType="dxTextBox" editorOptions={{ placeholder: 'Địa chỉ' }}>
            <Label text={translate('vienphi.benhnhan.diachi')} />
          </SimpleItem>
          <SimpleItem dataField="tenBenh" cssClass="pt-0" editorType="dxTextBox" editorOptions={{ placeholder: 'Chẩn đoán ICD' }}>
            <Label text={translate('vienphi.benhnhan.chanDoan')} />
          </SimpleItem>
        </GroupItem>
        {/* <GroupItem caption={translate('vienphi.benhnhan.thongtinbaohiem')} colCount={2}> */}
        <GroupItem>
          <GroupItem colCount={5}>
            <SimpleItem colSpan={2} editorType="dxTextBox" editorOptions={{ placeholder: 'Đối tượng bệnh nhân', value: data.coBhyt === 1 ? 'Bảo hiểm' : data.coBhyt === 0 ? 'Thu phí' : '', inputAttr: {style: 'font-weight: bold; color: red'} }}>
              <Label text={translate('vienphi.benhnhan.doituong')}/>
            </SimpleItem>
            <SimpleItem dataField="maThe" editorType="dxTextBox" colSpan={3} editorOptions={{ placeholder: 'Số thẻ BHYT' }}>
              <Label text={translate('vienphi.benhnhan.sothebhyt')}/>
            </SimpleItem>
          </GroupItem>
          <GroupItem colCount={4} cssClass="pt-0">
            <SimpleItem colSpan={2} dataField="phanTramThe" editorType="dxNumberBox" editorOptions={{ placeholder: 'Phần trăm bảo hiểm' }}>
              <Label text={translate('vienphi.benhnhan.phanTramBhyt')}/>
            </SimpleItem>
            <SimpleItem editorType="dxCheckBox" editorOptions={{ value: data.mienCungCt === 1 }} >
              <Label text={translate('vienphi.benhnhan.mienCungCt')} />
            </SimpleItem>
            <SimpleItem editorType="dxCheckBox" editorOptions={{ value: data.dungTuyen === 1 }} >
              <Label text={translate('vienphi.benhnhan.dungtuyen')} />
            </SimpleItem>
          </GroupItem>
          <GroupItem colCount={2} cssClass="pt-0">
            <SimpleItem dataField="gtTheTu" editorType="dxDateBox" editorOptions={{ displayFormat: 'dd/MM/yyyy', placeholder: 'Từ ngày' }}>
              <Label text={translate('vienphi.benhnhan.gtTheTu')} />
            </SimpleItem>
            <SimpleItem dataField="gtTheDen" editorType="dxDateBox"  editorOptions={{ displayFormat: 'dd/MM/yyyy', placeholder: 'Đến ngày' }}>
              <Label text={translate('vienphi.benhnhan.gtTheDen')} />
            </SimpleItem>
          </GroupItem>
          <SimpleItem cssClass="pt-0" dataField="noiDkbd" editorType="dxTextBox" colSpan={2} editorOptions={{ placeholder: 'Nơi ĐKBĐ' }}>
            <Label text={translate('vienphi.benhnhan.noiDkbd')}/>
          </SimpleItem>
        </GroupItem>
      </Form>
    );
  }

}
