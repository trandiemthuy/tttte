import React from 'react';
import { CheckBox } from 'devextreme-react/check-box';
import { getTenGioiTinh } from 'app/modules/vien-phi/utils/vienphi-function';

export const cellSttRender = cellData => <React.Fragment>{cellData.rowIndex + 1}</React.Fragment>

export const cellDateTimeRender = cellData => {
  const arr = cellData.text.split(' ');
  return <span>{arr[0]}<br/>{arr[1]}</span>;
};

export const cellCheckBoxRender = cellData => <CheckBox defaultValue={Number(cellData.text) === 1} readOnly />

export const cellGioiTinhRender = cellData => <React.Fragment>{getTenGioiTinh(Number(cellData.text))}</React.Fragment>;

export const groupCellRender = cellInfo => <strong>{cellInfo.text}</strong>;
