import React from 'react';
import { DateBox, Button as DateBoxButton } from 'devextreme-react/date-box';
import moment from 'moment';
import { VienPhiDefaultDateFormat } from "app/modules/vien-phi/utils/vienphi-constant";

export interface IVienPhiDateBoxComponentProps {
  value?: Date;
  hideButton?: boolean;
}

export interface IVienPhiDateBoxComponentState {
  dateValue: Date;
}

class VienPhiDateBoxComponent extends React.Component<IVienPhiDateBoxComponentProps, IVienPhiDateBoxComponentState> {

  constructor(props, state) {
    super(props, state);
    this.state = {
      dateValue: moment().toDate()
    }
  }

  onBtnClick = (name: string) => () => {
    const dateValue: Date =
      name === 'nextDate' ? moment(this.state.dateValue).add(1, 'days').toDate() :
        name === 'prevDate' ? moment(this.state.dateValue).subtract(1, 'days').toDate() :
          name === 'today' ? moment().toDate() : this.state.dateValue;
    this.setDateValue(dateValue);
  }

  onValueChanged = (e) => {
    const dateValue = e.value;
    this.setDateValue(dateValue);
  };

  setDateValue = (dateValue: Date) => {
    this.setState({
      dateValue
    });
    this.props.value.setTime(dateValue.getTime())
  }

  componentDidMount(): void {
    this.setState({
      dateValue: this.props.value
    })
  }

  render() {
    const { dateValue } = this.state;
    const todayButton = {
        icon: 'redo',
        type: 'default',
        visible: !this.props.hideButton,
        onClick: this.onBtnClick('today')
      };
      const prevDateButton = {
        icon: 'spinprev',
        stylingMode: 'text',
        visible: !this.props.hideButton,
        onClick: this.onBtnClick('prevDate')
      };
      const nextDateButton = {
        icon: 'spinnext',
        stylingMode: 'text',
        visible: !this.props.hideButton,
        onClick: this.onBtnClick('nextDate')
      };
    return (
      <DateBox value={dateValue}
               displayFormat={VienPhiDefaultDateFormat}
               stylingMode="outlined" onValueChanged={this.onValueChanged}
               inputAttr={{ style: 'text-align: center', readOnly: true }}
      >
        <DateBoxButton name="today" location="before" options={todayButton} />
        <DateBoxButton name="prevDate" location="before" options={prevDateButton} />
        <DateBoxButton name="nextDate" location="after" options={nextDateButton} />
        <DateBoxButton name="dropDown" />
      </DateBox>
    );
  }

}

export const VienPhiDateBox = (value: Date, hideButton = false) => () => (
  <VienPhiDateBoxComponent value = {value} hideButton={hideButton} />
);

export default VienPhiDateBox;
