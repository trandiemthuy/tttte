import React from 'react';

export interface IVienPhiTitleProps {
  title: string
}

class VienPhiTitle extends React.Component<IVienPhiTitleProps> {
  render() {
    return (
      <div className="border-bottom">
        <h3 className="text-center text-uppercase" style={{ color: '#01458e' }}>
          {this.props.title}
        </h3>
      </div>
    );
  }
}

export default VienPhiTitle;
