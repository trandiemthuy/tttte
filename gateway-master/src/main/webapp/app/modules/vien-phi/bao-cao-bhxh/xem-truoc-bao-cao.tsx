import React from 'react'
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { Button } from "devextreme-react";

export interface IVienPhiXemTruocBaoCaoProps extends StateProps, DispatchProps {
}

class VienPhiXemTruocBaoCao extends React.Component<IVienPhiXemTruocBaoCaoProps> {

  private iframeRef: HTMLIFrameElement;

  onBtnFullScreenClick = async e => {
    await this.iframeRef.requestFullscreen();
  }

  render() {
    const { url } = this.props;
    return (
      <div className="bg-light p-3">
        <div className="d-flex flex-row-reverse">
          <Button icon="mediumiconslayout"
                  text="Toàn màn hình"
                  type="default" stylingMode="outlined"
                  onClick={this.onBtnFullScreenClick}
                  disabled={!url || url === ''}
          />
        </div>
         <div className="mt-1" style={{height: 475}}>
           <iframe src={url} allowFullScreen
                   id="xem-bao-cao"
                   className="border" height="100%" width="100%"
                   ref={(self) => { this.iframeRef = self; }} />
         </div>
      </div>
     );
  }

}

const mapStateToProps = ({ baoCaoBhxh }: IRootState) => ({
  url: baoCaoBhxh.urlPreview
});

const mapDispatchToProps = {
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VienPhiXemTruocBaoCao);
