import React, { Component } from 'react';
import Form, { Label, SimpleItem } from 'devextreme-react/form';
import { Button, DropDownButton } from 'devextreme-react';
import { translate } from 'react-jhipster';
import { listLoaiKcb } from 'app/modules/vien-phi/models/loaikcb.model';
import { getListKieuFile } from 'app/modules/vien-phi/models/kieu-file.model';
import { dsMauBaoCaoBhxh } from 'app/modules/vien-phi/models/bao-cao-bhxh.model';
import { IRootState } from 'app/shared/reducers';
import { xemBaoCao } from 'app/modules/vien-phi/bao-cao-bhxh/bao-cao-bhxh.reducer';
import { connect } from 'react-redux';
import { VienPhiServicesUrl } from 'app/modules/vien-phi/utils/vienphi-constant';
import { convertDateToStringParam, getDvtt, printReportGet } from 'app/modules/vien-phi/utils/vienphi-function';
import axios from 'axios';
import notify from 'devextreme/ui/notify';
import { defaultRequestBaoCaoBhxh, IRequestBaoCaoBhxhModel } from 'app/modules/vien-phi/models/request-bao-cao-bhxh.model';
import VienPhiDateBox from 'app/modules/vien-phi/commons/datebox';

export interface IVienPhiFormRequestBaoCaoBhxhProps extends StateProps, DispatchProps {
}

export interface IVienPhiFormRequestBaoCaoBhxhState {
  formData: IRequestBaoCaoBhxhModel;
  dsKhoa: Array<any>;
}

class VienPhiFormRequestBaoCaoBhxh extends Component<IVienPhiFormRequestBaoCaoBhxhProps, IVienPhiFormRequestBaoCaoBhxhState> {

  constructor(props, state) {
    super(props, state);
    this.state = {
      formData: { ...defaultRequestBaoCaoBhxh },
      dsKhoa: []
    }
  }

  validateForm = (): boolean => {
    const { formData } = this.state;
    const allFieldsNotNull = Object.values(formData).filter(e => !e).length === 0;
    if (!allFieldsNotNull) {
      notify(translate('vienphi.baocaobhxh.dieukienkhonghopdetrong'), 'warning');
      return false;
    }
  }

  onBtnViewClick = e => {
    this.props.xemBaoCao(`${VienPhiServicesUrl}/api/bao-cao/bhxh/test?type=-1&id=${Math.random()}`);
  }

  onItemBtnPrintClick = async e => {
    const { formData } = this.state;
    const kieuFile = e.itemData.type;
    const data = {
      ...formData,
      tuNgay: convertDateToStringParam(formData.tuNgay),
      denNgay: convertDateToStringParam(formData.denNgay),
      kieuFile
    }
    // eslint-disable-next-line no-console
    const fileName = await printReportGet(`${VienPhiServicesUrl}/api/bao-cao/bhxh/test?type=${kieuFile}`, {});
    // eslint-disable-next-line no-console
    console.log(fileName);
  }

  dsKhoa = async () => {
    try {
      const url = `${VienPhiServicesUrl}/api/danh-sach-khoa?dvtt=${getDvtt()}`;
      const response = await axios.get<Array<any>>(url);
      return response.data ? response.data : [];
    }
    catch (e) {
      notify('Không thể load danh sách khoa !', 'error');
      return [];
    }
  }

  async componentDidMount() {
    const dsKhoa = await this.dsKhoa();
    this.setState({
      dsKhoa
    })
  }

  render() {
    const { formData, dsKhoa } = this.state;
    return (
      <React.Fragment>
        <Form id="form-bao-cao-bhxh"
              showColonAfterLabel
              formData={formData}
              className="p-3"
              labelLocation="top"
              colCount={2}
        >
          <SimpleItem cssClass="" colSpan={2} dataField="tuNgay" isRequired render={VienPhiDateBox(formData.tuNgay)}>
            <Label text={translate('vienphi.baocaobhxh.tungay')} />
          </SimpleItem>
          <SimpleItem cssClass="" colSpan={2} dataField="denNgay" isRequired render={VienPhiDateBox(formData.denNgay)}>
            <Label text={translate('vienphi.baocaobhxh.denngay')} />
          </SimpleItem>
          <SimpleItem cssClass="" dataField="loaiKcb" editorType="dxTagBox"
                      editorOptions={{
                        searchEnabled: true,
                        placeholder: 'Loại KCB',
                        showSelectionControls: true,
                        maxDisplayedTags: 0,
                        multiline: false,
                        selectAllMode: 'allPages',
                        applyValueMode: 'useButtons',
                        dataSource: listLoaiKcb,
                        displayExpr: 'name',
                        valueExpr: 'value'
                      }}
          >
            <Label text={translate('vienphi.baocaobhxh.loaikcb')} />
          </SimpleItem>
          <SimpleItem cssClass="" dataField="mauBaoCao" editorType="dxSelectBox"
                      editorOptions={{
                        searchEnabled: true,
                        placeholder: 'Mẫu báo cáo',
                        dataSource: dsMauBaoCaoBhxh,
                        displayExpr: 'name',
                        valueExpr: 'type'
                      }}
          >
            <Label text={translate('vienphi.baocaobhxh.maubaocao')} />
          </SimpleItem>
          <SimpleItem colSpan={2} cssClass="" dataField="donVi" editorType="dxSelectBox"
                      editorOptions={{
                        searchEnabled: true,
                        placeholder: 'Đơn vị',
                        dataSource: [],
                        displayExpr: 'name',
                        valueExpr: 'value'
                      }}
          >
            <Label text={translate('vienphi.baocaobhxh.domvi')} />
          </SimpleItem>
          <SimpleItem colSpan={2} cssClass="" dataField="khoa" editorType="dxTagBox"
                      editorOptions={{
                        searchEnabled: true,
                        showSelectionControls: true,
                        maxDisplayedTags: 0,
                        multiline: false,
                        selectAllMode: 'allPages',
                        applyValueMode: 'useButtons',
                        placeholder: 'Khoa',
                        dataSource: dsKhoa,
                        displayExpr: 'tenKhoa',
                        valueExpr: 'maKhoa'
                      }}
          >
            <Label text={translate('vienphi.baocaobhxh.khoa')} />
          </SimpleItem>
          <SimpleItem cssClass="" dataField="nhomChiPhi" editorType="dxSelectBox"
                      editorOptions={{
                        searchEnabled: true,
                        placeholder: 'Nhóm chi phí',
                        dataSource: [],
                        displayExpr: 'name',
                        valueExpr: 'type'
                      }}
          >
            <Label text={translate('vienphi.baocaobhxh.nhomchiphi')} />
          </SimpleItem>
          <SimpleItem cssClass="" dataField="nhomDoiTuong" editorType="dxSelectBox"
                      editorOptions={{
                        searchEnabled: true,
                        placeholder: 'Nhóm đối tượng',
                        dataSource: [],
                        displayExpr: 'name',
                        valueExpr: 'type'
                      }}
          >
            <Label text={translate('vienphi.baocaobhxh.nhomdoituong')} />
          </SimpleItem>
          <SimpleItem cssClass="" dataField="nguonTien" editorType="dxSelectBox"
                      editorOptions={{
                        searchEnabled: true,
                        placeholder: 'Nguồn tiền',
                        dataSource: [],
                        displayExpr: 'name',
                        valueExpr: 'type'
                      }}
          >
            <Label text={translate('vienphi.baocaobhxh.nguontien')} />
          </SimpleItem>
        </Form>
        <div className="d-flex justify-content-center p-2">
          <DropDownButton className="ml-2 mr-2" text="In báo cáo" icon="download" stylingMode="outlined"
            items={[...getListKieuFile()]} displayExpr="name" keyExpr="type"
            onItemClick={this.onItemBtnPrintClick}
          />
          <Button icon="chevronright" text="Xem" className="ml-2 mr-2" type="default" stylingMode="contained" onClick={this.onBtnViewClick} />
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ baoCaoBhxh }: IRootState) => ({
});

const mapDispatchToProps = {
  xemBaoCao
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VienPhiFormRequestBaoCaoBhxh);
