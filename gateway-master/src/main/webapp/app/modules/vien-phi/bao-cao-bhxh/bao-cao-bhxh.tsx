import React from 'react';
import VienPhiTitle from 'app/modules/vien-phi/commons/title';
import Box, { Item } from 'devextreme-react/box';
import { translate } from 'react-jhipster';
import VienPhiFormRequestBaoCaoBhxh from 'app/modules/vien-phi/bao-cao-bhxh/form-request-bao-cao-bhxh';
import VienPhiXemTruocBaoCao from 'app/modules/vien-phi/bao-cao-bhxh/xem-truoc-bao-cao';

class VienPhiBaoCaoBhxh extends React.Component<any, any> {
  render() {
    return (
      <div>
        <VienPhiTitle title={translate('vienphi.baocaobhxh.tieude')} />
        <Box>
          <Item ratio={3}>
            <VienPhiFormRequestBaoCaoBhxh />
          </Item>
          <Item ratio={9}>
            <VienPhiXemTruocBaoCao />
          </Item>
        </Box>
      </div>
    );
  }
}

export default VienPhiBaoCaoBhxh;
