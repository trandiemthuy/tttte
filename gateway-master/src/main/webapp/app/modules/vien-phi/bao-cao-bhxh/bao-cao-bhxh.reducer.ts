const ACTION_TYPE = {
  XEM_BAO_CAO: 'VienPhi/BaoCaoBhxh/XemBaoCao'
};

const initialState = {
  urlPreview: ''
};

export type BaoCaoBhxhState = Readonly<typeof initialState>;

export default (state: BaoCaoBhxhState = initialState, action): BaoCaoBhxhState => {
  switch (action.type) {
    case ACTION_TYPE.XEM_BAO_CAO:
      return {
        ...state,
        urlPreview: action.url
      };
    default:
      return state;
  }
};

export const xemBaoCao = url => async (dispatch, getState) => {
  await dispatch({
    type: ACTION_TYPE.XEM_BAO_CAO,
    url
  });
};
