import React from 'react';
import {Switch} from 'react-router-dom';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import Loadable from 'react-loadable';
import { LoadIndicator } from 'devextreme-react/load-indicator';
import 'app/modules/vien-phi/commons/vien-phi-datagrid.scss';

const loading = () => <div className="d-flex justify-content-center p-3"><LoadIndicator visible /></div>;

const VienPhiThuTien = Loadable({
  loader: () => import('app/modules/vien-phi/thu-vien-phi/thu-vien-phi'),
  loading
});

const VienPhiQuanLyBienLai = Loadable({
  loader: () => import('app/modules/vien-phi/quan-ly-bien-lai/quan-ly-bien-lai'),
  loading
});

const VienPhiKiemTraBangKe = Loadable({
  loader: () => import('app/modules/vien-phi/kiem-tra-bang-ke/kiem-tra-bang-ke'),
  loading
});

const VienPhiBaoCaoBhxh = Loadable({
  loader: () => import('app/modules/vien-phi/bao-cao-bhxh/bao-cao-bhxh'),
  loading
});

const VienPhiTaoSoLieuHangLoat = Loadable({
  loader: () => import('app/modules/vien-phi/tao-so-lieu-hang-loat/tao-so-lieu-hang-loat'),
  loading
});

const VienPhiImportFaker = Loadable({
  loader: () => import('app/modules/vien-phi/faker/faker'),
  loading
});

const VienPhiRoutes = ({match}) => (
<div id="vien-phi-module">
    <Switch>
      <ErrorBoundaryRoute path={`${match.url}/quan-ly-bien-lai`} component={VienPhiQuanLyBienLai} exact />
      <ErrorBoundaryRoute path={`${match.url}/thu-vien-phi`} component={VienPhiThuTien} exact />
      <ErrorBoundaryRoute path={`${match.url}/kiem-tra-bang-ke`} component={VienPhiKiemTraBangKe} exact />
      <ErrorBoundaryRoute path={`${match.url}/bao-cao-bhxh`} component={VienPhiBaoCaoBhxh} exact />
      <ErrorBoundaryRoute path={`${match.url}/tao-so-lieu-hang-loat`} component={VienPhiTaoSoLieuHangLoat} exact />
      <ErrorBoundaryRoute path={`${match.url}/import-faker-data`} component={VienPhiImportFaker} exact />
    </Switch>
  </div>
);

export default VienPhiRoutes;
