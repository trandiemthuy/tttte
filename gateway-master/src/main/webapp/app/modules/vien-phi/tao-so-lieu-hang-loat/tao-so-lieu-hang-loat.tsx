import React from 'react';
import Form, { GroupItem, Label, SimpleItem } from 'devextreme-react/form';
import { Button } from 'devextreme-react';
import { LoadIndicator } from 'devextreme-react/load-indicator';
import { Row } from 'reactstrap';
import VienPhiTitle from 'app/modules/vien-phi/commons/title';
import { translate } from 'react-jhipster';
import moment from 'moment';
import VienPhiDateBox from 'app/modules/vien-phi/commons/datebox';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { listLoaiKcb } from 'app/modules/vien-phi/models/loaikcb.model';
import { taoBangKeHangLoat } from 'app/modules/vien-phi/tao-so-lieu-hang-loat/tao-so-lieu-hang-loat.reducer';

export interface IVienPhiTaoSoLieuHangLoatState {
  formData: any;
}

export interface IVienPhiTaoSoLieuHangLoatProps extends StateProps, DispatchProps {
}

class VienPhiTaoSoLieuHangLoat extends React.Component<IVienPhiTaoSoLieuHangLoatProps, IVienPhiTaoSoLieuHangLoatState> {

  constructor(props,state) {
    super(props, state);
    this.state = {
      formData: {
        tuNgay: moment().toDate(),
        denNgay: moment().toDate(),
        loaiKcb: 1
      }
    }
  }

  onBtnTaoSoLieuClick = () => {
    const { tuNgay, denNgay, loaiKcb } = this.state.formData;
    this.props.taoBangKeHangLoat(tuNgay, denNgay, loaiKcb);
  }

  render() {
    const { formData } = this.state;
    const { creating, soLuongTaoThanhCong } = this.props;
    return (
      <React.Fragment>
        <VienPhiTitle title={translate('vienphi.taosolieuhangloat.tieude')} />
        <Row className="justify-content-center">
          <Form formData={formData}
            className="p-3"
            showColonAfterLabel
            labelLocation={'left'}
            colCount={7}
          >
            <SimpleItem colSpan={2} cssClass="p-1" isRequired render={VienPhiDateBox(formData.tuNgay)}>
              <Label text={translate('vienphi.taosolieuhangloat.tungay')} />
            </SimpleItem>
            <SimpleItem colSpan={2} cssClass="p-1" isRequired render={VienPhiDateBox(formData.denNgay)}>
              <Label text={translate('vienphi.taosolieuhangloat.denngay')} />
            </SimpleItem>
            <SimpleItem cssClass="p-1" colSpan={2} dataField="loaiKcb" editorType="dxSelectBox"
                        editorOptions={{
                          placeholder: 'Loại KCB',
                          dataSource: listLoaiKcb,
                          displayExpr: 'name',
                          valueExpr: 'value'
                        }}
            >
              <Label text={translate('vienphi.taosolieuhangloat.loaikcb')} />
            </SimpleItem>
            <GroupItem cssClass="p-1">
              <Row className="justify-content-center">
                <Button type="default" stylingMode="contained" disabled={creating} onClick={this.onBtnTaoSoLieuClick}>
                  <span className="dx-button-text">{translate('vienphi.taosolieuhangloat.taosolieu')}</span>
                </Button>
                <LoadIndicator height={25} width={25}  visible={creating} className="pr-2 pl-2" />
              </Row>
            </GroupItem>
          </Form>
        </Row>
        <h5 className={soLuongTaoThanhCong ? 'text-center' : 'd-none'}>
            Số lượng bệnh nhân đã tạo số liệu thành công:
            <i className="text-success pl-2 pr-2">
              {soLuongTaoThanhCong}
            </i>
        </h5>
      </React.Fragment>
    );
  }

}

const mapStateToProps = ({ taoSoLieuHangLoat }: IRootState) => ({
  creating: taoSoLieuHangLoat.creating,
  soLuongTaoThanhCong: taoSoLieuHangLoat.soLuongTaoThanhCong
});

const mapDispatchToProps = {
  taoBangKeHangLoat
}

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VienPhiTaoSoLieuHangLoat);
