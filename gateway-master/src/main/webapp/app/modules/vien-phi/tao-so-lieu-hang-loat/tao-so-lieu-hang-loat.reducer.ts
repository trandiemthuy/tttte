import { VienPhiServicesUrl } from 'app/modules/vien-phi/utils/vienphi-constant';
import { convertDateToStringParam, getDvtt } from 'app/modules/vien-phi/utils/vienphi-function';
import notify from 'devextreme/ui/notify';
import axios from 'axios';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';

export const ACTION_TYPE = {
  TAO_SO_LIEU: 'VienPhi/TaoSoLieuHangLoat/TaoSoLieu'
};

const initialState = {
  creating: false,
  soLuongTaoThanhCong: null
};

export type TaoSoLieuHangLoatState = Readonly<typeof initialState>;

export default (state: TaoSoLieuHangLoatState = initialState, action): TaoSoLieuHangLoatState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPE.TAO_SO_LIEU):
      return {
        ...state,
        creating: true,
        soLuongTaoThanhCong: null
      };
    case SUCCESS(ACTION_TYPE.TAO_SO_LIEU):
      notify('Tạo thành công', 'succes');
      return {
        ...state,
        creating: false,
        soLuongTaoThanhCong: action.payload.data
      };
    case FAILURE(ACTION_TYPE.TAO_SO_LIEU):
      if (action.payload.response.status === 409) {
        notify(action.payload.response.data.message, 'error');
      } else {
        notify('Đã có lỗi xảy ra !', 'error');
      }
      return {
        ...state,
        creating: false,
        soLuongTaoThanhCong: null
      };
    default:
      return state;
  }
};

export const taoBangKeHangLoat = (tuNgay, denNgay, loaiKcb) => async (dispatch, getState) => {
  const url = `${VienPhiServicesUrl}/api/bang-ke/tao-so-lieu-hang-loat`;
  const data = {
    tuNgay: convertDateToStringParam(tuNgay),
    denNgay: convertDateToStringParam(denNgay),
    loaiKcb,
    dvtt: getDvtt()
  };
  await dispatch({
    type: ACTION_TYPE.TAO_SO_LIEU,
    payload: axios.post(url, data)
  });
};
