import React from 'react';
import { translate } from 'react-jhipster';
import DataGrid, { Column, Editing, Lookup } from 'devextreme-react/data-grid';
import { IVienPhiQuyenBienLaiModel } from 'app/modules/vien-phi/models/quyen-bien-lai.model';
import CustomStore from 'devextreme/data/custom_store';
import { VienPhiDefaultDatetimeFormat, VienPhiServicesUrl } from 'app/modules/vien-phi/utils/vienphi-constant';
import { getDvtt } from 'app/modules/vien-phi/utils/vienphi-function';
import axios from 'axios';
import notify from 'devextreme/ui/notify';
import { cellDateTimeRender } from 'app/modules/vien-phi/commons/data-grid-components';
import { LoadPanel } from "devextreme-react/load-panel";

const url: Readonly<string> = `${VienPhiServicesUrl}/api/vien-phi/quyen-bien-lai/`;
const dsNgoaiTru: Readonly<Array<any>> = [
  {
    value: -1,
    name: 'Tất cả'
  },
  {
    value: 1,
    name: 'Ngoại trú'
  },
  {
    value: 0,
    name: 'Nội trú'
  }
];
const dsBhyt: Readonly<Array<any>> = [
  {
    value: -1,
    name: 'Tất cả'
  },
  {
    value: 1,
    name: 'Có BHYT'
  },
  {
    value: 0,
    name: 'Không BHYT'
  }
];
const dsTamUng: Readonly<Array<any>> = [
  {
    value: -1,
    name: 'Tất cả'
  },
  {
    value: 1,
    name: 'Tạm ứng'
  },
  {
    value: 0,
    name: 'Phiếu thu'
  }
];
const dsTrangThai: Readonly<Array<any>> = [
  {
    value: 1,
    name: 'Hoạt động'
  },
  {
    value: 2,
    name: 'Vô hiệu'
  }
];

export interface IVienPhiGridBienLaiState {
  dataSource: any;
  dsNhanVien: Array<any>;
  initializing: boolean;
}

/* eslint @typescript-eslint/member-ordering: off */
class VienPhiGridBienLai extends React.Component<any, IVienPhiGridBienLaiState> {

  constructor(props, state) {
    super(props, state);
    this.state = {
      dataSource: [],
      dsNhanVien: [],
      initializing: true
    }
  }

  textsEditing = {
    confirmDeleteTitle: translate('vienphi.quanlybienlai.quanlybienlai.xacnhan'),
    confirmDeleteMessage: translate('vienphi.quanlybienlai.quanlybienlai.xacnhanxoa'),
  }

  load = async () => {
    const response = await axios.get<Array<IVienPhiQuyenBienLaiModel>>(`${url}?dvtt=${getDvtt()}`);
    return response.data;
  }

  insert = async (values) => {
    const data = {
      ...values,
      trangThai: values.trangThai ? 1 : 0,
      dvtt: getDvtt()
    }
    const response = await axios.post(`${url}`, data);
    if (response.status === 201) {
      notify(translate('vienphi.quanlybienlai.quanlybienlai.dathem'), 'success')
    } else {
      notify(response.data.message, 'error')
    }
  }

  update = async (key, values) => {
    const data = { key, values }
    const response = await axios.put(`${url}edit`, data);
    if (response.status === 200) {
      notify(translate('vienphi.quanlybienlai.quanlybienlai.dacapnhat'), 'success')
    }
    else {
      notify(response.data.message,'error' )
    }
  }

  remove = async key => {
    const response = await axios.delete(`${url}${key}`);
    if (response.status === 204) {
      notify(translate('vienphi.quanlybienlai.quanlybienlai.daxoa'), 'success')
    } else {
      notify(response.data.message, 'error')
    }
  }

  dsNhanVien = async () => {
    try {
      const response = await axios.get(`${VienPhiServicesUrl}/api/ds-nhan-vien?dvtt=${getDvtt()}`);
      return  response.data ? response.data : []
    }
    catch (e) {
      return [];
    }
  }

  async componentDidMount() {
    const dataSource = new CustomStore({
      key: 'maQuyenBienLai',
      loadMode: 'raw',
      load: this.load,
      update: this.update,
      insert: this.insert,
      remove: this.remove
    });
    const dsNhanVien = await this.dsNhanVien();
    this.setState({
      dataSource,
      dsNhanVien,
      initializing: false
    })
  }

  render() {
    const { dataSource, dsNhanVien, initializing } = this.state;
    return (
      <React.Fragment>
        <LoadPanel
          shadingColor="rgba(0,0,0,0.4)"
          position={{ of: '#grid-bien-lai' }}
          visible={initializing}
          showIndicator
          shading={false}
          showPane
          closeOnOutsideClick={false}
        />
        <DataGrid
          id="grid-bien-lai"
          dataSource={dataSource}
          allowColumnReordering={true}
          // columnAutoWidth
          allowColumnResizing
          columnResizingMode={'widget'}
          showBorders
          showRowLines
          hoverStateEnabled
          className="m-1"
          height={465}
          filterRow={{  visible: true, applyFilter: 'auto' }}
          // headerFilter={{ visible: true }}
          pager={{ visible: true, showPageSizeSelector: true, allowedPageSizes: [10, 20, 50], showInfo: true, showNavigationButtons: true }}
        >
          <Editing mode="row" texts={this.textsEditing} allowUpdating allowAdding allowDeleting confirmDelete useIcons />
          <Column dataField="maQuyenBienLai" cssClass="font-weight-bold" caption="Mã quyển BL" width={50} allowEditing={false} />
          <Column dataField="tenQuyenBienLai" cssClass="font-weight-bold" caption="Tên quyên BL" validationRules={[{ type: "required" }]} />
          <Column dataField="kyHieuQuyenBienLai" caption="Ký hiệu" validationRules={[{ type: "required" }]} />
          <Column dataField="soBatDau" caption="Số bắt đầu" width={80} validationRules={[{ type: "required" }]} />
          <Column dataField="soKetThuc" caption="Số kết thúc" width={80} validationRules={[{ type: "required" }]} />
          <Column dataField="soHienTai" caption="Số hiện tại" width={80} validationRules={[{ type: "required" }]} />
          <Column dataField="trangThai" caption="Trạng thái" width={75} alignment="center" validationRules={[{ type: "required" }]}>
            <Lookup dataSource={dsTrangThai} displayExpr="name" valueExpr="value" />
          </Column>
          <Column dataField="nhanVien" dataType="number" caption="Nhân viên" validationRules={[{ type: "required" }]}>
            <Lookup dataSource={dsNhanVien} displayExpr={'tenNhanVien'} valueExpr={'maNhanVien'} />
          </Column>
          <Column dataField="ngoaiTru" caption="Ngoại trú" validationRules={[{ type: "required" }]}>
            <Lookup dataSource={dsNgoaiTru} displayExpr="name" valueExpr="value" />
          </Column>
          <Column dataField="tamUng" caption="Tạm ứng" validationRules={[{ type: "required" }]}>
            <Lookup dataSource={dsTamUng} displayExpr="name" valueExpr="value" />
          </Column>
          <Column dataField="bhyt" caption="BHYT" validationRules={[{ type: "required" }]}>
            <Lookup dataSource={dsBhyt} displayExpr="name" valueExpr="value" />
          </Column>
          <Column dataField="ngayGioTao" caption="Ngày giờ tạo" alignment="center" dataType="datetime" cellRender={cellDateTimeRender} format={VienPhiDefaultDatetimeFormat} allowEditing={false} />
        </DataGrid>
      </React.Fragment>
    );
  }

}

export default VienPhiGridBienLai;
