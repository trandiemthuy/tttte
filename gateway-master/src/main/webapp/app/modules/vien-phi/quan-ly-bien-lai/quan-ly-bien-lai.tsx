import React, { Component } from 'react';
import VienPhiTitle from 'app/modules/vien-phi/commons/title';
import { translate } from 'react-jhipster';
import VienPhiGridBienLai from 'app/modules/vien-phi/quan-ly-bien-lai/grid-bien-lai';

class VienPhiQuanLyBienLai extends Component {

  render() {
    return (
      <div>
        <VienPhiTitle title={translate('vienphi.quanlybienlai.quanlybienlai.tieude')}/>
        <VienPhiGridBienLai />
      </div>
    );
  }

}

export default VienPhiQuanLyBienLai;
