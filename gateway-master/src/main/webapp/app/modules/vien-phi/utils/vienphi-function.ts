import { listLoaiKcb } from 'app/modules/vien-phi/models/loaikcb.model';
import { listGioiTinh } from 'app/modules/vien-phi/models/gioi-tinh.model';
import moment from 'moment';
import CustomStore from 'devextreme/data/custom_store';
import { saveAs } from 'file-saver';
import axios from 'axios';

export const getDvtt = (): string => '91001';

export const getTenLoaiKcb = (value: number): string => {
  const loaiKcb = listLoaiKcb.find(e => e.value === value);
  return loaiKcb === undefined || loaiKcb === null ? '' : loaiKcb.name;
};

export const getTenGioiTinh = (value: number): string => {
  const gioiTinh = listGioiTinh.find(e => e.value === value);
  return gioiTinh === undefined || gioiTinh === null ? '' : gioiTinh.name;
};

export const convertDateToStringParam = (d: Date): string => moment(d).format('YYYY-MM-DD');

export const convertDateTimeToStringParam = (d: Date): string => moment(d).format('YYYY-MM-DD HH:mm:ss');

export const nvl = (value, defaultValue) => (value ? value : defaultValue);

export const initCustomStore = () =>
  new CustomStore({
    key: 'key',
    loadMode: 'raw',
    load: () => []
  });

export const printReport = (url: string, printAfterOpen: boolean): void => {
  const w = window.open(url, '_blank');
  if (printAfterOpen) {
    w.focus();
    w.print();
    w.onafterprint = () => {
      w.close();
    };
  }
};

export const printReportPost = async (url: string, data: any): Promise<string> => {
  try {
    const response = await axios.post(url, data, { responseType: 'blob' });
    const fileName = response.headers['content-disposition'].match(/filename="(.+)"/)[1];
    await saveAs(response.data, fileName);
    return fileName;
  } catch (e) {
    return Promise.reject(e);
  }
};

export const printReportGet = async (url: string, params: any): Promise<string> => {
  try {
    const response = await axios.get(url, { params, responseType: 'blob' });
    const fileName = response.headers['content-disposition'].match(/filename="(.+)"/)[1];
    await saveAs(response.data, fileName);
    return fileName;
  } catch (e) {
    return Promise.reject(e);
  }
};

export const boDauTiengViet = (str: string): string =>
  (str = str
    .toLowerCase()
    .replace(/[àáạảãâầấậẩẫăằắặẳẵ]/g, 'a')
    .replace(/[èéẹẻẽêềếệểễ]/g, 'e')
    .replace(/[ìíịỉĩ]/g, 'i')
    .replace(/[òóọỏõôồốộổỗơờớợởỡ]/g, 'o')
    .replace(/[ùúụủũưừứựửữ]/g, 'u')
    .replace(/[ỳýỵỷỹ]/g, 'y')
    .replace(/đ/g, 'd'));
