export const VienPhiServicesUrl: Readonly<string> = '/services/vienphi';

export const VienPhiDefaultNumberFormat: Readonly<string> = '#,##0.00';

export const VienPhiDefaultDatetimeFormat: Readonly<string> = 'dd/MM/yyyy HH:mm:ss';

export const VienPhiDefaultDateFormat: Readonly<string> = 'dd/MM/yyyy';
