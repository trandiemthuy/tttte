import moment from 'moment';

export interface IRequestBaoCaoBhxhModel {
  tuNgay: Date;
  denNgay: Date;
  loaiKcb: Array<number>;
  mauBaoCao: number;
  donVi: string;
  khoa: Array<string>;
  nhomChiPhi: Array<number>;
  nhomDoiTuong: Array<string>;
  nguonTien: Array<number>;
}

export const defaultRequestBaoCaoBhxh: Readonly<IRequestBaoCaoBhxhModel> = {
  tuNgay: moment().toDate(),
  denNgay: moment().toDate(),
  loaiKcb: [],
  mauBaoCao: null,
  donVi: null,
  khoa: [],
  nhomChiPhi: [],
  nhomDoiTuong: [],
  nguonTien: []
};
