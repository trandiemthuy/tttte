export interface IVienPhiBangKeModel {
  tenNhom: string;
  tenNhomCon: string;
  ngoaiDanhMuc: number;
  noiDung: string;
  dvt: string;
  soLuong: number;
  donGiaBv: number;
  donGiaBh: number;
  tyLeThanhToanTheoDv: number;
  thanhTienBv: number;
  tyLeThanhToanBhyt: number;
  thanhTienBh: number;
  quyBhyt: number;
  cungChiTra: number;
  khac: number;
  nguoiBenh: number;
  soPhieuMaDv: string;
  mucHuong: number;
}
