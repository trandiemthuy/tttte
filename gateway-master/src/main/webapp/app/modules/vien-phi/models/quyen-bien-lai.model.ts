export interface IVienPhiQuyenBienLaiModel {
  maQuyenBienLai: number;
  tenQuyenBienLai: string;
  kyHieuQuyenBienLai: string;
  dvtt: string;
  soBatDau: number;
  soKetThuc: number;
  soHienTai: number;
  trangThai: number;
  nhanVien: string;
  ngoaiTru: number;
  tamUng: number;
  bhyt: number;
  ngayGioTao: Date;
}
