export interface IChiTietThanhToanModel {
  soPhieuMaDv: string;
  nhomLanTt: string;
  noiDung: string;
  dvt: string;
  soLuong: number;
  donGia: number;
  thanhTien: number;
  tBhtt: number;
  tBncct: number;
  tBntt: number;
  tNguonKhac: number;
  tTranTt: number;
  phaiTt: number;
  ghiChuChiTietLanTt: string;
  tenNhomBangKe: string;
}
