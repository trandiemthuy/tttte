export interface IVienPhiBenhNhanModel {
  soVaoVien: number;
  soVaoVienDt: number;
  loaiKcb: number;
  dvtt: string;

  maBn: number;
  hoTen: string;
  ngaySinh: Date;
  gioiTinh: number;
  diaChi: string;

  maKhoa: string;

  coBhyt: number;
  maThe: string;
  phanTramThe: number;
  mienCungCt: number;
  gtTheTu: Date;
  gtTheDen: Date;
  noiDkbd: string;
  dungTuyen: number;

  ngayVao: Date;
  ngayRa: Date;

  tenBenh: string;
  trangThaiBn: string;
}

export const defaultVienPhiBenhNhanData: Readonly<IVienPhiBenhNhanModel> = {
  dungTuyen: null,
  maKhoa: '',
  trangThaiBn: '',
  coBhyt: null,
  diaChi: '',
  dvtt: '',
  gioiTinh: null,
  gtTheDen: null,
  gtTheTu: null,
  hoTen: '',
  loaiKcb: null,
  maBn: null,
  mienCungCt: null,
  ngayRa: null,
  ngaySinh: null,
  ngayVao: null,
  noiDkbd: '',
  phanTramThe: null,
  maThe: '',
  soVaoVien: null,
  soVaoVienDt: null,
  tenBenh: null
};
