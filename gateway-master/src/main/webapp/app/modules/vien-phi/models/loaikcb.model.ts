export interface ILoaiKcb {
  value: number;
  name: string;
}

export const listLoaiKcb: Array<ILoaiKcb> = [
  {
    value: 1,
    name: 'Ngoại trú'
  },
  {
    value: 2,
    name: 'BANT'
  },
  {
    value: 3,
    name: 'Nội trú'
  }
];

export const loaiKcbTatCa: ILoaiKcb = {
  value: -1,
  name: 'Tất ca'
};

export const listLoaiKcbTatCa: Array<ILoaiKcb> = [loaiKcbTatCa, ...listLoaiKcb];
