export interface IHinhThucThanhToan {
  value: number;
  name: string;
}

export const listHinhThucThanhToan: Array<IHinhThucThanhToan> = [
  {
    value: 1,
    name: 'Tiền mặt'
  }
];
