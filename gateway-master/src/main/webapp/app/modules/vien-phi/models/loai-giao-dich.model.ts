export interface ILoaiGiaoDich {
  value: number;
  name: string;
}

export const listLoaiGiaoDich: Array<ILoaiGiaoDich> = [
  {
    value: 1,
    name: 'THANH TOÁN VIỆN PHÍ'
  },
  {
    value: 2,
    name: 'THU TẠM ỨNG'
  }
];
