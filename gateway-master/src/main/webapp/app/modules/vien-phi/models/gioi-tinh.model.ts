export interface IGioiTinh {
  value: number;
  name: string;
}

export const listGioiTinh: Readonly<Array<IGioiTinh>> = [
  {
    value: 1,
    name: 'Nam'
  },
  {
    value: 2,
    name: 'Nữ'
  },
  {
    value: 3,
    name: 'Khác'
  }
];
