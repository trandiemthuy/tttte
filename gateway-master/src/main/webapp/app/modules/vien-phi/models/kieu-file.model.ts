export type reportExtension = 'PDF' | 'XLS' | 'XLSX' | 'RTF' | 'HTML';

export interface IVienPhiKieuFileModel {
  type: number;
  name: reportExtension;
}

const listKieuFile: Readonly<Array<IVienPhiKieuFileModel>> = [
  { type: 0, name: 'PDF' },
  { type: 1, name: 'XLS' },
  { type: 2, name: 'XLSX' },
  { type: 3, name: 'RTF' },
  { type: 4, name: 'HTML' }
];

export const getListKieuFile = (allow?: Array<reportExtension>) => {
  if (allow === undefined) {
    return listKieuFile;
  } else {
    return listKieuFile.filter(e => allow.findIndex(el => el === e.name) > -1);
  }
};
