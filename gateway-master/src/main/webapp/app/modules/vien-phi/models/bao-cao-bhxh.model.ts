export interface IMauBaoCaoBhxh {
  type: number;
  name: string;
}

export const dsMauBaoCaoBhxh: Readonly<Array<IMauBaoCaoBhxh>> = [
  { type: 1, name: 'Mẫu 79' },
  { type: 2, name: 'Mẫu 80' },
  { type: 3, name: 'Mẫu 19' },
  { type: 4, name: 'Mẫu 20' },
  { type: 5, name: 'Mẫu 21' }
];
