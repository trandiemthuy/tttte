import { IVienPhiQuyenBienLaiModel } from 'app/modules/vien-phi/models/quyen-bien-lai.model';

export interface IVienPhiLanTamUngModel {
  idLanTamUng: number;
  soVaoVien: number;
  soVaoVienDt: number;
  loaiKcb: number;
  dvtt: string;
  sttLanTamUng: string;
  maBn: number;
  ngayGioTao: Date;
  ngayGioCapNhat: Date;
  maNvTao: string;
  tenNvTao: string;
  ngayThu: Date;
  maPhongBanThu: string;
  soTienTamUng: number;
  soTienBnTra: number;
  soTienThoiLai: number;
  vienPhiQuyenBienLai: IVienPhiQuyenBienLaiModel;
  soBienLai: number;
  ghiChu: string;
  ttLanTamUng: number;
  ttSuDung: number;
  idLanTT: number;
  idLanTamUngNgoai: number;
}
