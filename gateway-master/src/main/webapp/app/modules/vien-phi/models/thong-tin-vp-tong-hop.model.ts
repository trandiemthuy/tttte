export interface IVienPhiThongTinVpTongHopModel {
  tongDaThu: number;
  thanhToan: number;
  tamUng: number;
  tamUngConLai: number;
  tongChiPhi: number;
  tongBh: number;
  bhChiTra: number;
  cungChiTra: number;
  bnPhaiTra: number;
  hoanUng: number;
  conLai: number;
}

export const defaultThongTinVpTongHop: Readonly<IVienPhiThongTinVpTongHopModel> = {
  tongDaThu: 0,
  thanhToan: 0,
  tamUng: 0,
  tamUngConLai: 0,
  tongChiPhi: 0,
  tongBh: 0,
  bhChiTra: 0,
  cungChiTra: 0,
  bnPhaiTra: 0,
  hoanUng: 0,
  conLai: 0
};
