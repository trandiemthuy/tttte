import React from 'react';
import { Translate, translate } from 'react-jhipster';
import TabPanel, { Item } from 'devextreme-react/tab-panel';
import GridVienPhiLanThanhToan from 'app/modules/vien-phi/thu-vien-phi/grid-lan-thanh-toan';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import GridVienPhiLanTamUng from 'app/modules/vien-phi/thu-vien-phi/grid-lan-tam-ung';

export interface IVienPhiDsLanThuProps extends StateProps, DispatchProps{
}

export class VienPhiDsLanThu extends React.Component<IVienPhiDsLanThuProps> {

  render() {
    return (
      <div className="pl-3">
        <u><h5><i className="dx-icon-detailslayout" /> <Translate contentKey="vienphi.dslanthu.dslanthu" /></h5></u>
        <TabPanel
          animationEnabled
          focusStateEnabled={false}
          height={300}
          className="pb-1"
        >
          <Item key="tab-thanh-toan" title={translate('vienphi.dslanthu.thanhtoan')}>
            <GridVienPhiLanThanhToan />
          </Item>
          <Item key="tab-tam-ung" title={translate('vienphi.dslanthu.tamung')}>
            <GridVienPhiLanTamUng />
          </Item>
        </TabPanel>
      </div>
    );
  }
}

const mapStateToProps = ({ vienPhiThuTien }: IRootState) => ({
  loaiGiaoDich: vienPhiThuTien.formDataTtLanThu.loaiGiaoDich
});

const mapDispatchToProps = {
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VienPhiDsLanThu);
