import React from 'react';
import { Button } from 'devextreme-react';
import Form, { GroupItem, Label, SimpleItem } from 'devextreme-react/form';
import { LoadPanel } from 'devextreme-react/load-panel';
import { translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import ArrayStore from 'devextreme/data/array_store';
import { listLoaiGiaoDich } from 'app/modules/vien-phi/models/loai-giao-dich.model';
import { listHinhThucThanhToan } from 'app/modules/vien-phi/models/hinh-thuc-tt.model';
import {
  getQuyenBienLai,
  refreshFormLanThu,
  thanhToanVienPhi,
  thuTamUng
} from 'app/modules/vien-phi/thu-vien-phi/thu-vien-phi.reducer';
import notify from "devextreme/ui/notify";
import { VienPhiDefaultNumberFormat } from 'app/modules/vien-phi/utils/vienphi-constant';

export interface IVienPhiTtLanThuProps extends StateProps, DispatchProps {
}

export class VienPhiTtLanThu extends React.Component<IVienPhiTtLanThuProps> {

  private dataSourceLoaiGiaoDich = new ArrayStore({
    data: listLoaiGiaoDich,
    key: 'value'
  });

  private dataSourceHinhThucTt = new ArrayStore({
    data: listHinhThucThanhToan,
    key: 'value'
  })

  onLoaiGiaoDichChange = e => {
    this.props.getQuyenBienLai();
  }

  inValidFormData = (): boolean => {
    const { formData } = this.props;
    if (formData.loaiGiaoDich === 1) {
      return formData.ngayThu === null ||
        (!formData.khongDungBienLai && (formData.maQuyenBienLai === null || formData.soBienLai === null));
    }
    else {
      return formData.ngayThu === null ||
        (!formData.khongDungBienLai && (formData.maQuyenBienLai === null || formData.soBienLai === null)) ||
        formData.soTienThuTamUng === null || formData.soTienThuTamUng <= 0;
    }
  }

  onBtnSaveClick = (print: boolean) => e =>  {
    if (this.inValidFormData()) {
      notify(translate('vienphi.ttlanthu.kiemtralaithongtin'), 'warning');
      return;
    }
    const { loaiGiaoDich } = this.props.formData;
    if (loaiGiaoDich === 1) {
      this.props.thanhToanVienPhi(print);
    }
    else if (loaiGiaoDich === 2) {
       this.props.thuTamUng(print);
    }
  }

  render() {
    const { dsQuyenBienLai, formData, savingLanThu, benhNhanSelected, loadingBienLai, searchingBenhNhan } = this.props;
    const dataSourceQuyenBienLai = new ArrayStore({
      data: dsQuyenBienLai,
      key: 'maQuyenBienLai'
    });
    return (
      <React.Fragment>
        <LoadPanel
          shadingColor="rgba(0,0,0,0.4)"
          position={{ of: '#form-thong-tin-thu-tien' }}
          visible={savingLanThu}
          showIndicator
          shading={false}
          showPane
          closeOnOutsideClick={false}
        />
        <Form
          id="form-thong-tin-thu-tien"
          formData={formData}
          showColonAfterLabel
          disabled={savingLanThu || benhNhanSelected.maBn === null || loadingBienLai || searchingBenhNhan}
        >
          {/* <GroupItem colCount={2} caption={translate('vienphi.ttlanthu.thongtinthu')}> */}
          <GroupItem colCount={2}>
            <SimpleItem cssClass="pt-0 font-weight-bold" dataField="loaiGiaoDich" editorType="dxSelectBox"
                        editorOptions={{
                          searchEnabled: true,
                          placeholder: 'Loại giao dịch',
                          dataSource: this.dataSourceLoaiGiaoDich,
                          displayExpr: 'name',
                          valueExpr: 'value',
                          onValueChanged: this.onLoaiGiaoDichChange,
                          inputAttr: {
                            style: 'font-weight: bold;'
                          }
                        }}>
              <Label text={translate('vienphi.ttlanthu.loaigiaodich')} />
            </SimpleItem>
            <GroupItem colCount={4}>
              <SimpleItem dataField="ngayThu" editorType="dxDateBox"
                          colSpan={3}
                          editorOptions={{
                            displayFormat: 'dd/MM/yyyy HH:mm:ss',
                            placeholder: 'Ngày thu tiền',
                            type: 'datetime',
                            disabled: formData.layNgayThuHienTai
                          }}>
                <Label text={translate('vienphi.ttlanthu.ngaythu')}/>
              </SimpleItem>
              <SimpleItem dataField="layNgayThuHienTai"
                          editorType="dxCheckBox"
                          editorOptions={{
                            onValueChanged: this.props.refreshFormLanThu
                          }} >
                <Label text={translate('vienphi.ttlanthu.ngaygiohientai')} />
              </SimpleItem>
            </GroupItem>
          </GroupItem>
          <GroupItem colCount={2} cssClass="pt-0">
            <SimpleItem dataField="maQuyenBienLai" editorType="dxSelectBox"
                        editorOptions={{
                          placeholder: 'Chọn quyển biên lai',
                          searchEnabled: true,
                          dataSource: dataSourceQuyenBienLai,
                          displayExpr: 'tenQuyenBienLai',
                          valueExpr: 'maQuyenBienLai',
                          disabled: formData.khongDungBienLai
                        }}>
              <Label text={translate('vienphi.ttlanthu.quyenbienlai')} />
            </SimpleItem>
            <GroupItem colCount={3}>
              <SimpleItem dataField="soBienLai" colSpan={2} editorType="dxNumberBox"
                          editorOptions={{
                            showSpinButtons: true,
                            showClearButton: true,
                            placeholder: 'Nhập số biên lai',
                            disabled: formData.khongDungBienLai
                          }}>
                <Label text={translate('vienphi.ttlanthu.sobienlai')} />
              </SimpleItem>
              <SimpleItem editorType="dxCheckBox" dataField="khongDungBienLai"
                          editorOptions={{
                            onValueChanged: this.props.refreshFormLanThu
                          }}>
                <Label text={translate('vienphi.ttlanthu.khongdungbienlai')} />
              </SimpleItem>
            </GroupItem>
          </GroupItem>
          <GroupItem cssClass="pt-0" visible={formData.loaiGiaoDich === 1}>
            <GroupItem cssClass="pt-0" colCount={2}>
              <SimpleItem dataField="soTienPhaiTt" editorType="dxNumberBox" editorOptions={{placeholder: 'Số tiền phải thanh toán', format: VienPhiDefaultNumberFormat, readOnly: true, inputAttr: {style: 'color: red; font-weight: bold'} }}>
                <Label text={translate('vienphi.ttlanthu.soTienPhaiTt')} />
              </SimpleItem>
            </GroupItem>
            <GroupItem cssClass="pt-0" colCount={2}>
              <SimpleItem dataField="soTienMienGiam" editorType="dxNumberBox" editorOptions={{disabled: true, placeholder: 'Số tiền miễn giảm', format: VienPhiDefaultNumberFormat, readOnly: true, inputAttr: {style: 'color: red; font-weight: bold'}}}>
                <Label text={translate('vienphi.ttlanthu.soTienMienGiam')} />
              </SimpleItem>
              <SimpleItem editorType="dxNumberBox" editorOptions={{placeholder: 'Số tiền đã tạm ứng', value: this.props.soTienTamUngConLai, format: VienPhiDefaultNumberFormat, readOnly: true, inputAttr: {style: 'color: red; font-weight: bold'}}}>
                <Label text={translate('vienphi.ttlanthu.soTienDaTamUng')} />
              </SimpleItem>
            </GroupItem>
            <GroupItem cssClass="pt-0" colCount={2}>
              <SimpleItem dataField="soTienThucThu" editorType="dxNumberBox" editorOptions={{placeholder: 'Số tiền thực thu', format: VienPhiDefaultNumberFormat, readOnly: true, inputAttr: {style: 'font-weight: bold; color: blue'}}}>
                <Label text={translate('vienphi.ttlanthu.soTienThucThu')} />
              </SimpleItem>
              <SimpleItem dataField="soTienThoiLai" editorType="dxNumberBox" editorOptions={{placeholder: 'Số tiền thối lại', format: VienPhiDefaultNumberFormat, readOnly: true, inputAttr: {style: 'color: black; font-weight: bold'}}}>
                <Label text={translate('vienphi.ttlanthu.soTienThoiLai')} />
              </SimpleItem>
            </GroupItem>
            <SimpleItem cssClass="pt-0 d-none" dataField="hinhThucTt" editorType="dxSelectBox"
                        editorOptions={{
                          placeholder: 'Hình thức thanh toán',
                          dataSource: this.dataSourceHinhThucTt,
                          displayExpr: 'name',
                          valueExpr: 'value',
                        }}>
              <Label text={translate('vienphi.ttlanthu.hinhthucthanhtoan')} />
            </SimpleItem>
          </GroupItem>
          <GroupItem cssClass="pt-0" colSpan={2} colCount={2} visible={formData.loaiGiaoDich === 2}>
              <SimpleItem dataField="soTienThuTamUng" editorType="dxNumberBox" editorOptions={{placeholder: 'Nhập só tiền tạm ứng ...', format: VienPhiDefaultNumberFormat}}>
                <Label text={translate('vienphi.ttlanthu.soTienTamUng')} />
              </SimpleItem>
            </GroupItem>
          <GroupItem cssClass="pt-0">
            <SimpleItem dataField="ghiChu" colSpan={2} editorType="dxTextBox" editorOptions={{placeholder: 'Ghi chú'}}>
              <Label text={translate('vienphi.ttlanthu.ghichu')} />
            </SimpleItem>
          </GroupItem>
          <GroupItem cssClass="pt-0">
            <div className="d-flex justify-content-center">
              <Button
                width={45}
                className="m-2"
                text="Lưu"
                type="default"
                stylingMode="contained"
                onClick={this.onBtnSaveClick(false)}
              />
              <Button
                width={70}
                className="m-2"
                text="Lưu và in"
                type="default"
                stylingMode="contained"
                onClick={this.onBtnSaveClick(true)}
              />
            </div>
          </GroupItem>
        </Form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ vienPhiThuTien }: IRootState) => ({
  dsQuyenBienLai: vienPhiThuTien.dsQuyenBienLai,
  formData: vienPhiThuTien.formDataTtLanThu,
  savingLanThu: vienPhiThuTien.savingLanThu,
  benhNhanSelected: vienPhiThuTien.benhNhanSelected,
  loadingBienLai: vienPhiThuTien.loadingBienLai,
  searchingBenhNhan: vienPhiThuTien.searchingBenhNhan,
  soTienTamUngConLai: vienPhiThuTien.thongTinVpTongHop.tamUngConLai
});

const mapDispatchToProps = {
  getQuyenBienLai,
  refreshFormLanThu,
  thuTamUng,
  thanhToanVienPhi
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VienPhiTtLanThu);
