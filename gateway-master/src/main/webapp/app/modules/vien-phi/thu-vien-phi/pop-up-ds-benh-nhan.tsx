import React, { Component } from 'react';
import { Popup } from 'devextreme-react/popup';
import { translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { getDsBenhNhan, selectBenhNhan, togglePopupDsbn } from 'app/modules/vien-phi/thu-vien-phi/thu-vien-phi.reducer';
import { connect } from 'react-redux';
import VienPhiGridDanhSachBenhNhan from 'app/modules/vien-phi/commons/grid-danh-sach-benh-nhan';
import VienPhiFormRequestDsBenhNhan from 'app/modules/vien-phi/commons/form-request-ds-benh-nhan';

export interface IVienPhiPopUpDsBenhNhanProps extends StateProps, DispatchProps {
}

class VienPhiPopUpDsBenhNhan extends Component<IVienPhiPopUpDsBenhNhanProps> {

  togglePopUp = (visible: boolean) => e => {
    this.props.togglePopupDsbn(visible);
  }

  onRowDbClick = e => {
    this.props.togglePopupDsbn(false);
    this.props.selectBenhNhan(e.data);
  }

  render() {
    return (
      <Popup
        visible={this.props.visible}
        onHiding={this.togglePopUp(false)}
        dragEnabled
        closeOnOutsideClick
        showTitle
        title={translate('vienphi.dsbenhnhan.tieude')}
        width={1200}
        height={600}
        className="vien-phi-pop-up"
      >
        <div className="d-flex align-items-stretch flex-column">
          <VienPhiFormRequestDsBenhNhan
            layout="horizontal"
            labelLocation="top"
            listField={['tuNgay', 'denNgay', 'loaiKcb', 'maKhoa', 'coBhyt', 'daThanhToan', 'daTamUng']}
            action={this.props.getDsBenhNhan}
            hideButtonDate
          />
          <div className="p-1 mt-2" style={{ height: '475px' }}>
            <VienPhiGridDanhSachBenhNhan dataSource={this.props.dataSource as any} onRowDblClick={this.onRowDbClick} />
          </div>
        </div>
      </Popup>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  visible: storeState.vienPhiThuTien.showPopUpDsBenhNhan,
  dataSource: storeState.vienPhiThuTien.dataSourceDsBenhNhan
});

const mapDispatchToProps = {
  togglePopupDsbn,
  getDsBenhNhan,
  selectBenhNhan
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VienPhiPopUpDsBenhNhan);
