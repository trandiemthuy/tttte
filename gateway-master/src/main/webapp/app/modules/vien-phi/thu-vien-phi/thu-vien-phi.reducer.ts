import axios from 'axios';
import { VienPhiServicesUrl } from 'app/modules/vien-phi/utils/vienphi-constant';
import {
  convertDateTimeToStringParam,
  convertDateToStringParam,
  getDvtt,
  initCustomStore,
  printReportGet
} from 'app/modules/vien-phi/utils/vienphi-function';
import moment from 'moment';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { translate } from 'react-jhipster';
import { listLoaiGiaoDich } from 'app/modules/vien-phi/models/loai-giao-dich.model';
import { IVienPhiQuyenBienLaiModel } from 'app/modules/vien-phi/models/quyen-bien-lai.model';
import { listHinhThucThanhToan } from 'app/modules/vien-phi/models/hinh-thuc-tt.model';
import { IVienPhiLanTamUngModel } from 'app/modules/vien-phi/models/lan-tam-ung.model';
import { defaultThongTinVpTongHop, IVienPhiThongTinVpTongHopModel } from 'app/modules/vien-phi/models/thong-tin-vp-tong-hop.model';
import { defaultVienPhiBenhNhanData, IVienPhiBenhNhanModel } from 'app/modules/vien-phi/models/benh-nhan.model';
import { IChiTietThanhToanModel } from 'app/modules/vien-phi/models/chi-tiet-thanh-toan.model';

export const ACTION_TYPE = {
  GET_DS_BENHNHAN: 'VienPhi/ThuTien/GetDsBenhNhan',
  TOGGLE_POPUP_DSBN: 'VienPhi/ThuTien/TogglePopupDsbn',
  SEARCH_BENH_NHAN: 'VienPhi/ThuTien/SearchBenhNhan',
  SELECT_BENH_NHAN: 'VienPhi/ThuTien/SelectBenhNhan',
  CREATE_BANG_KE: 'VienPhi/ThuTien/CreateBangKe',
  GET_QUYEN_BIEN_LAI: 'VienPhi/ThuTien/GetQuyenBienLai',
  RESET_FORMDATA_LANTHU: 'VienPhi/ThuTien/ResetFormDataLanThu',
  REFRESH_FORM_LANTHU: 'VienPhi/ThuTien/RefreshFormLanThu',
  THU_TAM_UNG: 'VienPhi/ThuTien/ThuTamUng',
  XOA_TAM_UNG: 'VienPhi/ThuTie/XoaTamUng',
  GET_DS_TAM_UNG: 'VienPhi/ThuTien/GetDsTamUng',
  GET_CT_CHUA_THANH_TOAN: 'VienPhi/ThuTien/GetChiTietChuaThanhToan',
  GET_TTVP_TH: 'VienPhi/ThuTien/GetThongTinVpTongHop',
  SELECT_CT_THANH_TOAN: 'VienPhi/ThuTien/SelectChiTietThanhToan',
  TINH_TIEN_THANH_TOAN: 'VienPhi/ThuTien/TinhTienThanhToan',
  THANH_TOAN: 'VienPhi/ThuTien/ThanhToan',
  HUY_THANH_TOAN: 'VienPhi/ThuTien/HuyThanhToan',
  GET_DS_LANTT: 'VienPhi/ThuTien/GetDSLanTt',
  IN_BANG_KE: 'VienPhi/ThuTien/InBangKe'
};

const defaultFormDataTtLanThu = {
  ngayThu: moment().toDate(),
  layNgayThuHienTai: true,
  loaiGiaoDich: listLoaiGiaoDich[0].value,
  soTienPhaiTt: 0,
  soTienMienGiam: 0,
  soTienThucThu: 0,
  soTienThoiLai: 0,
  soTienThuTamUng: 0,
  maQuyenBienLai: null,
  soBienLai: null,
  khongDungBienLai: false,
  hinhThucTt: listHinhThucThanhToan[0].value,
  ghiChu: null
};

const initialState = {
  dataSourceDsBenhNhan: initCustomStore(),
  showPopUpDsBenhNhan: false,
  searchingBenhNhan: false,
  creatingBangKe: false,
  benhNhanSelected: defaultVienPhiBenhNhanData,
  dsQuyenBienLai: [] as Array<IVienPhiQuyenBienLaiModel>,
  loadingBienLai: false,
  formDataTtLanThu: { ...defaultFormDataTtLanThu },
  savingLanThu: false,
  dataSourceTamUng: initCustomStore(),
  thongTinVpTongHop: { ...defaultThongTinVpTongHop },
  loadingTtVpTh: false,
  dataSourceChuaThanhToan: initCustomStore(),
  chiTietThanhToanSelected: [] as Array<IChiTietThanhToanModel>,
  dsLanThanhToan: initCustomStore()
};

export type VienPhiThuTienState = Readonly<typeof initialState>;

// Reducer

// eslint-disable-next-line complexity
export default (state: VienPhiThuTienState = initialState, action): VienPhiThuTienState => {
  switch (action.type) {
    case ACTION_TYPE.TOGGLE_POPUP_DSBN:
      return {
        ...state,
        showPopUpDsBenhNhan: action.visible
      };
    case REQUEST(ACTION_TYPE.SEARCH_BENH_NHAN):
      return {
        ...state,
        searchingBenhNhan: true
      };
    case SUCCESS(ACTION_TYPE.SEARCH_BENH_NHAN):
      if (action.payload.data === null || action.payload.data.maBn === undefined) {
        notify(translate('vienphi.thutien.khongtimthaybn'), 'error');
      }
      return {
        ...state,
        searchingBenhNhan: false
      };
    case FAILURE(ACTION_TYPE.SEARCH_BENH_NHAN):
      return {
        ...state,
        searchingBenhNhan: false
      };
    case ACTION_TYPE.SELECT_BENH_NHAN:
      return {
        ...state,
        benhNhanSelected: action.benhNhanSelected,
        thongTinVpTongHop: { ...defaultThongTinVpTongHop }
      };
    case REQUEST(ACTION_TYPE.GET_QUYEN_BIEN_LAI):
      return {
        ...state,
        loadingBienLai: true
      };
    case FAILURE(ACTION_TYPE.GET_QUYEN_BIEN_LAI):
      return {
        ...state,
        loadingBienLai: false,
        dsQuyenBienLai: [] as Array<IVienPhiQuyenBienLaiModel>
      };
    case SUCCESS(ACTION_TYPE.GET_QUYEN_BIEN_LAI):
      return {
        ...state,
        loadingBienLai: false,
        dsQuyenBienLai: action.payload.data,
        formDataTtLanThu: {
          ...state.formDataTtLanThu,
          maQuyenBienLai: action.payload.data.length > 0 ? action.payload.data[0].maQuyenBienLai : ''
        }
      };
    case ACTION_TYPE.RESET_FORMDATA_LANTHU:
      return {
        ...state,
        formDataTtLanThu: {
          ...defaultFormDataTtLanThu,
          loaiGiaoDich: action.loaiGiaoDich
        }
      };
    case ACTION_TYPE.REFRESH_FORM_LANTHU:
      return {
        ...state,
        formDataTtLanThu: {
          ...state.formDataTtLanThu
        }
      };
    case REQUEST(ACTION_TYPE.THANH_TOAN):
    case REQUEST(ACTION_TYPE.THU_TAM_UNG):
      return {
        ...state,
        savingLanThu: true
      };
    case FAILURE(ACTION_TYPE.THANH_TOAN):
    case FAILURE(ACTION_TYPE.THU_TAM_UNG):
      notify(translate('vienphi.thutien.thutienkhongthanhcong'), 'error');
      return {
        ...state,
        savingLanThu: false
      };
    case SUCCESS(ACTION_TYPE.THANH_TOAN):
    case SUCCESS(ACTION_TYPE.THU_TAM_UNG):
      notify(translate('vienphi.thutien.thutienthanhcong'), 'success');
      return {
        ...state,
        savingLanThu: false
      };
    case ACTION_TYPE.GET_DS_TAM_UNG:
      return {
        ...state,
        dataSourceTamUng: action.dataSource
      };
    case SUCCESS(ACTION_TYPE.XOA_TAM_UNG):
      if (action.payload.status === 204) {
        notify(translate('vienphi.gridlantamung.xoathanhcong'), 'success');
      }
      return {
        ...state
      };
    case FAILURE(ACTION_TYPE.XOA_TAM_UNG):
      if (action.payload.response.status === 409) {
        notify(action.payload.response.data.message, 'error');
      } else {
        notify('Đã có lỗi xảy ra !', 'error');
      }
      return {
        ...state
      };
    case ACTION_TYPE.GET_DS_BENHNHAN:
      return {
        ...state,
        dataSourceDsBenhNhan: action.dataSource
      };
    case REQUEST(ACTION_TYPE.GET_TTVP_TH):
      return {
        ...state,
        thongTinVpTongHop: { ...defaultThongTinVpTongHop },
        loadingTtVpTh: true
      };
    case FAILURE(ACTION_TYPE.GET_TTVP_TH):
      return {
        ...state,
        loadingTtVpTh: false
      };
    case SUCCESS(ACTION_TYPE.GET_TTVP_TH):
      return {
        ...state,
        thongTinVpTongHop: action.payload.data,
        loadingTtVpTh: false
      };
    case ACTION_TYPE.GET_CT_CHUA_THANH_TOAN:
      return {
        ...state,
        dataSourceChuaThanhToan: action.dataSource
      };
    case ACTION_TYPE.SELECT_CT_THANH_TOAN:
      return {
        ...state,
        chiTietThanhToanSelected: action.chiTietThanhToanSelected
      };
    case ACTION_TYPE.TINH_TIEN_THANH_TOAN:
      return {
        ...state,
        formDataTtLanThu: action.formDataTtLanThu
      };
    case ACTION_TYPE.GET_DS_LANTT:
      return {
        ...state,
        dsLanThanhToan: action.dataSource
      };
    case SUCCESS(ACTION_TYPE.HUY_THANH_TOAN):
      if (action.payload.status === 204) {
        notify(translate('vienphi.gridlantt.xoathanhcong'), 'success');
      }
      return {
        ...state
      };
    case FAILURE(ACTION_TYPE.HUY_THANH_TOAN):
      if (action.payload.response.status === 409) {
        notify(action.payload.response.data.message, 'error');
      } else {
        notify('Đã có lỗi xảy ra !', 'error');
      }
      return {
        ...state
      };
    case REQUEST(ACTION_TYPE.CREATE_BANG_KE):
      return {
        ...state,
        creatingBangKe: true
      };
    case FAILURE(ACTION_TYPE.CREATE_BANG_KE):
      notify('Có lỗi xảy ra', 'error');
      return {
        ...state,
        creatingBangKe: false
      };
    case SUCCESS(ACTION_TYPE.CREATE_BANG_KE):
      return {
        ...state,
        creatingBangKe: false
      };
    default:
      return state;
  }
};

export const togglePopupDsbn = (visible: boolean) => async (dispatch, getState) => {
  await dispatch({
    type: ACTION_TYPE.TOGGLE_POPUP_DSBN,
    visible
  });
};

export const resetFormDataLanThu = () => async (dispatch, getState) => {
  const { loaiGiaoDich } = getState().vienPhiThuTien.formDataTtLanThu;
  await dispatch({
    type: ACTION_TYPE.RESET_FORMDATA_LANTHU,
    loaiGiaoDich
  });
};

export const refreshFormLanThu = () => async (dispatch, getState) => {
  await dispatch({
    type: ACTION_TYPE.REFRESH_FORM_LANTHU
  });
};

export const getQuyenBienLai = () => async (dispatch, getState) => {
  const url = `${VienPhiServicesUrl}/api/vien-phi/quyen-bien-lai/theo-nghiep-vu`;
  const { benhNhanSelected, formDataTtLanThu } = getState().vienPhiThuTien;
  if (benhNhanSelected.maBn === null) return;
  const nhanVien = -1; // getState().authentication.account.id;
  const params = {
    dvtt: getDvtt(),
    nhanVien,
    ngoaiTru: benhNhanSelected.loaiKcb === 1 ? 1 : 0,
    tamUng: formDataTtLanThu.loaiGiaoDich === 2 ? 1 : 0,
    bhyt: benhNhanSelected.coBhyt
  };
  await dispatch({
    type: ACTION_TYPE.GET_QUYEN_BIEN_LAI,
    payload: axios.get<Array<IVienPhiQuyenBienLaiModel>>(url, { params })
  });
};

export const getDsTamUng = () => async (dispatch, getState) => {
  const { benhNhanSelected } = getState().vienPhiThuTien;
  const dataSource =
    benhNhanSelected.maBn === null
      ? initCustomStore()
      : new CustomStore({
          key: 'idLanTamUng',
          loadMode: 'raw',
          async load() {
            const url = `${VienPhiServicesUrl}/api/vien-phi/lan-tam-ung/get-list`;
            const params = {
              soVaoVien: benhNhanSelected.soVaoVien,
              soVaoVienDt: benhNhanSelected.soVaoVienDt,
              loaiKcb: benhNhanSelected.loaiKcb,
              dvtt: getDvtt()
            };
            const response = await axios.get<Array<IVienPhiLanTamUngModel>>(url, { params });
            return response.data;
          }
        });
  await dispatch({
    type: ACTION_TYPE.GET_DS_TAM_UNG,
    dataSource
  });
};

export const getDsLanThanhToan = () => async (dispatch, getState) => {
  const { benhNhanSelected } = getState().vienPhiThuTien;
  const dataSource =
    benhNhanSelected.maBn === null
      ? initCustomStore()
      : new CustomStore({
          key: 'idLanTt',
          loadMode: 'raw',
          async load() {
            const url = `${VienPhiServicesUrl}/api/vien-phi/ds-lan-thanh-toan`;
            const params = {
              soVaoVien: benhNhanSelected.soVaoVien,
              soVaoVienDt: benhNhanSelected.soVaoVienDt,
              loaiKcb: benhNhanSelected.loaiKcb,
              dvtt: getDvtt()
            };
            const response = await axios.get<Array<any>>(url, { params });
            return response.data;
          }
        });
  await dispatch({
    type: ACTION_TYPE.GET_DS_LANTT,
    dataSource
  });
};

export const getCtChuaThanhToan = () => async (dispatch, getState) => {
  const { benhNhanSelected } = getState().vienPhiThuTien;
  const dataSource =
    benhNhanSelected.maBn === null
      ? initCustomStore()
      : new CustomStore({
          key: 'soPhieuMaDv',
          loadMode: 'raw',
          async load() {
            const url = `${VienPhiServicesUrl}/api/vien-phi/chua-thanh-toan`;
            const params = {
              soVaoVien: benhNhanSelected.soVaoVien,
              soVaoVienDt: benhNhanSelected.soVaoVienDt,
              loaiKcb: benhNhanSelected.loaiKcb,
              dvtt: getDvtt()
            };
            const response = await axios.get<Array<IChiTietThanhToanModel>>(url, { params });
            return response.data;
          }
        });
  await dispatch({
    type: ACTION_TYPE.GET_CT_CHUA_THANH_TOAN,
    dataSource
  });
};

export const getThongTinVienPhiTongHop = () => async (dispatch, getState) => {
  const { benhNhanSelected } = getState().vienPhiThuTien;
  if (benhNhanSelected.maBn !== null) {
    const { soVaoVien, soVaoVienDt, dvtt, loaiKcb } = benhNhanSelected;
    const url = `${VienPhiServicesUrl}/api/vien-phi/thong-tin-tong-hop`;
    const params = { soVaoVien, soVaoVienDt, dvtt, loaiKcb };
    await dispatch({
      type: ACTION_TYPE.GET_TTVP_TH,
      payload: axios.get<IVienPhiThongTinVpTongHopModel>(url, { params })
    });
  } else {
    await dispatch({
      type: ACTION_TYPE.GET_TTVP_TH,
      payload: new Promise((resolve, reject) => {
        resolve({ data: { ...defaultThongTinVpTongHop } });
      })
    });
  }
};

export const createBangKe = () => async (dispatch, getState) => {
  const { benhNhanSelected } = getState().vienPhiThuTien;
  const { soVaoVien, soVaoVienDt, dvtt, loaiKcb } = benhNhanSelected;
  const url = `${VienPhiServicesUrl}/api/bang-ke/`;
  const data = { soVaoVien, soVaoVienDt, dvtt, loaiKcb };
  await dispatch({
    type: ACTION_TYPE.CREATE_BANG_KE,
    payload: axios.post(url, data)
  });
};

export const selectBenhNhan = (benhNhanSelected: IVienPhiBenhNhanModel) => async (dispatch, getState) => {
  await dispatch({
    type: ACTION_TYPE.SELECT_BENH_NHAN,
    benhNhanSelected
  });
  await dispatch(resetFormDataLanThu()).then(async () => {
    dispatch(getQuyenBienLai());
    dispatch(getDsTamUng());
    dispatch(getDsLanThanhToan());
    if (benhNhanSelected.maBn !== null) {
      await dispatch(createBangKe());
      await dispatch(getThongTinVienPhiTongHop());
      await dispatch(getCtChuaThanhToan());
    } else {
      dispatch(getThongTinVienPhiTongHop());
      dispatch(getCtChuaThanhToan());
    }
  });
};

export const searchBenhNhan = (formData: any) => async (dispatch, getState) => {
  const url = `${VienPhiServicesUrl}/api/danh-sach-benh-nhan/tim-kiem`;
  const params = {
    tuNgay: convertDateToStringParam(formData.tuNgay),
    denNgay: convertDateToStringParam(formData.denNgay),
    loaiKcb: formData.loaiKcb,
    dvtt: getDvtt(),
    maBn: formData.maBn
  };
  const result = await dispatch({
    type: ACTION_TYPE.SEARCH_BENH_NHAN,
    payload: axios.get<IVienPhiBenhNhanModel>(url, { params })
  });
  if (result.value.data === null || result.value.data.maBn === undefined) {
    await dispatch(selectBenhNhan(defaultVienPhiBenhNhanData));
  } else {
    await dispatch(selectBenhNhan(result.value.data));
  }
};

export const inPhieuThu = (loaiPhieu: number) => {
  notify('Printed !');
};

export const thuTamUng = (print: boolean) => async (dispatch, getState) => {
  const { benhNhanSelected, formDataTtLanThu } = getState().vienPhiThuTien;
  const { id, lastName } = getState().authentication.account;
  const url = `${VienPhiServicesUrl}/api/vien-phi/lan-tam-ung/`;
  const data = {
    soVaoVien: benhNhanSelected.soVaoVien,
    soVaoVienDt: benhNhanSelected.soVaoVienDt,
    loaiKcb: benhNhanSelected.loaiKcb,
    dvtt: getDvtt(),
    maBn: benhNhanSelected.maBn,
    maNvTao: 1,
    tenNvTao: lastName,
    ngayThu: convertDateTimeToStringParam(formDataTtLanThu.layNgayThuHienTai ? moment().toDate() : formDataTtLanThu.ngayThu),
    maPhongBanThu: benhNhanSelected.maKhoa,
    soTienTamUng: formDataTtLanThu.soTienThuTamUng,
    soTienBnTra: formDataTtLanThu.soTienThuTamUng,
    soTienThoiLai: 0,
    maQuyenBienLai: formDataTtLanThu.khongDungBienLai ? null : formDataTtLanThu.maQuyenBienLai,
    soBienLai: formDataTtLanThu.khongDungBienLai ? null : formDataTtLanThu.soBienLai,
    ghiChu: formDataTtLanThu.ghiChu
  };

  const result = await dispatch({
    type: ACTION_TYPE.THU_TAM_UNG,
    payload: axios.post(url, data)
  });

  await dispatch(selectBenhNhan(benhNhanSelected));
  if (print) {
    inPhieuThu(0);
  }
};

export const xoaTamUng = sttLanTamUng => async (dispatch, getState) => {
  const { benhNhanSelected } = getState().vienPhiThuTien;
  const url = `${VienPhiServicesUrl}/api/vien-phi/lan-tam-ung`;
  const params = {
    soVaoVien: benhNhanSelected.soVaoVien,
    soVaoVienDt: benhNhanSelected.soVaoVienDt,
    loaiKcb: benhNhanSelected.loaiKcb,
    dvtt: getDvtt(),
    sttLanTamUng
  };
  await dispatch({
    type: ACTION_TYPE.XOA_TAM_UNG,
    payload: axios.delete(url, { params })
  });
  await dispatch(selectBenhNhan(benhNhanSelected));
};

export const thanhToanVienPhi = (print: boolean) => async (dispatch, getState) => {
  const { benhNhanSelected, formDataTtLanThu, chiTietThanhToanSelected, thongTinVpTongHop } = getState().vienPhiThuTien;
  const { id, lastName } = getState().authentication.account;
  const url = `${VienPhiServicesUrl}/api/vien-phi/thanh-toan`;
  const data = {
    soVaoVien: benhNhanSelected.soVaoVien,
    soVaoVienDt: benhNhanSelected.soVaoVienDt,
    loaiKcb: benhNhanSelected.loaiKcb,
    dvtt: getDvtt(),
    maBn: benhNhanSelected.maBn,
    maNvTao: 1,
    tenNvTao: lastName,
    maPhongBanTt: benhNhanSelected.maKhoa,
    ngayThu: convertDateTimeToStringParam(formDataTtLanThu.layNgayThuHienTai ? moment().toDate() : formDataTtLanThu.ngayThu),
    soTienPhaiTt: formDataTtLanThu.soTienPhaiTt,
    soTienMienGiam: formDataTtLanThu.soTienMienGiam,
    soTienTamUng: thongTinVpTongHop.tamUngConLai,
    soTienThucThu: formDataTtLanThu.soTienThucThu,
    soTienThoiLai: formDataTtLanThu.soTienThoiLai,
    maQuyenBienLai: formDataTtLanThu.khongDungBienLai ? null : formDataTtLanThu.maQuyenBienLai,
    soBienLai: formDataTtLanThu.khongDungBienLai ? null : formDataTtLanThu.soBienLai,
    hinhThucTt: formDataTtLanThu.hinhThucTt,
    ghiChu: formDataTtLanThu.ghiChu,
    dsSoPhieuMaDv: chiTietThanhToanSelected.map(e => e.soPhieuMaDv)
  };
  await dispatch({
    type: ACTION_TYPE.THANH_TOAN,
    payload: axios.post(url, data)
  });
  await dispatch(selectBenhNhan(benhNhanSelected));
  if (print) {
    inPhieuThu(1);
  }
};

export const huyThanhToan = sttLanTt => async (dispatch, getState) => {
  const { benhNhanSelected } = getState().vienPhiThuTien;
  const url = `${VienPhiServicesUrl}/api/vien-phi/lan-thanh-toan`;
  const params = {
    soVaoVien: benhNhanSelected.soVaoVien,
    soVaoVienDt: benhNhanSelected.soVaoVienDt,
    loaiKcb: benhNhanSelected.loaiKcb,
    dvtt: getDvtt(),
    sttLanTt
  };
  await dispatch({
    type: ACTION_TYPE.HUY_THANH_TOAN,
    payload: axios.delete(url, { params })
  });
  await dispatch(selectBenhNhan(benhNhanSelected));
};

export const getDsBenhNhan = params => async (dispatch, getState) => {
  const dataSource = new CustomStore({
    loadMode: 'raw',
    key: ['soVaoVien', 'soVaoVienDt', 'loaiKcb', 'dvtt'],
    async load() {
      const url = `${VienPhiServicesUrl}/api/danh-sach-benh-nhan/thu-tien`;
      const response = await axios.get<Array<IVienPhiLanTamUngModel>>(url, { params });
      return response.data;
    }
  });
  await dispatch({
    type: ACTION_TYPE.GET_DS_BENHNHAN,
    dataSource
  });
};

export const tinhTienThanhToan = () => async (dispatch, getState) => {
  const { formDataTtLanThu, chiTietThanhToanSelected, thongTinVpTongHop } = getState().vienPhiThuTien;
  const { tamUngConLai } = thongTinVpTongHop;
  let soTienThucThu, soTienThoiLai;
  const soTienPhaiTt = chiTietThanhToanSelected.map(e => e.phaiTt).reduce((a, b) => a + b, 0);
  if (tamUngConLai - soTienPhaiTt > 0) {
    soTienThucThu = 0;
    soTienThoiLai = tamUngConLai - soTienPhaiTt;
  } else {
    soTienThucThu = soTienPhaiTt - tamUngConLai;
    soTienThoiLai = 0;
  }
  await dispatch({
    type: ACTION_TYPE.TINH_TIEN_THANH_TOAN,
    formDataTtLanThu: {
      ...formDataTtLanThu,
      soTienThucThu,
      soTienThoiLai,
      soTienPhaiTt
    }
  });
};

export const selectChiTietThanhToan = chiTietThanhToanSelected => async (dispatch, getState) => {
  await dispatch({
    type: ACTION_TYPE.SELECT_CT_THANH_TOAN,
    chiTietThanhToanSelected
  });
  await dispatch(tinhTienThanhToan());
};

export const inBangKe = () => async (dispatch, getState) => {
  const url = `${VienPhiServicesUrl}/api/bang-ke/print`;
  const { benhNhanSelected } = getState().vienPhiThuTien;
  const params = {
    soVaoVien: benhNhanSelected.soVaoVien,
    soVaoVienDt: benhNhanSelected.soVaoVienDt,
    loaiKcb: benhNhanSelected.loaiKcb,
    dvtt: getDvtt(),
    kieuFile: -1
  };
  await dispatch({
    type: ACTION_TYPE.IN_BANG_KE,
    payload: printReportGet(url, params)
  });
};
