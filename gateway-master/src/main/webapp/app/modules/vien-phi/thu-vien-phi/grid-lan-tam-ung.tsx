import React from 'react';
import DataGrid, { Column } from 'devextreme-react/data-grid';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { Button } from 'devextreme-react/button';
import { confirm } from 'devextreme/ui/dialog';
import { translate } from 'react-jhipster';
import { xoaTamUng } from 'app/modules/vien-phi/thu-vien-phi/thu-vien-phi.reducer';
import {
  cellCheckBoxRender,
  cellDateTimeRender,
  cellSttRender
} from 'app/modules/vien-phi/commons/data-grid-components';


export interface IGridVienPhiLanTamUng extends StateProps, DispatchProps {
}

export class GridVienPhiLanTamUng extends React.Component<IGridVienPhiLanTamUng> {

  onBtnDeleteClick = cellData => e => {
    const result = confirm(translate('vienphi.gridlantamung.xacnhanxoa'), translate('vienphi.gridlantamung.xacnhan'));
    result.then((dialogResult) => {
      if (dialogResult) {
        this.props.xoaTamUng(cellData.data.sttLanTamUng);
      }
    });
  }

  private cellActionRender = cellData => {
    return (
      <div className="d-flex justify-content-center">
        <Button type="default" stylingMode="outlined" className="ml-2 mr-2" icon="print" />
        <Button type="danger" stylingMode="outlined" onClick={this.onBtnDeleteClick(cellData)} className="ml-2 mr-2" icon="clear" />
      </div>
    );
  }

  render() {
    return (
      <DataGrid
        id="grid-lan-tam-ung"
        dataSource={this.props.dataSource as any}
        allowColumnResizing
        columnResizingMode={'widget'}
        // columnAutoWidth
        showBorders
        showRowLines
        // rowAlternationEnabled
        hoverStateEnabled
        height={'100%'}
        className="m-1"
      >
        <Column caption="STT" width={30} dataType="number" cellRender={cellSttRender} />
        <Column caption="Ngày thu" dataField="ngayThu" dataType="datetime" format="dd/MM/yyyy HH:mm:ss" alignment="center" cellRender={cellDateTimeRender} />
        <Column caption="Nhân viên" dataField="tenNvTao" />
        <Column caption="Số tiền" dataField="soTienTamUng" dataType="number" format="#,##0.00" />
        <Column caption="Đã sử dụng" dataField="ttSuDung" alignment="center" cellRender={cellCheckBoxRender}/>
        <Column caption="" alignment="center" cellRender={this.cellActionRender} />
      </DataGrid>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  dataSource: storeState.vienPhiThuTien.dataSourceTamUng
});

const mapDispatchToProps = {
  xoaTamUng
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(GridVienPhiLanTamUng);

