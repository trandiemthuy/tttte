import React from 'react';
import DataGrid, { Column } from 'devextreme-react/data-grid';
import { IRootState } from 'app/shared/reducers';
import { huyThanhToan } from 'app/modules/vien-phi/thu-vien-phi/thu-vien-phi.reducer';
import { connect } from 'react-redux';
import { cellDateTimeRender, cellSttRender } from 'app/modules/vien-phi/commons/data-grid-components';
import { VienPhiDefaultNumberFormat, VienPhiDefaultDatetimeFormat } from 'app/modules/vien-phi/utils/vienphi-constant';
import { Button } from 'devextreme-react/button';
import { confirm } from 'devextreme/ui/dialog';
import { translate } from 'react-jhipster';
import VienPhiPopUpChiTietLanTt from 'app/modules/vien-phi/thu-vien-phi/pop-up-chi-tiet-lan-thanh-toan';

export interface IGridVienPhiLanThanhToanProps extends StateProps, DispatchProps {
}

export interface IGridVienPhiLanThanhToanState {
  rowDbClickedData: any;
}

export class GridVienPhiLanThanhToan extends React.Component<IGridVienPhiLanThanhToanProps, IGridVienPhiLanThanhToanState> {

  constructor(props: IGridVienPhiLanThanhToanProps, state: IGridVienPhiLanThanhToanState) {
    super(props, state);
    this.state = {
      rowDbClickedData: null
    }
  }

  onBtnDeleteClick = cellData => e => {
    const result = confirm(translate('vienphi.gridlantt.xacnhanxoa'), translate('vienphi.gridlantt.xacnhan'));
    result.then((dialogResult) => {
      if (dialogResult) {
        this.props.huyThanhToan(cellData.data.sttLanTt);
      }
    });
  }

  private cellActionRender = cellData => {
    return (
      <div className="d-flex justify-content-center">
        <Button type="default" stylingMode="outlined" className="ml-2 mr-2" icon="print" />
        <Button type="danger" stylingMode="outlined" onClick={this.onBtnDeleteClick(cellData)} className="ml-2 mr-2" icon="clear" />
      </div>
    );
  }

  onRowDbClick = e => {
    this.setState({
      rowDbClickedData: e.data
    })
  }

  hidePopup = () => {
    this.setState({
      rowDbClickedData: null
    })
  }

  render(){
    return (
      <React.Fragment>
        <VienPhiPopUpChiTietLanTt
          data={this.state.rowDbClickedData}
          onHiding={this.hidePopup}
        />
        <DataGrid
          id="grid-lan-thanh-toan"
          dataSource={this.props.dataSource as any}
          allowColumnResizing
          columnResizingMode={'widget'}
          // columnAutoWidth
          showBorders
          showRowLines
          // rowAlternationEnabled
          hoverStateEnabled
          height="100%"
          className="m-1"
          onRowDblClick={this.onRowDbClick}
        >
          <Column caption="STT" width="30" dataType="number" cellRender={cellSttRender} />
          <Column caption="Ngày thu" dataField="ngayThu" dataType="datetime" format={VienPhiDefaultDatetimeFormat} alignment="center" cellRender={cellDateTimeRender} />
          <Column caption="Nhân viên" dataField="tenNvTao" dataType="string" />
          <Column caption="Số tiền" dataField="soTienThucThu" dataType="number" format={VienPhiDefaultNumberFormat} />
          <Column caption="Ghi chú" dataField="ghiChu" dataType="string" />
          <Column caption="" alignment="center" cellRender={this.cellActionRender} />
        </DataGrid>
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ vienPhiThuTien}: IRootState) => ({
  dataSource: vienPhiThuTien.dsLanThanhToan
});

const mapDispatchToProps = {
  huyThanhToan
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(GridVienPhiLanThanhToan);
