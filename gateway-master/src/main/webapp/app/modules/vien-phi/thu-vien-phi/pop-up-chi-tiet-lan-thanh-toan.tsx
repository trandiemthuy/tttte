import React from 'react';
import { translate } from 'react-jhipster';
import { Popup } from 'devextreme-react/popup';
import Box, { Item } from 'devextreme-react/box';
import Form, { ButtonItem, GroupItem, Label, SimpleItem } from 'devextreme-react/form';
import { VienPhiDefaultDatetimeFormat, VienPhiDefaultNumberFormat } from 'app/modules/vien-phi/utils/vienphi-constant';
import ArrayStore from "devextreme/data/array_store";
import { listHinhThucThanhToan } from "app/modules/vien-phi/models/hinh-thuc-tt.model";
import GridVienPhiChiTietThanhToan from "app/modules/vien-phi/thu-vien-phi/grid-chi-tiet-thanh-toan";

export interface IVienPhiPopUpChiTietLanTtProps {
  onHiding: any;
  data: any;
}

class VienPhiPopUpChiTietLanTt extends React.Component<IVienPhiPopUpChiTietLanTtProps> {

  private dataSourceHinhThucTt = new ArrayStore({
    data: listHinhThucThanhToan,
    key: 'value'
  });

  /* eslint no-console: off */
  render() {
    const formData = { ...this.props.data };
    const visible = !!this.props.data;
    return (
      <Popup
        visible={visible}
        onHiding={this.props.onHiding}
        dragEnabled
        closeOnOutsideClick
        showTitle
        title={translate('vienphi.gridlantt.chitietlantt')}
        width={950}
        height={420}
      >
        <div>
        {
          !visible ? <React.Fragment/> :
          <Box>
            <Item ratio={8}>
              <div className="p-2">
                <GridVienPhiChiTietThanhToan
                  id="grid-chua-thanh-toan"
                  dataSource = {this.props.data.listVienPhiChiTietLanTt}
                  height = {350}
                  disabledSelect
                />
              </div>
            </Item>
            <Item ratio={4}>
              <Form id="form-chi-tiet-lantt"
                    showColonAfterLabel
                    formData={formData}
                    className="p-2"
                    readOnly
              >
                <SimpleItem dataField="ngayThu" cssClass="pt-0" editorType="dxDateBox"
                            editorOptions={{ displayFormat: VienPhiDefaultDatetimeFormat }}>
                  <Label text={translate('vienphi.gridlantt.ngaytt')}/>
                </SimpleItem>
                <SimpleItem dataField="sttLanTt" cssClass="pt-0" editorType="dxTextBox">
                  <Label text={translate('vienphi.gridlantt.lantt')}/>
                </SimpleItem>
                <SimpleItem dataField="ghiChu" cssClass="pt-0" editorType="dxTextBox">
                  <Label text={translate('vienphi.gridlantt.ghichu')}/>
                </SimpleItem>
                <SimpleItem dataField="soTienPhaiTt" cssClass="pt-0" editorType="dxNumberBox"
                            editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: {style: 'color: red; font-weight: bold'} }}>
                  <Label text={translate('vienphi.gridlantt.soTienPhaiTt')}/>
                </SimpleItem>
                <SimpleItem dataField="soTienTamUng" cssClass="pt-0" editorType="dxNumberBox"
                            editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: {style: 'color: red; font-weight: bold'} }}>
                  <Label text={translate('vienphi.gridlantt.soTienDaTamUng')}/>
                </SimpleItem>
                <SimpleItem dataField="soTienMienGiam" cssClass="pt-0" editorType="dxNumberBox"
                            editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: {style: 'color: red; font-weight: bold'} }}>
                  <Label text={translate('vienphi.gridlantt.soTienMienGiam')}/>
                </SimpleItem>
                <SimpleItem dataField="soTienThucThu" cssClass="pt-0" editorType="dxNumberBox"
                            editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: {style: 'color: blue; font-weight: bold'} }}>
                  <Label text={translate('vienphi.gridlantt.soTienThucThu')}/>
                </SimpleItem>
                <SimpleItem dataField="soTienThoiLai" cssClass="pt-0" editorType="dxNumberBox"
                            editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: {style: 'color: black; font-weight: bold'} }}>
                  <Label text={translate('vienphi.gridlantt.soTienThoiLai')}/>
                </SimpleItem>
                <SimpleItem cssClass="pt-0 d-none" dataField="hinhThucTt" editorType="dxSelectBox"
                            editorOptions={{
                              placeholder: 'Hình thức thanh toán',
                              dataSource: this.dataSourceHinhThucTt,
                              displayExpr: 'name',
                              valueExpr: 'value',
                            }}>
                  <Label text={translate('vienphi.gridlantt.hinhthucthanhtoan')}/>
                </SimpleItem>
                <SimpleItem cssClass="pt-0" editorType="dxTextBox"
                            editorOptions={{ value: formData.vienPhiQuyenBienLai ? formData.vienPhiQuyenBienLai.tenQuyenBienLai : '' }}>
                  <Label text={translate('vienphi.gridlantt.quyenbienlai')}/>
                </SimpleItem>
                <SimpleItem dataField="soBienLai" cssClass="pt-0" editorType="dxNumberBox">
                  <Label text={translate('vienphi.gridlantt.sobienlai')}/>
                </SimpleItem>
                <SimpleItem cssClass="pt-0" editorType="dxCheckBox" editorOptions={{ value: !formData.vienPhiQuyenBienLai }}>
                  <Label text={translate('vienphi.gridlantt.khongdungbienlai')}/>
                </SimpleItem>
              </Form>
            </Item>
          </Box>
        }
        </div>
      </Popup>
    );
  }

}

export default VienPhiPopUpChiTietLanTt;
