import React from 'react';
import DataGrid, { Column, Selection, TotalItem, Summary, Grouping } from 'devextreme-react/data-grid';
import { nvl } from "app/modules/vien-phi/utils/vienphi-function";

export interface IGridVienPhiChiTietThanhToanProps {
  dataSource: any;
  id?: string;
  height?: any;
  onSelectionChanged?: any;
  selectAllAfterLoad?: boolean;
  disabledSelect: boolean;
}

class GridVienPhiChiTietThanhToan extends React.Component<IGridVienPhiChiTietThanhToanProps> {

  private grid;

  onRowClick = e => {
    if (e.rowType === 'data' && !this.props.disabledSelect) {
      const grid = e.component;
      const selectedRowKeys = grid.getSelectedRowKeys();
      if (grid.isRowSelected(e.key)) {
        grid.selectRows([...selectedRowKeys].filter(el => el !== e.key));
      }
      else {
        grid.selectRows([...selectedRowKeys, e.key]);
      }
    }
  }

  private groupCellRender = cellInfo => (
    <strong>{cellInfo.text}</strong>
  );

  private onInitialized = e => {
    this.grid = e.component;
  }

  tenNhomSortingMethod = (value1, value2): number => {
    const sttNhom1 = Number(value1.split('.')[0]);
    const sttNhom2 = Number(value2.split('.')[0]);
    return sttNhom1 - sttNhom2;
  }

  componentDidUpdate(prevProps: Readonly<IGridVienPhiChiTietThanhToanProps>, prevState: Readonly<{}>, snapshot?: any) {
    if (this.props.selectAllAfterLoad) {
      this.grid.deselectAll().then(() => this.grid.selectAll());
    }
  }

  /* eslint no-console: off */
  render() {
    const { dataSource } = this.props;
    return (
      <DataGrid
        id={this.props.id}
        dataSource={dataSource}
        allowColumnReordering={true}
        // columnAutoWidth
        allowColumnResizing
        columnResizingMode={'widget'}
        showBorders
        showRowLines
        hoverStateEnabled
        className="m-1"
        height={nvl(this.props.height, '100%')}
        onSelectionChanged={this.props.onSelectionChanged}
        onRowClick={this.onRowClick}
        onInitialized={this.onInitialized}
        paging={{ enabled: false }}
      >
        <Selection mode={this.props.disabledSelect ? 'none' : 'multiple'} showCheckBoxesMode="always" />
        <Grouping autoExpandAll />
        <Column groupCellRender={this.groupCellRender} sortingMethod={this.tenNhomSortingMethod} dataField="tenNhomBangKe" dataType="string" groupIndex={0} />
        <Column caption="Nội dung" dataField="noiDung" width="30%" dataType="string" />
        <Column caption="Số lượng" dataField="soLuong"  dataType="number" />
        <Column caption="Đơn giá" dataField="donGia"  dataType="number" format="#,##0.00" />
        <Column caption="Thành tiền" dataField="thanhTien"  dataType="number" format="#,##0.00" />
        <Column caption="Quỹ BHYT" dataField="tbhtt"  dataType="number" format="#,##0.00" />
        <Column caption="Số tiền cần thu" dataField="phaiTt"  dataType="number" format="#,##0.00" />
        <Summary>
          <TotalItem displayFormat="{0}" column="thanhTien" summaryType="sum" valueFormat="#,##0.00" />
          <TotalItem displayFormat="{0}" column="tbhtt" summaryType="sum" valueFormat="#,##0.00" />
          <TotalItem displayFormat="{0}" column="phaiTt" summaryType="sum" valueFormat="#,##0.00" />
        </Summary>
      </DataGrid>
    );
  }
}

export default GridVienPhiChiTietThanhToan;
