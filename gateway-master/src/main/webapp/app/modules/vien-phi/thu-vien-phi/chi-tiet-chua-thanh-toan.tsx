import React from 'react';
import { Translate } from 'react-jhipster';
import GridVienPhiChiTietThanhToan from 'app/modules/vien-phi/thu-vien-phi/grid-chi-tiet-thanh-toan';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { selectChiTietThanhToan } from 'app/modules/vien-phi/thu-vien-phi/thu-vien-phi.reducer';

export interface IVienPhiChiTietChuaThanhToanProps extends StateProps, DispatchProps{
}

class VienPhiChiTietChuaThanhToan extends React.Component<IVienPhiChiTietChuaThanhToanProps> {

  onSelectionChanged = e => {
    // e.selectedRowsData
    this.props.selectChiTietThanhToan(e.selectedRowsData);
  }

  render() {
    return (
      <div className="mt-2">
        <u><h5><i className="dx-icon-detailslayout" /> <Translate contentKey="vienphi.chitiettt.dvchuathanhtoan" /></h5></u>
        <GridVienPhiChiTietThanhToan
          id="grid-chua-thanh-toan"
          dataSource = {this.props.dataSource }
          height = {350}
          onSelectionChanged = {this.onSelectionChanged}
          selectAllAfterLoad={this.props.loaiGiaoDich === 1}
          disabledSelect={this.props.loaiGiaoDich !== 1}
        />
      </div>
    );
  }

}

const mapStateToProps = ({ vienPhiThuTien }: IRootState) => ({
  dataSource: vienPhiThuTien.dataSourceChuaThanhToan,
  loaiGiaoDich: vienPhiThuTien.formDataTtLanThu.loaiGiaoDich
});

const mapDispatchToProps = {
  selectChiTietThanhToan
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VienPhiChiTietChuaThanhToan);
