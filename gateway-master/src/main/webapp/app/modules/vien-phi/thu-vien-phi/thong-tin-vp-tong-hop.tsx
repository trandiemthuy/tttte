import React from 'react';
import Form, { ButtonItem, Label, SimpleItem } from 'devextreme-react/form';
import { LoadIndicator } from 'devextreme-react/load-indicator';
import { translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { VienPhiDefaultNumberFormat } from 'app/modules/vien-phi/utils/vienphi-constant';
import { inBangKe } from 'app/modules/vien-phi/thu-vien-phi/thu-vien-phi.reducer';

export interface ITtvpTongHopProps extends StateProps, DispatchProps {
}

class VienPhiTtvpTongHop extends React.Component<ITtvpTongHopProps> {

  btnInBangKeClick = e => {
    this.props.inBangKe();
  }

  render() {
    const { data, loading, disabledInBk } = this.props;
    return (
      <React.Fragment>
        <div className="d-flex justify-content-center p-2">
          <LoadIndicator visible={loading} height={30} width={30} />
        </div>
        <Form
          id="form-ttvp-tong-hop"
          formData={{...data}}
          readOnly
          showColonAfterLabel
        >
          <SimpleItem cssClass="pt-0 font-weight-bold" dataField="tongDaThu" editorType="dxNumberBox" editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: { style: 'text-align: right; color: red;' } }}>
            <Label text={translate('vienphi.ttvptonghop.tongtiendathu')} />
          </SimpleItem>
          <SimpleItem cssClass="pt-0" dataField="thanhToan" editorType="dxNumberBox" editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: { style: 'text-align: right;' } }}>
            <Label text={translate('vienphi.ttvptonghop.thanhtoan')} />
          </SimpleItem>
          <SimpleItem cssClass="pt-0" dataField="tamUng" editorType="dxNumberBox" editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: { style: 'text-align: right;' } }}>
            <Label text={translate('vienphi.ttvptonghop.tamung')} />
          </SimpleItem>
          <SimpleItem cssClass="pt-0 font-weight-bold" dataField="tongChiPhi" editorType="dxNumberBox" editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: { style: 'text-align: right; color: blue;' } }}>
            <Label text={translate('vienphi.ttvptonghop.tongchiphi')} />
          </SimpleItem>
          <SimpleItem cssClass="pt-0 font-weight-bold" dataField="tongBh" editorType="dxNumberBox" editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: { style: 'text-align: right;' } }}>
            <Label text={translate('vienphi.ttvptonghop.tongbh')} />
          </SimpleItem>
          <SimpleItem cssClass="pt-0" dataField="bhChiTra" editorType="dxNumberBox" editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: { style: 'text-align: right;' } }}>
            <Label text={translate('vienphi.ttvptonghop.bhchitra')} />
          </SimpleItem>
          <SimpleItem cssClass="pt-0" dataField="cungChiTra" editorType="dxNumberBox" editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: { style: 'text-align: right;' } }}>
            <Label text={translate('vienphi.ttvptonghop.cungchitra')} />
          </SimpleItem>
          <SimpleItem cssClass="pt-0 font-weight-bold" dataField="bnPhaiTra" editorType="dxNumberBox" editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: { style: 'text-align: right;' } }}>
            <Label text={translate('vienphi.ttvptonghop.tongtienbntra')} />
          </SimpleItem>
          <SimpleItem cssClass="pt-0 font-weight-bold" dataField="hoanUng" editorType="dxNumberBox" editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: { style: 'text-align: right; color: green;' } }}>
            <Label text={translate('vienphi.ttvptonghop.hoanung')} />
          </SimpleItem>
          <SimpleItem cssClass="pt-0 font-weight-bold" dataField="conLai" editorType="dxNumberBox" editorOptions={{ format: VienPhiDefaultNumberFormat, inputAttr: { style: 'text-align: right;' } }}>
            <Label text={translate('vienphi.ttvptonghop.conlai')} />
          </SimpleItem>
          <ButtonItem horizontalAlignment="right" buttonOptions={{ text: 'In bảng kê', type: 'default', disabled: disabledInBk, onClick: this.btnInBangKeClick }} />
        </Form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = ( { vienPhiThuTien }: IRootState) => ({
  data: vienPhiThuTien.thongTinVpTongHop,
  loading: vienPhiThuTien.loadingTtVpTh,
  disabledInBk: !vienPhiThuTien.benhNhanSelected.maBn
});

const mapDispatchToProps = {
  inBangKe
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VienPhiTtvpTongHop);

