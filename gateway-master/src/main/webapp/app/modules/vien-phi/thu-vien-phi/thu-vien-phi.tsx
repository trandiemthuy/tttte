import React from 'react';
import VienPhiTitle from 'app/modules/vien-phi/commons/title';
import { translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';
import { VienPhiBenhNhan } from 'app/modules/vien-phi/commons/benh-nhan';
import Box, { Item } from 'devextreme-react/box';
import VienPhiTimBenhNhan from 'app/modules/vien-phi/thu-vien-phi/tim-benh-nhan';
import VienPhiTtvpTongHop from 'app/modules/vien-phi/thu-vien-phi/thong-tin-vp-tong-hop';
import VienPhiTtLanThu from 'app/modules/vien-phi/thu-vien-phi/thong-tin-lan-thu';
import VienPhiDsLanThu from 'app/modules/vien-phi/thu-vien-phi/ds-lan-thu';
import VienPhiChiTietChuaThanhToan from 'app/modules/vien-phi/thu-vien-phi/chi-tiet-chua-thanh-toan';
import VienPhiPopUpDsBenhNhan from 'app/modules/vien-phi/thu-vien-phi/pop-up-ds-benh-nhan';
import { togglePopupDsbn } from 'app/modules/vien-phi/thu-vien-phi/thu-vien-phi.reducer';
import { LoadPanel } from 'devextreme-react/load-panel';

export interface IVienPhiThuTienProps extends StateProps, DispatchProps {
}

class VienPhiThuTien extends React.Component<IVienPhiThuTienProps> {

  render() {
    const { benhNhanSelected, creatingBangKe } = this.props;
    return (
      <React.Fragment>
        <LoadPanel
          shadingColor="rgba(0,0,0,0.4)"
          position={{ of: '#div-thu-vien-phi' }}
          visible={creatingBangKe}
          showIndicator
          shading={false}
          showPane
          closeOnOutsideClick={false}
        />
        <div id="div-thu-vien-phi">
          <VienPhiPopUpDsBenhNhan />
          <VienPhiTitle title={translate('vienphi.thutien.tieude')}/>
          <Box>
            <Item ratio={9}>
              <div className="p-1 border-bottom">
                <VienPhiTimBenhNhan />
              </div>
              <div className="p-1 border-bottom">
                <VienPhiBenhNhan data={benhNhanSelected}/>
              </div>
              <div className="p-1">
                <VienPhiTtLanThu/>
              </div>
            </Item>
            <Item ratio={3}>
              <div className="pl-3 ml-3">
                <VienPhiTtvpTongHop />
              </div>
            </Item>
          </Box>
          <Box>
            <Item ratio={7}>
               <VienPhiChiTietChuaThanhToan />
            </Item>
            <Item ratio={5}>
              <VienPhiDsLanThu />
            </Item>
          </Box>
        </div>
      </React.Fragment>
    );
  }

}

const mapStateToProps = ({ vienPhiThuTien }: IRootState) => ({
  benhNhanSelected: vienPhiThuTien.benhNhanSelected,
  creatingBangKe: vienPhiThuTien.creatingBangKe
});

const mapDispatchToProps = {
  togglePopupDsbn
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VienPhiThuTien);
