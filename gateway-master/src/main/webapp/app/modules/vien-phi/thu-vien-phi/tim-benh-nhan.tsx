import React from 'react';
import Form, { GroupItem, Label, SimpleItem, ButtonItem } from 'devextreme-react/form';
import { Button } from 'devextreme-react';
import { translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { searchBenhNhan, togglePopupDsbn } from 'app/modules/vien-phi/thu-vien-phi/thu-vien-phi.reducer';
import { connect } from 'react-redux';
import ArrayStore from 'devextreme/data/array_store';
import { listLoaiKcb } from 'app/modules/vien-phi/models/loaikcb.model';
import moment from 'moment';
import notify from 'devextreme/ui/notify';

export interface IVienPhiTimBenhNhanProps extends StateProps, DispatchProps {
}

export interface IVienPhiTimBenhNhanState {
  formData: any;
}

class VienPhiTimBenhNhan extends React.Component<IVienPhiTimBenhNhanProps, IVienPhiTimBenhNhanState> {

  private defaultFormData = {
    tuNgay: moment().toDate(),
    denNgay: moment().toDate(),
    loaiKcb: listLoaiKcb[0].value,
    maBn: null
  };
  private dataSourceLoaiKcb = new ArrayStore({
    data: listLoaiKcb,
    key: 'value'
  });

  constructor(props, state) {
    super(props, state);
    this.state = {
      formData: { ...this.defaultFormData }
    }
  }

  validateFormData = (): boolean => {
    const { formData } = this.state;
    const valid = formData.tuNgay !== null && formData.denNgay !== null && formData.maBn !== null && formData.maBan !== '';
    if (!valid) {
      notify(translate('vienphi.timbenhnhan.chuanhapduthongtin'), 'warning');
    }
    return valid;
  }

  btnDsbnClick = e => {
      this.props.togglePopupDsbn(true);
  }

  btnSearchClick = e => {
    if (this.validateFormData()) {
      this.props.searchBenhNhan(this.state.formData);
    }
  }

  btnClearClick = e => {
    this.setState({
      formData: { ...this.defaultFormData }
    });
  }

  render() {
    return (
      <Form
        id="form-tim-benh-nhan"
        showColonAfterLabel
        formData={this.state.formData}
        disabled={this.props.formDisabled}
      >
        <GroupItem colCount={9}>
          <SimpleItem colSpan={2} dataField="tuNgay" isRequired editorType="dxDateBox" editorOptions={{displayFormat: 'dd/MM/yyyy', placeholder: 'Từ ngày'}}>
            <Label text={translate('vienphi.timbenhnhan.tungay')} />
          </SimpleItem>
          <SimpleItem colSpan={2} dataField="denNgay" isRequired editorType="dxDateBox" editorOptions={{displayFormat: 'dd/MM/yyyy', placeholder: 'Đến ngày'}}>
            <Label text={translate('vienphi.timbenhnhan.denngay')} />
          </SimpleItem>
          <SimpleItem colSpan={2} dataField="loaiKcb" isRequired editorType="dxSelectBox"
                      editorOptions={{
                        placeholder: 'Loại KCB',
                        dataSource: this.dataSourceLoaiKcb,
                        displayExpr: 'name',
                        valueExpr: 'value'
                      }}
          >
            <Label text={translate('vienphi.timbenhnhan.loaikcb')} />
          </SimpleItem>
          <SimpleItem colSpan={2} dataField="maBn" editorType="dxNumberBox"
                      editorOptions={{
                        placeholder: 'Mã bệnh nhân',
                        onEnterKey: this.btnSearchClick
                      }}>
            <Label text={translate('vienphi.timbenhnhan.mabn')} />
          </SimpleItem>
          <GroupItem cssClass="p-0">
            <div className="d-flex">
              <Button icon="search" type="default" stylingMode="contained" onClick={this.btnSearchClick} />
              <Button icon="detailslayout" className="ml-2" type="default" stylingMode="outlined" onClick={this.btnDsbnClick} />
              <Button icon="clear" className="ml-2" type="default" stylingMode="outlined" onClick={this.btnClearClick} />
            </div>
          </GroupItem>
        </GroupItem>
      </Form>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  formDisabled: storeState.vienPhiThuTien.searchingBenhNhan
});

const mapDispatchToProps = {
  togglePopupDsbn,
  searchBenhNhan
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VienPhiTimBenhNhan);
