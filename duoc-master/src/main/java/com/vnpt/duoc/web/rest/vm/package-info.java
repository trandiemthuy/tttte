/**
 * View Models used by Spring MVC REST controllers.
 */
package com.vnpt.duoc.web.rest.vm;
