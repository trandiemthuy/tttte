package com.vnpt.duoc.jasper;

//import dangnhap.UserController;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.query.JRQueryExecuterFactory;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRProperties;
import net.sf.jasperreports.export.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;


public class JasperHelper extends JdbcTemplate{
    public static void printPDF(String jasperFile, Map parameters, Connection cnn, HttpServletResponse response) {
        printReport("pdf", jasperFile, parameters, cnn, response);
    }

    public static void printXLS(String jasperFile, Map parameters, Connection cnn, HttpServletResponse response) {
        printReport("xls", jasperFile, parameters, cnn, response);
    }

    public static void printXLSX(String jasperFile, Map parameters, Connection cnn, HttpServletResponse response) {
        printReport("xlsx", jasperFile, parameters, cnn, response);
    }

    public static void printRTF(String jasperFile, Map parameters, Connection cnn, HttpServletResponse response) {
        printReport("rtf", jasperFile, parameters, cnn, response);
    }

    public static void printReport(String exportType, String jasperFile, Map parameters, Connection cnn, HttpServletResponse response) {
        printReport(exportType, new File(jasperFile), parameters, cnn, response);
    }
    public static void printReport(String exportType, String jasperFile, Map parameters, DataSource dataSource, HttpServletResponse response) {
        try(Connection conn = dataSource.getConnection()){
            printReport(exportType, jasperFile, parameters, conn, response);
        }catch (Exception ex){

        }

    }

    public static void printReport(String exportType, String jasperFile, Map parameters, JRDataSource dataSource, HttpServletResponse response) {
        try {
            File reportFile = new File(jasperFile);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile);
            String fileName = reportFile.getName();
            printReport(exportType, jasperReport, fileName, parameters, dataSource, response);
        } catch (Exception ex) {
            Logger.getLogger(JasperHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void printReport(String exportType, File reportFile, Map parameters, Connection cnn, HttpServletResponse response) {
        try {
            printInfo(reportFile.getName(), parameters);  // Print line
            Connection conToUse = cnn;
            // Create close-suppressing Connection proxy, also preparing returned Statements.

            //File reportFile = new File(jasperFile);
            //String path = request.getSession().getServletContext().getRealPath("/WEB-INF/store_files/")+"/baocaongay" + "_" + arr[2] + date + ".pdf";
            //JasperDesign jasperDesign = JRXmlLoader.load(new File(request.getSession().getServletContext().getRealPath("/WEB-INF/pages/baocao/rp_baocaongay.jrxml")));
            //JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile);
            String fileName = reportFile.getName();
            printReport(exportType, jasperReport, fileName, parameters, cnn, response);
        } catch (Exception ex) {
            Logger.getLogger(JasperHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void printReportWithSubReport(String exportType, File reportFile, Map parameters, Connection cnn, HttpServletResponse response) {
        try {
            printInfo(reportFile.getName(), parameters);  // Print line
            Connection conToUse = cnn;
            // Create close-suppressing Connection proxy, also preparing returned Statements.

            //File reportFile = new File(jasperFile);
            //String path = request.getSession().getServletContext().getRealPath("/WEB-INF/store_files/")+"/baocaongay" + "_" + arr[2] + date + ".pdf";
            //JasperDesign jasperDesign = JRXmlLoader.load(new File(request.getSession().getServletContext().getRealPath("/WEB-INF/pages/baocao/rp_baocaongay.jrxml")));
            //JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile);
            String fileName = reportFile.getName();
            printReport(exportType, jasperReport, fileName, parameters, cnn, response);
        } catch (Exception ex) {
            Logger.getLogger(JasperHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void printReport(String exportType, File reportFile, Map parameters, DataSource dataSource, HttpServletResponse response) {
        try(Connection conn = dataSource.getConnection()){
            printReport(exportType, reportFile, parameters, conn, response);
        }catch (Exception ex){

        }
    }
    public static void printReportView(String exportType, File reportFile, Map parameters, Connection cnn, HttpServletResponse response) {
        try {
            printInfo(reportFile.getName(), parameters);  // Print line
            Connection conToUse = cnn;
            // Create close-suppressing Connection proxy, also preparing returned Statements.

            //File reportFile = new File(jasperFile);
            //String path = request.getSession().getServletContext().getRealPath("/WEB-INF/store_files/")+"/baocaongay" + "_" + arr[2] + date + ".pdf";
            //JasperDesign jasperDesign = JRXmlLoader.load(new File(request.getSession().getServletContext().getRealPath("/WEB-INF/pages/baocao/rp_baocaongay.jrxml")));
            //JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile);
            String fileName = reportFile.getName();
            printReportVew(exportType, jasperReport, fileName, parameters, cnn, response);
        } catch (Exception ex) {
            Logger.getLogger(JasperHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void printReport(String exportType, String fileName, File reportFile, Map parameters, Connection cnn, HttpServletResponse response) throws SQLException {
        try {
            printInfo(fileName, parameters);  // Print line

            //File reportFile = new File(jasperFile);
            //String path = request.getSession().getServletContext().getRealPath("/WEB-INF/store_files/")+"/baocaongay" + "_" + arr[2] + date + ".pdf";
            //JasperDesign jasperDesign = JRXmlLoader.load(new File(request.getSession().getServletContext().getRealPath("/WEB-INF/pages/baocao/rp_baocaongay.jrxml")));
            //JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile);
            printReport(exportType, jasperReport, fileName, parameters, cnn, response);
        } catch (JRException | SQLException ex) {
            Logger.getLogger(JasperHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void printReport(String exportType, JasperReport jasperReport, String fileName, Map parameters, JRDataSource dataSource, HttpServletResponse response) {
        try {
            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
            fileName = fileName.substring(0, fileName.length() - 6) + exportType;
            response.setContentType("application/" + exportType);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            if (exportType.equalsIgnoreCase("pdf")) {
                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("xls")) {
                JRXlsExporter exporter = new JRXlsExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("xlsx")) {
                JRXlsxExporter exporter = new JRXlsxExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("rtf")) {
                JRRtfExporter exporter = new JRRtfExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleWriterExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            }
        } catch (Exception ex) {
            Logger.getLogger(JasperHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void printReport(String exportType, JasperReport jasperReport, String fileName, Map parameters, Connection cnn, HttpServletResponse response) throws SQLException {
        try {
            // CMU định dạng số
//            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
//            HttpSession session = attr.getRequest().getSession(true);
//            String dinhdangso = session.getAttribute("Sess_DinhDangSo").toString();
//            if (dinhdangso.equals("1")) {
                parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
//            }
            printInfo(fileName, parameters);  // Print line

            //File reportFile = new File(jasperFile);
            //String path = request.getSession().getServletContext().getRealPath("/WEB-INF/store_files/")+"/baocaongay" + "_" + arr[2] + date + ".pdf";
            //JasperDesign jasperDesign = JRXmlLoader.load(new File(request.getSession().getServletContext().getRealPath("/WEB-INF/pages/baocao/rp_baocaongay.jrxml")));
            //JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            //JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile);
            JRProperties.setProperty(JRQueryExecuterFactory.QUERY_EXECUTER_FACTORY_PREFIX + "plsql", "com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");

            //JasperCompileManager.compileReportToFile("D:\\JavaProject\\vnpt_his\\web_his\\WEB-INF\\pages\\baocao\\rp_baocaongay.jrxml", "D:\\JavaProject\\vnpt_his\\web_his\\WEB-INF\\pages\\baocao\\rp_baocaongay.jasper");
            jasperReport.setProperty("net.sf.jasperreports.query.executer.factory.plsql", "com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
            //Maybe this too, but not positive

            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, cnn);
            JRProperties.setProperty(JRParagraph.DEFAULT_TAB_STOP_WIDTH, "10");
            //File file = new File(path);

            //response.setContentLength(new Long(file.length()).intValue());
            //fileName = fileName.substring(0, fileName.length() - 6) + exportType;
            fileName = fileName.substring(0, fileName.length() - 7) + "_" +new java.util.Date().getTime()/1000 + "." + exportType;
            System.out.println("exportType=" + exportType);
            System.out.println("fileName=" + fileName);
            response.setContentType("application/" + exportType);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            if (exportType.equalsIgnoreCase("pdf")) {
                //response.setHeader("Content-disposition", "inline;filename=" + fileName); Tuan chu thich
                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("xls")) {
                JRXlsExporter exporter = new JRXlsExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("xlsx")) {
                JRXlsxExporter exporter = new JRXlsxExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("rtf")) {
                JRRtfExporter exporter = new JRRtfExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleWriterExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            }else if (exportType.equalsIgnoreCase("docx")){
                JRDocxExporter export = new JRDocxExporter();
                export.setExporterInput(new SimpleExporterInput(print));
                export.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));

                SimpleDocxReportConfiguration config = new SimpleDocxReportConfiguration();
                //config.setFlexibleRowHeight(true); //Set desired configuration

                export.setConfiguration(config);
                export.exportReport();
            }
        } catch (Exception ex) {
            Logger.getLogger(JasperHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void printReportVew(String exportType, JasperReport jasperReport, String fileName, Map parameters, Connection cnn, HttpServletResponse response) throws SQLException {
        try {
            // CMU định dạng số
            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            HttpSession session = attr.getRequest().getSession(true);
            String dinhdangso = session.getAttribute("Sess_DinhDangSo").toString();
            if (dinhdangso.equals("1")) {
                parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
            }
            printInfo(fileName, parameters);  // Print line

            //File reportFile = new File(jasperFile);
            //String path = request.getSession().getServletContext().getRealPath("/WEB-INF/store_files/")+"/baocaongay" + "_" + arr[2] + date + ".pdf";
            //JasperDesign jasperDesign = JRXmlLoader.load(new File(request.getSession().getServletContext().getRealPath("/WEB-INF/pages/baocao/rp_baocaongay.jrxml")));
            //JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            //JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile);
            JRProperties.setProperty(JRQueryExecuterFactory.QUERY_EXECUTER_FACTORY_PREFIX + "plsql", "com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");

            //JasperCompileManager.compileReportToFile("D:\\JavaProject\\vnpt_his\\web_his\\WEB-INF\\pages\\baocao\\rp_baocaongay.jrxml", "D:\\JavaProject\\vnpt_his\\web_his\\WEB-INF\\pages\\baocao\\rp_baocaongay.jasper");
            jasperReport.setProperty("net.sf.jasperreports.query.executer.factory.plsql", "com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
            //Maybe this too, but not positive

            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, cnn);
            JRProperties.setProperty(JRParagraph.DEFAULT_TAB_STOP_WIDTH, "10");
            //File file = new File(path);

            //response.setContentLength(new Long(file.length()).intValue());
            //fileName = fileName.substring(0, fileName.length() - 6) + exportType;
            fileName = fileName.substring(0, fileName.length() - 7) + "_" +new java.util.Date().getTime()/1000 + "." + exportType;
            System.out.println("exportType=" + exportType);
            System.out.println("fileName=" + fileName);
            response.setContentType("application/" + exportType);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            if (exportType.equalsIgnoreCase("pdf")) {
                //response.setHeader("Content-disposition", "inline;filename=" + fileName); Tuan chu thich
                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("xls")) {
                JRXlsExporter exporter = new JRXlsExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("xlsx")) {
                JRXlsxExporter exporter = new JRXlsxExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("rtf")) {
                JRRtfExporter exporter = new JRRtfExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleWriterExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            }
        } catch (Exception ex) {
            Logger.getLogger(JasperHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void printReport(String exportType, List<ExporterInputItem> exporterInputItems, File reportFile, Map parameters, HttpServletResponse response) {
        try {
            String fileName = reportFile.getName();
            // CMU định dạng số
            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            HttpSession session = attr.getRequest().getSession(true);
            String dinhdangso = session.getAttribute("Sess_DinhDangSo").toString();
            if (dinhdangso.equals("1")) {
                parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
            }
            printInfo(fileName, parameters);  // Print line

            JRProperties.setProperty(JRQueryExecuterFactory.QUERY_EXECUTER_FACTORY_PREFIX + "plsql", "com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");

            fileName = fileName.substring(0, fileName.length() - 7) + "_" +new java.util.Date().getTime()/1000 + "." + exportType;
            System.out.println("exportType=" + exportType);
            System.out.println("fileName=" + fileName);
            response.setContentType("application/" + exportType);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            if (exportType.equalsIgnoreCase("pdf")) {
                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setExporterInput(new SimpleExporterInput(exporterInputItems));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("xls")) {
                JRXlsExporter exporter = new JRXlsExporter();
                exporter.setExporterInput(new SimpleExporterInput(exporterInputItems));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("xlsx")) {
                JRXlsxExporter exporter = new JRXlsxExporter();
                exporter.setExporterInput(new SimpleExporterInput(exporterInputItems));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("rtf")) {
                JRRtfExporter exporter = new JRRtfExporter();
                exporter.setExporterInput(new SimpleExporterInput(exporterInputItems));
                exporter.setExporterOutput(new SimpleWriterExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            }
        } catch (Exception ex) {
            Logger.getLogger(JasperHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void printInfo(String fileName, Map parameters) {
        // Debug start
//        if (UserController.isLocalHost) {
//            System.out.println("\nReport file name: " + fileName);
//        }
        System.out.println("\nReport file name: " + fileName);
        Iterator entries = parameters.entrySet().iterator();

        while (entries.hasNext()) {
            Map.Entry thisEntry = (Map.Entry) entries.next();
            Object key = thisEntry.getKey();
            Object value = thisEntry.getValue();
//            if (UserController.isLocalHost) {
//                System.out.println("Param [" + key + "]: " + value);
//            }
            System.out.println("Param [" + key + "]: " + value);
        }
//        if (UserController.isLocalHost) {
//            System.out.println("End " + fileName + "\n");
//        }
        System.out.println("End " + fileName + "\n");
        // Debug end
    }
    //HPG bổ sung format cell khi xuất excel
    public static void printReport_hpg(String exportType, File reportFile, Map parameters, Connection cnn, HttpServletResponse response) {
        try {
            printInfo(reportFile.getName(), parameters);  // Print line
            Connection conToUse = cnn;
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile);
            String fileName = reportFile.getName();
            jasperReport.setProperty("net.sf.jasperreports.export.xls.detect.cell.type", "true");
            printReport(exportType, jasperReport, fileName, parameters, cnn, response);
        } catch (Exception ex) {
            Logger.getLogger(JasperHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //end
    //YBI-hoannq
    //Date:01/01/2019
    //Description: Fix định dạng Number từ Jasper report ra excel
    public static void printReport(String exportType, File reportFile, Map parameters, Connection cnn, HttpServletResponse response,String mauXuatRieng) {
        try {
            printInfo(reportFile.getName(), parameters);  // Print line
            Connection conToUse = cnn;
            // Create close-suppressing Connection proxy, also preparing returned Statements.

            //File reportFile = new File(jasperFile);
            //String path = request.getSession().getServletContext().getRealPath("/WEB-INF/store_files/")+"/baocaongay" + "_" + arr[2] + date + ".pdf";
            //JasperDesign jasperDesign = JRXmlLoader.load(new File(request.getSession().getServletContext().getRealPath("/WEB-INF/pages/baocao/rp_baocaongay.jrxml")));
            //JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile);
            String fileName = reportFile.getName();
            printReport(mauXuatRieng,exportType, jasperReport, fileName, parameters, cnn, response);
        } catch (Exception ex) {
            Logger.getLogger(JasperHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void printReport(String mauXuatRieng,String exportType, JasperReport jasperReport, String fileName, Map parameters, Connection cnn, HttpServletResponse response) throws SQLException {
        try {

            printInfo(fileName, parameters);  // Print line

            //File reportFile = new File(jasperFile);
            //String path = request.getSession().getServletContext().getRealPath("/WEB-INF/store_files/")+"/baocaongay" + "_" + arr[2] + date + ".pdf";
            //JasperDesign jasperDesign = JRXmlLoader.load(new File(request.getSession().getServletContext().getRealPath("/WEB-INF/pages/baocao/rp_baocaongay.jrxml")));
            //JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            //JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile);
            JRProperties.setProperty(JRQueryExecuterFactory.QUERY_EXECUTER_FACTORY_PREFIX + "plsql", "com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");

            //JasperCompileManager.compileReportToFile("D:\\JavaProject\\vnpt_his\\web_his\\WEB-INF\\pages\\baocao\\rp_baocaongay.jrxml", "D:\\JavaProject\\vnpt_his\\web_his\\WEB-INF\\pages\\baocao\\rp_baocaongay.jasper");
            jasperReport.setProperty("net.sf.jasperreports.query.executer.factory.plsql", "com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
            //Maybe this too, but not positive

            JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, cnn);
            JRProperties.setProperty(JRParagraph.DEFAULT_TAB_STOP_WIDTH, "10");
            //File file = new File(path);

            //response.setContentLength(new Long(file.length()).intValue());
            //        fileName = fileName.substring(0, fileName.length() - 6) + exportType;
            fileName = fileName.substring(0, fileName.length() - 7) + "_" +new java.util.Date().getTime()/1000 + "." + exportType;
            System.out.println("exportType=" + exportType);
            System.out.println("fileName=" + fileName);
            response.setContentType("application/" + exportType);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            if (exportType.equalsIgnoreCase("pdf")) {
                //response.setHeader("Content-disposition", "inline;filename=" + fileName); Tuan chu thich
                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("xls")) {
                JRXlsExporter exporter = new JRXlsExporter();
                //Hoannq-YBI
                SimpleXlsReportConfiguration xlsReportConfiguration = new SimpleXlsReportConfiguration();
                xlsReportConfiguration.setDetectCellType(true);
                xlsReportConfiguration.setCollapseRowSpan(true);
                exporter.setConfiguration(xlsReportConfiguration);
                //end Hoannq
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("xlsx")) {
                JRXlsxExporter exporter = new JRXlsxExporter();
                //Hoannq-YBI
                SimpleXlsxReportConfiguration xlsxReportConfiguration = new SimpleXlsxReportConfiguration();
                xlsxReportConfiguration.setDetectCellType(true);
                xlsxReportConfiguration.setCollapseRowSpan(true);
                exporter.setConfiguration(xlsxReportConfiguration);
                //end Hoannq
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            } else if (exportType.equalsIgnoreCase("rtf")) {
                JRRtfExporter exporter = new JRRtfExporter();
                exporter.setExporterInput(new SimpleExporterInput(print));
                exporter.setExporterOutput(new SimpleWriterExporterOutput(response.getOutputStream()));
                exporter.exportReport();
            }
        } catch (Exception ex) {
            Logger.getLogger(JasperHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //End-YBI-Hoannq

}
