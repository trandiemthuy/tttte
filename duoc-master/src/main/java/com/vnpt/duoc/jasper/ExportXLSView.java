package com.vnpt.duoc.jasper;

import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class ExportXLSView extends AbstractXlsView {

    public static String[] arr_canchuyen;
    public static String sheetName;
    public static String fileName;

    @Override
    protected void buildExcelDocument(Map model, Workbook workbook,
                                      HttpServletRequest request, HttpServletResponse response) throws Exception {
        Sheet excelSheet = workbook.createSheet(sheetName);
        List<Map<String, Object>> list = (List<Map<String, Object>>) model.get("report_view");
        if (!list.isEmpty()) {
            Map m = list.get(0);
            Object[] arr_title = m.keySet().toArray();
            setExcelHeader(excelSheet, arr_title);
            setExcelRows(excelSheet, list, arr_title);
        }
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
    }

    public void setArrCanchuyen(String[] arr_canchuyen) {
        this.arr_canchuyen = arr_canchuyen;
    }

    public boolean isConvert(String str) {
        for (String s : arr_canchuyen) {
            if (s.equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    public void setExcelHeader(Sheet excelSheet, Object[] arr_title) {
        Row excelHeader = excelSheet.createRow(0);
        int i = 0;
        int length = arr_title.length;
        for (i = 0; i < length; i++) {
            excelHeader.createCell(i).setCellValue(arr_title[i].toString());
        }
    }

    public void setExcelRows(Sheet excelSheet, List<Map<String, Object>> list, Object[] arr_title) {
        int record = 1;
        int i = 0;
        int length = arr_title.length;
        for (Map m : list) {
            Row excelRow = excelSheet.createRow(record++);
            for (i = 0; i < length; i++) {
                excelRow.createCell(i).setCellValue(m.get(arr_title[i]) != null ? m.get(arr_title[i]).toString() : "");
            }
        }
        for (int j = 0; j < length; j++) {
            excelSheet.autoSizeColumn(j, true);
        }
    }
}
