package com.vnpt.duoc.jasper;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SoYLenhExportXLSView extends AbstractXlsView {
    public static String[] arr_canchuyen;
    public static String tensheet;
    public static String filename;
    public static String tenTinh;
    public static String tenBenhVien;
    public static String tenKhoa;
    public static String ngay;
    public static String dvtt;
    public static String sophieu;
    public static boolean blnFormatDocument;

    //VTU: 22/10/2016
    public static String maubc;
    //VTU: 22/10/2016

    //HPG: 19/12/10/2016
    public static String tsdv;
    //HPG: 19/12/2016
    //VNPT NGHEAN - HISHTROTGG-40962 - START
    public static String tsdv_sign;
    public static Integer tsdvKhoanCachChuKy;
    //VNPT NGHEAN - HISHTROTGG-40962 - END
    public static String tenPhong;
    @Override
    protected void buildExcelDocument(Map model, Workbook workbook,
                                      HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        Sheet excelSheet = workbook.createSheet(tensheet);
        List<Map<String, Object>> list = (List<Map<String, Object>>) model.get("report_view");
        if (!list.isEmpty()) {
            Map m = list.get(0);
            blnFormatDocument = true;
            if (blnFormatDocument) {
                Object[] arr_hospital_info = new Object[]{tenTinh, tenBenhVien, tenKhoa + tenPhong, sophieu};
                Collection<String> values = m.values();
                Object[] arr_title = values.toArray(new Object[values.size()]);
                if (tsdv.equals("1")) {
                    hpg_setExcelHeader(workbook, excelSheet, arr_title, 4);
                } else {
                    setExcelHeader(workbook, excelSheet, arr_title, 4);
                }
                //VTU: 22/10/2016
                //NEU NHU SU DUNG MAU BAO CAO CHUAN
                if (maubc.equals("0")) {
                    setExcelRows(workbook, excelSheet, list, arr_title, 5);
                } else if (maubc.equals("1")) {   //NEU NHU SD MAU BAO CAO CO THEM COT TONG O DUOI
                    if (tsdv.equals("1")) {
                        setExcelRows_Tong_hpg(workbook, excelSheet, list, arr_title, 5);
                    } else {
                        setExcelRows_Tong(workbook, excelSheet, list, arr_title, 5);
                    }
                } else {
                    setExcelRows(workbook, excelSheet, list, arr_title, 5);
                }
                setHospitalInfo(workbook, excelSheet, arr_hospital_info, 0, list.size() + 6);
            } else {
                Object[] arr_title = m.keySet().toArray();
                setExcelHeader(workbook, excelSheet, arr_title);
                setExcelRows(excelSheet, list, arr_title);
            }
        }
        response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
    }

    public void setArrCanchuyen(String[] arr_canchuyen) {
        this.arr_canchuyen = arr_canchuyen;
    }
    //22/03/2017 HPG tính tổng dòng cột thứ 2
    public void setExcelRows_Tong_hpg(Workbook workbook, Sheet excelSheet, List<Map<String, Object>> list, Object[] arr_title, int startRowNumber) {
        int record = startRowNumber + 1;
        int i = 0;
        int length = arr_title.length;
        arr_title = list.get(0).keySet().toArray();
        list.remove(0);
        CellStyle columnHeadStyleBorderOnly = workbook.createCellStyle();
        columnHeadStyleBorderOnly.setBorderBottom(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderTop(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderRight(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderLeft(CellStyle.BORDER_MEDIUM);
        //int[] arrTong=new int[]();
        int[] arrTong = new int[length];
        int nurec = list.size();
        for (Map m : list) {
            Row excelRow = excelSheet.createRow(record++);
            for (i = 0; i < length; i++) {
                Cell cell = excelRow.createCell(i + 1);
                cell.setCellValue((m.get(arr_title[i]) + "").equals("0") ? "" : (m.get(arr_title[i]) + ""));
                cell.setCellStyle(columnHeadStyleBorderOnly);
                if (i >= 1) {
                    arrTong[i] = arrTong[i] + Integer.parseInt(m.get(arr_title[i]) + "");
                }
            }
        }
        for (int j = 0; j < length; j++) {
            excelSheet.autoSizeColumn(j + 1, true);
        }
        Row excelRow = excelSheet.createRow(startRowNumber);

        CellStyle columnHeadStyle = workbook.createCellStyle();
        columnHeadStyle.setBorderBottom(CellStyle.BORDER_MEDIUM);
        columnHeadStyle.setBorderTop(CellStyle.BORDER_MEDIUM);
        columnHeadStyle.setBorderRight(CellStyle.BORDER_MEDIUM);
        columnHeadStyle.setBorderLeft(CellStyle.BORDER_MEDIUM);
        Font Font = workbook.createFont();
        Font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        Font.setColor(HSSFColor.RED.index);
        columnHeadStyle.setFont(Font);
        for (i = 0; i < length; i++) {
            if (i >= 1) {
                Cell cell = excelRow.createCell(i + 1);
                cell.setCellValue(arrTong[i] + "");
                cell.setCellStyle(columnHeadStyle);
            } else {
                Cell cell = excelRow.createCell(i + 1);
                cell.setCellValue("Tổng cộng: ");
                cell.setCellStyle(columnHeadStyle);
            }
        }
    }


    public boolean isConvert(String str) {
        for (String s : arr_canchuyen) {
            if (s.equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    public void setExcelHeader(Workbook workbook, Sheet excelSheet, Object[] arr_title) {
        Row excelHeader = excelSheet.createRow(0);
        CellStyle columnHeadStyle = workbook.createCellStyle();
        int i = 0;
        int length = arr_title.length;
        for (i = 0; i < 3; i++) {
            excelHeader.createCell(i).setCellValue(arr_title[i].toString());
        }

        for (i = 3; i < length; i++) {
            Cell cell = excelHeader.createCell(i);
            cell.setCellValue(arr_title[i].toString());
            short d = 90;
            columnHeadStyle.setRotation(d);
            cell.setCellStyle(columnHeadStyle);
        }
    }

    public void setExcelRows(Sheet excelSheet, List<Map<String, Object>> list, Object[] arr_title) {
        int record = 1;
        int i = 0;
        int length = arr_title.length;
        for (Map m : list) {
            Row excelRow = excelSheet.createRow(record++);
            for (i = 0; i < length; i++) {
                excelRow.createCell(i).setCellValue(m.get(arr_title[i]) != null ? m.get(arr_title[i]).toString() : "");
            }
        }
        for (int j = 0; j < length; j++) {
            excelSheet.autoSizeColumn(j, true);
        }
    }

    public void setHospitalInfo(Workbook workbook, Sheet excelSheet, Object[] arr_title, int startRowNumber, int length) {
        Font font15 = workbook.createFont();
        font15.setFontHeightInPoints((short) 15);

        Font font13 = workbook.createFont();
        font13.setFontHeightInPoints((short) 13);

        CellStyle cellStyleAlignCenter = workbook.createCellStyle();
        cellStyleAlignCenter.setAlignment((short) 2);
        cellStyleAlignCenter.setVerticalAlignment((short) 1);

        CellStyle cellStyleAlignCenterHuge = workbook.createCellStyle();
        cellStyleAlignCenterHuge.setAlignment((short) 2);
        cellStyleAlignCenterHuge.setVerticalAlignment((short) 1);
        cellStyleAlignCenterHuge.setFont(font15);

        CellStyle cellStyleAlignCenterLarge = workbook.createCellStyle();
        cellStyleAlignCenterLarge.setAlignment((short) 2);
        cellStyleAlignCenterLarge.setVerticalAlignment((short) 1);
        cellStyleAlignCenterLarge.setFont(font13);

        Row rowSoYTe = excelSheet.createRow(startRowNumber);
        Cell cellSoYTe = rowSoYTe.createCell(1);
        cellSoYTe.setCellValue(arr_title[0].toString());
        cellSoYTe.setCellStyle(cellStyleAlignCenter);

        Cell cellHeader = rowSoYTe.createCell(6);
        cellHeader.setCellValue("Sổ tổng hợp thuốc hàng ngày");
        cellHeader.setCellStyle(cellStyleAlignCenterHuge);

        excelSheet.addMergedRegion(new CellRangeAddress(startRowNumber, startRowNumber, 1, 3));
        excelSheet.addMergedRegion(new CellRangeAddress(startRowNumber, startRowNumber, 6, 20));

        Row rowTenBenhVien = excelSheet.createRow(startRowNumber + 1);
        Cell cellTenBenhVien = rowTenBenhVien.createCell(1);
        cellTenBenhVien.setCellValue(arr_title[1].toString());
        cellTenBenhVien.setCellStyle(cellStyleAlignCenter);

        Cell cellSubHeader = rowTenBenhVien.createCell(6);
        cellSubHeader.setCellValue(arr_title[2].toString());
        cellSubHeader.setCellStyle(cellStyleAlignCenterLarge);

        excelSheet.addMergedRegion(new CellRangeAddress(startRowNumber + 1, startRowNumber + 1, 1, 3));
        excelSheet.addMergedRegion(new CellRangeAddress(startRowNumber + 1, startRowNumber + 1, 6, 20));

        Row rowDate = excelSheet.createRow(startRowNumber + 2);
        Cell cellDate = rowDate.createCell(1);
        cellDate.setCellValue(ngay);
        cellDate.setCellStyle(cellStyleAlignCenter);

        cellSubHeader = rowDate.createCell(6);
        cellSubHeader.setCellValue(arr_title[3].toString());
        cellSubHeader.setCellStyle(cellStyleAlignCenterLarge);

        excelSheet.addMergedRegion(new CellRangeAddress(startRowNumber + 2, startRowNumber + 2, 1, 3));
        excelSheet.addMergedRegion(new CellRangeAddress(startRowNumber + 2, startRowNumber + 2, 6, 20));

        // [DNG] Sửa vị trí chữ kí sổ y lệnh
        if("48076".equals(dvtt)){
            CellStyle cellStyleSign = workbook.createCellStyle();
            cellStyleSign.setAlignment((short) 1);
            cellStyleSign.setVerticalAlignment((short) 1);
            cellStyleSign.setFont(font13);

            Row rowSignature = excelSheet.createRow(length);
            Cell cellTruongKhoaDuoc = rowSignature.createCell(1);
            cellTruongKhoaDuoc.setCellValue("TRƯỞNG KHOA DƯỢC        NGƯỜI PHÁT        NGƯỜI NHẬN        TRƯỞNG KHOA");
            cellTruongKhoaDuoc.setCellStyle(cellStyleSign);

            excelSheet.addMergedRegion(new CellRangeAddress(length, length, 1, 19));
        } //VNPT NGHEAN - HISHTROTGG-40962 - START
        else if("1".equals(tsdv_sign)){
            Font font13_b = workbook.createFont();
            font13_b.setFontHeightInPoints((short) 13);
            font13_b.setBold(true);
            CellStyle cellStyleSign = workbook.createCellStyle();
            cellStyleSign.setAlignment((short) 1);
            cellStyleSign.setVerticalAlignment((short) 1);
            cellStyleSign.setFont(font13_b);

            Row rowSignature = excelSheet.createRow(length);
            Cell cellTruongKhoaDuoc = rowSignature.createCell(1);
            cellTruongKhoaDuoc.setCellValue("Trưởng khoa dược       Người phát      Người nhận      Trưởng khoa");
            cellTruongKhoaDuoc.setCellStyle(cellStyleSign);
            excelSheet.addMergedRegion(new CellRangeAddress(length, length, 1, 19));

            Row row = excelSheet.getRow(4);
            excelSheet.setColumnWidth(0, 1000);
            for (int i = 4; i <= 20; i++) {
                Cell cell = row.getCell(i);
                if (cell != null && !"".equals(cell.getStringCellValue().trim())) {
                    excelSheet.autoSizeColumn(i, true);
                } else {
                    excelSheet.setColumnWidth(i, 754);
                }
            }
        }
        //VNPT NGHEAN - HISHTROTGG-40962 - END
        else {
            Row rowSignature = excelSheet.createRow(length);
            Cell cellTruongKhoaDuoc = rowSignature.createCell(9 - tsdvKhoanCachChuKy);
            cellTruongKhoaDuoc.setCellValue("TRƯỞNG KHOA DƯỢC");
            cellTruongKhoaDuoc.setCellStyle(cellStyleAlignCenterLarge);

            Cell cellYTa = rowSignature.createCell(17 - tsdvKhoanCachChuKy);
            cellYTa.setCellValue("NGƯỜI PHÁT");
            cellYTa.setCellStyle(cellStyleAlignCenterLarge);

            Cell cellNGUOINHAN = rowSignature.createCell(26 - tsdvKhoanCachChuKy);
            cellNGUOINHAN.setCellValue("NGƯỜI NHẬN");
            cellNGUOINHAN.setCellStyle(cellStyleAlignCenterLarge);

            Cell cellTruongKhoa = rowSignature.createCell(34 - tsdvKhoanCachChuKy);
            cellTruongKhoa.setCellValue("TRƯỞNG KHOA");
            cellTruongKhoa.setCellStyle(cellStyleAlignCenterLarge);

            excelSheet.addMergedRegion(new CellRangeAddress(length, length, 1, 7- tsdvKhoanCachChuKy));
            excelSheet.addMergedRegion(new CellRangeAddress(length, length, 9- tsdvKhoanCachChuKy, 15- tsdvKhoanCachChuKy));
            excelSheet.addMergedRegion(new CellRangeAddress(length, length, 17- tsdvKhoanCachChuKy, 23- tsdvKhoanCachChuKy));
        }

    }

    public void setExcelHeader(Workbook workbook, Sheet excelSheet, Object[] arr_title, int startRowNumber) {
        Row excelHeaderGroup = excelSheet.createRow(startRowNumber - 1);
        Row excelHeader = excelSheet.createRow(startRowNumber);

        CellStyle columnHeadStyleBorderOnly = workbook.createCellStyle();
        columnHeadStyleBorderOnly.setBorderBottom(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderTop(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderRight(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderLeft(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setWrapText(true);

        CellStyle columnHeadStyle = workbook.createCellStyle();
        columnHeadStyle.setBorderBottom(CellStyle.BORDER_MEDIUM);
        columnHeadStyle.setBorderTop(CellStyle.BORDER_MEDIUM);
        columnHeadStyle.setBorderRight(CellStyle.BORDER_MEDIUM);
        columnHeadStyle.setBorderLeft(CellStyle.BORDER_MEDIUM);

        int i = 0;
        int length = arr_title.length;
        for (i = 0; i < 3; i++) {
            if (dvtt.indexOf("10") == 0) {
                Cell fixedCell = excelHeaderGroup.createCell(i + 1);
                fixedCell.setCellValue(arr_title[i].toString());
                fixedCell.setCellStyle(columnHeadStyleBorderOnly);

                Cell fixedCell2 = excelHeader.createCell(i + 1);
                fixedCell2.setCellStyle(columnHeadStyleBorderOnly);

                excelSheet.addMergedRegion(new CellRangeAddress(startRowNumber - 1, startRowNumber, i + 1, i + 1));

            } else {
                Cell fixedCell = excelHeader.createCell(i + 1);
                fixedCell.setCellValue(arr_title[i].toString());
                fixedCell.setCellStyle(columnHeadStyleBorderOnly);
            }

        }

        String pattern = "(\\(@){1}.*(@\\)){1}";
        int startMergeCell = -1;
        String currentGroup = "";
        for (i = 3; i < length; i++) {
            if (dvtt.indexOf("10") == 0) {

                Cell cell = excelHeader.createCell(i + 1);
                Cell cellGroup = excelHeaderGroup.createCell(i + 1);
                cellGroup.setCellStyle(columnHeadStyleBorderOnly);
                String groupName = "";
                String itemName = "";
                Pattern p = Pattern.compile(pattern);
                Matcher m = p.matcher(arr_title[i].toString());

                if (m.find()) {
                    groupName = m.group(0);
                    itemName = m.replaceFirst("");
                } else {
                    itemName = arr_title[i].toString();
                }

                if (currentGroup.equals(groupName)) {
                    if (i == length - 1) {
                        if (startMergeCell > -1) {
                            excelSheet.addMergedRegion(new CellRangeAddress(startRowNumber - 1, startRowNumber - 1, startMergeCell, i + 1));
                        }
                    }
                } else {
                    currentGroup = groupName;
                    String groupNameDisplay = groupName.replace("(@", "");
                    groupNameDisplay = groupNameDisplay.replace("@)", "");
                    if (startMergeCell == -1) {
                        startMergeCell = i + 1;
                        cellGroup.setCellValue(groupNameDisplay);
                    } else {
                        //excelSheet.addMergedRegion(new CellRangeAddress(startRowNumber - 1, startRowNumber - 1, startMergeCell, i + 1));
                        // không hiểu ý đồ của tác giả viết, bị lỗi ngay dòng này, nên comment lại để in ra report - khangtn.tgg.
                        cellGroup.setCellValue(groupNameDisplay);
                        startMergeCell = i + 1;
                    }
                }
                cell.setCellValue(itemName);
                short d = 90;
                columnHeadStyle.setRotation(d);
                cell.setCellStyle(columnHeadStyle);
            } else {
                Cell cell = excelHeader.createCell(i + 1);
                String itemName = "";
                Pattern p = Pattern.compile(pattern);
                Matcher m = p.matcher(arr_title[i].toString());

                if (m.find()) {
                    itemName = m.replaceFirst("");
                } else {
                    itemName = arr_title[i].toString();
                }

                cell.setCellValue(itemName);
                short d = 90;
                columnHeadStyle.setRotation(d);
                cell.setCellStyle(columnHeadStyle);

            }

        }
    }

    public void hpg_setExcelHeader(Workbook workbook, Sheet excelSheet, Object[] arr_title, int startRowNumber) {
        Row excelHeaderGroup = excelSheet.createRow(startRowNumber - 1);
        Row excelHeader = excelSheet.createRow(startRowNumber);

        CellStyle columnHeadStyleBorderOnly = workbook.createCellStyle();
        columnHeadStyleBorderOnly.setBorderBottom(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderTop(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderRight(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderLeft(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setWrapText(true);

        CellStyle columnHeadStyle = workbook.createCellStyle();
        columnHeadStyle.setBorderBottom(CellStyle.BORDER_MEDIUM);
        columnHeadStyle.setBorderTop(CellStyle.BORDER_MEDIUM);
        columnHeadStyle.setBorderRight(CellStyle.BORDER_MEDIUM);
        columnHeadStyle.setBorderLeft(CellStyle.BORDER_MEDIUM);

        int i = 0;
        int length = arr_title.length;
        for (i = 0; i < 2; i++) {
            Cell fixedCell = excelHeader.createCell(i + 1);
            fixedCell.setCellValue(arr_title[i].toString());
            fixedCell.setCellStyle(columnHeadStyleBorderOnly);

        }

        String pattern = "(\\(@){1}.*(@\\)){1}";
        int startMergeCell = -1;
        String currentGroup = "";
        for (i = 2; i < length; i++) {
            Cell cell = excelHeader.createCell(i + 1);
            String itemName = "";
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(arr_title[i].toString());

            if (m.find()) {
                itemName = m.replaceFirst("");
            } else {
                itemName = arr_title[i].toString();
            }

            cell.setCellValue(itemName);
            short d = 90;
            columnHeadStyle.setRotation(d);
            cell.setCellStyle(columnHeadStyle);

        }
    }

    public void setExcelRows(Workbook workbook, Sheet excelSheet, List<Map<String, Object>> list, Object[] arr_title, int startRowNumber) {
        int record = startRowNumber;
        int i = 0;
        int length = arr_title.length;
        arr_title = list.get(0).keySet().toArray();
        list.remove(0);
        CellStyle columnHeadStyleBorderOnly = workbook.createCellStyle();
        columnHeadStyleBorderOnly.setBorderBottom(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderTop(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderRight(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderLeft(CellStyle.BORDER_MEDIUM);

        for (Map m : list) {
            Row excelRow = excelSheet.createRow(record++);
            for (i = 0; i < length; i++) {
                Cell cell = excelRow.createCell(i + 1);
                cell.setCellValue((m.get(arr_title[i]) + "").equals("0") ? "" : (m.get(arr_title[i]) + ""));

                cell.setCellStyle(columnHeadStyleBorderOnly);
            }
        }
        for (int j = 0; j < length; j++) {
            excelSheet.autoSizeColumn(j + 1, true);
        }
    }

    //vtu: 22/10/2016
    //DUNG CHO BAO CAO CO XUAT DONG TONG O DUOI
    public void setExcelRows_Tong(Workbook workbook, Sheet excelSheet, List<Map<String, Object>> list, Object[] arr_title, int startRowNumber) {
        int record = startRowNumber;
        int i = 0;
        int length = arr_title.length;
        arr_title = list.get(0).keySet().toArray();
        list.remove(0);
        CellStyle columnHeadStyleBorderOnly = workbook.createCellStyle();
        columnHeadStyleBorderOnly.setBorderBottom(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderTop(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderRight(CellStyle.BORDER_MEDIUM);
        columnHeadStyleBorderOnly.setBorderLeft(CellStyle.BORDER_MEDIUM);

        //int[] arrTong=new int[]();
        int[] arrTong = new int[length];
        int nurec = list.size();

        for (Map m : list) {
            Row excelRow = excelSheet.createRow(record++);
            for (i = 0; i < length; i++) {
                Cell cell = excelRow.createCell(i + 1);
                cell.setCellValue((m.get(arr_title[i]) + "").equals("0") ? "" : (m.get(arr_title[i]) + ""));

                cell.setCellStyle(columnHeadStyleBorderOnly);

                //VTU: Luu tong
                if (i >= 3) {
                    arrTong[i] = arrTong[i] + Integer.parseInt(m.get(arr_title[i]) + "");
                }
            }
        }
        for (int j = 0; j < length; j++) {
            excelSheet.autoSizeColumn(j + 1, true);
        }

        //VTU: Khuc nay tao record cuoi de tinh tong
        Row excelRow = excelSheet.createRow(record++);
        for (i = 3; i < length; i++) {
            Cell cell = excelRow.createCell(i + 1);
            cell.setCellValue(arrTong[i] + "");

            cell.setCellStyle(columnHeadStyleBorderOnly);

        }
    }
}
