package com.vnpt.duoc.duocModule.objects.chuyenkho;

public class ChiTietChuyenKhoObj {
    private Double donGia;
    private Double donGiaDV;
    private long duTruDVQL;
    private long duyet;
    private String ghiChu;
    private long idChuyenKhoVT;
    private long idChuyenKhoVTCT;
    private long idGiaoDich;
    private long idSoPhieuChuyen;
    private String keyQL;
    private long keyQLID;
    private long keyQLIDNhan;
    private long lanChuyenSoNhapKhoCT;
    private String maChuyenKhoVatTu;
    private String maDonViGiao;
    private String maDonViNhan;
    private String maDonViTao;
    private long maKhoNhan;
    private long maKhoGiao;
    private long maNghiepVuChuyen;
    private long maVatTu;
    private String ngayChuyen;
    private String ngayDuyetChuyenDi;
    private String ngayHetHan;
    private String ngayNhanChuyenDen;
    private String ngaySanXuat;
    private String ngayXuLy;
    private long nguoiNhap;
    private String soLoSanXuat;
    private Double soLuongDuocDuyet;
    private long soNhapKhoChiTiet;
    private String soPhieuChuyen;
    private Double soLuong;
    private String tenVatTu;
    private Double thanhTien;
    private String hoatChat;
    private String dvt;
    private String soThau;

    public Double getDonGia() {
        return donGia;
    }

    public void setDonGia(Double donGia) {
        this.donGia = donGia;
    }

    public Double getDonGiaDV() {
        return donGiaDV;
    }

    public void setDonGiaDV(Double donGiaDV) {
        this.donGiaDV = donGiaDV;
    }

    public long getDuTruDVQL() {
        return duTruDVQL;
    }

    public void setDuTruDVQL(long duTruDVQL) {
        this.duTruDVQL = duTruDVQL;
    }

    public long getDuyet() {
        return duyet;
    }

    public void setDuyet(long duyet) {
        this.duyet = duyet;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public long getIdChuyenKhoVT() {
        return idChuyenKhoVT;
    }

    public void setIdChuyenKhoVT(long idChuyenKhoVT) {
        this.idChuyenKhoVT = idChuyenKhoVT;
    }

    public long getIdChuyenKhoVTCT() {
        return idChuyenKhoVTCT;
    }

    public void setIdChuyenKhoVTCT(long idChuyenKhoVTCT) {
        this.idChuyenKhoVTCT = idChuyenKhoVTCT;
    }

    public long getIdGiaoDich() {
        return idGiaoDich;
    }

    public void setIdGiaoDich(long idGiaoDich) {
        this.idGiaoDich = idGiaoDich;
    }

    public long getIdSoPhieuChuyen() {
        return idSoPhieuChuyen;
    }

    public void setIdSoPhieuChuyen(long idSoPhieuChuyen) {
        this.idSoPhieuChuyen = idSoPhieuChuyen;
    }

    public String getKeyQL() {
        return keyQL;
    }

    public void setKeyQL(String keyQL) {
        this.keyQL = keyQL;
    }

    public long getKeyQLID() {
        return keyQLID;
    }

    public void setKeyQLID(long keyQLID) {
        this.keyQLID = keyQLID;
    }

    public long getKeyQLIDNhan() {
        return keyQLIDNhan;
    }

    public void setKeyQLIDNhan(long keyQLIDNhan) {
        this.keyQLIDNhan = keyQLIDNhan;
    }

    public long getLanChuyenSoNhapKhoCT() {
        return lanChuyenSoNhapKhoCT;
    }

    public void setLanChuyenSoNhapKhoCT(long lanChuyenSoNhapKhoCT) {
        this.lanChuyenSoNhapKhoCT = lanChuyenSoNhapKhoCT;
    }

    public String getMaChuyenKhoVatTu() {
        return maChuyenKhoVatTu;
    }

    public void setMaChuyenKhoVatTu(String maChuyenKhoVatTu) {
        this.maChuyenKhoVatTu = maChuyenKhoVatTu;
    }

    public String getMaDonViGiao() {
        return maDonViGiao;
    }

    public void setMaDonViGiao(String maDonViGiao) {
        this.maDonViGiao = maDonViGiao;
    }

    public String getMaDonViNhan() {
        return maDonViNhan;
    }

    public void setMaDonViNhan(String maDonViNhan) {
        this.maDonViNhan = maDonViNhan;
    }

    public String getMaDonViTao() {
        return maDonViTao;
    }

    public void setMaDonViTao(String maDonViTao) {
        this.maDonViTao = maDonViTao;
    }

    public long getMaKhoNhan() {
        return maKhoNhan;
    }

    public void setMaKhoNhan(long maKhoNhan) {
        this.maKhoNhan = maKhoNhan;
    }

    public long getMaKhoGiao() {
        return maKhoGiao;
    }

    public void setMaKhoGiao(long maKhoGiao) {
        this.maKhoGiao = maKhoGiao;
    }

    public long getMaNghiepVuChuyen() {
        return maNghiepVuChuyen;
    }

    public void setMaNghiepVuChuyen(long maNghiepVuChuyen) {
        this.maNghiepVuChuyen = maNghiepVuChuyen;
    }

    public long getMaVatTu() {
        return maVatTu;
    }

    public void setMaVatTu(long maVatTu) {
        this.maVatTu = maVatTu;
    }

    public String getNgayChuyen() {
        return ngayChuyen;
    }

    public void setNgayChuyen(String ngayChuyen) {
        this.ngayChuyen = ngayChuyen;
    }

    public String getNgayDuyetChuyenDi() {
        return ngayDuyetChuyenDi;
    }

    public void setNgayDuyetChuyenDi(String ngayDuyetChuyenDi) {
        this.ngayDuyetChuyenDi = ngayDuyetChuyenDi;
    }

    public String getNgayHetHan() {
        return ngayHetHan;
    }

    public void setNgayHetHan(String ngayHetHan) {
        this.ngayHetHan = ngayHetHan;
    }

    public String getNgayNhanChuyenDen() {
        return ngayNhanChuyenDen;
    }

    public void setNgayNhanChuyenDen(String ngayNhanChuyenDen) {
        this.ngayNhanChuyenDen = ngayNhanChuyenDen;
    }

    public String getNgaySanXuat() {
        return ngaySanXuat;
    }

    public void setNgaySanXuat(String ngaySanXuat) {
        this.ngaySanXuat = ngaySanXuat;
    }

    public String getNgayXuLy() {
        return ngayXuLy;
    }

    public void setNgayXuLy(String ngayXuLy) {
        this.ngayXuLy = ngayXuLy;
    }

    public long getNguoiNhap() {
        return nguoiNhap;
    }

    public void setNguoiNhap(long nguoiNhap) {
        this.nguoiNhap = nguoiNhap;
    }

    public String getSoLoSanXuat() {
        return soLoSanXuat;
    }

    public void setSoLoSanXuat(String soLoSanXuat) {
        this.soLoSanXuat = soLoSanXuat;
    }

    public Double getSoLuongDuocDuyet() {
        return soLuongDuocDuyet;
    }

    public void setSoLuongDuocDuyet(Double soLuongDuocDuyet) {
        this.soLuongDuocDuyet = soLuongDuocDuyet;
    }

    public long getSoNhapKhoChiTiet() {
        return soNhapKhoChiTiet;
    }

    public void setSoNhapKhoChiTiet(long soNhapKhoChiTiet) {
        this.soNhapKhoChiTiet = soNhapKhoChiTiet;
    }

    public String getSoPhieuChuyen() {
        return soPhieuChuyen;
    }

    public void setSoPhieuChuyen(String soPhieuChuyen) {
        this.soPhieuChuyen = soPhieuChuyen;
    }

    public Double getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Double soLuong) {
        this.soLuong = soLuong;
    }

    public String getTenVatTu() {
        return tenVatTu;
    }

    public void setTenVatTu(String tenVatTu) {
        this.tenVatTu = tenVatTu;
    }

    public Double getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(Double thanhTien) {
        this.thanhTien = thanhTien;
    }

    public String getHoatChat() {
        return hoatChat;
    }

    public void setHoatChat(String hoatChat) {
        this.hoatChat = hoatChat;
    }

    public String getDvt() {
        return dvt;
    }

    public void setDvt(String dvt) {
        this.dvt = dvt;
    }

    public String getSoThau() {
        return soThau;
    }

    public void setSoThau(String soThau) {
        this.soThau = soThau;
    }
}
