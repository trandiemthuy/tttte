package com.vnpt.duoc.duocModule;

public class DuocConfigs {
    public static final String reportURL = "reports/duoc/";
    public static final String ERROR_TITLE_FILENOTFOUND_EXCEPTIONS = "Không tìm thấy tập tin";
    public static final String ERROR_TITLE_EXCEPTIONS = "Có gì đó không ổn. :(";
}
