package com.vnpt.duoc.duocModule.dao.duyetphieu;

import java.util.Date;
import java.util.List;

public interface DuyetPhieuDAO {
    List getDanhSachKhoChuyenTheoNhanVien(String dvtt, String maNhanVien);
    List getDanhSachPhieuChuyenTheoNghiepVu(Date tuNgay, Date denNgay, long maNghiepVu, long maKho, long trangThai, String dvtt);

    // Duyệt
    int updateSoLuongDuyet(String dvtt, long idChuyenKhoChiTiet, Double soLuongYeuCau, Double soLuongDuyet, String idGiaoDich, long maUser, String maKhoa, String maPhong);
    int duyetPhieuChuyenKho(String dvtt, long idPhieuChuyen, Date ngayDuyet, long nguoiDuyet, String idGiaoDich, String maKhoa, String maPhong);
    int huyDuyetChuyenKho(String dvtt, long idPhieuChuyen, long nguoiDuyet, String idGiaoDich, String maKhoa, String maPhong);

    // Print
    List getDanhSachVatTuChiTietPhieuChuyen(String dvtt, long idPhieuChuyen);
    List getDanhSachVatTuPhieuChuyen(String dvtt, Date tuNgay, Date denNgay, long maKho, long trangThai);
}
