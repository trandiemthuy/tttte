package com.vnpt.duoc.duocModule.controller.nhanduoc;

import com.vnpt.duoc.duocModule.DuocConfigs;
import com.vnpt.duoc.duocModule.DuocTransaction;
import com.vnpt.duoc.duocModule.DuocUtils;
import com.vnpt.duoc.duocModule.dao.chuyenkho.ChuyenKhoDAO;
import com.vnpt.duoc.duocModule.dao.hethong.DocSo;
import com.vnpt.duoc.duocModule.dao.hethong.HeThongDAO;
import com.vnpt.duoc.duocModule.dao.nhanduoc.NhanDuocDAO;
import com.vnpt.duoc.jasper.ExportXLSView;
import com.vnpt.duoc.jasper.JasperHelper;
import com.zaxxer.hikari.HikariDataSource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.net.InetAddress;
import java.time.MonthDay;
import java.time.YearMonth;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/nhanduoc")
@Api(value="NhanDuocController")
public class NhanDuocController {
    private final NhanDuocDAO nhanDuocDAO;
    private final ChuyenKhoDAO chuyenKhoDAO;
    private final HeThongDAO heThongDAO;
    private final HikariDataSource dataSourceDuocV2;

    public NhanDuocController(NhanDuocDAO nhanDuocDAO, ChuyenKhoDAO chuyenKhoDAO, HeThongDAO heThongDAO, HikariDataSource dataSourceDuocV2) {
        this.nhanDuocDAO = nhanDuocDAO;
        this.chuyenKhoDAO = chuyenKhoDAO;
        this.heThongDAO = heThongDAO;
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    @ResponseBody
    @RequestMapping(value = "/getDanhSachPhieuNhanDuocVeKho", method = RequestMethod.GET)
    public List getDanhSachPhieuNhanDuocVeKho(@RequestParam("dvtt") String dvtt,
                                          @RequestParam("tuNgay") String tuNgay,
                                          @RequestParam("denNgay") String denNgay,
                                          @RequestParam("maNghiepVu") int maNghiepVu,
                                          @RequestParam("maKho") int maKho,
                                          @RequestParam("duyet") int duyet,
                                          @RequestParam("maPhongBan") String maPhongBan,
                                          HttpSession session) {
        try {
            return nhanDuocDAO.getDanhSachPhieuNhanDuocVeKho(DuocUtils.convertStringToDate(tuNgay, "dd/MM/yyyy"),
                DuocUtils.convertStringToDate(denNgay, "dd/MM/yyyy"), dvtt, duyet, maKho,
                maPhongBan, maNghiepVu);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ResponseBody
    @RequestMapping(value = "/nhanDuocVeKho", method = RequestMethod.POST)
    public int nhanDuocVeKho(@RequestParam("dvtt") String dvtt,
                             @RequestParam("maNghiepVu") int maNghiepVu,
                             @RequestParam("idChuyenKho") long idChuyenKho,
                             @RequestParam("maKhoGiao") int maKhoGiao,
                             @RequestParam("maKhoNhan") int maKhoNhan,
                             @RequestParam("ngayNhan") String ngayNhan,
                             @RequestParam("nguoiNhan") long nguoiNhan,
                             @RequestParam("maPhongBan") String maPhongBan,
                             @RequestParam("maPhongBenh") String maPhongBenh,
                             HttpSession session,
                             HttpServletResponse response,
                             HttpServletRequest request) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();

            String idGiaoDich = DuocTransaction.insert(0, 19, "Nhận dược về kho",
                nguoiNhan, dvtt, 0, clientIP, hostIP, "", "nhanDuocVeKho", dataSourceDuocV2);
            return nhanDuocDAO.nhanDuocVeKho(maNghiepVu, dvtt, idChuyenKho, maKhoGiao, maKhoNhan, DuocUtils.convertStringToDate(ngayNhan, "dd/MM/yyyy"), nguoiNhan, idGiaoDich, maPhongBan, maPhongBenh);
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/huyNhanDuocVeKho", method = RequestMethod.POST)
    public int huyNhanDuocVeKho(@RequestParam("dvtt") String dvtt,
                                @RequestParam("idChuyenKho") long idChuyenKho,
                                @RequestParam("nguoiHuy") long nguoiHuy,
                                @RequestParam("maPhongBan") String maPhongBan,
                                @RequestParam("maPhongBenh") String maPhongBenh,
                                HttpSession session,
                                HttpServletResponse response,
                                HttpServletRequest request) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 20, "Hủy nhận dược về kho",
                nguoiHuy, dvtt, 0, clientIP, hostIP, "", "huyNhanDuocVeKho", dataSourceDuocV2);
            return nhanDuocDAO.huyNhanDuocVeKho(idChuyenKho, dvtt, nguoiHuy, idGiaoDich, maPhongBan, maPhongBenh);
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    @ApiOperation("Xuất dữ liệu Excel nhận dược")
    @RequestMapping(value = "/exportExcelNhanDuoc", method = RequestMethod.GET)
    public ModelAndView exportExcelNhanDuoc(@RequestParam("dvtt") String dvtt,
                                            @RequestParam("tuNgay") String tuNgay,
                                            @RequestParam("denNgay") String denNgay,
                                            @RequestParam("maNghiepVu") int maNghiepVu,
                                            @RequestParam("maKho") int maKho,
                                            @RequestParam("duyet") int duyet,
                                            @RequestParam("maPhongBan") String maPhongBan,
                                            HttpServletRequest request) {
        try {
            List list = nhanDuocDAO.getDanhSachPhieuNhanDuocVeKho(DuocUtils.convertStringToDate(tuNgay, "dd/MM/yyyy"),
                DuocUtils.convertStringToDate(denNgay, "dd/MM/yyyy"), dvtt, duyet, maKho,
                maPhongBan, maNghiepVu);
            ExportXLSView.sheetName = "DSvattu";
            ExportXLSView.fileName = "dsvattu.xls";
            return new ModelAndView(new ExportXLSView(), "report_view", list);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ApiOperation("Xuất dữ liệu Excel nhận dược")
    @ResponseBody
    @RequestMapping(value = "/printBienBanKiemNhapNhanDuoc", method = RequestMethod.GET)
    public void printBienBanKiemNhapNhanDuoc(@RequestParam(value = "dvtt") String dvtt,
                                             @RequestParam(value = "idChuyenKho") long idChuyenKho,
                                             @RequestParam(value = "maPhongBan") String maPhongBan,
                                             @RequestParam(value = "nguoiLap") long nguoiLap,
                                             @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                             @RequestParam(value = "tenTinh") String tenTinh,
                                             @RequestParam(value = "loaiFile") String loaiFile,
                                             HttpSession session,
                                             HttpServletResponse response,
                                             HttpServletRequest request) {
        try {
            String tenbenhvien = tenBenhVien.toUpperCase();
            Map m = chuyenKhoDAO.getThongTinPhieuChuyenKho(dvtt, idChuyenKho, 0);
            String tenPhongBan = heThongDAO.getTenPhongBan(maPhongBan);

            Map parameters = new HashMap();
            parameters.put("dvtt", dvtt);
            parameters.put("idPhieuChuyen", idChuyenKho);
            parameters.put("so_phieu_chuyen", m.get("SoPhieuChuyen").toString());
            parameters.put("kho_giao", m.get("Khogiao").toString());
            parameters.put("kho_nhan", m.get("Khonhan").toString());
            parameters.put("ten_dvtt", tenbenhvien);
            parameters.put("ten_khoa", tenPhongBan);
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            Double tongTienPhieu = chuyenKhoDAO.getTongTienPhieuChuyenKho(idChuyenKho);
            parameters.put("soyte", donviquanlytructiep);
            parameters.put("ngayin", "Ngày " + MonthDay.now().getDayOfMonth() + " tháng " + MonthDay.now().getMonthValue() + " năm " + YearMonth.now().getYear());
            parameters.put("tong_tien_bang_chu", new DocSo().docso(tongTienPhieu));
            parameters.put("sophieu", m.get("SoPhieuChuyen").toString());
            parameters.put("tenbenhvien", tenbenhvien);
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("xuattukho", m.get("Khogiao").toString());
            parameters.put("xuatdenkho", m.get("Khonhan").toString());
            parameters.put("soluutru", m.get("SoluuTru").toString());
            parameters.put("sotienbangchu", new DocSo().docso(tongTienPhieu));
            parameters.put("dvtt", dvtt);
            parameters.put("ngay", MonthDay.now().getDayOfMonth() + "");
            parameters.put("thang", MonthDay.now().getMonthValue() + "");
            parameters.put("nam", YearMonth.now().getYear() + "");
            parameters.put("nguoilapbang", nguoiLap);
            File reportFile;
            String reportName = "rp_bbkiemnhap_ts89074.jasper";
            reportFile = new ClassPathResource(DuocConfigs.reportURL + "nhanduoc/" + reportName).getFile();
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
