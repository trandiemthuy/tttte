package com.vnpt.duoc.duocModule.objects.chuyenkho;

import com.vnpt.duoc.duocModule.DuocUtils;
import net.minidev.json.JSONObject;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.Date;

public class ChuyenKhoObj {
    private long duTruCSTT;
    private long duTruDVQL;
    private long duyet;
    private String ghiChu;
    private String goiKhoa;
    private String idChuyenCha;
    private long idChuyenKhoVatTu;
    private long idGiaoDich;
    private String maDonViGiao;
    private String maDonViNhan;
    private String maDonViTao;
    private long maKhoGiao;
    private long maKhoNhan;
    private long maNghiepVuChuyen;
    private long maNguoiDuyet;
    private long maNguoiGiao;
    private long maNguoiNhan;
    private long maNguoiTao;
    private String maPhongBanTao;
    private Date ngayPhieuChuyen;
    private Date ngayChuyen;
    private String ngayDuyet;
    private String ngayDuyetChuyenDi;
    private String ngayGioChuyen;
    private String ngayGioDuyet;
    private String ngayGioDuyetChuyenDi;
    private String ngayGioNhanChuyenDen;
    private String ngayGioTao;
    private String ngayNhanChuyenDen;
    private String ngayTao;
    private String nghiepVu;
    private String soLuuTru;
    private String soPhieuChuyen;
    private String soPhieuNhap;
    private long ttYC;
    private long xuatHuy; // 0: Xuất thực 1: Xuất hủy 2: Xuất nhượng 3: Xuất điều trị
    private String diaChi;

    public ChuyenKhoObj (JSONObject jsonObject, HttpSession session, String idGiaoDich) throws ParseException {
        this.idChuyenKhoVatTu = jsonObject.get("idChuyenKhoVatTu") == null ? 0 : Long.parseLong(jsonObject.get("idChuyenKhoVatTu").toString());
        this.maDonViTao = jsonObject.get("maDonViTao").toString();
        this.maNghiepVuChuyen = Long.parseLong(jsonObject.get("maNghiepVuChuyen").toString());
        this.nghiepVu = jsonObject.get("nghiepVu").toString();
        this.ngayPhieuChuyen = DuocUtils.convertStringToDate(jsonObject.get("ngayPhieuChuyen").toString(), "dd/MM/yyyy");
        this.ghiChu = jsonObject.get("ghiChu").toString();
        this.ngayChuyen = DuocUtils.convertStringToDate(jsonObject.get("ngayChuyen").toString(), "dd/MM/yyyy");
        this.maDonViNhan = jsonObject.get("maDonViNhan").toString();
        this.maDonViGiao = jsonObject.get("maDonViGiao").toString();
        this.maNguoiTao = Long.parseLong(jsonObject.get("maNguoiTao").toString());
        this.maPhongBanTao = jsonObject.get("maPhongBanTao").toString();
        this.diaChi = jsonObject.get("diaChi").toString();
        this.duTruCSTT = Integer.parseInt(jsonObject.get("duTruCSTT").toString());
        this.maKhoGiao = Integer.parseInt(jsonObject.get("maKhoGiao").toString());
        this.maKhoNhan = Integer.parseInt(jsonObject.get("maKhoNhan").toString());
        this.idGiaoDich = Long.parseLong(idGiaoDich);
        this.xuatHuy = Integer.parseInt(jsonObject.get("xuatHuy").toString());
    }

    public long getDuTruCSTT() {
        return duTruCSTT;
    }

    public void setDuTruCSTT(long duTruCSTT) {
        this.duTruCSTT = duTruCSTT;
    }

    public long getDuTruDVQL() {
        return duTruDVQL;
    }

    public void setDuTruDVQL(long duTruDVQL) {
        this.duTruDVQL = duTruDVQL;
    }

    public long getDuyet() {
        return duyet;
    }

    public void setDuyet(long duyet) {
        this.duyet = duyet;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getGoiKhoa() {
        return goiKhoa;
    }

    public void setGoiKhoa(String goiKhoa) {
        this.goiKhoa = goiKhoa;
    }

    public String getIdChuyenCha() {
        return idChuyenCha;
    }

    public void setIdChuyenCha(String idChuyenCha) {
        this.idChuyenCha = idChuyenCha;
    }

    public long getIdChuyenKhoVatTu() {
        return idChuyenKhoVatTu;
    }

    public void setIdChuyenKhoVatTu(long idChuyenKhoVatTu) {
        this.idChuyenKhoVatTu = idChuyenKhoVatTu;
    }

    public long getIdGiaoDich() {
        return idGiaoDich;
    }

    public void setIdGiaoDich(long idGiaoDich) {
        this.idGiaoDich = idGiaoDich;
    }

    public String getMaDonViGiao() {
        return maDonViGiao;
    }

    public void setMaDonViGiao(String maDonViGiao) {
        this.maDonViGiao = maDonViGiao;
    }

    public String getMaDonViNhan() {
        return maDonViNhan;
    }

    public void setMaDonViNhan(String maDonViNhan) {
        this.maDonViNhan = maDonViNhan;
    }

    public String getMaDonViTao() {
        return maDonViTao;
    }

    public void setMaDonViTao(String maDonViTao) {
        this.maDonViTao = maDonViTao;
    }

    public long getMaKhoGiao() {
        return maKhoGiao;
    }

    public void setMaKhoGiao(long maKhoGiao) {
        this.maKhoGiao = maKhoGiao;
    }

    public long getMaKhoNhan() {
        return maKhoNhan;
    }

    public void setMaKhoNhan(long maKhoNhan) {
        this.maKhoNhan = maKhoNhan;
    }

    public long getMaNghiepVuChuyen() {
        return maNghiepVuChuyen;
    }

    public void setMaNghiepVuChuyen(long maNghiepVuChuyen) {
        this.maNghiepVuChuyen = maNghiepVuChuyen;
    }

    public long getMaNguoiDuyet() {
        return maNguoiDuyet;
    }

    public void setMaNguoiDuyet(long maNguoiDuyet) {
        this.maNguoiDuyet = maNguoiDuyet;
    }

    public long getMaNguoiGiao() {
        return maNguoiGiao;
    }

    public void setMaNguoiGiao(long maNguoiGiao) {
        this.maNguoiGiao = maNguoiGiao;
    }

    public long getMaNguoiNhan() {
        return maNguoiNhan;
    }

    public void setMaNguoiNhan(long maNguoiNhan) {
        this.maNguoiNhan = maNguoiNhan;
    }

    public long getMaNguoiTao() {
        return maNguoiTao;
    }

    public void setMaNguoiTao(long maNguoiTao) {
        this.maNguoiTao = maNguoiTao;
    }

    public String getMaPhongBanTao() {
        return maPhongBanTao;
    }

    public void setMaPhongBanTao(String maPhongBanTao) {
        this.maPhongBanTao = maPhongBanTao;
    }

    public Date getNgayPhieuChuyen() {
        return ngayPhieuChuyen;
    }

    public void setNgayPhieuChuyen(Date ngayPhieuChuyen) {
        this.ngayPhieuChuyen = ngayPhieuChuyen;
    }

    public Date getNgayChuyen() {
        return ngayChuyen;
    }

    public void setNgayChuyen(Date ngayChuyen) {
        this.ngayChuyen = ngayChuyen;
    }

    public String getNgayDuyet() {
        return ngayDuyet;
    }

    public void setNgayDuyet(String ngayDuyet) {
        this.ngayDuyet = ngayDuyet;
    }

    public String getNgayDuyetChuyenDi() {
        return ngayDuyetChuyenDi;
    }

    public void setNgayDuyetChuyenDi(String ngayDuyetChuyenDi) {
        this.ngayDuyetChuyenDi = ngayDuyetChuyenDi;
    }

    public String getNgayGioChuyen() {
        return ngayGioChuyen;
    }

    public void setNgayGioChuyen(String ngayGioChuyen) {
        this.ngayGioChuyen = ngayGioChuyen;
    }

    public String getNgayGioDuyet() {
        return ngayGioDuyet;
    }

    public void setNgayGioDuyet(String ngayGioDuyet) {
        this.ngayGioDuyet = ngayGioDuyet;
    }

    public String getNgayGioDuyetChuyenDi() {
        return ngayGioDuyetChuyenDi;
    }

    public void setNgayGioDuyetChuyenDi(String ngayGioDuyetChuyenDi) {
        this.ngayGioDuyetChuyenDi = ngayGioDuyetChuyenDi;
    }

    public String getNgayGioNhanChuyenDen() {
        return ngayGioNhanChuyenDen;
    }

    public void setNgayGioNhanChuyenDen(String ngayGioNhanChuyenDen) {
        this.ngayGioNhanChuyenDen = ngayGioNhanChuyenDen;
    }

    public String getNgayGioTao() {
        return ngayGioTao;
    }

    public void setNgayGioTao(String ngayGioTao) {
        this.ngayGioTao = ngayGioTao;
    }

    public String getNgayNhanChuyenDen() {
        return ngayNhanChuyenDen;
    }

    public void setNgayNhanChuyenDen(String ngayNhanChuyenDen) {
        this.ngayNhanChuyenDen = ngayNhanChuyenDen;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNghiepVu() {
        return nghiepVu;
    }

    public void setNghiepVu(String nghiepVu) {
        this.nghiepVu = nghiepVu;
    }

    public String getSoLuuTru() {
        return soLuuTru;
    }

    public void setSoLuuTru(String soLuuTru) {
        this.soLuuTru = soLuuTru;
    }

    public String getSoPhieuChuyen() {
        return soPhieuChuyen;
    }

    public void setSoPhieuChuyen(String soPhieuChuyen) {
        this.soPhieuChuyen = soPhieuChuyen;
    }

    public String getSoPhieuNhap() {
        return soPhieuNhap;
    }

    public void setSoPhieuNhap(String soPhieuNhap) {
        this.soPhieuNhap = soPhieuNhap;
    }

    public long getTtYC() {
        return ttYC;
    }

    public void setTtYC(long ttYC) {
        this.ttYC = ttYC;
    }

    public long getXuatHuy() {
        return xuatHuy;
    }

    public void setXuatHuy(long xuatHuy) {
        this.xuatHuy = xuatHuy;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }
}
