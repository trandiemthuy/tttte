package com.vnpt.duoc.duocModule.dao.khamchuabenh;

import com.vnpt.duoc.duocModule.objects.khamchuabenh.ToaThuocNgoaiTruObj;
import com.vnpt.duoc.duocModule.objects.khamchuabenh.ToaThuocNoiTruObj;

import java.util.Date;
import java.util.List;

public interface KcbDAO {
    int getSoLuongTonKho(String dvtt, // Đơn vị trực thuộc
                         int maKho, // Mã kho vật tư
                         String searchTerm, // Từ khóa cần tìm
                         int loaiTimKiem); // Loại tìm kiếm: 0: Theo tên    1: theo hoạt chất
    List getDanhSachDuocTonKho(String dvtt, // đơn vị trực thuộc
                               int loaiTimKiem, // 0: Tìm theo tên vật tư        1: Tìm theo hoạt chất        Default: 0
                               int loaiKhamBenh, // 0: Khám bệnh ngoại trú      1: Khám bệnh nội trú          2: BANT
                               String maLoaiVatTu, // Mã loại vật tư
                               int maNhomVatTu, // Mã nhóm vật tư
                               int maKho, // Mã kho vật tư
                               String searchTerm, // Từ khóa cần tìm
                               int start, // Bắt đầu
                               int limit, // Giới hạn record một trang
                               int totalPage, // Tổng số trang
                               String hienThiDonGiaKhiGo, // Hiển thị đơn giá khi gõ (lấy từ tham số)
                               String cauHinhPBGoThuoc, // Cấu hình gõ thuốc
                               String maPhongBan); // Mã phòng ban
    int addTachToaDichVuNoiTru(ToaThuocNoiTruObj toaThuocNoiTruObj);
    int addToaThuocNoiTru(ToaThuocNoiTruObj toaThuocNoiTruObj);
    List getDanhSachChiTietToaThuocNoiTru(String dvtt, // Đơn vị trực thuộc
                                          String maToaThuoc, // Mã toa thuốc
                                          String nghiepVu, // nghiệp vụ
                                          String sttBenhAn, // Số thứ tự bệnh án
                                          String sttDotDieuTri, // Số thứ tự đợt điều trị
                                          String soVaoVien, // Số vào viện
                                          String soVaoVienDT, // Số vào viện điều trị
                                          String soPhieu, // Số phiếu
                                          String ma, // Mã dịch vụ, mã chuẩn đoán hình ảnh, mã xét nghiệm, ....
                                          String theoDV); // Theo dịch vụ

    /* Ngoại trú*/
    String getSoPhieuThanhToan(String maKhamBenh, String dvtt, String dieuTriChuyenKhoa, int soVaoVien);
    String getDaThanhToanNgoaiTru(String dvtt, Date ngayRaToa, String maBenhNhan, String maKhamBenh, int soVaoVien);
    String getKiemTraXuatThuoc(String dvtt, String maToaThuoc, String nghiepVu, Date ngayRaToa, String maBenhNhan, int soVaoVien);
    String getKiemTraTrungThuocNgoaiTru(String dvtt, String maToaThuoc, String nghiepVu, Date ngayRaToa, String maBenhNhan, int maVatTu, int soVaoVien);
    int addTachToaDichVuNgoaiTru(ToaThuocNgoaiTruObj toaThuocNgoaiTruObj);
    int addToaThuocNgoaiTru(ToaThuocNgoaiTruObj toaThuocNgoaiTruObj);
    List getDanhSachChiTietToaThuocNgoaiTru(String maToaThuoc, // Mã toa thuốc
                                            String dvtt, // Đơn vị trực thuộc
                                            String nghiepVu, // Nghiệp vụ
                                            String soVaoVien, // Số vào viện
                                            String ma, // Mã dịch vụ, mã chuẩn đoán hình ảnh, mã xét nghiệm, ....
                                            String soPhieu, // Số phiếu
                                            String theoDV, // theo dịch vụ
                                            String soPhieuTTPT, // Số phiếu TTPT
                                            String maTTPT); // Mã TTPT
}
