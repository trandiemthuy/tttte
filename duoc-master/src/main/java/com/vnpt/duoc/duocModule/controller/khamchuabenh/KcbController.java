package com.vnpt.duoc.duocModule.controller.khamchuabenh;

import com.vnpt.duoc.duocModule.DuocTransaction;
import com.vnpt.duoc.duocModule.dao.hethong.HeThongDAO;
import com.vnpt.duoc.duocModule.dao.khamchuabenh.KcbDAO;
import com.vnpt.duoc.duocModule.objects.khamchuabenh.ToaThuocNgoaiTruObj;
import com.vnpt.duoc.duocModule.objects.khamchuabenh.ToaThuocNoiTruObj;
import com.zaxxer.hikari.HikariDataSource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/duoc")
@Api(value="KcbController")
public class KcbController {
    private final KcbDAO kcbDAO;
    private final HeThongDAO heThongDAO;
    private final HikariDataSource dataSourceDuocV2;

    public KcbController(KcbDAO kcbDAO, HeThongDAO heThongDAO, HikariDataSource dataSourceDuocV2) {
        this.kcbDAO = kcbDAO;
        this.heThongDAO = heThongDAO;
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    /* Nội trú */

    @ApiOperation("Lấy danh sách tồn kho vật tư")
    @RequestMapping(value="/getDanhSachDuocTonKho", method = RequestMethod.GET)
    public List getDanhSachDuocTonKho(@RequestParam(value = "dvtt") String dvtt,
                                      @RequestParam(value = "page") int page, // Trang hiện tại
                                      @RequestParam(value = "rows") int rows, // Số dòng một trang
                                      @RequestParam(value = "searchTerm") String searchTerm, // Từ khóa cần tìm
                                      @RequestParam(value = "maKhoVatTu") int maKhoVatTu,
                                      @RequestParam(value = "maLoaiVatTu", defaultValue = "0") String maLoaiVatTu,
                                      @RequestParam(value = "maNhomVatTu", required = false, defaultValue = "-1") String maNhomVatTu,
                                      @RequestParam(value = "maPhongBan", required = false, defaultValue = "-1") String maPhongBan,
                                      HttpServletResponse response,
                                      HttpServletRequest request,
                                      HttpSession session) {
        try {
            if (searchTerm.equals("")) {
                return Collections.emptyList();
            } else {
                searchTerm = searchTerm + "%";
                int c = kcbDAO.getSoLuongTonKho(dvtt, maKhoVatTu, searchTerm, 0);
                int total_pages;
                if (c > 0) {
                    total_pages = (c / rows) + 1;
                } else {
                    total_pages = 0;
                }
                if (page > total_pages) {
                    page = total_pages;
                }
                int start = rows * page - rows;
                List<Map<String, Object>> listDanhSachVatTu;
                listDanhSachVatTu = kcbDAO.getDanhSachDuocTonKho(dvtt,
                    0,
                    0,
                    maLoaiVatTu,
                    Integer.parseInt(maNhomVatTu),
                    maKhoVatTu,
                    searchTerm,
                    start,
                    rows,
                    total_pages,
                    "1", // Hiển thị đơn gia khi gõ thuốc
                    "0",
                    maPhongBan
                );
                return listDanhSachVatTu;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Thêm chi tiết toa thuốc nội trú\n Kết quả trả về: " +
        "\n -10: Trùng thuốc" +
        "\n 200: Chốt kho" +
        "\n 100: Chốt số liệu dược" +
        "\n 6: Số lượng vượt quá số lượng tồn" +
        "\n -1: Lỗi Exception/Rollback" +
        "\n 0: Thêm thành công")
    @ResponseBody
    @RequestMapping(value = "/addChiTietToaThuocNoiTru", method = RequestMethod.POST)
    public int addChiTietToaThuocNoiTru(@RequestBody ToaThuocNoiTruObj toaThuocNoiTruObj,
                                 HttpSession session,
                                 HttpServletResponse response,
                                 HttpServletRequest request) {
        try {
            if ((heThongDAO.getMoTaThamSoByMaThamSo(toaThuocNoiTruObj.getDvtt(), 40041).equals("1")) && (toaThuocNoiTruObj.getNghiepVu().equals("noitru_toadichvu") || toaThuocNoiTruObj.getNghiepVu().equals("ba_ngoaitru_toadichvu"))) //vuthanhvinh.nan HISHTROTGG-29698
            {
                kcbDAO.addTachToaDichVuNoiTru(toaThuocNoiTruObj); // Thêm dữ liệu tách toa thuốc dịch vụ
            }

            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 27, "Thêm thuốc nội trú",
                toaThuocNoiTruObj.getMaUser(), toaThuocNoiTruObj.getDvtt(), 0,
                clientIP, hostIP, "", "addChiTietToaThuocNoiTru", dataSourceDuocV2);
            toaThuocNoiTruObj.setIdGiaoDich(Long.parseLong(idGiaoDich));
            return kcbDAO.addToaThuocNoiTru(toaThuocNoiTruObj);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Lấy danh sách chi tiết toa thuốc nội trú")
    @RequestMapping(value="/getDanhSachChiTietToaThuocNoiTru", method = RequestMethod.GET)
    public List getDanhSachChiTietToaThuocNoiTru(@RequestParam(value = "soVaoVien") String soVaoVien,
                                                 @RequestParam(value = "soVaoVienDT") String soVaoVienDT,
                                                 @RequestParam(value = "sttBenhAn") String sttBenhAn,
                                                 @RequestParam(value = "sttDotDieuTri") String sttDotDieuTri,
                                                 @RequestParam(value = "maToaThuoc") String maToaThuoc,
                                                 @RequestParam(value = "nghiepVu") String nghiepVu,
                                                 @RequestParam(value = "dvtt") String dvtt,
                                                 @RequestParam(value = "soPhieu", required = false, defaultValue = "") String soPhieu,
                                                 @RequestParam(value = "ma", required = false, defaultValue = "0") String ma,
                                                 @RequestParam(value = "theoDV", required = false, defaultValue = "0") String theoDV,
                                                 HttpServletResponse response,
                                                 HttpServletRequest request,
                                                 HttpSession session) {
        try {
                return kcbDAO.getDanhSachChiTietToaThuocNoiTru(dvtt,
                    maToaThuoc,
                    nghiepVu,
                    sttBenhAn,
                    sttDotDieuTri,
                    soVaoVien,
                    soVaoVienDT,
                    soPhieu,
                    ma,
                    theoDV);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    /* Ngoại trú */
    @ApiOperation("Thêm chi tiết toa thuốc nội trú\n Kết quả trả về: " +
        "\n 3: Trùng thuốc" +
        "\n 4: Đã thanh toán" +
        "\n 5: Xuất thuốc" +
        "\n 200: Chốt kho" +
        "\n 300: Kế toán xác nhận bảng kê" +
        "\n -1: Lỗi Exception/Rollback" +
        "\n 0: Thêm thành công")
    @ResponseBody
    @RequestMapping(value = "/addChiTietToaThuocNgoaiTru", method = RequestMethod.POST)
    public int addChiTietToaThuocNgoaiTru(@RequestBody ToaThuocNgoaiTruObj toaThuocNgoaiTruObj,
                                        HttpSession session,
                                        HttpServletResponse response,
                                        HttpServletRequest request) {
        try {
            if (toaThuocNgoaiTruObj.getSoPhieuThanhToan().trim().equals("")) {
                toaThuocNgoaiTruObj.setSoPhieuThanhToan(kcbDAO.getSoPhieuThanhToan(toaThuocNgoaiTruObj.getMaKhamBenh(), toaThuocNgoaiTruObj.getDvtt(), "0", toaThuocNgoaiTruObj.getSoVaoVien()));
            }
            if ((heThongDAO.getMoTaThamSoByMaThamSo(toaThuocNgoaiTruObj.getDvtt(), 40041).equals("1")) && (toaThuocNgoaiTruObj.getNghiepVu().equals("noitru_toadichvu") || toaThuocNgoaiTruObj.getNghiepVu().equals("ba_ngoaitru_toadichvu") || toaThuocNgoaiTruObj.getNghiepVu().equals("ngoaitru_toadichvu")))
            {
                kcbDAO.addTachToaDichVuNgoaiTru(toaThuocNgoaiTruObj); // Thêm dữ liệu tách toa thuốc dịch vụ
            }
            if (toaThuocNgoaiTruObj.getCoBHYT() > 0
//                && tsht.dathanhtoanvanrathuoc.equals("0")
            ) {
                toaThuocNgoaiTruObj.setDaThanhToan(kcbDAO.getDaThanhToanNgoaiTru(toaThuocNgoaiTruObj.getDvtt(), toaThuocNgoaiTruObj.getNgayRaToa(), toaThuocNgoaiTruObj.getMaBenhNhan(), toaThuocNgoaiTruObj.getMaKhamBenh(), toaThuocNgoaiTruObj.getSoVaoVien()));
//                if (tsht.thanhtoanvienphinhieunac.equals("1") || tsht.THANHTOANNHIEULANNGOAITRUBH.equals("1")) {
//                } else
                    if (Integer.parseInt(toaThuocNgoaiTruObj.getDaThanhToan()) > 0) {
                    return 4; // Đã thanh toán
                }
            }
            if (!kcbDAO.getKiemTraXuatThuoc(toaThuocNgoaiTruObj.getDvtt(), toaThuocNgoaiTruObj.getMaToaThuoc(), toaThuocNgoaiTruObj.getNghiepVu(), toaThuocNgoaiTruObj.getNgayRaToa(), toaThuocNgoaiTruObj.getMaBenhNhan(), toaThuocNgoaiTruObj.getSoVaoVien()).equals("0")) {
                return 5; // xuất dược
            }
            if (toaThuocNgoaiTruObj.getSoPhieu().equals("")) {
//                if (tsht.cungthuocvanraduoc.equals("0")) {
                    if (!kcbDAO.getKiemTraTrungThuocNgoaiTru(toaThuocNgoaiTruObj.getDvtt(), toaThuocNgoaiTruObj.getMaToaThuoc(), toaThuocNgoaiTruObj.getNghiepVu(), toaThuocNgoaiTruObj.getNgayRaToa(), toaThuocNgoaiTruObj.getMaBenhNhan(), toaThuocNgoaiTruObj.getMaVatTu(), toaThuocNgoaiTruObj.getSoVaoVien()).equals("0")) {
                        return 3; // Trùng thuốc
                    }
//                }
            }

            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 24, "Thêm thuốc ngoại trú",
                toaThuocNgoaiTruObj.getMaUser(), toaThuocNgoaiTruObj.getDvtt(), 0,
                clientIP, hostIP, "", "addChiTietToaThuocNgoaiTru", dataSourceDuocV2);
            toaThuocNgoaiTruObj.setIdGiaoDich(Long.parseLong(idGiaoDich));
            return kcbDAO.addToaThuocNgoaiTru(toaThuocNgoaiTruObj);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Lấy danh sách chi tiết toa thuốc ngoại trú")
    @RequestMapping(value="/getDanhSachChiTietToaThuocNgoaiTru", method = RequestMethod.GET)
    public List getDanhSachChiTietToaThuocNgoaiTru(@RequestParam(value = "soVaoVien") String soVaoVien,
                                                 @RequestParam(value = "soPhieuTTPT", required = false, defaultValue = "") String soPhieuTTPT,
                                                 @RequestParam(value = "maTTPT", required = false, defaultValue = "") String maTTPT,
                                                 @RequestParam(value = "maToaThuoc") String maToaThuoc,
                                                 @RequestParam(value = "nghiepVu") String nghiepVu,
                                                 @RequestParam(value = "dvtt") String dvtt,
                                                 @RequestParam(value = "soPhieu", required = false, defaultValue = "") String soPhieu,
                                                 @RequestParam(value = "ma", required = false, defaultValue = "0") String ma,
                                                 @RequestParam(value = "theoDV", required = false, defaultValue = "0") String theoDV,
                                                 HttpServletResponse response,
                                                 HttpServletRequest request,
                                                 HttpSession session) {
        try {
            return kcbDAO.getDanhSachChiTietToaThuocNgoaiTru(maToaThuoc,
                dvtt,
                nghiepVu,
                soVaoVien,
                ma,
                soPhieu,
                theoDV,
                soPhieuTTPT,
                maTTPT
                );
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
}
