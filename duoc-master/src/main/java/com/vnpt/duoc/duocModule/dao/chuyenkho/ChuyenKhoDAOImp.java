package com.vnpt.duoc.duocModule.dao.chuyenkho;

import com.vnpt.duoc.duocModule.objects.chuyenkho.ChiTietChuyenKhoObj;
import com.vnpt.duoc.duocModule.objects.chuyenkho.ChuyenKhoObj;
import com.vnpt.duoc.jdbc.JdbcTemplate;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ChuyenKhoDAOImp implements ChuyenKhoDAO {
    private final HikariDataSource dataSourceDuocV2;

    public ChuyenKhoDAOImp(HikariDataSource dataSourceDuocV2) {
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    @Override
    public List getDanhSachPhieuChuyenKho(String tuNgay, String denNgay, String dvtt, long nghiepVu, String maPhongBan) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        String sql = "call DC_CHUYENKHO_PHIEU_SEL(?,?,?,?,?)#c,t,t,s,l,s";
        return jdbcTemplate.queryForList(sql, new Object[]{tuNgay, denNgay, dvtt, nghiepVu, maPhongBan});
    }

    @Override
    public List getDanhSachNghiepVu() {
        String sql = "call HIS_DUOCV2.DC_SP_SEL_NGHIEPVUDUOCV2()#c";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List getDanhSachNghiepVuNhanVien(long maNV, String dvtt) {
        String sql = "call HIS_DUOCV2.DC_SP_SEL_NGHIEPVUDUOC_NV_V2(?,?)#c,l,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{maNV, dvtt});
    }

    @Override
    public List getDanhSachKhoNhan(long nghiepVu, String dvtt, long maNhanVien) {
        String sql = "call HIS_DUOCV2.DC_SP_CK_HT_NOIYEUCAU_NV_V2(?,?,?)#c,l,s,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{nghiepVu, dvtt, maNhanVien});
    }

    @Override
    public List getDanhSachKhoChuyen(long nghiepVu, String dvtt) {
        String sql = "call HIS_DUOCV2.DC_SP_CK_HT_NOICHUYEN_V2(?,?)#c,l,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{nghiepVu, dvtt});
    }

    @Override
    public String getDonViQuanLyTrucTiep(String dvtt) {
        String sql = "SELECT MA_DONVIQUANLY FROM HIS_FW.DM_DONVI WHERE MA_DONVI = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{dvtt}, String.class);
    }

    @Override
    public List getDanhSachVatTuTonKho(String dvtt, long maKho, Date ngayChuyen, long nghiepVu) {
        String sql = "call HIS_DUOCV2.DC_LOADVATTUCHUYENKHO_V2(?,?,?,?)#c,s,l,t,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{
            dvtt,
            maKho,
            ngayChuyen,
            nghiepVu
        });
    }

    @Override
    public List getDanhSachChiTietPhieuChuyenKho(long idPhieuChuyen, String dvtt) {
        String sql = "call HIS_DUOCV2.DC_CHUYENKHO_CHITIET_PHIEU_SEL (?,?)#c,l,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{idPhieuChuyen, dvtt});
    }

    @Override
    public Map addPhieuChuyenKho(ChuyenKhoObj chuyenKhoObj, String maKhoa, String maPhong) {
        String sql = "call HIS_DUOCV2.DC_CHUYENKHO_PHIEU_INSERT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)#c,s,l,s,t,s,t,s,s,l,l,l,s,l,l,l,s,s,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForMap(sql, new Object[]{
            chuyenKhoObj.getMaDonViTao(),
            chuyenKhoObj.getMaNghiepVuChuyen(),
            chuyenKhoObj.getNghiepVu(),
            chuyenKhoObj.getNgayPhieuChuyen(),
            chuyenKhoObj.getGhiChu(),
            chuyenKhoObj.getNgayChuyen(),
            chuyenKhoObj.getMaDonViGiao(),
            chuyenKhoObj.getMaDonViNhan(),
            chuyenKhoObj.getMaKhoGiao(),
            chuyenKhoObj.getMaKhoNhan(),
            chuyenKhoObj.getMaNguoiTao(),
            chuyenKhoObj.getDiaChi(),
            chuyenKhoObj.getDuTruCSTT(),
            chuyenKhoObj.getMaPhongBanTao(),
            chuyenKhoObj.getIdGiaoDich(),
            maKhoa,
            maPhong,
            chuyenKhoObj.getXuatHuy()
        });
    }

    @Override
    public int updateNhatPhieuChuyenKhoV2(ChuyenKhoObj chuyenKhoObj, String maKhoa, String maPhong) {
        String sql = "call HIS_DUOCV2.DC_CHUYENKHO_PHIEU_UPDATE(?,?,?,?,?,?,?,?,?,?,?,?,?)#l,l,s,t,t,s,l,s,s,l,l,s,s,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{
            chuyenKhoObj.getIdChuyenKhoVatTu(),
            chuyenKhoObj.getMaDonViTao(),
            chuyenKhoObj.getNgayPhieuChuyen(),
            chuyenKhoObj.getNgayChuyen(),
            chuyenKhoObj.getGhiChu(),
            chuyenKhoObj.getMaKhoNhan(),
            chuyenKhoObj.getMaDonViGiao(),
            chuyenKhoObj.getMaDonViNhan(),
            chuyenKhoObj.getMaNguoiTao(),
            chuyenKhoObj.getIdGiaoDich(),
            maKhoa,
            maPhong,
            chuyenKhoObj.getXuatHuy()
        }, Integer.class);
    }

    @Override
    public int deletePhieuChuyenKho(long idPhieuChuyen, String dvtt, long nguoiXoa, long idGiaoDich, String maKhoa, String maPhong) {
        String sql = "call HIS_DUOCV2.DC_SP_CHUYENKHO_DEL_PH_V2 (?,?,?,?,?,?)#l,l,s,l,l,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForInt(sql, new Object[]{idPhieuChuyen, dvtt, nguoiXoa, idGiaoDich, maKhoa, maPhong});
    }

    @Override
    public Map addChiTietPhieuChuyenKho(ChiTietChuyenKhoObj chiTietObj, String maKhoa, String maPhong) {
        String sql = "call HIS_DUOCV2.DC_CHUYENKHO_CHITIET_PHIEU_INS(?,?,?,?,?,?,?,?,?,?,?,?,?,?)#c,s,s,l,s,l,l,t,l,s,s,s,l,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForMap(sql, new Object[]{
            chiTietObj.getIdChuyenKhoVT(),
            chiTietObj.getMaDonViTao(),
            chiTietObj.getMaVatTu(),
            chiTietObj.getTenVatTu(),
            chiTietObj.getSoLuong(),
            chiTietObj.getDonGia(),
            new Date(chiTietObj.getNgayChuyen()),
            chiTietObj.getNguoiNhap(),
            chiTietObj.getGhiChu(),
            chiTietObj.getSoLoSanXuat(),
            chiTietObj.getNgayHetHan(),
            chiTietObj.getIdGiaoDich(),
            maKhoa,
            maPhong
        });
    }

    @Override
    public int deleteChiTietPhieuChuyenKho(long idChuyenKhoVTCT, String dvtt, long nguoiXoa, long idGiaoDich, String maKhoa, String maPhong) {
        String sql = "call HIS_DUOCV2.DC_SP_CHUYENKHO_CT_DEL_V2(?,?,?,?,?,?)#l,l,s,l,l,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForInt(sql, new Object[]{idChuyenKhoVTCT, dvtt, nguoiXoa, idGiaoDich, maKhoa, maPhong});
    }

    // Print

    @Override
    public Map getThongTinPhieuChuyenKho(String dvtt, long idPhieuChuyen, long maNghiepVu) {
        String sql = "call HIS_DUOCV2.DC_SP_THONGTINPHIEUCHUYENKHO(?,?,?)#c,s,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        try {
            return jdbcTemplate.queryForMap(sql, new Object[]{dvtt, idPhieuChuyen, maNghiepVu});
        } catch (Exception e) {
            return new HashMap();
        }
    }

    @Override
    public Double getTongTienPhieuChuyenKho(long idPhieuChuyen) {
        String sql = "call HIS_DUOCV2.DC_SP_TONGTIENPHIEUCHUYENKHO(?)#l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{idPhieuChuyen}, Double.class);
    }

    @Override
    public List getDanhSachBanInPhieuDuTruKhoaPhong(String dvtt, long idChuyenKhoVatTu) {
        String sql = "call HIS_DUOCV2.NOITRUKHOAPHONG_DUTRU_TONGTIEN(?,?)#c,s,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, idChuyenKhoVatTu});
    }
}
