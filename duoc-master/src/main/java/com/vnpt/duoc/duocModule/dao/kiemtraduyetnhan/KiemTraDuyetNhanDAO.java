package com.vnpt.duoc.duocModule.dao.kiemtraduyetnhan;

import java.util.List;

public interface KiemTraDuyetNhanDAO {
    List getDanhSachPhieuChuyenKhoTheoTrangThai(String dvtt, int trangThai);
}
