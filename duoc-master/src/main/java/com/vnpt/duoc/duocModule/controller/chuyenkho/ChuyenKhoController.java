package com.vnpt.duoc.duocModule.controller.chuyenkho;

import com.vnpt.duoc.duocModule.DuocConfigs;
import com.vnpt.duoc.duocModule.DuocTransaction;
import com.vnpt.duoc.duocModule.DuocUtils;
import com.vnpt.duoc.duocModule.dao.chuyenkho.ChuyenKhoDAO;
import com.vnpt.duoc.duocModule.dao.hethong.DocSo;
import com.vnpt.duoc.duocModule.dao.hethong.HeThongDAO;
import com.vnpt.duoc.duocModule.objects.chuyenkho.ChiTietChuyenKhoObj;
import com.vnpt.duoc.duocModule.objects.chuyenkho.ChuyenKhoObj;
import com.vnpt.duoc.jasper.JasperHelper;
import com.zaxxer.hikari.HikariDataSource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import liquibase.pro.packaged.S;
import net.minidev.json.JSONObject;
import net.sf.jasperreports.engine.JRParameter;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.MonthDay;
import java.time.Year;
import java.time.YearMonth;
import java.util.*;

@RestController
@RequestMapping("api/chuyenkho")
@Api(value="ChuyenKhoController")
public class ChuyenKhoController {
    private final ChuyenKhoDAO chuyenKhoDAO;
    private final HeThongDAO heThongDAO;
    private final HikariDataSource dataSourceDuocV2;

    public ChuyenKhoController(ChuyenKhoDAO chuyenKhoDAO, HeThongDAO heThongDAO, HikariDataSource dataSourceDuocV2) {
        this.chuyenKhoDAO = chuyenKhoDAO;
        this.heThongDAO = heThongDAO;
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    @ApiOperation("Lấy danh sách chuyển kho theo khoảng thời gian")
    @RequestMapping(value="/getDanhSachPhieuChuyenKho", method = RequestMethod.GET)
    public List getDanhSachPhieuChuyenKho(@RequestParam(value = "dvtt") String dvtt,
                                          @RequestParam(value = "tuNgay") String tuNgay,
                                          @RequestParam(value = "denNgay") String denNgay,
                                          @RequestParam(value = "maNghiepVu") long maNghiepVu,
                                          @RequestParam(value = "maPhongBan") String maPhongBan,
                                          HttpServletResponse response,
                                          HttpServletRequest request,
                                          HttpSession session) {
        try {
            return chuyenKhoDAO.getDanhSachPhieuChuyenKho(tuNgay, denNgay, dvtt, maNghiepVu, maPhongBan);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách nghiệp vụ")
    @RequestMapping(value="/getDanhSachNghiepVu", method = RequestMethod.GET)
    public List getDanhSachPhieuChuyenKho(HttpServletResponse response,
                                          HttpServletRequest request,
                                          HttpSession session) {
        try {
            return chuyenKhoDAO.getDanhSachNghiepVu();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách nghiệp vụ theo nhân viên")
    @RequestMapping(value="/getDanhSachNghiepVuNhanVien", method = RequestMethod.GET)
    public List getDanhSachNghiepVuNhanVien(@RequestParam(value = "dvtt") String dvtt,
                                            @RequestParam(value = "maNV") long maNV,
                                            HttpServletResponse response,
                                            HttpServletRequest request,
                                            HttpSession session) {
        try {
            return chuyenKhoDAO.getDanhSachNghiepVuNhanVien(maNV, dvtt);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách kho chuyển")
    @RequestMapping(value="/getDanhSachKhoChuyen", method = RequestMethod.GET)
    public List getDanhSachKhoChuyen(@RequestParam(value = "dvtt") String dvtt,
                                            @RequestParam(value = "maNghiepVu") long maNghiepVu,
                                            HttpServletResponse response,
                                            HttpServletRequest request,
                                            HttpSession session) {
        try {
            return chuyenKhoDAO.getDanhSachKhoChuyen(maNghiepVu, dvtt);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách kho nhận")
    @RequestMapping(value="/getDanhSachKhoNhan", method = RequestMethod.GET)
    public List getDanhSachKhoNhan(@RequestParam(value = "dvtt") String dvtt,
                                     @RequestParam(value = "maNghiepVu") long maNghiepVu,
                                     @RequestParam(value = "maNV") long maNV,
                                     HttpServletResponse response,
                                     HttpServletRequest request,
                                     HttpSession session) {
        try {
            return chuyenKhoDAO.getDanhSachKhoNhan(maNghiepVu, dvtt, maNV);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy đơn vị quản lý")
    @RequestMapping(value="/getDonViQuanLy", method = RequestMethod.GET)
    public String getDanhSachKhoNhan(@RequestParam(value = "dvtt") String dvtt,
                                   HttpServletResponse response,
                                   HttpServletRequest request,
                                   HttpSession session) {
        try {
            return chuyenKhoDAO.getDonViQuanLyTrucTiep(dvtt);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @ApiOperation("Lấy danh sách vật tư chuyển kho")
    @RequestMapping(value="/getDanhSachVatTuTonKho", method = RequestMethod.GET)
    public List getDanhSachVatTuTonKho(@RequestParam(value = "dvtt") String dvtt,
                                       @RequestParam(value = "maKho") long maKho,
                                       @RequestParam(value = "ngayChuyen") String ngayChuyen,
                                       @RequestParam(value = "maNghiepVu") long maNghiepVu,
                                       HttpServletResponse response,
                                       HttpServletRequest request,
                                       HttpSession session) {
        try {
            return chuyenKhoDAO.getDanhSachVatTuTonKho(dvtt, maKho, DuocUtils.convertStringToDate(ngayChuyen, "dd/MM/yyyy"), maNghiepVu);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Thêm phiếu chuyển kho")
    @ResponseBody
    @RequestMapping(value = "/addPhieuChuyenKho", method = RequestMethod.POST)
    public Map addPhieuChuyenKho(@RequestBody JSONObject jsonObject,
                                 @RequestParam(value = "maPhongBan") String maPhongBan,
                                 @RequestParam(value = "maKhoa") String maKhoa,
                                 HttpSession session,
                                 HttpServletResponse response,
                                 HttpServletRequest request) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 13, "Thêm phiếu chuyển kho",
                Long.parseLong(jsonObject.get("maNguoiTao").toString()), jsonObject.get("maDonViTao").toString(), 0,
                clientIP, hostIP, "", "addPhieuChuyenKho", dataSourceDuocV2);
            ChuyenKhoObj chuyenKhoObj = new ChuyenKhoObj(jsonObject, session, idGiaoDich);
            return chuyenKhoDAO.addPhieuChuyenKho(chuyenKhoObj, maPhongBan, maKhoa);
        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap();
        }
    }

    @ApiOperation("Cập nhật thông tin phiếu chuyển kho")
    @ResponseBody
    @RequestMapping(value = "/updatePhieuChuyenKho", method = RequestMethod.POST)
    public int updatePhieuChuyenKho(@RequestBody JSONObject jsonObject,
                                    @RequestParam(value = "maPhongBan") String maPhongBan,
                                    @RequestParam(value = "maKhoa") String maKhoa,
                                    HttpSession session,
                                    HttpServletResponse response,
                                    HttpServletRequest request) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 15, "Cập nhật phiếu chuyển kho",
                Long.parseLong(jsonObject.get("maNguoiTao").toString()), jsonObject.get("maDonViTao").toString(), 0,
                clientIP, hostIP, "", "updatePhieuChuyenKho", dataSourceDuocV2);
            ChuyenKhoObj chuyenKhoObj = new ChuyenKhoObj(jsonObject, session, idGiaoDich);
            return chuyenKhoDAO.updateNhatPhieuChuyenKhoV2(chuyenKhoObj, maPhongBan, maKhoa);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Xóa phiếu chuyển kho")
    @RequestMapping(value = "/deletePhieuChuyenKho", method = RequestMethod.DELETE)
    @ResponseBody
    public int deletePhieuChuyenKho(@RequestParam(value = "idPhieuChuyen") long idPhieuChuyen,
                                    @RequestParam(value = "dvtt") String dvtt,
                                    @RequestParam(value = "nguoiXoa") long nguoiXoa,
                                    @RequestParam(value = "maPhongBan") String maPhongBan,
                                    @RequestParam(value = "maKhoa") String maKhoa,
                                    HttpSession session,
                                    HttpServletRequest request,
                                    HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 14, "Xóa phiếu chuyển kho",
                nguoiXoa, dvtt, 0,
                clientIP, hostIP, "", "deletePhieuNhapKho", dataSourceDuocV2);
            return chuyenKhoDAO.deletePhieuChuyenKho(idPhieuChuyen, dvtt, nguoiXoa, Long.parseLong(idGiaoDich), maKhoa, maPhongBan);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Thêm chi tiết phiếu chuyển kho")
    @ResponseBody
    @RequestMapping(value = "/addChiTietPhieuChuyenKho", method = RequestMethod.POST)
    public Map addChiTietPhieuChuyenKho(@RequestBody ChiTietChuyenKhoObj jsonObject,
                                        @RequestParam(value = "maPhongBan") String maPhongBan,
                                        @RequestParam(value = "maKhoa") String maKhoa,
                                        HttpSession session,
                                        HttpServletResponse response,
                                        HttpServletRequest request) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 10, "Thêm chi tiết phiếu chuyển kho",
                jsonObject.getNguoiNhap(), jsonObject.getMaDonViTao(), 0,
                clientIP, hostIP, "", "addChiTietPhieuChuyenKho", dataSourceDuocV2);
            ChiTietChuyenKhoObj tempChiTiet = jsonObject;
            tempChiTiet.setIdGiaoDich(Long.parseLong(idGiaoDich));
            return chuyenKhoDAO.addChiTietPhieuChuyenKho(tempChiTiet, maPhongBan, maKhoa);
        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap();
        }
    }

    @ApiOperation("Xóa chi tiết phiếu chuyển kho")
    @RequestMapping(value = "/deleteChiTietPhieuChuyenKho", method = RequestMethod.DELETE)
    @ResponseBody
    public int deleteChiTietPhieuChuyenKho(@RequestParam(value = "idChuyenKhoVTCT") long idChuyenKhoVTCT,
                                           @RequestParam(value = "dvtt") String dvtt,
                                           @RequestParam(value = "nguoiXoa") long nguoiXoa,
                                           @RequestParam(value = "maPhongBan") String maPhongBan,
                                           @RequestParam(value = "maKhoa") String maKhoa,
                                           HttpSession session,
                                           HttpServletRequest request,
                                           HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 12, "Xóa chi tiết phiếu chuyển kho",
                nguoiXoa, dvtt, 0,
                clientIP, hostIP, "", "deletePhieuNhapKho", dataSourceDuocV2);
            return chuyenKhoDAO.deleteChiTietPhieuChuyenKho(idChuyenKhoVTCT, dvtt, nguoiXoa, Long.parseLong(idGiaoDich), maKhoa, maPhongBan);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Lấy danh sách chi tiết chuyển kho")
    @RequestMapping(value="/getDanhSachChiTietPhieuChuyenKho", method = RequestMethod.GET)
    public List getDanhSachChiTietPhieuChuyenKho(@RequestParam(value = "dvtt") String dvtt,
                                                 @RequestParam(value = "idPhieuChuyen") long idPhieuChuyen,
                                                 HttpServletResponse response,
                                                 HttpServletRequest request,
                                                 HttpSession session) {
        try {
            return chuyenKhoDAO.getDanhSachChiTietPhieuChuyenKho(idPhieuChuyen, dvtt);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @RequestMapping(value = "/printPhieuChuyenKho", method = RequestMethod.GET)
    public @ResponseBody
    void printPhieuChuyenKho(HttpServletResponse response,
                             HttpServletRequest request,
                             @RequestParam(value = "dvtt") String dvtt,
                             @RequestParam(value = "idPhieuChuyen") long idPhieuChuyen,
                             @RequestParam(value = "maNghiepVu") long maNghiepVu,
                             @RequestParam(value = "tenBenhVien") String tenBenhVien,
                             @RequestParam(value = "tenTinh") String tenTinh,
                             @RequestParam(value = "loaiFile") String loaiFile // pdf   xls    rtf
    ) {
        try {
            String tenbenhvien = tenBenhVien.toUpperCase();
            Map<String, Object> mapthongtin = chuyenKhoDAO.getThongTinPhieuChuyenKho(dvtt, idPhieuChuyen, maNghiepVu);
            Map parameters = new HashMap();
            parameters.put("dvtt", dvtt);
            parameters.put("IDChuyenKhoVatTu", idPhieuChuyen);
            parameters.put("maNghiepVu", maNghiepVu);
            parameters.put("sophieu", mapthongtin.get("SOPHIEUCHUYEN").toString());
            parameters.put("soluutru", mapthongtin.get("SOLUUTRU").toString());
            parameters.put("donvigiao", mapthongtin.get("KHOGIAO").toString());
            parameters.put("donvinhan", mapthongtin.get("KHONHAN").toString());
            parameters.put("tenbenhvien", tenbenhvien);
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
//            if (!tsht.tendonviquanlytructiep.equals("")) {
//                donviquanlytructiep = tsht.tendonviquanlytructiep.toUpperCase();
//            }
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("ngay", MonthDay.now().getDayOfMonth() + "");
            parameters.put("thang", MonthDay.now().getMonthValue() + "");
            parameters.put("nam", YearMonth.now().getYear() + "");
            File reportFile;
            String reportName = "rp_chuyenkhovattu.jasper";
            reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @ResponseBody
    @RequestMapping(value = "/printPhieuXuatKho", method = RequestMethod.GET)
    public void printPhieuXuatKho(HttpServletResponse response,
                                  HttpServletRequest request,
                                  HttpSession session,
                                  @RequestParam(value = "dvtt") String dvtt,
                                  @RequestParam(value = "idPhieuChuyen") long idPhieuChuyen,
                                  @RequestParam(value = "maNghiepVu") long maNghiepVu,
                                  @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                  @RequestParam(value = "tenTinh") String tenTinh,
                                  @RequestParam(value = "nguoiLap") String nguoiLap,
                                  @RequestParam(value = "loaiFile") String loaiFile,
                                  @RequestParam(value = "mau") int mau // 0: mẫu đứng      1: mẫu ngang
    ) {
        try {
            String tenbenhvien = tenBenhVien.toUpperCase();
            Map map = chuyenKhoDAO.getThongTinPhieuChuyenKho(dvtt, idPhieuChuyen, maNghiepVu);
            Map parameters = new HashMap();
            parameters.put("dvtt", dvtt);
            parameters.put("idPhieuChuyen", idPhieuChuyen);
            parameters.put("sophieu", map.get("SoPhieuChuyen").toString());
            parameters.put("soluutru", map.get("SoluuTru").toString());
            parameters.put("xuattukho", map.get("Khogiao").toString());
            parameters.put("khonoitru", map.get("Khogiao").toString());
            parameters.put("xuatdenkho", map.get("Khonhan").toString());
            parameters.put("khoanhan", map.get("Khonhan").toString());
            parameters.put("tenbenhvien", tenbenhvien);
            parameters.put("tieude", "PHIẾU XUẤT KHO");
            parameters.put("lydoxuatkho", map.get("GhiChu").toString());
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
//            if (!tsht.tendonviquanlytructiep.equals("")) {
//                donviquanlytructiep = tsht.tendonviquanlytructiep.toUpperCase();
//            }
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("ngay", MonthDay.now().getDayOfMonth() + "");
            parameters.put("thang", MonthDay.now().getMonthValue() + "");
            parameters.put("nam", YearMonth.now().getYear() + "");
            parameters.put("gio", LocalTime.now().getHour() + "");
            parameters.put("phut", LocalTime.now().getMinute() + "");
            parameters.put("giay", LocalTime.now().getMinute() + "");
            parameters.put("nguoilapbang", nguoiLap);
            Double tongTien = chuyenKhoDAO.getTongTienPhieuChuyenKho(idPhieuChuyen);
            parameters.put("sotienbangchu", new DocSo().docso(tongTien));
            parameters.put("nguoilapbang", nguoiLap);
            parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
            File reportFile;
            if (mau == 0) { // mẫu đứng
                String reportName = "rp_phieuxuatkho_khodung.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
            } else { // mẫu ngang
                String reportName = "rp_phieuxuatkho_khongang.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
            }
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @RequestMapping(value = "/printBienBanThanhLyMatHongVo", method = RequestMethod.GET)
    public @ResponseBody
    void printBienBanThanhLyMatHongVo(HttpServletResponse response,
                                      HttpServletRequest request,
                                      @RequestParam(value = "dvtt") String dvtt,
                                      @RequestParam(value = "idPhieuChuyen") long idPhieuChuyen,
                                      @RequestParam(value = "soPhieuChuyen") String soPhieuChuyen,
                                      @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                      @RequestParam(value = "tenTinh") String tenTinh,
                                      @RequestParam(value = "loaiFile") String loaiFile,
                                      @RequestParam(value = "mau") String mau // 0: Biên bản thanh lý      1: Biên bản mất hỏng vỡ
    ) {
        try {
            String tenbenhvien = tenBenhVien.toUpperCase();
            Map mapthongtin = chuyenKhoDAO.getThongTinPhieuChuyenKho(dvtt, idPhieuChuyen, 0);
            Map parameters = new HashMap();
            parameters.put("dvtt", dvtt);
            parameters.put("sophieu", soPhieuChuyen);
            parameters.put("soluutru", mapthongtin.get("SoluuTru").toString());
            parameters.put("xuattukho", mapthongtin.get("Khogiao").toString());
            parameters.put("khonoitru", mapthongtin.get("Khogiao").toString());
            parameters.put("xuatdenkho", mapthongtin.get("Khonhan").toString());
            parameters.put("khoanhan", mapthongtin.get("Khonhan").toString());
            parameters.put("tenbenhvien", tenbenhvien);
            parameters.put("tieude", "PHIẾU XUẤT KHO");
            parameters.put("lydoxuatkho", mapthongtin.get("GhiChu").toString());
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
//            if (!tsht.tendonviquanlytructiep.equals("")) {
//                donviquanlytructiep = tsht.tendonviquanlytructiep.toUpperCase();
//            }
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("ngay", MonthDay.now().getDayOfMonth() + "");
            parameters.put("thang", MonthDay.now().getMonthValue() + "");
            parameters.put("nam", YearMonth.now().getYear() + "");
            File reportFile;

            if (mau.equals("0")) {
                String reportName = "bdg_rp_bienbanthanhly_duocv2.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
            } else if (mau.equals("1")) {
                String reportName = "bdg_rp_bienbanxacnhan_hongvo_duocv2.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
            } else {
                String reportName = "bdg_rp_bienbanchuyentra_nhacc_duocv2.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
            }
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @ApiOperation("Lấy danh sách bản in phiếu dự trù khoa phòng")
    @RequestMapping(value="/getDanhSachBanInPhieuDuTruKhoaPhong", method = RequestMethod.GET)
    public List getDanhSachBanInPhieuDuTruKhoaPhong(@RequestParam(value = "dvtt") String dvtt,
                                                    @RequestParam(value = "idPhieuChuyen") long idPhieuChuyen,
                                                    HttpServletResponse response,
                                                    HttpServletRequest request,
                                                    HttpSession session) {
        try {
            return chuyenKhoDAO.getDanhSachBanInPhieuDuTruKhoaPhong(dvtt, idPhieuChuyen);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("In phiếu dự trù khoa phòng")
    @RequestMapping(value = "/printPhieuDuTruKhoaPhong", method = RequestMethod.GET)
    public @ResponseBody
    void printPhieuDuTruKhoaPhong(@RequestParam(value = "dvtt") String dvtt,
                                  @RequestParam(value = "soPhieuChuyen") String soPhieuChuyen,
                                  @RequestParam(value = "idPhieuChuyen") String idPhieuChuyen,
                                  @RequestParam(value = "ngayChuyen") String ngayChuyen,
                                  @RequestParam(value = "thanhTien") String thanhTien,
                                  @RequestParam(value = "maKhoVatTu") String maKhoVatTu,
                                  @RequestParam(value = "tenKhoVatTu") String tenKhoVatTu,
                                  @RequestParam(value = "maPhongBan") String maPhongBan,
                                  @RequestParam(value = "tenKhoYeuCau") String tenKhoYeuCau,
                                  @RequestParam(value = "maLoaiVatTu") String maLoaiVatTu,
                                  @RequestParam(value = "nguoiTao") String nguoiTao,
                                  @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                  @RequestParam(value = "tenTinh") String tenTinh,
                                  @RequestParam(value = "loaiFile") String loaiFile,
                                  HttpServletRequest request,
                                  HttpServletResponse response) {
        try {
            Map parameters = new HashMap();
            String nguoiin = "";
            String[] arr_nk = ngayChuyen.split("/");
            String ngay = arr_nk[0];
            String thang = arr_nk[1];
            String nam = arr_nk[2];
            String ngaygio = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new java.util.Date());
            String trangthai = "0";
            String nguoiduyet = "";
            if (maLoaiVatTu.equals("TH")) {
                parameters.put("tieude", "PHIẾU LĨNH THUỐC");
                parameters.put("mauin", "MS: 01D/BV-01");
            } else if (maLoaiVatTu.equals("TH_HTT")) {
                parameters.put("tieude", "PHIẾU LĨNH THUỐC HƯỚNG THẦN, TIỀN CHẤT");
                parameters.put("mauin", "MS: 08D");
            } else if (maLoaiVatTu.equals("VT_TH")) {
                parameters.put("tieude", "PHIẾU LĨNH VẬT TƯ TIÊU HAO");
                parameters.put("mauin", "MS: 03D/BV-01");
            } else if (maLoaiVatTu.equals("VT_TT")) {
                parameters.put("tieude", "PHIẾU LĨNH VẬT TƯ THAY THẾ");
                parameters.put("mauin", "MS: 03D/BV-01");
            } else if (maLoaiVatTu.equals("DICHTRUYEN")) {
                parameters.put("tieude", "PHIẾU LĨNH DỊCH TRUYỀN");
                parameters.put("mauin", "MS: 02D/BV-01");
            } else if (maLoaiVatTu.equals("TH_NGHIEN")) {
                parameters.put("tieude", "PHIẾU LĨNH THUỐC GÂY NGHIỆN");
                parameters.put("mauin", "MS: 08D");
            } else if (maLoaiVatTu.equals("VT_YT")) {
                parameters.put("tieude", "PHIẾU LĨNH VẬT TƯ Y TẾ");
                parameters.put("mauin", "MS: 03D/BV-01");
            } else if (maLoaiVatTu.equals("HC")) {
                parameters.put("tieude", "PHIẾU LĨNH HOÁ CHẤT");
                parameters.put("mauin", "MS: 01D/BV-01");
            } else if (maLoaiVatTu.equals("YDC")) {
                parameters.put("tieude", "PHIẾU LĨNH Y DỤNG CỤ");
                parameters.put("mauin", "MS: 03D/BV-01");
            } else if (maLoaiVatTu.equals("TH_CP")) {
                parameters.put("tieude", "PHIẾU LĨNH THUỐC CHẾ PHẨM YHCT");
                parameters.put("mauin", "MS: 01D/BV-01");
            } else if (maLoaiVatTu.equals("HC")) {
                parameters.put("tieude", "PHIẾU LĨNH HÓA CHẤT");
                parameters.put("mauin", "MS: 02D/BV-01");
            } else if (maLoaiVatTu.equals("SP")) {
                parameters.put("tieude", "PHIẾU LĨNH SINH PHẨM");
                parameters.put("mauin", "MS: 02D/BV-01");
            } else {
                parameters.put("tieude", "PHIẾU LĨNH THUỐC");
                parameters.put("mauin", "MS: 01D/BV-01");
            }
            String Tongtienchu = new DocSo().docso(Double.parseDouble(thanhTien));
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            String tenPhongBan = heThongDAO.getTenPhongBan(maPhongBan);
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("tenbenhvien", tenBenhVien.toUpperCase());
            parameters.put("idPhieuChuyen", idPhieuChuyen);
            parameters.put("MaLoaiVatTu", maLoaiVatTu);
            parameters.put("soPhieuChuyen", soPhieuChuyen);
            parameters.put("khonoitru", tenKhoVatTu);
            parameters.put("MAKHOVATTU", maKhoVatTu);
            parameters.put("sotienbangchu", Tongtienchu);
            parameters.put("khoanhan", tenKhoYeuCau);
            parameters.put("dvtt", dvtt);
            parameters.put("ngay", ngay);
            parameters.put("thang", thang);
            parameters.put("nam", nam);
            parameters.put("nguoiin", nguoiin);
            parameters.put("nguoilap", nguoiTao);
            parameters.put("TUTHUOC", trangthai);
            parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
            parameters.put("ngaytao", ngayChuyen);
            parameters.put("nguoiduyet", nguoiduyet);
            parameters.put("ngaygio", ngaygio);
            parameters.put("khoa", tenPhongBan);
            String ngaythangduyet = "Ngày " + arr_nk[0] + " tháng " + arr_nk[1] + " năm " + arr_nk[2];
            parameters.put("ngaythangduyet", ngaythangduyet);

            File reportFile;
            if (maLoaiVatTu.equals("TH_HTT")) {
                String reportName = "rp_phieulinhduoc_noitru_khoaphong_HTT.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
            } else {
                if (maLoaiVatTu.equals("TH_NGHIEN")) {
                    String reportName = "rp_phieulinhduoc_noitru_khoaphong_HTT.jasper";
                    reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
                } else {
                    String reportName = "rp_phieulinhduoc_noitru_khoaphong.jasper";
                    reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
                }
            }
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ApiOperation("In phiếu bổ sung dự trù khoa phòng")
    @ResponseBody
    @RequestMapping(value = "/printPhieuDuTruBoSungKhoaPhong", method = RequestMethod.GET)
    public void printPhieuDuTruBoSungKhoaPhong(@RequestParam(value = "dvtt") String dvtt,
                                               @RequestParam(value = "idPhieuChuyen") String idPhieuChuyen,
                                               @RequestParam(value = "soLuuTru") String soLuuTru,
                                               @RequestParam(value = "ngayChuyen") String ngayChuyen,
                                               @RequestParam(value = "ngayPhieuChuyen") String ngayPhieuChuyen,
                                               @RequestParam(value = "tongTien") String tongTien,
                                               @RequestParam(value = "maKhoVatTu") String maKho,
                                               @RequestParam(value = "tenKhoVatTu") String tenKho,
                                               @RequestParam(value = "maLoaiVatTu") String maLoaiVatTu,
                                               @RequestParam(value = "tenKhoYC") String tenKhoYC,
                                               @RequestParam(value = "maPhongBan") String maPhongBan,
                                               @RequestParam(value = "maNguoiIn") String maNguoiIn, // tên người tạo
                                               @RequestParam(value = "tenNguoiTao") String tenNguoiTao, // tên người tạo
                                               @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                               @RequestParam(value = "tenTinh") String tenTinh,
                                               @RequestParam(value = "loaiFile") String loaiFile,
                                               HttpServletRequest request,
                                               HttpServletResponse response,
                                               HttpSession session) {
        try {
            Map parameters = new HashMap();
            String[] arr_nk = ngayChuyen.split("/");
            String ngay = arr_nk[0];
            String thang = arr_nk[1];
            String nam = arr_nk[2];
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            String tenPhongBan = heThongDAO.getTenPhongBan(maPhongBan);
            parameters.put("idPhieuChuyen", idPhieuChuyen);
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("tenbenhvien", tenBenhVien.toUpperCase());
            parameters.put("khoa", tenPhongBan);
            parameters.put("MaLoaiVatTu", maLoaiVatTu);
            parameters.put("sotienbangchu", Double.parseDouble(tongTien));
            parameters.put("sotienbangchu_vtu", new DocSo().docso(Double.parseDouble(tongTien)));
            parameters.put("soluutru", soLuuTru);
            parameters.put("khonoitru", tenKho); //TENKHO - kho chuyển
            parameters.put("MAKHOVATTU", maKho);
            parameters.put("khoanhan", tenKhoYC);//TENPHONGBAN - tủ trực dự trù
            parameters.put("dvtt", dvtt);
            parameters.put("ngay", ngay);
            parameters.put("thang", thang);
            parameters.put("nam", nam);
            parameters.put("nguoilap", tenNguoiTao);
            parameters.put("nguoiin", maNguoiIn);
            parameters.put("TUTHUOC", "0");
            parameters.put("ngaytao", ngayPhieuChuyen);
            parameters.put("nguoiduyet", "");
            parameters.put("ngaygio", new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new java.util.Date()));
            parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
            File reportFile;
            String reportName = "rp_phieulinhduoc_noitru_khoaphong_lsg.jasper";
            reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/printPhieuDuTruDinhMuc", method = RequestMethod.GET)
    public @ResponseBody
    void printPhieuDuTruDinhMuc(HttpServletResponse response,
                                HttpServletRequest request,
                                HttpSession session,
                                @RequestParam(value = "dvtt") String dvtt,
                                @RequestParam(value = "idPhieuChuyen") long idPhieuChuyen,
                                @RequestParam(value = "maNghiepVu") long maNghiepVu,
                                @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                @RequestParam(value = "tenTinh") String tenTinh,
                                @RequestParam(value = "loaiFile") String loaiFile
    ) {
        try {
            String tenbenhvien = tenBenhVien.toUpperCase();
            Map<String, Object> thongTinPhieuChuyen = chuyenKhoDAO.getThongTinPhieuChuyenKho(dvtt, idPhieuChuyen, maNghiepVu);
            Timestamp ts = Timestamp.valueOf(thongTinPhieuChuyen.get("NGAYPHIEUCHUYEN").toString());
            String dy = ts.toString().substring(0, 10);
            String [] ngaychuyen = dy.split("-");
            String ts3636 = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 3636);
            Map parameters = new HashMap();
            parameters.put("dvtt", dvtt);
            parameters.put("IDChuyenKhoVatTu", idPhieuChuyen);
            parameters.put("maNghiepVu", maNghiepVu);
            parameters.put("sophieu", thongTinPhieuChuyen.get("SoPhieuChuyen").toString());
            parameters.put("NoiChuyen", thongTinPhieuChuyen.get("Khogiao").toString());
            parameters.put("NoiYeuCau", thongTinPhieuChuyen.get("Khonhan").toString());
            parameters.put("tenbenhvien", tenbenhvien);
            parameters.put("lydoxuatkho", thongTinPhieuChuyen.get("Ghichu").toString());
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("ngay", ts3636.equals("1") ? ngaychuyen[2] : MonthDay.now().getDayOfMonth() + "");
            parameters.put("thang", ts3636.equals("1") ? ngaychuyen[1] : MonthDay.now().getMonthValue() + "");
            parameters.put("nam", ts3636.equals("1") ? ngaychuyen[0] : YearMonth.now().getYear() + "");
            String tongtien;
            try {
                tongtien = chuyenKhoDAO.getTongTienPhieuChuyenKho(idPhieuChuyen).toString();
            } catch (Exception e) {
                tongtien = "0";
            }
            parameters.put("sotienbangchu", new DocSo().docso(Double.parseDouble(tongtien)));
            parameters.put("tieude", "PHIẾU LĨNH");
            File reportFile;
            String reportName = "rp_phieuxuatdinhmuc.jasper";
            reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @RequestMapping(value = "/printPhieuHoanTraKhoaPhong", method = RequestMethod.GET)
    public @ResponseBody
    void printPhieuHoanTraKhoaPhong(@RequestParam(value = "dvtt") String dvtt,
                                    @RequestParam(value = "idPhieuChuyen") String idPhieuChuyen,
                                    @RequestParam(value = "soPhieuChuyen") String soPhieuChuyen,
                                    @RequestParam(value = "ngayChuyen") String ngayChuyen,
                                    @RequestParam(value = "thanhTien") String thanhTien,
                                    @RequestParam(value = "maKhoVatTu") String maKhoVatTu,
                                    @RequestParam(value = "tenKhoVatTu") String tenKhoVatTu, // Kho chuyển
                                    @RequestParam(value = "tenKhoYC") String tenKhoYC, // Kho yêu cầu
                                    @RequestParam(value = "maLoaiVatTu") String maLoaiVatTu,
                                    @RequestParam(value = "nguoiIn") String nguoiIn,
                                    @RequestParam(value = "nguoiTao") String nguoiTao,
                                    @RequestParam(value = "maPhongBan") String maPhongBan,
                                    @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                    @RequestParam(value = "tenTinh") String tenTinh,
                                    @RequestParam(value = "loaiFile") String loaiFile,
                                    HttpServletRequest request,
                                    HttpServletResponse response,
                                    HttpSession session) {
        try {
            Map parameters = new HashMap();
            String[] ngaychuyen = ngayChuyen.split("/");
            String ngay = ngaychuyen[0];
            String thang = ngaychuyen[1];
            String nam = ngaychuyen[2];

            String tenPhongBan = heThongDAO.getTenPhongBan(maPhongBan);
            String phieulinhthuoc = heThongDAO.getMoTaThamSoByMaThamSo(dvtt,820002);
            String phieulinhdichtruyen = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 820004);
            if (maLoaiVatTu.equals("TH")) {
                if (phieulinhthuoc.equals("1")) {
                    parameters.put("tieude", "PHIẾU HOÀN TRẢ THUỐC");
                } else {
                    parameters.put("tieude", "PHIẾU HOÀN TRẢ THUỐC THƯỜNG");
                }
            } else if (maLoaiVatTu.equals("TH_HTT")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ THUỐC THÀNH PHẨM HƯỚNG TÂM THẦN");
            } else if (maLoaiVatTu.equals("VT_TH")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ VẬT TƯ TIÊU HAO");
            } else if (maLoaiVatTu.equals("VT_TT")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ VẬT TƯ THAY THẾ");
            } else if (maLoaiVatTu.equals("DICHTRUYEN")) {
                if (phieulinhdichtruyen.equals("1")) {
                    parameters.put("tieude", "PHIẾU HOÀN TRẢ THUỐC");
                } else {
                    parameters.put("tieude", "PHIẾU HOÀN TRẢ DỊCH TRUYỀN");
                }
            } else if (maLoaiVatTu.equals("TH_NGHIEN")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ THUỐC THÀNH PHẨM GÂY NGHIỆN");
            } else if (maLoaiVatTu.equals("VT_YT")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ VẬT TƯ Y TẾ");
            } else if (maLoaiVatTu.equals("YDC")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ Y DỤNG CỤ");
            } else if (maLoaiVatTu.equals("HC")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ HÓA CHẤT");
            } else {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ DƯỢC NỘI TRÚ");
            }
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            String tenBenhVienStr = tenBenhVien.toUpperCase();
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("tenbenhvien", tenBenhVienStr);
            parameters.put("khoa", tenPhongBan);
            parameters.put("maLoaiVatTu", maLoaiVatTu);
            parameters.put("tongTien",new BigDecimal(thanhTien));
            parameters.put("sotienbangchu", new DocSo().docso(Double.parseDouble(thanhTien)));
            parameters.put("soPhieuChuyen", soPhieuChuyen);
            parameters.put("idPhieuChuyen", idPhieuChuyen);
            parameters.put("khonoitru", tenKhoYC);
            parameters.put("maKhoVatTu", maKhoVatTu);
            parameters.put("khoanhan", tenKhoVatTu);
            parameters.put("dvtt", dvtt);
            parameters.put("ngay", ngay);
            parameters.put("thang", thang);
            parameters.put("nam", nam);
            parameters.put("nguoiin", nguoiIn);
            parameters.put("nguoilap", nguoiTao);
            parameters.put("TUTHUOC", "0");
            parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
            File reportFile;
            if (maLoaiVatTu.equals("TH_HTT") || maLoaiVatTu.equals("TH_NGHIEN")) {
                String reportName = "rp_phieuhoantraduoc_noitru_khoaphong_HTT.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
            } else {
                String reportName = "rp_phieuhoantraduoc_noitru_khoaphong.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
            }
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
