package com.vnpt.duoc.duocModule.dao.duyetdutruhoantra;

import com.vnpt.duoc.jdbc.JdbcTemplate;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DuyetDuTruHoanTraDAOImp implements DuyetDuTruHoanTraDAO {
    private final HikariDataSource dataSourceDuocV2;

    public DuyetDuTruHoanTraDAOImp(HikariDataSource dataSourceDuocV2) {
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    // Dự trù nội trú
    @Override
    public List getDanhSachPhieuDuTruNoiTru(String dvtt, String tuNgay, String denNgay, String maPhongBan, int trangThai, int isBANT) {
        String sql = "call HIS_DUOCV2.DC_DTRU_NOITRU_DSPHIEUTONG_SEL (?,?,?,?,?,?)#c,s,t,t,s,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{ dvtt, tuNgay, denNgay, maPhongBan, trangThai, isBANT });
    }

    @Override
    public List getDanhSachChiTietPhieuDutru(String dvtt, long idPhieuDuTruTong, String maPhongBan, int hinhThucXem, int thoiGian) {
        String sql = "call HIS_DUOCV2.NOITRU_DSVATTU_PHIEUDUTRU_V2 (?,?,?,?,?)#c,s,l,s,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{ dvtt, idPhieuDuTruTong, maPhongBan , hinhThucXem, thoiGian});
    }

    @Override
    public List getDanhSachBanInPhieuDuTru(String dvtt, long idPhieuDuTruTong, int tuTuThuoc, int tachLoai) {
        String sql = "call HIS_DUOCV2.NOITRU_PHIEUDUTRU_TONGTIEN(?,?,?,?)#c,s,l,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, idPhieuDuTruTong, tuTuThuoc, tachLoai});
    }

    @Override
    public List getTenBNPhieuDuTru(long idPhieuDuTruTong, String DVTT) {
        String sql = "call HIS_DUOCV2.DANHSACH_TENBN_TENPHONG(?,?)#c,l,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{idPhieuDuTruTong, DVTT});
    }

    @Override
    public List getDanhSachSoYLenhTatCa(int maKhoa, String dvtt, int tuTuThuoc, String tuNgay, String denNgay) {
        String sql = "call HIS_DUOCV2.NOITRU_SOYLENH_PDT_TATCA (?,?,?,?,?)#c,l,s,l,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{maKhoa, dvtt, tuTuThuoc, tuNgay, denNgay});
    }

    @Override
    public List getDanhSachSoYLenhPhieuDuTru(long idPhieuDuTruTong, String soPhieuLuuTru, String dvtt, int tuTuThuoc, String maPhong, String tuNgay, String denNgay) {
        String sql = "call HIS_DUOCV2.NOITRU_SOYLENH_PDT_PHONG (?,?,?,?,?,?,?)#c,l,s,s,l,s,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{idPhieuDuTruTong, soPhieuLuuTru, dvtt, tuTuThuoc, maPhong, tuNgay, denNgay});
    }

    @Override
    public List getDanhSachExportExcelPhieuDuTru(String dvtt, String tuNgay, String denNgay, int duyet, int isBANT) {
        String sql = "call HIS_DUOCV2.DSVATTU_DUYET_PHIEUDUTRU_EXCEL(?,?,?,?,?)#c,s,t,t,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, tuNgay, denNgay, duyet, isBANT});
    }

    @Override
    public int duyetPhieuDuTru(String dvtt, long idPhieuDuTruTong, int duyetPhieu, long maUser, String ngayDuyet, String idGiaoDich, String maKhoa, String maPhong) {
        String sql = "call HIS_DUOCV2.DC_DUYET_NTRU_DTRU_DUYETPHIEU (?,?,?,?,?,?,?,?)#l,s,l,l,l,t,l,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForInt(sql, new Object[]{ dvtt, idPhieuDuTruTong, duyetPhieu, maUser, ngayDuyet, idGiaoDich, maKhoa, maPhong });
    }

    // Hoàn trả nội trú
    @Override
    public List getDanhSachPhieuHoanTraNoiTru(String dvtt, String tuNgay, String denNgay, int trangThai, int isBANT, long maNhanVien) {
        String sql = "call HIS_DUOCV2.NOITRU_HOANTRA_DSPHIEUDUYET (?,?,?,?,?,?)#c,s,t,t,l,l,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, tuNgay, denNgay, trangThai, isBANT, maNhanVien});
    }

    @Override
    public List getDanhSachChiTietPhieuHoanTra(long idPhieuHoanTraTong, int hinhThucXem) {
        String sql = "call HIS_DUOCV2.NOITRU_DSVATTU_PHIEUHOANTRA_V2 (?,?)#c,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{idPhieuHoanTraTong, hinhThucXem});
    }

    @Override
    public List getDanhSachBanInPhieuHoanTra(String dvtt, long idPhieuHoanTraTong, int tuTuThuoc, int tachLoai) {
        String sql = "call HIS_DUOCV2.NOITRU_PHHOANTRA_TONGTIEN(?,?,?,?)#c,s,l,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, idPhieuHoanTraTong, tuTuThuoc, tachLoai});
    }

    @Override
    public List getDanhSachExportExcelPhieuHoanTra(String dvtt, String tuNgay, String denNgay, int duyet, int isBANT) {
        String sql = "call HIS_DUOCV2.DSVATTU_DUYET_HOANTRA_EXCEL(?,?,?,?,?)#c,s,t,t,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, tuNgay, denNgay, duyet, isBANT});
    }

    @Override
    public int duyetPhieuHoanTra(String dvtt, long idPhieuHoanTra, int duyetPhieu, long maUser, String ngayDuyet, String idGiaoDich, String maKhoa, String maPhong) {
        String sql = "call HIS_DUOCV2.DC_HTRA_NOITRU_DUYET (?,?,?,?,?,?,?,?)#l,s,l,l,l,t,l,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForInt(sql, new Object[]{dvtt, idPhieuHoanTra, duyetPhieu, maUser, ngayDuyet, idGiaoDich, maKhoa, maPhong});
    }
}
