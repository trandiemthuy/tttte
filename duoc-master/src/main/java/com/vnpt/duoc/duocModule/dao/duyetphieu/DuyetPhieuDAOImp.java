package com.vnpt.duoc.duocModule.dao.duyetphieu;

import com.vnpt.duoc.jdbc.JdbcTemplate;
import com.zaxxer.hikari.HikariDataSource;
import liquibase.pro.packaged.J;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class DuyetPhieuDAOImp implements DuyetPhieuDAO {
    private final HikariDataSource dataSourceDuocV2;

    public DuyetPhieuDAOImp(HikariDataSource dataSourceDuocV2) {
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    @Override
    public List getDanhSachKhoChuyenTheoNhanVien(String dvtt, String maNhanVien) {
        String sql = "call HIS_MANAGER.dc_sp_duyetphieu_ds_kho(?,?)#c,s,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, maNhanVien});
    }

    @Override
    public List getDanhSachPhieuChuyenTheoNghiepVu(Date tuNgay, Date denNgay, long maNghiepVu, long maKho, long trangThai, String dvtt) {
        String sql = "call HIS_DUOCV2.DC_DUYET_PHIEU_SEL(?,?,?,?,?,?)#c,t,t,l,l,l,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, tuNgay,
            denNgay,
            maNghiepVu,
            maKho,
            trangThai,
            dvtt);
    }

    // Duyệt
    @Override
    public int updateSoLuongDuyet(String dvtt, long idChuyenKhoChiTiet, Double soLuongYeuCau, Double soLuongDuyet, String idGiaoDich, long maUser, String maKhoa, String maPhong) {
        String sql = "call HIS_DUOCV2.DC_CHUYENKHO_SLDUYET_UPDATE(?,?,?,?,?,?,?,?)#l,s,l,l,l,l,l,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{
            dvtt, idChuyenKhoChiTiet, soLuongYeuCau, soLuongDuyet, idGiaoDich, maUser, maKhoa, maPhong
        }, Integer.class);
    }

    @Override
    public int duyetPhieuChuyenKho(String dvtt, long idPhieuChuyen, Date ngayDuyet, long nguoiDuyet, String idGiaoDich, String maKhoa, String maPhong) {
        String sql = "call HIS_DUOCV2.DC_CHUYENKHO_PHIEU_DUYET(?,?,?,?,?,?,?)#l,s,l,t,l,l,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{
            dvtt,
            idPhieuChuyen,
            ngayDuyet,
            nguoiDuyet,
            idGiaoDich,
            maKhoa,
            maPhong
        }, Integer.class);
    }

    @Override
    public int huyDuyetChuyenKho(String dvtt, long idPhieuChuyen, long nguoiDuyet, String idGiaoDich, String maKhoa, String maPhong) {
        String sql = "call HIS_DUOCV2.DC_CHUYENKHO_PHIEU_HUYDUYET(?,?,?,?,?,?)#l,s,l,l,l,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{
            dvtt,
            idPhieuChuyen,
            nguoiDuyet,
            idGiaoDich,
            maKhoa,
            maPhong
        }, Integer.class);
    }

    // Print
    @Override
    public List getDanhSachVatTuChiTietPhieuChuyen(String dvtt, long idPhieuChuyen) {
        String sql = "call HIS_DUOCV2.DSVATTU_XUATCHUYENKHO(?,?)#c,l,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, idPhieuChuyen, dvtt);
    }

    @Override
    public List getDanhSachVatTuPhieuChuyen(String dvtt, Date tuNgay, Date denNgay, long maKho, long trangThai) {
        String sql = "call HIS_DUOCV2.DC_RP_DUYETPHIEU_CK_EXCEL(?,?,?,?,?)#c,t,t,s,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql,
            tuNgay,
            denNgay,
            dvtt,
            maKho,
            trangThai);
    }
}
