package com.vnpt.duoc.duocModule.objects.xuatduoc;

public class ThongTinPhieuXuatDuocObj {
    private String dvtt;
    private String maNghiepVu;
    private String soPhieuTT;
    private String tenBenhNhan;
    private String ngayXuat;
    private long tuoi;
    private long namSinh;
    private String thoiGianXuatThuoc;
    private long phanTramBaoHiem;
    private String ngayHoanTatKham;
    private String chanDoan; // icd
    private long soVaoVien;
    private String maToaThuoc;
    private String maKhamBenh;
    private long maBenhNhan;
    private long vuotCan; // 0: Không vượt cận 1: Vượt cận
    private long daThanhToan; // 0: Chưa thanh toán 1: Đã thanh toán
    private String doiTuong;
    private long dungTuyen;
    private String ngayKhamBenh;
    private String maKhoa;
    private String maPhong;
    private long maNguoiXuat;
    private long idGiaoDich;

    public long getIdGiaoDich() {
        return idGiaoDich;
    }

    public void setIdGiaoDich(long idGiaoDich) {
        this.idGiaoDich = idGiaoDich;
    }

    public long getMaNguoiXuat() {
        return maNguoiXuat;
    }

    public void setMaNguoiXuat(long maNguoiXuat) {
        this.maNguoiXuat = maNguoiXuat;
    }

    public String getDvtt() {
        return dvtt;
    }

    public void setDvtt(String dvtt) {
        this.dvtt = dvtt;
    }

    public String getMaNghiepVu() {
        return maNghiepVu;
    }

    public void setMaNghiepVu(String maNghiepVu) {
        this.maNghiepVu = maNghiepVu;
    }

    public String getSoPhieuTT() {
        return soPhieuTT;
    }

    public void setSoPhieuTT(String soPhieuTT) {
        this.soPhieuTT = soPhieuTT;
    }

    public String getTenBenhNhan() {
        return tenBenhNhan;
    }

    public void setTenBenhNhan(String tenBenhNhan) {
        this.tenBenhNhan = tenBenhNhan;
    }

    public String getNgayXuat() {
        return ngayXuat;
    }

    public void setNgayXuat(String ngayXuat) {
        this.ngayXuat = ngayXuat;
    }

    public long getTuoi() {
        return tuoi;
    }

    public void setTuoi(long tuoi) {
        this.tuoi = tuoi;
    }

    public long getNamSinh() {
        return namSinh;
    }

    public void setNamSinh(long namSinh) {
        this.namSinh = namSinh;
    }

    public String getThoiGianXuatThuoc() {
        return thoiGianXuatThuoc;
    }

    public void setThoiGianXuatThuoc(String thoiGianXuatThuoc) {
        this.thoiGianXuatThuoc = thoiGianXuatThuoc;
    }

    public long getPhanTramBaoHiem() {
        return phanTramBaoHiem;
    }

    public void setPhanTramBaoHiem(long phanTramBaoHiem) {
        this.phanTramBaoHiem = phanTramBaoHiem;
    }

    public String getNgayHoanTatKham() {
        return ngayHoanTatKham;
    }

    public void setNgayHoanTatKham(String ngayHoanTatKham) {
        this.ngayHoanTatKham = ngayHoanTatKham;
    }

    public String getChanDoan() {
        return chanDoan;
    }

    public void setChanDoan(String chanDoan) {
        this.chanDoan = chanDoan;
    }

    public long getSoVaoVien() {
        return soVaoVien;
    }

    public void setSoVaoVien(long soVaoVien) {
        this.soVaoVien = soVaoVien;
    }

    public String getMaToaThuoc() {
        return maToaThuoc;
    }

    public void setMaToaThuoc(String maToaThuoc) {
        this.maToaThuoc = maToaThuoc;
    }

    public String getMaKhamBenh() {
        return maKhamBenh;
    }

    public void setMaKhamBenh(String maKhamBenh) {
        this.maKhamBenh = maKhamBenh;
    }

    public long getMaBenhNhan() {
        return maBenhNhan;
    }

    public void setMaBenhNhan(long maBenhNhan) {
        this.maBenhNhan = maBenhNhan;
    }

    public long getVuotCan() {
        return vuotCan;
    }

    public void setVuotCan(long vuotCan) {
        this.vuotCan = vuotCan;
    }

    public long getDaThanhToan() {
        return daThanhToan;
    }

    public void setDaThanhToan(long daThanhToan) {
        this.daThanhToan = daThanhToan;
    }

    public String getDoiTuong() {
        return doiTuong;
    }

    public void setDoiTuong(String doiTuong) {
        this.doiTuong = doiTuong;
    }

    public long getDungTuyen() {
        return dungTuyen;
    }

    public void setDungTuyen(long dungTuyen) {
        this.dungTuyen = dungTuyen;
    }

    public String getNgayKhamBenh() {
        return ngayKhamBenh;
    }

    public void setNgayKhamBenh(String ngayKhamBenh) {
        this.ngayKhamBenh = ngayKhamBenh;
    }

    public String getMaKhoa() {
        return maKhoa;
    }

    public void setMaKhoa(String maKhoa) {
        this.maKhoa = maKhoa;
    }

    public String getMaPhong() {
        return maPhong;
    }

    public void setMaPhong(String maPhong) {
        this.maPhong = maPhong;
    }
}
