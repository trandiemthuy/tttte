package com.vnpt.duoc.duocModule.dao.xuatduocbanle;

import com.vnpt.duoc.duocModule.objects.khamchuabenh.ToaThuocNgoaiTruObj;
import com.vnpt.duoc.duocModule.objects.xuatduocbanle.ThongTinPhieuXuatDuocBanLeObj;
import com.vnpt.duoc.jdbc.JdbcTemplate;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class XuatDuocBanLeDAOImp implements XuatDuocBanLeDAO {

    private JdbcTemplate jdbcTemplate;

    public XuatDuocBanLeDAOImp(HikariDataSource dataSourceDuocV2) {
        this.jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
    }

    @Override
    public List getDanhSachToaThuocBanLe(String dvtt, String ngay) {
        String sql = "call HIS_MANAGER.KB_TOATHUOC_BANLE_SEL(?,?)#c,s,t";
        return jdbcTemplate.queryForList(sql, new Object[]{
            dvtt, ngay
        });
    }

    @Override
    public int addToaThuocBanLe(ThongTinPhieuXuatDuocBanLeObj thongTinPhieuXuatDuocBanLeObj) {
        String sql = "call HIS_MANAGER.KB_TOATHUOC_BANLE_INS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)#l,s,s,t,t,l,l,l,s,s,s,s,s,s,l,s";
        return jdbcTemplate.queryForObject(sql, new Object[]{
            thongTinPhieuXuatDuocBanLeObj.getTenKhachHang(),
            thongTinPhieuXuatDuocBanLeObj.getGhiChu(),
            thongTinPhieuXuatDuocBanLeObj.getNgayXuat(),
            thongTinPhieuXuatDuocBanLeObj.getNgayXuat(),
            thongTinPhieuXuatDuocBanLeObj.getMaNhanVien(),
            thongTinPhieuXuatDuocBanLeObj.getMaPhongBan(),
            thongTinPhieuXuatDuocBanLeObj.getMaPhongBenh(),
            thongTinPhieuXuatDuocBanLeObj.getDvtt(),
            thongTinPhieuXuatDuocBanLeObj.getMaBacSi(),
            thongTinPhieuXuatDuocBanLeObj.getMaICD(),
            thongTinPhieuXuatDuocBanLeObj.getTenICD(),
            thongTinPhieuXuatDuocBanLeObj.getTenBenhPhu(),
            thongTinPhieuXuatDuocBanLeObj.getDiaChi(),
            thongTinPhieuXuatDuocBanLeObj.getTuoi(),
            thongTinPhieuXuatDuocBanLeObj.getLiDoXuat()
        }, int.class);
    }

    @Override
    public int updateToaThuocBanLe(ThongTinPhieuXuatDuocBanLeObj thongTinPhieuXuatDuocBanLeObj) {
        String sql = "call HIS_MANAGER.KB_TOATHUOC_BANLE_UPD(?,?,?,?,?,?,?,?,?,?,?)#l,l,s,s,l,l,l,s,s,s,s,s";
        return jdbcTemplate.queryForObject(sql, new Object[]{
            thongTinPhieuXuatDuocBanLeObj.getMaToaThuoc(),
            thongTinPhieuXuatDuocBanLeObj.getTenKhachHang(),
            thongTinPhieuXuatDuocBanLeObj.getGhiChu(),
            thongTinPhieuXuatDuocBanLeObj.getMaNhanVien(),
            thongTinPhieuXuatDuocBanLeObj.getMaPhongBan(),
            thongTinPhieuXuatDuocBanLeObj.getMaPhongBenh(),
            thongTinPhieuXuatDuocBanLeObj.getDvtt(),
            thongTinPhieuXuatDuocBanLeObj.getMaBacSi(),
            thongTinPhieuXuatDuocBanLeObj.getMaICD(),
            thongTinPhieuXuatDuocBanLeObj.getTenICD(),
            thongTinPhieuXuatDuocBanLeObj.getTenBenhPhu()
        }, int.class);
    }

    @Override
    public int checkDeleteToaThuocBanLe(String maToaThuoc, String dvtt) {
        String sql = "call HIS_DUOCV2.CHECK_DELETE_TOA_THUOC_BAN_LE(?,?)#l,s,s";
        return jdbcTemplate.queryForObject(sql, new Object[]{maToaThuoc, dvtt}, int.class);
    }

    @Override
    public int deleteToaThuocBanLe(String maToaThuoc, String dvtt) {
        String sql = "call HIS_MANAGER.KB_TOATHUOC_BANLE_DEL(?,?)#s,s,l";
        return jdbcTemplate.queryForObject(sql, new Object[]{dvtt, maToaThuoc}, int.class);
    }

    @Override
    public List getDanhSachChiTietThuocBanLe(String maToaThuoc, String dvtt, String nghiepVu) {
        String sql = "call HIS_MANAGER.KB_NGT_TOABANLE_SELECT(?,?,?)#c,s,s,s";
        return jdbcTemplate.queryForList(sql, new Object[]{maToaThuoc, dvtt, nghiepVu});
    }

    @Override
    public int deleteChiTietToaThuocBanLe(ToaThuocNgoaiTruObj toaThuocNgoaiTruObj) {
        String sql = "call HIS_DUOCV2.KB_NGOAI_CT_TOABANLE_DEL(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)#l,s,s,s,s,s,d,l,d,s,l,l,l,l,s,s";
        return jdbcTemplate.queryForObject(sql, new Object[]{
            toaThuocNgoaiTruObj.getSttToaThuoc(),
            toaThuocNgoaiTruObj.getMaToaThuoc(),
            toaThuocNgoaiTruObj.getDvtt(),
            toaThuocNgoaiTruObj.getMaKhamBenh(),
            toaThuocNgoaiTruObj.getSoPhieuThanhToan(),
            toaThuocNgoaiTruObj.getThanhTien(),
            toaThuocNgoaiTruObj.getMaKhoVatTu(),
            toaThuocNgoaiTruObj.getDonGiaBV(),
            toaThuocNgoaiTruObj.getNghiepVu(),
            toaThuocNgoaiTruObj.getMaVatTu(),
            toaThuocNgoaiTruObj.getSoVaoVien(),
            toaThuocNgoaiTruObj.getMaUser(),
            toaThuocNgoaiTruObj.getIdGiaoDich(),
            toaThuocNgoaiTruObj.getMaKhoa(),
            toaThuocNgoaiTruObj.getMaPhong()
        }, Integer.class);
    }

    @Override
    public int addChiTietToaThuocBanLe(ToaThuocNgoaiTruObj toaThuocNgoaiTruObj) {
        String sql = "call HIS_DUOCV2.KB_NT_CT_TT_BANLE_INS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" +
            "#l,s,s,l,l,s,s,s,d,d,d,d,d,d,d,d,d,d,t,s,l,s,l,l,l,s,s,s,t,l,l,l,l,s,s";
        return jdbcTemplate.queryForObject(sql, new Object[]{
            toaThuocNgoaiTruObj.getDvtt(),
            toaThuocNgoaiTruObj.getMaToaThuoc(),
            toaThuocNgoaiTruObj.getMaKhoVatTu(),
            toaThuocNgoaiTruObj.getMaVatTu(),
            toaThuocNgoaiTruObj.getTenVatTu(),
            toaThuocNgoaiTruObj.getTenGoc(),
            toaThuocNgoaiTruObj.getNghiepVu(),
            toaThuocNgoaiTruObj.getSoLuong(),
            toaThuocNgoaiTruObj.getSoLuongThucLinh(),
            toaThuocNgoaiTruObj.getDonGiaBV(),
            toaThuocNgoaiTruObj.getDonGiaBH(),
            toaThuocNgoaiTruObj.getThanhTien(),
            toaThuocNgoaiTruObj.getSoNgay(),
            toaThuocNgoaiTruObj.getSang(),
            toaThuocNgoaiTruObj.getTrua(),
            toaThuocNgoaiTruObj.getChieu(),
            toaThuocNgoaiTruObj.getToi(),
            toaThuocNgoaiTruObj.getNgayRaToa(),
            toaThuocNgoaiTruObj.getGhiChu(),
            toaThuocNgoaiTruObj.getMaBacSi(),
            toaThuocNgoaiTruObj.getCachSuDung(),
            toaThuocNgoaiTruObj.getCoBHYT(),
            toaThuocNgoaiTruObj.getNamHienTai(),
            toaThuocNgoaiTruObj.getMaBenhNhan(),
            toaThuocNgoaiTruObj.getSoPhieuThanhToan(),
            toaThuocNgoaiTruObj.getIdTiepNhan(),
            toaThuocNgoaiTruObj.getMaKhamBenh(),
            toaThuocNgoaiTruObj.getNgayHienTai(),
            toaThuocNgoaiTruObj.getDaThanhToan(),
            toaThuocNgoaiTruObj.getSoVaoVien(),
            toaThuocNgoaiTruObj.getMaUser(),
            toaThuocNgoaiTruObj.getIdGiaoDich(),
            toaThuocNgoaiTruObj.getMaKhoa(),
            toaThuocNgoaiTruObj.getMaPhong()
        }, Integer.class);
    }
}
