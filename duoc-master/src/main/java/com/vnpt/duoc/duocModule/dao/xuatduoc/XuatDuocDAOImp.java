package com.vnpt.duoc.duocModule.dao.xuatduoc;

import com.vnpt.duoc.duocModule.objects.xuatduoc.ThongTinPhieuXuatDuocObj;
import com.vnpt.duoc.jdbc.JdbcTemplate;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class XuatDuocDAOImp implements XuatDuocDAO {
    private JdbcTemplate jdbcTemplate;

    public XuatDuocDAOImp(HikariDataSource dataSourceDuocV2) {
        this.jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
    }

    @Override
    public List getDanhSachBenhNhanXuatDuoc(String dvtt, int maNghiepVu, String ngay, int trangThai, String maKhoa) {
        String sql = "call HIS_DUOCV2.DC_SP_DSBENHNHANLANHDUOC_V2(?,?,?,?,?)#c,s,l,s,l,s";
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, maNghiepVu, ngay, trangThai, maKhoa});
    }

    @Override
    public List getDanhSachVatTuXuatDuoc(String dvtt, int maNghiepVu, String maToaThuoc, int trangThai, int soVaoVien) {
        String sql = "call HIS_DUOCV2.DC_SP_CTTOATHUOC_NV3_V2(?,?,?,?,?)#c,s,l,s,l,l";
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, maNghiepVu, maToaThuoc, trangThai, soVaoVien});
    }

    @Override
    public int xuatDuocVatTu(ThongTinPhieuXuatDuocObj thongTinPhieuXuatDuocObj, int trangThai) {
        String sql = "call HIS_DUOCV2.DC_SP_XUATDUOC_VATTU_SVV_V2(?,?,?,?,?,?,?,?,?,?,?,?,?,?)#l,s,l,s,l,l,l,l,t,t,l,l,l,s,s";
        return jdbcTemplate.queryForObject(sql, new Object[]{
            thongTinPhieuXuatDuocObj.getDvtt(),
            thongTinPhieuXuatDuocObj.getMaNghiepVu(),
            thongTinPhieuXuatDuocObj.getMaToaThuoc(),
            trangThai,
            thongTinPhieuXuatDuocObj.getDaThanhToan(),
            thongTinPhieuXuatDuocObj.getDungTuyen(),
            thongTinPhieuXuatDuocObj.getPhanTramBaoHiem(),
            new Date(),
            thongTinPhieuXuatDuocObj.getNgayKhamBenh(),
            thongTinPhieuXuatDuocObj.getSoVaoVien(),
            thongTinPhieuXuatDuocObj.getMaNguoiXuat(),
            thongTinPhieuXuatDuocObj.getIdGiaoDich(),
            thongTinPhieuXuatDuocObj.getMaKhoa(),
            thongTinPhieuXuatDuocObj.getMaPhong()
        }, Integer.class);
    }

    @Override
    public List getDanhSachXuatDuocTongHop(String dvtt, String ngayLap, String nghiepVu, String hinhThuc) {
        String sql = "call HIS_MANAGER.AGG_DC_SP_DSPHIEUXUATDUOC_NGAY(?,?,?,?)#c,s,s,s,s";
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, ngayLap, nghiepVu, hinhThuc});
    }

    @Override
    public int traThuocVeKho(ThongTinPhieuXuatDuocObj thongTinPhieuXuatDuocObj, String nghiepVu) {
        String sql = "call HIS_DUOCV2.KB_NGT_TRATHUOC_VEKHO(?,?,?,?,?,?,?,?)#l,s,l,s,l,s,l,s,s" ;
        return jdbcTemplate.queryForObject(sql, new Object[]{
            thongTinPhieuXuatDuocObj.getMaToaThuoc(),
            thongTinPhieuXuatDuocObj.getSoVaoVien(),
            thongTinPhieuXuatDuocObj.getDvtt(),
            thongTinPhieuXuatDuocObj.getMaNguoiXuat(),
            nghiepVu,
            thongTinPhieuXuatDuocObj.getIdGiaoDich(),
            thongTinPhieuXuatDuocObj.getMaKhoa(),
            thongTinPhieuXuatDuocObj.getMaPhong()
        }, Integer.class);
    }
}
