package com.vnpt.duoc.duocModule.dao.nhanduoc;

import java.util.Date;
import java.util.List;

public interface NhanDuocDAO {
    List getDanhSachPhieuNhanDuocVeKho(Date tuNgay, Date denNgay, String dvtt, int duyet, int maKho, String maPhongBan, int maNghiepVu);
    int nhanDuocVeKho(int maNghiepVu, String dvtt, long idChuyenKhoVatTu, int maKhoGiao, int maKhoNhan, Date ngayNhan, long nguoiNhan, String idGiaoDich, String maKhoa, String maPhong);
    int huyNhanDuocVeKho(long idChuyenKho, String dvtt, long nguoiHuy, String idGiaoDich, String maKhoa, String maPhong);
}
