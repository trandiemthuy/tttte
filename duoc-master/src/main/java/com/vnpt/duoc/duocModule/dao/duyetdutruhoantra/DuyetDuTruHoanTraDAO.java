package com.vnpt.duoc.duocModule.dao.duyetdutruhoantra;

import java.util.List;

public interface DuyetDuTruHoanTraDAO {
    // Dự trù nội trú
    List getDanhSachPhieuDuTruNoiTru(String dvtt, String tuNgay, String denNgay, String maPhongBan, int trangThai, int isBANT );
    List getDanhSachChiTietPhieuDutru(String dvtt, long idPhieuDuTruTong, String maPhongBan, int hinhThucXem, int thoiGian);

    List getDanhSachBanInPhieuDuTru(String dvtt, long idPhieuDuTruTong, int tuTuThuoc, int tachLoai);
    List getTenBNPhieuDuTru(long idPhieuDuTruTong, String dvtt);
    List getDanhSachSoYLenhTatCa(int maKhoa, String dvtt, int tuTuThuoc, String tuNgay, String denNgay);
    List getDanhSachSoYLenhPhieuDuTru(long idPhieuDutruTong, String soPhieuLuuTru, String dvtt, int tuTuThuoc, String maPhong, String tuNgay, String denNgay);
    List getDanhSachExportExcelPhieuDuTru(String dvtt, String tuNgay, String denNgay, int duyet, int isBANT);

    int duyetPhieuDuTru(String dvtt, long idPhieuDuTruTong, int duyetPhieu, long maUser, String ngayDuyet,String idGiaoDich, String maKhoa, String maPhong);

    // Hoàn trả nội trú
    List getDanhSachPhieuHoanTraNoiTru(String dvtt, String tuNgay, String denNgay, int trangThai, int isBANT, long maNhanVien);
    List getDanhSachChiTietPhieuHoanTra(long idPhieuHoanTraTong, int hinhThucXem);

    List getDanhSachBanInPhieuHoanTra(String dvtt, long idPhieuHoanTraTong, int tuTuThuoc,int tachLoai);
    List getDanhSachExportExcelPhieuHoanTra(String dvtt, String tuNgay, String denNgay, int duyet, int isBANT);

    int duyetPhieuHoanTra(String dvtt, long idPhieuHoanTra, int duyetPhieu, long maUser, String ngayDuyet, String idGiaoDich, String maKhoa, String maPhong);
}
