package com.vnpt.duoc.duocModule.dao.xuatduocbanle;
import com.vnpt.duoc.duocModule.objects.khamchuabenh.ToaThuocNgoaiTruObj;
import com.vnpt.duoc.duocModule.objects.xuatduocbanle.ThongTinPhieuXuatDuocBanLeObj;

import java.util.List;

public interface XuatDuocBanLeDAO {
    List getDanhSachToaThuocBanLe(String dvtt, String ngay);

    int addToaThuocBanLe(ThongTinPhieuXuatDuocBanLeObj thongTinPhieuXuatDuocBanLeObj);
    int updateToaThuocBanLe(ThongTinPhieuXuatDuocBanLeObj thongTinPhieuXuatDuocBanLeObj);
    int checkDeleteToaThuocBanLe(String maToaThuoc, String dvtt);
    int deleteToaThuocBanLe(String maToaThuoc, String dvtt);

    List getDanhSachChiTietThuocBanLe(String maToaThuoc, String dvtt, String nghiepVu);
    int deleteChiTietToaThuocBanLe(ToaThuocNgoaiTruObj toaThuocNgoaiTruObj);
    int addChiTietToaThuocBanLe(ToaThuocNgoaiTruObj toaThuocNgoaiTruObj);
}
