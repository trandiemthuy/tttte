package com.vnpt.duoc.duocModule;

import com.vnpt.duoc.jdbc.JdbcTemplate;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.stereotype.Repository;

@Repository
public class DuocTransaction {
    public static String insert(long idGiaoDich, long idLoaiGiaoDich, String noidung, long maUser, String dvtt, int maLoi,
                                String clientIP, String hostIP, String noiDungLoi, String API, HikariDataSource dataSource){
        String sql = "call HIS_DUOCV2.DC_GIAODICH_INS(?,?,?,?,?,?,?,?,?,?)#s,l,l,s,l,s,l,s,s,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject(sql, new Object[]{
            idGiaoDich,
            idLoaiGiaoDich,
            noidung,
            maUser,
            dvtt,
            maLoi,
            clientIP,
            hostIP,
            noiDungLoi,
            API
        }, String.class);
    }

    public static String getMoTaThamSoDuoc(String dvtt, long maThamSo, HikariDataSource dataSource) {
        String sql = "call HIS_DUOCV2.MOTATHAMSO_THEOMATHAMSO(?,?)#s,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject(sql, new Object[]{dvtt, maThamSo}, String.class);
    }
}
