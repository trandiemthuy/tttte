package com.vnpt.duoc.duocModule.controller.hethong;

import com.vnpt.duoc.duocModule.dao.hethong.HeThongDAO;
import com.zaxxer.hikari.HikariDataSource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/hethong")
@Api(value="HeThongController")
public class HeThongController {
    private HeThongDAO heThongDAO;
    private HikariDataSource dataSourceDuocV2;

    public HeThongController(HeThongDAO heThongDAO, HikariDataSource dataSourceDuocV2) {
        this.heThongDAO = heThongDAO;
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    @ApiOperation("Lấy danh sách khoa nội trú")
    @RequestMapping(value="/getDanhSachKhoaNoiTru", method = RequestMethod.GET)
    public List getDanhSachKhoaNoiTru(@RequestParam("dvtt") String dvtt,
                                      HttpServletRequest request,
                                      HttpServletResponse response,
                                      HttpSession session) {
        try {
            return heThongDAO.getDanhSachKhoaNoiTru(dvtt);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách kho nội trú")
    @RequestMapping(value="/getDanhSachKhoNoiTru", method = RequestMethod.GET)
    public List getDanhSachKhoNoiTru(@RequestParam("dvtt") String dvtt,
                                     @RequestParam("maPhongBan") String maPhongBan,
                                     @RequestParam("nghiepVu") String nghiepVu,
                                     HttpServletRequest request,
                                     HttpServletResponse response,
                                     HttpSession session) {
        try {
            return heThongDAO.getDanhSachKhoNoiTru(dvtt, maPhongBan, nghiepVu);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách bác sĩ")
    @RequestMapping(value="/getDanhSachBacSi", method = RequestMethod.GET)
    public List getDanhSachBacSi(@RequestParam("dvtt") String dvtt,
                                 HttpServletRequest request,
                                 HttpServletResponse response,
                                 HttpSession session) {
        try {
            return heThongDAO.getDanhSachBacSi(dvtt);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
}
