package com.vnpt.duoc.duocModule.controller.kiemtraduyetnhan;

import com.vnpt.duoc.duocModule.DuocUtils;
import com.vnpt.duoc.duocModule.dao.kiemtraduyetnhan.KiemTraDuyetNhanDAO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("api/kiemtraduyetnhan")
@Api(value="KiemTraDuyetNhanController")
public class KiemTraDuyetNhanController {
    private final KiemTraDuyetNhanDAO kiemTraDuyetNhanDAO;

    public KiemTraDuyetNhanController(KiemTraDuyetNhanDAO kiemTraDuyetNhanDAO) {
        this.kiemTraDuyetNhanDAO = kiemTraDuyetNhanDAO;
    }

    @ApiOperation("Lấy danh sách phiếu theo trạng thái")
    @RequestMapping(value="/getDanhSachPhieuChuyenKhoTheoTrangThai", method = RequestMethod.GET)
    public List getDanhSachPhieuChuyenKhoTheoTrangThai(@RequestParam("dvtt") String dvtt,
                                                   @RequestParam("trangThai") int trangThai,
                                                   HttpServletResponse response,
                                                   HttpServletRequest request,
                                                   HttpSession session) {
        try {
            return kiemTraDuyetNhanDAO.getDanhSachPhieuChuyenKhoTheoTrangThai(dvtt, trangThai);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
}
