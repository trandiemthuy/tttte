create or replace FUNCTION "HIS_DUOCV2"."GET_ALL_TSDV"(i_dvtt varchar2)
RETURN SYS_REFCURSOR IS
    cur             SYS_REFCURSOR;
BEGIN
  OPEN CUR FOR  SELECT *
                FROM HIS_FW.DM_THAMSO_DONVI
                WHERE DVTT = i_dvtt
                ORDER BY MA_THAMSO ASC;
    RETURN CUR;
END GET_ALL_TSDV;
