package com.vnpt.duoc.duocModule.objects.khamchuabenh;

import java.util.Date;

public class ToaThuocNgoaiTruObj {
    private long maUser; // Mã người dùng
    private long idGiaoDich; // id giao dịch
    private String maPhongRaThuoc; // Mã phòng ra thuốc
    private int capCuu; // Cấp cứu: 0: Không phải      1: Cấp cứu
    private String dvtt; // Đơn vị trực thuộc
    private String maToaThuoc; // Mã toa thuốc
    private int maKhoVatTu; // Mã kho vật tư
    private int maVatTu; // Mã vật tư
    private String tenVatTu; // Tên vật tư
    private String tenGoc; // Tên gốc
    private String nghiepVu; // Nghiệp vụ
    private Double soLuong; // Số lượng
    private Double soLuongThucLinh; // Số lượng thực lĩnh
    private Double donGiaBV; // Đơn giá bệnh viện
    private Double donGiaBH; // Đơn giá bảo hiểm
    private Double thanhTien; // Thành tiền
    private long soNgay; // Số ngày sử dụng
    private Double sang; // Số lượng thuốc sử dụng buổi sáng
    private Double trua; // Số lượng thuốc sử dụng buổi trưa
    private Double chieu; // Số lượng thuốc sử dụng buổi chiều
    private Double toi; // Số lượng thuốc sử dụng buổi tối
    private Date ngayRaToa; // Ngày ra toa
    private String ghiChu; // Ghi chú toa thuốc
    private long maBacSi; // Mã bác sĩ ra thuốc
    private String cachSuDung; // Cách sử dụng
    private String dvt; // Đơn vị tính
    private int coBHYT; // Có BHYT (tỉ lễ miễn giảm)
    private int namHienTai; // Năm hiện tại
    private String maBenhNhan; // Mã bệnh nhân
    private String soPhieuThanhToan; // Số phiếu thanh toán
    private String idTiepNhan; // id tiếp nhận
    private String maKhamBenh; // Mã khám bệnh
    private Date ngayHienTai; // Ngày hiện tại
    private String daThanhToan; // Đã thanh toán
    private int soVaoVien; // số vào viện
    private String maGoiDichVu; // Mã gói dịch vụ
    private String soPhieu; // Số phiếu
    private String maCDHA; // Mã chuẩn đoán hình ảnh
    private String soPhieuTTPT; // Số phiếu tiểu thuật phẫu thuật
    private String maTTPT; // Mã tiểu thuật phẫu thuật
    private String soDangKyMN; // Số đăng ký MN
    private String soPhieuXN; // Số phiếu xét  nghiệm
    private String maKhoa; // Mã khoa ra thuốc
    private String maPhong; // Mã phòng ra thuốc
    private Double donGia82112; // Đơn giá theo tham số 82112
    private Double donGiaKhongLamTron; // Đơn giá không làm tròn
    private int tachToaDichVu; // Tách toa dịch vụ: 0: Không tách   1: Tách
    private String sttToaThuoc;

    public String getSttToaThuoc() {
        return sttToaThuoc;
    }

    public void setSttToaThuoc(String sttToaThuoc) {
        this.sttToaThuoc = sttToaThuoc;
    }

    public int getTachToaDichVu() {
        return tachToaDichVu;
    }

    public void setTachToaDichVu(int tachToaDichVu) {
        this.tachToaDichVu = tachToaDichVu;
    }

    public Double getDonGia82112() {
        return donGia82112;
    }

    public void setDonGia82112(Double donGia82112) {
        this.donGia82112 = donGia82112;
    }

    public Double getDonGiaKhongLamTron() {
        return donGiaKhongLamTron;
    }

    public void setDonGiaKhongLamTron(Double donGiaKhongLamTron) {
        this.donGiaKhongLamTron = donGiaKhongLamTron;
    }

    public long getMaUser() {
        return maUser;
    }

    public void setMaUser(long maUser) {
        this.maUser = maUser;
    }

    public long getIdGiaoDich() {
        return idGiaoDich;
    }

    public void setIdGiaoDich(long idGiaoDich) {
        this.idGiaoDich = idGiaoDich;
    }

    public String getMaPhongRaThuoc() {
        return maPhongRaThuoc;
    }

    public void setMaPhongRaThuoc(String maPhongRaThuoc) {
        this.maPhongRaThuoc = maPhongRaThuoc;
    }

    public int getCapCuu() {
        return capCuu;
    }

    public void setCapCuu(int capCuu) {
        this.capCuu = capCuu;
    }

    public String getDvtt() {
        return dvtt;
    }

    public void setDvtt(String dvtt) {
        this.dvtt = dvtt;
    }

    public String getMaToaThuoc() {
        return maToaThuoc;
    }

    public void setMaToaThuoc(String maToaThuoc) {
        this.maToaThuoc = maToaThuoc;
    }

    public int getMaKhoVatTu() {
        return maKhoVatTu;
    }

    public void setMaKhoVatTu(int maKhoVatTu) {
        this.maKhoVatTu = maKhoVatTu;
    }

    public int getMaVatTu() {
        return maVatTu;
    }

    public void setMaVatTu(int maVatTu) {
        this.maVatTu = maVatTu;
    }

    public String getTenVatTu() {
        return tenVatTu;
    }

    public void setTenVatTu(String tenVatTu) {
        this.tenVatTu = tenVatTu;
    }

    public String getTenGoc() {
        return tenGoc;
    }

    public void setTenGoc(String tenGoc) {
        this.tenGoc = tenGoc;
    }

    public String getNghiepVu() {
        return nghiepVu;
    }

    public void setNghiepVu(String nghiepVu) {
        this.nghiepVu = nghiepVu;
    }

    public Double getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Double soLuong) {
        this.soLuong = soLuong;
    }

    public Double getSoLuongThucLinh() {
        return soLuongThucLinh;
    }

    public void setSoLuongThucLinh(Double soLuongThucLinh) {
        this.soLuongThucLinh = soLuongThucLinh;
    }

    public Double getDonGiaBV() {
        return donGiaBV;
    }

    public void setDonGiaBV(Double donGiaBV) {
        this.donGiaBV = donGiaBV;
    }

    public Double getDonGiaBH() {
        return donGiaBH;
    }

    public void setDonGiaBH(Double donGiaBH) {
        this.donGiaBH = donGiaBH;
    }

    public Double getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(Double thanhTien) {
        this.thanhTien = thanhTien;
    }

    public long getSoNgay() {
        return soNgay;
    }

    public void setSoNgay(long soNgay) {
        this.soNgay = soNgay;
    }

    public Double getSang() {
        return sang;
    }

    public void setSang(Double sang) {
        this.sang = sang;
    }

    public Double getTrua() {
        return trua;
    }

    public void setTrua(Double trua) {
        this.trua = trua;
    }

    public Double getChieu() {
        return chieu;
    }

    public void setChieu(Double chieu) {
        this.chieu = chieu;
    }

    public Double getToi() {
        return toi;
    }

    public void setToi(Double toi) {
        this.toi = toi;
    }

    public Date getNgayRaToa() {
        return ngayRaToa;
    }

    public void setNgayRaToa(Date ngayRaToa) {
        this.ngayRaToa = ngayRaToa;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public long getMaBacSi() {
        return maBacSi;
    }

    public void setMaBacSi(long maBacSi) {
        this.maBacSi = maBacSi;
    }

    public String getCachSuDung() {
        return cachSuDung;
    }

    public void setCachSuDung(String cachSuDung) {
        this.cachSuDung = cachSuDung;
    }

    public String getDvt() {
        return dvt;
    }

    public void setDvt(String dvt) {
        this.dvt = dvt;
    }

    public int getCoBHYT() {
        return coBHYT;
    }

    public void setCoBHYT(int coBHYT) {
        this.coBHYT = coBHYT;
    }

    public int getNamHienTai() {
        return namHienTai;
    }

    public void setNamHienTai(int namHienTai) {
        this.namHienTai = namHienTai;
    }

    public String getMaBenhNhan() {
        return maBenhNhan;
    }

    public void setMaBenhNhan(String maBenhNhan) {
        this.maBenhNhan = maBenhNhan;
    }

    public String getSoPhieuThanhToan() {
        return soPhieuThanhToan;
    }

    public void setSoPhieuThanhToan(String soPhieuThanhToan) {
        this.soPhieuThanhToan = soPhieuThanhToan;
    }

    public String getIdTiepNhan() {
        return idTiepNhan;
    }

    public void setIdTiepNhan(String idTiepNhan) {
        this.idTiepNhan = idTiepNhan;
    }

    public String getMaKhamBenh() {
        return maKhamBenh;
    }

    public void setMaKhamBenh(String maKhamBenh) {
        this.maKhamBenh = maKhamBenh;
    }

    public Date getNgayHienTai() {
        return ngayHienTai;
    }

    public void setNgayHienTai(Date ngayHienTai) {
        this.ngayHienTai = ngayHienTai;
    }

    public String getDaThanhToan() {
        return daThanhToan;
    }

    public void setDaThanhToan(String daThanhToan) {
        this.daThanhToan = daThanhToan;
    }

    public int getSoVaoVien() {
        return soVaoVien;
    }

    public void setSoVaoVien(int soVaoVien) {
        this.soVaoVien = soVaoVien;
    }

    public String getMaGoiDichVu() {
        return maGoiDichVu;
    }

    public void setMaGoiDichVu(String maGoiDichVu) {
        this.maGoiDichVu = maGoiDichVu;
    }

    public String getSoPhieu() {
        return soPhieu;
    }

    public void setSoPhieu(String soPhieu) {
        this.soPhieu = soPhieu;
    }

    public String getMaCDHA() {
        return maCDHA;
    }

    public void setMaCDHA(String maCDHA) {
        this.maCDHA = maCDHA;
    }

    public String getSoPhieuTTPT() {
        return soPhieuTTPT;
    }

    public void setSoPhieuTTPT(String soPhieuTTPT) {
        this.soPhieuTTPT = soPhieuTTPT;
    }

    public String getMaTTPT() {
        return maTTPT;
    }

    public void setMaTTPT(String maTTPT) {
        this.maTTPT = maTTPT;
    }

    public String getSoDangKyMN() {
        return soDangKyMN;
    }

    public void setSoDangKyMN(String soDangKyMN) {
        this.soDangKyMN = soDangKyMN;
    }

    public String getSoPhieuXN() {
        return soPhieuXN;
    }

    public void setSoPhieuXN(String soPhieuXN) {
        this.soPhieuXN = soPhieuXN;
    }

    public String getMaKhoa() {
        return maKhoa;
    }

    public void setMaKhoa(String maKhoa) {
        this.maKhoa = maKhoa;
    }

    public String getMaPhong() {
        return maPhong;
    }

    public void setMaPhong(String maPhong) {
        this.maPhong = maPhong;
    }
}
