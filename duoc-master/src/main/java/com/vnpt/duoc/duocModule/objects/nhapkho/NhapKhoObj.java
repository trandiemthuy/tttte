package com.vnpt.duoc.duocModule.objects.nhapkho;

import com.vnpt.duoc.duocModule.DuocUtils;
import net.minidev.json.JSONObject;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.Date;

public class NhapKhoObj {
    private long dongY; // 1|0
    private String dvtt;
    private String ghiChu;
    private String gioNhap;
    private String idGiaoDich;
    private long idNhapKhoTuNhaCungCap;
    private String kyHieuHD;
    private String kyHieuHoaDon;
    private long maKhoHoanTra;
    private long maNhaCungCap;
    private String maDonViTao;
    private long maKhoNhan;
    private long maNghiepVuChuyen;
    private long maNguoiNhan;
    private String maPhongBanNhap;
    private Date ngayHoaDon; // dd/mm/yyyy
    private Date ngayNhap; // dd/mm/yyyy
    private String nguoiGiaoHang;
    private long nguoiTao;
    private String nguonDuoc;
    private Double phanTramGiaBanLe;
    private long phieuNhapTonBanDau; // 1 | 0
    private String soHoaDon;
    private String soHopDong;
    private String soLuuTru;
    private String soPhieuNhap;
    private long ttHoaDon;
    private long vat; // 0 -> 99

    public NhapKhoObj() {
    }

    public NhapKhoObj(long dongY, String dvtt, String ghiChu, String gioNhap, String idGiaoDich, long idNhapKhoTuNhaCungCap, String kyHieuHD, String kyHieuHoaDon, long maKhoHoanTra, long maNhaCungCap, String maDonViTao, long maKhoNhan, long maNghiepVuChuyen, long maNguoiNhan, String maPhongBanNhap, Date ngayHoaDon, Date ngayNhap, String nguoiGiaoHang, long nguoiTao, String nguonDuoc, Double phanTramGiaBanLe, long phieuNhapTonBanDau, String soHoaDon, String soHopDong, String soLuuTru, String soPhieuNhap, long ttHoaDon, long vat) {
        this.dongY = dongY;
        this.dvtt = dvtt;
        this.ghiChu = ghiChu;
        this.gioNhap = gioNhap;
        this.idGiaoDich = idGiaoDich;
        this.idNhapKhoTuNhaCungCap = idNhapKhoTuNhaCungCap;
        this.kyHieuHD = kyHieuHD;
        this.kyHieuHoaDon = kyHieuHoaDon;
        this.maKhoHoanTra = maKhoHoanTra;
        this.maNhaCungCap = maNhaCungCap;
        this.maDonViTao = maDonViTao;
        this.maKhoNhan = maKhoNhan;
        this.maNghiepVuChuyen = maNghiepVuChuyen;
        this.maNguoiNhan = maNguoiNhan;
        this.maPhongBanNhap = maPhongBanNhap;
        this.ngayHoaDon = ngayHoaDon;
        this.ngayNhap = ngayNhap;
        this.nguoiGiaoHang = nguoiGiaoHang;
        this.nguoiTao = nguoiTao;
        this.nguonDuoc = nguonDuoc;
        this.phanTramGiaBanLe = phanTramGiaBanLe;
        this.phieuNhapTonBanDau = phieuNhapTonBanDau;
        this.soHoaDon = soHoaDon;
        this.soHopDong = soHopDong;
        this.soLuuTru = soLuuTru;
        this.soPhieuNhap = soPhieuNhap;
        this.ttHoaDon = ttHoaDon;
        this.vat = vat;
    }

    public NhapKhoObj (JSONObject object, HttpSession session, String idGiaoDich) throws ParseException {
        this.idNhapKhoTuNhaCungCap = object.get("idNhapKhoTuNhaCungCap") == null ? 0 : Long.parseLong(object.get("idNhapKhoTuNhaCungCap").toString());
        this.soHoaDon = object.get("soHoaDon").toString();
        this.ngayHoaDon = DuocUtils.convertStringToDate(object.get("ngayHoaDon").toString(), "dd/MM/yyyy");
        this.vat = Long.parseLong(object.get("vat").toString());
        this.ghiChu = object.get("ghiChu").toString();
        this.ngayNhap = DuocUtils.convertStringToDate(object.get("ngayNhap").toString(), "dd/MM/yyyy");
        this.maNhaCungCap = Long.parseLong(object.get("maNhaCungCap").toString());
        this.maKhoNhan = Long.parseLong(object.get("maKhoNhan").toString());
        this.maDonViTao = object.get("maDonViTao").toString();
        this.maNguoiNhan = Long.parseLong(object.get("maNguoiNhan").toString());
        this.nguoiTao = Long.parseLong(object.get("nguoiTao").toString());
        this.nguonDuoc = object.get("nguonDuoc").toString();
        this.soHopDong = object.get("soHopDong") == null ? "" : object.get("soHopDong").toString();
        this.nguoiGiaoHang = object.get("soHopDong") == null ? "" : object.get("nguoiGiaoHang").toString();
        this.phanTramGiaBanLe = Double.parseDouble(object.get("phanTramGiaBanLe").toString());
        this.kyHieuHD = object.get("kyHieuHD").toString();
        this.maNghiepVuChuyen = Integer.parseInt(object.get("maNghiepVuChuyen").toString());
        this.maPhongBanNhap = object.get("maPhongBanNhap").toString();
//        this.userName = session.getAttribute("Sess_Var").toString();
        this.idGiaoDich = idGiaoDich;
        this.dvtt = object.get("dvtt").toString();
    }

    public long getDongY() {
        return dongY;
    }

    public void setDongY(long dongY) {
        this.dongY = dongY;
    }

    public String getDvtt() {
        return dvtt;
    }

    public void setDvtt(String dvtt) {
        this.dvtt = dvtt;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getGioNhap() {
        return gioNhap;
    }

    public void setGioNhap(String gioNhap) {
        this.gioNhap = gioNhap;
    }

    public String getIdGiaoDich() {
        return idGiaoDich;
    }

    public void setIdGiaoDich(String idGiaoDich) {
        this.idGiaoDich = idGiaoDich;
    }

    public long getIdNhapKhoTuNhaCungCap() {
        return idNhapKhoTuNhaCungCap;
    }

    public void setIdNhapKhoTuNhaCungCap(long idNhapKhoTuNhaCungCap) {
        this.idNhapKhoTuNhaCungCap = idNhapKhoTuNhaCungCap;
    }

    public String getKyHieuHD() {
        return kyHieuHD;
    }

    public void setKyHieuHD(String kyHieuHD) {
        this.kyHieuHD = kyHieuHD;
    }

    public String getKyHieuHoaDon() {
        return kyHieuHoaDon;
    }

    public void setKyHieuHoaDon(String kyHieuHoaDon) {
        this.kyHieuHoaDon = kyHieuHoaDon;
    }

    public long getMaKhoHoanTra() {
        return maKhoHoanTra;
    }

    public void setMaKhoHoanTra(long maKhoHoanTra) {
        this.maKhoHoanTra = maKhoHoanTra;
    }

    public long getMaNhaCungCap() {
        return maNhaCungCap;
    }

    public void setMaNhaCungCap(long maNhaCungCap) {
        this.maNhaCungCap = maNhaCungCap;
    }

    public String getMaDonViTao() {
        return maDonViTao;
    }

    public void setMaDonViTao(String maDonViTao) {
        this.maDonViTao = maDonViTao;
    }

    public long getMaKhoNhan() {
        return maKhoNhan;
    }

    public void setMaKhoNhan(long maKhoNhan) {
        this.maKhoNhan = maKhoNhan;
    }

    public long getMaNghiepVuChuyen() {
        return maNghiepVuChuyen;
    }

    public void setMaNghiepVuChuyen(long maNghiepVuChuyen) {
        this.maNghiepVuChuyen = maNghiepVuChuyen;
    }

    public long getMaNguoiNhan() {
        return maNguoiNhan;
    }

    public void setMaNguoiNhan(long maNguoiNhan) {
        this.maNguoiNhan = maNguoiNhan;
    }

    public String getMaPhongBanNhap() {
        return maPhongBanNhap;
    }

    public void setMaPhongBanNhap(String maPhongBanNhap) {
        this.maPhongBanNhap = maPhongBanNhap;
    }

    public Date getNgayHoaDon() {
        return ngayHoaDon;
    }

    public void setNgayHoaDon(Date ngayHoaDon) {
        this.ngayHoaDon = ngayHoaDon;
    }

    public Date getNgayNhap() {
        return ngayNhap;
    }

    public void setNgayNhap(Date ngayNhap) {
        this.ngayNhap = ngayNhap;
    }

    public String getNguoiGiaoHang() {
        return nguoiGiaoHang;
    }

    public void setNguoiGiaoHang(String nguoiGiaoHang) {
        this.nguoiGiaoHang = nguoiGiaoHang;
    }

    public long getNguoiTao() {
        return nguoiTao;
    }

    public void setNguoiTao(long nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public String getNguonDuoc() {
        return nguonDuoc;
    }

    public void setNguonDuoc(String nguonDuoc) {
        this.nguonDuoc = nguonDuoc;
    }

    public Double getPhanTramGiaBanLe() {
        return phanTramGiaBanLe;
    }

    public void setPhanTramGiaBanLe(Double phanTramGiaBanLe) {
        this.phanTramGiaBanLe = phanTramGiaBanLe;
    }

    public long getPhieuNhapTonBanDau() {
        return phieuNhapTonBanDau;
    }

    public void setPhieuNhapTonBanDau(long phieuNhapTonBanDau) {
        this.phieuNhapTonBanDau = phieuNhapTonBanDau;
    }

    public String getSoHoaDon() {
        return soHoaDon;
    }

    public void setSoHoaDon(String soHoaDon) {
        this.soHoaDon = soHoaDon;
    }

    public String getSoHopDong() {
        return soHopDong;
    }

    public void setSoHopDong(String soHopDong) {
        this.soHopDong = soHopDong;
    }

    public String getSoLuuTru() {
        return soLuuTru;
    }

    public void setSoLuuTru(String soLuuTru) {
        this.soLuuTru = soLuuTru;
    }

    public String getSoPhieuNhap() {
        return soPhieuNhap;
    }

    public void setSoPhieuNhap(String soPhieuNhap) {
        this.soPhieuNhap = soPhieuNhap;
    }

    public long getTtHoaDon() {
        return ttHoaDon;
    }

    public void setTtHoaDon(long ttHoaDon) {
        this.ttHoaDon = ttHoaDon;
    }

    public long getVat() {
        return vat;
    }

    public void setVat(long vat) {
        this.vat = vat;
    }
}


