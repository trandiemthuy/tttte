package com.vnpt.duoc.duocModule.objects.tonghopdutru;

public class ThongTinPhieuDuTruObj {
    private String dvtt;
    private String idPhieu; // Danh sách id phiếu điều trị
    private String tuNgay;
    private String denNgay;
    private int isBANT;
    private String maPhongBan;
    private String maPhongBenh;
    private long maNguoiLap;
    private String ghiChu;
    private long idGiaoDich;

    public String getMaPhongBenh() {
        return maPhongBenh;
    }

    public void setMaPhongBenh(String maPhongBenh) {
        this.maPhongBenh = maPhongBenh;
    }

    public String getTuNgay() {
        return tuNgay;
    }

    public void setTuNgay(String tuNgay) {
        this.tuNgay = tuNgay;
    }

    public String getDenNgay() {
        return denNgay;
    }

    public void setDenNgay(String denNgay) {
        this.denNgay = denNgay;
    }

    public long getIdGiaoDich() {
        return idGiaoDich;
    }

    public void setIdGiaoDich(long idGiaoDich) {
        this.idGiaoDich = idGiaoDich;
    }

    public String getDvtt() {
        return dvtt;
    }

    public void setDvtt(String dvtt) {
        this.dvtt = dvtt;
    }

    public String getIdPhieu() {
        return idPhieu;
    }

    public void setIdPhieu(String idPhieu) {
        this.idPhieu = idPhieu;
    }

    public int getIsBANT() {
        return isBANT;
    }

    public void setIsBANT(int isBANT) {
        this.isBANT = isBANT;
    }

    public String getMaPhongBan() {
        return maPhongBan;
    }

    public void setMaPhongBan(String maPhongBan) {
        this.maPhongBan = maPhongBan;
    }

    public long getMaNguoiLap() {
        return maNguoiLap;
    }

    public void setMaNguoiLap(long maNguoiLap) {
        this.maNguoiLap = maNguoiLap;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
}
