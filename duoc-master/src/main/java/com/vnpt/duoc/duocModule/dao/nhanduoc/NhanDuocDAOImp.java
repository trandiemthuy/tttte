package com.vnpt.duoc.duocModule.dao.nhanduoc;

import com.vnpt.duoc.jdbc.JdbcTemplate;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class NhanDuocDAOImp implements NhanDuocDAO {
    private final HikariDataSource dataSourceDuocV2;

    public NhanDuocDAOImp(HikariDataSource dataSourceDuocV2) {
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    @Override
    public List getDanhSachPhieuNhanDuocVeKho(Date tuNgay, Date denNgay, String dvtt, int duyet, int maKho, String maPhongBan, int maNghiepVu) {
        String sql = "call HIS_DUOCV2.DC_SP_NHANVEKHO_DSPHIEU_V2(?,?,?,?,?,?,?)#c,t,t,s,l,l,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{
            tuNgay,
            denNgay,
            dvtt,
            duyet,
            maKho,
            maPhongBan,
            maNghiepVu
        });
    }

    @Override
    public int nhanDuocVeKho(int maNghiepVu, String dvtt, long idChuyenKhoVatTu, int maKhoGiao, int maKhoNhan, Date ngayNhan, long nguoiNhan, String idGiaoDich, String maKhoa, String maPhong) {
        String sql = "call HIS_DUOCV2.DC_SP_NHANKHO_CHITIET_INS_V2(?,?,?,?,?,?,?,?,?,?)#l,l,s,l,l,l,t,l,l,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{
            maNghiepVu,
            dvtt,
            idChuyenKhoVatTu,
            maKhoGiao,
            maKhoNhan,
            ngayNhan,
            nguoiNhan,
            idGiaoDich,
            maKhoa,
            maPhong
        }, Integer.class);
    }

    @Override
    public int huyNhanDuocVeKho(long idChuyenKho, String dvtt, long nguoiHuy, String idGiaoDich, String maKhoa, String maPhong) {
        String sql = "call HIS_DUOCV2.DC_SP_NHANKHO_CT_DELETE_V2(?,?,?,?,?,?)#l,l,s,l,l,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{
            idChuyenKho, dvtt, nguoiHuy, idGiaoDich, maKhoa, maPhong
        }, Integer.class);
    }
}
