package com.vnpt.duoc.duocModule.controller.xuatduoc;

import com.vnpt.duoc.duocModule.DuocConfigs;
import com.vnpt.duoc.duocModule.DuocTransaction;
import com.vnpt.duoc.duocModule.DuocUtils;
import com.vnpt.duoc.duocModule.dao.hethong.HeThongDAO;
import com.vnpt.duoc.duocModule.dao.xuatduoc.XuatDuocDAO;
import com.vnpt.duoc.duocModule.objects.xuatduoc.ThongTinPhieuXuatDuocObj;
import com.vnpt.duoc.jasper.ExportXLSView;
import com.vnpt.duoc.jasper.JasperHelper;
import com.zaxxer.hikari.HikariDataSource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/xuatduoc")
@Api(value="Xuất dược Controller")
public class XuatDuocController {

    private XuatDuocDAO xuatDuocDAO;
    private HeThongDAO heThongDAO;
    private HikariDataSource dataSourceDuocV2;

    public XuatDuocController(XuatDuocDAO xuatDuocDAO, HeThongDAO heThongDAO, HikariDataSource dataSourceDuocV2) {
        this.xuatDuocDAO = xuatDuocDAO;
        this.heThongDAO = heThongDAO;
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    @ApiOperation("Lấy danh sách bệnh nhân xuất dược")
    @RequestMapping(value="/getDanhSachBenhNhanXuatDuoc", method = RequestMethod.GET)
    public List getDanhSachBenhNhanXuatDuoc(@RequestParam("dvtt") String dvtt,
                                            @RequestParam("maNghiepVu") int maNghiepVu,
                                            @RequestParam("ngay") String ngay,
                                            @RequestParam("trangThai") int trangThai,
                                            @RequestParam("maKhoa") String maKhoa,
                                            HttpServletRequest request,
                                            HttpServletResponse response,
                                            HttpSession session) {
        try {
            return xuatDuocDAO.getDanhSachBenhNhanXuatDuoc(dvtt, maNghiepVu, ngay, trangThai, maKhoa);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách vật tư xuất dược")
    @RequestMapping(value="/getDanhSachVatTuXuatDuoc", method = RequestMethod.GET)
    public List getDanhSachVatTuXuatDuoc(@RequestParam("dvtt") String dvtt,
                                         @RequestParam("maNghiepVu") int maNghiepVu,
                                         @RequestParam("maToaThuoc") String maToaThuoc,
                                         @RequestParam("trangThai") int trangThai,
                                         @RequestParam("soVaoVien") int soVaoVien,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         HttpSession session) {
        try {
            return xuatDuocDAO.getDanhSachVatTuXuatDuoc(dvtt, maNghiepVu, maToaThuoc, trangThai, soVaoVien);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Xuất dược")
    @ResponseBody
    @RequestMapping(value = "/xuatDuocVatTu", method = RequestMethod.POST)
    public int xuatDuocVatTu(@RequestBody ThongTinPhieuXuatDuocObj thongTinPhieuXuatDuocObj,
                             @RequestParam("trangThai") int trangThai,
                             HttpSession session,
                             HttpServletRequest request,
                             HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            int idLoaiGiaoDich;
            String noiDung;
            if (trangThai == 1) {
                idLoaiGiaoDich = 21;
                noiDung = "Xuất dược vật tư";
            } else {
                idLoaiGiaoDich = 22;
                noiDung = "Hủy xuất dược vật tư";
            }
            String idGiaoDich = DuocTransaction.insert(0, idLoaiGiaoDich, noiDung,
                thongTinPhieuXuatDuocObj.getMaNguoiXuat(), thongTinPhieuXuatDuocObj.getDvtt(), 0,
                clientIP, hostIP, "", "xuatDuocVatTuV2", dataSourceDuocV2);
            ThongTinPhieuXuatDuocObj temp = thongTinPhieuXuatDuocObj;
            temp.setIdGiaoDich(Long.parseLong(idGiaoDich));
            return xuatDuocDAO.xuatDuocVatTu(temp, trangThai);
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    @RequestMapping(value = "/printDanhSachBenhNhanXuatDuoc", produces = "application/json; charset=utf-8")
    public @ResponseBody
    void printDanhSachBenhNhanXuatDuoc(HttpServletResponse response,
                                       HttpServletRequest request,
                                       @RequestParam(value = "dvtt") String dvtt,
                                       @RequestParam(value = "ngay") String ngay,
                                       @RequestParam(value = "nghiepVu") String nghiepVu,
                                       @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                       @RequestParam(value = "tenTinh") String tenTinh,
                                       @RequestParam(value = "loaiFile") String loaiFile
    ) {
        try {
            String tenbenhvien = tenBenhVien.toUpperCase();
            String tieude = "Ngày " + DuocUtils.convertFromFormatToDate(ngay, "-", "/");
            Map parameters = new HashMap();
            parameters.put("dvtt", dvtt);
            parameters.put("ngay", DuocUtils.convertFromFormatToDate(ngay, "-", ""));
            parameters.put("nghiepvu", nghiepVu);
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            parameters.put("tentinh", donviquanlytructiep);
            parameters.put("ngaybaocao", tieude);
            parameters.put("tenbenhvien", tenbenhvien);
            File reportFile;
            String reportName;
            if (nghiepVu.equals("ngoaitru_toabanle")) {
                reportName = "rp_danhsachbnlanhduoctheongay_banle.jasper";
            } else if (nghiepVu.equals("ba_ngoaitru_toathuoc")) {
                reportName = "rp_danhsachbnlanhduoctheongay_bant.jasper";
            } else {
                reportName = "rp_danhsachbnlanhduoctheongay.jasper";
            }
            reportFile = new ClassPathResource(DuocConfigs.reportURL + "xuatduoc/" + reportName).getFile();
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (FileNotFoundException ex) {
            DuocUtils.responseFileNotFoundToClient(ex, response);
        }
        catch (Exception ex) {
            String errTitle = DuocConfigs.ERROR_TITLE_EXCEPTIONS;
            String errContent = "Thông tin lỗi: " + ex;
            DuocUtils.responseErrorToClient(errTitle, errContent, response);
            ex.printStackTrace();
        }
    }

    @ApiOperation("Danh sách phiếu xuất khoa/phòng tổng hợp / chi tiết")
    @ResponseBody
    @RequestMapping(value = "/exportDanhSachPhieuXuat", method = RequestMethod.GET)
    public ModelAndView exportDanhSachPhieuXuat(@RequestParam(value = "dvtt") String dvtt,
                                              @RequestParam(value = "ngayLap") String ngayLap,
                                              @RequestParam(value = "nghiepVu") String nghiepVu,
                                              @RequestParam(value = "hinhThuc") String hinhThuc // tonghop  |  chitiet
    ) {
        List<Map<String, Object>> list = xuatDuocDAO.getDanhSachXuatDuocTongHop(dvtt, ngayLap, nghiepVu, hinhThuc);
        ExportXLSView.sheetName = "Danh sách phiếu xuất";
        ExportXLSView.fileName = "danh_sach_phieu_xuat.xls";
        return new ModelAndView(new ExportXLSView(), "report_view", list);
    }

    @ApiOperation("Trả thuốc về kho")
    @ResponseBody
    @RequestMapping(value = "/traThuocVeKho", method = RequestMethod.POST)
    public int traThuocVeKho(@RequestBody ThongTinPhieuXuatDuocObj thongTinPhieuXuatDuocObj,
                             @RequestParam("nghiepVu") String nghiepVu,
                             HttpSession session,
                             HttpServletRequest request,
                             HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 30, "Trả dược về kho",
                thongTinPhieuXuatDuocObj.getMaNguoiXuat(), thongTinPhieuXuatDuocObj.getDvtt(), 0,
                clientIP, hostIP, "", "traThuocVeKho", dataSourceDuocV2);
            ThongTinPhieuXuatDuocObj temp = thongTinPhieuXuatDuocObj;
            temp.setIdGiaoDich(Long.parseLong(idGiaoDich));
            return xuatDuocDAO.traThuocVeKho(temp, nghiepVu);
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }
}
