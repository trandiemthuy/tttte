package com.vnpt.duoc.duocModule.objects.nhapkho;

public class ChiTietNhapKhoObj {
    private long idPhieuNhap;
    private long idNhapKhoCT;
    private String dvtt;
    private long nghiepVu;
    private String soPhieuNhap;
    private String soHoaDon;
    private String quyCach;
    private Double quyCachQuyDoi;
    private String soLoSanXuat;
    private String ngaySanXuat; // dd/mm/yyyy
    private String ngayHetHan; // dd/mm/yyyy
    private long maVatTu;
    private long vat;
    private Double soLuong;
    private Double soLuongQuyDoi;
    private Double donGiaHoaDon;
    private Double donGiaQuyDoi;
    private Double donGiaQuyDoiVAT;
    private Double donGia;
    private Double thanhTien;
    private Double tienVAT;
    private Double thanhTienVAT;
    private String ghiChu;
    private String maDonViNhan;
    private Double duocConLai;
    private String hoatChat;
    private String dvt;
    private String soThau;
    private Double donGiaTruocVAT;
    private long maNguoiNhap;
    private long idGiaoDich;
    private long maNhapKhoVatTu;
    private Double thanhTienHoaDon;
    private String ngayNhap;
    private Double phanTramHaoHut;
    private Double donGiaTruocHaoHut;
    private Double donGiaSauHaoHut;
    private Double soLuongTruocHaoHut;
    private Double soLuongSauHaoHut;
    private Double phanTramDonGiaDV;
    private Double donGiaDV;
    private Double donGiaVP;
    private long soLuongQuyCach;
    private Double donGiaChinhSua;
    private Double soLuongTruocQuyDoi;

    public ChiTietNhapKhoObj() {
    }

    public Double getDonGiaVP() {
        return donGiaVP;
    }

    public void setDonGiaVP(Double donGiaVP) {
        this.donGiaVP = donGiaVP;
    }

    public Double getSoLuongTruocQuyDoi() {
        return soLuongTruocQuyDoi;
    }

    public void setSoLuongTruocQuyDoi(Double soLuongTruocQuyDoi) {
        this.soLuongTruocQuyDoi = soLuongTruocQuyDoi;
    }

    public long getSoLuongQuyCach() {
        return soLuongQuyCach;
    }

    public void setSoLuongQuyCach(long soLuongQuyCach) {
        this.soLuongQuyCach = soLuongQuyCach;
    }

    public Double getDonGiaChinhSua() {
        return donGiaChinhSua;
    }

    public void setDonGiaChinhSua(Double donGiaChinhSua) {
        this.donGiaChinhSua = donGiaChinhSua;
    }

    public Double getDonGiaDV() {
        return donGiaDV;
    }

    public void setDonGiaDV(Double donGiaDV) {
        this.donGiaDV = donGiaDV;
    }

    public Double getPhanTramDonGiaDV() {
        return phanTramDonGiaDV;
    }

    public void setPhanTramDonGiaDV(Double phanTramDonGiaDV) {
        this.phanTramDonGiaDV = phanTramDonGiaDV;
    }

    public Double getSoLuongTruocHaoHut() {
        return soLuongTruocHaoHut;
    }

    public void setSoLuongTruocHaoHut(Double soLuongTruocHaoHut) {
        this.soLuongTruocHaoHut = soLuongTruocHaoHut;
    }

    public Double getSoLuongSauHaoHut() {
        return soLuongSauHaoHut;
    }

    public void setSoLuongSauHaoHut(Double soLuongSauHaoHut) {
        this.soLuongSauHaoHut = soLuongSauHaoHut;
    }

    public void setPhanTramHaoHut(Double phanTramHaoHut) {
        this.phanTramHaoHut = phanTramHaoHut;
    }

    public Double getDonGiaTruocHaoHut() {
        return donGiaTruocHaoHut;
    }

    public void setDonGiaTruocHaoHut(Double donGiaTruocHaoHut) {
        this.donGiaTruocHaoHut = donGiaTruocHaoHut;
    }

    public Double getDonGiaSauHaoHut() {
        return donGiaSauHaoHut;
    }

    public void setDonGiaSauHaoHut(Double donGiaSauHaoHut) {
        this.donGiaSauHaoHut = donGiaSauHaoHut;
    }

    public double getPhanTramHaoHut() {
        return phanTramHaoHut;
    }

    public void setPhanTramHaoHut(double phanTramHaoHut) {
        this.phanTramHaoHut = phanTramHaoHut;
    }

    public String getNgayNhap() {
        return ngayNhap;
    }

    public void setNgayNhap(String ngayNhap) {
        this.ngayNhap = ngayNhap;
    }

    public long getMaNhapKhoVatTu() {
        return maNhapKhoVatTu;
    }

    public void setMaNhapKhoVatTu(long maNhapKhoVatTu) {
        this.maNhapKhoVatTu = maNhapKhoVatTu;
    }

    public Double getThanhTienHoaDon() {
        return thanhTienHoaDon;
    }

    public void setThanhTienHoaDon(Double thanhTienHoaDon) {
        this.thanhTienHoaDon = thanhTienHoaDon;
    }

    public long getMaNguoiNhap() {
        return maNguoiNhap;
    }

    public void setMaNguoiNhap(long maNguoiNhap) {
        this.maNguoiNhap = maNguoiNhap;
    }

    public long getIdGiaoDich() {
        return idGiaoDich;
    }

    public void setIdGiaoDich(long idGiaoDich) {
        this.idGiaoDich = idGiaoDich;
    }

    public String getDvtt() {
        return dvtt;
    }

    public void setDvtt(String dvtt) {
        this.dvtt = dvtt;
    }

    public long getIdPhieuNhap() {
        return idPhieuNhap;
    }

    public void setIdPhieuNhap(long idPhieuNhap) {
        this.idPhieuNhap = idPhieuNhap;
    }

    public long getIdNhapKhoCT() {
        return idNhapKhoCT;
    }

    public void setIdNhapKhoCT(long idNhapKhoCT) {
        this.idNhapKhoCT = idNhapKhoCT;
    }

    public long getNghiepVu() {
        return nghiepVu;
    }

    public void setNghiepVu(long nghiepVu) {
        this.nghiepVu = nghiepVu;
    }

    public String getSoPhieuNhap() {
        return soPhieuNhap;
    }

    public void setSoPhieuNhap(String soPhieuNhap) {
        this.soPhieuNhap = soPhieuNhap;
    }

    public String getSoHoaDon() {
        return soHoaDon;
    }

    public void setSoHoaDon(String soHoaDon) {
        this.soHoaDon = soHoaDon;
    }

    public String getQuyCach() {
        return quyCach;
    }

    public void setQuyCach(String quyCach) {
        this.quyCach = quyCach;
    }

    public Double getQuyCachQuyDoi() {
        return quyCachQuyDoi;
    }

    public void setQuyCachQuyDoi(Double quyCachQuyDoi) {
        this.quyCachQuyDoi = quyCachQuyDoi;
    }

    public String getSoLoSanXuat() {
        return soLoSanXuat;
    }

    public void setSoLoSanXuat(String soLoSanXuat) {
        this.soLoSanXuat = soLoSanXuat;
    }

    public String getNgaySanXuat() {
        return ngaySanXuat;
    }

    public void setNgaySanXuat(String ngaySanXuat) {
        this.ngaySanXuat = ngaySanXuat;
    }

    public String getNgayHetHan() {
        return ngayHetHan;
    }

    public void setNgayHetHan(String ngayHetHan) {
        this.ngayHetHan = ngayHetHan;
    }

    public long getMaVatTu() {
        return maVatTu;
    }

    public void setMaVatTu(long maVatTu) {
        this.maVatTu = maVatTu;
    }

    public long getVat() {
        return vat;
    }

    public void setVat(long vat) {
        this.vat = vat;
    }

    public Double getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Double soLuong) {
        this.soLuong = soLuong;
    }

    public Double getSoLuongQuyDoi() {
        return soLuongQuyDoi;
    }

    public void setSoLuongQuyDoi(Double soLuongQuyDoi) {
        this.soLuongQuyDoi = soLuongQuyDoi;
    }

    public Double getDonGiaHoaDon() {
        return donGiaHoaDon;
    }

    public void setDonGiaHoaDon(Double donGiaHoaDon) {
        this.donGiaHoaDon = donGiaHoaDon;
    }

    public Double getDonGiaQuyDoi() {
        return donGiaQuyDoi;
    }

    public void setDonGiaQuyDoi(Double donGiaQuyDoi) {
        this.donGiaQuyDoi = donGiaQuyDoi;
    }

    public Double getDonGiaQuyDoiVAT() {
        return donGiaQuyDoiVAT;
    }

    public void setDonGiaQuyDoiVAT(Double donGiaQuyDoiVAT) {
        this.donGiaQuyDoiVAT = donGiaQuyDoiVAT;
    }

    public Double getDonGia() {
        return donGia;
    }

    public void setDonGia(Double donGia) {
        this.donGia = donGia;
    }

    public Double getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(Double thanhTien) {
        this.thanhTien = thanhTien;
    }

    public Double getTienVAT() {
        return tienVAT;
    }

    public void setTienVAT(Double tienVAT) {
        this.tienVAT = tienVAT;
    }

    public Double getThanhTienVAT() {
        return thanhTienVAT;
    }

    public void setThanhTienVAT(Double thanhTienVAT) {
        this.thanhTienVAT = thanhTienVAT;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getMaDonViNhan() {
        return maDonViNhan;
    }

    public void setMaDonViNhan(String maDonViNhan) {
        this.maDonViNhan = maDonViNhan;
    }

    public Double getDuocConLai() {
        return duocConLai;
    }

    public void setDuocConLai(Double duocConLai) {
        this.duocConLai = duocConLai;
    }

    public String getHoatChat() {
        return hoatChat;
    }

    public void setHoatChat(String hoatChat) {
        this.hoatChat = hoatChat;
    }

    public String getDvt() {
        return dvt;
    }

    public void setDvt(String dvt) {
        this.dvt = dvt;
    }

    public String getSoThau() {
        return soThau;
    }

    public void setSoThau(String soThau) {
        this.soThau = soThau;
    }

    public Double getDonGiaTruocVAT() {
        return donGiaTruocVAT;
    }

    public void setDonGiaTruocVAT(Double donGiaTruocVAT) {
        this.donGiaTruocVAT = donGiaTruocVAT;
    }
}
