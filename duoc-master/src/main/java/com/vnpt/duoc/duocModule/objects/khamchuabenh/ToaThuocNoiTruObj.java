package com.vnpt.duoc.duocModule.objects.khamchuabenh;

import java.util.Date;

public class ToaThuocNoiTruObj {
    private long maUser; // Mã người dùng
    private long idGiaoDich; // id giao dịch
    private long soNgaySuDung; // Số ngày sử dụng
    private String dvtt; // Đơn vị trục thuộc
    private String maToaThuoc; // Mã toa thuốc
    private long maKhoVatTu; // Mã kho vật tư ra thuốc
    private long maVatTu; // Mã vật tư ra thuốc
    private String tenVatTu; // Tên vật tư ra thuốc
    private String hoatChat; // Hoạt chất vật tư ra thuốc
    private String dvt; // Đơn vị tính
    private String nghiepVu; // Nghiệp vụ
    private Double soLuong; // Số lượng
    private Double soLuongThucLinh; // Số lượng thực lĩnh
    private Double donGiaBV; // Đơn giá bệnh viện
    private Double donGiaBH; // Đơn giá bảo hiểm
    private Double thanhTien; // Thành tiền
    private long soNgay; // Số ngày
    private long sang; // Số thuốc sử dụng vào buổi sáng
    private long trua; // Số thuốc sử dụng vào buổi trưa
    private long chieu; // Số thuốc sử dụng vào buổi chiều
    private long toi; // Số thuốc sử dụng vào buổi tối
    private Date ngayRaToa; // Ngày ra toa
    private String ghiChu; // Ghi chú
    private String maBacSi; // Mã bác sĩ ra thuốc
    private int daThanhToan; // Đã thanh toán hay chưa: 0: Chưa     1: Đã thanh toán
    private String cachSuDung; // Cách sử dụng thuốc
    private int nam; // Năm
    private String sttDieuTri; // Số thứ tự điều trị
    private String sttBenhAn; // Số thứ tự bệnh án
    private String sttDotDieuTri; // Số thứ tự đợt điều trị
    private int tuTuThuoc; // Từ tủ thuốc   0: Ra từ kho    1: Ra từ tủ
    private int coBHYT; // Có BHYT (Tỉ lệ)
    private int namHienTai; // Năm hiện tại
    private String maBenhNhan; // Mã bệnh nhân
    private String soPhieuThanhToan; // Số phiếu thanh toán
    private Date ngayHienTai; // Ngày hiện tại
    private String maPhongBan; // Mã phòng ban
    private int soVaoVien; // Số vào viện
    private int soVaoVienDT; // Số vào viện điều trị
    private String soPhieu; // Số phiếu
    private String maCHDA; // Mã chuẩn đoán hình ảnh
    private String maGoiDichVu; // Mã gói dịch vụ
    private String soPhieuTTPT; // Số phiếu tiểu thuật phẫu thuật
    private String maTTPT; // Mã tiểu thuật phẩu thuật
    private String soPhieuXN; // Số phiếu xét nghiệm
    private String maKhoa; // Mã khoa
    private String maPhong; // Mã phòng
    private int tachToaDichVu; // Tách toa dịch vụ: 0: không    1: tách


    public int getTachToaDichVu() {
        return tachToaDichVu;
    }

    public void setTachToaDichVu(int tachToaDichVu) {
        this.tachToaDichVu = tachToaDichVu;
    }

    public long getMaUser() {
        return maUser;
    }

    public void setMaUser(long maUser) {
        this.maUser = maUser;
    }

    public long getIdGiaoDich() {
        return idGiaoDich;
    }

    public void setIdGiaoDich(long idGiaoDich) {
        this.idGiaoDich = idGiaoDich;
    }

    public long getSoNgaySuDung() {
        return soNgaySuDung;
    }

    public void setSoNgaySuDung(long soNgaySuDung) {
        this.soNgaySuDung = soNgaySuDung;
    }

    public String getDvtt() {
        return dvtt;
    }

    public void setDvtt(String dvtt) {
        this.dvtt = dvtt;
    }

    public String getMaToaThuoc() {
        return maToaThuoc;
    }

    public void setMaToaThuoc(String maToaThuoc) {
        this.maToaThuoc = maToaThuoc;
    }

    public long getMaKhoVatTu() {
        return maKhoVatTu;
    }

    public void setMaKhoVatTu(long maKhoVatTu) {
        this.maKhoVatTu = maKhoVatTu;
    }

    public long getMaVatTu() {
        return maVatTu;
    }

    public void setMaVatTu(long maVatTu) {
        this.maVatTu = maVatTu;
    }

    public String getTenVatTu() {
        return tenVatTu;
    }

    public void setTenVatTu(String tenVatTu) {
        this.tenVatTu = tenVatTu;
    }

    public String getHoatChat() {
        return hoatChat;
    }

    public void setHoatChat(String hoatChat) {
        this.hoatChat = hoatChat;
    }

    public String getDvt() {
        return dvt;
    }

    public void setDvt(String dvt) {
        this.dvt = dvt;
    }

    public String getNghiepVu() {
        return nghiepVu;
    }

    public void setNghiepVu(String nghiepVu) {
        this.nghiepVu = nghiepVu;
    }

    public Double getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Double soLuong) {
        this.soLuong = soLuong;
    }

    public Double getSoLuongThucLinh() {
        return soLuongThucLinh;
    }

    public void setSoLuongThucLinh(Double soLuongThucLinh) {
        this.soLuongThucLinh = soLuongThucLinh;
    }

    public Double getDonGiaBV() {
        return donGiaBV;
    }

    public void setDonGiaBV(Double donGiaBV) {
        this.donGiaBV = donGiaBV;
    }

    public Double getDonGiaBH() {
        return donGiaBH;
    }

    public void setDonGiaBH(Double donGiaBH) {
        this.donGiaBH = donGiaBH;
    }

    public Double getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(Double thanhTien) {
        this.thanhTien = thanhTien;
    }

    public long getSoNgay() {
        return soNgay;
    }

    public void setSoNgay(long soNgay) {
        this.soNgay = soNgay;
    }

    public long getSang() {
        return sang;
    }

    public void setSang(long sang) {
        this.sang = sang;
    }

    public long getTrua() {
        return trua;
    }

    public void setTrua(long trua) {
        this.trua = trua;
    }

    public long getChieu() {
        return chieu;
    }

    public void setChieu(long chieu) {
        this.chieu = chieu;
    }

    public long getToi() {
        return toi;
    }

    public void setToi(long toi) {
        this.toi = toi;
    }

    public Date getNgayRaToa() {
        return ngayRaToa;
    }

    public void setNgayRaToa(Date ngayRaToa) {
        this.ngayRaToa = ngayRaToa;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getMaBacSi() {
        return maBacSi;
    }

    public void setMaBacSi(String maBacSi) {
        this.maBacSi = maBacSi;
    }

    public int getDaThanhToan() {
        return daThanhToan;
    }

    public void setDaThanhToan(int daThanhToan) {
        this.daThanhToan = daThanhToan;
    }

    public String getCachSuDung() {
        return cachSuDung;
    }

    public void setCachSuDung(String cachSuDung) {
        this.cachSuDung = cachSuDung;
    }

    public int getNam() {
        return nam;
    }

    public void setNam(int nam) {
        this.nam = nam;
    }

    public String getSttDieuTri() {
        return sttDieuTri;
    }

    public void setSttDieuTri(String sttDieuTri) {
        this.sttDieuTri = sttDieuTri;
    }

    public String getSttBenhAn() {
        return sttBenhAn;
    }

    public void setSttBenhAn(String sttBenhAn) {
        this.sttBenhAn = sttBenhAn;
    }

    public String getSttDotDieuTri() {
        return sttDotDieuTri;
    }

    public void setSttDotDieuTri(String sttDotDieuTri) {
        this.sttDotDieuTri = sttDotDieuTri;
    }

    public int getTuTuThuoc() {
        return tuTuThuoc;
    }

    public void setTuTuThuoc(int tuTuThuoc) {
        this.tuTuThuoc = tuTuThuoc;
    }

    public int getCoBHYT() {
        return coBHYT;
    }

    public void setCoBHYT(int coBHYT) {
        this.coBHYT = coBHYT;
    }

    public int getNamHienTai() {
        return namHienTai;
    }

    public void setNamHienTai(int namHienTai) {
        this.namHienTai = namHienTai;
    }

    public String getMaBenhNhan() {
        return maBenhNhan;
    }

    public void setMaBenhNhan(String maBenhNhan) {
        this.maBenhNhan = maBenhNhan;
    }

    public String getSoPhieuThanhToan() {
        return soPhieuThanhToan;
    }

    public void setSoPhieuThanhToan(String soPhieuThanhToan) {
        this.soPhieuThanhToan = soPhieuThanhToan;
    }

    public Date getNgayHienTai() {
        return ngayHienTai;
    }

    public void setNgayHienTai(Date ngayHienTai) {
        this.ngayHienTai = ngayHienTai;
    }

    public String getMaPhongBan() {
        return maPhongBan;
    }

    public void setMaPhongBan(String maPhongBan) {
        this.maPhongBan = maPhongBan;
    }

    public int getSoVaoVien() {
        return soVaoVien;
    }

    public void setSoVaoVien(int soVaoVien) {
        this.soVaoVien = soVaoVien;
    }

    public int getSoVaoVienDT() {
        return soVaoVienDT;
    }

    public void setSoVaoVienDT(int soVaoVienDT) {
        this.soVaoVienDT = soVaoVienDT;
    }

    public String getSoPhieu() {
        return soPhieu;
    }

    public void setSoPhieu(String soPhieu) {
        this.soPhieu = soPhieu;
    }

    public String getMaCHDA() {
        return maCHDA;
    }

    public void setMaCHDA(String maCHDA) {
        this.maCHDA = maCHDA;
    }

    public String getMaGoiDichVu() {
        return maGoiDichVu;
    }

    public void setMaGoiDichVu(String maGoiDichVu) {
        this.maGoiDichVu = maGoiDichVu;
    }

    public String getSoPhieuTTPT() {
        return soPhieuTTPT;
    }

    public void setSoPhieuTTPT(String soPhieuTTPT) {
        this.soPhieuTTPT = soPhieuTTPT;
    }

    public String getMaTTPT() {
        return maTTPT;
    }

    public void setMaTTPT(String maTTPT) {
        this.maTTPT = maTTPT;
    }

    public String getSoPhieuXN() {
        return soPhieuXN;
    }

    public void setSoPhieuXN(String soPhieuXN) {
        this.soPhieuXN = soPhieuXN;
    }

    public String getMaKhoa() {
        return maKhoa;
    }

    public void setMaKhoa(String maKhoa) {
        this.maKhoa = maKhoa;
    }

    public String getMaPhong() {
        return maPhong;
    }

    public void setMaPhong(String maPhong) {
        this.maPhong = maPhong;
    }
}
