package com.vnpt.duoc.duocModule.dao.nhapkho;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vnpt.duoc.duocModule.objects.nhapkho.ChiTietNhapKhoObj;
import com.vnpt.duoc.duocModule.objects.nhapkho.NhapKhoObj;
import com.zaxxer.hikari.HikariDataSource;
import liquibase.pro.packaged.J;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
import com.vnpt.duoc.jdbc.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
//@Transactional(propagation = Propagation.REQUIRES_NEW)
public class NhapKhoDAOImp implements NhapKhoDAO {

    private final HikariDataSource dataSourceDuocV2;

    @Autowired
    public NhapKhoDAOImp(HikariDataSource dataSourceDuocV2) { this.dataSourceDuocV2 = dataSourceDuocV2; }

    @Override
    public List getDanhSachPhieuNhapKho(String dvtt, String tuNgay, String denNgay, String maNghiepVu) {
        String sql = "call his_duocv2.duoc_danhsachphieunhapkho_v2(?,?,?,?)#c,t,t,s,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{tuNgay, denNgay, dvtt, maNghiepVu});
    }

    @Override
    public List getDanhSachNguonDuoc() {
        String sql = "call HIS_MANAGER.DC_SP_NGUONDUOC_SEL()#c";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List getDanhSachNhaCungCap() {
        String sql = "SELECT MANHACC, TENNHACC FROM HIS_MANAGER.DC_TB_NHACC WHERE TRANGTHAI=1";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List getDanhSachKhoNhap(String tenNghiepVu, String dvtt) {
        String sql = "call HIS_MANAGER.DC_SP_HT_KHODUOC_XL_NV(?,?)#c,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{tenNghiepVu, dvtt});
    }

    @Override
    public List getDanhSachNguoiNhan(String dvtt, String tenKhoa) {
        String sql = "call HIS_MANAGER.DANHSACHNHANVIEN(?,?)#c,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, tenKhoa});
    }

    @Override
    public List getDanhSachHoiDongKiemNhap(String dvtt) {
        String sql = "SELECT MAHDKN, TENHDKN FROM HIS_MANAGER.BDH_DMHOIDONGKIEMNHAP WHERE DVTT = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql,new Object[]{dvtt});
    }

    @Override
    public List getAllThamSoDonVi(String dvtt) {
        String sql = "call HIS_DUOCV2.GET_ALL_TSDV(?)#c,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql,new Object[]{dvtt});
    }

    @Override
    public List getDanhSachVatTu(String dvtt) {
        String sql = "SELECT * FROM HIS_MANAGER.DC_TB_VATTU WHERE DVTT = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt});
    }

    @Override
    public Map addPhieuNhapKho(NhapKhoObj nhapKhoObj) {
        try {
            String sql = "call HIS_DUOCV2.DC_SP_NHAPKHO_NCC_INS_V2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" +
                "#c,s,s,t,l,s,t,l,l,s,l,s,s,s,l,s,l,s,l";
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
            return jdbcTemplate.queryForMap(sql, new Object[]{
                nhapKhoObj.getDvtt(),
                nhapKhoObj.getSoHoaDon(),
                nhapKhoObj.getNgayHoaDon(),
                nhapKhoObj.getVat(),
                nhapKhoObj.getGhiChu(),
                nhapKhoObj.getNgayNhap(),
                nhapKhoObj.getMaNhaCungCap(),
                nhapKhoObj.getMaKhoNhan(),
                nhapKhoObj.getMaNguoiNhan(),
                nhapKhoObj.getNguoiTao(),
                nhapKhoObj.getNguonDuoc(),
                nhapKhoObj.getSoHopDong(),
                nhapKhoObj.getNguoiGiaoHang(),
                nhapKhoObj.getPhanTramGiaBanLe(),
                nhapKhoObj.getKyHieuHD(),
                nhapKhoObj.getMaNghiepVuChuyen(),
                nhapKhoObj.getMaPhongBanNhap(),
                nhapKhoObj.getIdGiaoDich()
            });
        } catch (Exception e) {
            return new HashMap();
        }
    }

    @Override
    public int updatePhieuNhapKho(NhapKhoObj nhapKhoObj) {
        try {
            String sql = "call HIS_DUOCV2.DC_SP_NHAPKHOTUNHACC_UPDATE_V2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" +
                "#l,l,s,s,t,l,s,t,l,s,s,l,s,s,s,l";
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
            return jdbcTemplate.queryForObject(sql, new Object[]{
                nhapKhoObj.getIdNhapKhoTuNhaCungCap(),
                nhapKhoObj.getDvtt(),
                nhapKhoObj.getSoHoaDon(),
                nhapKhoObj.getNgayHoaDon(),
                nhapKhoObj.getVat(),
                nhapKhoObj.getGhiChu(),
                nhapKhoObj.getNgayNhap(),
                nhapKhoObj.getMaNhaCungCap(),
                nhapKhoObj.getMaKhoNhan(),
                nhapKhoObj.getMaNguoiNhan(),
                nhapKhoObj.getNguoiTao(),
                nhapKhoObj.getSoHopDong(),
                nhapKhoObj.getNguoiGiaoHang(),
                nhapKhoObj.getKyHieuHD(),
                nhapKhoObj.getIdGiaoDich()
            }, Integer.class);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int deletePhieuNhapKho(long idPhieuNhap, String DVTT, long nguoiXoa, long idGiaoDich) {
        try {
            String sql = "call HIS_DUOCV2.DC_NHAPKHOTUNCC_PHIEU_DEL(?,?,?,?)#l,l,s,l,l";
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
            return jdbcTemplate.queryForInt(sql, new Object[]{idPhieuNhap, DVTT, nguoiXoa, idGiaoDich});
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public List getDanhSachChiTietPhieuNhapKho(int loaiDanhSach, long idNhapKho) {
        String sql = "call HIS_DUOCV2.DC_NHAPKHOTUNCC_HOADON_SEL(?,?)#c,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{loaiDanhSach, idNhapKho});
    }

    @Override
    public List getDanhSachNhanVienKiemNhap(long idNhapKhoTuNhaCC, String dvtt) {
        String sql = "call HIS_DUOCV2.LAYDANHSACHNHANVIEN_KIEMNHAP(?,?)#c,l,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{idNhapKhoTuNhaCC, dvtt});
    }

    @Override
    public int addNhanVienKiemNhap(long idNhapKhoNhaCC, String soPhieuNhap, String dvtt, int maNhanVien, String tenNhanVien, String tenChucDanh) {
        String sql = "call HIS_DUOCV2.THEMNHANVIEN_KIEMNHAP(?,?,?,?,?,?)#l,l,s,s,l,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{idNhapKhoNhaCC, soPhieuNhap, dvtt, maNhanVien, tenNhanVien, tenChucDanh}, Integer.class);
    }

    @Override
    public int deleteNhanVienKiemNhap(long idNhapKhoTuNhaCC, String dvtt, int maNhanVien) {
        String sql = "call HIS_DUOCV2.XOANHANVIEN_KIEMNHAP(?,?,?)" + "#l,l,s,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{idNhapKhoTuNhaCC, dvtt, maNhanVien}, Integer.class);
    }

    @Override
    public int deleteChiTietHoaDon(long idChiTiet, long idPhieuNhap, String dvtt, long nguoiXoa, long idGiaoDich) {
        String sql = "call HIS_DUOCV2.DC_NHAPKHOTUNCC_HOADON_DEL_V2(?,?,?,?,?)#l,l,l,s,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{idChiTiet, idPhieuNhap, dvtt, nguoiXoa, idGiaoDich}, Integer.class);
    }

    @Override
    public int deleteChiTietNhapKho(long soNhapKhoChiTiet, String dvtt, long nguoiXoa, long idGiaoDich) {
        String sql = "call HIS_DUOCV2.DC_SP_NHAPKHOTUNCC_CT_DEL_V2(?,?,?,?)#l,l,s,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForInt(sql, new Object[]{soNhapKhoChiTiet, dvtt, nguoiXoa, idGiaoDich});
    }

    @Override
    public int deleteNhapKho(long idNhapKhoTuNhaCC, String dvtt, long nguoiXoa, long idGiaoDich, String maKhoa, String maPhong) {
        String sql = "call DC_SP_NHAPKHO_NCC_NK_DEL_V2(?,?,?,?,?,?)#l,l,s,l,l,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForInt(sql, new Object[]{idNhapKhoTuNhaCC, dvtt, nguoiXoa, idGiaoDich, maKhoa, maPhong});
    }

    @Override
    public Map addChiTietHoaDon(ChiTietNhapKhoObj chiTietNhapKhoObj) {
        String sql = "call HIS_DUOCV2.DC_NHAPKHOTUNCC_HOADON_INS_V2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" +
            "#c,s,l,s,s,s,l,s,t,t,l,l,l,l,d,d,d,d,d,d,d,l,s,l,l,s,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        try {
            return jdbcTemplate.queryForMap(sql, new Object[]{
                chiTietNhapKhoObj.getNghiepVu(),
                chiTietNhapKhoObj.getIdPhieuNhap(),
                chiTietNhapKhoObj.getDvtt(),
                chiTietNhapKhoObj.getSoHoaDon(),
                chiTietNhapKhoObj.getQuyCach(),
                chiTietNhapKhoObj.getQuyCachQuyDoi(),
                chiTietNhapKhoObj.getSoLoSanXuat(),
                chiTietNhapKhoObj.getNgaySanXuat(),
                chiTietNhapKhoObj.getNgayHetHan(),
                chiTietNhapKhoObj.getMaVatTu(),
                chiTietNhapKhoObj.getVat(),
                chiTietNhapKhoObj.getSoLuong(),
                chiTietNhapKhoObj.getSoLuongQuyDoi(),
                chiTietNhapKhoObj.getDonGiaHoaDon(),
                chiTietNhapKhoObj.getDonGiaQuyDoi(),
                chiTietNhapKhoObj.getDonGiaQuyDoiVAT(),
                chiTietNhapKhoObj.getDonGia(),
                chiTietNhapKhoObj.getThanhTien(),
                chiTietNhapKhoObj.getTienVAT(),
                chiTietNhapKhoObj.getThanhTienVAT(),
                chiTietNhapKhoObj.getMaNguoiNhap(),
                chiTietNhapKhoObj.getGhiChu(),
                chiTietNhapKhoObj.getMaDonViNhan(),
                chiTietNhapKhoObj.getDuocConLai(),
                chiTietNhapKhoObj.getSoPhieuNhap(),
                chiTietNhapKhoObj.getIdGiaoDich()
            });
        } catch (Exception e) {
            return new HashMap();
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public int addChiTietNhapKho(ChiTietNhapKhoObj chiTietNhapKhoObj) {
        String sql = "call HIS_DUOCV2.DC_SP_NHAPKHOTUNCC_CT_INS_V2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" +
            "#l,s,s,s,l,s,s,t,t,l,l,l,d,d,d,t,l,d,s,l,l,d,d,l,d,l,l,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{
            chiTietNhapKhoObj.getMaNhapKhoVatTu(),
            chiTietNhapKhoObj.getSoPhieuNhap(),
            chiTietNhapKhoObj.getDvtt(),
            chiTietNhapKhoObj.getMaDonViNhan(),
            chiTietNhapKhoObj.getSoHoaDon(),
            chiTietNhapKhoObj.getSoLoSanXuat(),
            chiTietNhapKhoObj.getNgaySanXuat(),
            chiTietNhapKhoObj.getNgayHetHan(),
            chiTietNhapKhoObj.getMaVatTu(),
            chiTietNhapKhoObj.getVat(),
            chiTietNhapKhoObj.getSoLuong(),
            chiTietNhapKhoObj.getDonGiaTruocVAT(),
            chiTietNhapKhoObj.getDonGia(),
            chiTietNhapKhoObj.getThanhTien(),
            chiTietNhapKhoObj.getNgayNhap(),
            chiTietNhapKhoObj.getMaNguoiNhap(),
            chiTietNhapKhoObj.getThanhTienHoaDon(),
            chiTietNhapKhoObj.getGhiChu(),
            chiTietNhapKhoObj.getIdNhapKhoCT(),
            chiTietNhapKhoObj.getIdPhieuNhap(),
            chiTietNhapKhoObj.getPhanTramHaoHut(),
            chiTietNhapKhoObj.getDonGiaTruocHaoHut(),
            chiTietNhapKhoObj.getSoLuongTruocHaoHut(),
            chiTietNhapKhoObj.getDonGiaSauHaoHut(),
            chiTietNhapKhoObj.getPhanTramDonGiaDV(),
            chiTietNhapKhoObj.getDonGiaDV(),
            chiTietNhapKhoObj.getNghiepVu(),
            chiTietNhapKhoObj.getIdGiaoDich()
        }, Integer.class);
    }

    @Override
    public int checkThuocDaSuDung(long idNhapKho, long nghiepVu) {
        String sql = "call HIS_DUOCV2.DC_CHECK_THUOC_DASUDUNG(?,?)#s,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{idNhapKho, nghiepVu}, Integer.class);
    }

    @Override
    public List getDonGiaVaSoLuong(String soluong, String dongia, String soluongquycach, String dongiachinhsua, String vat, String tienvatsauchinhsua) {
        String sql = "call HIS_MANAGER.dc_sp_nk_ncc_DG_SL_tienVAT(?,?,?,?,?,?)#c,d,d,l,d,l,d";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{soluong, dongia, soluongquycach, dongiachinhsua, vat, tienvatsauchinhsua});
    }


    // Print

    @Override
    public List getThongTinPhieuNhapKho(String soPhieuNhap, String soHoaDon, String DVTT) {
        String sql = "call HIS_DUOCV2.DC_SP_THONGTIN_PHIEUNHAPKHO(?,?,?)#c,s,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{soPhieuNhap, soHoaDon, DVTT});
    }

    @Override
    public String getTongTienPhieuNhapKho(long idNhapKhoTuNhaCC, String loai, String trangThai, String DVTT) {
        String sql = "call HIS_DUOCV2.DC_SP_NHAPKHONCC_TONGTIEN_V2(?,?,?,?)#l,l,s,s,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{idNhapKhoTuNhaCC, loai, trangThai, DVTT}, String.class);
    }

    @Override
    public List getDanhSachVatTuMau(String dvtt) {
        String sql = "call HIS_DUOCV2.DC_SP_DANHSACHVATTU_XLS_V2(?)#c,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt});
    }

    @Override
    public Map getHoiDongKiemNhap(long id, String dvtt) {
        String sql = "select * from HIS_MANAGER.BDH_DMHOIDONGKIEMNHAP_CHITIET where MAHDKN = ? and dvtt = ?";
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
            return jdbcTemplate.queryForMap(sql, new Object[]{id, dvtt});
        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap();
        }
    }
}
