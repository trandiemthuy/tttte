package com.vnpt.duoc.duocModule.objects.xuatduocbanle;

public class ThongTinPhieuXuatDuocBanLeObj {
    private String dvtt;
    private String nghiepVu;
    private String maToaThuoc;
    private String tenKhachHang;
    private String ngayXuat;
    private String ghiChu;
    private int noiXuat;
    private String tenNoiXuat;
    private String maBacSi;
    private String tenBacSi;
    private String maICD;
    private String tenICD;
    private String maBenhPhu;
    private String tenBenhPhu;
    private String maPhongBan;
    private String maPhongBenh;
    private int daThanhToan; // 0: Chưa thanh toán 1: Đã thanh toán
    private long idGiaoDich;
    private String diaChi;
    private int tuoi;
    private String liDoXuat;
    private long maNhanVien;

    public long getMaNhanVien() {
        return maNhanVien;
    }

    public void setMaNhanVien(long maNhanVien) {
        this.maNhanVien = maNhanVien;
    }

    public int getTuoi() {
        return tuoi;
    }

    public void setTuoi(int tuoi) {
        this.tuoi = tuoi;
    }

    public String getLiDoXuat() {
        return liDoXuat;
    }

    public void setLiDoXuat(String liDoXuat) {
        this.liDoXuat = liDoXuat;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getDvtt() {
        return dvtt;
    }

    public void setDvtt(String dvtt) {
        this.dvtt = dvtt;
    }

    public String getNghiepVu() {
        return nghiepVu;
    }

    public void setNghiepVu(String nghiepVu) {
        this.nghiepVu = nghiepVu;
    }

    public String getMaToaThuoc() {
        return maToaThuoc;
    }

    public void setMaToaThuoc(String maToaThuoc) {
        this.maToaThuoc = maToaThuoc;
    }

    public String getTenKhachHang() {
        return tenKhachHang;
    }

    public void setTenKhachHang(String tenKhachHang) {
        this.tenKhachHang = tenKhachHang;
    }

    public String getNgayXuat() {
        return ngayXuat;
    }

    public void setNgayXuat(String ngayXuat) {
        this.ngayXuat = ngayXuat;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public int getNoiXuat() {
        return noiXuat;
    }

    public void setNoiXuat(int noiXuat) {
        this.noiXuat = noiXuat;
    }

    public String getTenNoiXuat() {
        return tenNoiXuat;
    }

    public void setTenNoiXuat(String tenNoiXuat) {
        this.tenNoiXuat = tenNoiXuat;
    }

    public String getMaBacSi() {
        return maBacSi;
    }

    public void setMaBacSi(String maBacSi) {
        this.maBacSi = maBacSi;
    }

    public String getTenBacSi() {
        return tenBacSi;
    }

    public void setTenBacSi(String tenBacSi) {
        this.tenBacSi = tenBacSi;
    }

    public String getMaICD() {
        return maICD;
    }

    public void setMaICD(String maICD) {
        this.maICD = maICD;
    }

    public String getTenICD() {
        return tenICD;
    }

    public void setTenICD(String tenICD) {
        this.tenICD = tenICD;
    }

    public String getMaBenhPhu() {
        return maBenhPhu;
    }

    public void setMaBenhPhu(String maBenhPhu) {
        this.maBenhPhu = maBenhPhu;
    }

    public String getTenBenhPhu() {
        return tenBenhPhu;
    }

    public void setTenBenhPhu(String tenBenhPhu) {
        this.tenBenhPhu = tenBenhPhu;
    }

    public String getMaPhongBan() {
        return maPhongBan;
    }

    public void setMaPhongBan(String maPhongBan) {
        this.maPhongBan = maPhongBan;
    }

    public String getMaPhongBenh() {
        return maPhongBenh;
    }

    public void setMaPhongBenh(String maPhongBenh) {
        this.maPhongBenh = maPhongBenh;
    }

    public int getDaThanhToan() {
        return daThanhToan;
    }

    public void setDaThanhToan(int daThanhToan) {
        this.daThanhToan = daThanhToan;
    }

    public long getIdGiaoDich() {
        return idGiaoDich;
    }

    public void setIdGiaoDich(long idGiaoDich) {
        this.idGiaoDich = idGiaoDich;
    }
}
