package com.vnpt.duoc.duocModule.dao.xuatduoc;

import com.vnpt.duoc.duocModule.objects.xuatduoc.ThongTinPhieuXuatDuocObj;

import java.util.List;

public interface XuatDuocDAO {
    List getDanhSachBenhNhanXuatDuoc(String dvtt, int maNghiepVu, String ngay, int trangThai, String maKhoa);
    List getDanhSachVatTuXuatDuoc(String dvtt, int maNghiepVu, String maToaThuoc, int trangThai, int soVaoVien);
    int xuatDuocVatTu(ThongTinPhieuXuatDuocObj thongTinPhieuXuatDuocObj, int trangThai);
    List getDanhSachXuatDuocTongHop(String dvtt, String ngayLap, String nghiepVu, String hinhThuc);
    int traThuocVeKho(ThongTinPhieuXuatDuocObj thongTinPhieuXuatDuocObj, String nghiepVu);
}
