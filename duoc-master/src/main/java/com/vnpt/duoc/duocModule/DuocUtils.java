package com.vnpt.duoc.duocModule;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DuocUtils {
    public static Date convertStringToDate(String date, String format) throws ParseException {
        return new SimpleDateFormat(format).parse(date);
    }
    public static String convertFromFormatToDate(String date, String formatInput, String formatOutput) {
        switch (formatOutput) {
            case "": {
                String [] arrDate = date.split(formatInput);
                String day = arrDate[2];
                String month = arrDate[1];
                String year = arrDate[0];
                return day + formatOutput + month + formatOutput + year;
            }
            case "/": {
                String [] arrDate = date.split(formatInput);
                String day = arrDate[2];
                String month = arrDate[1];
                String year = arrDate[0];
                return day + formatOutput + month + formatOutput + year;
            }
            case "-": {
                String [] arrDate = date.split(formatInput);
                String day = arrDate[0];
                String month = arrDate[1];
                String year = arrDate[2];
                return day + formatOutput + month + formatOutput + year;
            }

            default: {
                return "";
            }
        }
    }
    public static String getNgayHienTai() {
        return new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
    }

    private static String makeDivContent(String title, String content) {
        String result =
            "<div" +
                "  style='width: 100%;" +
                "         height: auto;" +
                "         padding-top: 25vh; " +
                "         padding-bottom: 25vh;" +
                "         background-image: radial-gradient( circle farthest-corner at 10% 20%,  rgba(77,97,252,0.63) 0%, rgba(77,97,252,1) 90% );'" +
                ">" +
                "  <div" +
                "    style='margin: auto;" +
                "    width: 70%;" +
                "    height: auto;" +
                "    padding: 10px;" +
                "    text-align: center;" +
                "    background: rgba(255, 255, 255, 0.1);" +
                "    border: solid 1px rgba(255, 255, 255, 0.2);" +
                "    border-radius: 5px;" +
//                "    box-shadow: 5px 10px 18px rgba(255, 255, 255, 0.2);" +
                "    box-shadow: 0 0 25px rgba(0, 0, 0, 0.1), inset 0 0 1px rgba(255, 255, 255, 0.6);" +
                "    color: white; " +
                "    word-wrap: break-word;'" +
                "  >" +
                "  <div style='border-bottom: solid 1px rgba(255, 255, 255, 0.3);'>" +
                "    <h1 style='color: rgba(0, 35, 122, 0.7);'>" +
                        title +
                "    </h1>" +
                "  </div>" +
                "  <h2 style='color: rgba(0, 35, 122, 0.7);'>" +
                        content +
                "  </h2>" +
                "  </div>" +
                "</div>";
        return result;
    }
    public static void responseFileNotFoundToClient(FileNotFoundException ex, HttpServletResponse response) {
        try {
            String message = ex.getMessage();
            message = message.substring(message.lastIndexOf("/") + 1, message.lastIndexOf("]"));
            String errTitle = DuocConfigs.ERROR_TITLE_FILENOTFOUND_EXCEPTIONS;
            String errContent= "Tên tập tin: " + message;
            String responseToClient = makeDivContent(errTitle, errContent);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().write(responseToClient);
            response.getWriter().flush();
            response.getWriter().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void responseErrorToClient(String title, String content, HttpServletResponse response) {
        try {
            String responseToClient = makeDivContent(title, content);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().write(responseToClient);
            response.getWriter().flush();
            response.getWriter().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
