package com.vnpt.duoc.duocModule.dao.nhapkho;

import com.vnpt.duoc.duocModule.objects.nhapkho.ChiTietNhapKhoObj;
import com.vnpt.duoc.duocModule.objects.nhapkho.NhapKhoObj;

import java.util.List;
import java.util.Map;

public interface NhapKhoDAO {
    List getDanhSachPhieuNhapKho(String dvtt, String tuNgay, String denNgay, String maNghiepVu);
    List getDanhSachNguonDuoc();
    List getDanhSachNhaCungCap();
    List getDanhSachKhoNhap(String maNghiepVu, String dvtt);
    List getDanhSachNguoiNhan(String dvtt, String tenKhoa);
    List getDanhSachHoiDongKiemNhap(String dvtt);
    List getAllThamSoDonVi(String dvtt);
    List getDanhSachVatTu(String dvtt);
    Map addPhieuNhapKho(NhapKhoObj nhapKhoObj);
    int updatePhieuNhapKho(NhapKhoObj nhapKhoObj);
    int deletePhieuNhapKho(long idPhieuNhap, String DVTT, long nguoiXoa, long idGiaoDich);
    List getDanhSachChiTietPhieuNhapKho(int loaiDanhSach, long idNhapKho);
    List getDanhSachNhanVienKiemNhap(long idNhapKhoTuNhaCC, String dvtt);
    int addNhanVienKiemNhap(long idNhapKhoNhaCC, String soPhieuNhap, String dvtt, int maNhanVien, String tenNhanVien, String tenChucDanh);
    int deleteNhanVienKiemNhap(long idNhapKhoTuNhaCC, String dvtt, int maNhanVien);
    int deleteChiTietHoaDon(long idChitiet, long idPhieuNhap, String dvtt, long nguoiXoa, long idGiaoDich);
    int deleteChiTietNhapKho(long soNhapKhoChiTiet, String dvtt, long nguoiXoa, long idGiaoDich);
    int deleteNhapKho(long idNhapKhoTuNhaCC, String dvtt, long nguoiXoa, long idGiaoDich, String maKhoa, String maPhong);
    Map addChiTietHoaDon(ChiTietNhapKhoObj chiTietNhapKhoObj);
    int addChiTietNhapKho(ChiTietNhapKhoObj chiTietNhapKhoObj);
    int checkThuocDaSuDung(long idNhapKho, long nghiepVu);
    List getDonGiaVaSoLuong(String soluong, String dongia, String soluongquycach, String dongiachinhsua, String vat, String tienvatsauchinhsua);

    // Print
    List getThongTinPhieuNhapKho(String soPhieuNhap, String soHoaDon, String DVTT);
    String getTongTienPhieuNhapKho(long idNhapKhoTuNhaCC, String loai, String trangThai, String DVTT);
    List getDanhSachVatTuMau(String dvtt);
    Map getHoiDongKiemNhap(long id, String dvtt);
}
