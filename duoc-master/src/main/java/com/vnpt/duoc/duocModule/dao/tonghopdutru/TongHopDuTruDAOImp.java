package com.vnpt.duoc.duocModule.dao.tonghopdutru;

import com.vnpt.duoc.duocModule.objects.tonghopdutru.ThongTinPhieuDuTruObj;
import com.vnpt.duoc.jdbc.JdbcTemplate;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class TongHopDuTruDAOImp implements TongHopDuTruDAO {
    private JdbcTemplate jdbcTemplate;

    public TongHopDuTruDAOImp(HikariDataSource dataSourceDuocV2) {
        this.jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
    }

    @Override
    public List getDanhSachPhieuChuaDuTru(String dvtt, String maPhongBan, String tuNgay, String denNgay, int trangThai, int bant) {
        String sql = "call HIS_DUOCV2.NOITRU_TOATHUOC_SEL_PHIEUDT_V2(?,?,?,?,?,?)#c,s,s,t,t,l,l";
        return jdbcTemplate.queryForList(sql, new Object[]{
            dvtt,
            maPhongBan,
            tuNgay,
            denNgay,
            trangThai,
            bant
        });
    }

    @Override
    public List getDanhSachPhieuDuTru(String dvtt, String tuNgay, String denNgay, String maPhongBan, int trangThai, int isBANT) {
        String sql = "call HIS_DUOCV2.DC_DTRU_NOITRU_DSPHIEUTONG_SEL (?,?,?,?,?,?)#c,s,t,t,s,l,l";
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt,
            tuNgay,
            denNgay,
            maPhongBan,
            trangThai,
            isBANT });
    }

    @Override
    public List getDanhSachVatTuPhieuDutru(String dvtt, long idPhieuDuTruTong, String maPhongBan, int hinhThucXem, int thoiGian) {
        String sql = "call HIS_DUOCV2.NOITRU_DSVATTU_PHIEUDUTRU_V2 (?,?,?,?,?)#c,s,l,s,l,l";
        return jdbcTemplate.queryForList(sql, new Object[]{ dvtt, idPhieuDuTruTong, maPhongBan , hinhThucXem, thoiGian});
    }

    @Override
    public List getDanhSachBenhNhanPhieuDuTru(String dvtt, long idPhieuDuTruTong, String ngayTao, String maPhongBan) {
        String sql = "call HIS_DUOCV2.DC_DTRU_NOITRU_DSPDIEUTRI_SEL (?,?,?,?)#c,s,l,t,s";
        return jdbcTemplate.queryForList(sql, new Object[]{ dvtt, idPhieuDuTruTong, ngayTao, maPhongBan });
    }

    @Override
    public Map tongHopDuTruTheoPhieu(String dvtt, Date ngayGioHeThong, long maUser, String ghiChu, int bant, String maPhongBan, String idDieuTris, long idGiaoDich, String maPhongBanPhieu) {
        String sql = "call HIS_DUOCV2.NOITRU_TH_DUTRU_PHIEU_TONG(?,?,?,?,?,?,?,?,?)#c,s,s,t,l,s,l,s,s,l";
        return jdbcTemplate.queryForMap(sql, new Object[]{
            maPhongBanPhieu,
            dvtt,
            ngayGioHeThong,
            maUser,
            ghiChu,
            bant,
            maPhongBan,
            idDieuTris,
            idGiaoDich
        });
    }

    @Override
    public Map tongHopDuTruTheoNgay(String dvtt, String tuNgay, String denNgay, Date ngayGioHeThong, long nguoiLap, String ghiChu, int bant, String maPhongBan, long idGiaoDich, String maPhongBanPhieu) {
        String sql = "call HIS_DUOCV2.NOITRU_TH_DUTRU_NGAY_TONG(?,?,?,?,?,?,?,?,?,?)#c,s,s,t,l,s,t,t,l,s,l";
        return jdbcTemplate.queryForMap(sql, new Object[]{
            maPhongBanPhieu,
            dvtt,
            ngayGioHeThong,
            nguoiLap,
            ghiChu,
            tuNgay,
            denNgay,
            bant,
            maPhongBan,
            idGiaoDich
        });
    }

    @Override
    public int deletePhieuDuTru(long idPhieuDuTru, String maPhongBan, String dvtt, long maNV, String idGiaoDich, String maPhong) {
        String sql = "call HIS_DUOCV2.NOITRU_PHIEUDUTRU_HUY_TONG_V2(?,?,?,?,?,?)#l,l,s,s,l,l,s";
        return jdbcTemplate.queryForObject(sql, new Object[]{
            idPhieuDuTru,
            maPhongBan,
            dvtt,
            maNV,
            idGiaoDich,
            maPhong
        }, Integer.class);
    }

    @Override
    public int updateTrangThaiPhieuDuTru(String dvtt, long idPhieu, int trangThai, long maNV, String idGiaoDich, String maPhongBan, String maPhongBenh) {
        String sql = "call HIS_DUOCV2.NTRU_PHIEUDTRU_CAPNHATTT_TONG(?,?,?,?,?,?,?)#l,s,l,l,l,l,s,s";
        return jdbcTemplate.queryForObject(sql, new Object[]{
            dvtt,
            idPhieu,
            trangThai,
            maNV,
            idGiaoDich,
            maPhongBan,
            maPhongBenh
        }, Integer.class);
    }


    // Tổng hợp hoàn trả

    @Override
    public List getDanhSachPhieuHoanTra(String dvtt, String tuNgay, String denNgay, String maPhongBan, int trangThai, int isBANT) {
        String sql = "call HIS_DUOCV2.DC_HTRA_NOITRU_DSPHIEUTONG_SEL(?,?,?,?,?,?)#c,s,t,t,s,l,l";
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, tuNgay, denNgay, maPhongBan, trangThai, isBANT});
    }

    @Override
    public List getDanhSachPhieuChuaHoanTra(String dvtt, String maPhongBan, String tuNgay, String denNgay, int bant) {
        String sql = "call HIS_DUOCV2.NOITRU_TOATHUOC_SEL_PHIEUHT_V2(?,?,?,?,?)#c,s,s,t,t,l";
        return jdbcTemplate.queryForList(sql, new Object[]{
            dvtt,
            maPhongBan,
            tuNgay,
            denNgay,
            bant
        });
    }

    @Override
    public List getDanhSachVatTuPhieuHoanTra(long idPhieuHoanTraTong, int hinhThucXem) {
        String sql = "call HIS_DUOCV2.NOITRU_DSVATTU_PHIEUHOANTRA_V2(?,?)#c,l,l";
        return jdbcTemplate.queryForList(sql, new Object[]{idPhieuHoanTraTong, hinhThucXem});
    }

    @Override
    public List getDanhSachBenhNhanPhieuHoanTra(long idPhieuHoanTraTong) {
        String sql = "call HIS_DUOCV2.DC_HTRA_NOITRU_DSPDIEUTRI_SEL(?)#c,l";
        return jdbcTemplate.queryForList(sql, new Object[]{idPhieuHoanTraTong});
    }

    @Override
    public Map tongHopHoanTraTheoPhieu(ThongTinPhieuDuTruObj thongTinPhieuDuTruObj) {
        String sql = "call HIS_DUOCV2.NOITRU_TH_HOANTRA_PHIEU_TONG(?,?,?,?,?,?,?,?,?)#c,s,s,t,l,s,l,s,s,l";
        return jdbcTemplate.queryForMap(sql, new Object[]{
            thongTinPhieuDuTruObj.getMaPhongBan(),
            thongTinPhieuDuTruObj.getDvtt(),
            new Date(),
            thongTinPhieuDuTruObj.getMaNguoiLap(),
            thongTinPhieuDuTruObj.getGhiChu(),
            thongTinPhieuDuTruObj.getIsBANT(),
            thongTinPhieuDuTruObj.getMaPhongBan(),
            thongTinPhieuDuTruObj.getIdPhieu(),
            thongTinPhieuDuTruObj.getIdGiaoDich()
        });
    }

    @Override
    public Map tongHopHoanTraTheoNgay(ThongTinPhieuDuTruObj thongTinPhieuDuTruObj) {
        String sql = "call HIS_DUOCV2.NOITRU_TH_HOANTRA_NGAY_TONG(?,?,?,?,?,?,?,?,?,?)#c,s,s,t,l,s,t,t,l,s,l";
        return jdbcTemplate.queryForMap(sql, new Object[]{
            thongTinPhieuDuTruObj.getMaPhongBan(),
            thongTinPhieuDuTruObj.getDvtt(),
            new Date(),
            thongTinPhieuDuTruObj.getMaNguoiLap(),
            thongTinPhieuDuTruObj.getGhiChu(),
            thongTinPhieuDuTruObj.getTuNgay(),
            thongTinPhieuDuTruObj.getDenNgay(),
            thongTinPhieuDuTruObj.getIsBANT(),
            thongTinPhieuDuTruObj.getMaPhongBan(),
            thongTinPhieuDuTruObj.getIdGiaoDich()
        });
    }

    @Override
    public int deletePhieuHoanTra(ThongTinPhieuDuTruObj thongTinPhieuDuTruObj) {
        String sql = "call HIS_DUOCV2.NOITRU_PHIEUHTRA_HUY_TONG_V2(?,?,?,?,?,?)#c,l,s,l,l,s,s";
        return jdbcTemplate.queryForObject(sql, new Object[]{
            thongTinPhieuDuTruObj.getIdPhieu(),
            thongTinPhieuDuTruObj.getDvtt(),
            thongTinPhieuDuTruObj.getMaNguoiLap(),
            thongTinPhieuDuTruObj.getIdGiaoDich(),
            thongTinPhieuDuTruObj.getMaPhongBan(),
            thongTinPhieuDuTruObj.getMaPhongBenh()
        }, Integer.class);
    }

    @Override
    public int updateTrangThaiPhieuHoanTra(ThongTinPhieuDuTruObj thongTinPhieuDuTruObj, int trangThai) {
        String sql = "call HIS_DUOCV2.NTRU_PHIEUHTRA_CAPNHATTT_TONG(?,?,?,?,?,?,?)#l,s,l,l,l,l,s,s";
        return jdbcTemplate.queryForObject(sql, new Object[]{
            thongTinPhieuDuTruObj.getDvtt(),
            thongTinPhieuDuTruObj.getIdPhieu(),
            trangThai,
            thongTinPhieuDuTruObj.getMaNguoiLap(),
            thongTinPhieuDuTruObj.getIdGiaoDich(),
            thongTinPhieuDuTruObj.getMaPhongBan(),
            thongTinPhieuDuTruObj.getMaPhongBenh()
        }, Integer.class);
    }

    @Override
    public Map getThongTinPhieuHoanTra(String dvtt, long idPhieuHoanTra, int isBANT) {
        String sql = "call HIS_DUOCV2.THONGTIN_PHIEUHOANTRA_V2(?,?,?)#c,s,l,l";
        return jdbcTemplate.queryForMap(sql, new Object[]{dvtt, idPhieuHoanTra, isBANT});
    }
}
