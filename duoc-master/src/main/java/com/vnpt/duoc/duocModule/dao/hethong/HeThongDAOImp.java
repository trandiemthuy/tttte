package com.vnpt.duoc.duocModule.dao.hethong;

import com.vnpt.duoc.jdbc.JdbcTemplate;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class HeThongDAOImp implements HeThongDAO {
    JdbcTemplate jdbcTemplate;

    public HeThongDAOImp(HikariDataSource dataSourceDuocV2) {
        this.jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
    }

    @Override
    public String getMoTaThamSoByMaThamSo(String dvtt, long maThamSo) {
        String sql = "call HIS_FW.DM_SELECT_TSHT_THEOMA(?,?)#s,s,l";
        return jdbcTemplate.queryForObject(sql, new Object[]{dvtt, maThamSo}, String.class);
    }

    @Override
    public String getTenPhongBan(String maPhongBan) {
        String sql = "select ten_phongban from his_fw.dm_phongban where ma_phongban=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{maPhongBan}, String.class);
    }

    @Override
    public Map getDuongDanBaoCao(String dvtt, String maloaireport) {
        String sql = "call HIS_MANAGER.AGG_maureport_select_dd(?,?)#c,s,l";
        return jdbcTemplate.queryForMap(sql, new Object[]{dvtt, maloaireport});
    }

    @Override
    public List getDanhSachKhoaNoiTru(String dvtt) {
        String sql = "call HIS_MANAGER.NOITRU_KHOANOITRU_SELECT(?)#c,s";
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt});
    }

    @Override
    public List getDanhSachKhoNoiTru(String dvtt, String maPhongBan, String nghiepVu) {
        String sql = "call HIS_MANAGER.KB_NOI_KHOAPHONG_GET_KHODUOC(?,?,?)#c,s,s,s";
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, maPhongBan, nghiepVu});
    }

    @Override
    public List getDanhSachBacSi(String dvtt) {
        String sql = "call HIS_MANAGER.DC_F_DS_BSY_SELECTALL(?)#c,s";
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt});
    }
}
