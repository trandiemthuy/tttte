create or replace FUNCTION "HIS_DUOCV2"."DC_HTRA_NOITRU_DSPHIEUTONG_SEL" (i_DVTT       IN VARCHAR2,
                                                                     i_tuNgay     IN DATE,
                                                                     i_denNgay    IN DATE,
                                                                     i_maPhongBan IN VARCHAR2,
                                                                     i_trangThai  IN NUMBER,
                                                                     i_bant       IN NUMBER)
  return SYS_REFCURSOR IS
  cur SYS_REFCURSOR;
begin
  IF i_trangThai = 1 THEN
    OPEN cur FOR
      SELECT ht.ID_PHIEU_HOAN_TRA_TONG,
             ht.SOPHIEUTAO_LUUTRU,
             ht.MA_DON_VI,
             to_char(ht.NGAY_HOAN_TRA, 'YYYY-MM-DD') as NGAY,
             ht.GHI_CHU,
             ht.MA_PHONG_BAN_HOAN_TRA,
             ht.NAM,
             ht.NGUOI_HOAN_TRA AS NGUOITAO,
             ht.NGAY_GIO_HOAN_TRA,
             ht.TRANG_THAI,
             nv.TEN_NHANVIEN AS TEN_NGUOI_TAO,
             pb.ten_phongban
        FROM NOITRU_PHIEUHOANTRA_TONG ht
        join his_fw.dm_phongban pb
          on ht.MA_PHONG_BAN_HOAN_TRA =  pb.ma_phongban
        , HIS_FW.DM_NHANVIEN nv

       WHERE ht.MA_DON_VI = i_DVTT
         AND trunc(ht.NGAY_HOAN_TRA) BETWEEN i_tuNgay AND i_denNgay
         AND ht.MA_PHONG_BAN_HOAN_TRA = decode(i_maPhongBan,'-1',ht.MA_PHONG_BAN_HOAN_TRA,i_maPhongBan)
         AND ht.TRANG_THAI = i_trangThai
         and ht.BANT = i_bant
         AND ht.NGUOI_HOAN_TRA = nv.ma_nhanvien
				 ORDER BY SOPHIEUTAO_LUUTRU DESC;
  ELSE
    OPEN cur FOR
      SELECT ht.ID_PHIEU_HOAN_TRA_TONG,
             ht.SOPHIEUTAO_LUUTRU,
             ht.MA_DON_VI,
             to_char(ht.NGAY_HOAN_TRA, 'YYYY-MM-DD') as NGAY,
             ht.GHI_CHU,
             ht.MA_PHONG_BAN_HOAN_TRA,
             ht.NAM,
             ht.NGUOI_HOAN_TRA AS NGUOITAO,
             ht.NGAY_GIO_HOAN_TRA,
             ht.TRANG_THAI,
             nv.TEN_NHANVIEN AS TEN_NGUOI_TAO,
             pb.ten_phongban
        FROM NOITRU_PHIEUHOANTRA_TONG ht
        join his_fw.dm_phongban pb
          on ht.MA_PHONG_BAN_HOAN_TRA =  pb.ma_phongban,
        HIS_FW.DM_NHANVIEN nv
       WHERE ht.MA_DON_VI = i_DVTT
         AND trunc(ht.NGAY_HOAN_TRA) BETWEEN i_tuNgay AND i_denNgay
         AND ht.MA_PHONG_BAN_HOAN_TRA = decode(i_maPhongBan,'-1',ht.MA_PHONG_BAN_HOAN_TRA,i_maPhongBan)
         AND ht.TRANG_THAI > 1
         and ht.BANT = i_bant
         AND ht.NGUOI_HOAN_TRA = nv.ma_nhanvien
				 ORDER BY SOPHIEUTAO_LUUTRU DESC;
  END IF;
  return cur;
end;

