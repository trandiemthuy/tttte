package com.vnpt.duoc.duocModule.dao.hethong;

public class DocSo {
    public static String dochangchuc(double so, boolean daydu) {
        String[] mangso = {"không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín"};
        String chuoi = "";
        int chuc = (int) Math.floor(so / 10);
        int donvi = (int) so % 10;
        if (chuc > 1) {
            chuoi = " " + mangso[chuc] + " mươi";
            if (donvi == 1) {
                chuoi += " mốt";
            }
        } else if (chuc == 1) {
            chuoi = " mười";
            if (donvi == 1) {
                chuoi += " một";
            }
        } else if (daydu && donvi > 0) {
            chuoi = " lẻ";
        }
        if (donvi == 5 && chuc >= 1) {
            chuoi += " lăm";
        } else if (donvi > 1 || (donvi == 1 && chuc == 0)) {
            chuoi += " " + mangso[donvi];
        }
        return chuoi;
    }
    //Đọc block 3 số

    public static String docblock(double so, boolean daydu) {
        String[] mangso = {"không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín"};
        String chuoi = "";
        int tram = (int) Math.floor(so / 100);
        so = so % 100;
        if (daydu || tram > 0) {
            chuoi = " " + mangso[tram] + " trăm";
            chuoi += dochangchuc(so, true);
        } else {
            chuoi = dochangchuc(so, false);
        }
        return chuoi;
    }
    //Đọc số hàng triệu

    public String dochangtrieu(double so, boolean daydu) {
        String[] mangso = {"không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín"};
        String chuoi = "";
        int trieu = (int) Math.floor(so / 1000000);
        so = so % 1000000;
        if (trieu > 0) {
            chuoi = docblock(trieu, daydu) + " triệu";
            daydu = true;
        }
        double nghin = Math.floor(so / 1000);
        so = so % 1000;
        if (nghin > 0) {
            chuoi += docblock(nghin, daydu) + " nghìn";
            daydu = true;
        }
        if (so > 0) {
            chuoi += docblock(so, daydu);
        }
        return chuoi;
    }

    //Đọc số
    public String docso(double so) {
        try {
            String[] mangso = {"không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín"};

            if (so == 0) {
                return mangso[0];
            }
            String chuoi = "", hauto = "";
            do {
                double ty = so % 1000000000;
                so = Math.floor(so / 1000000000);
                if (so > 0) {
                    chuoi = dochangtrieu(ty, true) + hauto + chuoi;
                } else {
                    chuoi = dochangtrieu(ty, false) + hauto + chuoi;
                }
                hauto = " tỷ";
            } while (so > 0);
            try {
                if (chuoi.trim().substring(chuoi.trim().length() - 1, 1).equals(",")) {
                    chuoi = chuoi.trim().substring(0, chuoi.trim().length() - 1);
                }
            } catch (Exception e) {
            }
            chuoi = chuoi.trim().substring(0, 1).toUpperCase() + chuoi.trim().substring(1, chuoi.trim().length());
            return chuoi.trim() + " đồng";
        } catch (Exception e) {
            return "Không đồng";
        }
    }

    public String docso(String so) {
        int indexOfDot = so.indexOf(".");
        if (indexOfDot == -1) return docso(Double.parseDouble(so));
        String nguyen = so.substring(0, indexOfDot);
        String thapPhan = so.substring(indexOfDot + 1);
        if (Double.parseDouble(thapPhan) == 0) return docso(Double.parseDouble(so));
        else {
            String docPhanNguyen = docso(Double.parseDouble(nguyen));
            StringBuilder docPhanThapPhan = new StringBuilder();
            /* Doc so 0 o dau (neu co) */
            for (int i=0; i<thapPhan.length(); i++) {
                if (thapPhan.charAt(i) == '0')
                    docPhanThapPhan.append("không").append(" ");
                else break;
            }
            docPhanThapPhan.append(docso(Double.parseDouble(thapPhan))).append(" ");
            return
                (docPhanNguyen.substring(0, docPhanNguyen.length() - 4)  // bo tu 'Dong'
                    + "phẩy " + docPhanThapPhan.toString().toLowerCase()).trim();
        }
    }
}
