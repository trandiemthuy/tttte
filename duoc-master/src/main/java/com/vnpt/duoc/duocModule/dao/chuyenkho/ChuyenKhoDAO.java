package com.vnpt.duoc.duocModule.dao.chuyenkho;

import com.vnpt.duoc.duocModule.objects.chuyenkho.ChiTietChuyenKhoObj;
import com.vnpt.duoc.duocModule.objects.chuyenkho.ChuyenKhoObj;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ChuyenKhoDAO {
    List getDanhSachPhieuChuyenKho(String tuNgay, String denNgay, String dvtt, long nghiepVu, String maPhongBan);
    List getDanhSachNghiepVuNhanVien(long maNV, String dvtt);
    List getDanhSachNghiepVu();
    List getDanhSachKhoChuyen(long nghiepVu, String dvtt);
    List getDanhSachKhoNhan(long nghiepVu, String dvtt, long maNhanVien);
    String getDonViQuanLyTrucTiep(String dvtt);
    List getDanhSachVatTuTonKho(String dvtt, long maKho, Date ngayChuyen, long nghiepVu);
    List getDanhSachChiTietPhieuChuyenKho(long idPhieuChuyen, String dvtt);

    Map addPhieuChuyenKho(ChuyenKhoObj chuyenKhoObj, String maKhoa, String maPhong);
    int updateNhatPhieuChuyenKhoV2(ChuyenKhoObj chuyenKhoObj, String maKhoa, String maPhong);
    int deletePhieuChuyenKho(long idPhieuChuyen, String dvtt, long nguoiXoa, long idGiaoDich, String maKhoa, String maPhong);
    Map addChiTietPhieuChuyenKho(ChiTietChuyenKhoObj chiTietObj, String maKhoa, String maPhong);
    int deleteChiTietPhieuChuyenKho(long idChuyenKhoVTCT, String dvtt, long nguoiXoa, long idGiaoDich, String maKhoa, String maPhong);

    // Print
    Map getThongTinPhieuChuyenKho(String dvtt, long idPhieuChuyen, long maNghiepVu);
    Double getTongTienPhieuChuyenKho(long idPhieuChuyen);
    List getDanhSachBanInPhieuDuTruKhoaPhong(String dvtt, long idChuyenKhoVatTu);
}
