package com.vnpt.duoc.duocModule.dao.tonghopdutru;

import com.vnpt.duoc.duocModule.objects.tonghopdutru.ThongTinPhieuDuTruObj;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TongHopDuTruDAO {
    // Tổng hợp dự trù
    List getDanhSachPhieuChuaDuTru(String dvtt, String maPhongBan, String tuNgay, String denNgay, int trangThai, int bant );
    List getDanhSachPhieuDuTru(String dvtt, String tuNgay, String denNgay, String maPhongBan, int trangThai, int isBANT );
    List getDanhSachVatTuPhieuDutru(String dvtt, long idPhieuDuTruTong, String maPhongBan, int hinhThucXem, int thoiGian);
    List getDanhSachBenhNhanPhieuDuTru(String dvtt, long idPhieuDuTruTong, String ngayTao, String maPhongBan );

    Map tongHopDuTruTheoPhieu(String dvtt, Date ngayGioHeThong, long maUser, String ghiChu, int bant, String maPhongBan, String idDieuTris, long idGiaoDich, String maPhongBanPhieu);
    Map tongHopDuTruTheoNgay(String dvtt, String tuNgay, String denNgay, Date ngayGioHeThong, long nguoiLap, String ghiChu, int bant, String maPhongBan, long idGiaoDich, String maPhongBanPhieu);
    int deletePhieuDuTru(long idPhieuDuTru, String maPhongBan, String dvtt, long maNV, String idGiaoDich, String maPhong);
    int updateTrangThaiPhieuDuTru( String dvtt, long idPhieu, int trangThai, long maNV, String idGiaoDich, String maPhongBan, String maPhongBenh);

    // Tổng hợp hoàn trả
    List getDanhSachPhieuHoanTra(String dvtt, String tuNgay, String denNgay, String maPhongBan, int trangThai, int isBANT);
    List getDanhSachPhieuChuaHoanTra(String dvtt, String maPhongBan, String tuNgay, String denNgay, int bant);
    List getDanhSachVatTuPhieuHoanTra(long idPhieuHoanTraTong, int hinhThucXem);
    List getDanhSachBenhNhanPhieuHoanTra(long idPhieuHoanTraTong);

    Map tongHopHoanTraTheoPhieu(ThongTinPhieuDuTruObj thongTinPhieuDuTruObj);
    Map tongHopHoanTraTheoNgay(ThongTinPhieuDuTruObj thongTinPhieuDuTruObj);
    int deletePhieuHoanTra(ThongTinPhieuDuTruObj thongTinPhieuDuTruObj);
    int updateTrangThaiPhieuHoanTra(ThongTinPhieuDuTruObj thongTinPhieuDuTruObj, int trangThai);

    Map getThongTinPhieuHoanTra(String dvtt, long idPhieuHoanTra, int isBANT);
}
