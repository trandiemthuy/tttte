package com.vnpt.duoc.duocModule.controller.tonghopdutru;

import com.vnpt.duoc.duocModule.DuocConfigs;
import com.vnpt.duoc.duocModule.DuocTransaction;
import com.vnpt.duoc.duocModule.DuocUtils;
import com.vnpt.duoc.duocModule.dao.hethong.HeThongDAO;
import com.vnpt.duoc.duocModule.dao.tonghopdutru.TongHopDuTruDAO;
import com.vnpt.duoc.duocModule.objects.tonghopdutru.ThongTinPhieuDuTruObj;
import com.vnpt.duoc.jasper.JasperHelper;
import com.zaxxer.hikari.HikariDataSource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.jasperreports.engine.JRParameter;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.InetAddress;
import java.util.*;

@RestController
@RequestMapping("api/tonghopdutru")
@Api(value="Tổng hợp dự trù nội trú Controller")
public class TongHopDuTruController {
    private TongHopDuTruDAO tongHopDuTruDAO;
    private HeThongDAO heThongDAO;
    private HikariDataSource dataSourceDuocV2;

    public TongHopDuTruController(TongHopDuTruDAO tongHopDuTruDAO, HeThongDAO heThongDAO, HikariDataSource dataSourceDuocV2) {
        this.tongHopDuTruDAO = tongHopDuTruDAO;
        this.heThongDAO = heThongDAO;
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    @ApiOperation("Lấy danh sách phiếu điều trị chưa dự trù/hoàn trả")
    @RequestMapping(value="/getDanhSachPhieuChuaDuTru", method = RequestMethod.GET)
    public List getDanhSachPhieuChuaDuTru(
                                          HttpServletResponse response,@RequestParam("dvtt") String dvtt,
                                          @RequestParam("tuNgay") String tuNgay,
                                          @RequestParam("denNgay") String denNgay,
                                          @RequestParam("trangThai") int trangThai,
                                          @RequestParam("bant") int bant,
                                          @RequestParam("maPhongBan") String maPhongBan,
                                          @RequestParam("loaiPhieu") String loaiPhieu, // 0: Dự trù        1: Hoàn trả
                                          HttpServletRequest request,
                                          HttpSession session) {
        try {
            if (loaiPhieu.equals("0")) {
                return tongHopDuTruDAO.getDanhSachPhieuChuaDuTru(dvtt, maPhongBan, tuNgay, denNgay, trangThai, bant);
            } else {
                return tongHopDuTruDAO.getDanhSachPhieuChuaHoanTra(dvtt, maPhongBan, tuNgay, denNgay, bant);
            }
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách phiếu dự trù /hoàn trả")
    @RequestMapping(value="/getDanhSachPhieuDuTru", method = RequestMethod.GET)
    public List getDanhSachPhieuDuTru(@RequestParam("dvtt") String dvtt,
                                      @RequestParam("tuNgay") String tuNgay,
                                      @RequestParam("denNgay") String denNgay,
                                      @RequestParam("trangThai") int trangThai,
                                      @RequestParam("bant") int bant,
                                      @RequestParam("maPhongBan") String maPhongBan,
                                      @RequestParam("loaiPhieu") String loaiPhieu,
                                      HttpServletResponse response,
                                      HttpServletRequest request,
                                      HttpSession session) {
        try {
            if (loaiPhieu.equals("0")) {
                return tongHopDuTruDAO.getDanhSachPhieuDuTru(dvtt, tuNgay, denNgay, maPhongBan, trangThai, bant);
            } else {
                return tongHopDuTruDAO.getDanhSachPhieuHoanTra(dvtt, tuNgay, denNgay, maPhongBan, trangThai, bant);
            }
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách vật tư phiếu dự trù nội trú")
    @RequestMapping(value="/getDanhSachVatTuPhieuDutru", method = RequestMethod.GET)
    public List getDanhSachVatTuPhieuDutru(@RequestParam("dvtt") String dvtt,
                                           @RequestParam("idPhieu") long idPhieu,
                                           @RequestParam("maPhongBan") String maPhongBan,
                                           @RequestParam("hinhThucXem") int hinhThucXem,
                                           @RequestParam("thoiGian") int thoiGian,
                                           @RequestParam("loaiPhieu") String loaiPhieu, // 0: Dự trù        1: Hoàn trả
                                           HttpServletResponse response,
                                      HttpServletRequest request,
                                      HttpSession session) {
        try {
            if (loaiPhieu.equals("0")) {
                return tongHopDuTruDAO.getDanhSachVatTuPhieuDutru(dvtt, idPhieu, maPhongBan, hinhThucXem, thoiGian);
            } else {
                return tongHopDuTruDAO.getDanhSachVatTuPhieuHoanTra(idPhieu, hinhThucXem);
            }

        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách bệnh nhân phiếu dự trù nội trú")
    @RequestMapping(value="/getDanhSachBenhNhanPhieuDuTru", method = RequestMethod.GET)
    public List getDanhSachBenhNhanPhieuDuTru(@RequestParam("dvtt") String dvtt,
                                           @RequestParam("idPhieu") long idPhieu,
                                           @RequestParam("ngayLap") String ngayLap,
                                           @RequestParam("maPhongBan") String maPhongBan,
                                           @RequestParam("loaiPhieu") String loaiPhieu, // 0: Dự trù        1: Hoàn trả
                                           HttpServletResponse response,
                                           HttpServletRequest request,
                                           HttpSession session) {
        try {
            if (loaiPhieu.equals("0")) {
                return tongHopDuTruDAO.getDanhSachBenhNhanPhieuDuTru(dvtt, idPhieu, ngayLap, maPhongBan);
            } else {
                return tongHopDuTruDAO.getDanhSachBenhNhanPhieuHoanTra(idPhieu);
            }
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    //
    @ApiOperation("Tổng hợp dự trù theo phiếu")
    @ResponseBody
    @RequestMapping(value = "/tongHopDuTruTheoPhieu", method = RequestMethod.POST)
    public Map tongHopDuTruTheoPhieu(@RequestBody ThongTinPhieuDuTruObj thongTinPhieu,
                                     @RequestParam("loaiPhieu") String loaiPhieu, // 0: Dự trù        1: Hoàn trả
                                     HttpSession session,
                                     HttpServletRequest request,
                                     HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();

            if (loaiPhieu.equals("0")) {
                String idGiaoDich = DuocTransaction.insert(0, 34, "Tổng hợp dự trù theo phiếu",
                    thongTinPhieu.getMaNguoiLap(), thongTinPhieu.getDvtt(), 0,
                    clientIP, hostIP, "", "tongHopDuTruTheoPhieu", dataSourceDuocV2);
                return tongHopDuTruDAO.tongHopDuTruTheoPhieu(thongTinPhieu.getDvtt(),
                    new Date(),
                    thongTinPhieu.getMaNguoiLap(),
                    thongTinPhieu.getGhiChu(),
                    thongTinPhieu.getIsBANT(),
                    thongTinPhieu.getMaPhongBan(),
                    thongTinPhieu.getIdPhieu(),
                    Long.parseLong(idGiaoDich),
                    thongTinPhieu.getMaPhongBan());
            } else {
                String idGiaoDich = DuocTransaction.insert(0, 47, "Tổng hợp hoàn trả theo phiếu",
                    thongTinPhieu.getMaNguoiLap(), thongTinPhieu.getDvtt(), 0,
                    clientIP, hostIP, "", "tongHopDuTruTheoPhieu", dataSourceDuocV2);
                ThongTinPhieuDuTruObj temp = thongTinPhieu;
                temp.setIdGiaoDich(Long.parseLong(idGiaoDich));
                return tongHopDuTruDAO.tongHopHoanTraTheoPhieu(temp);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap();
        }
    }

    @ApiOperation("Tổng hợp dự trù theo ngày")
    @ResponseBody
    @RequestMapping(value = "/tongHopDuTruTheoNgay", method = RequestMethod.POST)
    public Map tongHopDuTruTheoNgay(@RequestBody ThongTinPhieuDuTruObj thongTinPhieu,
                                    @RequestParam("loaiPhieu") String loaiPhieu, // 0: Dự trù        1: Hoàn trả
                                    HttpSession session,
                                    HttpServletRequest request,
                                    HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            if (loaiPhieu.equals("0")) {
                String idGiaoDich = DuocTransaction.insert(0, 39, "Tổng hợp dự trù theo ngày",
                    thongTinPhieu.getMaNguoiLap(), thongTinPhieu.getDvtt(), 0,
                    clientIP, hostIP, "", "tongHopDuTruTheoNgay", dataSourceDuocV2);
                return tongHopDuTruDAO.tongHopDuTruTheoNgay(thongTinPhieu.getDvtt(),
                    thongTinPhieu.getTuNgay(),
                    thongTinPhieu.getDenNgay(),
                    new Date(),
                    thongTinPhieu.getMaNguoiLap(),
                    thongTinPhieu.getGhiChu(),
                    thongTinPhieu.getIsBANT(),
                    thongTinPhieu.getMaPhongBan(),
                    Long.parseLong(idGiaoDich),
                    thongTinPhieu.getMaPhongBan());
            } else {
                String idGiaoDich = DuocTransaction.insert(0, 48, "Tổng hợp hoàn trả theo ngày",
                    thongTinPhieu.getMaNguoiLap(), thongTinPhieu.getDvtt(), 0,
                    clientIP, hostIP, "", "tongHopDuTruTheoNgay", dataSourceDuocV2);
                ThongTinPhieuDuTruObj temp = thongTinPhieu;
                temp.setIdGiaoDich(Long.parseLong(idGiaoDich));
                return tongHopDuTruDAO.tongHopHoanTraTheoNgay(temp);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap();
        }
    }

    @ApiOperation("Xóa phiếu dự trù")
    @ResponseBody
    @RequestMapping(value = "/deletePhieuDuTru", method = RequestMethod.POST)
    public int deletePhieuDuTru(@RequestBody ThongTinPhieuDuTruObj thongTinPhieu,
                                @RequestParam("loaiPhieu") String loaiPhieu, // 0: Dự trù        1: Hoàn trả
                                HttpSession session,
                                HttpServletRequest request,
                                HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            if (loaiPhieu.equals("0")) {
                String idGiaoDich = DuocTransaction.insert(0, 38, "Xóa phiếu dự trù",
                    thongTinPhieu.getMaNguoiLap(), thongTinPhieu.getDvtt(), 0,
                    clientIP, hostIP, "", "deletePhieuDuTru", dataSourceDuocV2);
                return tongHopDuTruDAO.deletePhieuDuTru(Long.parseLong(thongTinPhieu.getIdPhieu()), thongTinPhieu.getMaPhongBan(), thongTinPhieu.getDvtt(), thongTinPhieu.getMaNguoiLap(), idGiaoDich, thongTinPhieu.getMaPhongBenh());
            } else {
                String idGiaoDich = DuocTransaction.insert(0, 36, "Xóa phiếu hoàn trả",
                    thongTinPhieu.getMaNguoiLap(), thongTinPhieu.getDvtt(), 0,
                    clientIP, hostIP, "", "deletePhieuDuTru", dataSourceDuocV2);
                ThongTinPhieuDuTruObj temp = thongTinPhieu;
                temp.setIdGiaoDich(Long.parseLong(idGiaoDich));
                return tongHopDuTruDAO.deletePhieuHoanTra(temp);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Gửi phiếu dự trù/hoàn trả lên khoa dược")
    @ResponseBody
    @RequestMapping(value = "/guiPhieuDuTruLenKhoaDuoc", method = RequestMethod.POST)
    public int guiPhieuDuTruLenKhoaDuoc(@RequestBody ThongTinPhieuDuTruObj thongTinPhieu,
                                        @RequestParam("trangThaiPhieu") int trangThaiPhieu,
                                        @RequestParam("loaiPhieu") String loaiPhieu,
                                        HttpSession session,
                                        HttpServletRequest request,
                                        HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            if (loaiPhieu.equals("0")) {
                String idGiaoDich = DuocTransaction.insert(0, 32, "Gửi phiếu dự trù lên khoa dược",
                    thongTinPhieu.getMaNguoiLap(), thongTinPhieu.getDvtt(), 0,
                    clientIP, hostIP, "", "updateTrangThaiPhieuDuTru", dataSourceDuocV2);
                return tongHopDuTruDAO.updateTrangThaiPhieuDuTru(thongTinPhieu.getDvtt(), Long.parseLong(thongTinPhieu.getIdPhieu()), trangThaiPhieu, thongTinPhieu.getMaNguoiLap(), idGiaoDich, thongTinPhieu.getMaPhongBan(), thongTinPhieu.getMaPhongBenh());
            } else {
                String idGiaoDich = DuocTransaction.insert(0, 37, "Gửi phiếu hoàn trả lên khoa dược",
                    thongTinPhieu.getMaNguoiLap(), thongTinPhieu.getDvtt(), 0,
                    clientIP, hostIP, "", "updateTrangThaiPhieuDuTru", dataSourceDuocV2);
                ThongTinPhieuDuTruObj temp = thongTinPhieu;
                temp.setIdGiaoDich(Long.parseLong(idGiaoDich));
                return tongHopDuTruDAO.updateTrangThaiPhieuHoanTra(temp, trangThaiPhieu);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("In phiếu hoàn trả thuốc")
    @RequestMapping(value = "/printPhieuHoanTraThuoc", method = RequestMethod.GET)
    public @ResponseBody
    void printPhieuHoanTraThuoc(HttpSession session,
                                HttpServletResponse response,
                                HttpServletRequest request,
                                @RequestParam(value = "dvtt") String dvtt,
                                @RequestParam(value = "isBANT") int isBANT,
                                @RequestParam(value = "idPhieu") long idPhieu,
                                @RequestParam(value = "tenTinh") String tenTinh,
                                @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                @RequestParam(value = "loaiFile") String loaiFile
    ) {
        try {
            Map parameters = new HashMap();
            Map<String, Object> thongTinPhieu = tongHopDuTruDAO.getThongTinPhieuHoanTra(dvtt, idPhieu, isBANT);
            parameters.put("dvtt", dvtt);
            parameters.put("ngayHD", thongTinPhieu.get("NGAY_HOANTRA").toString());
            parameters.put("idPhieuHoanTraTong", idPhieu);
            parameters.put("soPhieuLuuTru", thongTinPhieu.get("SOPHIEUTAO_LUUTRU").toString());
            parameters.put("tenDVQL", tenTinh);
            parameters.put("tenDV", tenBenhVien);
            parameters.put("tenPhongBan", thongTinPhieu.get("TEN_PHONGBAN").toString());
            parameters.put("tenBaoCao", "PHIẾU TRẢ LẠI THUỐC");
            parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
            File reportFile;
            String reportName = "rp_dutruhoantra_phieutralaithuoc.jasper";
            reportFile = new ClassPathResource(DuocConfigs.reportURL + "tonghopdutru/" + reportName).getFile();
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (FileNotFoundException ex) {
            DuocUtils.responseFileNotFoundToClient(ex, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
