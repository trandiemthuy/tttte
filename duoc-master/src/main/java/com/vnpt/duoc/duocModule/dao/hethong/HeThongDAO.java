package com.vnpt.duoc.duocModule.dao.hethong;

import java.util.List;
import java.util.Map;

public interface HeThongDAO {
     String getMoTaThamSoByMaThamSo(String dvtt, long maThamSo);
     String getTenPhongBan(String maPhongBan);
     Map getDuongDanBaoCao(String dvtt, String maloaireport);
     List getDanhSachKhoaNoiTru(String dvtt);
     List getDanhSachKhoNoiTru(String dvtt, String maPhongBan, String nghiepVu);
     List getDanhSachBacSi(String dvtt);
}
