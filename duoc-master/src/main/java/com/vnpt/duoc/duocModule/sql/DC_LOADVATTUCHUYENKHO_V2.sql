create or replace FUNCTION "HIS_DUOCV2". "DC_LOADVATTUCHUYENKHO_V2" (i_dvtt in varchar2,
                                      i_makhovattu in number,
                                      i_ngaychuyen in date,
                                      i_nghiepVu in number)
/**----------v1-------------**/
  RETURN SYS_REFCURSOR
 IS
  cur          SYS_REFCURSOR;
  f_COUNT      NUMBER(10);
  f_dvql varchar2(11);
BEGIN
  if i_nghiepVu=37 then
    select ma_donviquanly into f_dvql from his_fw.dm_donvi where ma_donvi=i_dvtt;
      OPEN cur FOR
        SELECT
              tc.MA_VAT_TU AS MaVatTu,
--              tc.TEN_VAT_TU AS TEN_VAT_TU,
              case when nvl(vt.ten_hien_thi,' ')=' ' then tc.TEN_VAT_TU else vt.ten_hien_thi end  as TEN_VAT_TU,
              SUM(tc.SL_TON) AS SoLuong,
              tc.DON_GIA AS DON_GIA,
              tc.Hoat_Chat,
              nvl(tc.Ham_Luong,' ') as ham_luong,
              tc.DVT,
              nvl(vt.MaBaoCao,' ') as MaBaoCao,
              nvl(vt.GHICHUVATTU, ' ') GHICHU,
              nvl(nhap.NGUONDUOC, ' ') nguon_Duoc,
              --vinh long them so lo va han dung
              tc.solosanxuat
              ,to_char(tc.ngay_het_han,'dd/mm/yyyy') as ngayhethan
               --end vinh long
          from
              DC_TB_XNT_TONCUOI tc
              join his_manager.dc_tb_vattu vt  on tc.MA_DON_VI = vt.DVTT and tc.MA_VAT_TU = vt.MAVATTU
              join HIS_DUOCV2.DC_TB_NHAPKHOTUNHACC_chitiet ct on  tc.sonhapkhochitiet = ct.sonhapkhochitiet and ct.mavattu = vt.mavattu-- -- nguonduoc co o bang nay nhung khong co mavattu
              join HIS_DUOCV2.DC_TB_NHAPKHOTUNHACC nhap on  ct.ID_NHAPKHOTUNHACC = nhap.ID_NHAPKHOTUNHACC
         where
            tc.MA_DON_VI = f_dvql
                   and tc.MA_KHO_VAT_TU = i_makhovattu
                   and tc.hoat_dong = 1
                   and tc.SL_TON > 0
                   and tc.NGAY_NHAP_KHO <= i_ngaychuyen
         GROUP BY tc.MA_VAT_TU,
                          tc.TEN_VAT_TU,
                          vt.ten_hien_thi,
                          tc.DON_GIA,
                          tc.Hoat_Chat,
                          tc.Ham_Luong,
                          tc.DVT,
                            vt.MaBaoCao,
                          vt.GHICHUVATTU,
                          nhap.NGUONDUOC,
                          --vinh long them so lo va han dung
                          tc.solosanxuat,
                          tc.NGAY_HET_HAN;
    else
      OPEN cur FOR
        SELECT
              tc.MA_VAT_TU AS MaVatTu,
--              tc.TEN_VAT_TU AS TEN_VAT_TU,
              case when nvl(vt.ten_hien_thi,' ')=' ' then tc.TEN_VAT_TU else vt.ten_hien_thi end  as TEN_VAT_TU,
              SUM(tc.SL_TON) AS SoLuong,
              tc.DON_GIA AS DON_GIA,
              tc.Hoat_Chat,
              nvl(tc.Ham_Luong,' ') as ham_luong,
              tc.DVT,
              nvl(vt.MaBaoCao,' ') as MaBaoCao,
              nvl(vt.GHICHUVATTU, ' ') GHICHU,
              nvl(nhap.NGUONDUOC, ' ') nguon_Duoc,
              --vinh long them so lo va han dung
              tc.solosanxuat
              ,to_char(tc.ngay_het_han,'dd/mm/yyyy') as ngayhethan
               --end vinh long
          from
              DC_TB_XNT_TONCUOI tc
              join his_manager.dc_tb_vattu vt  on tc.MA_DON_VI = vt.DVTT and tc.MA_VAT_TU = vt.MAVATTU
              join HIS_DUOCV2.DC_TB_NHAPKHOTUNHACC_chitiet ct on  tc.sonhapkhochitiet = ct.sonhapkhochitiet and ct.mavattu = vt.mavattu-- -- nguonduoc co o bang nay nhung khong co mavattu
              join HIS_DUOCV2.DC_TB_NHAPKHOTUNHACC nhap on  ct.ID_NHAPKHOTUNHACC = nhap.ID_NHAPKHOTUNHACC
         where
            tc.MA_DON_VI = i_dvtt
                   and tc.MA_KHO_VAT_TU = i_makhovattu
                   and tc.hoat_dong = 1
                   and tc.SL_TON > 0
                   and tc.NGAY_NHAP_KHO <= i_ngaychuyen
         GROUP BY tc.MA_VAT_TU,
                          tc.TEN_VAT_TU,
                          vt.ten_hien_thi,
                          tc.DON_GIA,
                          tc.Hoat_Chat,
                          tc.Ham_Luong,
                          tc.DVT,
                            vt.MaBaoCao,
                          vt.GHICHUVATTU,
                          nhap.NGUONDUOC,
                          --vinh long them so lo va han dung
                          tc.solosanxuat,
                          tc.NGAY_HET_HAN;
    end if;
  return cur;
END;
