package com.vnpt.duoc.duocModule.dao.khamchuabenh;

import com.vnpt.duoc.duocModule.objects.khamchuabenh.ToaThuocNgoaiTruObj;
import com.vnpt.duoc.duocModule.objects.khamchuabenh.ToaThuocNoiTruObj;
import com.vnpt.duoc.jdbc.JdbcTemplate;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class KcbDAOImp implements KcbDAO {
    private final HikariDataSource dataSourceDuocV2;

    public KcbDAOImp(HikariDataSource dataSourceDuocV2) {
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    @Override
    public int getSoLuongTonKho(String dvtt, int maKho, String searchTerm, int loaiTimKiem) {
        String sql = "call HIS_DUOCV2.DC_SP_DEMTONKHOHIENTAI_KB_V2(?,?,?,?)#l,s,l,s,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{
            dvtt,
            maKho,
            searchTerm,
            loaiTimKiem
        }, Integer.class);
    }

    @Override
    public List getDanhSachDuocTonKho(String dvtt, int loaiTimKiem, int loaiKhamBenh, String maLoaiVatTu, int maNhomVatTu,
                                      int maKho, String searchTerm, int start, int limit, int totalPage, String hienThiDonGiaKhiGo, String cauHinhPBGoThuoc, String maPhongBan) {
        String sql = "call HIS_DUOCV2.DC_SP_LOADTONKHOHIENTAI_KB(?,?,?,?,?,?,?,?,?,?,?,?,?)#c,s,l,l,s,l,l,s,l,l,l,l,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{
            dvtt,
            loaiTimKiem,
            loaiKhamBenh,
            maLoaiVatTu,
            maNhomVatTu,
            maKho,
            searchTerm,
            start,
            limit,
            totalPage,
            hienThiDonGiaKhiGo,
            cauHinhPBGoThuoc,
            maPhongBan
        });
    }

    @Override
    public int addTachToaDichVuNoiTru(ToaThuocNoiTruObj toaThuocNoiTruObj) {
        String sql = "call HIS_MANAGER.INSERT_TACHTOADICHVU(?,?,?,?,?,?,?,?,?,?,?,?,?,?)#l,s,s,s,s,s,s,s,s,s,s,s,s,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{
            toaThuocNoiTruObj.getDvt(),
            toaThuocNoiTruObj.getMaToaThuoc(),
            toaThuocNoiTruObj.getMaKhoVatTu(),
            toaThuocNoiTruObj.getMaVatTu(),
            toaThuocNoiTruObj.getTenVatTu(),
            toaThuocNoiTruObj.getNghiepVu(),
            toaThuocNoiTruObj.getSoLuong(),
            toaThuocNoiTruObj.getSoLuongThucLinh(),
            toaThuocNoiTruObj.getDonGiaBV(),
            toaThuocNoiTruObj.getDonGiaBH(),
            toaThuocNoiTruObj.getThanhTien(),
            toaThuocNoiTruObj.getNgayRaToa(),
            toaThuocNoiTruObj.getTuTuThuoc(),
            toaThuocNoiTruObj.getTachToaDichVu()
        }, Integer.class);
    }

    @Override
    public int addToaThuocNoiTru(ToaThuocNoiTruObj toaThuocNoiTruObj) {
        String sql = "call HIS_DUOCV2.NOI_CT_TOATHUOC_INS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" +
            "#l,s,s,s,s,l,l,s,s,s,s,f,f,f,f,f,l,f,f,f,f,t,s,l,s,s,s,s,l,l,l,l,s,t,l,s,l,l,t,l,s,s,s,s,l,l,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{
            toaThuocNoiTruObj.getDvtt(),
            toaThuocNoiTruObj.getSoPhieu(),
            toaThuocNoiTruObj.getMaCHDA(),
            toaThuocNoiTruObj.getMaToaThuoc(),
            toaThuocNoiTruObj.getMaKhoVatTu(),
            toaThuocNoiTruObj.getMaVatTu(),
            toaThuocNoiTruObj.getTenVatTu(),
            toaThuocNoiTruObj.getHoatChat(),
            toaThuocNoiTruObj.getDvt(),
            toaThuocNoiTruObj.getNghiepVu(),
            toaThuocNoiTruObj.getSoLuong(),
            toaThuocNoiTruObj.getSoLuongThucLinh(),
            toaThuocNoiTruObj.getDonGiaBV(),
            toaThuocNoiTruObj.getDonGiaBH(),
            toaThuocNoiTruObj.getThanhTien(),
            toaThuocNoiTruObj.getSoNgay(),
            toaThuocNoiTruObj.getSang(),
            toaThuocNoiTruObj.getTrua(),
            toaThuocNoiTruObj.getChieu(),
            toaThuocNoiTruObj.getToi(),
            toaThuocNoiTruObj.getNgayRaToa(), // datetime
            toaThuocNoiTruObj.getGhiChu(),
            toaThuocNoiTruObj.getMaBacSi(),
            toaThuocNoiTruObj.getCachSuDung(),
            toaThuocNoiTruObj.getSttDieuTri(),
            toaThuocNoiTruObj.getSttBenhAn(),
            toaThuocNoiTruObj.getSttDotDieuTri(),
            toaThuocNoiTruObj.getTuTuThuoc(),
            toaThuocNoiTruObj.getCoBHYT(),
            toaThuocNoiTruObj.getNamHienTai(),
            toaThuocNoiTruObj.getMaBenhNhan(),
            toaThuocNoiTruObj.getSoPhieuThanhToan(),
            toaThuocNoiTruObj.getNgayHienTai(),
            toaThuocNoiTruObj.getDaThanhToan(),
            toaThuocNoiTruObj.getMaPhongBan(),
            toaThuocNoiTruObj.getSoVaoVien(),
            toaThuocNoiTruObj.getSoVaoVienDT(),
            toaThuocNoiTruObj.getNgayHienTai(),
            toaThuocNoiTruObj.getMaGoiDichVu(),
            toaThuocNoiTruObj.getSoPhieuTTPT(),
            toaThuocNoiTruObj.getMaTTPT(),
            toaThuocNoiTruObj.getSoPhieuXN(),
            toaThuocNoiTruObj.getSoNgaySuDung(),
            toaThuocNoiTruObj.getMaUser(),
            toaThuocNoiTruObj.getIdGiaoDich(),
            toaThuocNoiTruObj.getMaPhongBan()
        }, Integer.class);
    }

    @Override
    public List getDanhSachChiTietToaThuocNoiTru(String dvtt, String maToaThuoc, String nghiepVu, String sttBenhAn, String sttDotDieuTri, String soVaoVien, String soVaoVienDT, String soPhieu, String ma, String theoDV) {
        String sql = "call HIS_MANAGER.NOITRU_LOAD_CHITIET_TT_SVV(?,?,?,?,?,?,?,?,?,?)#c,s,s,s,s,s,l,l,s,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt,
            maToaThuoc,
            nghiepVu,
            sttBenhAn,
            sttDotDieuTri,
            soVaoVien,
            soVaoVienDT,
            soPhieu,
            ma,
            theoDV});
    }

    /* Ngoại trú */

    @Override
    public String getSoPhieuThanhToan(String maKhamBenh, String dvtt, String dieuTriChuyenKhoa, int soVaoVien) {
        String sql = "call HIS_MANAGER.KB_NT_GET_SOPHIEU_TT_GT(?,?,?,?)#s,s,s,l,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{maKhamBenh, dvtt, dieuTriChuyenKhoa, soVaoVien}, String.class);
    }

    @Override
    public String getDaThanhToanNgoaiTru(String dvtt, Date ngayRaToa, String maBenhNhan, String maKhamBenh, int soVaoVien) {
        String sql = "call HIS_MANAGER.KB_NGT_CTTT_KT_PTT_DATT_SVV_F(?,?,?,?,?)#l,s,t,s,s,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{dvtt, ngayRaToa, maBenhNhan, maKhamBenh, soVaoVien}, String.class);
    }

    @Override
    public String getKiemTraXuatThuoc(String dvtt, String maToaThuoc, String nghiepVu, Date ngayRaToa, String maBenhNhan, int soVaoVien) {
        String sql = "call HIS_MANAGER.KB_NGT_CTTT_KT_XUATTHUOC_SVV_F(?,?,?,?,?,?)#l,s,s,s,t,s,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{dvtt, maToaThuoc, nghiepVu, ngayRaToa, maBenhNhan, soVaoVien}, String.class);
    }

    @Override
    public String getKiemTraTrungThuocNgoaiTru(String dvtt, String maToaThuoc, String nghiepVu, Date ngayRaToa, String maBenhNhan, int maVatTu, int soVaoVien) {
        String sql = "call HIS_MANAGER.KB_NGT_CTTT_KT_TRUNG_SVV_F(?,?,?,?,?,?,?)#l,s,s,s,t,s,s,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{dvtt, maToaThuoc, nghiepVu, ngayRaToa, maBenhNhan, maVatTu, soVaoVien}, String.class);
    }

    @Override
    public int addTachToaDichVuNgoaiTru(ToaThuocNgoaiTruObj toaThuocNgoaiTruObj) {
        String sql = "call HIS_MANAGER.INSERT_TACHTOADICHVU(?,?,?,?,?,?,?,?,?,?,?,?,?,?)#l,s,s,s,s,s,s,s,s,s,s,s,s,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{
            toaThuocNgoaiTruObj.getDvt(),
            toaThuocNgoaiTruObj.getMaToaThuoc(),
            toaThuocNgoaiTruObj.getMaKhoVatTu(),
            toaThuocNgoaiTruObj.getMaVatTu(),
            toaThuocNgoaiTruObj.getTenVatTu(),
            toaThuocNgoaiTruObj.getNghiepVu(),
            toaThuocNgoaiTruObj.getSoLuong(),
            toaThuocNgoaiTruObj.getSoLuongThucLinh(),
            toaThuocNgoaiTruObj.getDonGiaBV(),
            toaThuocNgoaiTruObj.getDonGiaBH(),
            toaThuocNgoaiTruObj.getThanhTien(),
            toaThuocNgoaiTruObj.getNgayRaToa(),
            0,
            toaThuocNgoaiTruObj.getTachToaDichVu()
        }, Integer.class);
    }

    @Override
    public int addToaThuocNgoaiTru(ToaThuocNgoaiTruObj toaThuocNgoaiTruObj) {
        String sql = "call HIS_DUOCV2.KB_NT_CT_TTHUOC_INSERT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" +
            "#l,s,s,s,s,l,l,s,s,s,d,d,d,d,d,l,d,d,d,d,t,s,l,s,l,l,l,s,s,s,t,l,l,l,l,l,l,s,s,s,s,s,l,l,s,s,d";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForObject(sql, new Object[]{
            toaThuocNgoaiTruObj.getDvtt(),
            toaThuocNgoaiTruObj.getSoPhieu(),
            toaThuocNgoaiTruObj.getMaCDHA(),
            toaThuocNgoaiTruObj.getMaToaThuoc(),
            toaThuocNgoaiTruObj.getMaKhoVatTu(),
            toaThuocNgoaiTruObj.getMaVatTu(),
            toaThuocNgoaiTruObj.getTenVatTu(),
            toaThuocNgoaiTruObj.getTenGoc(),
            toaThuocNgoaiTruObj.getNghiepVu(),
            toaThuocNgoaiTruObj.getSoLuong(),
            toaThuocNgoaiTruObj.getSoLuongThucLinh(),
            toaThuocNgoaiTruObj.getDonGiaBV(),
            toaThuocNgoaiTruObj.getDonGiaBH(),
            toaThuocNgoaiTruObj.getThanhTien(),
            toaThuocNgoaiTruObj.getSoNgay(),
            toaThuocNgoaiTruObj.getSang(),
            toaThuocNgoaiTruObj.getTrua(),
            toaThuocNgoaiTruObj.getChieu(),
            toaThuocNgoaiTruObj.getToi(),
            toaThuocNgoaiTruObj.getNgayRaToa(),
            toaThuocNgoaiTruObj.getGhiChu(),
            toaThuocNgoaiTruObj.getMaBacSi(),
            toaThuocNgoaiTruObj.getCachSuDung(),
            toaThuocNgoaiTruObj.getCoBHYT(),
            toaThuocNgoaiTruObj.getNamHienTai(),
            toaThuocNgoaiTruObj.getMaBenhNhan(),
            toaThuocNgoaiTruObj.getSoPhieuThanhToan(),
            toaThuocNgoaiTruObj.getIdTiepNhan(),
            toaThuocNgoaiTruObj.getMaKhamBenh(),
            toaThuocNgoaiTruObj.getNgayHienTai(),
            toaThuocNgoaiTruObj.getDaThanhToan(),
            toaThuocNgoaiTruObj.getSoVaoVien(),
            toaThuocNgoaiTruObj.getMaKhoa(),
            toaThuocNgoaiTruObj.getCapCuu(),
            toaThuocNgoaiTruObj.getMaGoiDichVu(),
            toaThuocNgoaiTruObj.getMaPhongRaThuoc(),
            toaThuocNgoaiTruObj.getSoPhieuTTPT(),
            toaThuocNgoaiTruObj.getMaTTPT(),
            toaThuocNgoaiTruObj.getSoDangKyMN(),
            toaThuocNgoaiTruObj.getDonGia82112(),
            toaThuocNgoaiTruObj.getSoPhieuXN(),
            toaThuocNgoaiTruObj.getMaUser(),
            toaThuocNgoaiTruObj.getIdGiaoDich(),
            toaThuocNgoaiTruObj.getMaKhoa(),
            toaThuocNgoaiTruObj.getMaPhong(),
            toaThuocNgoaiTruObj.getDonGiaKhongLamTron()
        }, Integer.class);
    }

    @Override
    public List getDanhSachChiTietToaThuocNgoaiTru(String maToaThuoc, String dvtt, String nghiepVu, String soVaoVien, String ma, String soPhieu, String theoDV, String soPhieuTTPT, String maTTPT) {
        String sql = "call HIS_MANAGER.KB_NGT_TOA_SELECT_MTT_SVV(?,?,?,?,?,?,?,?,?)#c,s,s,s,l,s,s,s,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{maToaThuoc,
            dvtt,
            nghiepVu,
            soVaoVien,
            soPhieu,
            ma,
            theoDV,
            soPhieuTTPT,
            maTTPT});
    }
}
