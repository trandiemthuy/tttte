package com.vnpt.duoc.duocModule.controller.duyetdutruhoantra;

import com.vnpt.duoc.duocModule.DuocConfigs;
import com.vnpt.duoc.duocModule.DuocTransaction;
import com.vnpt.duoc.duocModule.DuocUtils;
import com.vnpt.duoc.duocModule.dao.duyetdutruhoantra.DuyetDuTruHoanTraDAO;
import com.vnpt.duoc.duocModule.dao.hethong.DocSo;
import com.vnpt.duoc.duocModule.dao.hethong.HeThongDAO;
import com.vnpt.duoc.jasper.ExportXLSView;
import com.vnpt.duoc.jasper.JasperHelper;
import com.vnpt.duoc.jasper.SoYLenhExportXLSView;
import com.zaxxer.hikari.HikariDataSource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.minidev.json.JSONObject;
import net.sf.jasperreports.engine.JRParameter;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("api/duyetdutruhoantra")
@Api(value="DuyetDuTruHoanTraController")
public class DuyetDuTruHoanTraController {
    private final DuyetDuTruHoanTraDAO duyetDuTruHoanTraDAO;
    private final HeThongDAO heThongDAO;
    private final HikariDataSource dataSourceDuocV2;

    public DuyetDuTruHoanTraController(DuyetDuTruHoanTraDAO duyetDuTruHoanTraDAO, HeThongDAO heThongDAO, HikariDataSource dataSourceDuocV2) {
        this.duyetDuTruHoanTraDAO = duyetDuTruHoanTraDAO;
        this.heThongDAO = heThongDAO;
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    // Dự trù nội trú
    @ApiOperation("Lấy danh sách phiếu dự trù nội trú")
    @RequestMapping(value = "/getDanhSachPhieuDuTruNoiTru", method = RequestMethod.GET)
    public @ResponseBody
    List getDanhSachPhieuDuTruNoiTru(@RequestParam("dvtt") String dvtt,
                                     @RequestParam("tuNgay") String tuNgay,
                                     @RequestParam("denNgay") String denNgay,
                                     @RequestParam(value = "trangThai") int trangThai,
                                     @RequestParam(value = "isBANT") int isBANT,
                                     @RequestParam(value = "maPhongBan") String maPhongBan,
                                     HttpSession session) {
        try {
            return duyetDuTruHoanTraDAO.getDanhSachPhieuDuTruNoiTru(dvtt, tuNgay, denNgay, maPhongBan, trangThai, isBANT);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách chi tiết phiếu dự trù nội trú")
    @RequestMapping(value = "/getDanhSachChiTietPhieuDutru", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public @ResponseBody
    List getDanhSachChiTietPhieuDutru(@RequestParam(value = "dvtt") String dvtt,
                                      @RequestParam(value = "hinhThucXem") int hinhThucXem,
                                      @RequestParam(value = "idPhieuDuTruTong") long idPhieuDuTruTong,
                                      @RequestParam(value = "maPhongBan") String maPhongBan,
                                      @RequestParam(value = "thoiGian", defaultValue = "0") int thoiGian,
                                      HttpSession session) {
        try {
            return duyetDuTruHoanTraDAO.getDanhSachChiTietPhieuDutru(dvtt, idPhieuDuTruTong, maPhongBan, hinhThucXem, thoiGian);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách bản in phiếu dự trù nội trú")
    @RequestMapping(value = "/getDanhSachBanInPhieuDuTru", produces = "application/json; charset=utf-8")
    public @ResponseBody
    List getDanhSachBanInPhieuDuTru(@RequestParam(value = "dvtt") String dvtt,
                                    @RequestParam(value = "idPhieuDuTruTong") long idPhieuDuTruTong,
                                    @RequestParam(value = "tuTuThuoc") int tuTuThuoc,
                                    @RequestParam(value = "tachLoai") int tachLoai) {
        return duyetDuTruHoanTraDAO.getDanhSachBanInPhieuDuTru(dvtt, idPhieuDuTruTong, tuTuThuoc, tachLoai);
    }

    @ApiOperation("In phiếu dự trù nội trú")
    @RequestMapping(value = "/printPhieuDuTruNoiTru", method = RequestMethod.GET)
    public @ResponseBody
    void printPhieuDuTruNoiTru(@RequestParam(value = "dvtt") String dvtt,
                               @RequestParam(value = "idPhieuDuTruTong") String idPhieuDuTruTong,
                               @RequestParam(value = "soPhieuLuuTru") String soPhieuLuuTru,
                               @RequestParam(value = "ngayLap") String ngayLap,
                               @RequestParam(value = "thanhTien") String thanhTien,
                               @RequestParam(value = "maKhoVatTu") String maKhoVatTu,
                               @RequestParam(value = "tenKhoVatTu") String tenKhoVatTu,
                               @RequestParam(value = "tenKhoa") String tenKhoa,
                               @RequestParam(value = "tenPhongBanDuTru") String tenPhongBanDuTru,
                               @RequestParam(value = "maLoaiVatTu") String maLoaiVatTu,
                               @RequestParam(value = "tuTuThuoc") String tuTuThuoc,
                               @RequestParam(value = "theoNgay") String theoNgay,
                               @RequestParam(value = "tachLoai") String tachLoai,
                               @RequestParam(value = "maNguoiIn") String maNguoiIn,
                               @RequestParam(value = "nguoiLap") String nguoiLap,
                               @RequestParam(value = "tenBenhVien") String tenBenhVien,
                               @RequestParam(value = "tenTinh") String tenTinh,
                               @RequestParam(value = "loaiFile") String loaiFile, // pdf   xls    rtf
                               HttpServletRequest request,
                               HttpServletResponse response,
                               HttpSession session) {
        try {
            Map parameters = new HashMap();
            String[] ngayDuTru = ngayLap.replaceAll("-","/").split("/");
            String ngay = ngayDuTru[0];
            String thang = ngayDuTru[1];
            String nam = ngayDuTru[2];
            //VLG thêm tham số chỉnh phiếu lĩnh/hoàn trả dịch truyền/thuốc thường thành phiếu lĩnh/hoàn trả thuốc
            String phieulinhdichtruyen = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 86205);
            //VLG thêm tham số chỉnh phiếu lĩnh/hoàn trả dịch truyền/thuốc thường thành phiếu lĩnh/hoàn trả thuốc
            if (tachLoai.equals("0")) {
                //tachLoai = 0 in tất cả, không tách loại
                parameters.put("tieude", "PHIẾU LĨNH THUỐC");
                parameters.put("loaiduocvattu", "");
                parameters.put("mauin", "");
            }else if (maLoaiVatTu.equals("TH")) {
                parameters.put("tieude", "PHIẾU LĨNH THUỐC");
                parameters.put("loaiduocvattu", "Tên thuốc nồng độ hàm lượng");
                parameters.put("mauin", "MS: 01D/BV-01");
            } else if (maLoaiVatTu.equals("TH_HTT_NGHIEN ")) {
                parameters.put("tieude", "PHIẾU LĨNH THUỐC GÂY NGHIỆN, HƯỚNG THẦN");
                parameters.put("mauin", "MS: 08D");
            } else if (maLoaiVatTu.equals("TH_HTT")) {
                parameters.put("tieude", "PHIẾU LĨNH THUỐC HƯỚNG THẦN, TIỀN CHẤT");
                parameters.put("mauin", "MS: 08D");
            } else if (maLoaiVatTu.equals("VT_TH")) {
                parameters.put("tieude", "PHIẾU LĨNH VẬT TƯ Y TẾ TIÊU HAO");
                parameters.put("loaiduocvattu", "Tên vật tư y tế tiêu hao");
                parameters.put("mauin", "MS: 03D/BV-01");
            } else if (maLoaiVatTu.equals("VT_TT")) {
                parameters.put("tieude", "PHIẾU LĨNH VẬT TƯ THAY THẾ");
                parameters.put("loaiduocvattu", "Tên thuốc nồng độ hàm lượng");
                parameters.put("mauin", "MS: 03D/BV-01");
            } else if (maLoaiVatTu.equals("DICHTRUYEN")) {
                if (phieulinhdichtruyen.equals("1")) {
                    parameters.put("tieude", "PHIẾU LĨNH THUỐC");
                    parameters.put("loaiduocvattu", "Tên thuốc nồng độ hàm lượng");
                    parameters.put("mauin", "MS: 02D/BV-01");
                } else {
                    parameters.put("tieude", "PHIẾU LĨNH DỊCH TRUYỀN");
                    parameters.put("loaiduocvattu", "Tên thuốc nồng độ hàm lượng");
                    parameters.put("mauin", "MS: 02D/BV-01");
                }
            } else if (maLoaiVatTu.equals("TH_NGHIEN")) {
                parameters.put("tieude", "PHIẾU LĨNH THUỐC GÂY NGHIỆN");
                parameters.put("loaiduocvattu", "Tên thuốc nồng độ hàm lượng");
                parameters.put("mauin", "MS: 08D");
            } else if (maLoaiVatTu.equals("VT_YT")) {
                parameters.put("tieude", "PHIẾU LĨNH VẬT TƯ Y TẾ");
                parameters.put("loaiduocvattu", "Tên thuốc nồng độ hàm lượng");
            } else if (maLoaiVatTu.equals("HC")) {
                parameters.put("tieude", "PHIẾU LĨNH HÓA CHẤT");
            } else if (maLoaiVatTu.equals("YDC")) {
                parameters.put("tieude", "PHIẾU LĨNH Y DỤNG CỤ");
            } else if (maLoaiVatTu.equals("TH_CP")) {
                parameters.put("tieude", "PHIẾU LĨNH THUỐC CHẾ PHẨM YHCT");
                parameters.put("mauin", "MS: 01D/BV-01");
            } else if (maLoaiVatTu.equals("HC")) {
                parameters.put("tieude", "PHIẾU LĨNH HÓA CHẤT");
                parameters.put("mauin", "MS: 02D/BV-01");
            } else if (maLoaiVatTu.equals("SP")) {
                parameters.put("tieude", "PHIẾU LĨNH SINH PHẨM");
                parameters.put("mauin", "MS: 02D/BV-01");
            } else {
                parameters.put("tieude", "PHIẾU LĨNH THUỐC");
                parameters.put("loaiduocvattu", "Tên thuốc nồng độ hàm lượng");
                parameters.put("mauin", "MS: 01D/BV-01");
            }
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("tenbenhvien", tenBenhVien.toUpperCase());
            parameters.put("khoa", tenKhoa);
            parameters.put("MaLoaiVatTu", maLoaiVatTu);
            parameters.put("idPhieuDuTruTong", idPhieuDuTruTong);
            parameters.put("soluutru", soPhieuLuuTru);
            parameters.put("khonoitru", tenKhoVatTu);
            parameters.put("MAKHOVATTU", maKhoVatTu);
            parameters.put("khoanhan", tenPhongBanDuTru);
            parameters.put("dvtt", dvtt);
            parameters.put("ngay", ngay);
            parameters.put("thang", thang);
            parameters.put("nam", nam);
            parameters.put("nguoilap", nguoiLap);
            parameters.put("ngayratoa", "Ngày " + ngayDuTru[0] + " tháng " + ngayDuTru[1] + " năm " + ngayDuTru[2]);
            parameters.put("theongay", theoNgay);
            parameters.put("sotienbangchu", new DocSo().docso(Double.parseDouble(thanhTien)));
            parameters.put("nguoiin", maNguoiIn);
            parameters.put("TUTHUOC", tuTuThuoc);
            parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
            // Lấy danh sách tên bệnh nhân và tên phòng của phiếu dự trù, nếu là một người thì hiển thị lên phiếu in.
            List<Map> dsTenBnTenPhong = duyetDuTruHoanTraDAO.getTenBNPhieuDuTru(Long.parseLong(idPhieuDuTruTong), dvtt);
            if (dsTenBnTenPhong != null && dsTenBnTenPhong.size() == 1) {
                Map m = dsTenBnTenPhong.get(0);
                String s = m.get("TEN_BENH_NHAN") + (m.get("TENGIUONGBENH") == null ? "" : " - " + m.get("TENGIUONGBENH").toString());
                parameters.put("tenBnTenPhong", s);
            }
            String chuky = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 31034);
            if (chuky == null || chuky == "" || chuky.equals("0")) {
                parameters.put("chuky", "Trưởng Khoa Dược             Người phát             Người lĩnh            Trưởng khoa điều trị");
            } else {
                String[] arrChuKy = chuky.split(";");
                String chuoi1 = "";
                String chuoi = "";
                for (int i = 0; i < arrChuKy.length - 1; i++) {
                    chuoi = chuoi + arrChuKy[i].trim() + "        ";
                }
                chuoi = chuoi + arrChuKy[arrChuKy.length - 1];
                chuoi1 = chuoi1 + arrChuKy[arrChuKy.length - 1];
                parameters.put("chuky1", chuoi1);
                parameters.put("chuky", chuoi);
            }
            //HPG bổ sung ngày duyệt phiếu
            String tsNgayDuyet = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 31061);
            if (tsNgayDuyet.equals("1")) {
                String ngayduyet = ngayLap;
                String[] arr_nd = ngayduyet.split("/");
                String ngaythangduyet = "Ngày " + arr_nd[0] + " tháng " + arr_nd[1] + " năm " + arr_nd[2];
                parameters.put("ngaythangduyet", ngaythangduyet);
                if (chuky == null || chuky == "" || chuky.equals("0")) {
                    parameters.put("chuky", "Trưởng Khoa Dược                    Người phát                    Người lĩnh                    Trưởng khoa điều trị");
                } else {
                    String[] arrChuKy = chuky.split(";");
                    String chuoi = "";
                    for (int i = 0; i < arrChuKy.length - 1; i++) {
                        String space = "                 ";
                        chuoi = chuoi + arrChuKy[i].trim() + space;
                    }
                    chuoi = chuoi + arrChuKy[arrChuKy.length - 1];
                    parameters.put("chuky", chuoi);
                }
            } else {
                //Ngay lap phieu
                String ngaythangduyet = "Ngày " + ngayDuTru[2] + " tháng " + ngayDuTru[1] + " năm " + ngayDuTru[0];
                parameters.put("ngaythangduyet", ngaythangduyet);
            }
            String tenThuKho = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 82108);
            parameters.put("tenThuKho", tenThuKho);

            File reportFile;
            //begin AGG DUY điều chỉnh hướng lấy report theo bảng và phân theo 3 loại report.
            String gopPhieu = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 31052);
            //begin AGG DUY điều chỉnh hướng lấy report theo bảng và phân theo 3 loại report.
            if (tachLoai.equals("0")){
                //in tất cả loại vật tư trong phiếu dự trù
                String reportName = "rp_phieulinhduoc_noitrutatcaloai.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
            }else if (maLoaiVatTu.equals("TH_HTT")) {
                if (gopPhieu.equals("1")) {
                    parameters.put("tieude", "PHIẾU LĨNH THUỐC THÀNH PHẨM GÂY NGHIỆN,THUỐC THÀNH PHẨM HƯỚNG TÂM THẦN,THUỐC THÀNH PHẨM TIỀN CHẤT");
                }
                String reportName = "rp_phieulinhduoc_dutrunoitru_HTT.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
            } else if (maLoaiVatTu.equals("TH_NGHIEN")) {
                if (gopPhieu.equals("1")) {
                    parameters.put("tieude", "PHIẾU LĨNH THUỐC THÀNH PHẨM GÂY NGHIỆN,THUỐC THÀNH PHẨM HƯỚNG TÂM THẦN,THUỐC THÀNH PHẨM TIỀN CHẤT");
                }
                String reportName = "rp_phieulinhduoc_dutrunoitru_NGHIEN.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
            } else {
                String reportName = "rp_phieulinhduoc_dutrunoitru.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
            }
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ApiOperation("In phiếu xuất kho dự trù nội trú")
    @RequestMapping(value = "/printPhieuXuatKhoDuTruNoiTru", method = RequestMethod.GET)
    public @ResponseBody
    void printPhieuXuatKhoDuTruNoiTru(@RequestParam(value = "dvtt") String dvtt,
                                      @RequestParam(value = "idPhieuDuTruTong") String idPhieuDuTruTong,
                                      @RequestParam(value = "soPhieuLuuTru") String soPhieuLuuTru,
                                      @RequestParam(value = "ngayLap") String ngayLap,
                                      @RequestParam(value = "thanhTien") String thanhTien,
                                      @RequestParam(value = "maKhoVatTu") String maKhoVatTu,
                                      @RequestParam(value = "tenKhoVatTu") String tenKhoVatTu,
                                      @RequestParam(value = "tenKhoa") String tenKhoa,
                                      @RequestParam(value = "tenPhongBanDuTru") String tenPhongBanDuTru,
                                      @RequestParam(value = "maLoaiVatTu") String maLoaiVatTu,
                                      @RequestParam(value = "nguoiLap") String nguoiLap,
                                      @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                      @RequestParam(value = "tenTinh") String tenTinh,
                                      @RequestParam(value = "loaiFile") String loaiFile, // pdf   xls    rtf
                                      HttpServletRequest request,
                                      HttpSession session,
                                      HttpServletResponse response) {
        try {
            Map parameters = new HashMap();
            String[] date = ngayLap.split("-");
            String ngay = date[0];
            String thang = date[1];
            String nam = date[2];
            if (maLoaiVatTu.equals("TH")) {
                parameters.put("tieude", "PHIẾU XUẤT KHO");
                parameters.put("mauin", "Mẫu số: C21 - HD");
            } else if (maLoaiVatTu.equals("TH_HTT")) {
                parameters.put("tieude", "PHIẾU XUẤT THUỐC THÀNH PHẨM HƯỚNG TÂM THẦN");
                parameters.put("mauin", "MS: 08D");
            } else if (maLoaiVatTu.equals("VT_TH")) {
                parameters.put("tieude", "PHIẾU XUẤT VẬT TƯ Y TẾ TIÊU HAO");
                parameters.put("mauin", "MS: 03D/BV-01");
            } else if (maLoaiVatTu.equals("VT_TT")) {
                parameters.put("tieude", "PHIẾU XUẤT VẬT TƯ THAY THẾ");
                parameters.put("mauin", "MS: 03D/BV-01");
            } else if (maLoaiVatTu.equals("DICHTRUYEN")) {
                parameters.put("tieude", "PHIẾU XUẤT DỊCH TRUYỀN");
                parameters.put("mauin", "MS: 02D/BV-01");
            } else if (maLoaiVatTu.equals("TH_NGHIEN")) {
                parameters.put("tieude", "PHIẾU XUẤT THUỐC THÀNH PHẨM GÂY NGHIỆN");
                parameters.put("mauin", "MS: 08D");
            } else if (maLoaiVatTu.equals("VT_YT")) {
                parameters.put("tieude", "PHIẾU XUẤT VẬT TƯ Y TẾ");
            } else if (maLoaiVatTu.equals("HC")) {
                parameters.put("tieude", "PHIẾU XUẤT HÓA CHẤT");
            } else {
                parameters.put("tieude", "PHIẾU XUẤT THUỐC");
                parameters.put("mauin", "MS: 01D/BV-01");
            }
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("tenbenhvien", tenBenhVien.toUpperCase());
            parameters.put("khoa", tenKhoa);
            parameters.put("MaLoaiVatTu", maLoaiVatTu);
            parameters.put("sotienbangchu", new DocSo().docso(Double.parseDouble(thanhTien)));
            parameters.put("soluutru", soPhieuLuuTru);
            parameters.put("idPhieuDuTruTong", idPhieuDuTruTong);
            parameters.put("khonoitru", tenKhoVatTu);
            parameters.put("MAKHOVATTU", maKhoVatTu);
            parameters.put("khoanhan", tenPhongBanDuTru);
            parameters.put("dvtt", dvtt);
            parameters.put("ngay", ngay);
            parameters.put("thang", thang);
            parameters.put("nam", nam);
            parameters.put("nguoilap", nguoiLap);
            parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
            File reportFile;
            if (maLoaiVatTu.equals("TH_HTT") || maLoaiVatTu.equals("TH_NGHIEN")) {
                String reportName = "rp_dutrunoitruxuatkho_HTT_NGHIEN.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
            } else {
                String reportName = "rp_phieuxuatkho_dutrunoitru.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
            }
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ApiOperation("In sổ y lệnh dự trù nội trú")
    @RequestMapping(value = "/printSoYLenhDuTruNoiTru")
    public @ResponseBody
    ModelAndView printSoYLenhDuTruNoiTru(@RequestParam(value = "idPhieuDuTruTong") long idPhieuDuTruTong,
                                         @RequestParam(value = "soPhieuTaoLuuTru") String soPhieuTaoLuuTru,
                                         @RequestParam(value = "dvtt") String dvtt,
                                         @RequestParam(value = "tuTuThuoc") int tuTuThuoc,
                                         @RequestParam(value = "mauBaoCao") String mauBaoCao,
                                         @RequestParam(value = "maKhoa") int maKhoa,
                                         @RequestParam(value = "tenKhoa") String tenKhoa,
                                         @RequestParam(value = "tenPhongBanDuTru") String tenPhongBanDuTru,
                                         @RequestParam(value = "tuNgay") String tuNgay,
                                         @RequestParam(value = "denNgay") String denNgay,
                                         @RequestParam(value = "maPhong", required = false, defaultValue = "-1") String maPhong,
                                         @RequestParam(value = "tenTinh") String tenTinh,
                                         @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                         HttpSession session) {
        String soPhieu[] = soPhieuTaoLuuTru.split("-");
        List<Map<String, Object>> list;
        if (idPhieuDuTruTong == 0) {
            list = duyetDuTruHoanTraDAO.getDanhSachSoYLenhTatCa(maKhoa, dvtt, tuTuThuoc, tuNgay, denNgay);
        } else {
            list = duyetDuTruHoanTraDAO.getDanhSachSoYLenhPhieuDuTru(idPhieuDuTruTong, soPhieu[0], dvtt, tuTuThuoc, maPhong, tuNgay, denNgay);
        }
        String ngay = tuNgay + "-" + denNgay;
        SoYLenhExportXLSView.tenPhong = tenPhongBanDuTru;
        SoYLenhExportXLSView.tenKhoa = tenKhoa;
        SoYLenhExportXLSView.tenBenhVien = tenBenhVien;
        SoYLenhExportXLSView.tenTinh = "S\u1ede Y T\u1ebe " + tenTinh.toUpperCase();
        SoYLenhExportXLSView.sophieu = soPhieu[0];
        SoYLenhExportXLSView.ngay = ngay;
        SoYLenhExportXLSView.dvtt = dvtt;
        SoYLenhExportXLSView.tensheet = "soylenh_sheet";
        SoYLenhExportXLSView.filename = "soylenh.xls";
        SoYLenhExportXLSView.maubc = mauBaoCao;
        SoYLenhExportXLSView.tsdv = "0";
        SoYLenhExportXLSView.tsdv_sign = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 40071);
        String thamsoKhoangCachChuKy = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 820008);
        if (thamsoKhoangCachChuKy == null) {
            SoYLenhExportXLSView.tsdvKhoanCachChuKy = 0;
        } else {
            SoYLenhExportXLSView.tsdvKhoanCachChuKy = Integer.parseInt(heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 820008));
        }
        return new ModelAndView(new SoYLenhExportXLSView(), "report_view", list);
    }

    @ApiOperation("In danh sách toa thuốc")
    @RequestMapping(value = "/printDanhSachToaThuoc", method = RequestMethod.GET)
    public @ResponseBody
    void printDanhSachToaThuoc(@RequestParam(value = "idPhieuDuTruTong") String idPhieuDuTruTong,
                               @RequestParam(value = "soPhieuTaoLuuTru") String soPhieuTaoLuuTru,
                               @RequestParam(value = "dvtt") String dvtt,
                               @RequestParam(value = "maKhoa") String maKhoa,
                               @RequestParam(value = "tuNgay") String tuNgay,
                               @RequestParam(value = "denNgay") String denNgay,
                               @RequestParam(value = "tenBenhVien") String tenBenhVien,
                               @RequestParam(value = "tenTinh") String tenTinh,
                               @RequestParam(value = "loaiFile") String loaiFile, // pdf   xls    rtf
                               HttpServletRequest request,
                               HttpServletResponse response,
                               HttpSession session) {
        try {
            Map parameters = new HashMap();
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("tenbenhvien", tenBenhVien.toUpperCase());
            parameters.put("idPhieuDuTruTong", idPhieuDuTruTong);
            parameters.put("sophieu", soPhieuTaoLuuTru);
            parameters.put("dvtt", dvtt);
            parameters.put("tungay", tuNgay);
            parameters.put("denngay", denNgay);
            parameters.put("makhoa", maKhoa);
            File reportFile;
            if (idPhieuDuTruTong.equals("0")) {
                String reportName = "toathuoc_phieudutru_tatca.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
            } else {
                String reportName = "toathuoc_phieudutru.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
            }
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ApiOperation("Xuất Excel danh sách phiếu dự trù")
    @RequestMapping(value = "/exportExcelDuyetPhieuDuTru", method = RequestMethod.GET)
    public ModelAndView exportExcelDuyetPhieuDuTru(@RequestParam(value = "tuNgay") String tuNgay,
                                                 @RequestParam(value = "denNgay") String denNgay,
                                                 @RequestParam(value = "dvtt") String dvtt,
                                                 @RequestParam(value = "duyet") int duyet,
                                                 @RequestParam(value = "isBANT") int isBANT,
                                                 HttpServletRequest request) {
        List<Map<String, Object>> xlsData;
        xlsData = duyetDuTruHoanTraDAO.getDanhSachExportExcelPhieuDuTru(dvtt, tuNgay, denNgay, duyet, isBANT);
        ExportXLSView.sheetName = "DSvattu";
        ExportXLSView.fileName = "dsvattu.xls";
        return new ModelAndView(new ExportXLSView(), "report_view", xlsData);
    }

    @ApiOperation("Duyệt tổng hợp dự trù nội trú")
    @ResponseBody
    @RequestMapping(value = "/duyetPhieuDuTru", method = RequestMethod.POST)
    public String duyetPhieuDuTruTong(@RequestBody List<JSONObject> listPhieuDuTru,
                                      @RequestParam("dvtt") String dvtt,
                                      @RequestParam("duyet") int duyet,
                                      @RequestParam("nguoiDuyet") long nguoiDuyet,
                                      @RequestParam("ngayDuyet") String ngayDuyet,
                                      @RequestParam("maPhongBan") String maPhongBan,
                                      @RequestParam("maPhongBenh") String maPhongBenh,
                                      HttpSession session,
                                      HttpServletResponse response,
                                      HttpServletRequest request) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String kq = "0";
            String idGiaoDich = DuocTransaction.insert(0, 35, "Duyệt, hủy duyệt phiếu tổng hợp dự trù nội trú",
                nguoiDuyet, dvtt, 0, clientIP, hostIP, "", "duyetPhieuDuTruTong", dataSourceDuocV2);
            for (JSONObject object : listPhieuDuTru) {
                long idPhieuDuTru = Long.parseLong(object.get("id").toString());
                int t = duyetDuTruHoanTraDAO.duyetPhieuDuTru(dvtt, idPhieuDuTru, duyet, nguoiDuyet, ngayDuyet, idGiaoDich, maPhongBan, maPhongBenh);
                // 100 chốt dược
                if (t == 100) {
                    return "100";
                } else if (t != 0) { // lỗi không duyệt được phiếu
                    kq += object.get("soPhieu").toString() + ",";
                }
            }
            return kq;
        } catch (Exception e) {
            e.printStackTrace();
            return "-1";
        }
    }

    // Hoàn trả nội trú
    @ApiOperation("Lấy danh sách phiếu hoàn trả nội trú")
    @RequestMapping(value = "/getDanhSachPhieuHoanTraNoiTru", method = RequestMethod.GET)
    public @ResponseBody
    List getDanhSachPhieuHoanTraNoiTru(@RequestParam(value = "dvtt") String dvtt,
                                       @RequestParam(value = "tuNgay") String tuNgay,
                                       @RequestParam(value = "denNgay") String denNgay,
                                       @RequestParam(value = "trangThai") int trangThai,
                                       @RequestParam(value = "isBANT") int isBANT,
                                       @RequestParam(value = "maNhanVien") long maNhanVien,
                                       HttpSession session) {
        try {
            return duyetDuTruHoanTraDAO.getDanhSachPhieuHoanTraNoiTru(dvtt, tuNgay, denNgay, trangThai, isBANT, maNhanVien);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách chi tiết phiếu hoàn trả nội trú")
    @RequestMapping(value = "/getDanhSachChiTietPhieuHoanTra", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public @ResponseBody
    List getDanhSachChiTietPhieuHoanTra(@RequestParam(value = "idPhieuHoanTraTong") long idPhieuHoanTraTong,
                                        @RequestParam(value = "hinhThucXem") int hinhThucXem,
                                        HttpSession session) {
        try {
            return duyetDuTruHoanTraDAO.getDanhSachChiTietPhieuHoanTra(idPhieuHoanTraTong, hinhThucXem);
        }catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách bản in phiếu hoàn trả nội trú")
    @RequestMapping(value = "/getDanhSachBanInPhieuHoanTra", produces = "application/json; charset=utf-8")
    public @ResponseBody
    List getDanhSachBanInPhieuHoanTra(@RequestParam(value = "dvtt") String dvtt,
                                      @RequestParam(value = "idPhieuHoanTraTong") long idPhieuHoanTraTong,
                                      @RequestParam(value = "tuTuThuoc") int tuTuThuoc,
                                      @RequestParam(value = "tachLoai") int tachLoai) {
        return duyetDuTruHoanTraDAO.getDanhSachBanInPhieuHoanTra(dvtt, idPhieuHoanTraTong, tuTuThuoc, tachLoai);
    }

    @RequestMapping(value = "/printPhieuHoanTraNoiTru", method = RequestMethod.GET)
    public @ResponseBody
    void printPhieuHoanTraNoiTru(@RequestParam(value = "dvtt") String dvtt,
                                 @RequestParam(value = "idPhieuHoanTraTong") String idPhieuHoanTraTong,
                                 @RequestParam(value = "soPhieuLuuTru") String soPhieuLuuTru,
                                 @RequestParam(value = "ngayLap") String ngayLap,
                                 @RequestParam(value = "thanhTien") String thanhTien,
                                 @RequestParam(value = "maKhoVatTu") String maKhoVatTu,
                                 @RequestParam(value = "tenKhoVatTu") String tenKhoVatTu,
                                 @RequestParam(value = "tenKhoa") String tenKhoa,
                                 @RequestParam(value = "tenPhongBanHoanTra") String tenPhongBanHoanTra,
                                 @RequestParam(value = "maLoaiVatTu") String maLoaiVatTu,
                                 @RequestParam(value = "tuTuThuoc") String tuTuThuoc,
                                 @RequestParam(value = "tachLoai") String tachLoai,
                                 @RequestParam(value = "maNguoiIn") String maNguoiIn,
                                 @RequestParam(value = "nguoiLap") String nguoiLap,
                                 @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                 @RequestParam(value = "tenTinh") String tenTinh,
                                 @RequestParam(value = "loaiFile") String loaiFile, // pdf   xls    rtf
                                 HttpServletRequest request,
                                 HttpServletResponse response,
                                 HttpSession session) {
        try {
            Map parameters = new HashMap();
            String[] arr_nk = ngayLap.replaceAll("-","/").split("/");
            String ngay = arr_nk[0];
            String thang = arr_nk[1];
            String nam = arr_nk[2];
            parameters.put("nguoiin", maNguoiIn);
            String phieulinhthuoc = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 86204);
            String phieulinhdichtruyen = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 86205);
            if (maLoaiVatTu.equals("TH")) {
                if (phieulinhthuoc.equals("1")) {
                    parameters.put("tieude", "PHIẾU HOÀN TRẢ THUỐC");
                    parameters.put("mauin", "MS: 05D/BV-01");
                } else {
                    parameters.put("tieude", "PHIẾU HOÀN TRẢ THUỐC THƯỜNG");
                    parameters.put("mauin", "MS: 05D/BV-01");
                }
            } else if (maLoaiVatTu.equals("TH_HTT")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ THUỐC THÀNH PHẨM HƯỚNG TÂM THẦN");
                parameters.put("mauin", "MS: 05D/BV-01");
            } else if (maLoaiVatTu.equals("VT_TH")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ VẬT TƯ TIÊU HAO");
                parameters.put("mauin", "MS: 05D/BV-01");

            } else if (maLoaiVatTu.equals("VT_TT")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ VẬT TƯ THAY THẾ");
                parameters.put("mauin", "MS: 05D/BV-01");
            } else if (maLoaiVatTu.equals("DICHTRUYEN")) {
                if (phieulinhdichtruyen.equals("1")) {
                    parameters.put("tieude", "PHIẾU HOÀN TRẢ THUỐC");
                    parameters.put("mauin", "MS: 05D/BV-01");
                } else {
                    parameters.put("tieude", "PHIẾU HOÀN TRẢ DỊCH TRUYỀN");
                    parameters.put("mauin", "MS: 05D/BV-01");
                }
            } else if (maLoaiVatTu.equals("TH_NGHIEN")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ THUỐC THÀNH PHẨM GÂY NGHIỆN");
                parameters.put("mauin", "MS: 05D/BV-01");
            } else if (maLoaiVatTu.equals("YDC")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ Y DỤNG CỤ");
                parameters.put("mauin", "MS: 05D/BV-01");
            } else if (maLoaiVatTu.equals("VT_YT")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ VẬT TƯ Y TẾ");
                parameters.put("mauin", "MS: 05D/BV-01");
            } else {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ DƯỢC NỘI TRÚ");
                parameters.put("mauin", "MS: 05D/BV-01");
            }
            String donviquanlytructiep = "SỞ Y TẾ "+ tenTinh.toUpperCase();
            String tenBenhVienStr = tenBenhVien.toUpperCase();
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("tenbenhvien", tenBenhVienStr);
            parameters.put("khoa", tenKhoa);
            parameters.put("MaLoaiVatTu", maLoaiVatTu);
            parameters.put("sotienbangchu", Double.parseDouble(thanhTien));
            parameters.put("idPhieuHoanTraTong",idPhieuHoanTraTong);
            parameters.put("soluutru", soPhieuLuuTru);
            parameters.put("khonoitru", tenPhongBanHoanTra);
            parameters.put("MAKHOVATTU", maKhoVatTu);
            parameters.put("khoanhan", tenKhoVatTu);
            parameters.put("dvtt", dvtt);
            parameters.put("ngay", ngay);
            parameters.put("thang", thang);
            parameters.put("nam", nam);
            parameters.put("nguoilap", nguoiLap);
            parameters.put("TUTHUOC", tuTuThuoc);
            parameters.put("ngayIn", DuocUtils.getNgayHienTai());
            String phieuht_tenbn = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 31010);
            parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
            File reportFile;
            if (tachLoai.equals("0")){
                String reportName = "rp_phieuhoantraduoc_tonghop.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
            } else if (maLoaiVatTu.equals("TH_HTT")) {
                if (phieuht_tenbn.equals("1")) {
                    String reportName = "rp_phieuhoantraduoc_tenbenhnhan_HTT.jasper";
                    reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
                } else {
                    String reportName = "rp_phieudutruhoantraduoc_HTT.jasper";
                    reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
                }
            } else if (maLoaiVatTu.equals("TH_NGHIEN")) {
                if (phieuht_tenbn.equals("1")) {
                    String reportName = "rp_phieuhoantraduoc_tenbenhnhan_HTT.jasper";
                    reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
                } else {
                    String reportName = "rp_phieudutruhoantra_NGHIEN.jasper";
                    reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
                }
            } else {
                if (phieuht_tenbn.equals("1")) {
                    String reportName = "rp_phieudutruhoantra_tenbenhnhan.jasper";
                    reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
                } else {
                    String reportName = "rp_phieudutruhoantraduoc.jasper";
                    reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
                }
            }
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/printPhieuXuatKhoHoanTraNoiTru", method = RequestMethod.GET)
    public @ResponseBody
    void printPhieuXuatKhoHoanTraNoiTru(@RequestParam(value = "idPhieuHoanTraTong") String idPhieuHoanTraTong,
                                        @RequestParam(value = "dvtt") String dvtt,
                                        @RequestParam(value = "soPhieuLuuTru") String soPhieuLuuTru,
                                        @RequestParam(value = "ngayLap") String ngayLap,
                                        @RequestParam(value = "thanhTien") String thanhTien,
                                        @RequestParam(value = "maKhoVatTu") String maKhoVatTu,
                                        @RequestParam(value = "tenKhoVatTu") String tenKhoVatTu,
                                        @RequestParam(value = "tenKhoa") String tenKhoa,
                                        @RequestParam(value = "tenPhongBanDuTru") String tenPhongBanDuTru,
                                        @RequestParam(value = "maLoaiVatTu") String maLoaiVatTu,
                                        @RequestParam(value = "tuTuThuoc") String tuTuThuoc,
                                        @RequestParam(value = "nguoiLap") String nguoiLap,
                                        @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                        @RequestParam(value = "tenTinh") String tenTinh,
                                        @RequestParam(value = "loaiFile") String loaiFile, // pdf   xls    rtf
                                        HttpServletRequest request,
                                        HttpServletResponse response,
                                        HttpSession session) {
        try {
            Map parameters = new HashMap();
            String[] arr_nk = ngayLap.replaceAll("-","/").split("/");
            String ngay = arr_nk[0];
            String thang = arr_nk[1];
            String nam = arr_nk[2];

            if (maLoaiVatTu.equals("TH")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ THUỐC THƯỜNG");
                parameters.put("mauin", "Mẫu số: C21 - HD");
            } else if (maLoaiVatTu.equals("TH_HTT")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ THUỐC THÀNH PHẨM HƯỚNG TÂM THẦN");
                parameters.put("mauin", "Mẫu số: C21 - HD");
            } else if (maLoaiVatTu.equals("VT_TH")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ VẬT TƯ TIÊU HAO");
                parameters.put("mauin", "Mẫu số: C21 - HD");
            } else if (maLoaiVatTu.equals("VT_TT")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ VẬT TƯ THAY THẾ");
                parameters.put("mauin", "Mẫu số: C21 - HD");
            } else if (maLoaiVatTu.equals("DICHTRUYEN")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ DỊCH TRUYỀN");
                parameters.put("mauin", "Mẫu số: C21 - HD");
            } else if (maLoaiVatTu.equals("TH_NGHIEN")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ THUỐC THÀNH PHẨM GÂY NGHIỆN");
                parameters.put("mauin", "Mẫu số: C21 - HD");
            } else if (maLoaiVatTu.equals("VT_YT")) {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ VẬT TƯ Y TẾ");
                parameters.put("mauin", "Mẫu số: C21 - HD");
            } else {
                parameters.put("tieude", "PHIẾU HOÀN TRẢ DƯỢC NỘI TRÚ");
                parameters.put("mauin", "Mẫu số: C21 - HD");
            }
            // Tongtien = new docso().docso(Double.parseDouble(Tongtien));
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("tenbenhvien", tenBenhVien.toUpperCase());
            parameters.put("khoa", tenKhoa);
            parameters.put("MaLoaiVatTu", maLoaiVatTu);
            parameters.put("sotienbangchu", new DocSo().docso(Double.parseDouble(thanhTien)));
            parameters.put("idPhieuHoanTraTong",idPhieuHoanTraTong);
            parameters.put("soluutru", soPhieuLuuTru);
            parameters.put("khonoitru", tenPhongBanDuTru);
            parameters.put("MAKHOVATTU", maKhoVatTu);
            parameters.put("khoanhan", tenKhoVatTu);
            parameters.put("dvtt", dvtt);
            parameters.put("ngay", ngay);
            parameters.put("thang", thang);
            parameters.put("nam", nam);
            parameters.put("nguoilap", nguoiLap);
            parameters.put("TUTHUOC", tuTuThuoc);
            parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
            File reportFile;
            if (maLoaiVatTu.equals("TH_HTT") || maLoaiVatTu.equals("TH_NGHIEN")) {
                String reportName = "rp_phieudutruhoantraduoc_HTT.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
            } else {
                String reportName = "rp_phieuxuatkhohoantraduoc.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetdutruhoantra/" + reportName).getFile();
            }
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/exportExcelDuyetPhieuHoanTra", method = RequestMethod.GET)
    public ModelAndView exportExcelDuyetPhieuHoanTra(@RequestParam(value = "tuNgay") String tuNgay,
                                                   @RequestParam(value = "denNgay") String denNgay,
                                                   @RequestParam(value = "dvtt") String dvtt,
                                                   @RequestParam(value = "duyet") int duyet,
                                                   @RequestParam(value = "isBANT") int isBANT,
                                                   HttpServletRequest request) {


        List<Map<String, Object>> xlsData = duyetDuTruHoanTraDAO.getDanhSachExportExcelPhieuHoanTra(dvtt, tuNgay, denNgay, duyet, isBANT);
        ExportXLSView.sheetName = "DSvattu";
        ExportXLSView.fileName = "dsvattu.xls";
        return new ModelAndView(new ExportXLSView(), "report_view", xlsData);
    }

    @ResponseBody
    @RequestMapping(value = "/duyetPhieuHoanTra", method = RequestMethod.POST)
    public String duyetPhieuHoanTra(@RequestBody List<JSONObject> listPhieuHoanTra,
                                        @RequestParam("dvtt") String dvtt,
                                        @RequestParam("duyet") int duyet,
                                        @RequestParam("nguoiDuyet") long nguoiDuyet,
                                        @RequestParam("ngayDuyet") String ngayDuyet,
                                        @RequestParam("maPhongBan") String maPhongBan,
                                        @RequestParam("maPhongBenh") String maPhongBenh,
                                        HttpSession session,
                                        HttpServletResponse response,
                                        HttpServletRequest request) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String kq = "0";
            String idGiaoDich = DuocTransaction.insert(0, 43, "Duyệt , hủy duyệt tổng hợp hoàn trả nội trú",
                nguoiDuyet, dvtt, 0, clientIP, hostIP, "", "duyetPhieuHoanTraTong", dataSourceDuocV2);
            for (JSONObject object : listPhieuHoanTra) {
                long idPhieuHoanTra = Long.parseLong(object.get("id").toString());
                int t = duyetDuTruHoanTraDAO.duyetPhieuHoanTra(dvtt, idPhieuHoanTra, duyet, nguoiDuyet, ngayDuyet, idGiaoDich, maPhongBan, maPhongBenh);
                // 100 chốt dược
                if (t == 100) {
                    return "100";
                } else if (t != 0) { // lỗi không duyệt được phiếu
                    kq += object.get("soPhieu").toString() + ",";
                }
            }
            return kq;
        } catch (Exception e) {
            e.printStackTrace();
            return "-1";
        }
    }
}
