package com.vnpt.duoc.duocModule.dao.kiemtraduyetnhan;

import com.vnpt.duoc.jdbc.JdbcTemplate;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class KiemTraDuyetNhanDAOImp implements KiemTraDuyetNhanDAO {
    private final HikariDataSource dataSourceDuocV2;

    public KiemTraDuyetNhanDAOImp(HikariDataSource dataSourceDuocV2) {
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    @Override
    public List getDanhSachPhieuChuyenKhoTheoTrangThai(String dvtt, int trangThai) {
        String sql = "call DC_SP_PHIEUCHUADUYETNHAN_SEL(?,?)#c,s,l";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourceDuocV2);
        return jdbcTemplate.queryForList(sql, new Object[]{dvtt, trangThai});
    }
}
