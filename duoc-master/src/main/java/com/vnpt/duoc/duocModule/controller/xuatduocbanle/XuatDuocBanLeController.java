package com.vnpt.duoc.duocModule.controller.xuatduocbanle;

import com.vnpt.duoc.duocModule.DuocTransaction;
import com.vnpt.duoc.duocModule.DuocUtils;
import com.vnpt.duoc.duocModule.dao.xuatduocbanle.XuatDuocBanLeDAO;
import com.vnpt.duoc.duocModule.objects.khamchuabenh.ToaThuocNgoaiTruObj;
import com.vnpt.duoc.duocModule.objects.xuatduocbanle.ThongTinPhieuXuatDuocBanLeObj;
import com.zaxxer.hikari.HikariDataSource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.InetAddress;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/xuatduocbanle")
@Api(value="Xuất dược bán lẻ Controller")
public class XuatDuocBanLeController {
    XuatDuocBanLeDAO xuatDuocBanLeDAO;
    private HikariDataSource dataSourceDuocV2;

    public XuatDuocBanLeController(XuatDuocBanLeDAO xuatDuocBanLeDAO, HikariDataSource dataSourceDuocV2) {
        this.xuatDuocBanLeDAO = xuatDuocBanLeDAO;
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    @ApiOperation("Lấy danh sách toa thuốc bán lẻ")
    @RequestMapping(value="/getDanhSachToaThuocBanLe", method = RequestMethod.GET)
    public List getDanhSachToaThuocBanLe(@RequestParam("dvtt") String dvtt,
                                            @RequestParam("ngay") String ngay,
                                            HttpServletRequest request,
                                            HttpServletResponse response,
                                            HttpSession session) {
        try {
            return xuatDuocBanLeDAO.getDanhSachToaThuocBanLe(dvtt, ngay);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Thêm toa thuốc bán lẻ")
    @ResponseBody
    @RequestMapping(value = "/addToaThuocBanLe", method = RequestMethod.POST)
    public int addToaThuocBanLe(@RequestBody ThongTinPhieuXuatDuocBanLeObj thongTinPhieuXuatDuocBanLeObj,
                                HttpSession session,
                                HttpServletRequest request,
                                HttpServletResponse response) {
        try {
            return xuatDuocBanLeDAO.addToaThuocBanLe(thongTinPhieuXuatDuocBanLeObj);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Cập nhật toa thuốc bán lẻ")
    @ResponseBody
    @RequestMapping(value = "/updateToaThuocBanLe", method = RequestMethod.POST)
    public int updateToaThuocBanLe(@RequestBody ThongTinPhieuXuatDuocBanLeObj thongTinPhieuXuatDuocBanLeObj,
                                HttpSession session,
                                HttpServletRequest request,
                                HttpServletResponse response) {
        try {
            return xuatDuocBanLeDAO.updateToaThuocBanLe(thongTinPhieuXuatDuocBanLeObj);
        } catch (Exception e) {
            DuocUtils.responseErrorToClient("Something went wrong!", "Thông tin lỗi: " + e.getMessage(), response);
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Xóa toa thuốc bán lẻ")
    @ResponseBody
    @RequestMapping(value = "/deleteToaThuocBanLe", method = RequestMethod.POST)
    public int deleteToaThuocBanLe(@RequestBody ThongTinPhieuXuatDuocBanLeObj thongTinPhieuXuatDuocBanLeObj,
                                   HttpSession session,
                                   HttpServletRequest request,
                                   HttpServletResponse response) {
        try {
            int checkDelete = xuatDuocBanLeDAO.checkDeleteToaThuocBanLe(thongTinPhieuXuatDuocBanLeObj.getMaToaThuoc(), thongTinPhieuXuatDuocBanLeObj.getDvtt());
            if (checkDelete != 0) {
                return checkDelete;
            }
            int ketQua;
            List<Map<String, Object>> list = xuatDuocBanLeDAO.getDanhSachChiTietThuocBanLe(thongTinPhieuXuatDuocBanLeObj.getMaToaThuoc(), thongTinPhieuXuatDuocBanLeObj.getDvtt(), thongTinPhieuXuatDuocBanLeObj.getNghiepVu());
            if (!list.isEmpty()) {
                for (Map m : list) {
                    ToaThuocNgoaiTruObj tt = new ToaThuocNgoaiTruObj();
                    tt.setSttToaThuoc(m.get("STT_TOATHUOC").toString());
                    tt.setMaToaThuoc(thongTinPhieuXuatDuocBanLeObj.getMaToaThuoc());
                    tt.setDvtt(thongTinPhieuXuatDuocBanLeObj.getDvtt());
                    tt.setMaKhamBenh("");
                    tt.setSoPhieuThanhToan("");
                    tt.setThanhTien(0.0);
                    tt.setMaKhoVatTu(Integer.parseInt(m.get("MAKHOVATTU").toString()));
                    tt.setDonGiaBV(0.0);
                    tt.setNghiepVu(thongTinPhieuXuatDuocBanLeObj.getNghiepVu());
                    tt.setMaVatTu(Integer.parseInt(m.get("MAVATTU").toString()));
                    tt.setSoVaoVien(0);
                    try {
                        String hostIP = InetAddress.getLocalHost().getHostAddress();
                        String clientIP = request.getRemoteAddr();
                        String idGiaoDich = DuocTransaction.insert(0, 25, "Xóa chi tiết thuốc bán lẻ",
                            thongTinPhieuXuatDuocBanLeObj.getMaNhanVien(), thongTinPhieuXuatDuocBanLeObj.getDvtt(), 0,
                            clientIP, hostIP, "", "deleteToaThuocBanLe", dataSourceDuocV2);
                        tt.setIdGiaoDich(Long.parseLong(idGiaoDich));
                        tt.setMaUser(thongTinPhieuXuatDuocBanLeObj.getMaNhanVien());
                        tt.setMaKhoa(thongTinPhieuXuatDuocBanLeObj.getMaPhongBan());
                        tt.setMaPhong(thongTinPhieuXuatDuocBanLeObj.getMaPhongBenh());
                        ketQua = xuatDuocBanLeDAO.deleteChiTietToaThuocBanLe(tt);
                    } catch (Exception e) {
                        return -1;  // Lỗi xóa chi tiết toa thuốc bán lẻ.
                    }
                    if (ketQua == 100) return 100; // Đã chốt số liệu dược.
                }
            }
            return xuatDuocBanLeDAO.deleteToaThuocBanLe(thongTinPhieuXuatDuocBanLeObj.getMaToaThuoc(), thongTinPhieuXuatDuocBanLeObj.getDvtt());
        } catch (Exception e) {
            DuocUtils.responseErrorToClient("Something went wrong!", "Thông tin lỗi: " + e.getMessage(), response);
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Lấy danh sách chi tiết toa thuốc bán lẻ")
    @RequestMapping(value="/getDanhSachChiTietThuocBanLe", method = RequestMethod.GET)
    public List getDanhSachChiTietThuocBanLe(@RequestParam("dvtt") String dvtt,
                                             @RequestParam("maToaThuoc") String maToaThuoc,
                                             @RequestParam("nghiepVu") String nghiepVu,
                                             HttpServletRequest request,
                                             HttpServletResponse response,
                                             HttpSession session) {
        try {
            return xuatDuocBanLeDAO.getDanhSachChiTietThuocBanLe(maToaThuoc, dvtt, nghiepVu);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Thêm chi tiết toa thuốc bán lẻ")
    @ResponseBody
    @RequestMapping(value = "/addChiTietToaThuocBanLe", method = RequestMethod.POST)
    public int addChiTietToaThuocBanLe(@RequestBody ToaThuocNgoaiTruObj toaThuocNgoaiTruObj,
                                HttpSession session,
                                HttpServletRequest request,
                                HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 23, "Thêm chi tiết toa thuốc bán lẻ",
                toaThuocNgoaiTruObj.getMaUser(), toaThuocNgoaiTruObj.getDvtt(), 0,
                clientIP, hostIP, "", "addChiTietToaThuocBanLe", dataSourceDuocV2);
            ToaThuocNgoaiTruObj temp = toaThuocNgoaiTruObj;
            temp.setIdGiaoDich(Long.parseLong(idGiaoDich));
            return xuatDuocBanLeDAO.addChiTietToaThuocBanLe(temp);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Xóa chi tiết toa thuốc bán lẻ")
    @ResponseBody
    @RequestMapping(value = "/deleteChiTietToaThuocBanLe", method = RequestMethod.POST)
    public int deleteChiTietToaThuocBanLe(@RequestBody ToaThuocNgoaiTruObj toaThuocNgoaiTruObj,
                                       HttpSession session,
                                       HttpServletRequest request,
                                       HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 25, "Xóa chi tiết toa thuốc bán lẻ",
                toaThuocNgoaiTruObj.getMaUser(), toaThuocNgoaiTruObj.getDvtt(), 0,
                clientIP, hostIP, "", "deleteChiTietToaThuocBanLe", dataSourceDuocV2);
            ToaThuocNgoaiTruObj temp = toaThuocNgoaiTruObj;
            temp.setIdGiaoDich(Long.parseLong(idGiaoDich));
            return xuatDuocBanLeDAO.deleteChiTietToaThuocBanLe(temp);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
