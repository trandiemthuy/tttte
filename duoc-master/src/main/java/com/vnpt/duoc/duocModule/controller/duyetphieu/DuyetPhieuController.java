package com.vnpt.duoc.duocModule.controller.duyetphieu;

import com.vnpt.duoc.duocModule.DuocConfigs;
import com.vnpt.duoc.duocModule.DuocTransaction;
import com.vnpt.duoc.duocModule.DuocUtils;
import com.vnpt.duoc.duocModule.dao.chuyenkho.ChuyenKhoDAO;
import com.vnpt.duoc.duocModule.dao.duyetphieu.DuyetPhieuDAO;
import com.vnpt.duoc.duocModule.dao.hethong.DocSo;
import com.vnpt.duoc.duocModule.dao.hethong.HeThongDAO;
import com.vnpt.duoc.jasper.ExportXLSView;
import com.vnpt.duoc.jasper.JasperHelper;
import com.zaxxer.hikari.HikariDataSource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.minidev.json.JSONObject;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.net.InetAddress;
import java.time.LocalTime;
import java.time.MonthDay;
import java.time.YearMonth;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/duyetphieu")
@Api(value="DuyetPhieuController")
public class DuyetPhieuController {
    private final DuyetPhieuDAO duyetPhieuDAO;
    private final ChuyenKhoDAO chuyenKhoDAO;
    private final HeThongDAO heThongDAO;
    private final HikariDataSource dataSourceDuocV2;

    public DuyetPhieuController(DuyetPhieuDAO duyetPhieuDAO, ChuyenKhoDAO chuyenKhoDAO, HeThongDAO heThongDAO, HikariDataSource dataSourceDuocV2) {
        this.duyetPhieuDAO = duyetPhieuDAO;
        this.chuyenKhoDAO = chuyenKhoDAO;
        this.heThongDAO = heThongDAO;
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }


    @ApiOperation("Lấy danh sách kho chuyển theo mã nhân viên")
    @RequestMapping(value="/getDanhSachKhoChuyenTheoNhanVien", method = RequestMethod.GET)
    public List getDanhSachKhoChuyenTheoNhanVien(@RequestParam(value = "dvtt") String dvtt,
                                                 @RequestParam(value = "maNhanVien") String maNhanVien,
                                                 HttpServletResponse response,
                                                 HttpServletRequest request,
                                                 HttpSession session) {
        try {
            return duyetPhieuDAO.getDanhSachKhoChuyenTheoNhanVien(dvtt, maNhanVien);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @ApiOperation("Lấy danh sách phiếu theo nghiệp vụ")
    @RequestMapping(value="/getDanhSachPhieuChuyenTheoNghiepVu", method = RequestMethod.GET)
    public List getDanhSachPhieuChuyenTheoNghiepVu(@RequestParam("dvtt") String dvtt,
                                                 @RequestParam("tuNgay") String tuNgay,
                                                 @RequestParam("denNgay") String denNgay,
                                                 @RequestParam("maNghiepVu") long maNghiepVu,
                                                 @RequestParam("maKho") long maKho,
                                                 @RequestParam("trangThaiPhieu") int trangThaiPhieu,
                                                 HttpServletResponse response,
                                                 HttpServletRequest request,
                                                 HttpSession session) {
        try {
            return duyetPhieuDAO.getDanhSachPhieuChuyenTheoNghiepVu(DuocUtils.convertStringToDate(tuNgay, "dd/MM/yyyy"),
                DuocUtils.convertStringToDate(denNgay, "dd/MM/yyyy"), maNghiepVu, maKho, trangThaiPhieu, dvtt);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    // Duyệt phiếu
    @ApiOperation("Duyệt phiếu chuyển kho")
    @ResponseBody
    @RequestMapping(value = "/duyetPhieuChuyenKho", method = RequestMethod.POST)
    public int duyetPhieuChuyenKho(@RequestBody List<JSONObject> jsonObjects,
                                   @RequestParam("dvtt") String dvtt,
                                   @RequestParam("nguoiDuyet") long nguoiDuyet,
                                   @RequestParam("maPhongBan") String maPhongBan,
                                   @RequestParam("maKhoa") String maKhoa,
                                   HttpSession session,
                                   HttpServletResponse response,
                                   HttpServletRequest request) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 16, "Duyệt phiếu chuyển kho",
                nguoiDuyet, dvtt, 0, clientIP, hostIP, "", "duyetPhieuChuyenKho", dataSourceDuocV2);
            Long idChuyenKho = Long.parseLong(jsonObjects.get(0).get("idPhieuChuyen").toString());
            String ngayDuyet = jsonObjects.get(0).get("ngayDuyet").toString();

            for (JSONObject object : jsonObjects) {
                Long idChuyenKhoChiTiet = Long.parseLong(object.get("idChuyenKhoChiTiet").toString());
                Double soLuongYeuCau = Double.parseDouble(object.get("soLuongYeuCau").toString());
                Double soLuongDuyet = Double.parseDouble(object.get("soLuongDuyet").toString());
                duyetPhieuDAO.updateSoLuongDuyet(dvtt, idChuyenKhoChiTiet, soLuongYeuCau, soLuongDuyet, idGiaoDich, nguoiDuyet, maKhoa, maPhongBan);
            }
            return duyetPhieuDAO.duyetPhieuChuyenKho(dvtt, idChuyenKho, DuocUtils.convertStringToDate(ngayDuyet, "dd/MM/yyyy"),
                nguoiDuyet, idGiaoDich, maKhoa, maPhongBan);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Hủy duyệt phiếu chuyển kho")
    @ResponseBody
    @RequestMapping(value = "/huyDuyetChuyenKho", method = RequestMethod.POST)
    public int huyDuyetChuyenKho(@RequestParam("idPhieuChuyen") long idPhieuChuyen,
                                 @RequestParam("dvtt") String dvtt,
                                 @RequestParam("nguoiDuyet") long nguoiDuyet,
                                 @RequestParam("maPhongBan") String maPhongBan,
                                 @RequestParam("maKhoa") String maKhoa,
                                 HttpSession session,
                                 HttpServletResponse response,
                                 HttpServletRequest request) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 18, "Hủy duyệt chuyển kho",
               nguoiDuyet, dvtt, 0, clientIP, hostIP, "", "huyDuyetChuyenKho", dataSourceDuocV2);
            return duyetPhieuDAO.huyDuyetChuyenKho(dvtt, idPhieuChuyen, nguoiDuyet, idGiaoDich, maKhoa, maPhongBan);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    //Print
    @ApiOperation("In phiếu xuất kho form duyệt phiếu")
    @ResponseBody
    @RequestMapping(value = "/printPhieuXuatKhoDuyet", method = RequestMethod.GET)
    public void printPhieuXuatKhoDuyet(HttpServletResponse response,
                                       HttpServletRequest request,
                                       HttpSession session,
                                       @RequestParam(value = "dvtt") String dvtt,
                                       @RequestParam(value = "idPhieuChuyen") long idPhieuChuyen,
                                       @RequestParam(value = "maNghiepVu") int maNghiepVu,
                                       @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                       @RequestParam(value = "tenTinh") String tenTinh,
                                       @RequestParam(value = "nguoiLap") String nguoiLap,
                                       @RequestParam(value = "mau") int mau, // 0: Đứng      1: Ngang
                                       @RequestParam(value = "loaiFile") String loaiFile) {
        try {
            String tenbenhvien = tenBenhVien.toUpperCase();
            Map map = chuyenKhoDAO.getThongTinPhieuChuyenKho(dvtt, idPhieuChuyen, maNghiepVu);
            Map parameters = new HashMap();
            parameters.put("dvtt", dvtt);
            parameters.put("idPhieuChuyen", idPhieuChuyen);
            parameters.put("sophieu", map.get("SoPhieuChuyen").toString());
            parameters.put("soluutru", map.get("SoluuTru").toString());
            parameters.put("xuattukho", map.get("Khogiao").toString());
            parameters.put("khonoitru", map.get("Khogiao").toString());
            parameters.put("xuatdenkho", map.get("Khonhan").toString());
            parameters.put("khoanhan", map.get("Khonhan").toString());
            parameters.put("tendonvichuyen", map.get("TENDONVICHUYEN").toString());
            parameters.put("tendonvinhan", map.get("TENDONVINHAN").toString());
            parameters.put("tennguoinhan", map.get("TENNGUOINHAN").toString());
            parameters.put("tenbenhvien", tenbenhvien);
            parameters.put("tieude", "PHIẾU XUẤT KHO");
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("lydoxuatkho", map.get("GhiChu").toString());
            parameters.put("ngay", MonthDay.now().getDayOfMonth() + "");
            parameters.put("thang", MonthDay.now().getMonthValue() + "");
            parameters.put("nam", YearMonth.now().getYear() + "");
            parameters.put("gio", LocalTime.now().getHour() + "");
            parameters.put("phut", LocalTime.now().getMinute() + "");
            parameters.put("giay", LocalTime.now().getMinute() + "");
            parameters.put("nguoilapbang", request.getSession().getAttribute("Sess_User"));
            Double tongTien = chuyenKhoDAO.getTongTienPhieuChuyenKho(idPhieuChuyen);
            parameters.put("sotienbangchu", new DocSo().docso(tongTien));
            parameters.put("nguoilapbang", nguoiLap);
            File reportFile;
            if(maNghiepVu == 37) { // Chuyển kho liên thông
                String reportName = "rp_phieuxuatkholienthong_duocv2.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "duyetphieu/" + reportName).getFile();
            }else {
                if (mau == 0) { // Mẫu đứng
                    String reportName = "rp_phieuxuatkho_khodung.jasper";
                    reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
                } else { // Mẫu ngang
                    String reportName = "rp_phieuxuatkho_khongang.jasper";
                    reportFile = new ClassPathResource(DuocConfigs.reportURL + "chuyenkho/" + reportName).getFile();
                }
            }
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    @ApiOperation("Xuất dữ liệu Excel chi tiết 1 phiếu")
    @RequestMapping(value = "/exportExcelChiTietPhieuChuyenKho", method = RequestMethod.GET)
    public ModelAndView exportExcelChiTietPhieuChuyenKho(@RequestParam(value = "dvtt") String dvtt,
                                                  @RequestParam(value = "idPhieuChuyen") long idPhieuChuyen,
                                                  HttpServletRequest request) {
        try {
            List list = duyetPhieuDAO.getDanhSachVatTuChiTietPhieuChuyen(dvtt, idPhieuChuyen);
            ExportXLSView.sheetName = "DSvattu";
            ExportXLSView.fileName = "dsvattu.xls";
            return new ModelAndView(new ExportXLSView(), "report_view", list);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ApiOperation("Xuất dữ liệu Excel tất cả phiếu trong khoảng thời gian")
    @RequestMapping(value = "/exportExcelPhieuChuyenKho", method = RequestMethod.GET)
    public ModelAndView exportExcelPhieuChuyenKho(@RequestParam("dvtt") String dvtt,
                                                  @RequestParam("tuNgay") String tuNgay,
                                                  @RequestParam("denNgay") String denNgay,
                                                  @RequestParam("maKho") int maKho,
                                                  @RequestParam("duyet") int duyet,
                                                  HttpSession session) {
        try {
            List list = duyetPhieuDAO.getDanhSachVatTuPhieuChuyen(dvtt,
                DuocUtils.convertStringToDate(tuNgay, "dd/MM/yyyy"),
                DuocUtils.convertStringToDate(denNgay, "dd/MM/yyyy"),
                maKho,
                duyet
                );
            ExportXLSView.sheetName = "DSvattu";
            ExportXLSView.fileName = "dsvattu.xls";
            return new ModelAndView(new ExportXLSView(), "report_view", list);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
