create or replace FUNCTION "HIS_MANAGER"."DANHSACHNHANVIEN"(p_DVTT    IN varchar2,
                                                          p_TENKHOA IN varchar2)
  RETURN SYS_REFCURSOR IS
  cur SYS_REFCURSOR;
BEGIN
  open cur for
    select nv.ma_nhanvien, nv.ten_nhanvien, cd.ten_chucdanh as chucdanh
      from his_fw.dm_nhanvien nv,
      his_fw.dm_phongban pb,
      HIS_FW.dm_chucdanh_nhanvien cd
     where pb.MA_DONVI = p_DVTT
       and pb.KYHIEU_PHONGBAN like ('%' || upper(p_TENKHOA) || '%')
       and nv.ma_phongban = to_number(pb.ma_phongban)
       and nv.chucdanh_nhanvien = cd.ma_chucdanh
     order by nv.ten_nhanvien;
  RETURN cur;
END;

