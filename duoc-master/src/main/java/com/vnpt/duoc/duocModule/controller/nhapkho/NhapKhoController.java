package com.vnpt.duoc.duocModule.controller.nhapkho;
import com.vnpt.duoc.duocModule.DuocConfigs;
import com.vnpt.duoc.duocModule.DuocTransaction;
import com.vnpt.duoc.duocModule.DuocUtils;
import com.vnpt.duoc.duocModule.dao.hethong.DocSo;
import com.vnpt.duoc.duocModule.dao.hethong.HeThongDAO;
import com.vnpt.duoc.duocModule.dao.nhapkho.NhapKhoDAO;
import com.vnpt.duoc.duocModule.objects.nhapkho.ChiTietNhapKhoObj;
import com.vnpt.duoc.duocModule.objects.nhapkho.NhapKhoObj;
import com.vnpt.duoc.jasper.ExportXLSView;
import com.vnpt.duoc.jasper.JasperHelper;
import com.zaxxer.hikari.HikariDataSource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.minidev.json.JSONObject;
import net.sf.jasperreports.engine.JRParameter;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.net.InetAddress;
import java.util.*;

@RestController
@RequestMapping("api/nhapkho")
@Api(value="NhapKhoController")
public class NhapKhoController {
    private final NhapKhoDAO nhapKhoDAO;
    private final HeThongDAO heThongDAO;
    private final HikariDataSource dataSourceDuocV2;

    public NhapKhoController(NhapKhoDAO nhapKhoDAO, HeThongDAO heThongDAO, HikariDataSource dataSourceDuocV2) {
        this.nhapKhoDAO = nhapKhoDAO;
        this.heThongDAO = heThongDAO;
        this.dataSourceDuocV2 = dataSourceDuocV2;
    }

    @ApiOperation("Lấy danh sách nhập kho theo khoảng thời gian")
    @RequestMapping(value="/getDanhSachPhieuNhapKho", method = RequestMethod.GET)
    public List getDanhSachPhieuNhapKho(@RequestParam(value = "dvtt") String dvtt,
                                        @RequestParam(value = "tuNgay") String tuNgay,
                                        @RequestParam(value = "denNgay") String denNgay,
                                        @RequestParam(value = "maNghiepVu") String maNghiepVu,
                                        HttpServletResponse response,
                                        HttpServletRequest request,
                                        HttpSession session) {
        return nhapKhoDAO.getDanhSachPhieuNhapKho(dvtt, tuNgay, denNgay, maNghiepVu);
    }

    @ApiOperation("Lấy danh sách nguồn dược")
    @RequestMapping(value="/getDanhSachNguonDuoc", method = RequestMethod.GET)
    public List getDanhSachNguonDuoc(HttpServletResponse response,
                                        HttpServletRequest request,
                                        HttpSession session) {
        return nhapKhoDAO.getDanhSachNguonDuoc();
    }
    @ApiOperation("Lấy danh sách nhà cung cấp")
    @RequestMapping(value="/getDanhSachNhaCungCap", method = RequestMethod.GET)
    public List getDanhSachNhaCungCap(HttpServletResponse response,
                                      HttpServletRequest request,
                                      HttpSession session) {
        return nhapKhoDAO.getDanhSachNhaCungCap();
    }
    @ApiOperation("Lấy danh sách kho dược")
    @RequestMapping(value="/getDanhSachKhoNhap", method = RequestMethod.GET)
    public List getDanhSachKhoNhap(@RequestParam(value = "tenNghiepVu") String tenNghiepVu,
                                   @RequestParam(value = "dvtt") String dvtt,
                                   HttpServletResponse response,
                                   HttpServletRequest request,
                                   HttpSession session) {
        return nhapKhoDAO.getDanhSachKhoNhap(tenNghiepVu, dvtt);
    }

    @ApiOperation("Lấy danh sách người nhận")
    @RequestMapping(value="/getDanhSachNguoiNhan", method = RequestMethod.GET)
    public List getDanhSachNguoiNhan(@RequestParam(value = "dvtt") String dvtt,
                                     @RequestParam(value = "tenKhoa") String tenKhoa,
                                     HttpServletResponse response,
                                     HttpServletRequest request,
                                     HttpSession session) {
        return nhapKhoDAO.getDanhSachNguoiNhan(dvtt, tenKhoa);
    }

    @ApiOperation("Lấy danh sách Hội đồng kiểm nhập")
    @RequestMapping(value="/getDanhSachHoiDongKiemNhap", method = RequestMethod.GET)
    public List getDanhSachHoiDongKiemNhap(@RequestParam(value = "dvtt") String dvtt,
                                     HttpServletResponse response,
                                     HttpServletRequest request,
                                     HttpSession session) {
        return nhapKhoDAO.getDanhSachHoiDongKiemNhap(dvtt);
    }

    @ApiOperation("Lấy danh sách tham số đơn vị")
    @RequestMapping(value="/getDanhSachThamSoDonVi", method = RequestMethod.GET)
    public List getDanhSachThamSoDonVi(@RequestParam(value = "dvtt") String dvtt,
                                        HttpServletResponse response,
                                        HttpServletRequest request,
                                        HttpSession session) {
        return nhapKhoDAO.getAllThamSoDonVi(dvtt);
    }

    @ApiOperation("Lấy danh sách vật tư")
    @RequestMapping(value="/getDanhSachVatTu", method = RequestMethod.GET)
    public List getDanhSachVatTu(@RequestParam(value = "dvtt") String dvtt,
                                   HttpServletResponse response,
                                   HttpServletRequest request,
                                   HttpSession session) {
        return nhapKhoDAO.getDanhSachVatTu(dvtt);
    }

    @ApiOperation("Thêm phiếu nhập kho")
    @ResponseBody
    @RequestMapping(value = "/addPhieuNhapKho", method = RequestMethod.POST)
    public Map addPhieuNhapKho(@RequestBody JSONObject jsonObject,
                               HttpSession session,
                               HttpServletResponse response,
                               HttpServletRequest request) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 1, "Thêm phiếu nhập kho, nghiệp vụ " + jsonObject.get("maNghiepVuChuyen").toString(),
                0, jsonObject.get("dvtt").toString() , 0,
                clientIP, hostIP, "", "addPhieuNhapKho", dataSourceDuocV2);
            NhapKhoObj nhapKhoObj = new NhapKhoObj(jsonObject, session, idGiaoDich);
            response.setStatus(HttpServletResponse.SC_CREATED);
            return nhapKhoDAO.addPhieuNhapKho(nhapKhoObj);
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return new HashMap();
        }
    }

    @ApiOperation("Cập nhật thông tin phiếu nhập kho")
    @ResponseBody
    @RequestMapping(value = "/updatePhieuNhapKho", method = RequestMethod.POST)
    public int updatePhieuNhapKho(@RequestBody JSONObject jsonObject,
                                  HttpSession session,
                                  HttpServletResponse response,
                                  HttpServletRequest request) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 1, "Cập nhật phiếu nhập kho, nghiệp vụ " + jsonObject.get("maNghiepVuChuyen").toString(),
                0, jsonObject.get("dvtt").toString() , 0,
                clientIP, hostIP, "", "updatePhieuNhapKho", dataSourceDuocV2);
            NhapKhoObj nhapKhoObj = new NhapKhoObj(jsonObject, session, idGiaoDich);
            response.setStatus(HttpServletResponse.SC_CREATED);
            return nhapKhoDAO.updatePhieuNhapKho(nhapKhoObj);
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return -1;
        }
    }

    @RequestMapping(value = "/deletePhieuNhapKho", method = RequestMethod.DELETE)
    @ResponseBody
    public int deletePhieuNhapKho(@RequestParam(value = "idPhieuNhap") long idPhieuNhap,
                           @RequestParam(value = "dvtt") String dvtt,
                           @RequestParam(value = "nguoiXoa") long nguoiXoa,
                           HttpSession session,
                           HttpServletRequest request,
                           HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 6, "Xóa phiếu nhập kho",
                nguoiXoa, dvtt, 0,
                clientIP, hostIP, "", "deletePhieuNhapKho", dataSourceDuocV2);
            return nhapKhoDAO.deletePhieuNhapKho(idPhieuNhap, dvtt, nguoiXoa, Long.parseLong(idGiaoDich));
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Lấy danh sách chi tiết phiếu nhập kho")
    @RequestMapping(value="/getDanhSachChiTietPhieuNhapKho", method = RequestMethod.GET)
    public List getDanhSachChiTietPhieuNhapKho(@RequestParam(value = "idNhapKho") long idNhapKho,
                                               @RequestParam(value = "loaiDanhSach") int loaiDanhSach,
                                               HttpSession session) {
        return nhapKhoDAO.getDanhSachChiTietPhieuNhapKho(loaiDanhSach, idNhapKho);
    }

    @ApiOperation("Lấy danh sách nhân viên kiểm nhập")
    @RequestMapping(value="/getDanhSachNhanVienKiemNhap", method = RequestMethod.GET)
    public List getDanhSachNhanVienKiemNhap(@RequestParam(value = "idNhapKho") long idNhapKho,
                                            @RequestParam(value = "dvtt") String dvtt,
                                            HttpSession session) {
        return nhapKhoDAO.getDanhSachNhanVienKiemNhap(idNhapKho, dvtt);
    }

    @ApiOperation("Thêm nhân viên kiểm nhập cho phiếu nhập")
    @ResponseBody
    @RequestMapping(value = "/addNhanVienKiemNhap", method = RequestMethod.POST)
    public int addNhanVienKiemNhap(@RequestBody JSONObject jsonObject,
                            HttpSession session,
                            HttpServletResponse response,
                            HttpServletRequest request) {
        try {
            return nhapKhoDAO.addNhanVienKiemNhap(Long.parseLong(jsonObject.get("idNhapKhoTuNhaCungCap").toString()),
                jsonObject.get("soPhieuNhap").toString(),
                jsonObject.get("dvtt").toString(),
                Integer.parseInt(jsonObject.get("maNhanVien").toString()),
                jsonObject.get("tenNhanVien").toString(),
                jsonObject.get("chucDanh").toString());
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Xóa nhân viên kiểm nhập cho phiếu nhập")
    @ResponseBody
    @RequestMapping(value = "/deleteNhanVienKiemNhap", method = RequestMethod.DELETE)
    public int deleteNhanVienKiemNhap(@RequestParam (value = "idPhieuNhap")int idNhapKhoNhaCC,
                                   @RequestParam (value = "dvtt")String dvtt,
                                   @RequestParam (value = "maNhanVien")int maNhanVien,
                                   HttpSession session,
                                   HttpServletResponse response,
                                   HttpServletRequest request) {
        try {
            return nhapKhoDAO.deleteNhanVienKiemNhap(idNhapKhoNhaCC, dvtt, maNhanVien);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Xóa chi tiết hóa đơn")
    @ResponseBody
    @RequestMapping(value = "/deleteChiTietHoaDon", method = RequestMethod.DELETE)
    public int deleteChiTietHoaDon(@RequestParam(value = "idChiTiet") long idChiTiet,
                            @RequestParam(value = "idPhieuNhap") long idPhieuNhap,
                            @RequestParam(value = "dvtt") String dvtt,
                            @RequestParam(value = "nguoiXoa") long nguoiXoa,
                            HttpSession session, HttpServletRequest request,
                            HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 4, "Xóa chi tiết phiếu nhập kho hóa đơn",
                nguoiXoa, dvtt, 0,
                clientIP, hostIP, "", "xoaChiTietPhieuNhapKhoHoaDon", dataSourceDuocV2);
            return nhapKhoDAO.deleteChiTietHoaDon(idChiTiet, idPhieuNhap, dvtt, nguoiXoa, Long.parseLong(idGiaoDich));
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Xóa chi tiết hóa đơn")
    @ResponseBody
    @RequestMapping(value = "/deleteChiTietNhapKho", method = RequestMethod.DELETE)
    public int deleteChiTietNhapKho(@RequestParam(value = "idChiTiet") long idChiTiet,
                             @RequestParam(value = "dvtt") String dvtt,
                             @RequestParam(value = "nguoiXoa") long nguoiXoa,
                             HttpSession session,
                             HttpServletRequest request,
                             HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 4, "Xóa chi tiết phiếu nhập kho",
                nguoiXoa, dvtt, 0,
                clientIP, hostIP, "", "deleteChiTietNhapKho", dataSourceDuocV2);
            return nhapKhoDAO.deleteChiTietNhapKho(idChiTiet, dvtt, nguoiXoa, Long.parseLong(idGiaoDich));
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Xóa nhập kho (Quy cách)")
    @ResponseBody
    @RequestMapping(value = "/deleteNhapKho", method = RequestMethod.DELETE)
    public int deleteNhapKho(@RequestParam(value = "idPhieuNhap") long idPhieuNhap,
                      @RequestParam(value = "dvtt") String dvtt,
                      @RequestParam(value = "nguoiXoa") long nguoiXoa,
                      @RequestParam(value = "maPhong") String maPhong,
                      @RequestParam(value = "maKhoa") String maKhoa,
                      HttpSession session,
                      HttpServletRequest request,
                      HttpServletResponse response) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 4, "Xóa chi tiết phiếu nhập kho",
                nguoiXoa, dvtt, 0,
                clientIP, hostIP, "", "deleteChiTietNhapKho", dataSourceDuocV2);
            return nhapKhoDAO.deleteNhapKho(idPhieuNhap, dvtt, nguoiXoa, Long.parseLong(idGiaoDich), maKhoa, maPhong);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @ApiOperation("Thêm chi tiết hóa đơn")
    @ResponseBody
    @RequestMapping(value = "/addChiTietHoaDon", method = RequestMethod.POST)
    public Map addChiTietHoaDon(@RequestBody ChiTietNhapKhoObj jsonObject,
                                         HttpSession session,
                                         HttpServletResponse response,
                                         HttpServletRequest request) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String idGiaoDich = DuocTransaction.insert(0, 3, "Thêm chi tiết hóa đơn",
                jsonObject.getMaNguoiNhap(), jsonObject.getDvtt(), 0,
                clientIP, hostIP, "", "addChiTietHoaDon", dataSourceDuocV2);
            ChiTietNhapKhoObj tempChiTiet = jsonObject;
            tempChiTiet.setIdGiaoDich(Long.parseLong(idGiaoDich));
            response.setStatus(HttpServletResponse.SC_CREATED);
            return nhapKhoDAO.addChiTietHoaDon(tempChiTiet);
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return new HashMap();
        }
    }

    @ApiOperation("Thêm chi tiết nhập kho")
    @ResponseBody
    @RequestMapping(value = "/addChiTietNhapKho", method = RequestMethod.POST)
    public int addChiTietNhapKho(@RequestBody List<ChiTietNhapKhoObj> jsonObjects,
                                                   HttpSession session,
                                                   HttpServletResponse response,
                                                   HttpServletRequest request) {
        try {
            String hostIP = InetAddress.getLocalHost().getHostAddress();
            String clientIP = request.getRemoteAddr();
            String dvtt = jsonObjects.get(0).getDvtt();
            long maUser = jsonObjects.get(0).getMaNguoiNhap();
            String tsTinhDonGia = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 203);
            String tsCMU = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 96064);

            String idGiaoDich = DuocTransaction.insert(0, 7, "Thêm chi tiết phiếu nhập kho sau khi tính",
                maUser, dvtt, 0, clientIP, hostIP, "", "addChiTietNhapKho", dataSourceDuocV2);
            // Check thuốc đã sử dụng thì ko cho nhập kho
            long idNhapKho = jsonObjects.get(0).getIdPhieuNhap();
            long maNghiepVu = jsonObjects.get(0).getNghiepVu();
            if (nhapKhoDAO.checkThuocDaSuDung(idNhapKho, maNghiepVu) == 1) {
                return 1;
            }
            for (ChiTietNhapKhoObj chiTietNhapKhoObj : jsonObjects) {
                ChiTietNhapKhoObj tempChiTietObj = chiTietNhapKhoObj;
                tempChiTietObj.setIdGiaoDich(Long.parseLong(idGiaoDich));
                if (chiTietNhapKhoObj.getNghiepVu() == 24) {
                    if (tsTinhDonGia.equals("1")) {
                        List<Map<String, Object>> listTinh = nhapKhoDAO.getDonGiaVaSoLuong(String.valueOf(tempChiTietObj.getSoLuongTruocQuyDoi()),
                            tempChiTietObj.getDonGiaHoaDon().toString(),
                            String.valueOf(tempChiTietObj.getSoLuongQuyCach()),
                            tempChiTietObj.getDonGiaChinhSua().toString(),
                            String.valueOf(chiTietNhapKhoObj.getVat()),
                            chiTietNhapKhoObj.getThanhTienVAT().toString());
                        if (!listTinh.isEmpty()) {
                            for (Map m : listTinh) {
                                tempChiTietObj.setSoLuong(Double.parseDouble(m.get("SOLUONG").toString()));
                                tempChiTietObj.setDonGiaTruocVAT(Double.parseDouble(m.get("DONGIATRUOCVAT").toString()));
                                tempChiTietObj.setDonGia(Double.parseDouble(m.get("DONGIA").toString()));
                                tempChiTietObj.setThanhTien(tempChiTietObj.getSoLuong() * tempChiTietObj.getDonGia());
                                if (tempChiTietObj.getSoLuong() > 0) {
                                    int kq = nhapKhoDAO.addChiTietNhapKho(tempChiTietObj);
                                    System.out.println("----Thêm chi tiết nhập kho: " + kq + "\n");
                                    if (kq == 1) {
                                        return kq;
                                    }
                                }
                            }
                        }
                    } else if (tsTinhDonGia.equals("2")) {
                        tempChiTietObj.setSoLuong(Double.parseDouble(String.valueOf(tempChiTietObj.getSoLuongQuyDoi())));
                        tempChiTietObj.setDonGiaTruocVAT(tempChiTietObj.getDonGiaQuyDoi());
                        if (tsCMU.equals("1")) {
                            tempChiTietObj.setDonGia(tempChiTietObj.getDonGiaChinhSua());
                        } else {
                            tempChiTietObj.setDonGia(tempChiTietObj.getDonGiaQuyDoiVAT());
                        }
                        tempChiTietObj.setThanhTien(tempChiTietObj.getSoLuong() * tempChiTietObj.getDonGia());
                        if (tempChiTietObj.getSoLuong() > 0) {
                            int kq = nhapKhoDAO.addChiTietNhapKho(tempChiTietObj);
                            if (kq == 1) {
                                return kq;
                            }
                        }
                    } else {
                        tempChiTietObj.setSoLuong(Double.parseDouble(String.valueOf(tempChiTietObj.getSoLuongQuyDoi())));
                        tempChiTietObj.setDonGiaTruocVAT(tempChiTietObj.getDonGiaQuyDoi());
                        if (tsCMU.equals("1")) {
                            tempChiTietObj.setDonGia(tempChiTietObj.getDonGiaChinhSua());
                        } else {
                            tempChiTietObj.setDonGia(tempChiTietObj.getDonGiaQuyDoiVAT());
                        }
                        tempChiTietObj.setThanhTien(tempChiTietObj.getSoLuong() * tempChiTietObj.getDonGia());

                        if (tempChiTietObj.getThanhTienHoaDon() != tempChiTietObj.getThanhTien()) {
                            tempChiTietObj.setThanhTien(tempChiTietObj.getThanhTienHoaDon());
                        }
                        if (chiTietNhapKhoObj.getSoLuong() > 0) {
                            int kq = nhapKhoDAO.addChiTietNhapKho(chiTietNhapKhoObj);
                            if (kq == 1) {
                                return kq;
                            }
                        }
                    }
                } else if (tempChiTietObj.getNghiepVu() == 23) {
                    tempChiTietObj.setSoLuong(Double.parseDouble(String.valueOf(tempChiTietObj.getSoLuongQuyDoi())));
                    tempChiTietObj.setDonGia(tempChiTietObj.getDonGiaSauHaoHut());
                    tempChiTietObj.setThanhTien(tempChiTietObj.getSoLuong() * tempChiTietObj.getDonGia());
                    ///////// set default tham số hóa đơn
                    tempChiTietObj.setQuyCach(" ");
                    tempChiTietObj.setQuyCachQuyDoi(0.0);
                    tempChiTietObj.setSoLuongQuyDoi(0.0);
                    tempChiTietObj.setDonGiaQuyDoi(0.0);
                    tempChiTietObj.setDonGiaHoaDon(0.0);
                    tempChiTietObj.setDonGiaQuyDoiVAT(0.0);
                    tempChiTietObj.setTienVAT(0.0);
                    tempChiTietObj.setDuocConLai(0.0);
                    ////////
                    Map m = nhapKhoDAO.addChiTietHoaDon(chiTietNhapKhoObj);
                    if (m.get("ID") != null) {
                        tempChiTietObj.setIdNhapKhoCT(Long.parseLong(m.get("ID").toString()));
                    } else if (m.get("SAISOT").toString().equals("3")) {
                        return 3;
                    } else return -1;
                    if (tempChiTietObj.getSoLuong() > 0) {
                        int kq = nhapKhoDAO.addChiTietNhapKho(chiTietNhapKhoObj);
                        if (kq == 1) {
                            return kq;
                        }
                    }
                }else{//nghiep vu = 52
                    tempChiTietObj.setSoLuong(tempChiTietObj.getSoLuongTruocHaoHut());
                    tempChiTietObj.setDonGiaTruocVAT(tempChiTietObj.getDonGiaTruocHaoHut());
                    tempChiTietObj.setDonGia(tempChiTietObj.getDonGiaTruocVAT() + tempChiTietObj.getDonGiaTruocVAT()*tempChiTietObj.getVat()/100);
                    tempChiTietObj.setThanhTien(tempChiTietObj.getSoLuong() * tempChiTietObj.getDonGia());
                    ///////// set default tham số hóa đơn
                    tempChiTietObj.setQuyCach(" ");
                    tempChiTietObj.setQuyCachQuyDoi(0.0);
                    tempChiTietObj.setSoLuongQuyDoi(0.0);
                    tempChiTietObj.setDonGiaQuyDoi(0.0);
                    tempChiTietObj.setDonGiaHoaDon(0.0);
                    tempChiTietObj.setDonGiaQuyDoiVAT(0.0);
                    tempChiTietObj.setTienVAT(0.0);
                    tempChiTietObj.setDuocConLai(0.0);
                    ////////
                    Map m = nhapKhoDAO.addChiTietHoaDon(tempChiTietObj);
                    if (m.get("ID") != null) {
                        tempChiTietObj.setIdNhapKhoCT(Long.parseLong(m.get("ID").toString()));
                    } else if (m.get("SAISOT").toString().equals("3")) {
                        return 3;
                    } else return -1;
                    if (tempChiTietObj.getSoLuong() > 0) {
                        int kq = nhapKhoDAO.addChiTietNhapKho(tempChiTietObj);
                        if (kq == 1) {
                            return kq;
                        }
                    }
                }
            }
            response.setStatus(HttpServletResponse.SC_CREATED);
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return -1;
        }
    }

    @ApiOperation("In phiếu nhập kho (hóa đơn)")
    @ResponseBody
    @RequestMapping(value = "/printPhieuNhapKhoHoaDon", method = RequestMethod.GET)
    public void inPhieuNhapKhoTuNhaCungCapHoaDon(HttpServletResponse response,
                                          HttpServletRequest request,
                                          @RequestParam(value = "idPhieuNhap") long idPhieuNhap,
                                          @RequestParam(value = "soPhieuNhap") String soPhieuNhap,
                                          @RequestParam(value = "soHoaDon") String soHoaDon,
                                          @RequestParam(value = "dvtt") String dvtt,
                                          @RequestParam(value = "nguoiNhan") String nguoiNhan,
                                          @RequestParam(value = "ngayHoaDon") String ngayHoaDon,
                                          @RequestParam(value = "ngayNhap") String ngayNhap,
                                          @RequestParam(value = "daNhapKho") String daNhapKho, // Đã nhập kho: 0: chưa   1: đã nhập kho
                                          @RequestParam(value = "loaiNhapKho") String loaiNhapKho, // Loại nhập kho: 0: quy cách   1: đông y
                                          @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                          @RequestParam(value = "loaiFile") String loaiFile, // Loại file: pdf/xls
                                          @RequestParam(value = "tenTinh") String tenTinh,
                                          @RequestParam(value = "maKhoa") String maKhoa,
                                          @RequestParam(value = "tenKhoa") String tenKhoa,
                                          @RequestParam(value = "nguoiIn") String nguoiIn
                                          ) {
        try {
            List<Map<String, Object>> listthongtin = nhapKhoDAO.getThongTinPhieuNhapKho(soPhieuNhap, soHoaDon, dvtt);
            Map mapthongtin = new HashMap();
            if (!listthongtin.isEmpty()) {
                mapthongtin = listthongtin.get(0);
            }
            String[] arr_ngaynhap = ngayNhap.split("-");
            String ngay = arr_ngaynhap[2];
            String nam = arr_ngaynhap[0];
            Map parameters = new HashMap();
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            String tenkhoa = heThongDAO.getTenPhongBan(maKhoa);
            parameters.put("tenkhoa", tenkhoa);
            parameters.put("tensoyte", donviquanlytructiep);
            parameters.put("dvtt", dvtt);
            parameters.put("sophieunhap", soPhieuNhap);
            parameters.put("soluutru", mapthongtin.get("SoluuTru").toString());
            parameters.put("tennhacc", mapthongtin.get("TenNhaCC").toString());
            parameters.put("nhaptaikho", mapthongtin.get("TenKhoVatTu").toString());
            parameters.put("tenbenhvien", tenBenhVien);
            parameters.put("nguoilapbang", nguoiIn);
            parameters.put("sohoadon", soHoaDon);
            parameters.put("ngayhoadon", DuocUtils.convertFromFormatToDate(ngayHoaDon, "-", "/"));
            parameters.put("nguoinhan", nguoiNhan);
            parameters.put("diengiai", mapthongtin.get("GhiChu").toString());
            parameters.put("ngay", ngay);
            parameters.put("nam", nam);
            parameters.put("ngaynhap", " Ngày " + arr_ngaynhap[2] + " tháng " + arr_ngaynhap[1] + " năm " + arr_ngaynhap[0]);
            String diachi = mapthongtin.get("DIACHI") == null ? "" : mapthongtin.get("DIACHI").toString();
            String msthue = mapthongtin.get("MSTHUE") == null ? "" : mapthongtin.get("MSTHUE").toString();
            parameters.put("diachi", diachi);
            parameters.put("msthue", msthue);
            parameters.put("ngayhientai", "Ngày " + (new Date().getDate()) + " tháng " + ((new Date()).getMonth()+1) + " năm " + (new Date()).getYear());
            parameters.put("nguoinhan", mapthongtin.get("nguoinhan").toString());
            parameters.put("nguoigiaohang", mapthongtin.get("nguoigiaohang").toString());
            parameters.put("diadiemnhapkho", tenBenhVien);
            parameters.put("ketoantruong", " ");
            parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
            File reportFile;
            if (daNhapKho.equals("0")) {
                parameters.put("sotien", new DocSo().docso(Double.parseDouble(nhapKhoDAO.getTongTienPhieuNhapKho(idPhieuNhap, loaiNhapKho, "0", dvtt))));
                String reportName = "rp_phieunhaptukhonhacungcap_hoadon.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "nhapkho/" + reportName).getFile();
            } else {
                parameters.put("sotien", new DocSo().docso(Double.parseDouble(nhapKhoDAO.getTongTienPhieuNhapKho(idPhieuNhap, loaiNhapKho,"1", dvtt))));
                String reportName = "rp_phieunhapkho_phieunhap.jasper";
                reportFile = new ClassPathResource(DuocConfigs.reportURL + "nhapkho/" + reportName).getFile();
            }
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ApiOperation("Xuất danh mục mẫu")
    @ResponseBody
    @RequestMapping(value = "/exportDanhMucVatTuMau", method = RequestMethod.GET)
    public ModelAndView exportDanhMucVatTuMau(@RequestParam(value = "dvtt") String dvtt) {
        List<Map<String, Object>> list = nhapKhoDAO.getDanhSachVatTuMau(dvtt);
        ExportXLSView.sheetName = "DSvattu";
        ExportXLSView.fileName = "dsvattu.xls";
        return new ModelAndView(new ExportXLSView(), "report_view", list);
    }

    @ApiOperation("In biên bản kiểm nhập")
    @RequestMapping(value = "/printBienBanKiemNhap", method = RequestMethod.GET)
    public @ResponseBody
    void inBienBanNhapKhoNhaCungCap(HttpServletResponse response,
                                    HttpServletRequest request,
                                    @RequestParam(value = "idPhieuNhap") long idPhieuNhap,
                                    @RequestParam(value = "soPhieuNhap") String soPhieuNhap,
                                    @RequestParam(value = "soHoaDon") String soHoaDon,
                                    @RequestParam(value = "dvtt") String dvtt,
                                    @RequestParam(value = "ngayNhap") String ngayNhap,
                                    @RequestParam(value = "ngayHoaDon") String ngayHoaDon,
                                    @RequestParam(value = "maKhoNhan") String maKhoNhan,
                                    @RequestParam(value = "hoiDongKiemNhap", defaultValue = " ", required = false) String hoiDongKiemNhap,
                                    @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                    @RequestParam(value = "tenTinh") String tenTinh,
                                    @RequestParam(value = "daNhapKho") String daNhapKho, // 0: Chưa nhập   1: Đã nhập
                                    @RequestParam(value = "loaiNhapKho") String loaiNhapKho, // 0: Đông y      1: Quy cách
                                    @RequestParam(value = "loaiFile") String loaiFile, // pdf   xls
                                    HttpSession session) {
        try {
            String in_bb_theonn = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 89052);
            Map parameters = new HashMap();
            parameters.put("id_nhapkhotunhacc", idPhieuNhap);
            parameters.put("dvtt", dvtt);
            parameters.put("sophieunhap", soPhieuNhap);
            parameters.put("sohoadon", soHoaDon);
            parameters.put("ngaynhap", ngayNhap);
            parameters.put("ngayhoadon", ngayHoaDon);
            parameters.put("makho", maKhoNhan);
            parameters.put("hoidongkiemnhap", hoiDongKiemNhap);
            String chutich = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 93061);
            parameters.put("chutich", chutich);
            String ketoan = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 93062);
            parameters.put("ketoan", ketoan);
            String khoaduoc = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 93063);
            parameters.put("khoaduoc", khoaduoc);
            String thukho = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 93064);
            parameters.put("thukho", thukho);
            String cungung = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 93065);
            parameters.put("cungung", cungung);
            String giokt = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 93066);
            parameters.put("giokt", giokt);
            String phutkt = heThongDAO.getMoTaThamSoByMaThamSo(dvtt, 93067);
            parameters.put("phutkt", phutkt);

            if (!in_bb_theonn.equals("1")) {
                parameters.put("thanh_tien_bang_chu", new DocSo().docso(Double.parseDouble(nhapKhoDAO.getTongTienPhieuNhapKho(idPhieuNhap, loaiNhapKho, daNhapKho, dvtt))));
            }
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            parameters.put("so_y_te", donviquanlytructiep);
            parameters.put("ten_dvtt", tenBenhVien);

            File reportFile;
            // ... There is something here!!!
            String reportName = "rp_bienbankiemnhaphoadon.jasper";
            reportFile = new ClassPathResource(DuocConfigs.reportURL + "nhapkho/" + reportName).getFile();
            parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
            parameters.put("SUBREPORT_DIR", DuocConfigs.reportURL + "nhapkho/");
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @ApiOperation("In biên bản kiểm nhập nhiều phiếu")
    @RequestMapping(value = "/printBienBanKiemNhapNhieuPhieu", method = RequestMethod.GET)
    public @ResponseBody
    void printBienBanKiemNhapNhieuPhieu(HttpServletResponse response,
                                              HttpServletRequest request,
                                              @RequestParam(value = "dvtt") String dvtt,
                                              @RequestParam(value = "soPhieuNhap") String soPhieuNhap,
                                              @RequestParam(value = "soHoaDon") String soHoaDon,
                                              @RequestParam(value = "maKhoNhan") String maKhoNhan,
                                              @RequestParam(value = "idHoiDong", required = false) long idHoiDong,
                                              @RequestParam(value = "loaiFile") String loaiFile,
                                              @RequestParam(value = "tenBenhVien") String tenBenhVien,
                                              @RequestParam(value = "tenTinh") String tenTinh,
                                              HttpSession session) {
        try {
            Map parameters = new HashMap();
            parameters.put("dvtt", dvtt);
            parameters.put("sophieunhap", soPhieuNhap);
            parameters.put("sohoadon", soHoaDon);
            parameters.put("makho", maKhoNhan);
            String donviquanlytructiep = "SỞ Y TẾ " + tenTinh.toUpperCase();
            parameters.put("so_y_te", donviquanlytructiep);
            parameters.put("ten_dvtt", tenBenhVien);
            Map m_kiemnhap = nhapKhoDAO.getHoiDongKiemNhap(idHoiDong, dvtt);
            if (!m_kiemnhap.isEmpty() && !(m_kiemnhap.get("ONGBA1") + "").equals("")) {
                parameters.put("chucdanh1", m_kiemnhap.get("CHUCDANH1") + "");
                parameters.put("chucdanh2", m_kiemnhap.get("CHUCDANH2") + "");
                parameters.put("chucdanh3", m_kiemnhap.get("CHUCDANH3") + "");
                parameters.put("chucdanh4", m_kiemnhap.get("CHUCDANH4") + "");
                parameters.put("chucdanh5", m_kiemnhap.get("CHUCDANH5") + "");
                parameters.put("hoten1", m_kiemnhap.get("ONGBA1") + "");
                parameters.put("hoten2", m_kiemnhap.get("ONGBA2") + "");
                parameters.put("hoten3", m_kiemnhap.get("ONGBA3") + "");
                parameters.put("hoten4", m_kiemnhap.get("ONGBA4") + "");
                parameters.put("hoten5", m_kiemnhap.get("ONGBA5") + "");
            } else {
                parameters.put("chucdanh1", "1. Phụ trách dược");
                parameters.put("chucdanh2", "2. Phụ trách kế toán");
                parameters.put("chucdanh3", "3. Nghiệp vụ dược");
                parameters.put("chucdanh4", "4. Thống kê dược");
                parameters.put("chucdanh5", "5. Thủ kho chẵn");
                parameters.put("hoten1", "....................................");
                parameters.put("hoten2", "....................................");
                parameters.put("hoten3", "....................................");
                parameters.put("hoten4", "....................................");
                parameters.put("hoten5", "....................................");
            }
            File reportFile;
            // ... There is something here!!!
            String reportName = "rp_bienbankiemnhaphoadon_nhieuphieu.jasper";
            reportFile = new ClassPathResource(DuocConfigs.reportURL + "nhapkho/" + reportName).getFile();
            JasperHelper.printReport(loaiFile, reportFile, parameters, DataSourceUtils.getConnection(dataSourceDuocV2), response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
